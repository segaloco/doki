.include	"tunables.i"

.segment	"SAVE_DATDATA"

	.export save_dat, save_dat_end, save_magic
save_dat:
	.res	ENG_CHARACTERS, 0
save_dat_end:

save_magic:
	.word	ENG_SAVE_MAGIC
save_magic_end:

.code

	.export save_null
save_null:
	.res	(save_magic_end-save_dat), $FF
