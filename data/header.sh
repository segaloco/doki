#!/bin/sh

# magic number
printf "FDS\032" > data/header.bin

# 2 disk sides
printf "\002" >> data/header.bin

# padding
printf "\000\000\000\000\000\000\000\000\000\000\000" >> data/header.bin
