.include	"system/palette.i"

	.export	title_palette, title_palette_end
title_palette:
	.byte	$12, $30, $27, $0F
	.byte	$12, $24, $18, $0F
	.byte	$12, $12, $12, $12
	.byte	$12, $30, $30, $12

	.byte	$12, $30, $28, $0F
	.byte	$12, $30, $25, $0F
	.byte	$12, $30, $12, $0F
	.byte	$12, $30, $23, $0F
title_palette_end:
