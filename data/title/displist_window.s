.include	"system/ppu.i"

.include	"tunables.i"

	.export	nmi_title_buffer_window
nmi_title_buffer_window:
	.dbyt	$2086
	.byte	$05
	.byte	$97
	sta	$9997, y
	.byte	$93
	jsr	$6A6
	tya
	txs
	tya
	txs
	.byte	$FB
	.byte	$93
	.byte	$20
	.byte	$C6
	.byte	$07
	.byte	$97
	.byte	$99
	.byte	$97
	.byte	$99
	.byte	$FB
	.byte	$FB
	.byte	$93
	jsr	$8E6
	tya
	txs
	tya
	txs
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$93
	and	($06, x)
	ora	#$97
	sta	$9997, y
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$93
	and	($26, x)
	asl	a
	tya
	txs
	tya
	txs
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$93
	and	($46, x)
	lsr	a
	.byte	$FB
	and	($50, x)
	ora	($93, x)
	and	($66, x)
	.byte	$4B
	.byte	$FB
	and	($71, x)
	ora	($93, x)
	and	($86, x)
	jmp	$21FB
	.byte	$92
	ora	($93, x)
	and	($A6, x)
	eor	$21FB
	.byte	$B3
	ora	($93, x)
	and	($C6, x)
	lsr	$21FB
	.byte	$D4
	ora	($93, x)
	and	($E6, x)
	.byte	$4F
	.byte	$FB
	and	($F5, x)
	ora	($93, x)
	.byte	$22
	asl	$50
	.byte	$FB
	.byte	$22
	asl	$01, x
	.byte	$93
	.byte	$22
	rol	$51
	.byte	$FB
	.byte	$22
	.byte	$37
	ora	($93, x)
	and	($E8, x)
	.byte	$83
	stx	$8E8F
	.byte	$22
	.byte	$33
	.byte	$83
	stx	$8E8F
	.byte	$22
	pha
	dec	$8F
	.byte	$22
	.byte	$93
	cpy	$8F
	.byte	$22
	lsr	$C8
	sta	$22, x
	.byte	$47
	iny
	sta	$22, x
	eor	#$4A
	sta	$22, x
	.byte	$54
	.byte	$44
	sta	$22, x
	cli
	ora	($96, x)
	.byte	$22
	adc	#$4A
	bcc	LC2A1
	.byte	$74
	eor	$95
	.byte	$22
	adc	$9601, y
	.byte	$22
	.byte	$89
	lsr	a
	.byte	$FC
	.byte	$22
	sty	$46, x
	sta	$22, x
	txs
	ora	($96, x)
	.byte	$22
	lda	#$4A
	.byte	$92
	.byte	$22
	lda	$46, x
	sta	$22, x
	.byte	$BB
	ora	($96, x)
	.byte	$22
	cmp	#$4A
LC2A1:	sta	($22), y
	cmp	$47, x
	sta	$22, x
	.byte	$DC
	ora	($96, x)
	.byte	$22
	sbc	$48, x
	sta	$22, x
	sbc	$9601, x
	.byte	$23
	ora	$49, x
	sta	$23, x
	asl	$9601, x
	.byte	$23
	plp
	.byte	$57
	sta	$23, x
	.byte	$3F
	ora	($96, x)
	.byte	$22
	bit	$8204
	sta	$88
	.byte	$8B
	.byte	$22
	jmp	$8304
	stx	$89
	sty	$6C22
	.byte	$04
	sty	$87
	txa
	sta	$C023
	eor	#$00
	.byte	$23
	cmp	#$03
	.byte	$44
	ora	$10, x
	.byte	$23
	cpy	a:$45
	.byte	$23
	cmp	($04), y
	.byte	$04
	ora	($04, x)
	bpl	LC310
	cmp	$45, x
	brk
	.byte	$23
	.byte	$DA
	asl	$10
	brk
	.byte	$04
	bpl	LC2F8
LC2F8:	brk
	.byte	$23
	cpx	#$12
	brk
	rti
	.byte	$52
	brk
	.byte	$54
	.byte	$54
	bpl	LC304
LC304:	brk
	.byte	$44
	eor	($50), y
	.byte	$54
	eor	$55, x
	bpl	LC30D
LC30D:	.byte	$04
	.byte	$23
	.byte	$F2
LC310:	lsr	$05
	.byte	$23
	sed
	pha
	brk

	.byte	NMI_LIST_END
