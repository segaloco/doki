.include	"modes.i"

	.export	title_actor_balloon_cues
title_actor_balloon_cues:
	.byte	34, 26, 18
; --------------
	.export	title_cycle_attrs_a, title_cycle_attrs_b
title_cycle_attrs_a:
	.byte	nmi_title_buffer_modes::attr_2
	.byte	nmi_title_buffer_modes::attr_4
	.byte	nmi_title_buffer_modes::attr_6

title_cycle_attrs_b:
	.byte	nmi_title_buffer_modes::attr_1
	.byte	nmi_title_buffer_modes::attr_3
	.byte	nmi_title_buffer_modes::attr_5
