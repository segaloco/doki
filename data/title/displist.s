.include	"system/ppu.i"

.include	"charmap.i"
.include	"tunables.i"

	.export	nmi_title_buffer_title_bg
nmi_title_buffer_title_bg:
	.dbyt	$2073
	.byte	(:++)-(:+)
	: .byte	$7B, $75, $75, $75, $75, $75, $75, $75, $75, $75, $78
	:

	.dbyt	$2093
	.byte	(:++)-(:+)
	: .byte	$7C, $00, $01, $02, $03, $04, $05, $06, $07, $08, $79
	:

	.dbyt	$20B3
	.byte	(:++)-(:+)
	: .byte	$7C, $09, $0A, $0B, $FC, $0C, $0D, $0E, $0F, $10, $79
	:

	.dbyt	$20D3
	.byte	(:++)-(:+)
	: .byte	$7C, $11, $12, $13, $14, $15, $16, $17, $18, $19, $79
	:

	.dbyt	$20F3
	.byte	(:++)-(:+)
	: .byte	$7D, $76, $76, $76, $76, $76, $76, $76, $76, $76, $7A
	:
; --------------
	.dbyt	$2164
	.byte	(:++)-(:+)
	: .byte	$6D, $6E
	:

	.dbyt	$2182
	.byte	(:++)-(:+)
	: .byte	$6D, $6E, $6F, $70, $6D, $6E
	:

	.dbyt	$21A2
	.byte	(:++)-(:+)
	: .byte	$6F, $70, $71, $72, $6F, $70
	:

	.dbyt	$21C0
	.byte	(:++)-(:+)
	: .byte	$75, $75, $73, $74, $47, $59, $73, $74, $75, $78
	:

	.dbyt	$21E0
	.byte	(:++)-(:+)
	: .byte	$FC, $FC, $77, $77, $77, $77, $77, $77, $FC, $79
	:

	.dbyt	$2200
	.byte	(:++)-(:+)
	: .byte	$76, $76, $76, $76, $76, $76, $76, $76, $76, $7A
	:
; --------------
	.dbyt	$22DB
	.byte	(:++)-(:+)
	: .byte	$7B, $75, $75, $75, $75
	:

	.dbyt	$22FB
	.byte	(:++)-(:+)
	: .byte	$7C, $FC, $FC, $FC, $FC
	:

	.dbyt	$231B
	.byte	(:++)-(:+)
	: .byte	$7D, $76, $76, $76, $76
	:
; --------------
	.dbyt	$2113
	.byte	(:++)-(:+)
	: .byte	$5F
	:

	.dbyt	$212F
	.byte	$07
	.byte	$1A
	.byte	$1B
	.byte	$5D
	.byte	$5E
	.byte	$1C
	.byte	$1D
	.byte	$60

	.dbyt	$214B
	.byte	$0D
	.byte	$1E
	.byte	$1F
	.byte	$20
	.byte	$21
	.byte	$22
	.byte	$23
	.byte	$24
	.byte	$25
	.byte	$26
	.byte	$27
	.byte	$28
	.byte	$29
	.byte	$2A

	.dbyt	$216A
	.byte	$0F
	.byte	$2B
	.byte	$2C
	.byte	$2D
	.byte	$2E
	.byte	$2F
	.byte	$30
	.byte	$31
	.byte	$32
	.byte	$33
	.byte	$34
	.byte	$35
	.byte	$36
	.byte	$37
	.byte	$38
	.byte	$39

	.dbyt	$218B
	.byte	$0E
	.byte	$3A
	.byte	$3B
	.byte	$3C
	.byte	$3D
	.byte	$3E
	.byte	$3F
	.byte	$40
	.byte	$41
	.byte	$42
	.byte	$43
	.byte	$44
	.byte	$45
	.byte	$46
	.byte	$57

	.dbyt	$21AB
	.byte	$0D
	.byte	$48
	.byte	$49
	.byte	$4A
	.byte	$4B
	.byte	$4C
	.byte	$4D
	.byte	$4E
	.byte	$4F
	.byte	$50
	.byte	$51
	.byte	$52
	.byte	$53
	.byte	$54

	.dbyt	$21CB
	.byte	$0A
	.byte	$55
	.byte	$56
	.byte	$FA
	.byte	$58
	.byte	$FA
	.byte	$62
	.byte	$5A
	.byte	$5B
	.byte	$5C
	.byte	$61

	.dbyt	$21F1
	.byte	$03
	.byte	$64
	.byte	$FA
	.byte	$63
; --------------
	.dbyt	$222A
	.byte	$01
	.byte	$5F

	.dbyt	$2246
	.byte	$07
	.byte	$1A
	.byte	$1B
	.byte	$5D
	.byte	$5E
	.byte	$1C
	.byte	$1D
	.byte	$60

	.dbyt	$2262
	.byte	$0D
	.byte	$1E
	.byte	$1F
	.byte	$20
	.byte	$21
	.byte	$22
	.byte	$23
	.byte	$24
	.byte	$25
	.byte	$26
	.byte	$27
	.byte	$28
	.byte	$29
	.byte	$2A

	.dbyt	$2281
	.byte	$0F
	.byte	$2B
	.byte	$2C
	.byte	$2D
	.byte	$2E
	.byte	$2F
	.byte	$30
	.byte	$31
	.byte	$32
	.byte	$33
	.byte	$34
	.byte	$35
	.byte	$36
	.byte	$37
	.byte	$38
	.byte	$39

	.dbyt	$22A2
	.byte	$0E
	.byte	$3A
	.byte	$3B
	.byte	$3C
	.byte	$3D
	.byte	$3E
	.byte	$3F
	.byte	$40
	.byte	$41
	.byte	$42
	.byte	$43
	.byte	$44
	.byte	$45
	.byte	$46
	.byte	$57

	.dbyt	$22C2
	.byte	$0D
	.byte	$48
	.byte	$49
	.byte	$4A
	.byte	$4B
	.byte	$4C
	.byte	$4D
	.byte	$4E
	.byte	$4F
	.byte	$50
	.byte	$51
	.byte	$52
	.byte	$53
	.byte	$54

	.dbyt	$22E2
	.byte	$0A
	.byte	$55
	.byte	$56
	.byte	$FA
	.byte	$58
	.byte	$FA
	.byte	$62
	.byte	$5A
	.byte	$5B
	.byte	$5C
	.byte	$61

	.dbyt	$2308
	.byte	$03
	.byte	$64
	.byte	$FA
	.byte	$63
; --------------
	.dbyt	$21F9
	.byte	$01
	.byte	$5F

	.dbyt	$2215
	.byte	$07
	.byte	$1A
	.byte	$1B
	.byte	$5D
	.byte	$5E
	.byte	$1C
	.byte	$1D
	.byte	$60

	.dbyt	$2231
	.byte	$0D
	.byte	$1E
	.byte	$1F
	.byte	$20
	.byte	$21
	.byte	$22
	.byte	$23
	.byte	$24
	.byte	$25
	.byte	$26
	.byte	$27
	.byte	$28
	.byte	$29
	.byte	$2A

	.dbyt	$2250
	.byte	$0F
	.byte	$2B
	.byte	$2C
	.byte	$2D
	.byte	$2E
	.byte	$2F
	.byte	$30
	.byte	$31
	.byte	$32
	.byte	$33
	.byte	$34
	.byte	$35
	.byte	$36
	.byte	$37
	.byte	$38
	.byte	$39

	.dbyt	$2271
	.byte	$0E
	.byte	$3A
	.byte	$3B
	.byte	$3C
	.byte	$3D
	.byte	$3E
	.byte	$3F
	.byte	$40
	.byte	$41
	.byte	$42
	.byte	$43
	.byte	$44
	.byte	$45
	.byte	$46
	.byte	$57

	.dbyt	$2291
	.byte	$0D
	.byte	$48
	.byte	$49
	.byte	$4A
	.byte	$4B
	.byte	$4C
	.byte	$4D
	.byte	$4E
	.byte	$4F
	.byte	$50
	.byte	$51
	.byte	$52
	.byte	$53
	.byte	$54

	.dbyt	$22B1
	.byte	$0A
	.byte	$55
	.byte	$56
	.byte	$FA
	.byte	$58
	.byte	$FA
	.byte	$62
	.byte	$5A
	.byte	$5B
	.byte	$5C
	.byte	$61

	.dbyt	$22D7
	.byte	$03
	.byte	$64
	.byte	$FA
	.byte	$63
; --------------
	.dbyt	$2344
	.byte	(:++)-(:+)
	: .byte	"c1987_NINTENDO_/_FUJI_TV"
	:
; --------------
	.dbyt	$23F1
	.byte	$47
	.byte	$F0
; --------------
	.export	nmi_title_buffer_attr_0
nmi_title_buffer_attr_0:
	.dbyt	$23D2
	.byte	$05
	.byte	$80
	.byte	$A8
	.byte	$AA
	.byte	$A2
	.byte	$20

	.dbyt	$23DA
	.byte	$04
	.byte	$88
	.byte	$AA
	.byte	$AA
	.byte	$2A

	.dbyt	$23E0
	.byte	$04
	.byte	$80
	.byte	$A0
	.byte	$A8
	.byte	$A0

	.dbyt	$23E8
	.byte	$04
	.byte	$8A
	.byte	$AA
	.byte	$AA
	.byte	$AA

	.dbyt	$23F2
	.byte	$01
	.byte	$FA

	.dbyt	$23DE
	.byte	$01
	.byte	$22

	.dbyt	$23E4
	.byte	$04
	.byte	$AA
	.byte	$AA
	.byte	$AA
	.byte	$A2

	.dbyt	$23EC
	.byte	$04
	.byte	$0A
	.byte	$8A
	.byte	$2A
	.byte	$02

	.byte	NMI_LIST_END
; ------------------------------------------------------------
	.export	nmi_title_buffer_attr_1
nmi_title_buffer_attr_1:
	.dbyt	$23D2
	.byte	$45
	.byte	$00

	.dbyt	$23DA
	.byte	$44
	.byte	$00

	.dbyt	$23DE
	.byte	$01
	.byte	$20

	.byte	NMI_LIST_END
; ------------------------------------------------------------
	.export	nmi_title_buffer_attr_2
nmi_title_buffer_attr_2:
	.dbyt	$23D2
	.byte	$05
	.byte	$40
	.byte	$54
	.byte	$55
	.byte	$51
	.byte	$10

	.dbyt	$23DA
	.byte	$05
	.byte	$44
	.byte	$55
	.byte	$55
	.byte	$15
	.byte	$21

	.byte	NMI_LIST_END
; ------------------------------------------------------------
	.export	nmi_title_buffer_attr_3
nmi_title_buffer_attr_3:
	.dbyt	$23E0
	.byte	$44
	.byte	$00

	.dbyt	$23E8
	.byte	$44
	.byte	$00

	.dbyt	$23F2
	.byte	$01
	.byte	$F0

	.byte	NMI_LIST_END
; ------------------------------------------------------------
	.export	nmi_title_buffer_attr_4
nmi_title_buffer_attr_4:
	.dbyt	$23E0
	.byte	$04
	.byte	$40
	.byte	$50
	.byte	$54
	.byte	$50

	.dbyt	$23E8
	.byte	$04
	.byte	$45
	.byte	$55
	.byte	$55
	.byte	$55

	.dbyt	$23F2
	.byte	$01
	.byte	$F5

	.byte	NMI_LIST_END
; ------------------------------------------------------------
	.export	nmi_title_buffer_attr_5
nmi_title_buffer_attr_5:
	.dbyt	$23DE
	.byte	$01
	.byte	$02

	.dbyt	$23E4
	.byte	$44
	.byte	$00

	.dbyt	$23EC
	.byte	$44
	.byte	$00

	.byte	NMI_LIST_END
; ------------------------------------------------------------
	.export	nmi_title_buffer_attr_6
nmi_title_buffer_attr_6:
	.dbyt	$23DE
	.byte	$01
	.byte	$12

	.dbyt	$23E4
	.byte	$04
	.byte	$55
	.byte	$55
	.byte	$55
	.byte	$51

	.dbyt	$23EC
	.byte	$04
	.byte	$05
	.byte	$45
	.byte	$15
	.byte	$01

	.byte	NMI_LIST_END
