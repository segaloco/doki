.segment	"BG_CHR_CDATA"

	.export	bg_chr_c
bg_chr_c:
; -----------------------------------------------------------
	.incbin "data/ppu/bg_chr_c/tile_bg_80.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_81.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_82.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_83.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_84.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_85.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_86.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_87.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_88.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_89.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_8A.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_8B.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_8C.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_8D.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_8E.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_8F.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_90.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_91.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_92.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_93.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_94.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_95.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_96.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_97.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_98.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_99.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_9A.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_9B.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_9C.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_9D.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_9E.bin"
	.incbin "data/ppu/bg_chr_c/tile_bg_9F.bin"
; -----------------------------------------------------------
