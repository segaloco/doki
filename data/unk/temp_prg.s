.include	"system/ppu.i"

.include	"mem.i"

.include        "unk.i"

.segment	"TEMP_PRGCODE"
; ------------------------------------------------------------
L60EB:
	.byte	L60EF_imajin-L60EF
	.byte	L60EF_lina-L60EF
	.byte	L60EF_mama-L60EF
	.byte	L60EF_papa-L60EF
L60EF:
L60EF_imajin:
	.byte	$00, $04, $02, $01
	.byte	$04, $07, $B0, $B0
	.byte	$98, $98, $A6, $AA
	.byte	$E0, $00, $07, $04
	.byte	$08, $18, $18, $04
	.byte	$E8, $E8, $FC

L60EF_papa:
	.byte	$00, $01, $01, $01
	.byte	$01, $02, $B2, $B2
	.byte	$98, $98, $AD, $AD
	.byte	$E0, $00, $07, $04
	.byte	$08, $18, $1D, $04
	.byte	$E8, $E3, $FC

L60EF_mama:
	.byte	$00, $04, $02, $01
	.byte	$04, $07, $D8, $D8
	.byte	$CB, $CB, $D2, $D6
	.byte	$E0, $00, $02, $01
	.byte	$08, $18, $16, $04
	.byte	$E8, $EA, $FC

L60EF_lina:
	.byte	$00, $06, $04, $02
	.byte	$06, $0C, $B3, $B3
	.byte	$98, $98, $AC, $B3
	.byte	$E0, $3C, $07, $04
	.byte	$08, $18, $15, $04
	.byte	$E8, $EB, $FC

palettes_players:
palette_imajin:	.byte	$FF, $01, $30, $27
palette_lina:	.byte	$FF, $06, $25, $36
palette_mama:	.byte	$FF, $01, $12, $36
palette_papa:	.byte	$FF, $06, $37, $27

L615B:
	.addr	L6271
	.addr	L61C8
	.addr	L6271
	.addr	L631B
	.addr	L6271
	.addr	L61C8
	.addr	L63C1
; -----------------------------
	.export	sub_6169
sub_6169:
        ldx     obj_id_player
        ldy     L60EB, x
        ldx     #$00
	: ; for () {
		lda     L60EF, y
	        sta     tbl_546, x

	        iny
	        inx
	        cpx     #$17
	        bcc     :-
	; }

        lda     obj_id_player
        asl     a
        asl     a
        tay
        ldx     #0
	: ; for (color of row) {
		lda     palettes_players, y
	        sta     palette_player, x

	        iny
	        inx
	        cpx     #PPU_COLOR_ROW_SIZE
	        bcc     :-
	; }

        lda     course_no
        asl     a
        tax
        lda     L615B, x
        sta     zp_addr_04
        lda     L615B+1, x
        sta     zp_addr_04+1
        ldy     #$A9
	: ; for () {
		lda     (zp_addr_04), y
	        sta     eng_map_ppu_buffer_write_str_a, y

	        dey
	        bne     :-
	; }

        lda     (zp_addr_04), y
        sta     eng_map_ppu_buffer_write_str_a, y
        ldy     #$68
	: ; for () {
		lda     L6462, y
	        sta     tbl_55F, y

	        dey
	        cpy     #$FF
	        bne     :-
	; }

        ldy     #$B5
	: ; for () {
		lda     L64CB, y
	        sta     tbl_63B, y

	        dey
	        cpy     #$FF
	        bne     :-
	; }

        rts
; ------------------------------------------------------------
L61C8:
	.byte	$20
	.byte	$00
	.byte	$04
	.byte	$00
	.byte	$10
	.byte	$80
	.byte	$02
	.byte	$00
	.byte	$20
	.byte	$00
	.byte	$04
	.byte	$00
	.byte	$10
	.byte	$80
	.byte	$02
	.byte	$0F
	.byte	$01
	.byte	$14
	.byte	$15
	.byte	$A8
	.byte	$68
; ------------------------------------------------------------
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
; ------------------------------------------------------------
L61EE:
        ldy     ppu_displist_offset
        lda     #2-1
        sta     zp_byte_02
	: ; for (i = 2-1; i >= 0; i--) {
		ldx     zp_byte_02
	        lda     $10
	        and     $BE65, x
	        bne     :+++ ; if () {
		        lda     $BE67, x
		        sta     ppu_displist_addr_dbyt, y
		        iny
		        lda     $BE69, x
		        sta     ppu_displist_addr_dbyt, y
		        iny
		        lda     #8
		        sta     ppu_displist_count-2, y
		        sta     zp_byte_03
		        dec     tbl_625, x
		        bpl     :+ ; if (--tbl_625[i] < 0) {
			        lda     #$07
			        sta     tbl_625, x
			: ; }
		
			lda     tbl_625, x
		        tax
		        iny
			: ; for (j = 8, x = tbl_625[i], y = ppu_displist_offset; j > 0; j--, x++, y++) {
				lda     eng_map_ppu_buffer_write_str_a, x
			        sta     ppu_displist_data, y

			        inx
			        iny
			        dec     zp_byte_03
			        bne     :-
			; }
		: ; }
	
		dec     zp_byte_02
	        bpl     :----
	; }

        lda     #$00
        sta     ppu_displist_data, y
        sty     ppu_displist_offset
        rts
; ------------------------------------------------------------
L623B:
        adc     $7271
        ldy     eng_map_bg_tile_off
        lda     byte_50E
        sec
        sbc     #$06
        tax
        lda     $BEC9, x
        sta     (zp_addr_01), y
	: ; for (y = eng_map_bg_tile_off; zp_addr_01[y] == 0x40; y = eng_map_area_increment_v()) {
		jsr     eng_map_area_increment_v
	        lda     (zp_addr_01), y
	        cmp     #$40
	        bne     :+ ; if (zp_addr_01[y] == 0x40) {
		        lda     #$6E
	        	sta     (zp_addr_01), y
	        	bne     :-
		; } else break;
	: ; }

	tya
        sec
        sbc     #$10
        tay
        lda     #$6F
        sta     (zp_addr_01), y
        rts
; ------------------------------------------------------------
L6265:
        ldy     eng_map_bg_tile_off
        lda     #$01
        sta     (zp_addr_01), y
        iny
        lda     #$02
        sta     (zp_addr_01), y
        rts
; ------------------------------------------------------------
L6271:
        brk
        ora     ($11), y
        .byte   $BB
        .byte   $EF
        dec     $00
        brk
        brk
        ora     ($11), y
        .byte   $BB
        .byte   $EF
        dec     $00
        .byte   $FF
        inc     $44EE
        brk
        bpl     L62C0
        .byte   $FF
        .byte   $FF
        inc     $44EE
        brk
        bpl     L62C8
        rti

        .byte   $02
        php
        and     ($02, x)
        rti

        and     ($08, x)
        ldx     ppu_displist_offset
        lda     #$1B
        sta     $0301, x
        sta     $0306, x
        lda     $10
        asl     a
        and     #$08
        ora     #$E0
        sta     $0302, x
        eor     #$08
        sta     $0307, x
        lda     #$02
        sta     $0303, x
        sta     $0308, x
        lda     $10
        lsr     a
        lsr     a
        lsr     a
        and     #$03
L62C0:  tay
        lda     $BE74, y
        sta     $0304, x
        .byte   $B9
L62C8:  adc     $BE, x
        sta     $0305, x
        lda     $10
        eor     #$FF
        asl     a
        and     #$07
        tay
        sty     $03
        lda     #$1B
        sta     $030B, x
        lda     #$F0
        sta     $030C, x
        lda     #$10
        sta     $030D, x
        lda     #$00
        sta     $031E, x
        sta     $0309, x
        sta     $030A, x
        txa
        clc
        adc     #$1D
        sta     ppu_displist_offset
        lda     #$07
        sta     $02
	L62FC:
		lda     eng_map_ppu_buffer_write_str_a, y
	        sta     $030E, x
	        inx
	        iny
	        dec     $02
	        bpl     L62FC

        ldy     $03
        lda     #$07
        sta     $02
	L630E:
		lda     $BE65, y
	        sta     $030E, x
	        inx
	        iny
	        dec     $02
	        bpl     L630E

        rts
; ------------------------------------------------------------
L631B:
        asl     a
        clc
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        rts
; -----------------------------
L6342:
        lda     $050D
        sta     $07
        lda     #$0C
        sta     $050E
        jsr     sub_C950
	; for () {
		: ; if () {
			inc     $050E
		: ; }

		lda     $07
	        sta     $050D
	        lda     $E7
	        clc
	        adc     #$10
	        sta     $E7
	        ldx     $E8
	        jsr     eng_map_bg_decoded_off_get
	        jsr     sub_C950
	        tya
	        and     #$F0
	        cmp     #$B0
	        bne     :+ ; if () {
		        jsr     eng_map_area_increment_h
		        jsr     eng_map_area_increment_h
		        lda     #$62
		        sta     (map_bg_ptr), y
		        clc
		        bcc     :--
		: ; } else {
			lda     $050E
		        cmp     #$0E
		        bne     :--
		; }
	; }

        jsr     eng_map_area_increment_h
        jsr     eng_map_area_increment_h
        lda     #$0B
        sta     ($01), y
        rts
; -----------------------------
L638C:
        lda     #$01
        sta     $08
	: ; for () {
		ldy     $E7
	        ldx     $E8
	        jsr     eng_map_bg_decoded_off_get
	        ldy     $E7
	        lda     $050D
	        sta     $07
	        ldx     $08
		: ; for () {
			lda     eng_map_ppu_buffer_write_str_a, x
		        sta     ($01), y
		        jsr     eng_map_area_increment_h
		        dec     $07
		        bpl     :-
		; }
	
	        lda     $08
	        bne     :+ ; if () {
		        rts
		: ; } else {
			lda     $E7
		        clc
		        adc     #$10
		        cmp     #$C0
		        bcc     :+ ; if () {
			        dec     $08
			: ; }
			sta     $E7
		
		        clc
		        bcc     :----
		; }
	; }
; ------------------------------------------------------------
L63C1:
        .byte   $DF
        .byte   $EF
        .byte   $F7
        .byte   $FB
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $AF
        .byte   $D7
        .byte   $EB
        sbc     $FB, x
        .byte   $F7
        .byte   $EF
        .byte   $DF
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        sbc     $EB, x
        .byte   $D7
        .byte   $AF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
; ------------------------------------------------------------
        ldy     ppu_displist_offset
        lda     #$13
        sta     $0301, y
        lda     #$13
        sta     $0310, y
        lda     #$02
        sta     $0302, y
        lda     #$12
        sta     $0311, y
        lda     #$0C
        sta     $0303, y
        sta     $0312, y
        tax
	L6407:
		lda     $BE55, x
	        asl     $BE55, x
	        rol     a
	        sta     $BE55, x
	        sta     $0304, y
	        lda     $BE61, x
	        lsr     $BE61, x
	        ror     a
	        sta     $BE61, x
	        sta     $0313, y
	        iny
	        dex
	        bne     L6407

        txa
        sta     $0313, y
        tya
        clc
        adc     #$12
        sta     ppu_displist_offset
        rts
; -----------------------------
L6431:
        ldy     eng_map_bg_tile_off
        lda     #$A5
        sta     (zp_addr_01), y
	L6437:
		jsr     eng_map_area_increment_v
	        lda     (zp_addr_01), y
	        cmp     #$40
	        bne     L6446
	        lda     #$A6
	        sta     (zp_addr_01), y
	        bne     L6437
	L6446:

	rts
; -----------------------------
L6447:
        ldy     eng_map_bg_tile_off
        lda     #$8C
        sta     (zp_addr_01), y
        iny
        lda     #$8D
        sta     (zp_addr_01), y
        lda     eng_map_bg_tile_off
        clc
        adc     #$10
        tay
        lda     #$8E
        sta     (zp_addr_01), y
        iny
        lda     #$8F
        sta     (zp_addr_01), y
        rts
; ------------------------------------------------------------
L6462:
	.byte   $3F
        brk
        jsr     $3038
        .byte   $1A
        .byte   $0F
        sec
        sec
        .byte   $0F
        .byte   $0F
        sec
        .byte   $17
        .byte   $17
        sec
        sec
        plp
        clc
        php
        sec
        ora     ($30, x)
        .byte   $27
        sec
        ora     ($12, x)
        rol     $38, x
        asl     $25
        rol     $38, x
        asl     $37
        .byte   $27
        brk
        and     ($4F, x)
        .byte   $03
        nop
        .byte   $FB
        cmp     ($21), y
        .byte   $AB
        asl     a
        .byte   $F3
        .byte   $F3
        .byte   $FB
        .byte   $FB
        .byte   $F3
        .byte   $F3
        .byte   $FB
        .byte   $FB
        .byte   $F3
        .byte   $F3
        .byte   $22
        .byte   $0B
        asl     a
        .byte   $F3
        .byte   $F3
        .byte   $FB
        .byte   $FB
        .byte   $F3
        .byte   $F3
        .byte   $FB
        .byte   $FB
        .byte   $F3
        .byte   $F3
        .byte   $23
        .byte   $57
        .byte   $03
        nop
        .byte   $FB
        cmp     ($23), y
        cmp     #$02
        .byte   $80
        jsr     $CD23
        .byte   $02
        .byte   $80
        jsr     $EA23
        .byte   $44
        bvc     L64BB
L64BB:  .byte   $1A
        bmi     L64EE
        .byte   $1A
        .byte   $1A
        and     ($21, x)
        .byte   $1A
        .byte   $1A
        bmi     L64ED
        .byte   $0F
        .byte   $1A
        bmi     L64E0
        .byte   $0F
; ------------------------------------------------------------
L64CB:
	clc
        brk
        php
        bpl     L64D0
L64D0:  php
        brk
        bpl     L64DC
        clc
        brk
        clc
        bpl     L64E1
        clc
        bpl     L64E4
L64DC:  clc
        php
        bpl     L64E8
L64E0:  brk
L64E1:  bpl     L64FB
        .byte   $4B
L64E4:  tya
        ora     ($60, x)
        .byte   $4B
L64E8:  tya
        eor     ($68, x)
        .byte   $CB
        .byte   $B0
L64ED:  brk
L64EE:  ldy     #$CB
        bcs     L6532
        tay
        .byte   $CB
        .byte   $B2
        brk
        ldy     #$CB
        .byte   $B2
        rti

        tay
L64FB:  .byte   $CB
        ldx     $00, y
        ldy     #$CB
        ldx     $40, y
        tay
        .byte   $CB
        ldy     $00, x
        ldy     #$CB
        ldy     $40, x
        tay
        jsr     $14C6
        .byte   $F3
        .byte   $F3
        .byte   $F3
        .byte   $FB
        .byte   $DB
        inx
        .byte   $E7
        inc     $FBEC
        .byte   $DC
        sbc     ($DA, x)
        .byte   $E7
        .byte   $DC
        dec     $F3FB, x
        .byte   $F3
        .byte   $F3
        brk
        jsr     $14C6
        .byte   $F3
        .byte   $F3
        .byte   $F3
        .byte   $FB
        .byte   $FB
        .byte   $FB
        .byte   $E7
        inx
        .byte   $FB
        .byte   $DB
        inx
        .byte   $E7
L6532:  inc     $FBEC
        .byte   $FB
        .byte   $FB
        .byte   $F3
        .byte   $F3
        .byte   $F3
        brk
        .byte   $22
        tax
        ora     $EEE9
        cpx     $FBE1
        .byte   $DA
        .byte   $FB
        .byte   $DB
        inc     $EDED
        inx
        .byte   $E7
        brk
        .byte   $22
        .byte   $EB
        .byte   $0B
        sbc     #$E5
        .byte   $DA
        .byte   $F2
        dec     $FBEB, x
        .byte   $FB
        cmp     ($EE), y
        sbc     #$00
        and     $E9
        ora     $E9
        .byte   $DA
        inc     $DEEC
        .byte   $27
        .byte   $DA
        .byte   $02
        tax
        tax
        brk
        jsr     $54C6
        .byte   $FB
        brk
        .byte   $22
        tax
        eor     a:$FB
        .byte   $22
        .byte   $EB
        .byte   $4B
        .byte   $FB
        brk
        and     $E9
        ora     $FB
        .byte   $FB
        .byte   $FB
        .byte   $FB
        .byte   $FB
; ------------------------------------------------------------
        brk
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $F7
        .byte   $E7
        .byte   $E7
        sbc     ($E3), y
        .byte   $07
        .byte   $1F
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $E7
        .byte   $F7
        .byte   $FB
        .byte   $FB
        sbc     $7DFD, x
        and     $FFFF, x
        .byte   $FF
        .byte   $7F
        .byte   $3F
        .byte   $9F
        .byte   $4F
        .byte   $27
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $1F
        .byte   $1F
        .byte   $3F
        .byte   $3F
        .byte   $7F
        .byte   $FF
        inc     $171C, x
        .byte   $17
        .byte   $27
        .byte   $2F
        .byte   $4F
        .byte   $9F
        inc     $1F1C, x
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $1F
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        .byte   $17
        brk
        brk
        brk
        brk
        brk
        brk
        brk
        brk
        clc
        .byte   $1A
        ror     $1858, x
        .byte   $1C
        .byte   $74
        .byte   $04
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        brk
        brk
        brk
        brk
        brk
        brk
        brk
