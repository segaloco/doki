	.byte	$81

	.export eng_hitboxes
eng_hitboxes:
	.byte	$00, $08, $10, $18, $20, $24

	.export eng_itemdir_hitboxes
eng_itemdir_hitboxes:
	.byte	$28, $2A, $29, $2B

	.export eng_other_hitboxes
eng_other_hitboxes:
	.byte	$2C, $2D, $2F, $33, $37, $3B, $3F

	.export eng_tile_cols_x, eng_tile_cols_y
eng_tile_cols_x:
	.incbin	"data/tile_cols_x.bin"

eng_tile_cols_y:
	.incbin	"data/tile_cols_y.bin"

	.export eng_hitboxes_left, eng_hitboxes_top
	.export eng_hitboxes_width, eng_hitboxes_height
eng_hitboxes_left:
	.byte   $02
	.byte   $02
	.byte   $03
	.byte	$00
	.byte   $03
	.byte   $03
	.byte	<(-$08)
	.byte	$00
	.byte   $03
	.byte	$01
	.byte	<(-$0D)
	.byte   $04
	.byte   $03
	.byte   $03
	.byte   $03
	.byte   <(-$0E)
	.byte   $03
	.byte   $03
	.byte   $05

eng_hitboxes_top:
	.byte   $0B
	.byte	$10
	.byte	$03
	.byte	$00
	.byte   $03
	.byte   $03
	.byte	<(-$08)
	.byte	$00
	.byte	$09
	.byte	$04
	.byte   $03
	.byte   $03
	.byte	$0E
	.byte   $03
	.byte   $03
	.byte   $03
	.byte	<(-$0A)
	.byte	$0C
	.byte   $02

eng_hitboxes_width:
	.byte   $0B
	.byte   $0B
	.byte	$09
	.byte	$10
	.byte	$09
	.byte	$19
	.byte	$20
	.byte	$20
	.byte	$03
	.byte	$1E
	.byte	$19
	.byte	$08
	.byte	$09
	.byte	$09
	.byte	$09
	.byte	$18
	.byte	$09
	.byte	$1A
	.byte   $06

eng_hitboxes_height:
	.byte	$16
	.byte	$11
	.byte	$0D
	.byte	$10
	.byte	$1A
	.byte	$19
	.byte	$24
	.byte	$10
	.byte   $03
	.byte   $04
	.byte	$2D
	.byte	$2C
	.byte	$0F
	.byte	$2E
	.byte	$3E
	.byte	$1E
	.byte	$28
	.byte   $13
	.byte	$48
