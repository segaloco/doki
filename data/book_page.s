.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"charmap.i"
.include	"tunables.i"

	.export nmi_buffer_mode_tbl
nmi_buffer_mode_tbl:
	.addr	ppu_displist_data
	.addr	tbl_583
	.addr	tbl_55F
	.addr	nmi_buffer_char_select
	.addr	nmi_buffer_game_over
	.addr	nmi_buffer_continue_save
	.addr	nmi_buffer_chapter_page
	.addr	tbl_67B
	.addr	tbl_693
	.addr	tbl_6AB
	.addr	tbl_6BC
	.addr	tbl_6CB
	.addr	tbl_6E8
	.addr	tbl_6D9
	.addr	tbl_6DE
	.addr	tbl_6E3
	.addr	disk_text_buffer
	.addr	nmi_buffer_warp
	.addr	tbl_7CB0
	.addr	nmi_buffer_text_side_a
	.addr	eng_smokepoof_ani_0
; -----------------------------
nmi_buffer_char_select:
	.dbyt	$2080
	.byte	$D6, $FE

	.dbyt	$2081
	.byte	$D6, $FE

	.dbyt	$2498
	.byte	$D6, $FE

	.dbyt	$2499
	.byte	$D6, $FE

	.dbyt	$249A
	.byte	$D6, $FE

	.dbyt	$249B
	.byte	$D6, $FE

	.dbyt	$249C
	.byte	$D6, $FE

	.dbyt	$20A2
	.byte	$D4, "1"

	.dbyt	$20BC
	.byte	$D4, "B"

	.dbyt	$24B7
	.byte	$D4, "7"

	.dbyt	$2323
	.byte	$5D, "3"

	.dbyt	$2720
	.byte	$57, "3"

	.dbyt	$2082
	.byte	(:++)-(:+)
	: .byte	"0"
	:

	.dbyt	$2322
	.byte	(:++)-(:+)
	: .byte	"2"
	:

	.dbyt	$2497
	.byte	(:++)-(:+)
	: .byte	"8"
	:

	.dbyt	$2737
	.byte	(:++)-(:+)
	: .byte	"6"
	:

	.dbyt	$209C
	.byte	(:++)-(:+)
	: .byte	"9A"
	:

	.dbyt	$233C
	.byte	(:++)-(:+)
	: .byte	"45"
	:

	.dbyt	$20C9
	.byte	(:++)-(:+)
	: .byte	"PUSH START - NEXT" 
	:

	.dbyt	$2124
	.byte	(:++)-(:+)
	: .byte	"CHAPTER"
	:

	.dbyt	$2148
	.byte	(:++)-(:+)
	: .byte	"1  2  3  4  5  6  7"
	:

	.dbyt	$22EC
	.byte	(:++)-(:+)
	: .byte	"PLAYER CHANGE ?"
	:

	.dbyt	$23C8
	.byte	(:++)-(:+)
	: .byte	$44
	:

	.dbyt	$23CF
	.byte	(:++)-(:+)
	: .byte	$11, $44
	:

	.byte	$23
	.byte	$D7
	.byte	$03
	.byte	$11
	.byte	$44
	.byte	$22

	.byte	$23
	.byte	$DF
	.byte	$03
	.byte	$11
	.byte	$44
	.byte	$22

	.byte	$23
	.byte	$E7
	.byte	$03
	.byte	$11
	.byte	$44
	.byte	$22

	.byte	$23
	.byte	$EF
	.byte	$02
	.byte	$11
	.byte	$44

	.byte	$23
	.byte	$F0
	.byte	$48
	.byte	$05

	.byte	$23
	.byte	$CA
	.byte	$45
	.byte	$A0

	.byte	$23
	.byte	$D1
	.byte	$46
	.byte	$AA

	.byte	$23
	.byte	$EA
	.byte	$45
	.byte	$A0

	.byte	$27
	.byte	$CD
	.byte	$01
	.byte	$44

	.byte	$27
	.byte	$D5
	.byte	$01
	.byte	$44

	.byte	$27
	.byte	$DD
	.byte	$01
	.byte	$44

	.byte	$27
	.byte	$E5
	.byte	$01
	.byte	$44

	.byte	$27
	.byte	$F0
	.byte	$46
	.byte	$05

	.byte	$27
	.byte	$C9
	.byte	$04
	.byte	$80
	.byte	$A0
	.byte	$A0
	.byte	$20

	.byte	$27
	.byte	$D2
	.byte	$02
	.byte	$AF
	.byte	$AF

	.byte	$27
	.byte	$D8
	.byte	$45
	.byte	$FF

	.byte	$27
	.byte	$E0
	.byte	$45
	.byte	$FF

	.byte	$27
	.byte	$E8
	.byte	$45
	.byte	$FF

	.byte	$27
	.byte	$EB
	.byte	$03
	.byte	$AF
	.byte	$AF
	.byte	$64
	.byte	NMI_LIST_END
; -----------------------------
	.export	book_page_subchapter_icons
	.export	book_page_subchapter_icons_end
	.export	book_page_chapter_text_start
	.export	book_page_lives_text_start
	.export	book_page_number_start
nmi_buffer_chapter_page:
	.dbyt	$2509
	.byte	(:++)-(:+)
book_page_subchapter_icons:
	: .byte	"       "
	:
book_page_subchapter_icons_end:

	.dbyt	PPU_VRAM_BG2+BOOK_PAGE_BG_CHAPTER
	.byte	(:++)-(:+)
book_page_chapter_text_start:
	: .byte	"CHAPTER 1-1"
	:

	.dbyt	PPU_VRAM_BG2+BOOK_PAGE_BG_LIVES
	.byte	(:++)-(:+)
book_page_lives_text_start:
	: .byte	"x 0"
	:

	.dbyt	PPU_VRAM_BG2+BOOK_PAGE_BG_PAGENO
	.byte	(:++)-(:+)
book_page_number_start:
	: .byte	" 1"
	:

	.byte	NMI_LIST_END
; -----------------------------
book_charsel_BG_IMAJIN		= $187
book_charsel_BG_LINA		= $1E7
book_charsel_BG_MAMA		= $247
book_charsel_BG_PAPA		= $2A7

	.export tbl_chapterstarts
tbl_chapterstarts:
	.byte	$00
	.byte   $03
	.byte	$06
	.byte	$09
	.byte   $0C
	.byte   $0F
	.byte   $12
	.byte   $14
tbl_chapterstarts_end:
	.assert (tbl_chapterstarts_end-tbl_chapterstarts) = (ENG_CHAPTER_COUNT + 1), error, "tbl_chapterstarts chapter count mismatch"

	.export eng_charsel_oam, eng_charsel_oam_end
eng_charsel_oam:
	.byte   ENG_CHARSEL_IMAJIN_POS_V, $B0, (obj_attr::priority_high|obj_attr::color0), ENG_CHARSEL_HEADLEFT_POS_H
	.byte	ENG_CHARSEL_IMAJIN_POS_V, $B0, (obj_attr::h_flip|obj_attr::priority_high|obj_attr::color0), ENG_CHARSEL_HEADRIGHT_POS_H
	.byte   ENG_CHARSEL_LINA_POS_V, $B2, (obj_attr::priority_high|obj_attr::color2), ENG_CHARSEL_HEADLEFT_POS_H
	.byte	ENG_CHARSEL_LINA_POS_V, $B2, (obj_attr::h_flip|obj_attr::priority_high|obj_attr::color2), ENG_CHARSEL_HEADRIGHT_POS_H
	.byte   ENG_CHARSEL_MAMA_POS_V, $B6, (obj_attr::priority_high|obj_attr::color1), ENG_CHARSEL_HEADLEFT_POS_H
	.byte	ENG_CHARSEL_MAMA_POS_V, $B6, (obj_attr::h_flip|obj_attr::priority_high|obj_attr::color1), ENG_CHARSEL_HEADRIGHT_POS_H
	.byte   ENG_CHARSEL_PAPA_POS_V, $B4, (obj_attr::priority_high|obj_attr::color3), ENG_CHARSEL_HEADLEFT_POS_H
	.byte	ENG_CHARSEL_PAPA_POS_V, $B4, (obj_attr::h_flip|obj_attr::priority_high|obj_attr::color3), ENG_CHARSEL_HEADRIGHT_POS_H
eng_charsel_oam_end:

book_charsel_ADDR_0	= PPU_VRAM_BG1+book_charsel_BG_IMAJIN
book_charsel_ADDR_1	= PPU_VRAM_BG1+book_charsel_BG_LINA
book_charsel_ADDR_2	= PPU_VRAM_BG1+book_charsel_BG_MAMA
book_charsel_ADDR_3	= PPU_VRAM_BG1+book_charsel_BG_PAPA

	.export book_charsel_dest_h, book_charsel_dest_l, book_save_bits
book_charsel_dest_h:
	.byte	>(book_charsel_ADDR_0)
	.byte	>(book_charsel_ADDR_1)
	.byte   >(book_charsel_ADDR_2)
	.byte   >(book_charsel_ADDR_3)
book_charsel_dest_h_end:
	.assert	(book_charsel_dest_h_end-book_charsel_dest_h) = ENG_CHARACTERS, error, "book_charsel_dest_h character count mismatch"

	.export book_charsel_dest_l
book_charsel_dest_l:
	.byte	<(book_charsel_ADDR_0)
	.byte	<(book_charsel_ADDR_1)
	.byte   <(book_charsel_ADDR_2)
	.byte   <(book_charsel_ADDR_3)
book_charsel_dest_l_end:
	.assert	(book_charsel_dest_l_end-book_charsel_dest_l) = ENG_CHARACTERS, error, "book_charsel_dest_l character count mismatch"

book_save_bits:
	.byte	$01, $02, $04, $08
	.byte	$10, $20, $40, $80
book_save_bits_end:
	.assert (book_save_bits_end-book_save_bits) = (ENG_CHAPTER_COUNT + 1), error, "book_save_bits chapter count mismatch"

	.export eng_charsel_row_pos_v
eng_charsel_row_pos_v:
	.byte   ENG_CHARSEL_IMAJIN_POS_V, ENG_CHARSEL_LINA_POS_V, ENG_CHARSEL_MAMA_POS_V, ENG_CHARSEL_PAPA_POS_V
eng_charsel_row_pos_v_end:
	.assert	(eng_charsel_row_pos_v_end-eng_charsel_row_pos_v) = ENG_CHARACTERS, error, "eng_charsel_row_pos_v character count mismatch"

	.export oam_73FD, oam_73FD_end
oam_73FD:
	.byte	OAM_73FD_POS_V, $A1, (obj_attr::priority_high|obj_attr::color1), OAM_73FD_LEFT_POS_H
	.byte	OAM_73FD_POS_V, $A3, (obj_attr::priority_high|obj_attr::color1), OAM_73FD_RIGHT_POS_H
	.byte	OAM_73FD_POS_V, $FC, (obj_attr::priority_high|obj_attr::color1), OAM_73FD_LEFT_POS_H
	.byte	OAM_73FD_POS_V, $FC, (obj_attr::h_flip|obj_attr::priority_high|obj_attr::color1), OAM_73FD_RIGHT_POS_H
	.byte	OAM_73FD_POS_V, $FE, (obj_attr::priority_high|obj_attr::color1), OAM_73FD_LEFT_POS_H
	.byte	OAM_73FD_POS_V, $FE, (obj_attr::h_flip|obj_attr::priority_high|obj_attr::color1), OAM_73FD_RIGHT_POS_H
	.byte   OAM_73FD_POS_V, $70, (obj_attr::priority_high|obj_attr::color1), OAM_73FD_LEFT_POS_H
	.byte	OAM_73FD_POS_V, $72, (obj_attr::priority_high|obj_attr::color1), OAM_73FD_RIGHT_POS_H
oam_73FD_end:
