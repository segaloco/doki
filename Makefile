#
# Yume Kojo Doki Doki Panic for the Famicom Disk System
#
AS		=	ca65
ASFLAGS		=	-DCONS -U -I ./inc
LD		=	ld65
LDFLAGS		=	-C link.ld

CHECK		=	tools/check.sh
BINTOFDF	=	fdtc/bintofdf
FDTC		=	fdtc/fdtc

CKSUM_BG_CHR_1  =       2005116339
CKSUM_BG_CHR_2  =       47972404
CKSUM_BG_CHR_3  =       4059705679
CKSUM_BG_CHR_4  =       817772681
CKSUM_BG_CHR_C  =       821221512
CKSUM_CHAPT_01  =       241072252
CKSUM_CHAPT_02  =       2840514088
CKSUM_CHAPT_03  =       3609649626
CKSUM_CHAPT_04  =       2442764772
CKSUM_CHAPT_05  =       3828567520
CKSUM_CHAPT_06  =       1082563587
CKSUM_CHAPT_07  =       2218997506
CKSUM_DUMMY_DT  =       3065931776
CKSUM_ENDING_1  =       353481282
CKSUM_ENDING_2  =       1203226223
CKSUM_ENDING_C  =       1324829805
CKSUM_EN_SND_D  =       1572285169
CKSUM_IMAJIN_C  =       2328979548
CKSUM_LINA_C    =       3759642619
CKSUM_MAIN_PRO  =       791380428
CKSUM_MAMA_C    =       2339929742
CKSUM_PAPA_C    =       100716177
CKSUM_SAVE_DAT  =       3065931776
CKSUM_SOUND_DT  =       2933143640
CKSUM_TEMP_PRG  =       409918156
CKSUM_TITLE_C   =       1767627079

FDS		= 	dokidoki.fds

BIN_KYODAKU	=	kyodaku_.bin

OBJS_KYODAKU	=	data/security.o

BIN_TITLE_C	=	title_c.bin

OBJS_TITLE_C	=	data/ppu/title_c.o

BIN_MAIN_PRO	=	main_pro.bin

OBJS_BASE	=	data/unk/main_pro_unk00.o \
			src/lib/sdisk.o \
			src/lib/scroll.o \
			data/book_page.o \
			src/lib/tbljmp.o \
			src/lib/misc.o \
			src/start.o \
			src/game/core.o \
			src/int.o \
			src/lib/disk.o \
			src/lib/ppu_base.o

OBJS_PLAYER	=	data/player_col_dat.o \
			src/game/framecycle.o \
			src/game/area_sec_routines.o \
			src/obj/player/state_base.o \
			src/obj/player/kill.o \
			src/obj/player/state.o \
			src/obj/player/state_ext.o \
			src/lib/calcpage.o \
			src/obj/player/tile_checks.o \
			src/obj/player/collision_calc.o \
			src/lib/calcpage_a_y.o \
			src/lib/calcpage_y.o \
			src/obj/player/collision_bounds.o \
			src/game/camera_veloc_x.o \
			src/obj/player/transition.o

OBJS_ACTOR_BASE	=	src/obj/actor/core.o \
			src/obj/actor/base_init.o \
			src/obj/actor/beezo_init.o \
			src/obj/actor/phanto_init.o \
			src/obj/actor/bobomb_init.o \
			src/obj/actor/enemy_state.o \
			src/obj/actor/enemy_state_ext.o \
			src/obj/actor/spawnjar_init.o \
			src/obj/actor/sparks.o \
			src/obj/actor/ani_timer_inc.o \
			src/obj/actor/swarms.o \
			src/obj/actor/fireball.o \
			src/obj/actor/panser.o \
			src/obj/actor/key_misc_init.o \
			src/obj/actor/starman.o \
			src/obj/actor/spawnjar_routine.o \
			src/obj/actor/doormask.o \
			src/obj/actor/trouter.o \
			src/obj/actor/hoopstar.o \
			src/obj/actor/heart.o \
			src/obj/actor/birdo.o \
			src/obj/actor/items.o \
			src/obj/actor/albatross.o \
			src/obj/actor/projectile.o \
			src/obj/actor/shyguy_prj.o \
			src/obj/actor/phanto_routine.o \
			src/obj/actor/ninji.o \
			src/obj/actor/beezo_routine.o \
			src/obj/actor/base_bobomb_routine.o \
			src/obj/actor/bullet.o \
			src/obj/actor/throw.o \
			src/obj/actor/life_pow_door_subsp.o \
			src/obj/actor/sprite_alloc.o \
			src/obj/actor/bighead.o \
			src/obj/actor/waterfall_log_misc.o \
			src/obj/actor/clear.o \
			src/obj/actor/damage.o

OBJS_ACTOR_RENDER =	src/obj/actor/render_pos.o \
			src/obj/actor/birdo_render.o \
			src/obj/actor/base_render.o \
			src/obj/actor/porcupo_render.o

OBJS_ACTOR_PHYSICS =	src/obj/actor/physics.o

OBJS_MAP	=	src/game/map.o

OBJS_TITLE	=	src/title/displist.o \
			data/title/displist.o \
			data/title/palette.o \
			data/title/cycle_attrs.o \
			data/title/displist_window.o \
			data/title/palette_wait.o \
			src/title/proc_tbl.o \
			src/start_b.o \
			src/title/proc.o \
			src/title/actor.o

OBJS_DISK_SAVE	=	data/title/displist_wait.o \
			src/lib/disk_save.o

OBJS_APU	=	src/apu/alt.o \
			src/apu/apu.o

OBJS_MAIN_PRO	=	$(OBJS_BASE) \
			$(OBJS_PLAYER) \
			$(OBJS_ACTOR_BASE) \
			$(OBJS_ACTOR_RENDER) \
			$(OBJS_ACTOR_PHYSICS) \
			$(OBJS_MAP) \
			$(OBJS_TITLE) \
			$(OBJS_DISK_SAVE) \
			$(OBJS_APU)

BIN_SAVE_DAT	=	save_dat.bin

OBJS_SAVE_DAT	=	data/save_dat.o

BIN_IMAJIN_C	=	imajin_c.bin

OBJS_IMAJIN_C	=	data/ppu/imajin_c.o

BIN_LINA_C	=	lina_c.bin

OBJS_LINA_C	=	data/ppu/lina_c.o

BIN_MAMA_C	=	mama_c.bin

OBJS_MAMA_C	=	data/ppu/mama_c.o

BIN_PAPA_C	=	papa_c.bin

OBJS_PAPA_C	=	data/ppu/papa_c.o

BIN_BG_CHR_1	=	bg_chr_1.bin

OBJS_BG_CHR_1	=	data/ppu/bg_chr_1.o

BIN_BG_CHR_2	=	bg_chr_2.bin

OBJS_BG_CHR_2	=	data/ppu/bg_chr_2.o

BIN_BG_CHR_3	=	bg_chr_3.bin

OBJS_BG_CHR_3	=	data/ppu/bg_chr_3.o

BIN_BG_CHR_4	=	bg_chr_4.bin

OBJS_BG_CHR_4	=	data/ppu/bg_chr_4.o

BIN_BG_CHR_C	=	bg_chr_c.bin

OBJS_BG_CHR_C	=	data/ppu/bg_chr_c.o

BIN_CHAPT_01	=	chapt_01.bin

OBJS_CHAPT_01	=	src/chapters/chapt_01.o

BIN_CHAPT_02	=	chapt_02.bin

OBJS_CHAPT_02	=	src/chapters/chapt_02.o

BIN_CHAPT_03	=	chapt_03.bin

OBJS_CHAPT_03	=	src/chapters/chapt_03.o

BIN_CHAPT_04	=	chapt_04.bin

OBJS_CHAPT_04	=	src/chapters/chapt_04.o

BIN_CHAPT_05	=	chapt_05.bin

OBJS_CHAPT_05	=	src/chapters/chapt_05.o

BIN_CHAPT_06	=	chapt_06.bin

OBJS_CHAPT_06	=	src/chapters/chapt_06.o

BIN_CHAPT_07	=	chapt_07.bin

OBJS_CHAPT_07	=	src/chapters/chapt_07.o

BIN_ENDING_1	=	ending_1.bin

OBJS_ENDING_1	=	src/ending/ending_1.o

BIN_TEMP_PRG	=	temp_prg.bin

OBJS_TEMP_PRG	=	src/temp_prg.o \
			data/unk/temp_prg.o

BIN_SOUND_DT	=	sound_dt.bin

OBJS_SOUND_DT	=	data/unk/sound_dt.o

BIN_DUMMY_DT	=	dummy_dt.bin

OBJS_DUMMY_DT	=	data/dummy_dt.o

BIN_ENDING_C	=	ending_c.bin

OBJS_ENDING_C	=	data/ppu/ending_c.o

BIN_EN_SND_D	=	en_snd_d.bin

OBJS_EN_SND_D	=	data/unk/en_snd_d.o

BIN_ENDING_2	=	ending_2.bin

OBJS_ENDING_2	=	src/ending/ending_2.o

OBJS	=	$(OBJS_KYODAKU) \
		$(OBJS_TITLE_C) \
		$(OBJS_SAVE_DAT) \
		$(OBJS_MAIN_PRO) \
		$(OBJS_IMAJIN_C) \
		$(OBJS_LINA_C) \
		$(OBJS_MAMA_C) \
		$(OBJS_PAPA_C) \
		$(OBJS_BG_CHR_1) \
		$(OBJS_BG_CHR_2) \
		$(OBJS_BG_CHR_3) \
		$(OBJS_BG_CHR_4) \
		$(OBJS_BG_CHR_C) \
		$(OBJS_CHAPT_01) \
		$(OBJS_CHAPT_02) \
		$(OBJS_CHAPT_03) \
		$(OBJS_CHAPT_04) \
		$(OBJS_CHAPT_05) \
		$(OBJS_CHAPT_06) \
		$(OBJS_CHAPT_07) \
		$(OBJS_ENDING_1) \
		$(OBJS_TEMP_PRG) \
		$(OBJS_SOUND_DT) \
		$(OBJS_DUMMY_DT) \
		$(OBJS_ENDING_C) \
		$(OBJS_EN_SND_D) \
		$(OBJS_ENDING_2)	

FILES	=	$(BIN_KYODAKU) \
		$(BIN_TITLE_C) \
		$(BIN_MAIN_PRO) \
		$(BIN_SAVE_DAT) \
		$(BIN_IMAJIN_C) \
		$(BIN_LINA_C) \
		$(BIN_MAMA_C) \
		$(BIN_PAPA_C) \
		$(BIN_BG_CHR_1) \
		$(BIN_BG_CHR_2) \
		$(BIN_BG_CHR_3) \
		$(BIN_BG_CHR_4) \
		$(BIN_BG_CHR_C) \
		$(BIN_CHAPT_01) \
		$(BIN_CHAPT_02) \
		$(BIN_CHAPT_03) \
		$(BIN_CHAPT_04) \
		$(BIN_CHAPT_05) \
		$(BIN_CHAPT_06) \
		$(BIN_CHAPT_07) \
		$(BIN_ENDING_1) \
		$(BIN_TEMP_PRG) \
		$(BIN_SOUND_DT) \
		$(BIN_DUMMY_DT) \
		$(BIN_ENDING_C) \
		$(BIN_EN_SND_D) \
		$(BIN_ENDING_2)

SIDEA_FDFS	=	kyodaku_.fdf \
			en_snd_d.fdf \
			main_pro.fdf \
			ending_2.fdf \
			title_c.fdf \
			ending_c.fdf \
			save_dat.fdf

SIDEA_DEF	=	data/fs/superblock/sidea.def

SIDEB_FDFS	=	temp_prg.fdf \
			chapt_01.fdf \
			chapt_02.fdf \
			chapt_03.fdf \
			chapt_04.fdf \
			chapt_05.fdf \
			chapt_06.fdf \
			chapt_07.fdf \
			ending_1.fdf \
			sound_dt.fdf \
			imajin_c.fdf \
			lina_c.fdf \
			mama_c.fdf \
			papa_c.fdf \
			bg_chr_c.fdf \
			bg_chr_1.fdf \
			bg_chr_2.fdf \
			bg_chr_3.fdf \
			bg_chr_4.fdf \
			dummy_dt.fdf

SIDEB_DEF	=	data/fs/superblock/sideb.def

FDFS	=	$(SIDEA_FDFS) \
		$(SIDEB_FDFS)

SIDEA		=	sidea.fdt
SIDEB		=	sideb.fdt

SIDES		=	$(SIDEA) \
			$(SIDEB)

HEADER		=	data/header.bin

.SUFFIXES : .bin .fdf .fdt .fds

all: $(FDS)

check: $(BIN_MAIN_PRO)
	$(CHECK) $(BIN_BG_CHR_1) $(CKSUM_BG_CHR_1)
	$(CHECK) $(BIN_BG_CHR_2) $(CKSUM_BG_CHR_2)
	$(CHECK) $(BIN_BG_CHR_3) $(CKSUM_BG_CHR_3)
	$(CHECK) $(BIN_BG_CHR_4) $(CKSUM_BG_CHR_4)
	$(CHECK) $(BIN_BG_CHR_C) $(CKSUM_BG_CHR_C)
	$(CHECK) $(BIN_CHAPT_01) $(CKSUM_CHAPT_01)
	$(CHECK) $(BIN_CHAPT_02) $(CKSUM_CHAPT_02)
	$(CHECK) $(BIN_CHAPT_03) $(CKSUM_CHAPT_03)
	$(CHECK) $(BIN_CHAPT_04) $(CKSUM_CHAPT_04)
	$(CHECK) $(BIN_CHAPT_05) $(CKSUM_CHAPT_05)
	$(CHECK) $(BIN_CHAPT_06) $(CKSUM_CHAPT_06)
	$(CHECK) $(BIN_CHAPT_07) $(CKSUM_CHAPT_07)
	$(CHECK) $(BIN_DUMMY_DT) $(CKSUM_DUMMY_DT)
	$(CHECK) $(BIN_ENDING_1) $(CKSUM_ENDING_1)
	$(CHECK) $(BIN_ENDING_2) $(CKSUM_ENDING_2)
	$(CHECK) $(BIN_ENDING_C) $(CKSUM_ENDING_C)
	$(CHECK) $(BIN_EN_SND_D) $(CKSUM_EN_SND_D)
	$(CHECK) $(BIN_IMAJIN_C) $(CKSUM_IMAJIN_C)
	$(CHECK) $(BIN_LINA_C) $(CKSUM_LINA_C)
	$(CHECK) $(BIN_MAIN_PRO) $(CKSUM_MAIN_PRO)
	$(CHECK) $(BIN_MAMA_C) $(CKSUM_MAMA_C)
	$(CHECK) $(BIN_PAPA_C) $(CKSUM_PAPA_C)
	$(CHECK) $(BIN_SAVE_DAT) $(CKSUM_SAVE_DAT)
	$(CHECK) $(BIN_SOUND_DT) $(CKSUM_SOUND_DT)
	$(CHECK) $(BIN_TEMP_PRG) $(CKSUM_TEMP_PRG)
	$(CHECK) $(BIN_TITLE_C) $(CKSUM_TITLE_C)

$(FDS): $(BIN_MAIN_PRO) $(HEADER) $(SIDES)
	cat $(HEADER) $(SIDES) > $@

$(SIDEA): $(SIDEA_FDFS)
	$(FDTC) $(SIDEA_FDFS) <$(SIDEA_DEF) | cat - /dev/zero | dd of=$@ bs=1 count=65500 2>/dev/null

$(SIDEB): $(SIDEB_FDFS)
	$(FDTC) $(SIDEB_FDFS) <$(SIDEB_DEF) | cat - /dev/zero | dd of=$@ bs=1 count=65500 2>/dev/null

.s.o:
	$(AS) $(ASFLAGS) -o $@ $<

.bin.fdf:
	$(BINTOFDF) $< <data/fs/files/$<.def >$@

$(BIN_MAIN_PRO): $(OBJS)
	$(LD) $(OBJS) $(LDFLAGS)

data/header.bin:
	data/header.sh

clean:
	rm -rf $(FDS) $(SIDES) $(OBJS) $(HEADER) $(FILES) $(FDFS)
