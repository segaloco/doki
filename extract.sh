#!/bin/sh

INFILE=$1

if test -z $INFILE
then
	INFILE=/dev/zero
fi

# Directory structure
mkdir -p data/ppu/title_c data/ppu/ending_c data/ppu/imajin_c data/ppu/lina_c data/ppu/mama_c data/ppu/papa_c
mkdir -p data/ppu/bg_chr_1 data/ppu/bg_chr_2 data/ppu/bg_chr_3 data/ppu/bg_chr_4 data/ppu/bg_chr_c

# CHR Common
TILE_SIZE=16
BG_BANK=256

# EN-SND-D
dd if=$INFILE bs=1 skip=332 count=1536 >data/unk/en_snd_d.bin

# MAIN-PRO
dd if=$INFILE bs=1 skip=3924 count=32 >data/ppu/sdisk_chr.bin

dd if=$INFILE bs=1 skip=10634 count=67 >data/tile_cols_x.bin
dd if=$INFILE bs=1 skip=10701 count=67 >data/tile_cols_y.bin

dd if=$INFILE bs=1 skip=24398 count=124 >data/map_floor_h.bin
dd if=$INFILE bs=1 skip=24522 count=124 >data/map_floor_v.bin

# TITLE-C
BANKBASE=41087
BANKLEN=8176
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while test $I -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/title_c/tile_obj_`printf "%.2X" $I`.bin
	I=`expr $I + 1`
done

while test $I -ne $TILES
do
	J=`expr $I - $BG_BANK`
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/title_c/tile_bg_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done

# ENDING-C
BANKBASE=49280
BANKLEN=3072
TILES=`expr $BANKLEN / $TILE_SIZE`
TILE_START=224

I=0
J=$TILE_START
while test $J -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/ending_c/tile_obj_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
	J=`expr $J + 1`
done

while test $I -ne $TILES
do
	J=`expr $I - \( $BG_BANK - $TILE_START \)`
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/ending_c/tile_bg_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done

# SOUND-DT
dd if=$INFILE bs=1 skip=101071 count=532 >data/unk/sound_dt.bin

# IMAJIN-C
BANKBASE=101620
BANKLEN=832
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while test $I -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/imajin_c/tile_obj_`printf "%.2X" $I`.bin
	I=`expr $I + 1`
done

# LINA-C
BANKBASE=102469
BANKLEN=832
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while test $I -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/lina_c/tile_obj_`printf "%.2X" $I`.bin
	I=`expr $I + 1`
done

# MAMA-C
BANKBASE=103318
BANKLEN=832
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while test $I -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/mama_c/tile_obj_`printf "%.2X" $I`.bin
	I=`expr $I + 1`
done

# PAPA-C
BANKBASE=104167
BANKLEN=832
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while test $I -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/papa_c/tile_obj_`printf "%.2X" $I`.bin
	I=`expr $I + 1`
done

# BG-CHR-C
BANKBASE=105016
BANKLEN=512
TILES=`expr $BANKLEN / $TILE_SIZE`

I=0
while test $I -ne $TILES
do
	J=`expr $I + 128`
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_c/tile_bg_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done

# BG-CHR-1
BANKBASE=105545
BANKLEN=2560
TILES=`expr $BANKLEN / $TILE_SIZE`
TILE_START=224

I=0
J=$TILE_START
while test $J -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_1/tile_obj_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
	J=`expr $J + 1`
done

while test $I -ne $TILES
do
	J=`expr $I - \( $BG_BANK - $TILE_START \)`
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_1/tile_bg_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done

# BG-CHR-2
BANKBASE=108122
BANKLEN=2560
TILES=`expr $BANKLEN / $TILE_SIZE`
TILE_START=224

I=0
J=$TILE_START
while test $J -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_2/tile_obj_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
	J=`expr $J + 1`
done

while test $I -ne $TILES
do
	J=`expr $I - \( $BG_BANK - $TILE_START \)`
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_2/tile_bg_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done

# BG-CHR-3
BANKBASE=110699
BANKLEN=2560
TILES=`expr $BANKLEN / $TILE_SIZE`
TILE_START=224

I=0
J=$TILE_START
while test $J -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_3/tile_obj_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
	J=`expr $J + 1`
done

while test $I -ne $TILES
do
	J=`expr $I - \( $BG_BANK - $TILE_START \)`
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_3/tile_bg_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done

# BG-CHR-4
BANKBASE=113276
BANKLEN=2560
TILES=`expr $BANKLEN / $TILE_SIZE`
TILE_START=224

I=0
J=$TILE_START
while test $J -ne $BG_BANK && test $I -ne $TILES
do
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_4/tile_obj_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
	J=`expr $J + 1`
done

while test $I -ne $TILES
do
	J=`expr $I - \( $BG_BANK - $TILE_START \)`
	dd if=$INFILE bs=1 count=$TILE_SIZE skip=`expr $I \* $TILE_SIZE + $BANKBASE` >data/ppu/bg_chr_4/tile_bg_`printf "%.2X" $J`.bin
	I=`expr $I + 1`
done
