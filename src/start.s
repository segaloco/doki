.include	"system/cpu.i"

.include	"system/fds.i"

.segment	"VECTORS"

	.addr	irq_bg
	.addr	irq_sprite
	.addr	nmi
	.addr	start
	.addr	int_null

.code

	.export	start, start_hot
start:
	lda	FDS_STACK_RESET_F
	cmp	#FDS_RESET_READY
	bne	:+
	lda	FDS_STACK_RESET_T
	cmp	#FDS_RESET_SOFT
	bne	:+ ; if (soft_reset) {
		lda	#FDS_RESET_FAIL
		sta	FDS_STACK_RESET_F
		jmp	(RESET)
	: ; } else {
	start_hot:
		jsr	start_b
		jsr	sdisk_load_files_sideb
	; }

	; NOTE: falls through to main for FDS side B
