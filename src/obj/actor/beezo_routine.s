.include	"mem.i"

	.export eng_actor_beezo_routine, eng_actor_dophys
eng_actor_beezo_routine:
	jsr	eng_enemy_damage_check

	jsr	render_actor_base

	inc	timer_ani_actor, x
	jsr	actor_timer_throw_tick
	jsr	actor_timer_ani_inc
	jsr	actor_timer_held_tick

	lda	veloc_y_actor, x
	beq	:+++
	bpl	:+ ; if (veloc_y_actor) {
		; if (veloc_y_actor < 0) {
			sta	timer_actor_throw, x
		: ; }

		lda	frame_count
		lsr	a
		bcc	:+ ; if (eng_cycle_odd) {
			dec	veloc_y_actor, x
		: ; }

	eng_actor_dophys:
		jsr	motion_x
		jmp	motion_y
	: ; } else {
		jsr	motion_x
		jmp	eng_actor_dophys
	; }
