.include	"system/cpu.i"

.include	"mem.i"
.include	"math.i"
.include	"tunables.i"
.include	"modes.i"
.include	"actor.i"
.include	"misc.i"

.include	"unk.i"

eng_actor_doclip_h:
	lda	#0
	sta	zp_byte_EE

	lda	attr_actor, x
	ldy	#1
	and	#actor_attrs::bit2
	bne	:+
	lda	obj_id_actor, x
	cmp	#actor::pokey
	beq	:+
	cmp	#actor::ostro
	beq	:+
	cmp	#actor::doormask_boss
	beq	:+
	lda	actor_mode, x
	and	#(actor_flags::wide)
	beq	:++
	: ; if (this.attr & bit2 || this.obj_id.in(pokey, ostro, doormask_boss) || this.mode & wide) {
		ldy	#3
	: ; }
	; for (y; y >= 0; y--) {
		lda	pos_x_lo_actor, x
		clc
		adc	eng_actor_doclip_off_x_lo, y
		sta	eng_actor_doclip_x_lo
		lda	pos_x_hi_actor, x
		adc	#0
		sta	eng_actor_doclip_x_hi
		lda	eng_actor_doclip_x_lo
		cmp	pos_x_lo_screen_left
		lda	eng_actor_doclip_x_hi
		sbc	pos_x_hi_screen_left
		beq	:+ ; if ((eng_actor_doclip_x - pos_x_lo_screen_left) == 0) {
			lda	zp_byte_EE
			ora	eng_actor_doclip_zp_byte_EE_bit, y
			sta	zp_byte_EE
		: ; }

		dey
		bpl	:--
	; }

eng_actor_doclip_h_rts:
	rts
; ----------------------------
eng_actor_dobounds_branch_type:
	.byte	CPU_OPCODE_BCC
	.byte	CPU_OPCODE_BCS

eng_actor_doclip_zp_byte_EE_bit:
	.byte	$08, $04, $02, $01

eng_actor_doclip_off_x_lo:
	.byte	$00, $08, $10, $18
; ----------------------------
actor_dobounds_y		= zp_addr_00

actor_dobounds_screen_y		= zp_dbyt_00
actor_dobounds_screen_y_b	= zp_dbyt_02

actor_dobounds_bound_left	= zp_dbyt_00
actor_dobounds_bound_right	= zp_dbyt_02

	.export eng_actor_dobounds, actor_destroy
eng_actor_dobounds:
	jsr	eng_actor_doclip_h

	lda	#34
	ldy	obj_id_actor, x
	cpy	#actor::wart
	beq	:+
	cpy	#actor::tryclyde
	beq	:+ ; if (this.obj_id.not_in(wart, tryclyde)) {
		lda	#16
	: ; }
	adc	pos_y_lo_actor, x
	sta	actor_dobounds_y

	lda	pos_y_hi_actor, x
	adc	#0
	sta	actor_dobounds_y+1

	lda	actor_dobounds_y
	cmp	pos_y_lo_screen
	lda	actor_dobounds_y+1
	sbc	pos_y_hi_screen
	sta	zp_byte_EF

	cpy	#$17
	beq	eng_actor_doclip_h_rts
	cpy	#$3F
	beq	eng_actor_doclip_h_rts
	cpy	#$41
	beq	eng_actor_doclip_h_rts
	cpy	#$2B
	beq	eng_actor_doclip_h_rts
	txa
	and	#MOD_2
	tay
	lda	eng_actor_dobounds_branch_type, y
	sta	:+
	lda	frame_count
	lsr	a
	: bcc	eng_actor_doclip_h_rts

	lda	pos_y_lo_screen
	sbc	#48
	sta	actor_dobounds_screen_y+1
	lda	pos_y_hi_screen

	sbc	#0
	sta	actor_dobounds_screen_y
	inc	actor_dobounds_screen_y
	lda	pos_y_lo_screen
	adc	#<-1
	php
	adc	#48
	sta	actor_dobounds_screen_y_b+1
	lda	pos_y_hi_screen
	adc	#0
	plp
	adc	#0
	sta	actor_dobounds_screen_y_b
	inc	actor_dobounds_screen_y_b
	lda	pos_y_lo_actor, x
	cmp	actor_dobounds_screen_y+1
	ldy	pos_y_hi_actor, x
	iny
	tya
	sbc	actor_dobounds_screen_y
	bmi	:++ ; if (this.pos_y >= screen_y) {
		lda	pos_y_lo_actor, x
		cmp	actor_dobounds_screen_y_b+1
		ldy	pos_y_hi_actor, x
		iny
		tya
		sbc	actor_dobounds_screen_y_b
		bpl	:++ ; if (this.pos_y < screen_y_b) {
			lda	pos_x_lo_screen_left
			sbc	#48
			sta	actor_dobounds_bound_left+1
			lda	pos_x_hi_screen_left
			sbc	#0
			sta	actor_dobounds_bound_left
			inc	actor_dobounds_bound_left
			lda	pos_x_lo_screen_right
			adc	#48
			sta	actor_dobounds_bound_right+1
			lda	pos_x_hi_screen_right
			adc	#0
			sta	actor_dobounds_bound_right
			inc	actor_dobounds_bound_right
			lda	pos_x_lo_actor, x
			cmp	actor_dobounds_bound_left+1
			ldy	pos_x_hi_actor, x
			iny
			tya
			sbc	actor_dobounds_bound_left
			bmi	:+ ; if (this.pos_x >= bound_left) {
				lda	pos_x_lo_actor, x
				cmp	actor_dobounds_bound_right+1
				ldy	pos_x_hi_actor, x
				iny
				tya
				sbc	actor_dobounds_bound_right
				bmi	eng_actor_dobounds_rts
			: ; }

			ldy	obj_id_actor, x
			lda	eng_actor_flag_tbl, y
			and	#$08
			bne	eng_actor_dobounds_rts
		: ; }
	; }

	lda	timer_actor_held, x
	bne	eng_actor_dobounds_rts

actor_destroy:
	ldy	eng_actor_data_offset, x
	bmi	:+ ; if (eng_actor_data_offset[x] >= 0) {
		lda	(course_actor_addr), y
		and	#$7F
		sta	(course_actor_addr), y
	: ; }

	lda	#actor_states::null
	sta	proc_id_actor, x

eng_actor_dobounds_rts:
	rts
; -----------------------------------------------------------
	.export state_actor_normal
state_actor_normal:
	lda	#1
	sta	eng_actor_noholdtop, x

	ldy	timer_actor_throw, x
	dey
	cpy	#31
	bcs	:+ ; if ((--this.timer_throw) < 31) {
		inc	timer_actor_throw, x
	: ; }

	jsr	eng_actor_dobounds
	lda	ppu_scroll_req
	and	#scroll_req::bit2
	bne	state_actor_normal_render
	lda	eng_timer_stopwatch
	bne	:+
	lda	timer_actor_stun, x
	beq	:++++
	: lda	obj_id_actor, x
	cmp	#actor::heart
	beq	:+++
	cmp	#actor::vegetable_small
	bcs	:+++ ; if ((timer_stopwatch != 0 || this.timer_stun != 0) && this.obj_id != heart && this.obj_id < vegetable_small) {
		; if (!(ppu_scroll_req & bit2)) {
			jsr	eng_enemy_damage_check

			lda	timer_actor_throw, x
			beq	:+ ; if (this.timer_throw != 0) {
				jsr	eng_actor_domove
			: ; }

			lda	timer_actor_held, x
			beq	:+ ; if (this.timer_held != 0) {
				dec	timer_ani_actor, x
				jmp	actor_throw
			: ; }

			jsr	eng_col_actor
		state_actor_normal_render: ; }
		jmp	render_actor_base
	: ; }

	ldy	#ACTOR_DIR_RIGHT
	lda	veloc_x_actor, x
	beq	:++
	bpl	:+ ; if (this.veloc_x != 0x1) {
		; if (this.veloc_x <= 0) {
			iny
		: ; }

		sty	disp_dir_actor, x
	: ; }

	ldy	obj_id_actor, x
	lda	actor_attr_tbl, y
	and	#(actor_attrs::bit5)
	bne	:+++ ; if (!(actor_attr_tbl[this.obj_id] & bit5)) {
		lda	attr_actor, x
		and	#<~(actor_attrs::bit5)
		sta	attr_actor, x
		lda	timer_actor_held, x
		cmp	#2
		bcc	:+++ ; if (this.timer_held >= 2) {
			lda	obj_id_actor, x
			cmp	#actor::bobomb
			bne	:+
			lda	collision_actor, x
			and	#(actor_collisions::floor)
			bne	:++
			: lda	actor_attr_tbl, y
			bpl	:++
			: ; if ((this.obj_id == bobomb && (this.collision & floor)) || actor_attr_tbl[this.obj_id] & bit7) {
				lda	attr_actor, x
				ora	#(actor_attrs::bit5)
				sta	attr_actor, x
			: ; }
		; }
	; }

	jsr	eng_actor_routines

	lda	pos_y_hi_actor, x
	bmi	:+
	lda	pos_y_rel_actor
	cmp	#$E8
	bcc	:+ ; if (this.pos_y >= 0 && this.pos_y_rel >= 0xE8) {
		rts
	: ; } else {
		jmp	eng_col_actor
	; }
; -----------------------------------------------------------
eng_actor_routines:
	lda	obj_id_actor, x
	jsr	tbljmp
	.addr	actor_proc_heart
	.addr	actor_proc_base
	.addr	actor_proc_base
	.addr	actor_proc_base
	.addr	actor_proc_base
	.addr	actor_proc_base
	.addr	actor_proc_base
	.addr	actor_proc_base
	.addr	eng_actor_ostro_routine
	.addr	actor_proc_bobomb
	.addr	actor_proc_albatross
	.addr	actor_proc_albatross
	.addr	actor_proc_albatross
	.addr	actor_proc_ninji_run
	.addr	actor_proc_ninji_jump
	.addr	eng_actor_beezo_routine
	.addr	eng_actor_beezo_routine
	.addr	sub_c3f4
	.addr	sub_c27e
	.addr	actor_proc_trouter
	.addr	actor_proc_hoopstar
	.addr	actor_proc_spawnjar
	.addr	actor_proc_spawnjar
	.addr	actor_proc_phanto
	.addr	sub_c387
	.addr	sub_c308
	.addr	sub_c42b
	.addr	actor_proc_proj_egg
	.addr	actor_proc_birdo
	.addr	sub_C365
	.addr	actor_proc_proj_egg
	.addr	sub_c16f
	.addr	actor_proc_fireball
	.addr	actor_proc_panser_red_grey
	.addr	actor_proc_panser_pink
	.addr	actor_proc_panser_red_grey
	.addr	sub_c3d2
	.addr	actor_proc_fire_bomb
	.addr	sub_c4d3
	.addr	sub_c59a
	.addr	sub_c2ac
	.addr	sub_c369
	.addr	sub_c331
	.addr	sub_c149
	.addr	actor_proc_sparks
	.addr	actor_proc_sparks
	.addr	actor_proc_sparks
	.addr	actor_proc_sparks
	.addr	actor_proc_vegetable
	.addr	actor_proc_vegetable
	.addr	actor_proc_vegetable
	.addr	eng_actor_bighead_routine
	.addr	actor_proc_coin
	.addr	actor_proc_bomb
	.addr	sub_c10a
	.addr	actor_proc_life_pow
	.addr	actor_proc_life_pow
	.addr	actor_proc_waterfall_log
	.addr	actor_proc_door_subsp
	.addr	actor_proc_key
	.addr	actor_proc_lamp
	.addr	actor_proc_lifepickup
	.addr	actor_proc_oneup
	.addr	actor_C15A
	.addr	actor_proc_doormask
	.addr	actor_proc_doormask
	.addr	actor_proc_crystalball
	.addr	actor_proc_starman
	.addr	actor_proc_lifepickup
