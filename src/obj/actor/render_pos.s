.include	"actor.i"

.include	"mem.i"

	.export eng_actor_chr_idx_page_0
	.export eng_actor_sprite_tbl
eng_actor_chr_idx_page_0:
	.byte	$D0, $D2
	.byte	$D4, $D6

	.byte	$F8, $F8
	.byte	$FA, $FA

	.byte	$CC, $CE
	.byte	$CC, $CE

	.byte	$C8, $CA
	.byte	$C8, $CA

	.byte	$70, $72
	.byte	$74, $76

	.byte	$C0, $C2
	.byte	$C4, $C6

	.byte	$3C, $3E

	.byte	$40, $42
	.byte	$44, $46
	.byte	$48, $4A

	.byte	$78, $7A
	.byte	$7C, $7E

	.byte	$DC, $DA
	.byte	$DC, $DE

	.byte	$FE, $FE
	.byte	$FC, $FC

	.byte	$94, $94
	.byte	$96, $96
	.byte	$98, $98
	.byte	$9A, $9A
	.byte	$9C, $9E
	.byte	$9C, $9E

	.byte	$7D, $7F

	.byte	$C1, $C3

	.byte	$8C, $8C
	.byte	$8E, $8E

	.byte	$E0, $E2

	.byte	$6B, $6D
	.byte	$6D, $6F

	.byte	$3A, $3A
	.byte	$3A, $3A
	.byte	$38, $38
	.byte	$38, $38
	.byte	$36, $36
	.byte	$36, $36
	.byte	$34, $34
	.byte	$34, $34

	.byte	$AE, $FB
	.byte	$AE, $FB

	.byte	$80, $82
	.byte	$84, $86
	.byte	$80, $82

	.byte	$AA, $AC

	.byte	$88, $8A
	.byte	$84, $86
	.byte	$88, $8A

	.byte	$AA, $AC

	.byte	$BC, $BE

	.byte	$AA, $AC

	.byte	$BC, $BE

	.byte	$AA, $AC

	.byte	$B5, $B9
	.byte	$B5, $B9

	.byte	$81, $83
	.byte	$85, $87

	.byte	$FF, $FF
	.byte	$FF, $FF

	.byte	$81, $83

	.byte	$F5, $87

	.byte	$C5, $C7
	.byte	$C9, $CB

	.byte	$92, $94

	.byte	$29, $29
	.byte	$2B, $2B

	.byte	$3D, $3F

	.byte	$4C, $4E
	.byte	$50, $52
	.byte	$4C, $4E

	.byte	$56, $58
	.byte	$FB, $5C
	.byte	$FB, $5A
	.byte	$FB, $FB
	.byte	$FB, $54

	.byte	$CF, $CF

	.byte	$A5, $A5

	.byte	$B0, $B0

	.byte	$90, $90

	.byte	$CD, $CD

	.byte	$A8, $A8
	.byte	$A8, $A8
	.byte	$A0, $A2
	.byte	$A4, $A4
	.byte	$A4, $A4

	.byte	$4D, $4D

	.byte	$8C, $8C

	.byte	$A6, $A6
	.byte	$AB, $AB
; -----------------------------------------------------------
eng_actor_sprite_tbl:
	.byte	actor_sprite::null

	.byte	2*actor_sprite::unk_00

	.byte	2*actor_sprite::unk_04

	.byte	2*actor_sprite::unk_00

	.byte	2*actor_sprite::unk_06

	.byte	2*actor_sprite::unk_08
	.byte	2*actor_sprite::unk_08
	.byte	2*actor_sprite::unk_08

	.byte	$6F

	.byte	2*actor_sprite::unk_0A

	.byte	2*actor_sprite::unk_0C
	.byte	2*actor_sprite::unk_0C
	.byte	2*actor_sprite::unk_0C

	.byte	2*actor_sprite::unk_10
	.byte	2*actor_sprite::unk_10

	.byte	2*actor_sprite::unk_12
	.byte	2*actor_sprite::unk_12

	.byte	$59

	.byte	$2F

	.byte	2*actor_sprite::unk_43
	.byte	2*actor_sprite::unk_44

	.byte	actor_sprite::none
	.byte	actor_sprite::none

	.byte	2*actor_sprite::unk_46

	.byte	$43
	.byte	$43

	.byte	$53

	.byte	2*actor_sprite::unk_2B
	.byte	2*actor_sprite::unk_2D

	.byte	$43

	.byte	2*actor_sprite::unk_39

	.byte	actor_sprite::null

	.byte	2*actor_sprite::unk_54

	.byte	2*actor_sprite::unk_56
	.byte	2*actor_sprite::unk_56
	.byte	2*actor_sprite::unk_56

	.byte	$35
	.byte	$3B

	.byte	$53

	.byte	$5B
	.byte	$41
	.byte	$51

	.byte	actor_sprite::null

	.byte	actor_sprite::null

	.byte	2*actor_sprite::unk_5B
	.byte	2*actor_sprite::unk_5B
	.byte	2*actor_sprite::unk_5B
	.byte	2*actor_sprite::unk_5B

	.byte	2*actor_sprite::unk_14
	.byte	2*actor_sprite::unk_15
	.byte	2*actor_sprite::unk_16
	.byte	2*actor_sprite::unk_17
	.byte	2*actor_sprite::unk_18
	.byte	2*actor_sprite::unk_1A

	.byte	actor_sprite::null

	.byte	2*actor_sprite::unk_1C
	.byte	2*actor_sprite::unk_1D

	.byte	2*actor_sprite::unk_21

	.byte	2*actor_sprite::unk_41
	.byte	2*actor_sprite::unk_41

	.byte	2*actor_sprite::unk_42

	.byte	2*actor_sprite::unk_50
	.byte	2*actor_sprite::one_up

	.byte	$33

	.byte	2*actor_sprite::unk_47
	.byte	2*actor_sprite::unk_47

	.byte	2*actor_sprite::unk_4F

	.byte	2*actor_sprite::unk_53
	.byte	2*actor_sprite::unk_52
; -----------------------------------------------------------
	.export lib_calc_actor_pos
lib_calc_actor_pos:
	lda	pos_y_lo_actor, x
	clc
	sbc	pos_y_lo_screen
	sta	pos_y_rel_actor
	lda	pos_x_lo_actor, x
	sec
	sbc	pos_x_lo_screen_left
	sta	pos_x_rel_actor
	rts
