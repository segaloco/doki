.include	"actor.i"

.include	"mem.i"



BULLET_PRJ_VELOC_X	= 32
BOBOMB_CUE_1	= $3A
BOBOMB_CUE_2	= $30

TWEETER_VELOC_Y		= <-16
TWEETER_CYCLE_COUNT	= zp_actor_work_byte_1
TWEETER_CYCLE_RESET	= 4

; -----------------------------------------------------------
	.export actor_bullet_prj_veloc_x
actor_bullet_prj_veloc_x:
	.byte	BULLET_PRJ_VELOC_X
	.byte	<-(BULLET_PRJ_VELOC_X)
; ----------------------------
	.export actor_proc_bobomb
actor_proc_bobomb:
	ldy	timer_actor, x
	cpy	#BOBOMB_CUE_1
	bcs	actor_proc_base ; if (actor.timer_1 < BOBOMB_CUE_1) {
		lda	collision_actor, x
		and	#(actor_collisions::floor)
		beq	:+ ; if (actor.collision & floor) {
			lda	#0
			sta	veloc_x_actor, x
		: ; }

		dec	timer_ani_actor, x
		tya
		bne	:++
		lda	timer_actor_held, x
		beq	:+ ; if (actor.timer_ani == 0) {
			; if (actor.timer_held != 0) {
				sty	player_item_held
				sty	timer_actor_held, x
			: ; }

			jmp	render_bombboom
		: cmp	#BOBOMB_CUE_2
		bcs	actor_proc_base
		lsr	a
		bcc	actor_proc_base ; } else if (this.timer < BOBOMB_CUE_2 && (this.timer % 1)) {
			inc	attr_actor, x
			lda	attr_actor, x
			and	#<~(actor_attrs::bit2)
			sta	attr_actor, x
		; }
	; }
; -----------------------------------------------------------
	.export	actor_proc_base
	.export actor_proc_base_disp
	.export actor_proc_base_motion_do
	.export actor_proc_base_veloc_reverse
	.export actor_proc_base_veloc_reset
	.export actor_proc_base_veloc_apply
actor_proc_base:
	jsr	col_actor_tile_proc
	jsr	eng_enemy_damage_check
	lda	eng_actor_tbl_480, x
	beq	:++ ; if (eng_actor_tbl_480[x]) {
		lda	collision_actor, x
		and	#(actor_collisions::ceil)
		beq	:+ ; if (actor.collision & ceil) {
			jmp	actor_destroy
		: ; } else {
			dec	eng_actor_tbl_480, x
			inc	timer_actor, x

		actor_proc_base_disp:
			lda	attr_actor, x
			ora	#(actor_attrs::bit5)
			sta	attr_actor, x
			jsr	motion_y
			jmp	render_actor_base
		; }
	: ; }

	lda	collision_actor, x
	and	disp_dir_actor, x
	beq	:++ ; if (actor.collision & actor.disp_dir) {
		jsr	motion_dir_flip
		lda	timer_actor_throw, x
		beq	:+ ; if (actor.timer_throw != 0) {
			jsr	actor_proc_base_veloc_reverse
			jsr	actor_proc_base_veloc_reverse
		: ; }
	: ; }

	jsr	actor_timer_held_tick
	jsr	render_actor_base
	lda	obj_id_actor, x
	inc	timer_ani_actor, x
	cmp	#actor::snifit_grey
	bne	:++ ; if (actor.id == snifit_grey) {
		lda	timer_actor_throw, x
		bne	:+ ; if (actor.timer_throw == 0) {
			sta	veloc_x_actor, x
		: ; }
	: ; }

	jsr	eng_actor_domove
	lda	collision_actor, x
	ldy	veloc_y_actor, x
	bpl	:+++++
	and	#actor_collisions::ceil
	beq	:+ ; if (actor.veloc_y < 0 && (actor.collision & ceil)) {
		lda	#0
		sta	veloc_y_actor, x
		rts
	: ; } else if ((actor.veloc_y < 0 && !(actor.collision & ceil)) || !(actor.collision & floor)) {
		lda	timer_actor_throw, x
		bne	:+
		lda	obj_id_actor, x
		cmp	#actor::snifit_grey
		bne	:+
		lda	veloc_y_actor, x
		cmp	#<-2
		bne	:+ ; if (actor.timer_throw == 0 && actor.id == snifit_grey && actor.veloc_y == -2) {
			lda	#2
			sta	timer_actor_shake, x
		: ; }

		dec	timer_ani_actor, x
		lda	obj_id_actor, x
		cmp	#actor::snifit_pink
		beq	:+
		cmp	#actor::shyguy_pink
		bne	:++
		: lda	timer_actor_throw, x
		bne	:+
		lda	eng_actor_tbl_477, x
		bne	:+ ; if (actor.id.in(snifit_pink, shyguy_pink) && actor.timer_throw == 0 && !eng_actor_tbl_477[x]) {
			inc	eng_actor_tbl_477, x
			jmp	motion_dir_flip
		: ; } else {
			rts
		; }
	: ; }
	and	#actor_collisions::floor
	beq	:-----

	lda	#0
	sta	eng_actor_tbl_477, x

	ldy	obj_id_actor, x
	cpy	#actor::tweeter
	bne	:++ ; if (actor.id == tweeter) {
		lda	#$3F
		jsr	actor_proc_base_shake_throw_skip

		inc	TWEETER_CYCLE_COUNT, x
		ldy	#TWEETER_VELOC_Y
		lda	TWEETER_CYCLE_COUNT, x
		and	#TWEETER_CYCLE_RESET-1
		bne	:+ ; if (!(++TWEETER_CYCLE_COUNT[x] % 4)) {
			ldy	#<(2*(TWEETER_VELOC_Y))
		: ; }
		sty	veloc_y_actor, x

		jmp	motion_y
	: ; }

	lda	#$1F
	cpy	#actor::bobomb
	beq	actor_proc_base_shake_throw_skip
	cpy	#actor::flurry
	beq	actor_proc_base_shake_throw_skip
	lda	#$3F
	cpy	#actor::ninji_running
	beq	actor_proc_base_shake_throw_skip
	lda	#$7F
	cpy	#actor::snifit_red
	beq	:+
	cpy	#actor::snifit_red
	beq	:+
	cpy	#actor::snifit_pink
	beq	:+
	cpy	#actor::snifit_grey
	bne	actor_proc_base_ani_skip
	lda	timer_actor_throw, x
	bne	actor_proc_base_ani_skip ; if (actor.id.not_in(bobomb, flurry, ninji_running, snifit_red, snifit_pink, snifit_grey) && actor.timer_throw == 0) {
		jsr	motion_side_get
		iny
		sty	disp_dir_actor, x

		lda	timer_ani_actor, x
		and	#%00111111
		bne	:+ ; if (actor.timer_ani % 64) {
			lda	#<-24
			sta	veloc_y_actor, x
			jmp	motion_y
		: ; }
	; }

	lda	timer_actor_shake, x
	beq	:++ ; if (actor.timer_shake != 0) {
		dec	timer_ani_actor, x
		dec	timer_actor_shake, x
		bne	:+ ; if (--actor.timer_shake == 0) {
			jsr	eng_actor_bullet_prj_lo
			jmp	actor_proc_base_reset_skip
		: ; }
	: ; }

	txa
	asl	a
	asl	a
	asl	a
	adc	frame_count
	asl	a
	bne	:+++ ; if ((((actor_index * 8) + frame_count) * 2) == 0) {
		lda	obj_id_actor, x
		cmp	#actor::snifit_grey
		bne	:+ ; if (actor.id == snifit_grey) {
			lda	#2
			sta	timer_actor_shake, x
			jmp	actor_proc_base_shake_do
		: lda	pos_y_lo_actor, x
		sec
		sbc	#16
		cmp	pos_y_lo_player
		bne	:+ ; } else if ((actor.pos_y - 16) == player.pos_y) {
			lda	#48
			sta	timer_actor_shake, x
		: ; }
	: ; }

	lda	#%01111111
; ----------------------------
actor_proc_base_shake_throw_skip:
	and	timer_ani_actor, x
	beq	actor_proc_base_motion_y_skip

actor_proc_base_ani_skip:
	lda	timer_actor_throw, x
	beq	actor_proc_base_reset_skip
	lda	veloc_y_actor, x
	cmp	#26
	bcc	actor_proc_base_motion_y_skip
	lda	#<-16

actor_proc_base_motion_do:
	jsr	actor_proc_base_veloc_apply
	jsr	motion_y

actor_proc_base_veloc_reverse:
	lda	veloc_x_actor, x
	sta	zp_addr_00
	asl	a
	ror	veloc_x_actor, x
	rts

actor_proc_base_motion_y_skip:
	jsr	actor_init_base_timerless

actor_proc_base_reset_skip:
	lda	obj_id_actor, x
	cmp	#actor::shyguy_red
	bne	:+
	lda	veloc_y_actor, x
	cmp	#4
	bcc	:+ ; if (actor.id == shyguy_red && actor.veloc_y >= 4) {
		jsr	actor_init_base_timerless
	: ; }

actor_proc_base_shake_do:
	asl	attr_actor, x
	lsr	attr_actor, x

actor_proc_base_veloc_reset:
	lda	#0

actor_proc_base_veloc_apply:
	sta	veloc_y_actor, x
	lda	pos_y_lo_actor, x
	and	#%11110000
	sta	pos_y_lo_actor, x
	rts
