.include	"system/ppu.i"

.include	"tunables.i"
.include	"modes.i"
.include	"actor.i"

.include	"mem.i"

.include	"unk.i"

; -----------------------------------------------------------
	.export	eng_smokepoof_ani_0, eng_smokepoof_ani_1
	.export eng_smokepoof_ani_2, eng_smokepoof_ani_3
	.export eng_smokepoof_ani_idx
eng_smokepoof_ani_0:
	.dbyt   PPU_VRAM_BG1+$2D0
	.byte	(:++)-(:+)
:
	.byte   $FC, $FC, $AD, $FA
:

eng_smokepoof_ani_1:
	.dbyt   PPU_VRAM_BG1+$2F0
	.byte	(:++)-(:+)
:
	.byte   $FC, $FC, $AC, $AD
:

eng_smokepoof_ani_2:
	.dbyt   PPU_VRAM_BG1+$310
	.byte	(:++)-(:+)
:
	.byte   $FC, $FC, $AC, $AC, $AD, $FA
:

eng_smokepoof_ani_3:
	.dbyt   PPU_VRAM_BG1+$330
	.byte	(:++)-(:+)
:
	.byte	$FC, $FC, $AC, $AC, $AC, $AD
:

	.byte	$00

eng_smokepoof_ani_idx:
	.byte	$00, $07, $0E, $17
; -----------------------------------------------------------
	.export eng_map_flicker
eng_map_flicker:
	.byte	$C0, $70, $80, $50
	.byte	$A0, $40, $B0, $60
	.byte	$90, $C0, $70, $80
	.byte	$50, $A0, $40, $B0
	.byte	$60
; ----------------------------
	.export eng_map_reset
eng_map_reset:
	lda	#0
	sta	eng_area_started
	sta	eng_carried_actor
	sta	eng_timer_subspace_2
	sta	eng_timer_subspace

	ldx	#(ENG_ACTOR_MAX)-1
	: ; for (actor of actors) {
		lda	proc_id_actor, x
		beq	:++ ; if (actor.proc_id != null) {
			lda	timer_actor_held, x
			beq	:+
			lda	obj_id_actor, x
			cmp	#actor::mask_block
			beq	:+ ; if (actor.timer_held != 0 && actor.id != mask_block) {
				sta	eng_carried_actor
			: ; }

			jsr	actor_destroy
		: ; }

		dex
		bpl	:---
	; }

eng_map_reset_rts:
	rts
; -----------------------------------------------------------
eng_map_one_up_chr_idx:
	.byte	$B0
	.byte	$B2
	.byte	$B6
	.byte	$B4
eng_map_one_up_chr_idx_end:
	.assert (eng_map_one_up_chr_idx_end-eng_map_one_up_chr_idx = (ENG_CHARACTERS)), error, "eng_map_one_up_chr_idx player count mismatch"
; ----------------------------
	.export eng_map_init
eng_map_init:
	lda	eng_transition_area
	bne	eng_map_reset_rts
	lda	eng_area_started
	beq	:+ ; if (!eng_transition_area && eng_area_started) {
		jmp	game_proc
	: ; }

	inc	eng_area_started
	sta	eng_timer_pow_quake
	sta	eng_timer_sky_flash
	sta	eng_doormask_open_size
	sta	eng_doormask_closing
	sta	eng_swarm_type
	sta	eng_timer_doormask_open
	sta	eng_scroll_lock_x
	sta	game_scrolldir
	sta	veloc_x_player
	sta	timer_player
	sta	player_item_held
	sta	eng_timer_player_state
	sta	eng_bg_y
	sta	eng_pokey_scr_x_b
	sta	eng_timer_crouch_jump
	sta	eng_timer_jump_float
	sta	eng_player_quicksand
	sta	eng_boss_beaten

	ldy	obj_id_player
	lda	eng_map_one_up_chr_idx, y
	sta	eng_actor_chr_idx_page_0+2*(actor_sprite::one_up)
	sta	eng_actor_chr_idx_page_0+1+2*(actor_sprite::one_up)
	lda	#$B6
	sta	byte_4BA

	lda	eng_transition_type
	ora	eng_level
	bne	:+ ; if (!(eng_transition_type|eng_level)) {
		lda	#$10
		sta	apu_sfx_queue2
	: ; }

	lda	eng_carried_actor
	beq	:++ ; if (eng_carried_actor) {
		ldx	#5
		stx	actor_index

		cmp	#actor::life_heart
		beq	:++ ; if (eng_carried_actor != life_heart) {
			sta	obj_id_actor, x
			ldy	#1
			sty	proc_id_actor+5
			ldy	#$FF
			sty	eng_actor_data_offset+5

			cmp	#actor::rocket
			bne	:+ ; if (eng_carried_actor == rocket) {
				sta	zp_actor_work_byte_2, x
				sta	eng_player_in_rocket, x
				sta	eng_actor_tbl_477, x
				lda	#0
				sta	pos_x_hi_actor, x
				sta	pos_y_hi_actor, x
				jsr	actor_attr_set
				lda	#ENG_ACTOR_ROCKET_VELOC_Y
				sta	veloc_y_actor, x
				asl	a
				sta	pos_y_lo_actor, x
				lda	#<(ENG_ACTOR_ROCKET_POS_X)
				sta	pos_x_lo_actor, x
				bne	:++
			: ; } else {
				pha

				stx	player_item_held_id
				jsr	actor_init_base
				lda	#1
				sta	timer_actor_held, x
				sta	player_item_held
				jsr	actor_throw

				pla
				cmp	#actor::key
				bne	:+ ; if (eng_carried_actor == key) {
					inc	zp_actor_work_byte_1, x
					dex
					stx	actor_index
					lda	#actor_states::normal
					sta	proc_id_actor, x
					lda	#actor::phanto
					sta	obj_id_actor, x
					jsr	actor_init_base
					lda	pos_y_lo_screen
					sta	pos_y_lo_actor, x
					lda	pos_y_hi_screen
					sta	pos_y_hi_actor, x
					lda	pos_x_lo_screen_left
					sta	pos_x_lo_actor, x
					lda	pos_x_hi_screen_left
					sta	pos_x_hi_actor, x
					jsr	actor_erase
				; }
			; }
		; }
	: ; }

	ldy	course_sub
	lda	tbl_C01E, y
	sta	zp_addr_00
	lda	tbl_C01B, y
	sta	zp_addr_00+1
	lda	tbl_C024, y
	sta	zp_addr_02
	lda	tbl_C021, y
	sta	zp_addr_02+1
	ldy	eng_level_area
	lda	(zp_addr_02), y
	sta	course_actor_addr
	lda	(zp_addr_00), y
	sta	course_actor_addr+1

	lda	eng_level_h
	bne	eng_map_init_level_h ; if (!eng_level_h) {
		lda	#(ENG_MAP_BOUND_COUNT_V)-1
		sta	zp_work+$09

		lda	pos_y_lo_screen
		sbc	#<(ENG_MAP_BOUND_DEC)
		and	#ENG_MAP_BOUND_MASK
		sta	eng_ppu_seam
		lda	pos_y_hi_screen
		sbc	#>(ENG_MAP_BOUND_DEC)
		sta	eng_ppu_seam+1

		: ; for (r = ENG_MAP_BOUND_COUNT_V-1; r >= 0; r--) {
			lda	eng_ppu_seam+1
			cmp	#ENG_MAP_BOUND_MARGIN
			bcs	:+++ ; if (eng_ppu_seam >= margin) {
				ldx	#(ENG_MAP_BOUND_COUNT_ENEMIES)-1
				: ; for (enemy of enemies) {
					stx	actor_index

					lda	proc_id_actor, x
					bne	:+ ; if (enemy.proc_id == null) {
						jsr	actor_init_v_nocheck
					: ; }

					dex
					bpl	:--
				; }
			: ; }

			jsr	eng_map_seam_inc

			dec	zp_work+$09
			bpl	:----
		; }

		rts
	; }
; ----------------------------
eng_map_seam_inc:
	lda	eng_ppu_seam
	clc
	adc	#PPU_CHR_SIZE
	sta	eng_ppu_seam
	bcc	:+
	inc	eng_ppu_seam+1
	:

	rts
; ----------------------------
eng_map_init_level_h: ; else if (eng_level_h) {
		lda	pos_x_lo_screen_left
		sbc	#<(ENG_MAP_BOUND_DEC)
		and	#ENG_MAP_BOUND_MASK
		sta	eng_ppu_seam
		lda	pos_x_hi_screen_left
		sbc	#>(ENG_MAP_BOUND_DEC)
		sta	eng_ppu_seam+1

		lda	#(ENG_MAP_BOUND_COUNT_H)-1
		sta	zp_work+$09

		: ; for (r = ENG_MAP_BOUND_COUNT_H-1; r >= 0; r--) {
			lda	eng_ppu_seam+1
			cmp	#ENG_MAP_BOUND_MARGIN
			bcs	:+++ ; if (eng_ppu_seam >= margin) {
				ldx	#(ENG_MAP_BOUND_COUNT_ENEMIES)-1
				: ; for (enemy of enemies) {
					stx	actor_index

					lda	proc_id_actor, x
					bne	:+ ; if (enemy.proc_id == null) {
						jsr	actor_init_h_nocheck
					: ; }

					dex
					bpl	:--
				; }
			: ; }

			jsr	eng_map_seam_inc

			dec	zp_work+$09
			bpl	:----
		; }

		rts
	; }
; -----------------------------------------------------------
game_proc:
	lda	eng_timer_stopwatch
	beq	:+++ ; if (eng_timer_stopwatch) {
		lda	frame_count
		and	#$1F
		bne	:+ ; if (!(frame_count & 0x1F)) {
			ldy	#$40
			sty	apu_dpcm_queue
		: ; }

		lsr	a
		bcc	:+ ; if (frame_count % 2) {
			dec	eng_timer_stopwatch
		: ; }
	: ; }

	lda	pos_x_lo_screen_left
	clc
	adc	#<-1
	sta	pos_x_lo_screen_right
	lda	pos_x_hi_screen_left
	adc	#0
	sta	pos_x_hi_screen_right

	dec	byte_400
	bpl	:+ ; if (--byte_400 < 0) {
		lda	#8
		sta	byte_400
	: ; }

	ldx	#ENG_ACTOR_MAX-1
	game_proc_loop: ; for (actor of actors) {
		stx	actor_index
		txa
		clc
		adc	byte_400
		tay
		lda	eng_map_flicker, y
		ldy	timer_actor_held, x
		beq	:++ ; if (actor.timer_held != 0) {
			lda	#$10
			ldy	obj_id_actor, x
			cmp	#actor::rocket
			bne	:+ ; if (actor.obj_id == rocket) {
				lda	#$00
			: ; }
		: ; }
		sta	zp_byte_F4

		lda	proc_id_actor, x
		cmp	#actor_states::dead
		bcs	:+
		lda	obj_id_actor, x
		cmp	#actor::vegetable_small
		bcs	:+
		lda	eng_timer_stopwatch
		bne	:++++
		: ; if (actor.proc_id >= dead || actor.id >= vegetable_small || timer_stopwatch == 0) {
			lda	timer_actor, x
			beq	:+ ; if (actor.timer != 0) {
				dec	timer_actor, x
			: ; }

			lda	eng_timer_actor_2, x
			beq	:+ ; if (actor.timer_2 != 0) {
				dec	eng_timer_actor_2, x
			: ; }
		: ; }

		lda	timer_actor_flash, x
		beq	:+ ; if (actor.timer_flash != 0) {
			dec	timer_actor_flash, x
		: ; }

		lda	timer_actor_stun, x
		beq	:+
		lda	frame_count
		lsr	a
		bcc	:+ ; if (actor.timer_stun != 0 && (frame_count % 2) != 0) {
			dec	timer_actor_stun, x
		: ; }

		lda	byte_4BA
		and	#$02
		sta	zp_addr_00
		lda	byte_4BB
		and	#$02
		eor	zp_addr_00
		clc
		beq	:+ ; if ((byte_4BB & 0x02) ^ zp_addr_00) {
			sec
		: ; }
		ror	byte_4BA
		ror	byte_4BB

		jsr	lib_calc_actor_pos
		jsr	actor_proc

		ldx	actor_index
		dex
		bpl	game_proc_loop
	; }

	lda	eng_swarm_type
	beq	actor_init_swarm_stop_rts ; if (eng_swarm_type) {
		sec
		sbc	#actor::swarm
		jsr	tbljmp
		.addr	actor_init_swarm_albatross
		.addr	actor_init_swarm_beezo
		.addr	actor_init_swarm_stop
		.addr	actor_init_swarm_vegetables
	; }
; ----------------------------
	.export	actor_init_swarm_stop
actor_init_swarm_stop:
	lda	#0
	sta	eng_swarm_type

actor_init_swarm_stop_rts:
	rts
; -----------------------------------------------------------
actor_proc:
	lda	proc_id_actor, x
	jsr	tbljmp
	.addr	actor_init
	.addr	state_actor_normal
	.addr	state_actor_dead
	.addr	state_actor_blockfizz
	.addr	state_actor_bombboom
	.addr	state_actor_smokepoof
	.addr	state_actor_sand
	.addr	state_actor_sinking
; -----------------------------------------------------------
eng_enemy_screen_margin_lo:
	.byte	$18, <-($20)

eng_enemy_screen_margin_hi:
	.byte	$01, <-($01)
; ----------------------------
actor_init:
	lda	eng_boss_beaten
	bne	actor_init_rts
	cpx	#$05
	bcs	actor_init_rts ; if (x < 5) {
		lda	eng_level_h
		jsr	tbljmp
		.addr	actor_init_v
		.addr	actor_init_h
	; }
; ----------------------------
actor_init_h:
	ldy	disp_dir_player
	lda	pos_x_lo_screen_left
	clc
	adc	eng_enemy_screen_margin_lo-1, y
	and	#ENG_MAP_BOUND_MASK
	sta	state_actor_screen_lo
	lda	pos_x_hi_screen_left
	adc	eng_enemy_screen_margin_hi-1, y
	sta	state_actor_screen_hi
	cmp	#ENG_HSCROLL_COLWIDTH
	bcs	actor_init_rts

actor_init_h_nocheck:
	lda	eng_inner_space
	cmp	#2
	beq	actor_init_rts
	ldx	#0
	stx	zp_addr_00
	:
		cpx	state_actor_screen_hi
		beq	:+
		lda	zp_addr_00
		tay
		clc
		adc	(course_actor_addr), y
		sta	zp_addr_00
		inx
		jmp	:-
	:

	ldy	zp_addr_00
	lda	(course_actor_addr), y
	sta	zp_addr_00+1
	ldx	#$FF
	dey
	:
		iny
		iny
		inx
		inx
		cpx	zp_addr_00+1
		bcc	:+ ; if () {
			ldx	actor_index
		actor_init_rts:
			rts
		: ; }

		lda	(course_actor_addr), y
		bmi	:--
		iny
		lda	(course_actor_addr), y
		dey
		and	#$F0
		cmp	state_actor_screen_lo
		bne	:--

	ldx	actor_index
	sta	pos_x_lo_actor, x
	lda	state_actor_screen_hi
	sta	pos_x_hi_actor, x
	iny
	lda	(course_actor_addr), y
	dey
	asl	a
	asl	a
	asl	a
	asl	a
	sta	pos_y_lo_actor, x
	lda	#0
	sta	pos_y_hi_actor, x
	jmp	actor_init_do
; ----------------------------
actor_init_v:
	lda	frame_count
	and	#$01
	tay
	iny
	lda	ppu_scroll_req
	beq	:+ ; if (ppu_scroll_req) {
		and	#$03
		eor	#$03
		tay
	: ; }
	lda	pos_y_lo_screen
	clc
	adc	eng_enemy_screen_margin_lo-1, y
	and	#ENG_MAP_BOUND_MASK
	sta	state_actor_screen_lo
	lda	pos_y_hi_screen
	adc	eng_enemy_screen_margin_hi-1, y
	sta	state_actor_screen_hi
	cmp	#ENG_HSCROLL_COLWIDTH
	bcs	actor_init_rts

actor_init_v_nocheck:
	ldx	#0
	stx	zp_work+$00
	: ; for (x = 0; x != state_actor_screen_hi; x++) {
		cpx	state_actor_screen_hi
		beq	:+

		lda	zp_work+$00
		tay
		clc
		adc	(course_actor_addr), y
		sta	zp_work+$00

		inx
		jmp	:-
	: ; }

	ldy	zp_work+$00
	lda	(course_actor_addr), y
	sta	zp_work+$01
	ldx	#$FF
	dey
	: ; for (x = 0xFF; course_actor_addr[y] < 0 && ((course_actor_addr[y+1] * 16) != zp_work[5]); x += 2) {
		iny
		iny
		inx
		inx
		cpx	zp_work+$01
		bcc	:+ ; if (x >= zp_work[1]) {
			ldx	actor_index

		enemy_state_null_rts:
			rts
		:

		lda	(course_actor_addr), y
		bmi	:--
		iny
		lda	(course_actor_addr), y
		dey
		asl	a
		asl	a
		asl	a
		asl	a
		cmp	state_actor_screen_lo
		bne	:--
	; }

	ldx	actor_index
	sta	pos_y_lo_actor, x
	lda	state_actor_screen_hi
	sta	pos_y_hi_actor, x
	iny
	lda	(course_actor_addr), y
	dey
	and	#ENG_MAP_BOUND_MASK
	sta	pos_x_lo_actor, x
	lda	#0
	sta	pos_x_hi_actor, x
; ----------------------------
actor_init_do:
	sta	eng_enemy_door_tbl, x
	sty	zp_addr_0C
	lda	(course_actor_addr), y
	and	#$3F
	cmp	#$30
	bcs	:+
	lda	eng_level_h
	beq	:+ ; if (eng_level_h) {
		jsr	motion_side_get
		lda	zp_byte_0F
		adc	#$18
		cmp	#$30
		bcc	enemy_state_null_rts
	: ; }

	ldy	zp_addr_0C
	lda	(course_actor_addr), y
	cmp	#$5C
	bcs	:+
	cmp	#$45
	bcc	:+ ; if (course_actor_addr[zp_addr_0C] >= 0x45 && course_actor_addr[zp_addr_0C] < 0x5C) {
		sta	eng_swarm_type
		rts
	: ; }

	ora	#$80
	sta	(course_actor_addr), y
	cmp	#$DC
	and	#$7F
	bcc	:+ ; if () {
		and	#$3F
		sta	eng_enemy_door_tbl, x
	: ; }

	sta	obj_id_actor, x
	tya
	sta	eng_actor_data_offset, x
	inc	proc_id_actor, x
	lda	obj_id_actor, x
	jsr	tbljmp
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_stationary
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_bobomb
	.addr	actor_init_base
	.addr	actor_init_albatross_right
	.addr	actor_init_albatross_left
	.addr	actor_init_base
	.addr	actor_init_stationary
	.addr	actor_init_beezo_dive
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_trouter
	.addr	actor_init_base
	.addr	actor_init_spawnjar
	.addr	actor_init_spawnjar
	.addr	actor_init_phanto
	.addr	actor_init_cobrat
	.addr	actor_init_cobrat
	.addr	actor_init_pokey
	.addr	actor_init_base
	.addr	actor_init_birdo
	.addr	actor_init_mouser
	.addr	actor_init_base
	.addr	actor_init_tryclyde
	.addr	actor_init_base
	.addr	actor_init_stationary
	.addr	actor_init_base
	.addr	actor_init_stationary
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_whalespout
	.addr	actor_init_base
	.addr	actor_init_fryguy
	.addr	actor_init_fryguy
	.addr	actor_init_wart
	.addr	actor_init_doormask_boss
	.addr	actor_init_sparks
	.addr	actor_init_sparks
	.addr	actor_init_sparks
	.addr	actor_init_sparks
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_base
	.addr	actor_init_waterfall_log
	.addr	actor_init_base
	.addr	actor_init_key
	.addr	actor_init_base
	.addr	actor_init_stationary
	.addr	actor_init_stationary
	.addr	actor_init_base
	.addr	actor_init_doormask
	.addr	actor_init_doormask
	.addr	actor_init_miscpickup
	.addr	actor_init_miscpickup
	.addr	actor_init_miscpickup
; -----------------------------------------------------------
