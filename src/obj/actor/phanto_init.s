.include	"mem.i"



PHANTO_VELOC_X		= 12

; -----------------------------------------------------------
	.export	actor_init_phanto
actor_init_phanto:
	jsr	actor_init_base
	lda	#PHANTO_VELOC_X
	sta	veloc_x_actor, x
	rts
