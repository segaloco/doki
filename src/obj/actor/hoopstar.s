.include	"actor.i"

.include	"mem.i"



; -----------------------------------------------------------
actor_hoopstar_veloc_y:
	.byte   <-6, 12

actor_hoopstar_attrs:
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
; ----------------------------
	.export actor_proc_hoopstar
actor_proc_hoopstar:
	jsr	eng_enemy_damage_check
	inc	timer_ani_actor, x
	jsr	actor_timer_held_tick
	jsr	render_actor_base
	jsr	actor_timer_throw_tick
	lda	#0
	sta	veloc_x_actor, x
	jsr	col_player_climb_box
	ldy	eng_actor_tbl_477, x
	bcc	:+
	lda	pos_y_lo_actor, x
	cmp	pos_y_lo_screen
	lda	pos_y_hi_actor, x
	sbc	pos_y_hi_screen
	beq	:+++
	asl	a
	rol	a
	and	#%00000001
	bpl	:++ ; if (!col_player_climb_box() || (actor.pos_y - screen.pos_y) != 0) {
		: ; if () {
			tya
			eor	#1
		: ; }

		sta	eng_actor_tbl_477, x
		tay
	: ; }

	lda	actor_hoopstar_veloc_y, y
	sta	veloc_y_actor, x
	lda	actor_hoopstar_attrs, y
	sta	attr_actor, x

	jsr	motion_side_get
	lda	zp_byte_0F
	adc	#$10
	cmp	#$20
	bcs	:+ ; if ((zp_byte_0F + 0x10) < 0x20) {
		asl	veloc_y_actor, x
	: ; }

	jmp	motion_y
