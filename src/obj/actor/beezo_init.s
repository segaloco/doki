.include	"system/ppu.i"

.include	"tunables.i"
.include	"modes.i"
.include	"actor.i"

.include	"mem.i"



; -----------------------------------------------------------
eng_actor_beezo_offset_x:
	.byte   $FE, $00

eng_actor_beezo_veloc_tbl:
	.byte	$12, $16, $1A, $1E
	.byte	$22, $26, $2A, $2D
	.byte	$30, $32, $34, $37
	.byte	$39, $3B, $3D, $3E
; ----------------------------
	.export actor_init_beezo_dive, actor_init_beezo_dive_end
actor_init_beezo_dive:
	jsr	actor_init_base
	ldy	disp_dir_player
	lda	pos_x_lo_screen_left
	adc	eng_actor_beezo_offset_x-1, y
	sta	pos_x_lo_actor, x
	lda	pos_x_hi_screen_left
	adc	#0
	sta	pos_x_hi_actor, x

actor_init_beezo_dive_end:
	lda	pos_y_hi_player
	bpl	:+ ; if (pos_y_hi_player & 0x80) {
		ldy	#0
		beq	:++
	: ; } else {
		lda	pos_y_lo_player
		sec
		sbc	pos_y_lo_screen
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		tay
	: ; }

	lda	eng_actor_beezo_veloc_tbl, y
	sta	veloc_y_actor, x
	rts
