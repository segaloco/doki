.include	"actor.i"

.include	"mem.i"



ENG_ACTOR_BIGHEAD_VELOC_X	= $1C

; -----------------------------------------------------------
eng_actor_bighead_veloc_x:
	.byte   ENG_ACTOR_BIGHEAD_VELOC_X
	.byte	<-(ENG_ACTOR_BIGHEAD_VELOC_X)
; ----------------------------
	.export eng_actor_bighead_routine, sub_ABF8
eng_actor_bighead_routine:
	jsr	col_actor_tile_proc
	jsr	actor_timer_held_tick

	lda	collision_actor, x
	and	#(actor_collisions::left|actor_collisions::right)
	beq	:+ ; if (collision_actor[x] & (left|right)) {
	sub_ABF8:
		lda	#$40
		sta	byte_602
		jmp	actor_proc_lamp_end
	: ; } else {
		lda	collision_actor, x
		and	#(actor_collisions::floor)
		beq	:+ ; if (collision_actor[x] & floor) {
			jsr	actor_proc_base_veloc_reset
		: ; }

		jsr	render_actor_base
		ldy	disp_dir_actor, x
		lda	eng_actor_bighead_veloc_x-1, y
		sta	veloc_x_actor, x
		jmp	eng_actor_domove
	; }