.include	"tunables.i"
.include	"actor.i"

.include	"mem.i"



; -----------------------------------------------------------
actor_sparks_veloc_x:
	.byte	<-16, <-32

actor_sparks_veloc_y:
	.byte	<-16, <-32
	.byte	16, 32
; ----------------------------
	.export actor_init_sparks, actor_proc_sparks
actor_init_sparks:
	jsr	actor_init_base

	ldy	obj_id_actor, x
	lda	actor_sparks_veloc_x-actor::spark_1, y
	sta	veloc_x_actor, x
	lda	actor_sparks_veloc_y-actor::spark_1, y
	sta	veloc_y_actor, x
	rts
; ----------------------------
actor_sparks_col_data:
	.byte   (actor_collisions::ceil|actor_collisions::floor)
	.byte	(actor_collisions::left|actor_collisions::right)

actor_sparks_veloc_x_idx:
	.byte	0, 10
; ----------------------------
actor_proc_sparks:
	jsr	eng_enemy_damage_check
	jsr	actor_timer_ani_inc
	jsr	render_actor_base

	lda	pos_x_lo_actor, x
	ora	pos_y_lo_actor, x
	and	#%00001111
	bne	:++ ; if (!((actor.pos_x | actor.pos_y) % 32)) {
		jsr	col_actor_tile_proc_b

		ldy	eng_actor_tbl_477, x
		lda	collision_actor, x
		and	actor_sparks_col_data, y
		beq	:+
		lda	actor_sparks_col_data, y
		eor	#%00001111
		and	collision_actor, x
		beq	:++ ; if ((actor.collision & col_data[actor.tbl_477]) && (actor.collision & (col_data[actor.tbl_477] ^ 0xF))) {
			tya
			eor	#1
			sta	eng_actor_tbl_477, x
			tay

		sub_9EE0:
			txa
			clc
			adc	actor_sparks_veloc_x_idx, y
			tay

			lda	veloc_x_actor, y
			eor	#<-1
			adc	#1
			sta	veloc_x_actor, y

			rts
		: ; } else {
			tya
			eor	#1
			sta	eng_actor_tbl_477, x
			jsr	sub_9EE0
		; }
	: ; }

	lda	eng_actor_tbl_477, x
	bne	:+ ; if (!eng_actor_tbl_477[x]) {
		jmp	motion_x
	: ; } else {
		jmp	motion_y
	; }
