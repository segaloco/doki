.include	"mem.i"
.include	"actor.i"

actor_throw_pos_y_hi	= zp_byte_07

; -----------------------------------------------------------
	.export actor_throw
actor_throw:
	lda	player_dir
	eor	#1
	tay
	iny
	sty	disp_dir_actor, x
	lda	pos_x_lo_player
	sta	pos_x_lo_actor, x
	lda	pos_x_hi_player
	sta	pos_x_hi_actor, x
	lda	pos_y_hi_player
	sta	actor_throw_pos_y_hi
	lda	pos_y_lo_player

	ldy	col_box_actor, x
	cpy	#3
	beq	:+
	cpy	#2
	beq	:+
	sbc	#14
	bcs	:+ ; if (actor.col_box.not_in(3, 2) && (player.pos_y.lo < 14)) {
		dec	actor_throw_pos_y_hi
	: ; }

	ldx	eng_player_ani_frame
	cpx	#4
	clc
	bne	:+ ; if (player.ani_frame == 4) {
		adc	#8
	: ; }

	php

	ldx	actor_index
	ldy	timer_actor_held, x
	clc
	adc	pos_y_lo_throw, y
	sta	pos_y_lo_actor, x

	lda	actor_throw_pos_y_hi
	adc	pos_y_hi_throw, y

	plp
	adc	#0
	sta	pos_y_hi_actor, x
	cpy	#5
	bcs	:++
	lda	obj_id_actor, x
	cmp	#actor::vegetable_small
	bcs	:++ ; if (actor.timer_held < 5 && actor.id < vegetable_small) {
		lda	timer_actor_stun, x
		bne	:+ ; if (actor.timer_stun == 0) {
			inc	timer_ani_actor, x
		: ; }

		asl	attr_actor, x
		sec
		ror	attr_actor, x
	: ; }

	jsr	lib_calc_actor_pos
	jmp	render_actor_base
