.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"mem.i"
.include	"actor.i"
.include	"modes.i"
.include	"tunables.i"

	.byte	16, <-16

	.export actor_proc_life_pow
actor_proc_life_pow:
	jsr	actor_proc_life_pow_b

actor_proc_life_pow_rts:
	rts

actor_proc_life_pow_b:
	lda	timer_actor_held, x
	beq	:+ ; if (actor.timer_held != 0) {
		pla
		pla
		jmp	actor_throw
	: ; }

	jsr	render_actor_base
	lda	obj_id_actor, x
	cmp	#actor::pow_block
	bcs	:+ ; if (this.obj_id < pow_block) {
		jsr	col_actor_tile_proc_b

		jmp	:++
	: ; } else {
		jsr	col_actor_tile_proc
	: ; }

actor_proc_life_pow_do:
	lda	timer_actor_throw, x
	beq	actor_proc_life_pow_rts ; if (actor.timer_throw != 0) {
		jsr	eng_actor_domove
		pla
		pla

		lda	collision_actor, x
		pha
		and	#(actor_collisions::left|actor_collisions::right)
		beq	:+ ; if (actor.collision & (left|right)) {
			lda	#0
			sta	veloc_x_actor, x
			lda	pos_x_lo_actor, x
			adc	#8
			and	#%11110000
			sta	pos_x_lo_actor, x
			lda	eng_level_h
			beq	:+ ; if (eng_level_h) {
				lda	pos_x_hi_actor, x
				adc	#0
				sta	pos_x_hi_actor, x
			; }
		: ; } 
		
		pla
		ldy	veloc_y_actor, x
		bmi	:++++
		and	#(actor_collisions::floor)
		beq	:++++ ; if (actor.veloc_y >= 0 && (actor.collision & floor)) {
			lda	zp_byte_0E
			cmp	#$10
			bne	:+
			lda	veloc_x_actor, x
			beq	:+ ; if (zp_byte_0E == 0x10 && actor.veloc_x != 0) {
				lda	#20
				jmp	actor_proc_base_veloc_apply
			: lda	obj_id_actor, x
			cmp	#actor::pow_block
			bne	:+ ; } else if (actor.obj_id == pow_block) {
				lda	#32
				sta	eng_timer_pow_quake
				lda	#4
				sta	apu_dpcm_queue
				jmp	actor_proc_pow
			: lda	veloc_y_actor, x
			cmp	#22
			bcc	:+ ; } else if (actor.veloc_y >= 22) {
				jsr	actor_proc_base_veloc_reset
				lda	#$F5
				jmp	actor_proc_base_motion_do
			: ; } else {
				jsr	actor_proc_base_veloc_reset
				lda	zp_actor_work_byte_1, x
				jsr	eng_item_lift_replace
				jmp	actor_destroy
			; }
		: ; }
	; }

actor_proc_life_pow_b_rts:
	rts
; -----------------------------------------------------------
actor_door_subsp_branches:
	.byte	CPU_OPCODE_BCC, CPU_OPCODE_BCS, CPU_OPCODE_BCC
; ----------------------------
	.export actor_proc_door_subsp
actor_proc_door_subsp:
	lda	#ACTOR_DIR_LEFT
	sta	disp_dir_actor, x
	ldy	eng_timer_subspace_2
	beq	:+
	lda	frame_count
	and	#%00000011
	bne	:+
	ldy	proc_id_player
	cpy	#player_state::death
	beq	:+ 
	dec	eng_timer_subspace_2
	bne	:+ ; if (eng_timer_subspace_2 && !(frame_count % 4) && player.state != death && !(--eng_timer_subspace_2)) {
		sta	eng_inner_space
		jsr	eng_map_reset
		jmp	eng_actor_door_subspace_pop_stack
	: ; } else {
		lda	eng_timer_actor_2, x
		bne	actor_proc_life_pow_b_rts
		lda	eng_timer_subspace
		beq	:+ ; if (eng_timer_subspace) {
			dec	eng_timer_subspace
			bne	:+ ; if (!(--eng_timer_subspace)) {
				jmp	actor_proc_lamp_end
			: ; }
		; }

		lda	attr_actor, x
		ora	#(actor_attrs::bit6)
		sta	attr_actor, x
		ldy	eng_door_timer_ani
		lda	actor_door_subsp_pos_x, y
		ldy	#(ACTOR_DIR_RIGHT)-1
		asl	a
		bcc	:+ ; if (actor_door_subsp_pos_x[eng_door_timer_ani] < 0) {
			iny
			sty	disp_dir_actor, x
		: ; }

		lda	actor_door_subsp_branches, y
		sta	:+
		lda	actor_door_subsp_branches+1, y
		sta	:++++
		lda	eng_door_timer_ani
		beq	:+++ ; if (eng_door_timer_ani) {
			lda	zp_byte_F4
			pha

			jsr	actor_sprite_alloc
			cpy	zp_byte_F4
			: bcc	:+ ; if (y ? zp_byte_F4) {
				sty	zp_byte_F4
			: ; }

			lda	#$7A
			jsr	render_actor_base_draw
			ldy	zp_byte_F4
			lda	oam_buffer+OBJ_POS_H+(OBJ_SIZE*1), y
			sec
			sbc	#4
			sta	oam_buffer+OBJ_POS_H+(OBJ_SIZE*1), y
			sta	oam_buffer+OBJ_POS_H+(OBJ_SIZE*3), y
			pla
			sta	zp_byte_F4
		: ; }

		jsr	actor_sprite_alloc
		cpy	zp_byte_F4
		: bcs	:+ ; if (y ? zp_byte_F4) {
			sty	zp_byte_F4
		: ; }

		lda	eng_door_timer_ani
		cmp	#$19
		bcc	:+ ; if (eng_door_timer_ani >= 0x19) {
			ldy	#0
			sty	zp_byte_F4
		: ; }

		lda	#$76
		ldy	eng_actor_tbl_477, x
		beq	:+ ; if (actor.tbl_477) {
			lda	#$7E
		: ; }
		jsr	render_actor_base_draw

		ldx	eng_door_timer_ani
		beq	actor_proc_door_subsp_end ; if (eng_door_timer_ani) {
			inc	eng_door_timer_ani
			ldy	zp_byte_F4

			lda	actor_door_subsp_pos_x, x
			bmi	actor_proc_door_subsp_end ; if (actor_door_subsp_pos_x[eng_door_timer_ani-1] >= 0) {
				clc
				adc	oam_buffer+0*OBJ_SIZE+OBJ_POS_H, y
				sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_H, y
				sta	oam_buffer+2*OBJ_SIZE+OBJ_POS_H, y
				cpx	#$30
				bne	actor_proc_door_subsp_end ; if ((eng_door_timer_ani-1) == 0x30) {
					lda	#0
					sta	eng_door_timer_ani
					jsr	eng_map_reset

					lda	eng_transition_type
					cmp	#transition_state::door
					bne	:+ ; if (eng_transition_type == door) {
						inc	eng_transition_area
						bne	:++
					: ; } else {
						lda	eng_inner_space
						eor	#$02
						sta	eng_inner_space
					: ; }
				; }
			; }
		; }
	; }

	eng_actor_door_subspace_pop_stack: ; if ((eng_timer_subspace_2 && !(frame_count % 4) && player.state != death && !(--eng_timer_subspace_2)) || (!eng_door_timer_ani || actor_door_subsp_pos_x[eng_door_timer_ani-1] < 0 || (eng_door_timer_ani-1) != 0x30)) {
		pla
		pla
		pla
		pla
		pla
		pla
	; }

actor_proc_door_subsp_end:
	ldx	actor_index
	rts
; ----------------------------
actor_door_subsp_pos_x:
	.byte	0, 1, 1, 2, 2, 3, 4, 6
	.byte	8, <-1, <-1, <-1, <-1, <-1, <-1, <-1
	.byte	<-1, <-1, <-1, <-1, <-1, <-1, <-1, <-1
	.byte   <-1, 8, 6, 4, 3, 2, 2, 2
	.byte	2, 1, 1, 1, 1, 1, 0, 0
	.byte	0, 0, 0, 0, 0, 0, 0, 0
	.byte	0
; ----------------------------
	.export actor_door_subsp_spawn_2
	.export actor_door_subsp_spawn_1
	.export actor_door_subsp_spawn_0
actor_door_subsp_spawn_2:
	lda	#2
	bne	:+

actor_door_subsp_spawn_1:
	lda	#1
	bne	:+

actor_door_subsp_spawn_0:
	lda	#0

	: pha
	ldy	#(ENG_ACTOR_MAX)-1
	: ; for (actor of actors) {
		lda	proc_id_actor, y
		beq	:+
		lda	obj_id_actor, y
		cmp	#actor::door_subspace
		bne	:+ ; if (actor.proc_id != null && actor.obj_id == door_subspace) {
			lda	#actor_states::smokepoof
			sta	proc_id_actor, y
			lda	#32
			sta	timer_actor, y
		: ; }

		dey
		bpl	:--
	; }

	jsr	actor_init_shyguy_prj
	bmi	:+ ; if (actor_init_shyguy_prj() >= 0) {
		lda	#0
		sta	eng_door_timer_ani
		sta	eng_timer_subspace
		ldx	zp_addr_00
		pla
		sta	eng_actor_tbl_477, x
		lda	#actor::door_subspace
		sta	obj_id_actor, x
		lda	pos_x_lo_player
		adc	#8
		and	#$F0
		sta	pos_x_lo_actor, x
		lda	pos_x_hi_player
		adc	#0
		sta	pos_x_hi_actor, x
		lda	pos_y_lo_player
		sta	pos_y_lo_actor, x
		lda	pos_y_hi_player
		sta	pos_y_hi_actor, x
		lda	#(actor_attrs::bit6|actor_attrs::bit0)
		sta	attr_actor, x
		ldx	actor_index
		rts
	: ; } else {
		pla
		rts
	; }
