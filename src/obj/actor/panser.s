.include	"actor.i"

.include	"mem.i"



PANSER_OFFSET_X	= 5
PANSER_VELOC_X	= 16

	.export actor_proc_panser_pink
	.export actor_proc_panser_red_grey
; -----------------------------------------------------------
actor_panser_veloc_x:
	.byte	PANSER_VELOC_X, <-(PANSER_VELOC_X)
; ----------------------------
actor_proc_panser_pink:
	lda	timer_ani_actor, x
	asl	a
	bne	:+ ; if (!(this.timer_ani & 0x80)) {
		jsr	actor_init_base_tracking
	: ; }
; ----------------------------
actor_proc_panser_red_grey:
	jsr	col_actor_tile_proc
	lda	collision_actor, x
	pha

	and	#(actor_collisions::floor)
	beq	:+ ; if (this.collision & floor) {
		jsr	actor_proc_base_veloc_reset
	: ; }

	pla
	and	#(actor_collisions::left|actor_collisions::right)
	beq	:+ ; if (this.collision & (left|right)) {
		jsr	motion_dir_flip
	: ; }

	jsr	eng_actor_domove
	lda	#(actor_flags::mirror_ani|actor_flags::cant_lift|actor_flags::harm_top)
	sta	actor_mode, x
	lda	#ACTOR_DIR_LEFT
	sta	disp_dir_actor, x
	jsr	eng_enemy_damage_check

	inc	timer_ani_actor, x
	lda	timer_ani_actor, x
	and	#%00101111
	bne	:+ ; if (!(++this.timer_ani & 0x2F)) {
		lda	#16
		sta	timer_actor, x
	: ; }

	ldy	timer_actor, x
	beq	:+++ ; if (this.timer != 0) {
		cpy	#6
		bne	:++ ; if (this.timer == 6) {
			jsr	actor_init_shyguy_prj_lo
			bmi	:++ ; if (actor_init_shyguy_prj_lo() >= 0) {
				lda	obj_id_actor, x
				pha
				ldx	zp_addr_00
				lda	byte_4BB
				and	#%00001111
				ora	#%10111100
				sta	veloc_y_actor, x
				jsr	motion_side_get
				pla
				cmp	#actor::panser_3
				lda	actor_panser_veloc_x, y
				bcc	:+ ; if (this.id >= panser_3) {
					lda	#0
				: ; }
				sta	veloc_x_actor, x

				lda	pos_x_lo_actor, x
				sbc	#<(PANSER_OFFSET_X)
				sta	pos_x_lo_actor, x
				lda	pos_x_hi_actor, x
				sbc	#>(PANSER_OFFSET_X)
				sta	pos_x_hi_actor, x
				lda	#actor::fireball
				sta	obj_id_actor, x
				jsr	actor_attr_set
				ldx	actor_index
			; }
		: ; }

		lda	attr_actor, x
		ora	#(actor_attrs::h_flip)
		sta	attr_actor, x
		lda	#$AE
		jsr	render_actor_base_draw
		lda	attr_actor, x
		and	#<~(actor_attrs::h_flip)
		sta	attr_actor, x
		rts
	: ; } else {
		jmp	render_actor_base
	; }
