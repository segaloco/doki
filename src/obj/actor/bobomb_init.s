.include	"mem.i"

ENG_BOBOMB_TIMER_1_INIT		= $FF

	.export actor_init_bobomb
actor_init_bobomb:
	jsr	actor_init_base
	lda	#ENG_BOBOMB_TIMER_1_INIT
	sta	timer_actor, x
	rts
