.include	"actor.i"

.include	"mem.i"



; -----------------------------------------------------------
	.export eng_actor_bullet_prj, eng_actor_bullet_prj_lo
eng_actor_bullet_prj:
	jsr	actor_init_shyguy_prj
	jmp	:+

eng_actor_bullet_prj_lo:
	jsr	actor_init_shyguy_prj_lo

	: bmi	:+ ; if (a >= 0) {
		ldy	disp_dir_actor, x
		ldx	zp_addr_00
		lda	actor_bullet_prj_veloc_x-1, y
		sta	veloc_x_actor, x
		lda	#0
		sta	veloc_y_actor, x
		lda	#actor::bullet
		sta	obj_id_actor, x
		jsr	actor_attr_set
		ldx	actor_index
	: ; }

	rts
