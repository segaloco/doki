.include	"actor.i"

.include	"mem.i"



TROUTER_OFFSET_X	= 8

TROUTER_POS_Y_LO_METATILES	= zp_actor_work_byte_2

	.export actor_init_trouter, actor_proc_trouter
; -----------------------------------------------------------
actor_init_trouter:
	jsr	actor_init_stationary
	lda	pos_x_lo_actor, x
	adc	#TROUTER_OFFSET_X
	sta	pos_x_lo_actor, x
	lda	pos_y_lo_actor, x
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	TROUTER_POS_Y_LO_METATILES, x
	lda	#128
	sta	timer_actor, x

actor_init_trouter_rts:
	rts
; ----------------------------
actor_trouter_veloc_y:
	.byte	<-84, <-82, <-79, <-75
	.byte	<-72, <-68, <-64, <-60
	.byte	<-56, <-52, <-46, <-40

actor_trouter_offset_y_lo:
	.byte   <-110, <-22
; ----------------------------
actor_proc_trouter:
	jsr	eng_enemy_damage_check
	inc	timer_ani_actor, x
	jsr	actor_timer_throw_tick
	jsr	actor_timer_held_tick
	lda	#(actor_attrs::bit3|actor_attrs::bit0)
	ldy	veloc_y_actor, x
	bmi	:+ ; if (actor.veloc_y >= 0) {
		lda	#(actor_attrs::v_flip|actor_attrs::bit3|actor_attrs::bit0)
	: ; }

	sta	attr_actor, x
	ldy	eng_level_h
	lda	pos_y_lo_actor, x
	cmp	actor_trouter_offset_y_lo, y
	bcc	:+ ; if (pos_y_lo_actor >= actor_trouter_offset_y_lo) {
		ldy	timer_actor, x
		bne	actor_init_trouter_rts
		sta	pos_y_lo_actor, x
		ldy	TROUTER_POS_Y_LO_METATILES, x
		lda	actor_trouter_veloc_y, y
		sta	veloc_y_actor, x
		lda	#192
		sta	timer_actor, x
	: ; }

	jsr	eng_actor_dophys
	inc	veloc_y_actor, x
	jmp	render_actor_base
