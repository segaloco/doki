.include	"mem.i"
.include	"actor.i"

SPAWNJAR_OFFSET_X	= 7
SPAWNJAR_OFFSET_Y	= 4
SPAWNJAR_VELOC_Y	= <-8

	.export actor_proc_spawnjar
actor_proc_spawnjar:
	jsr	col_actor_tile_proc
	and	#(actor_collisions::left|actor_collisions::right)
	bne	:+ ; if (col_actor_tile_proc() & (left|right)) {
		jmp	actor_destroy
	: ; } else {
		inc	timer_ani_actor, x
		lda	timer_ani_actor, x
		asl	a
		bne	:+ ; if ((++this.timer_ani << 1) == 0) {
			jsr	actor_init_shyguy_prj_lo
			bmi	:+ ; if (actor_init_shyguy_prj_lo() >= 0) {
				ldy	zp_addr_00
				lda	pos_x_lo_actor, y
				sec
				sbc	#(SPAWNJAR_OFFSET_X)-1
				sta	pos_x_lo_actor, y
				lda	pos_y_lo_actor, y
				sbc	#<(SPAWNJAR_OFFSET_Y)
				sta	pos_y_lo_actor, y
				lda	pos_y_hi_actor, y
				sbc	#>(SPAWNJAR_OFFSET_Y)
				sta	pos_y_hi_actor, y
				lda	#$1A
				sta	eng_actor_tbl_480, y
				lda	#SPAWNJAR_VELOC_Y
				sta	veloc_y_actor, y

				lda	obj_id_actor, x
				cmp	#actor::spawnjar_bobomb
				bne	:+ ; if (this.obj_id == spawnjar_bobomb) {
					lda	#actor::bobomb
					sta	obj_id_actor, y
					lda	veloc_x_actor, y
					asl	a
					sta	veloc_x_actor, y
					lda	#255
					sta	timer_actor, y
				: ; }
			; }
		; }

		rts
	; }
