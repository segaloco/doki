.include	"actor.i"

.include	"mem.i"



NINJI_JUMP_VELOC_Y			= 40
ENG_ACTOR_NINJI_FRAME_MASK	= %00111111

; -----------------------------------------------------------
actor_ninji_jump_veloc_y:
	.byte	<-(NINJI_JUMP_VELOC_Y-16)
	.byte	<-(NINJI_JUMP_VELOC_Y+8)
	.byte	<-(NINJI_JUMP_VELOC_Y)
	.byte	<-(NINJI_JUMP_VELOC_Y+8)
; ----------------------------
	.export actor_proc_ninji_jump, actor_proc_ninji_run
actor_proc_ninji_jump:
	lda	collision_actor, x
	and	#actor_collisions::floor
	beq	:++ ; if (actor.collision & floor) {
		lda	timer_actor_throw, x
		bne	:+ ; if (actor.timer_throw == 0) {
			sta	veloc_x_actor, x
		: ; }

		txa
		asl	a
		asl	a
		asl	a
		adc	frame_count
		and	#ENG_ACTOR_NINJI_FRAME_MASK
		bne	:+ ; if (!((x*8)+frame_count & frame_mask)) {
			lda	timer_ani_actor, x
			and	#%11000000
			asl	a
			rol	a
			rol	a
			tay
			lda	actor_ninji_jump_veloc_y, y
			sta	veloc_y_actor, x
			jsr	motion_y
		; }
	: ; }

	jmp	actor_proc_base
; -----------------------------------------------------------
actor_proc_ninji_run:
	lda	collision_actor, x
	and	#(actor_collisions::floor)
	beq	:+
	lda	pos_y_lo_player
	clc
	adc	#16
	cmp	pos_y_lo_actor, x
	bne	:+ ; if ((actor.collision & floor) && (player.pos_y + 16) == this.pos_y) {
		jsr	motion_side_get
		iny
		tya
		cmp	disp_dir_actor, x
		bne	:+
		lda	zp_byte_0F
		adc	#NINJI_JUMP_VELOC_Y
		cmp	#2*(NINJI_JUMP_VELOC_Y)
		bcs	:+ ; if (actor.disp_dir == ++y && (zp_byte_0F + 0x28) >= 0x50) {
			lda	#<-(NINJI_JUMP_VELOC_Y)
			sta	veloc_y_actor, x
			jsr	motion_y
		; }
	: ; }

	jmp	actor_proc_base
