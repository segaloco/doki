.include	"actor.i"

.include	"mem.i"



ENG_ACTOR_FIREBALL_HAS_COLLISION	= zp_actor_work_byte_1

; -----------------------------------------------------------
	.export actor_proc_fireball
actor_proc_fireball:
	jsr	col_actor_tile_proc
	jsr	actor_proc_fire_base
	jsr	eng_enemy_damage_check
	jsr	render_actor_base

	lda	ENG_ACTOR_FIREBALL_HAS_COLLISION, x
	bne	:+ ; if (!ENG_ACTOR_FIREBALL_HAS_COLLISION) {
		jmp	eng_actor_domove
	: ; } else {
		lda	collision_actor, x
		and	#(actor_collisions::left|actor_collisions::right)
		beq	:+ ; if (actor.collision & (left|right))) {
			jsr	actor_proc_lamp_end
		: ; }

		jmp	eng_actor_dophys
	; }
