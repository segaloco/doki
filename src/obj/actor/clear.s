.include	"mem.i"
.include	"actor.i"
.include	"modes.i"
.include	"tunables.i"

	.export eng_actors_pow_clear, eng_actors_clear
eng_actors_pow_clear:
	lda	#0

eng_actors_clear:
	sta	zp_byte_00

	ldx	#(ENG_ACTOR_MAX)-1
	: ; for (actor of actors) {
		lda	proc_id_actor, x
		cmp	#actor_states::normal
		bne	:++++
		lda	zp_byte_00
		beq	:+++
		lda	obj_id_actor, x
		cmp	#actor::bomb
		beq	:+
		cmp	#actor::vegetable_small
		bcs	:++++
		: ; if (actor.proc_id == normal && zp_byte_00 && (actor.obj_id == bomb || actor.obj_id < vegetable_small)) {
			lda	player_item_held
			beq	:+
			cpx	player_item_held_id
			bne	:+ ; if (player.item_held && this == actor_held) {
				lda	#0
				sta	player_item_held
			: ; }

			stx	zp_byte_0E
			jsr	actor_proc_lamp_end
			ldx	zp_byte_0E
			jmp	:++
		: ; } else if (actor.proc_id == normal && !zp_byte_00) {
			lda	collision_actor, x
			beq	:+
			lda	#<-40
			sta	veloc_y_actor, x
			lda	collision_actor, x
			ora	#(actor_collisions::damage)
			sta	collision_actor, x
		: ; }

		dex
		bpl	:-----
	; }

	ldx	actor_index
	rts
