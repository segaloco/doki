.include	"mem.i"
.include	"actor.i"
.include	"modes.i"

	.export actor_proc_fire_bomb
	.export actor_proc_proj_egg
	.export actor_proc_fire_base
; -----------------------------------------------------------
actor_proc_fire_bomb:
	jsr	actor_proc_proj_base

actor_proc_fire_base:
	asl	attr_actor, x
	lda	frame_count
	lsr	a
	lsr	a
	lsr	a
	ror	attr_actor, x
	rts
; ----------------------------
	bne	actor_proc_proj_base

actor_proc_proj_egg:
	jsr	col_actor_tile_proc

actor_proc_proj_base:
	jsr	eng_enemy_damage_check
	jsr	actor_timer_held_tick
	lda	zp_actor_work_byte_2, x
	ora	timer_actor_throw, x
	beq	:+ ; if (zp_actor_work_byte_2[x] | actor.timer_throw) {
		jmp	render_actor
	: ; } else {
		lda	veloc_y_actor, x
		bpl	:+ ; if (actor.veloc_y < 0) {
			sta	zp_actor_work_byte_2, x
		: ; }

		lda	collision_actor, x
		and	#(actor_collisions::left|actor_collisions::right)
		beq	:++ ; if (actor.collision & (left|right)) {
			sta	zp_actor_work_byte_2, x

			lda	obj_id_actor, x
			cmp	#actor::bullet
			bne	:+ ; if (actor.obj_id == bullet) {
				lda	#actor_states::dead
				sta	proc_id_actor, x
				inc	pos_y_lo_actor, x
				inc	pos_y_lo_actor, x
			: ; }

			jsr	motion_dir_flip
			jsr	actor_proc_base_veloc_reverse
			jsr	actor_proc_base_veloc_reverse
		: ; }

		jsr	motion_x
		jmp	render_actor_base
	; }
