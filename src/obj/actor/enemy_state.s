.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"modes.i"
.include	"actor.i"
.include	"math.i"
.include	"tunables.i"

	.export state_actor_dead, actor_erase
	.export	state_actor_blockfizz, state_actor_sinking
	.export	eng_boom_pos_v
	.export	state_actor_bombboom, state_actor_smokepoof
	.export state_actor_sand
	.export render_actor
	.export eng_actor_domove
	.export eng_actor_move_v
state_actor_dead:
	jsr	eng_actor_dobounds
	lda	proc_id_actor, x
	bne	:++
	lda	eng_enemy_door_tbl, x
	beq	:+ ; if (this.proc_id == null && eng_enemy_door_tbl[x]) {
	state_actor_dead_door:
		sta	eng_boss_beaten
		jsr	eng_actors_clear
		jsr	actor_init_swarm_stop
		lda	#2
		sta	apu_music_queue_2
		lda	eng_tbl_maskdoors, x
		sta	pos_x_hi_actor, x
		lda	#$80
		sta	pos_x_lo_actor, x
		asl	a
		sta	pos_y_hi_actor, x
		lda	#$B0
		sta	pos_y_lo_actor, x
		lda	#(actor_attrs::bit6|actor_attrs::bit0)
		sta	attr_actor, x
		sta	actor_mode, x
		jmp	actor_proc_lamp_end
	: lda	obj_id_actor, x
	cmp	#actor::bullet
	beq	:+
	inc	eng_actor_heart_counter
	ldy	eng_actor_heart_counter
	cpy	#ENG_HEART_COUNT_ENEMIES
	bcc	:+ ; } else if (this.proc_id == null && this.id != bullet && ++eng_actor_heart_counter >= 8) {
		lda	#0
		sta	eng_actor_heart_counter
		lda	#actor_states::normal
		sta	proc_id_actor, x
		sta	attr_actor, x
		lda	#(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
		sta	actor_mode, x
		lda	#actor::heart
		sta	obj_id_actor, x
		lda	pos_y_lo_actor, x
		sbc	#<(ENG_HEART_SPRITE_OFFSET)
		sta	pos_y_lo_actor, x
		lda	pos_y_hi_actor, x
		sbc	#>(ENG_HEART_SPRITE_OFFSET)
		sta	pos_y_hi_actor, x

	actor_erase:
		lda	#$FF
		sta	eng_actor_data_offset, x
		rts
	: ; } else {
		asl	attr_actor, x
		sec
		ror	attr_actor, x

	render_actor:
		jsr	render_actor_base

	eng_actor_domove:
		lda	timer_actor_shake, x
		bne	:+ ; if (!timer_actor_shake[x]) {
			jsr	motion_x
		: ; }

	eng_actor_move_v:
		jsr	motion_y
		lda	veloc_y_actor, x
		bmi	:+
		cmp	#ENG_VELOC_Y_MAX+1
		bcs	:++
		: ; if (veloc_y_actor[x] < 0 || veloc_y_actor[x] < ENG_VELOC_Y_MAX+1) {
			inc	veloc_y_actor, x
			inc	veloc_y_actor, x
		: ; }

		rts
	; }
; -----------------------------------------------------------
state_actor_blockfizz:
	jsr	eng_actor_dobounds

	lda	timer_actor, x
	beq	:++ ; if (this.timer != 0) {
		tay

		lsr	a
		lsr	a
		and	#ACTOR_DIR_RIGHT
		sta	disp_dir_actor, x
		lda	#(actor_attrs::bit0)
		sta	attr_actor, x
		sta	actor_mode, x

		lda	#$3C
		cpy	#12
		bcc	:+ ; if (this.timer < 12) {
			lda	#$3E
		: ; }

		jmp	render_actor_base_draw
	: ; }

state_actor_blockfizz_destroy:
	jmp	actor_destroy
; -----------------------------------------------------------
state_actor_sinking:
	jsr	eng_actor_dobounds
	jsr	eng_enemy_damage_check

	lda	timer_actor_held, x
	beq	:+ ; if (this.timer_held != 0) {
		lda	#actor_states::normal
		sta	proc_id_actor, x
		rts
	: lda	timer_actor, x
	beq	state_actor_blockfizz_destroy
	lda	obj_id_actor, x
	cmp	#actor::vegetable_small
	bcs	:+ ; } else if (this.timer != 0 && this.id < vegetable_small) {
		jsr	actor_timer_ani_inc
		lda	frame_count
		and	#MOD_4
		sta	timer_actor_shake, x
		lda	frame_count
		and	#$10
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		adc	#1
		sta	disp_dir_actor, x
	: ; }

	jsr	actor_proc_base_disp
	jmp	eng_col_actor
; -----------------------------------------------------------
eng_boom_pos_h:
	.byte	$F8, $00, $F8, $00, $08, $10, $08, $10

	.export	eng_boom_pos_v
eng_boom_pos_v:
	.byte	$F8, $F8, $08, $08, $F8, $F8, $08, $08
; ----------------------------
state_actor_bombboom:
	jsr	eng_actor_dobounds

	lda	zp_byte_EE
	ora	zp_byte_EF
	bne	state_actor_blockfizz_destroy
	lda	timer_actor, x
	beq	state_actor_blockfizz_destroy ; if (!(zp_byte_EE | zp_byte_EF) && this.timer != 0) {
		cmp	#26
		bcs	:+
		sbc	#17
		bmi	:+ ; if (this.timer < 26 && this.timer >= 17) {
			tay
			jsr	eng_actor_boom_blocks
		: ; }

		lda	#$60
		sta	zp_work+$00
		ldx	#0
		ldy	#$40
		: ; for (x = 0, y = 0x40, i = 0x60; x < 8; x++, y += 4, i += 2) {
			lda	pos_y_rel_actor
			clc
			adc	eng_boom_pos_v, x
			sta	oam_buffer+OBJ_POS_V, y
			lda	pos_x_rel_actor
			clc
			adc	eng_boom_pos_h, x
			sta	oam_buffer+OBJ_POS_H, y
			lda	#(obj_attr::priority_high|obj_attr::color1)
			sta	oam_buffer+OBJ_ATTR, y
			lda	zp_work+$00
			sta	oam_buffer+OBJ_CHR_NO, y
			clc
			adc	#2
			sta	zp_work+$00
			iny
			iny
			iny
			iny

			inx
			cpx	#8
			bne	:-
		; }

		ldx	actor_index
		jmp	eng_col_actor

	state_actor_bombboom_rts:
		rts
	; }
; ----------------------------
eng_boom_block_off_x_lo:
	.byte   $FB, $08, $15
	.byte	$FB, $08, $15
	.byte	$FB, $08, $15

eng_boom_block_off_x_hi:
	.byte   $FF, $00, $00
	.byte   $FF, $00, $00
	.byte   $FF, $00, $00

eng_boom_block_off_y_lo:
	.byte   $FC, $FC, $FC
	.byte	$08, $08, $08
	.byte	$14, $14, $14

eng_boom_block_off_y_hi:
	.byte   $FF, $FF, $FF
	.byte	$00, $00, $00
	.byte	$00, $00, $00

eng_boom_block_zp_addr_07_hi:
	.byte   $5F, $06
; ----------------------------
eng_actor_boom_blocks:
	lda	pos_x_lo_actor, x
	clc
	adc	eng_boom_block_off_x_lo, y
	sta	eng_boom_block_x_lo
	lda	pos_x_hi_actor, x
	adc	eng_boom_block_off_x_hi, y
	sta	eng_boom_block_x_hi
	cmp	#$0B
	bcs	state_actor_bombboom_rts
	lda	pos_y_lo_actor, x
	adc	eng_boom_block_off_y_lo, y
	and	#$F0
	sta	eng_boom_block_y_lo
	sta	eng_boom_block_y_lo_b
	lda	pos_y_hi_actor, x
	adc	eng_boom_block_off_y_hi, y
	sta	eng_boom_block_y_hi
	cmp	#$0A
	bcs	state_actor_bombboom_rts
	ldy	eng_level_h
	bne	:++ ; if (eng_level_h) {
		lsr	a
		ror	eng_boom_block_y_lo
		lsr	a
		ror	eng_boom_block_y_lo
		lsr	a
		ror	eng_boom_block_y_lo
		lsr	a
		ror	eng_boom_block_y_lo

		lda	eng_boom_block_y_lo
		ldy	#$FF
		: ; for () {
			sec
			sbc	#($10)-1
			iny
			bcs	:-
		; }
		sty	eng_boom_block_x_hi

		adc	#$0F
		asl	a
		asl	a
		asl	a
		asl	a
		sta	eng_boom_block_y_lo
	: ; }

	lda	eng_boom_block_x_lo
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	zp_byte_04
	ora	eng_boom_block_y_lo
	sta	zp_work+$05

	ldy	#0
	lda	pos_x_hi_screen_left
	cmp	#$0A
	bne	:+ ; if (pos_x_hi_screen_left == 0x0A) {
		sty	eng_boom_block_x_hi
		iny
	: ; }

	lda	#$10
	sta	zp_addr_07
	lda	eng_boom_block_zp_addr_07_hi, y
	sta	zp_addr_07+1
	ldy	eng_boom_block_x_hi
	: ; for (y = eng_boom_block_x_hi; y >= 0; y--) {
		lda	zp_addr_07
		clc
		adc	#$F0
		sta	zp_addr_07
		lda	zp_addr_07+1
		adc	#0
		sta	zp_addr_07+1

		dey
		bpl	:-
	; }

	ldy	zp_work+$05
	lda	(zp_addr_07), y
	cmp	#$9C
	beq	:+
	cmp	#$93
	beq	:+
	cmp	#$70
	beq	:+ ; if () {
		rts
	: ; }

	lda	#$40
	sta	(zp_addr_07), y
	lda	eng_boom_block_x_hi
	and	#$01
	eor	#$01
	asl	a
	asl	a

	ldy	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		asl	a
	: ; }

	pha
	lda	eng_boom_block_y_lo
	sta	fdsbios_ppu_y_px
	lda	eng_boom_block_x_lo
	and	#CLAMP_16
	sta	fdsbios_ppu_x_px
	jsr	fdsbios_ppu_px_to_nt

	ldx	ppu_displist_offset
	lda	fdsbios_ppu_ntaddr+1
	sta	ppu_displist_addr_dbyt+1, x
	clc
	adc	#$20
	sta	ppu_displist_payload+3, x
	pla
	ora	fdsbios_ppu_ntaddr
	sta	ppu_displist_addr_dbyt, x
	adc	#0
	sta	ppu_displist_payload+2, x
	lda	#2
	sta	ppu_displist_count, x
	sta	ppu_displist_payload+4, x
	lda	#$FA
	sta	ppu_displist_payload, x
	sta	ppu_displist_payload+1, x
	sta	ppu_displist_payload+5, x
	sta	ppu_displist_payload+6, x
	lda	#0
	sta	ppu_displist_payload+7, x
	txa
	clc
	adc	#10
	sta	ppu_displist_offset

	ldx	#ENG_ACTOR_MAX-1
	: ; for (actor of actors) {
		lda	proc_id_actor, x
		beq	:+
		dex
		bpl	:-
		bmi	:+++
	: ; } if (actor_found) {
		lda	eng_boom_block_x_lo
		and	#$F0
		sta	pos_x_lo_actor, x
		lda	eng_boom_block_x_hi

		ldy	eng_level_h
		bne	:+ ; if (!eng_level_h) {
			tya
		: ; }

		sta	pos_x_hi_actor, x
		lda	eng_boom_block_y_lo_b
		sta	pos_y_lo_actor, x
		lda	eng_boom_block_y_hi
		sta	pos_y_hi_actor, x
		jsr	actor_init_base_timerless
		jsr	actor_proc_pow
	: ; }

	ldx	actor_index
	rts
; -----------------------------------------------------------
eng_tiles_smoke_poof:
	.byte	$46, $4A, $4E, $52
; ----------------------------
state_actor_smokepoof:
	jsr	eng_actor_dobounds
	lda	attr_actor, x
	ora	#(actor_attrs::h_flip)
	sta	attr_actor, x

	lda	timer_actor, x
	beq	:+++++ ; if (this.timer != 0) {
		lsr	a
		lsr	a
		lsr	a
		tay
		lda	eng_tiles_smoke_poof, y
		jsr	render_actor_base_draw
		lda	eng_enemy_door_tbl, x
		beq	:++++
		lda	timer_actor, x
		cmp	#3
		bne	:++++ ; if (eng_enemy_door_tbl[x] && this.timer == 3) {
			ldy	#$22
			sty	eng_smokepoof_ani_0
			sty	eng_smokepoof_ani_1
			iny
			sty	eng_smokepoof_ani_2
			sty	eng_smokepoof_ani_3
			tay
			: ; for (y; y >= y; y--) {
				lda	eng_tbl_maskdoors, x
				and	#$01
				asl	a
				asl	a
				eor	#$04

				ldx	eng_level_h
				bne	:+ ; if (!eng_level_h) {
					asl	a
				: ; }

				ldx	eng_smokepoof_ani_idx, y
				ora	eng_smokepoof_ani_0, x
				sta	eng_smokepoof_ani_0, x
				ldx	actor_index
				dey
				bpl	:--
			; }

			lda	#$14
			sta	nmi_buffer_ptr

			ldy	eng_tbl_maskdoors, x
			lda	#>(bg_6000-ENEMY_DOOR_BG_PAGESIZE)
			sta	zp_addr_00+1
			lda	#<(bg_6000-ENEMY_DOOR_BG_PAGESIZE)
			sta	zp_addr_00
			: ; for (y = eng_tbl_maskdoors[x]; y >= 0; y--) {
				lda	zp_addr_00
				clc
				adc	#<(ENEMY_DOOR_BG_PAGESIZE)
				sta	zp_addr_00
				lda	zp_addr_00+1
				adc	#>(ENEMY_DOOR_BG_PAGESIZE)
				sta	zp_addr_00+1

				dey
				bpl	:-
			; }

			ldy	#ENEMY_DOOR_BG_POS+$00
			lda	#$56
			sta	(zp_addr_00), y
			ldy	#ENEMY_DOOR_BG_POS+$10
			sta	(zp_addr_00), y
			lda	#$53
			ldy	#ENEMY_DOOR_BG_POS+$01
			sta	(zp_addr_00), y
			ldy	#ENEMY_DOOR_BG_POS+$12
			sta	(zp_addr_00), y
			lda	#$54
			ldy	#ENEMY_DOOR_BG_POS+$11
			sta	(zp_addr_00), y
		: ; }

		rts
	: ; } else {
		lda	obj_id_actor, x
		cmp	#actor::fryguy_split
		bne	:+
		dec	eng_actor_fryguy_split_frame
		bpl	:+ ; if (actor_type == fryguy_split && --eng_actor_fryguy_split_frame < 0) {
			inc	eng_enemy_door_tbl, x
			inc	obj_id_actor, x
			jmp	state_actor_dead_door
		: ; } else {
			jmp	actor_destroy
		; }
	; }
; -----------------------------------------------------------
state_actor_sand:
	jsr	eng_actor_dobounds
	lda	#(actor_attrs::h_flip|actor_attrs::bit1)
	sta	attr_actor, x

	lda	timer_actor, x
	beq	:++ ; if (this.timer != 0) {
		lda	#<-($08)
		sta	veloc_y_actor, x
		jsr	motion_y

		lda	#$B2
		ldy	timer_actor, x
		cpy	#16
		bcs	:+ ; if (this.timer < 16) {
			lda	#(actor_flags::mirror_ani)
			sta	actor_mode, x
			lda	#(actor_attrs::bit0)
			sta	attr_actor, x
			asl	a
			sta	disp_dir_actor, x
			inc	timer_ani_actor, x
			jsr	actor_timer_ani_inc
			lda	#$B4
		: ; }
		jmp	render_actor_base_draw
	: ; } else {
		cpx	player_item_held_id
		bne	:+ ; if (!player_item_held_id) {
			lda	#0
			sta	player_item_held
		: ; }

		jmp	actor_destroy
	; }
