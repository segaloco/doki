.include	"mem.i"
.include	"actor.i"
.include	"modes.i"
.include	"tunables.i"

SHYGUY_PRJ_VELOC_X	= 5

	.export actor_init_shyguy_prj
	.export actor_init_shyguy_prj_lo
actor_init_shyguy_prj:
	ldy	#(ENG_ACTOR_MAX)-1
	bne	:+

actor_init_shyguy_prj_lo:
	ldy	#(ENG_ACTOR_LO_COUNT)-1

	: ; for (y; y >= 0 && actor.proc_id != null; y--) {
		lda	a:proc_id_actor, y
		beq	:+
		dey
		bpl	:-
	; }

	; if () {
		rts
	: ; } else {
		lda	#actor_states::normal
		sta	a:proc_id_actor, y
		lsr	a
		sta	eng_enemy_door_tbl, y
		lda	#actor::shyguy_red
		sta	a:obj_id_actor, y
		lda	pos_x_lo_actor, x
		adc	#<(SHYGUY_PRJ_VELOC_X)
		sta	a:pos_x_lo_actor, y
		lda	pos_x_hi_actor, x
		adc	#>(SHYGUY_PRJ_VELOC_X)
		sta	a:pos_x_hi_actor, y
		lda	pos_y_lo_actor, x
		sta	a:pos_y_lo_actor, y
		lda	pos_y_hi_actor, x
		sta	a:pos_y_hi_actor, y
		sty	zp_addr_00
		tya
		tax
		jsr	actor_init_base
		jsr	actor_erase
		ldx	actor_index
		rts
	; }
