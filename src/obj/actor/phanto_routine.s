.include	"actor.i"

.include	"mem.i"



PHANTO_VELOC_X_INC	= $01
PHANTO_VELOC_X_MAX	= $30

PHANTO_VELOC_Y_INC	= $01
PHANTO_VELOC_Y_MAX	= $18

; -----------------------------------------------------------
actor_phanto_veloc_x_inc:
	.byte	PHANTO_VELOC_X_INC
	.byte	<-(PHANTO_VELOC_X_INC)

actor_phanto_veloc_x_max:
	.byte	PHANTO_VELOC_X_MAX
	.byte	<-(PHANTO_VELOC_X_MAX)

actor_phanto_veloc_y_inc:
	.byte	PHANTO_VELOC_Y_INC
	.byte	<-(PHANTO_VELOC_Y_INC)
	.byte	PHANTO_VELOC_Y_INC

actor_phanto_veloc_y_max:
	.byte	PHANTO_VELOC_Y_MAX
	.byte	<-(PHANTO_VELOC_Y_MAX)
	.byte	PHANTO_VELOC_Y_MAX
; ----------------------------
	.export actor_proc_phanto
actor_proc_phanto:
	jsr	render_actor_base

	ldy	#1
	lda	player_item_held
	beq	:+
	ldx	player_item_held_id
	lda	obj_id_actor, x
	ldx	actor_index
	cmp	#actor::key
	bcc	:+
	cmp	#actor::lamp
	bcs	:+ ; if (eng_held_actor_type >= key && eng_held_actor_type < lamp) {
		dey
	: ; }

	lda	pos_y_hi_actor, x
	clc
	adc	#1
	sta	zp_work+$05
	lda	pos_y_lo_player
	cmp	pos_y_lo_actor, x
	ldx	pos_y_hi_player
	inx
	txa
	sbc	zp_work+$05
	ldx	actor_index
	bcs	:+ ; if (++player.pos_y.hi < zp_work[5]) {
		iny
	: ; }

	lda	veloc_y_actor, x
	cmp	actor_phanto_veloc_y_max, y
	beq	:+ ; if (actor.veloc_y != actor_phanto_veloc_y_max[y]) {
		clc
		adc	actor_phanto_veloc_y_inc, y
		sta	veloc_y_actor, x
	: ; }

	lda	eng_actor_tbl_480, x
	clc
	adc	#$A0
	sta	eng_actor_tbl_480, x
	bcc	:+ ; if (eng_actor_tbl_480_wrap) {
		lda	eng_actor_tbl_477, x
		and	#%00000001
		tay
		lda	veloc_x_actor, x
		clc
		adc	actor_phanto_veloc_x_inc, y
		sta	veloc_x_actor, x
		cmp	actor_phanto_veloc_x_max, y
		bne	:+ ; if (actor.veloc_x == actor_phanto_veloc_x_max[y]) {
			inc	eng_actor_tbl_477, x
		: ; }
	; }

	lda	eng_level_h
	beq	:+ ; if (eng_level_h) {
		lda	veloc_x_player
		sta	accel_x_actor, x
	: ; }

	jmp	eng_actor_dophys
