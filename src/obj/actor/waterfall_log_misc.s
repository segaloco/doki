.include	"actor.i"
.include	"modes.i"

.include	"mem.i"



	.export actor_proc_pow
	.export actor_timer_held_tick
	.export actor_timer_throw_tick
; -----------------------------------------------------------
actor_proc_pow:
	lda	#actor_states::blockfizz
	sta	proc_id_actor, x
	lda	#24
	sta	timer_actor, x
	: rts
; ----------------------------
actor_timer_held_tick:
	lda	timer_actor_held, x
	beq	:-
	pla
	pla
	jmp	actor_throw
; ----------------------------
actor_timer_throw_tick:
	lda	timer_actor_throw, x
	beq	:+
	pla
	pla
	jmp	render_actor
; -----------------------------------------------------------
WATERFALL_LOG_VELOC_Y_UP	= <-2
WATERFALL_LOG_VELOC_Y_DOWN	= 4
WATERFALL_LOG_SPAWN_POS_Y	= <-16
WATERFALL_LOG_CLIMB_HEIGHT	= 12

WATERFALL_LOG_POS_Y_START	= zp_actor_work_byte_1
WATERFALL_LOG_MOTION_DOWN	= zp_actor_work_byte_2

	.export actor_init_waterfall_log
	.export actor_proc_waterfall_log
actor_init_waterfall_log:
	jsr	actor_init_stationary
	sta	timer_actor_stun, x
	lda	pos_y_lo_actor, x
	sta	WATERFALL_LOG_POS_Y_START, x
	: rts
; ----------------------------
actor_proc_waterfall_log:
	asl	attr_actor, x
	lda	frame_count
	asl	a
	asl	a
	asl	a
	asl	a
	ror	attr_actor, x

	ldy	WATERFALL_LOG_MOTION_DOWN, x
	bne	:++ ; if (!log_going_down) {
		lda	attr_actor, x
		ora	#(actor_attrs::bit5)
		sta	attr_actor, x

		lda	WATERFALL_LOG_POS_Y_START, x
		sec
		sbc	#WATERFALL_LOG_CLIMB_HEIGHT
		cmp	pos_y_lo_actor, x
		lda	#WATERFALL_LOG_VELOC_Y_UP
		bcc	:+ ; if (actor.pos_y < (WATERFALL_LOG_POS_Y_START-WATERFALL_LOG_CLIMB_HEIGHT)) {
			lda	attr_actor, x
			and	#<~(actor_attrs::bit5)
			sta	attr_actor, x
			inc	WATERFALL_LOG_MOTION_DOWN, x
			lda	#WATERFALL_LOG_VELOC_Y_DOWN
		: ; }
		sta	veloc_y_actor, x

		jmp	:++
	: ; } else {
		lda	frame_count
		and	#%00000111
		bne	:+ ; if (!(frame_count % 8)) {
			inc	veloc_y_actor, x
		; }
	: ; }

	jsr	motion_y

	lda	pos_y_lo_actor, x
	cmp	#WATERFALL_LOG_SPAWN_POS_Y
	bcc	:+ ; if (actor.pos_y < WATERFALL_LOG_SPAWN_POS_Y) {
		lda	#0
		sta	WATERFALL_LOG_MOTION_DOWN, x
		lda	WATERFALL_LOG_POS_Y_START, x
		sta	pos_y_lo_actor, x
	: ; }

	jmp	render_actor_base
