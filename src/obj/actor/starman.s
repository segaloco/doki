.include	"actor.i"

.include	"mem.i"



STARMAN_OFFSET_X	= <-48
STARMAN_OFFSET_Y	= <-32

STARMAN_VELOC_X	= 8
STARMAN_VELOC_Y	= <-4

; -----------------------------------------------------------
	.export actor_init_starman, actor_proc_starman
actor_init_starman:
	jsr	actor_init_shyguy_prj_lo
	bmi	:+ ; if (actor_init_shyguy_prj_lo() >= 0) {
		ldx	zp_addr_00
		lda	#actor::starman
		sta	obj_id_actor, x
		lda	pos_x_lo_screen_left
		adc	#<(STARMAN_OFFSET_X)
		sta	pos_x_lo_actor, x
		lda	pos_x_hi_screen_left
		adc	#>(STARMAN_OFFSET_X)
		sta	pos_x_hi_actor, x
		lda	pos_y_lo_screen
		adc	#<(STARMAN_OFFSET_Y)
		sta	pos_y_lo_actor, x
		lda	pos_y_hi_screen
		adc	#>(STARMAN_OFFSET_Y)
		sta	pos_y_hi_actor, x
		jsr	actor_attr_set
		ldx	actor_index
	: ; }

	rts
; -----------------------------------------------------------
actor_proc_starman:
	lda	#STARMAN_VELOC_Y
	sta	veloc_y_actor, x

	ldy	#<-(STARMAN_VELOC_X)
	lda	frame_count
	sta	timer_actor_flash, x
	bpl	:+ ; if (timer_actor_flash < 0) {
		ldy	#STARMAN_VELOC_X
	: ; }
	sty	veloc_x_actor, x

	jmp	render_actor
