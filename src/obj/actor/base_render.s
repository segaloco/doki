.include	"system/ppu.i"

.include	"mem.i"
.include	"tunables.i"
.include	"actor.i"
.include	"modes.i"

.include	"unk.i"

	.export render_albatross
render_albatross:
	lda	zp_byte_EE
	pha
	jsr	render_actor_base_rocket_check
	pla
	asl	a
	sta	zp_byte_EE

	lda	ENG_ACTOR_ALBATROSS_BOMB_THROWN, x
	ora	zp_byte_EF
	bne	render_actor_base_default ; if (!bomb_thrown | zp_byte_EF)) {
		lda	pos_x_rel_actor
		adc	#8
		sta	zp_addr_01
		lda	disp_dir_actor, x
		sta	eng_actor_render_dir
		lda	#actor_attrs::bit0
		sta	eng_actor_render_attr
		sta	eng_actor_render_attr_b
		jsr	actor_sprite_alloc
		ldx	#$14
		jmp	render_actor_base_orientation
	; }
; -----------------------------------------------------------
	.export render_actor_base
	.export render_actor_base_rocket_check
render_actor_base:
	ldy	obj_id_actor, x
	lda	eng_actor_sprite_tbl, y
	cmp	#actor_sprite::none
	beq	render_actor_base_default
	cpy	#actor::mouser ; switch (this.obj_id[x]) {
	bne	:+
		jmp	eng_actor_mouser_render

	: cpy	#actor::doormask_boss
	bne	:+
		jmp	eng_actor_doormask_boss_render

	; case eng_actor_sprite_tbl[y] == none:
	; default:
	render_actor_base_default:
		rts

	: cpy	#actor::pidgit
	bne	:+
		jmp	eng_actor_pidgit_render

	: cpy	#actor::porcupo
	bne	:+
		jmp	actor_proc_porcupo

	: cpy	#actor::vegetable_large
	bne	:+
		jmp	render_vegetable_large

	: cpy	#actor::autobomb
	bne	:+
		jmp	eng_actor_autobomb_render

	: cpy	#actor::fryguy
	bne	:+
		jmp	eng_actor_fryguy_render

	: cpy	#actor::doormask_left
	bne	:+
		jmp	render_doormask

	: cpy	#actor::wart
	bne	:+
		jmp	eng_actor_wart_render

	: cpy	#actor::whale_spout
	bne	:+
		jmp	eng_actor_whale_spout_render

	: cpy	#actor::pokey
	bne	:+
		jmp	eng_actor_pokey_render

	: cpy	#actor::heart
	bne	:+
		jmp	render_heart

	: cpy	#actor::ostro
	bne	:+
		jmp	eng_actor_ostro_render

	: cpy	#actor::tryclyde
	bne	:+
		jmp	eng_actor_tryclyde_render

	: cpy	#actor::birdo
	bne	:+
		jmp	eng_actor_birdo_render

	: cpy	#actor::albatross_carry
	bcc	render_actor_base_rocket_check
	cpy	#actor::albatross_left+1
	bcs	render_actor_base_rocket_check
		jmp	render_albatross

render_actor_base_rocket_check:
	ldy	obj_id_actor, x
	cpy	#actor::rocket
	bne	:+ ; if (this.obj_id == rocket) {
		jmp	eng_actor_rocket_render
	: ; }

	lda	eng_actor_sprite_tbl, y
; -----------------------------------------------------------
	.export render_actor_base_draw
render_actor_base_draw:
	sta	eng_actor_sprite_id
	lda	zp_byte_EF
	bne	render_actor_base_default
	lda	actor_mode, x
	and	#actor_flags::tilemap2
	sta	eng_actor_chr_idx_page_id

	ldy	disp_dir_actor, x
	lda	attr_actor, x
	and	#(actor_attrs::h_flip|actor_attrs::bit3)
	beq	:+ ; if (actor_attr & (h_flip|bit3)) {
		ldy	#ACTOR_DIR_LEFT
		lda	eng_inner_space
		cmp	#2
		bne	:+ ; if (eng_inner_space == 2) {
			dey
		; }
	: ; }
	sty	eng_actor_render_dir

	lda	attr_actor, x
	and	#(actor_attrs::bit6|actor_attrs::bit2)
	sta	eng_actor_render_attr_b
	lda	pos_y_rel_actor
	sta	zp_addr_00
	lda	#0
	sta	zp_byte_0D

	lda	timer_actor_shake, x
	and	#$02
	lsr	a
	ldy	zp_byte_EE
	beq	:+ ; if (zp_byte_EE) {
		lda	#0
	: ; }
	adc	pos_x_rel_actor
	sta	zp_addr_01

	lda	attr_actor, x
	and	#(actor_attrs::v_flip|actor_attrs::bit5|actor_attrs::bit1|actor_attrs::bit0)
	ldy	timer_actor_flash, x
	beq	:+ ; if (timer_actor_flash[x]) {
		and	#(actor_attrs::v_flip|actor_attrs::bit5)
		sta	zp_work+$08
		tya
		lsr	a
		and	#(actor_attrs::bit1|actor_attrs::bit0)
		ora	zp_work+$08
	: ; }
	sta	eng_actor_render_attr

	lda	actor_mode, x
	sta	zp_addr_0C
	asl	a
	lda	timer_ani_actor, x
	ldx	eng_actor_sprite_id
	and	#$08
	beq	:++
	bcc	:+ ; if ((this.timer_ani & 0x08) && (this.mode & mirror_ani)) {
		lda	#ACTOR_DIR_RIGHT
		sta	eng_actor_render_dir
		bne	:++
	: ; } else if ((this.timer_ani & 0x08) && !(this.mode & mirror_ani)) {
		inx
		inx

		lda	eng_actor_render_attr_b
		and	#actor_attrs::bit6
		beq	:+ ; if (eng_actor_render_attr_b & bit6) {
			inx
			inx
	
			lda	zp_addr_0C
			and	#$20
			beq	:+ ; if (zp_addr_0C & 0x20) {
				inx
				inx
			; }
		; }
	: ; }

	ldy	zp_byte_F4

	lda	eng_actor_render_attr_b
	and	#actor_attrs::bit6
	beq	:++ ; if (eng_actor_render_attr_b & bit6) {
		lda	eng_actor_render_attr_b
		and	#actor_attrs::bit2
		beq	:+ ; if (eng_actor_render_attr_b & bit2) {
			lda	zp_byte_EE
			sta	zp_byte_08

			lda	eng_actor_render_dir
			cmp	#ACTOR_DIR_RIGHT
			bne	:+ ; if (eng_actor_render_dir == ACTOR_DIR_RIGHT) {
				lda	zp_addr_00+1
				adc	#$0F
				sta	zp_addr_00+1
				asl	zp_byte_EE
				asl	zp_byte_EE
			; }
		: ; }

		jsr	render_actor_base_tiles

		lda	eng_actor_render_attr_b
		and	#actor_attrs::bit2
		beq	:+ ; if (eng_actor_render_attr_b & bit2) {
			lda	pos_y_rel_actor
			sta	zp_addr_00
			lda	pos_x_rel_actor
			sta	zp_addr_00+1
			lda	zp_byte_08
			sta	zp_byte_EE

			lda	eng_actor_render_dir
			cmp	#ACTOR_DIR_RIGHT
			beq	:+ ; if (eng_actor_render_dir != ACTOR_DIR_RIGHT) {
				lda	zp_addr_00+1
				adc	#$0F
				sta	zp_addr_00+1
				asl	zp_byte_EE
				asl	zp_byte_EE
			: ; }
		; }
	; }

render_actor_base_orientation:
	jsr	render_actor_base_tiles
	ldy	zp_byte_F4
	lda	eng_actor_render_attr_b
	cmp	#actor_attrs::bit6
	bne	:++
	lda	eng_actor_render_attr
	bpl	:++
	lda	zp_addr_0C
	and	#$20
	beq	:+ ; if (eng_actor_render_attr_b == bit6 && (eng_running_actor_attrs & v_flip) && (zp_addr_0C & 0x20)) {
		ldx	zp_byte_0D
		lda	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, x
		pha
		lda	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, x
		pla
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		lda	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, x
		pha
		lda	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, x
		pla
		sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		lda	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, x
		pha
		lda	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, x
		pla
		sta	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, y
		bcs	:++
	: ; } else {
		lda	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		pha
		lda	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		pla
		sta	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, y
		lda	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		pha
		lda	oam_buffer+3*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		pla
		sta	oam_buffer+3*OBJ_SIZE+OBJ_POS_V, y
	: ; }

	ldx	actor_index
	lda	attr_actor, x
	and	#actor_attrs::h_flip
	beq	:+ ; if (eng_running_actor_attrs & h_flip) {
		lda	eng_actor_render_attr
		sta	oam_buffer+0*OBJ_SIZE+OBJ_ATTR, y
		sta	oam_buffer+2*OBJ_SIZE+OBJ_ATTR, y
		ora	#(obj_attr::h_flip)
		sta	oam_buffer+1*OBJ_SIZE+OBJ_ATTR, y
		sta	oam_buffer+3*OBJ_SIZE+OBJ_ATTR, y
	: ; }

	rts
; ----------------------------
	.export render_actor_base_tiles
render_actor_base_tiles:
	lda	zp_addr_0C
	and	#$20
	bne	LB09A ; if (!(zp_addr_0C & 0x20)) {
		lda	eng_actor_chr_idx_page_id
		bne	:+ ; if (eng_actor_chr_idx_page_id == 0) {
			lda	eng_actor_chr_idx_page_0, x
			sta	oam_buffer+0*OBJ_SIZE+OBJ_CHR_NO, y
			lda	eng_actor_chr_idx_page_0+1, x
			sta	oam_buffer+1*OBJ_SIZE+OBJ_CHR_NO, y
			bne	:++
		: ; } else {
			lda	eng_actor_chr_idx_page_1, x
			sta	oam_buffer+0*OBJ_SIZE+OBJ_CHR_NO, y
			lda	eng_actor_chr_idx_page_1+1, x
			sta	oam_buffer+1*OBJ_SIZE+OBJ_CHR_NO, y
		: ; }

		lda	eng_actor_render_dir
		lsr	a
		lda	#0
		bcc	:+ ; if (eng_actor_render_dir == ACTOR_DIR_RIGHT) {
			lda	oam_buffer+0*OBJ_SIZE+OBJ_CHR_NO, y
			pha
			lda	oam_buffer+1*OBJ_SIZE+OBJ_CHR_NO, y
			sta	oam_buffer+0*OBJ_SIZE+OBJ_CHR_NO, y
			pla
			sta	oam_buffer+1*OBJ_SIZE+OBJ_CHR_NO, y
			lda	#(obj_attr::h_flip)
		: ; }
		ora	eng_actor_render_attr
		sta	oam_buffer+0*OBJ_SIZE+OBJ_ATTR, y
		sta	oam_buffer+1*OBJ_SIZE+OBJ_ATTR, y

		lda	#OAM_INIT_SCANLINE
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		lda	zp_byte_EE
		and	#$08
		bne	:+ ; if (!(zp_byte_EE & 0x08)) {
			lda	zp_addr_00
			sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		: ; }
		lda	zp_byte_EE
		and	#$04
		bne	:+ ; if (!(zp_byte_EE & 0x04)) {
			lda	zp_addr_00
			sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		: ; }

		lda	zp_addr_00
		clc
		adc	#$10
		sta	zp_addr_00
		lda	zp_addr_00+1
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_H, y
		clc
		adc	#8
		sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_H, y
		tya
		clc
		adc	#8
		tay
		inx
		inx
		rts
	LB09A: ; } else {
		lda	eng_actor_chr_idx_page_1, x
		sta	oam_buffer+0*OBJ_SIZE+OBJ_CHR_NO, y
		lda	eng_actor_chr_idx_page_1+1, x
		sta	oam_buffer+1*OBJ_SIZE+OBJ_CHR_NO, y
		lda	eng_actor_chr_idx_page_1+2, x
		sta	oam_buffer+2*OBJ_SIZE+OBJ_CHR_NO, y

		lda	eng_actor_render_dir
		lsr	a
		lda	#0
		bcc	:+ ; if (eng_actor_render_dir == ACTOR_DIR_RIGHT) {
			lda	oam_buffer+0*OBJ_SIZE+OBJ_CHR_NO, y
			pha
			lda	oam_buffer+2*OBJ_SIZE+OBJ_CHR_NO, y
			sta	oam_buffer+0*OBJ_SIZE+OBJ_CHR_NO, y
			pla
			sta	oam_buffer+2*OBJ_SIZE+OBJ_CHR_NO, y
			lda	#(obj_attr::h_flip)
		: ; }
		ora	eng_actor_render_attr
		sta	oam_buffer+0*OBJ_SIZE+OBJ_ATTR, y
		sta	oam_buffer+1*OBJ_SIZE+OBJ_ATTR, y
		sta	oam_buffer+2*OBJ_SIZE+OBJ_ATTR, y

		lda	#OAM_INIT_SCANLINE
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		sta	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, y
		lda	zp_byte_EE
		and	#$08
		bne	:+ ; if (!(zp_byte_EE & 0x08)) {
			lda	zp_addr_00
			sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		: ; }
		lda	zp_byte_EE
		and	#$04
		bne	:+ ; if (!(zp_byte_EE & 0x04)) {
			lda	zp_addr_00
			sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_V, y
		: ; }
		lda	zp_byte_EE
		and	#$02
		bne	:+ ; if (!(zp_byte_EE & 0x02)) {
			lda	zp_addr_00
			sta	oam_buffer+2*OBJ_SIZE+OBJ_POS_V, y
		: ; }

		lda	zp_addr_00
		clc
		adc	#$10
		sta	zp_addr_00
		lda	zp_addr_00+1
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_H, y
		adc	#8
		sta	oam_buffer+1*OBJ_SIZE+OBJ_POS_H, y
		adc	#8
		sta	oam_buffer+2*OBJ_SIZE+OBJ_POS_H, y
		txa
		pha
		jsr	actor_sprite_alloc
		pla
		tax
		lda	zp_byte_0D
		bne	:+ ; if () {
			sty	zp_byte_0D
		: ; }

		inx
		inx
		inx
		rts
	; }
