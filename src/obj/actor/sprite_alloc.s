.include	"system/ppu.i"

.include	"mem.i"
.include	"tunables.i"

	.export actor_sprite_alloc
actor_sprite_alloc:
	ldx	#ENG_ACTOR_MAX-1
	: ; for (actor of actors) {
		lda	proc_id_actor, x
		beq	:++++ ; if (this.proc_id != null) {
			: dex
			bpl	:-- ; if (actor == first) {
				ldy	#0
				lda	oam_buffer+OBJ_POS_V+(OBJ_SIZE*0), y
				cmp	#OAM_INIT_SCANLINE
				bne	:+
				lda	oam_buffer+OBJ_POS_V+(OBJ_SIZE*1), y
				cmp	#OAM_INIT_SCANLINE
				beq	:++
				: ; if (oam_buffer[y].V != OAM_INIT_SCANLINE && oam_buffer[y+1].V != OAM_INIT_SCANLINE) {
					ldy	#16
				: ; }

			actor_sprite_alloc_end:
				ldx	actor_index
				rts
			; }
		: ; } else {
			txa
			clc
			adc	byte_400
			tay
			lda	eng_map_flicker, y
			tay
			lda	oam_buffer+OBJ_POS_V+(OBJ_SIZE*0), y
			cmp	#OAM_INIT_SCANLINE
			bne	:----
			lda	oam_buffer+OBJ_POS_V+(OBJ_SIZE*1), y
			cmp	#OAM_INIT_SCANLINE
			bne	:----
			beq	actor_sprite_alloc_end
		; }
	; }
