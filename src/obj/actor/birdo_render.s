.include	"mem.i"
.include	"modes.i"
.include	"actor.i"

	.export eng_actor_birdo_render
eng_actor_birdo_render:
	lda	proc_id_actor, x
	cmp	#actor_states::normal
	bne	:+
	lda	timer_actor_flash, x
	beq	:++
	: ; if (this.proc_id == normal || this.timer_flash != 0) {
		lda	#2*actor_sprite::unk_35
		bne	:++
	: lda	timer_actor, x
	beq	:++ ; } else if (this.timer != 0) {
		lda	#2*actor_sprite::unk_31
	: ; }
	; if (this.proc_id == normal || this.timer_flash != 0 || this.timer != 0) {
		jmp	render_actor_base_draw
	: ; } else {
		jmp	render_actor_base_rocket_check
	; }
