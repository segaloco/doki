.include	"actor.i"

.include	"mem.i"



	.export actor_init_swarm_albatross, actor_init_swarm_beezo
; -----------------------------------------------------------
swarm_albatross_offset_x_lo:
	.byte	<-16, 0

swarm_albatross_offset_x_hi:
	.byte   <-1, 1
; ----------------------------
actor_init_swarm_albatross:
	jsr	actor_init_swarm
	adc	swarm_albatross_offset_x_lo, y
	sta	pos_x_lo_actor, x
	lda	pos_x_hi_screen_left
	adc	swarm_albatross_offset_x_hi, y
	sta	pos_x_hi_actor, x
	sty	zp_byte_01

	lda	#actor::albatross_carry
	sta	obj_id_actor, x
	jsr	actor_attr_set
	lda	byte_4BB
	and	#%00011111
	adc	#32
	sta	pos_y_lo_actor, x

	ldy	zp_byte_01
	jsr	actor_init_base_movement
	asl	veloc_x_actor, x
	rts
; -----------------------------------------------------------
swarm_beezo_offset_x_lo:
	.byte	0, <-1
; ----------------------------
actor_init_swarm_beezo:
	jsr	actor_init_swarm
	adc	swarm_beezo_offset_x_lo, y
	sta	pos_x_lo_actor, x
	lda	eng_level_h
	beq	:+ ; if (eng_level_h) {
		lda	pos_x_hi_screen_left
		adc	#0
	: ; }

	sta	pos_x_hi_actor, x
	lda	pos_y_lo_screen
	sta	pos_y_lo_actor, x
	lda	pos_y_hi_screen
	sta	pos_y_hi_actor, x
	sty	zp_byte_01

	lda	#actor::beezo_dive
	sta	obj_id_actor, x
	jsr	actor_attr_set

	ldy	zp_byte_01
	jsr	actor_init_base_movement
	jsr	actor_init_beezo_dive_end
	rts
; -----------------------------------------------------------
actor_init_swarm:
	lda	eng_timer_stopwatch
	bne	:++ ; if (eng_timer_stopwatch == 0) {
		lda	byte_623
		clc
		adc	#3
		sta	byte_623
		bcc	:++ ; if ((byte_623 += 3) < 0x100) {
			jsr	actor_init_shyguy_prj_lo
			bmi	:++ ; if () {
				ldy	#0
				lda	frame_count
				and	#%01000000
				bne	:+ ; if ((frame_count % 128) < 64) {
					iny
				: ; }

				ldx	zp_addr_00
				lda	pos_x_lo_screen_left
				rts
			: ; }
		; }
	; }

	pla
	pla
	rts
