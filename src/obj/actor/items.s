.include	"tunables.i"
.include	"actor.i"
.include	"modes.i"

.include	"mem.i"



COIN_VELOC_Y_TRIGGER	= <-22
ITEM_PRESENT_VELOC_Y	= <-32

; -----------------------------------------------------------
	.byte	$18, <-($18)

tbl_A3C8:
	.byte	$FE, $F8, $F0, $E8
; -----------------------------------------------------------
	.export actor_proc_coin
actor_proc_coin:
	jsr	actor_timer_ani_inc
	lda	veloc_y_actor, x
	cmp	#COIN_VELOC_Y_TRIGGER
	bne	:+ ; if (actor.veloc_y == COIN_VELOC_Y_TRIGGER) {
		lda	#$04
		sta	apu_sfx_queue2
	: ; }
; ----------------------------
	.export actor_proc_oneup
actor_proc_oneup:
	lda	veloc_y_actor, x
	cmp	#16
	bmi	actor_proc_lifepickup
	jsr	actor_proc_lamp_end
	lda	obj_id_actor, x
	cmp	#actor::one_up
	beq	:+ ; if (actor.veloc_y >= 16 && actor.id != one_up) {
		inc	bonus_coins
		rts
	: ; } ; else {
		inc	byte_620
		inc	player_lives
		bne	:+ ; if (++player.lives > 255) {
			dec	player_lives
		: ; }

		lda	#4

	actor_proc_oneup_end:
		sta	apu_music_queue_2
		rts
	; }
; -----------------------------
	.export actor_proc_crystalball
actor_proc_crystalball:
	jsr	actor_attach_birdo

	.export actor_proc_lifepickup
actor_proc_lifepickup:
	lda	timer_actor_held, x
	cmp	#1
	bne	:+
	lda	player_crouch
	beq	:++
	: ; if (actor.timer_held != 1 || player_crouch) {
		jmp	actor_proc_bomb
	: ; } else {
		jsr	actor_throw
		lda	#0
		sta	player_item_held
		sta	timer_actor_held, x
		jsr	actor_proc_lamp_end
		lda	obj_id_actor, x
		cmp	#actor::crystal_ball
		bne	:++ ; if (actor.id == crystal_ball) {
			lda	eng_doormask_open_size
			bne	:+ ; if (!eng_doormask_open_size) {
				lda	#$20
				sta	apu_music_queue_2
				lda	#$60
				sta	eng_timer_doormask_open
				inc	eng_doormask_open_size
			: ; }

			rts
		: ; }

		cmp	#actor::one_up
		beq	:+
		cmp	#actor::stopwatch
		beq	:+++
		cmp	#actor::life_heart
		bne	:++ ; if (actor.id == life_heart) {
			ldx	zp_actor_work_byte_1+0
			inc	eng_actor_lifeheart_pull_tbl, x
			ldx	actor_index
			inc	hp_player_max
			jsr	eng_player_transition_reload_hearts_max
			lda	#$01
			jmp	actor_proc_oneup_end
		: ; } else if (actor.id == one_up || actor.id != stopwatch) {
			; if (actor.id == one_up) {
				lda	#(actor_attrs::h_flip)
				sta	attr_actor, x
			: ; }

			lda	#ITEM_PRESENT_VELOC_Y
			sta	veloc_y_actor, x
			lda	#actor_states::normal
			sta	proc_id_actor, x
			rts
		: ; } else {
			lda	#255
			sta	eng_timer_stopwatch
			rts
		; }
	; }
; ----------------------------
	.export actor_proc_key
actor_proc_key:
	jsr	actor_attach_birdo
; ----------------------------
	.export actor_proc_bomb, render_bombboom
actor_proc_bomb:
	jsr	col_actor_tile_proc

	lda	collision_actor, x
	pha
	and	disp_dir_actor, x
	beq	:+ ; if (actor.collision & actor.disp_dir) {
		jsr	motion_dir_flip
		jsr	actor_proc_base_veloc_reverse
		jsr	actor_proc_base_veloc_reverse
		jsr	actor_proc_base_veloc_reverse
	: ; }

	pla
	and	#(actor_collisions::floor)
	beq	:++++ ; if (actor.collision & floor) {
		lda	#0
		sta	timer_actor_throw, x
		lda	veloc_y_actor, x
		cmp	#9
		bcc	:+ ; if (actor.veloc_y < 9) {
			lsr	a
			lsr	a
			lsr	a
			lsr	a
			tay
			lda	tbl_A3C8, y
			jsr	actor_proc_base_motion_do
			jmp	:+++
		: ; } else {
			jsr	actor_proc_base_veloc_reset

			lda	zp_addr_0A+1
			bne	:+ ; if (!zp_addr_0A) {
				sta	veloc_x_actor, x
			: ; }
		: ; }
	: ; }

	lda	obj_id_actor, x
	cmp	#actor::bomb
	bne	:+++ ; if (actor.id == bomb) {
		lda	timer_actor, x
		bne	:++ ; if (actor.timer == 0) {
			ldy	timer_actor_held, x
			beq	:+ ; if (actor.timer_held != 0) {
				sta	player_item_held
				sta	timer_actor_held, x
			: ; }

		render_bombboom:
			lda	#actor_states::bombboom
			sta	proc_id_actor, x
			lda	#32
			sta	timer_actor, x
			sta	eng_timer_sky_flash
			lda	#$01
			sta	apu_dpcm_queue
			lsr	a
			sta	timer_actor_throw, x
			rts
		: ; }

		cmp	#actor::doormask_right
		bcs	:+
		lsr	a
		bcc	:+ ; if (actor.id >= unk40 && actor.id % 1) {
			inc	attr_actor, x
			lda	attr_actor, x
			and	#<~(actor_attrs::bit2)
			sta	attr_actor, x
		: ; }
	; }
; ----------------------------
	.export actor_proc_vegetable, render_vegetable_large
actor_proc_vegetable:
	jsr	actor_timer_held_tick
	jsr	eng_actor_domove

render_vegetable_large:
	lda	zp_actor_work_byte_2, x
	bne	:+ ; if (!zp_actor_work_byte_2[x]) {
		jmp	render_actor_base_rocket_check
	: ; } else {
		jmp	render_actor_base_draw
	; }
; -----------------------------------------------------------
	.export actor_proc_lamp, actor_proc_lamp_end
actor_proc_lamp:
	jsr	actor_timer_held_tick
	jsr	col_actor_tile_proc

	lda	collision_actor, x
	pha
	and	#(actor_collisions::left|actor_collisions::right)
	beq	:+ ; if (actor.collision & (left|right)) {
		jsr	motion_dir_flip
		jsr	actor_proc_base_veloc_reverse
		jsr	actor_proc_base_veloc_reverse
	: ; }

	pla
	and	#(actor_collisions::floor)
	beq	actor_proc_vegetable ; if (actor.collision & floor) {
		jsr	actor_proc_base_veloc_reset
		lda	pos_y_lo_actor, x
		sec
		sbc	#16
		sta	pos_y_lo_actor, x
		lda	pos_x_lo_actor, x
		adc	#7
		and	#%11110000
		sta	pos_x_lo_actor, x
		lda	pos_x_hi_actor, x
		adc	#0
		sta	pos_x_hi_actor, x
		lda	#16
		sta	eng_timer_actor_2, x
		lda	#$02
		sta	byte_602
		inc	zp_actor_work_byte_2, x
		lda	#actor::door_subspace
		sta	obj_id_actor, x

		lda	eng_level_h
		bne	:+ ; if (!eng_level_h) {
			lda	#$40
			sta	byte_602
			jsr	actor_destroy
		: ; }

		jsr	actor_init_shyguy_prj_lo
		bmi	:+ ; if (actor_init_shyguy_prj_lo() >= 0) {
			ldy	zp_addr_00
			lda	pos_x_lo_actor, x
			sta	pos_x_lo_actor, y
			lda	pos_x_hi_actor, x
			sta	pos_x_hi_actor, y
			lda	#(actor_attrs::bit6|actor_attrs::bit0)
			sta	attr_actor, y
			tya
			tax

		actor_proc_lamp_end:
			lda	attr_actor, x
			and	#<~(actor_attrs::bit1|actor_attrs::bit0)
			ora	#(actor_attrs::bit0)
			sta	attr_actor, x
			lda	#actor_states::smokepoof
			sta	proc_id_actor, x
			sta	timer_ani_actor, x
			lda	#31
			sta	timer_actor, x
			ldx	actor_index
		: ; }

		rts
	; }
; -----------------------------------------------------------

ENG_ACTOR_ATTACH_BIRDO_VELOC_X	= 8
ENG_ACTOR_ATTACH_BIRDO_VELOC_Y	= <-32
ENG_ACTOR_ATTACH_BIRDO_OFFSET_Y	= 14

actor_attach_birdo_veloc_x:
	.byte	<-(ENG_ACTOR_ATTACH_BIRDO_VELOC_X)
	.byte	ENG_ACTOR_ATTACH_BIRDO_VELOC_X
; ----------------------------
actor_attach_birdo:
	lda	ENG_ACTOR_BIRDO_TYPE, x
	bne	:+++ ; if (!ENG_ACTOR_BIRDO_TYPE[x]) {
		ldy	#6-1
		: ; for (y = 5; y >= 0; y--) {
			lda	proc_id_actor, y
			cmp	#actor_states::normal
			bne	:+ ; if (actor.proc_id == normal) {
				lda	obj_id_actor, y
				cmp	#actor::birdo
				beq	:+++
			: ; }

			dey
			bpl	:--
		; }
	: ; } else {
		lda	#1
		sta	ENG_ACTOR_BIRDO_TYPE, x
		jmp	actor_attr_set
	: ; }

	lda	pos_x_lo_actor, y
	sta	pos_x_lo_actor, x
	lda	pos_y_lo_actor, y
	adc	#ENG_ACTOR_ATTACH_BIRDO_OFFSET_Y
	sta	pos_y_lo_actor, x
	jsr	motion_side_get
	lda	actor_attach_birdo_veloc_x, y
	sta	veloc_x_actor, x
	lda	#ENG_ACTOR_ATTACH_BIRDO_VELOC_Y
	sta	veloc_y_actor, x
	pla
	pla
	lda	#(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
	sta	actor_mode, x
	lda	#$30
	sta	zp_byte_F4
	jmp	render_actor_base
