.include	"mem.i"
.include	"actor.i"

ENG_ACTOR_KEY_SLOTS	= 6
ENG_ACTOR_MISCPICKUP_SLOTS	= 6

	.export actor_init_key
actor_init_key:
	ldy	#(ENG_ACTOR_KEY_SLOTS)-1
	: ; for (y = ENG_ACTOR_KEY_SLOTS-1; y >= 0; y--) {
		lda	proc_id_actor, y
		beq	:+
		cpy	actor_index
		beq	:+ ; if (actor.proc_id != null && actor.idx != current) {
			lda	obj_id_actor, y
			cmp	#actor::key
			beq	:+++
		: ; }

		dey
		bpl	:--
	; }

	lda	eng_key_used
	bne	:++
	: ; if (!eng_key_used && key_not_found) {
		jmp	actor_init_stationary
	: ; } else {
		jmp	actor_destroy
	; }
; -----------------------------------------------------------
	.export actor_init_miscpickup
actor_init_miscpickup:
	ldy	#ENG_ACTOR_MISCPICKUP_SLOTS-1
	: ; for (y = ENG_ACTOR_MISCPICKUP_SLOTS-1; y >= 0; y--) {
		lda	proc_id_actor, y
		beq	:+
		cpy	actor_index
		beq	:+ ; if (actor.proc_id != null && actor.idx != current) {
			lda	obj_id_actor, y
			cmp	#actor::crystal_ball
			beq	:--
		: ; }

		dey
		bpl	:--
	; }

	lda	eng_doormask_open_size
	bne	:---
	beq	:----
