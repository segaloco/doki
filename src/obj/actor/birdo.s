.include	"tunables.i"
.include	"actor.i"

.include	"mem.i"



ENG_ACTOR_BIRDO_VELOC_X	= $0A
ENG_ACTOR_BIRDO_VELOC_Y	= <-($20)
ENG_ACTOR_BIRDO_1_XPOS	= $A0
ENG_ACTOR_BIRDO_2_XPOS	= $B0
ENG_BIRDO_HP		= 3

ENG_ACTOR_BIRDO_WALK_CYCLE	= zp_actor_work_byte_2
ENG_ACTOR_BIRDO_WALK_CYCLE_CUE	= $40

	.export actor_init_birdo, actor_proc_birdo
; -----------------------------------------------------------
eng_actor_birdo_attrs:
	.byte   (actor_attrs::bit6|actor_attrs::bit1|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit1)
; ----------------------------
actor_init_birdo:
	jsr	actor_init_base

	ldy	#0
	lda	pos_x_lo_actor, x
	cmp	#ENG_ACTOR_BIRDO_1_XPOS
	beq	:+ ; if (actor.pos_x != birdo_1_xpos) {
		iny
		cmp	#ENG_ACTOR_BIRDO_2_XPOS
		beq	:+ ; if (actor.pos_x != birdo_2_xpos) {
			iny
		; }
	: ; }
	sty	ENG_ACTOR_BIRDO_TYPE, x
	lda	eng_actor_birdo_attrs, y
	sta	attr_actor, x
	lda	#(ENG_BIRDO_HP)-1
	sta	hp_actor, x
	lda	pos_x_hi_actor, x
	sta	eng_tbl_maskdoors, x
	rts
; -----------------------------------------------------------
actor_birdo_offset_x_lo:
	.byte	<-2, <-8
; -----------------------------------------------------------
actor_proc_birdo:
	jsr	eng_enemy_damage_check
	jsr	col_actor_tile_proc
	lda	#0
	sta	veloc_x_actor, x
	jsr	motion_side_get
	iny
	sty	disp_dir_actor, x
	jsr	render_actor_base
	lda	collision_actor, x
	and	#(actor_collisions::floor)
	beq	:++++
	jsr	actor_proc_base_veloc_reset
	lda	frame_count
	bne	:+
	lda	#ENG_ACTOR_BIRDO_VELOC_Y
	sta	veloc_y_actor, x
	bne	:++++
; ----------------------------
eng_actor_birdo_cycle_mask:
	.byte   $7F
	.byte   $3F
	.byte   $3F

eng_actor_birdo_prj_type:
	.byte	$08
	.byte	$06
	.byte	$04
; ----------------------------
	: ; if ((collision_actor & bit2) && frame_count) {
		ldy	ENG_ACTOR_BIRDO_TYPE, x
		lda	eng_actor_birdo_cycle_mask, y
		and	frame_count
		bne	:+
		lda	zp_byte_EE
		and	#$0C
		bne	:+ ; if (!(eng_actor_birdo_cycle_mask[birdo_type] & frame_count) && (zp_byte_EE & 0x0C)) {
			lda	#28
			sta	timer_actor, x
			lda	#$01
			sta	byte_602
		: ; }

		ldy	timer_actor, x
		bne	actor_proc_birdo_spit
		inc	ENG_ACTOR_BIRDO_WALK_CYCLE, x
		lda	ENG_ACTOR_BIRDO_WALK_CYCLE, x
		and	#ENG_ACTOR_BIRDO_WALK_CYCLE_CUE
		beq	actor_proc_birdo_spit_rts
		jsr	actor_timer_ani_inc

		lda	#ENG_ACTOR_BIRDO_VELOC_X
		ldy	ENG_ACTOR_BIRDO_WALK_CYCLE, x
		bmi	:+ ; if (ENG_ACTOR_BIRDO_WALK_CYCLE[x] >= 0) {
			lda	#<-(ENG_ACTOR_BIRDO_VELOC_X)
		: ; }
		sta	veloc_x_actor, x

		jmp	motion_x
	: ; } else {
		jmp	eng_actor_move_v
	; }
; -----------------------------------------------------------
actor_proc_birdo_spit:
	cpy	#8
	bne	:++++
	jsr	eng_actor_bullet_prj
	bmi	:++++ ; if (this.timer == 8 && eng_actor_bullet_prj() >= 0) {
		ldy	hp_actor, x
		lda	ENG_ACTOR_BIRDO_TYPE, x
		ldx	zp_addr_00
		cmp	#$02
		beq	:+
		cmp	#$01
		bne	:++
		lda	byte_4BB
		and	#$1F
		cmp	eng_actor_birdo_prj_type, y
		bcs	:++
		: ; if ((birdo_type == 2) || (birdo_type == 1 && ((byte_4BB & 0x1F) < eng_actor_birdo_prj_type[y]))) {
			inc	ENG_ACTOR_BIRDO_TYPE, x
			lda	#actor::fireball
			bne	:++
		: ; } else {
			lda	#actor::egg
		: ; }
		sta	obj_id_actor, x

		lda	pos_y_lo_actor, x
		clc
		adc	#$03
		sta	pos_y_lo_actor, x
		ldy	disp_dir_actor, x
		lda	pos_x_lo_actor, x
		adc	actor_birdo_offset_x_lo-1, y
		sta	pos_x_lo_actor, x
		jsr	actor_attr_set
		ldx	actor_index
	: ; }

actor_proc_birdo_spit_rts:
	rts
