.include	"mem.i"
.include	"modes.i"
.include	"misc.i"
.include	"actor.i"

ENG_DOORMASK_COLOFFSET		= $0B

; -----------------------------------------------------------
	.export actor_init_doormask, actor_init_stationary
actor_init_doormask:
	dec	pos_y_lo_actor, x
	dec	pos_y_lo_actor, x
	ldy	#1
	sty	eng_hitboxes_top+ENG_DOORMASK_COLOFFSET
	iny
	sty	eng_hitboxes_left+ENG_DOORMASK_COLOFFSET

actor_init_stationary:
	jsr	actor_init_base
	lda	#0
	sta	veloc_x_actor, x
	rts
; -----------------------------------------------------------
	.export actor_proc_doormask, render_doormask
actor_proc_doormask:
	lda	zp_byte_EE
	beq	:++
	:
		jmp	render_doormask
	:
	lda	eng_timer_doormask_open
	beq	:+
	dec	eng_timer_doormask_open
	bne	:-- ; if (!zp_byte_EE && --eng_timer_doormask_open == 0) {
		; if (eng_timer_doormask_open_was_1) {
			lda	#$80
			sta	apu_dpcm_queue
		: ; }

		lda	eng_doormask_closing
		beq	:+++
		dec	eng_doormask_open_size
		bne	:--- ; if (eng_doormask_closing && !(--eng_doormask_open_size)) {
			lda	#0
			sta	eng_doormask_closing
			lda	#transition_state::door
			sta	eng_transition_type
			jsr	eng_map_reset

			ldy	course_sub
			lda	course_no
			cmp	#6
			bne	:+ ; if (course_no == 6) {
				iny
			: ; }

			cpy	#2
			bcc	:+ ; if (course_sub >= 2) {
				inc	eng_transition_area
				rts
			: ; } else {
				lda	#system_modes::bonus_round
				sta	proc_id_system
				rts
			; }
		: ; } else if (!eng_doormask_closing) {
			lda	eng_doormask_open_size
			beq	:+++
			cmp	#$30
			beq	:++
			lda	zp_byte_EE
			and	#$04
			bne	:+++ ; if (eng_doormask_open_size.not_in(0, 0x30) && !(zp_byte_EE & 0x04)) {
				inc	eng_doormask_open_size
				lda	frame_count
				and	#$03
				bne	:+ ; if (frame_count % 4) {
					dec	eng_hitboxes_top+ENG_DOORMASK_COLOFFSET
					inc	eng_hitboxes_left+ENG_DOORMASK_COLOFFSET
				: ; }

				jmp	:++
			: ; } else if (eng_doormask_open_size == 0x30) {
				lda	collision_actor, x
				and	#(actor_collisions::player_col)
				beq	:+
				lda	pos_y_lo_actor, x
				cmp	pos_y_lo_player
				bcs	:+
				lda	collision_player
				and	#(actor_collisions::floor)
				beq	:+
				lda	player_item_held
				bne	:+ ; if ((collision_actor[x] & player_col) && pos_y_lo_player < pos_y_lo_actor[x] && (collision_player & floor) && !player_item_held) {
					lda	#player_state::maskexit
					sta	proc_id_player
					lda	#$30
					sta	eng_timer_player_state
					lda	#<-4
					sta	veloc_y_player
					lda	#$80
					sta	apu_dpcm_queue
					inc	eng_doormask_closing
				; }
			: ; }
		; }
	; }

render_doormask:
	lda	zp_byte_EF
	bne	:++++++ ; if (!zp_byte_EF) {
		lda	obj_id_actor, x
		sec
		sbc	#(actor::doormask_right)-1
		sta	disp_dir_actor, x

		lda	eng_doormask_open_size
		sta	zp_byte_07
		lsr	a
		lsr	a
		eor	#$FF
		sec
		adc	pos_y_rel_actor
		sta	pos_y_rel_actor
		ldy	eng_door_timer_ani
		beq	:+ ; if (((eng_doormask_open_size/4)^0xFF)+(pos_y_rel_actor+1)) {
			ldy	#$10
		: ; }
		sty	zp_byte_F4

		lda	#$8E
		ldy	zp_byte_07
		beq	:+ ; if (zp_byte_07) {
			lda	#$92
		: ; }
		jsr	render_actor_base_draw

		lda	zp_byte_07
		tay
		lsr	a
		clc
		adc	pos_y_rel_actor
		adc	#8
		cpy	#0
		bne	:+ ; if (zp_byte_07) {
			adc	#7
		: ; }
		sta	zp_addr_00
		jsr	actor_sprite_alloc

		ldx	#$9A
		lda	zp_byte_07
		beq	:++ ; if (zp_byte_07) {
			lda	eng_doormask_closing
			beq	:+ ; if (eng_doormask_closing) {
				ldy	#$10
			: ; }

			ldx	#$96
		: ; }
		sty	zp_byte_F4

		jsr	render_actor_base_tiles
		jsr	render_actor_base_tiles
	: ; }

	ldx	actor_index
	rts
