.include	"system/fds.i"

.include	"mem.i"
.include	"tunables.i"
.include	"actor.i"
.include	"modes.i"
.include	"macros.i"

.include	"unk.i"

ENG_ACTOR_TILE_NUDGE	= $10

	.export motion_side_get
motion_side_get:
	lda	pos_x_lo_player
	sbc	pos_x_lo_actor, x
	sta	eng_actor_physics_side_lo

	lda	pos_x_hi_player
	ldy	#0
	sbc	pos_x_hi_actor, x
	bcs	:+ ; if (pos_x_hi_player < pos_x_hi_actor[x]) {
		iny
	: ; }

	rts
; -----------------------------------------------------------
ENG_PHYSICS_VELOC_MASKED	= zp_work+$00
ENG_PHYSICS_VELOC_MACRO		= zp_work+$01
ENG_PHYSICS_VELOC_HIGH_OFF	= zp_work+$02

	.export motion_y, motion_x
motion_y:
	txa
	clc
	adc	#PHYSICS_X_Y_OFFSET
	tax

motion_x:
	lda	veloc_x_actor, x
	clc
	adc	accel_x_actor, x
	pha
		asl	a
		asl	a
		asl	a
		asl	a
		sta	ENG_PHYSICS_VELOC_MACRO
	pla
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	cmp	#8
	bcc	:+ ; if ((next_veloc/16) <= 8) {
		ora	#%11110000
	: ; }
	sta	ENG_PHYSICS_VELOC_MASKED

	ldy	#0
	asl	a
	bcc	:+ ; if (ENG_PHYSICS_VELOC_MASKED < 0) {
		dey
	: ; }
	sty	ENG_PHYSICS_VELOC_HIGH_OFF

	lda	subpix_x_actor, x
	clc
	adc	ENG_PHYSICS_VELOC_MACRO
	sta	subpix_x_actor, x
	lda	pos_x_lo_actor, x
	adc	ENG_PHYSICS_VELOC_MASKED
	sta	pos_x_lo_actor, x
	rol	ENG_PHYSICS_VELOC_MACRO

	cpx	#PHYSICS_X_Y_OFFSET
	bcs	:+
	lda	#0
	sta	eng_actor_noholdtop, x
	lda	obj_id_actor, x
	cmp	#actor::bullet
	beq	:+
	cmp	#actor::beezo_dive
	beq	:+
	cmp	#actor::beezo_normal
	beq	:+
	ldy	eng_level_h
	beq	:++
	: ; if (x >= PHYSICS_X_Y_OFFSET || (this.obj_id.not_in(bullet, beezo_dive, beezo_normal) && eng_level_h)) {
		lsr	ENG_PHYSICS_VELOC_MACRO
		lda	pos_x_hi_actor, x
		adc	ENG_PHYSICS_VELOC_HIGH_OFF
		sta	pos_x_hi_actor, x
	: ; }

	ldx	actor_index
	rts
; -----------------------------------------------------------
	.export motion_dir_flip
motion_dir_flip:
	lda	veloc_x_actor, x
	abs_m
	sta	veloc_x_actor, x
	beq	:+
		lda	disp_dir_actor, x
		eor	#(ACTOR_DIR_LEFT|ACTOR_DIR_RIGHT)
		sta	disp_dir_actor, x
	:

	jmp	motion_x
; -----------------------------------------------------------
col_actor_tile_proc_x_push:
	.byte	<-(ENG_ACTOR_TILE_NUDGE)
	.byte	ENG_ACTOR_TILE_NUDGE
; ----------------------------
	.export col_player_climb_box
col_player_climb_box:
	jsr	eng_col_next_player_hitbox
	tay
	lda	veloc_y_obj, x
	bmi	:+ ; if (obj[x].veloc_y >= 0) {
		iny
	: ; }

	jsr	col_player_climbtile
	bcs	:+
	lda	zp_addr_00
	cmp	#$82
	beq	:+ ; if (!col_player_climbtile() && zp_addr_00 != 0x82) {
		clc
	: ; }

	dex
	rts
; -----------------------------------------------------------
	.export col_actor_tile_proc_b, col_actor_tile_proc
col_actor_tile_proc_b:
	lda	#4
	bne	:+

col_actor_tile_proc:
	lda	#0

	: sta	zp_byte_07

	lda	#0
	sta	zp_addr_0A+1
	sta	zp_byte_0E
	jsr	eng_col_next_player_hitbox
	sta	zp_byte_08
	lda	veloc_y_obj, x
	bpl	:+ ; if (obj[x].veloc_y < 0) {
		jsr	eng_col_next_player_tile
		inc	zp_byte_07
		inc	zp_byte_08
		bne	col_actor_tile_proc_apply_accel
	: ; }

	inc	zp_byte_07
	inc	zp_byte_08
	jsr	eng_col_next_player_tile

	lda	obj_id_obj, x
	cmp	#actor::cobrat_jar
	beq	:+
	cmp	#actor::cobrat_sand
	beq	:+
	lda	zp_addr_00
	sec
	sbc	#$8A
	cmp	#2
	bcs	:+ ; if (actor.obj_id.not_in(cobrat_jar, cobrat_sand) && (zp_addr_00 - 0x8B) < 2) {
		asl	a
		adc	#1
		sta	veloc_y_obj, x
		lda	#actor_states::sinking
		sta	proc_id_obj, x
		lda	#$FF
		sta	timer_obj, x
	: ; }

	lda	zp_addr_00
	sta	zp_byte_0E
	sec
	sbc	#$65
	cmp	#$02
	bcs	:+++
	ldy	timer_actor_stun-1, x
	bne	:+++ ; if ((zp_byte_0E - 0x66) < 2 && !byte_437[x]) {
		ldy	obj_id_obj, x
		cpy	#actor::vegetable_small
		bcc	:+ ; if (actor.obj_id >= vegetable_small) {
			tay
			lda	veloc_y_obj, x
			cmp	#3
			bcs	:+++
			lda	zp_byte_0D
			and	#$03
			bne	:+++ ; if (obj[x].veloc_y < 3 && !(zp_byte_0D)) {
				lda	col_actor_tile_proc_x_push, y
				sta	veloc_x_obj, x
				sta	zp_addr_0A+1
				bne	:+++
			; }
		: ; } else {
			ldy	veloc_x_obj, x
			beq	:+
			eor	disp_dir_obj, x
			lsr	a
			bcs	:+ ; if (obj[x].veloc_x != 0 && obj[x].disp_dir != ACTOR_DIR_RIGHT) {
				dec	timer_ani_obj, x
				dec	timer_ani_obj, x
			: ; }
			inc	timer_ani_obj, x
		: ; }
	; }

col_actor_tile_proc_apply_accel:
	lda	veloc_x_obj, x
	clc
	adc	accel_x_actor-1, x
	bmi	:+ ; if ((obj[x].veloc_x + obj[x].accel_x) >= 0) {
		inc	zp_byte_07
		inc	zp_byte_08
	: ; }

	jsr	eng_col_next_player_tile
	dex
	rts
; -----------------------------------------------------------
eng_col_next_player_tile:
	ldy	zp_byte_08
	jsr	eng_collision_tile_type
	ldy	zp_byte_07
	lda	col_actor_tile_proc_types, y
	tay
	lda	zp_addr_00
	jsr	col_player_type
	bcc	:+ ; if (col_player_type(col_actor_tile_proc_types[zp_byte_07])) {
		ldy	zp_byte_07
		lda	col_actor_tile_proc_dirs, y
		ora	collision_obj, x
		sta	collision_obj, x
	: ; }

	inc	zp_byte_07
	inc	zp_byte_08
	rts
; ----------------------------
eng_col_next_player_hitbox:
	inx

	lda	collision_obj, x
	sta	zp_addr_0C+1
	and	#<~(actor_collisions::ceil|actor_collisions::floor|actor_collisions::left|actor_collisions::right)
	sta	collision_obj, x

	ldy	eng_actor_tbl_492-1, x
	lda	eng_hitboxes, y

eng_col_next_player_hitbox_rts:
	rts
; ----------------------------
col_actor_tile_proc_types:
	.byte   2, 1, 2, 2
	.byte	0, 0, 0, 0
; ----------------------------
col_actor_tile_proc_dirs:
	.byte	actor_collisions::ceil, actor_collisions::floor, actor_collisions::left, actor_collisions::right
	.byte	actor_collisions::ceil, actor_collisions::floor, actor_collisions::left, actor_collisions::right
; -----------------------------------------------------------
	.export eng_col_actor
eng_col_actor:
	lda	#0
	sta	accel_x_actor, x
	lda	collision_actor, x
	and	#%00001111
	sta	collision_actor, x
	lda	proc_id_actor, x
	cmp	#actor_states::bombboom
	bne	:+ ; if (this.proc_id == bombboom) {
		ldy	#6
		bne	:+++
	: cmp	#actor_states::sinking
	beq	:+
	cmp	#actor_states::normal
	bne	eng_col_next_player_hitbox_rts
	: lda	timer_actor_held, x
	bne	eng_col_next_player_hitbox_rts ; } else if (this.proc_id.in(sinking, normal) && this.timer_held == 0) {
		ldy	col_box_actor, x
	: ; } else return;

	lda	eng_hitboxes_width, y
	sta	zp_byte_09
	lda	#0
	sta	zp_byte_00
	lda	eng_hitboxes_left, y
	bpl	:+ ; if (eng_hitboxes_left[y] < 0) {
		dec	zp_byte_00
	: ; }

	clc
	adc	pos_x_lo_actor, x
	sta	zp_map_pointer
	lda	pos_x_hi_actor, x
	adc	zp_byte_00
	sta	zp_byte_01
	lda	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		sta	zp_byte_01
	: ; }

	lda	eng_hitboxes_height, y
	sta	zp_byte_0B
	lda	#0
	sta	zp_byte_00
	lda	eng_hitboxes_top, y
	bpl	:+ ; if (eng_hitboxes_top[y] > 0x7F) {
		dec	zp_byte_00
	: ; }

	clc
	adc	pos_y_lo_actor, x
	sta	zp_byte_07
	lda	pos_y_hi_actor, x
	adc	zp_byte_00
	sta	zp_byte_03

	eng_col_actor_loop: ; for (x; x >= 0; x--) {
		stx	zp_byte_ED
		txa
		bne	:++++ ; if (x == player) {
			lda	eng_player_in_rocket
			ora	player_lock
			bne	:+
			lda	proc_id_obj, x
			cmp	#player_state::lift
			bcc	:++
			: jmp	eng_col_actor_loop_end
			: ; if (!this.in_rocket && !this.locked && this.proc_id < lift) {
				ldy	actor_index
				lda	timer_actor_throw, y
				beq	:+
				cmp	#32
				bcc	:--
				: ; if (actor.timer_throw == 0 || actor.timer_throw >= 32) {
					ldy	player_crouch, x
					jmp	:++++++
				; } else continue;
			; } else continue;
		: ; } else {
			ldy	actor_index
			lda	proc_id_actor, y
			cmp	#actor_states::bombboom
			beq	:+
			lda	actor_mode, y
			and	#actor_flags::no_actor_col
			bne	eng_col_actor_loop_end
			: ; if (actor.proc_id == bombboom || !(actor.mode & no_actor_col)) {
				lda	proc_id_actor-1, x
				cmp	#actor_states::bombboom
				bne	:+ ; if (this.proc_id == bombboom) {
					ldy	#6
					bne	:+++
				: cmp	#actor_states::sinking
				beq	:+
				cmp	#actor_states::normal
				bne	eng_col_actor_loop_end
				: ; } else if (this.proc_id.in(sinking, null)) {
					lda	timer_actor_held-1, x
					bne	eng_col_actor_loop_end
					lda	collision_actor-1, x
					and	#actor_flags::tilemap2
					bne	eng_col_actor_loop_end
					lda	actor_mode-1, x
					and	#$04
					bne	eng_col_actor_loop_end ; if (!zp_byte_A7[x] && !(zp_byte_5A[x] & 0x10) && !(byte_46D[x] & 0x04)) {
						ldy	col_box_actor-1, x
					; } else continue;
				: ; } else continue;
			; } else continue;
		: ; }

		lda	eng_hitboxes_width, y
		sta	zp_byte_0A
		lda	#0
		sta	zp_byte_00
		lda	eng_hitboxes_left, y
		bpl	:+ ; if (eng_hitboxes_left[y] < 0) {
			dec	zp_byte_00
		: ; }

		clc
		adc	pos_x_lo_obj, x
		sta	zp_map_pointer+1
		lda	pos_x_hi_obj, x
		adc	zp_byte_00
		sta	zp_byte_02
		lda	eng_level_h
		bne	:+ ; if (!eng_level_h) {
			sta	zp_byte_02
		: ; }

		lda	eng_hitboxes_height, y
		sta	zp_byte_0C
		lda	#$00
		sta	zp_byte_00
		lda	eng_hitboxes_top, y
		bpl	:+ ; if (eng_hitboxes_top > 0x7F) {
			dec	zp_byte_00
		: ; }

		clc
		adc	pos_y_lo_obj, x
		sta	zp_byte_08
		lda	pos_y_hi_obj, x
		adc	zp_byte_00
		sta	zp_byte_04
		jsr	col_player_calc
		bcs	:+ ; if (!col_player_calc()) {
			lda	zp_byte_0B
			pha

			jsr	eng_col_actor_base

			pla
			sta	zp_byte_0B
		: ; }

	eng_col_actor_loop_end:
		dex
		bmi	:+ ; if (x >= 0) {
			jmp	eng_col_actor_loop
		; } else break;
	: ; }

	ldx	actor_index
:
	rts
; -----------------------------------------------------------
eng_col_actor_base:
	txa
	bne	:+
	lda	player_item_held
	beq	:+
	lda	player_item_held_id
	cmp	actor_index
	beq	:-
	: ; if (x || !player_item_held || player_item_held_id != actor_index) {
		ldy	actor_index
		lda	obj_id_actor, y
		tay
		lda	collision_actor_types, y
		jsr	tbljmp
		.addr	eng_col_actor_0
		.addr	eng_col_actor_1
		.addr	eng_col_actor_2
		.addr	eng_col_actor_3
		.addr	eng_col_actor_4
	; }
; -----------------------------------------------------------
eng_col_actor_4:
	txa
	bne	:++
	lda	joypad_p1
	and	#joypad_button::up
	beq	:++
	lda	collision_player
	and	#(actor_collisions::floor)
	beq	:++
	lda	eng_col_res_h
	cmp	#$FA
	bcs	:++
	lda	eng_door_timer_ani
	ora	eng_timer_subspace
	bne	:++
	lda	player_item_held
	beq	:+
	ldy	player_item_held_id
	lda	obj_id_actor, y
	cmp	#actor::key
	bne	:++
	: ; if (joypad_p1_up && player_col_floor && eng_col_res_h < 0xFA && (eng_door_timer_ani | eng_timer_subspace) && (player_item_held || eng_held_actor_type == key)) {
		ldy	actor_index
		lda	pos_x_lo_actor, y
		sta	pos_x_lo_player
		lda	pos_x_hi_actor, y
		sta	pos_x_hi_player
		jsr	eng_player_backup
		lda	#transition_state::subspace
		sta	eng_transition_type
		jmp	eng_tile_door_check_finish
	: ; } else {
		rts
	; }
; -----------------------------------------------------------
eng_col_actor_0:
	ldy	actor_index
	txa
	beq	eng_col_actor_0_norun
	lda	timer_actor_throw, y
	bne	:+
	lda	proc_id_actor, y
	cmp	#actor_states::bombboom
	beq	:+
	txa
	tay
	dey
	ldx	actor_index
	inx
	lda	proc_id_actor, y
	cmp	#actor_states::bombboom
	beq	:+
	lda	timer_actor_throw, y
	beq	:++++++
	lda	collision_obj, x
	and	#actor_collisions::damage
	bne	:++++++
	: ; if ((actor_index != 0 && (actor.timer_throw != 0 || actor.proc_id == bombboom)) || (actor.timer_throw != 0 && !(actor.col_damage))) {
		lda	eng_timer_actor_2, y
		ora	timer_actor_flash, y
		bne	:+++ ; if (actor.timer_2 == 0 && actor.timer_flash == 0) {
			lda	actor_mode, y
			and	#(actor_flags::death_noise)
			beq	:+ ; if (actor_flags_death_noise) {
				jsr	eng_col_actor_cry
			: ; }

			lda	hp_actor, y
			sec
			sbc	#1
			sta	hp_actor, y
			bmi	:+ ; if (hp_actor >= 0) {
				jsr	eng_col_actor_cry
				lda	#$21
				sta	timer_actor_flash, y
				lsr	a
				sta	timer_actor_stun, y
				bne	:++
			: ; } else {
				lda	collision_actor, y
				ora	#actor_collisions::damage
				sta	collision_actor, y
				lda	#<-($20)
				sta	veloc_y_actor, y
				lda	veloc_x_actor, y
				sta	zp_addr_00
				asl	a
				ror	zp_addr_00
				lda	zp_addr_00
				sta	veloc_x_actor, y
			; }
		: ; }

		lda	obj_id_obj, x
		cmp	#actor::vegetable_small
		bcs	:+ ; if (actor_type < vegetable_small) {
			jsr	eng_col_actor_hurt
		: ; }
	: ; } else if () {
		ldx	zp_byte_ED
		rts
	; }
; ----------------------------
eng_col_actor_0_veloc_x:
	.byte	<-(8), 8
; ----------------------------
eng_col_actor_0_norun:
	lda	zp_byte_EE
	and	#$08
	bne	eng_col_actor_0_rts
	lda	obj_id_actor, y
	bne	:+ ; if (actor.obj_id == heart) {
		sta	proc_id_actor, y

		lda	#$04
		sta	byte_602

		ldy	hp_player_max
		lda	hp_player
		clc
		adc	#16
		sta	hp_player
		cmp	eng_hearts_max_values, y
		bcc	eng_col_actor_0_rts
		jmp	eng_player_transition_reload_hearts_max
	: cmp	#actor::starman
	bne	:+ ; } else if (eng_running_actor_type == starman) {
		lda	#63
		sta	timer_player_star

		lda	#$10
		sta	apu_music_base_req

		lda	#actor_states::null
		sta	proc_id_actor, y

	eng_col_actor_0_rts:
		rts
	: cmp	#actor::whale_spout
	bne	:++ ; } else if (eng_running_actor_type == whale_spout) {
		lda	zp_actor_work_byte_1, y
		cmp	#$DC
		bcs	:+
		lda	timer_player_star
		beq	eng_col_actor_0_finish ; if (zp_actor_work_byte_1 < 0xDC && timer_player_star != 0) {
			lda	#$DC
			sta	zp_actor_work_byte_1, y
			lda	#0
			sta	veloc_y_actor, y
		: ; }

	LB51E:
		rts
	: cmp	#actor::wart
	bne	:+ ; } else if (eng_running_actor_type == wart) {
		lda	zp_actor_work_byte_2, x
		bne	LB51E
	: ldy	timer_player_star
	beq	:++++ ; } else if (timer_player_star != 0) {
		ldx	actor_index
		cmp	#actor::autobomb_fire
		beq	:+
		cmp	#actor::fireball
		bne	:++
		: ; if (actor.obj_id.in(fireball, autobomb_fire)) {
			lda	#0
			sta	actor_mode, x
			jsr	sub_ABF8
			jmp	:++
		: ; } else {
			jsr	motion_side_get
			lda	eng_col_actor_0_veloc_x, y
			sta	veloc_x_actor, x
			lda	#<-($20)
			sta	veloc_y_actor, x
			lda	collision_actor, x
			ora	#actor_collisions::damage
			sta	collision_actor, x
		: ; }

		ldx	zp_byte_ED
		ldy	actor_index
		rts
	: ; }

eng_col_actor_0_finish:
	ldy	actor_index
	lda	proc_id_actor, y
	cmp	#actor_states::bombboom
	beq	:+
	lda	actor_mode, y
	and	#actor_flags::harm_top
	bne	:+
	jsr	eng_col_calc_flags
	lda	eng_col_current_mask
	and	#(actor_collisions::ceil|actor_collisions::left|actor_collisions::right)
	beq	:++
	: ; if (eng_running_enemy_state == bombboom || eng_running_enemy_harm_top || eng_col_current_mask & (ceil|left|right)) {
		jmp	eng_col_player_hurt
	: ; } else {
	eng_col_pickup_do:
		lda	#0
		sta	player_airborne
		ldx	actor_index
		lda	collision_actor, x
		ora	#(actor_collisions::player_top)
		sta	collision_actor, x

		lda	actor_mode, x
		and	#actor_flags::cant_lift
		bne	:+
		bit	joypad_p1
		bvc	:+
		lda	player_item_held
		ora	player_crouch
		bne	:+ ; if (actor_can_lift && joypad_p1_face_b && !player_item_held && !player_crouch) {
			sta	collision_actor, x
			stx	player_item_held_id
			sta	timer_actor_shake, x
			lda	#7
			sta	timer_actor_held, x
			jsr	eng_lift_item_create_sprite_ani
			lda	obj_id_actor, x
			cmp	#actor::pidgit
			bne	:+ ; if (actor.obj_id == pidgit) {
				jsr	sub_C231
			; }
		: ; }

		ldx	zp_byte_ED
		rts
	; }
; -----------------------------------------------------------
eng_col_actor_2:
	ldy	actor_index
	txa
	beq	eng_col_actor_2_norun ; if (x != 0) {
		lda	timer_actor_throw, y
		bne	LB5D3
		lda	obj_id_actor, y
		cmp	#actor::key
		beq	LB5D6 ; if (actor_type != key) {
			jsr	eng_col_calc_flags
			lda	eng_col_current_mask
			and	disp_dir_obj, x
			beq	:+ ; if ((eng_col_current_mask & obj[x].disp_dir)) {
				dex
				jsr	motion_dir_flip
				ldx	zp_byte_ED
			: ; }

			jsr	eng_col_get_clamp
			cpy	#0
			beq	LB5D6
		; }
		LB5D3: ; if (eng_col_get_clamp()) {
			jmp	eng_col_actor_running_hurt
		LB5D6: ; } else {
			rts
		; }
	; }
; ----------------------------
eng_col_actor_2_duck_flag:
	.byte	actor_collisions::ceil, actor_collisions::floor
; ----------------------------
	eng_col_actor_2_norun: ; } else {
		lda	collision_actor, y
		ora	#actor_collisions::player_col
		sta	collision_actor, y
		jsr	eng_col_calc_flags

		lda	eng_col_current_mask
		and	disp_dir_player
		beq	:+ ; if (eng_col_current_mask & disp_dir_player) {
			jsr	eng_player_bounds_physics
		: ; }

		lda	eng_col_current_mask
		and	#actor_collisions::floor
		beq	:+ ; if (eng_col_current_mask & floor) {
			jsr	eng_col_pickup_do
		: ; }

		jsr	eng_col_get_clamp
		cpy	#1
		bne	:+
		ldy	actor_index
		lda	veloc_y_actor, y
		beq	:+ ; if (eng_col_get_clamp() == 1 && eng_running_actor_veloc_y) {
			and	#$80
			asl	a
			rol	a
			tay
			lda	eng_col_current_mask
			and	eng_col_actor_2_duck_flag, y
			beq	:+ ; if (eng_col_current_mask & eng_col_actor_2_duck_flag[]) {
				ldy	actor_index
				lda	veloc_y_actor, y
				abs_m
				sta	veloc_y_actor, y
				lda	#1
				sta	player_crouch
				lda	#4
				sta	eng_player_ani_frame
				lda	#$10
				sta	eng_timer_player_state
			: ; }
		; }

		rts
	; }
; ----------------------------
eng_col_actor_3:
	txa
	beq	:-
	jmp	eng_col_actor_running_hurt
; -----------------------------------------------------------
eng_col_actor_1:
	ldy	actor_index
	txa
	bne	:++++
	lda	proc_id_actor, y
	cmp	#actor_states::bombboom
	bne	:+
	lda	timer_player_star
	beq	:+++
	eng_col_actor_1_rts: ; if (x == player && eng_running_enemy_state == bombboom && timer_player_star != 0) {
		rts
	: ; } else if (x == player && eng_running_enemy_state != bombboom) {
		jsr	eng_col_calc_flags
		lda	eng_col_current_mask
		and	#actor_collisions::ceil
		beq	:+
		lda	player_item_held
		bne	eng_col_actor_1_rts ; if ((this_col & ceil) && !player_item_held) {
			ldy	actor_index
			sty	player_item_held_id
			lda	#1
			sta	timer_actor_held, y
			inc	player_item_held
		: ; }

		lda	eng_col_current_mask
		and	#actor_collisions::floor
		beq	eng_col_actor_1_rts
		jmp	eng_col_pickup_do
	: ; } else if (x == player && eng_running_enemy_state == bombboom && timer_player_star == 0) {
		jmp	eng_col_player_hurt
	: ; } else {
		lda	obj_id_obj, x
		cmp	#actor::wart
		bne	:++
		lda	timer_obj, x
		beq	LB6CD ; if (this.obj_id == wart && this.timer != 0) {
			lda	#actor_states::null
			sta	proc_id_actor, y
			jsr	eng_col_actor_hurt
			lda	#96
			sta	timer_actor_flash-1, x
			lsr	a
			sta	timer_actor_stun-1, x

			lda	hp_actor-1, x
			bne	:+ ; if (!hp_actor-1[x]) {
				inc	eng_scroll_lock_x
			: ; }

			rts
		: cmp	#actor::vegetable_small
		bcs	:--
		cmp	#actor::wart_bubble
		bne	:+ ; } else if (this.obj_id == wart_bubble) {
			lda	#actor_states::smokepoof
			sta	proc_id_actor, y
			lda	#30
			sta	timer_actor, y
			rts
		: ; } else if (this.obj_id < vegetable_small) {
			lda	proc_id_actor, y
			cmp	#actor_states::bombboom
			beq	:+++ ; if (this.proc_id != bombboom) {
				lda	obj_id_actor, y
				cmp	#actor::bighead
				beq	:++ ; if (this.obj_id != bighead) {
					lda	#<-24
					sta	veloc_y_actor, y
					stx	zp_addr_00
					ldx	veloc_x_actor, y
					bmi	:+ ; if (eng_running_actor_veloc_x >= 0) {
						lda	#24
					: ; }
					sta	veloc_x_actor, y
					ldx	zp_addr_00
				: ; }

			eng_col_actor_running_hurt:
				ldy	actor_index
			: ; }

			jsr	eng_col_actor_hurt
			bne	:+ ; if (!eng_col_actor_hurt()) {
				lda	veloc_x_obj, x
				asl	a
				ror	veloc_x_obj, x
				asl	a
				ror	veloc_x_obj, x
			: ; }
		; }

	LB6CD:
		rts
	; }
; -----------------------------------------------------------
	.export eng_col_player_hurt
eng_col_player_hurt:
	lda	timer_player
	bne	:++
	lda	hp_player
	sec
	sbc	#ENG_HEALTH_METER_Y_OFFSET
	bcc	:+++ ; if ((hp_player) > ENG_HEALTH_METER_Y_OFFSET) {
		; if (hurt_timer == 0) {
			sta	hp_player

			lda	#ENG_PLAYER_HIT_COOLDOWN_TIME
			sta	timer_player
			lda	eng_player_screen_x
			sec
			sbc	pos_x_rel_actor
			asl	a
			asl	a
			sta	veloc_x_player

			lda	#ENG_PLAYER_HURT_VELOC_Y
			ldy	veloc_y_player
			bpl	:+ ; if (veloc_y_player < 0) {
				lda	#0
			: ; }
			sta	veloc_y_player

			lda	#$10
			sta	apu_dpcm_queue
		: ; }

		rts
	: ; } else { 
		lda	#ENG_PLAYER_HURT_VELOC_Y
		sta	veloc_y_player
		lda	#ENG_PLAYER_DEATH_FALL_TIME
		sta	eng_timer_player_state
		ldy	actor_index
		bmi	:+
			lsr	a
			sta	timer_actor_stun, y
		:

		jmp	eng_player_kill
	; }
; -----------------------------------------------------------
eng_col_actor_hurt:
	lda	eng_timer_actor_2-1, x
	ora	timer_actor_flash-1, x
	bne	:+++ ; if (!(eng_timer_actor_2[x-1]|timer_actor_flash[x-1])) {
		lda	actor_mode-1, x
		and	#(actor_flags::death_noise)
		beq	:+ ; if (actor_mode[x-1] & death_noise) {
			jsr	eng_col_actor_cry
		: ; }

		dec	hp_actor-1, x
		bmi	:+ ; if (hp_actor[x-1] >= 0) {
			lda	#ENG_ACTOR_HIT_COOLDOWN_TIME
			sta	timer_actor_flash-1, x
			lsr	a
			sta	timer_actor_stun-1, x

		eng_col_actor_cry:
			lda	#$20
			sta	byte_602
			rts
		: ; } else {
			lda	collision_obj, x
			ora	#actor_collisions::damage
			sta	collision_obj, x
			lda	#ENG_ACTOR_COOLDOWN_HURT_VELOC_Y
			sta	veloc_y_obj, x
			lda	veloc_x_actor, y
			sta	veloc_x_obj, x
			lda	#0
		; }
	: ; }

	rts
; -----------------------------------------------------------
eng_col_calc_flags:
	lda	#0
	sta	eng_col_current_mask
	ldy	actor_index
	lda	eng_col_res_v
	cmp	#$F6
	bcs	:+++ ; if (eng_col_res_v < 0xF6) {
		lda	pos_x_lo_actor, y
		ldy	#ACTOR_DIR_LEFT
		cmp	pos_x_lo_obj, x
		bmi	:+ ; if (eng_running_pos_x_lo_actor > pos_x_lo_player[x]) {
			ldy	#ACTOR_DIR_RIGHT
		: ; }
		sty	eng_col_current_mask

		tya
		and	disp_dir_obj, x
		beq	:+
		ldy	actor_index
		lda	eng_actor_noholdtop, y
		bne	:+ ; if (disp_dir_obj[x] && eng_running_actor_tbl_4A4) {
			lda	veloc_x_actor, y
			sta	accel_x_actor-1, x
		: ; }

		rts
	: ; } else {
		lda	pos_y_lo_actor, y
		cpx	#1
		bcs	:+ ; if (x < 1) {
			ldy	player_crouch
			sec
			sbc	eng_col_calc_flags_duck_adj, y
		: ; }

		cmp	pos_y_lo_obj, x
		bmi	:+++ 
		lda	veloc_y_obj, x
		bmi	eng_col_calc_flags_rts ; if (obj[x].pos_y_lo >= 0 && obj[x].veloc_y >= 0) {
			ldy	actor_index
			lda	eng_actor_noholdtop, y
			bne	:+ ; if (!eng_running_actor_tbl_4A4) {
				lda	a:veloc_x_actor, y
				sta	accel_x_actor-1, x
			: ; }

			ldy	#0
			inc	eng_col_res_v
			inc	eng_col_res_v
			bpl	:+ ; if ((eng_col_res_v += 2) < 0) {
				dey
			: ; }

			lda	eng_col_res_v
			clc
			adc	pos_y_lo_obj, x
			sta	pos_y_lo_obj, x
			tya
			adc	pos_y_hi_obj, x
			sta	pos_y_hi_obj, x

			ldy	#actor_collisions::floor
			bne	:+++
		: lda	veloc_y_obj, x
		beq	:+
		bpl	eng_col_calc_flags_rts ; } else {
			: ldy	#actor_collisions::ceil
		: ; } if ((obj[x].pos_y_lo >= 0 && obj[x].veloc_y >= 0) || (obj[x].pos_y_lo < 0 && obj[x].veloc_y == 0)) {
			sty	eng_col_current_mask

			ldy	actor_index
			lda	eng_actor_noholdtop, y
			bne	:+ ; if (!eng_running_actor_tbl_4A4) {
				lda	veloc_y_actor, y
				sta	accel_y_obj, x
			: ; }

			lda	#0
			sta	veloc_y_obj, x
			lda	subpix_y_actor, y
			sta	subpix_y_obj, x
			inc	timer_ani_obj, x
		eng_col_calc_flags_rts: ; }

		rts
	; }
; ----------------------------
eng_col_calc_flags_duck_adj:
	.byte   $0B, $10
; -----------------------------------------------------------
eng_col_get_clamp:
	ldy	#0
	lda	collision_obj, x
	ora	eng_col_current_mask
	and	#(actor_collisions::ceil|actor_collisions::floor)
	cmp	#(actor_collisions::ceil|actor_collisions::floor)
	beq	:+
	lda	collision_obj, x
	ora	eng_col_current_mask
	and	#(actor_collisions::left|actor_collisions::right)
	cmp	#(actor_collisions::left|actor_collisions::right)
	bne	:++ ; if (((collision_obj[x] & (ceil|floor)) != (ceil_floor)) && (((collision_obj[x]|eng_col_current_mask) & (ceil|floor)) == (left|right))) {
		iny
	: ; } if (((collision_obj[x] & (ceil|floor)) == (ceil_floor))) {
		iny
	: ; }

	rts
