.include	"mem.i"

SPAWNJAR_TIMER_ANI	= 80

	.export actor_init_spawnjar
actor_init_spawnjar:
	jsr	actor_init_base
	lda	#SPAWNJAR_TIMER_ANI
	sta	timer_ani_actor, x
	rts
