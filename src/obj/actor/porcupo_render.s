.include	"system/ppu.i"

.include	"mem.i"



; -----------------------------------------------------------
actor_porcupo_pos_idx:
	.byte   4, 0

actor_porcupo_pos_x:
	.byte   <-1, <-1, 0, 0
	.byte	1, 1, 0, 0

actor_porcupo_pos_y:
	.byte	1, 0, 0, 1
	.byte	1, 0, 0, 1
; ----------------------------
	.export actor_proc_porcupo
actor_proc_porcupo:
	jsr	render_actor_base_rocket_check
	lda	zp_byte_EE
	and	#$0C
	bne	:+ ; if (!(zp_byte_EE & 0x0C)) {
		lda	timer_ani_actor, x
		and	#%00001100
		lsr	a
		lsr	a
		sta	zp_addr_00
		lda	disp_dir_actor, x
		tax
		lda	actor_porcupo_pos_idx-1, x
		adc	zp_byte_F4
		tay
		txa
		asl	a
		asl	a
		adc	zp_addr_00
		tax
		lda	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		adc	actor_porcupo_pos_y-4, x
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_V, y
		lda	oam_buffer+0*OBJ_SIZE+OBJ_POS_H, y
		adc	actor_porcupo_pos_x-4, x
		sta	oam_buffer+0*OBJ_SIZE+OBJ_POS_H, y
		ldx	actor_index
	: ; }

	rts
