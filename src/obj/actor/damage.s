.include	"mem.i"
.include	"actor.i"
.include	"modes.i"

.include	"unk.i"

	.export eng_enemy_damage_check
eng_enemy_damage_check:
	lda	collision_actor, x
	and	#(actor_collisions::damage)
	beq	:+++++ ; if (collision_actor[x] & damage) {
		lda	timer_actor_held, x
		beq	:+ ; if (timer_actor_held[x]) {
			lda	#0
			sta	player_item_held
		: ; }

		ldy	obj_id_actor, x
		lda	eng_actor_flag_tbl, y
		and	#$08
		asl	a
		bne	:+ ; if (!(eng_actor_tbl_9825[this.obj_id] & 0x08)) {
			lda	byte_602
			bne	:++ ; if (!byte_602) {
				lda	#$40
			; }
		: ; } if ((eng_actor_tbl_9825[this.obj_id] & 0x08) || !byte_602) {
			sta	byte_602
		: ; }

		cpy	#actor::pidgit
		bne	:+
		lda	timer_actor_throw, x
		bne	:+ ; if (this.obj_id == pidgit && !timer_actor_throw[x]) {
			jsr	sub_C231
		: ; }

		lda	#actor_states::dead
		sta	proc_id_actor, x
		pla
		pla
	: ; }

	rts
