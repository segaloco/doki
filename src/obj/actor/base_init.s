.include	"system/ppu.i"

.include	"tunables.i"
.include	"modes.i"
.include	"actor.i"

.include	"mem.i"



; -----------------------------------------------------------
	.export actor_attr_tbl, eng_actor_flag_tbl
	.export actor_attr_set, collision_actor_types
actor_attr_tbl:
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit1|actor_attrs::bit0)
	.byte	(actor_attrs::bit1)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit1)
	.byte	(actor_attrs::bit1|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit2|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit2|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit2|actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit1)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit2|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	0
	.byte	0
	.byte	(actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit1)
	.byte	(actor_attrs::bit3|actor_attrs::bit1)
	.byte	(actor_attrs::bit6|actor_attrs::bit1)
	.byte	(actor_attrs::bit6|actor_attrs::bit1|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit1)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit1|actor_attrs::bit0)
	.byte	(actor_attrs::bit1)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit3|actor_attrs::bit2|actor_attrs::bit1|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip|actor_attrs::bit1)
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit3|actor_attrs::bit2|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::bit3|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip)
	.byte	(actor_attrs::bit6|actor_attrs::bit2|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::bit6|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit0)
	.byte	(actor_attrs::h_flip|actor_attrs::bit1)
	.byte	(actor_attrs::v_flip|actor_attrs::h_flip|actor_attrs::bit1)

eng_actor_flag_tbl:
	.byte	(actor_flags::no_actor_col)
	.byte	0
	.byte	0
	.byte	0
	.byte	(actor_flags::cant_lift|actor_flags::harm_top)
	.byte	0
	.byte	0
	.byte	0
	.byte	(actor_flags::fast|actor_flags::tilemap2)
	.byte	(actor_flags::fast)
	.byte	(actor_flags::fast|actor_flags::cant_lift)
	.byte	(actor_flags::fast|actor_flags::cant_lift)
	.byte	(actor_flags::fast|actor_flags::cant_lift)
	.byte	(actor_flags::fast)
	.byte	(actor_flags::fast)
	.byte	(actor_flags::fast)
	.byte	(actor_flags::fast)
	.byte	(actor_flags::tilemap2|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::mirror_ani|actor_flags::fast|actor_flags::tilemap2)
	.byte	(actor_flags::mirror_ani)
	.byte	0
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::tilemap2)
	.byte	(actor_flags::fast|actor_flags::tilemap2)
	.byte	(actor_flags::mirror_ani|actor_flags::tilemap2)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::death_noise|actor_flags::cant_lift)
	.byte	(actor_flags::wide|actor_flags::tilemap2|actor_flags::death_noise|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::fast)
	.byte	(actor_flags::tilemap2|actor_flags::death_noise)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::fast|actor_flags::tilemap2)
	.byte	(actor_flags::fast|actor_flags::tilemap2|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::mirror_ani|actor_flags::tilemap2|actor_flags::no_actor_col|actor_flags::cant_lift)
	.byte	(actor_flags::fast|actor_flags::tilemap2)
	.byte	(actor_flags::mirror_ani|actor_flags::tilemap2|actor_flags::death_noise|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::mirror_ani|actor_flags::fast|actor_flags::tilemap2|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::tilemap2|actor_flags::death_noise|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::death_noise|actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::cant_lift|actor_flags::harm_top)
	.byte	(actor_flags::cant_lift|actor_flags::harm_top)
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	(actor_flags::no_actor_col)
	.byte	(actor_flags::no_actor_col)
	.byte	(actor_flags::no_actor_col)
	.byte	0
	.byte	0
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift)
	.byte	(actor_flags::no_actor_col)
	.byte	0
	.byte	(actor_flags::no_actor_col)
	.byte	(actor_flags::no_actor_col)
	.byte	(actor_flags::no_actor_col)
	.byte	(actor_flags::tilemap2|actor_flags::no_actor_col|actor_flags::cant_lift)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift)
	.byte	(actor_flags::no_actor_col|actor_flags::cant_lift)
	.byte	(actor_flags::death_noise|actor_flags::no_actor_col)
	.byte	(actor_flags::no_actor_col)
	.byte	(actor_flags::no_actor_col)

eng_actor_tbl_976A:
	.byte	0, 5, 5, 5
	.byte	5, 5, 5, 5
	.byte	$C, 5, 5, 5
	.byte	5, 5, 5, 5
	.byte	5, 5, 5, 5
	.byte	5, $D, $D, 5
	.byte	$C, $C, 5, $D
	.byte	$C, $C, 5, $E
	.byte	$D, 5, 5, 5
	.byte	$C, 5, 5, 5
	.byte	5, 5, 5, 0
	.byte	$F, $F, $F, $F
	.byte	5, 5, 5, 5
	.byte	5, 5, 5, 4
	.byte	4, 4, 4, 4
	.byte	4, 4, 4, $10
	.byte	0, 0, 5, 5
	.byte	5

col_box_actor_tbl:
	.byte	8, 2, 2, 2
	.byte	2, 2, 2, 2
	.byte	4, 2, 9, 9
	.byte	9, 2, 2, 2
	.byte	2, 2, 2, 2
	.byte	2, 8, 8, 2
	.byte	4, 4, $E, 8
	.byte	4, 4, 2, $F
	.byte	2, 2, 2, 2
	.byte	$10, 2, $12, 2
	.byte	$F, 2, $11, $B
	.byte	2, 2, 2, 2
	.byte	2, 2, 2, 2
	.byte	2, 2, 4, 3
	.byte	3, 7, 4, 3
	.byte	3, 3, 3, 9
	.byte	$B, $B, 2, 2
	.byte	2

collision_actor_types:
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_1
	.byte	actor_col_type::type_1
	.byte	actor_col_type::type_1
	.byte	actor_col_type::type_1
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_1
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_3
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_4
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_2
	.byte	actor_col_type::type_0
	.byte	actor_col_type::type_2
; ----------------------------
	.export actor_attr_set
actor_attr_set:
	ldy	obj_id_actor, x
	lda	actor_attr_tbl, y
	and	#<~(actor_attrs::v_flip)
	sta	attr_actor, x
	lda	eng_actor_flag_tbl, y
	sta	actor_mode, x
	lda	col_box_actor_tbl, y
	sta	col_box_actor, x
	lda	eng_actor_tbl_976A, y
	sta	eng_actor_tbl_492, x
	rts
; -----------------------------------------------------------
	.export actor_init_base
	.export actor_init_base_timerless, actor_init_base_noreset
	.export	actor_init_base_track_attr
	.export actor_init_base_tracking, actor_init_base_movement
actor_init_base:
	lda	#0
	sta	timer_actor, x

actor_init_base_timerless:
	lda	#0
	sta	zp_actor_work_byte_1, x

actor_init_base_noreset:
	lda	#0
	sta	zp_actor_work_byte_2, x
	sta	timer_actor_throw, x
	sta	timer_actor_held, x
	sta	timer_ani_actor, x
	sta	timer_actor_shake, x
	sta	collision_actor, x
	sta	timer_actor_stun, x
	sta	eng_timer_actor_2, x
	sta	accel_x_actor, x
	sta	accel_y_actor, x
	sta	timer_actor_flash, x
	sta	eng_actor_tbl_477, x
	sta	eng_actor_tbl_480, x
	sta	hp_actor, x
	sta	veloc_y_actor, x

actor_init_base_track_attr:
	jsr	actor_attr_set

actor_init_base_tracking:
	jsr	motion_side_get

actor_init_base_movement:
	iny
	sty	disp_dir_actor, x
	lda	eng_boom_pos_v+3-1, y
	sta	veloc_x_actor, x
	lda	actor_mode, x
	and	#(actor_flags::fast)
	beq	:+ ; if (actor.mode & fast) {
		asl	veloc_x_actor, x
	: ; }

	rts
