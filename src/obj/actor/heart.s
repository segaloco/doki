.include	"system/ppu.i"

.include	"actor.i"

.include	"mem.i"



HEART_VELOC_X		= 4
HEART_VELOC_Y		= <-8
HEART_DRIFT_FRAMES	= 32
HEART_CHR_NO		= $D8

; -----------------------------------------------------------
	.export actor_proc_heart, render_heart
actor_proc_heart:
	lda	zp_byte_EF
	beq	:+ ; if (zp_byte_EF) {
		jmp	actor_destroy
	: ; }

	ldy	#<-(HEART_VELOC_X)
	lda	frame_count
	and	#HEART_DRIFT_FRAMES
	beq	:+ ; if ((frame_count % drift_frames) > (drift_frames / 2)) {
		ldy	#HEART_VELOC_X
	: ; }
	sty	veloc_x_actor, x

	lda	#HEART_VELOC_Y
	sta	veloc_y_actor, x
	jsr	eng_actor_dophys

render_heart:
	lda	zp_byte_EE
	and	#$08
	ora	zp_byte_EF
	bne	:+ ; if (!((zp_byte_EE & 0x08) | zp_byte_EF)) {
		ldy	zp_byte_F4
		lda	pos_y_rel_actor
		sta	oam_buffer+OBJ_POS_V, y
		lda	pos_x_rel_actor
		sta	oam_buffer+OBJ_POS_H, y
		lda	#HEART_CHR_NO
		sta	oam_buffer+OBJ_CHR_NO, y
		lda	frame_count
		and	#HEART_DRIFT_FRAMES
		eor	#HEART_DRIFT_FRAMES
		asl	a
		ora	#(obj_attr::color1)
		sta	oam_buffer+OBJ_ATTR, y
	: ; }

	rts
