.include	"tunables.i"
.include	"actor.i"
.include	"modes.i"

.include	"mem.i"



; -----------------------------------------------------------
ALBATROSS_VELOC_X	= 16
ALBATROSS_BOMB_VELOC_Y	= 16
ALBATROSS_BOMB_MARGIN_X	= 48

actor_albatross_offset_x_lo:
	.byte   <-16

actor_albatross_offset_x_hi:
	.byte   <-1, 0
; ----------------------------
	.export	actor_init_albatross_left
	.export actor_init_albatross_right
	.export actor_proc_albatross
actor_init_albatross_left:
	jsr	actor_init_base
	lda	#<-(ALBATROSS_VELOC_X)
	bne	:+

actor_init_albatross_right:
	jsr	actor_init_base
	lda	#ALBATROSS_VELOC_X

:
	sta	veloc_x_actor, x
	inc	ENG_ACTOR_ALBATROSS_BOMB_THROWN, x
	lda	obj_id_actor, x
	sec
	sbc	#(actor::albatross_left-1)
	tay
	lda	pos_x_lo_screen_left
	adc	actor_albatross_offset_x_lo, y
	sta	pos_x_lo_actor, x
	lda	pos_x_hi_screen_left
	adc	actor_albatross_offset_x_hi, y
	sta	pos_x_hi_actor, x
	rts
; -----------------------------------------------------------
actor_proc_albatross:
	jsr	render_albatross
	inc	timer_ani_actor, x

	lda	ENG_ACTOR_ALBATROSS_BOMB_THROWN, x
	bne	:+++ ; if (!bomb_thrown) {
		lda	collision_actor, x
		and	#(actor_collisions::damage)
		bne	:+
		jsr	motion_side_get
		lda	eng_actor_physics_side_lo
		adc	#ALBATROSS_BOMB_MARGIN_X
		cmp	#ALBATROSS_BOMB_MARGIN_X*2
		bcs	:++
		: jsr	actor_init_shyguy_prj_lo
		bmi	:+ ; if (((collision_actor & damage) || (side_lo < ALBATROSS_BOMB_MARGIN_X)) && actor_init_shyguy_prj_lo >= 0) {
			ldx	zp_addr_00
			lda	#actor::bobomb
			sta	obj_id_actor, x
			lda	pos_y_lo_actor, x
			adc	#ALBATROSS_BOMB_VELOC_Y
			sta	pos_y_lo_actor, x
			jsr	actor_init_bobomb
			ldx	actor_index
			inc	ENG_ACTOR_ALBATROSS_BOMB_THROWN, x
		: ; }

		jmp	:++
	: ; } else {
		jsr	eng_enemy_damage_check
	: ; }

	jmp	motion_x
