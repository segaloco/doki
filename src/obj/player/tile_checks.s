.include	"tunables.i"
.include	"modes.i"
.include	"actor.i"
.include	"misc.i"

.include	"mem.i"

.include	"unk.i"

	.export col_player_tiles, col_player_type
	.export eng_collision_tile_type
	.export eng_lift_item_create_sprite_ani
	.export eng_tile_snap_to, eng_tile_snap_to_page
; -----------------------------------------------------------
col_player_tiles_page:
	.byte   $02
	.byte   $02
	.byte	$01
	.byte	$01
	.byte   $02
	.byte   $02
	.byte   $02
	.byte   $02

col_player_tiles_bits:
	.byte	actor_collisions::ceil
	.byte	actor_collisions::ceil
	.byte	actor_collisions::floor
	.byte	actor_collisions::floor
	.byte	actor_collisions::left
	.byte	actor_collisions::left
	.byte	actor_collisions::right
	.byte	actor_collisions::right

eng_player_tilecol_accel_y:
	.byte	<(-(ENG_TILECOL_ACCEL_Y)), ENG_TILECOL_ACCEL_Y
; ----------------------------
col_player_tiles:
	lda	#0
	sta	collision_player
	sta	eng_surface_traction
	sta	zp_byte_07
	sta	zp_addr_0A
	sta	zp_byte_0E
	sta	zp_addr_0C
	jsr	col_player_cherry_climb
	lda	player_crouch
	asl	a
	ora	player_item_held
	tax
	lda	eng_hitboxes, x
	sta	zp_byte_08
	lda	veloc_y_player
	clc
	adc	accel_y_player
	bpl	:+ ; if ((veloc_y_player + accel_y_player) < 0) {
		jsr	col_player_tile_check
		jsr	col_player_tile_increment
		lda	collision_player
		bne	:+++++++
		beq	col_player_tiles_end
	: ; } else {
		lda	#0
		sta	eng_player_quicksand
		jsr	col_player_tile_increment
		jsr	col_player_tile_check
		lda	collision_player
		bne	:+++ ; if (!collision_player) {
			ldx	#1
			lda	course_no
			cmp	#1
			beq	:+
			cmp	#5
			bne	:++
			: ; if (course_no.in(1, 5)) {
				jsr	sub_C525
			: ; }

			stx	player_airborne
			jmp	:+++++
		: ; }

		lda	pos_y_lo_player
		and	#$0C
		bne	:++++ ; if (!(pos_y_lo_player & 0x0C)) {
			sta	player_airborne
			lda	pos_y_lo_player
			and	#$F0
			sta	pos_y_lo_player
			lsr	zp_addr_0A
			bcc	:+ ; if (!(zp_addr_0A & 1)) {
				ldx	zp_addr_0A
				lda	eng_player_tilecol_accel_y, x
				sta	accel_x_player
			: ; }

			lsr	zp_addr_0C
			bcc	:+ ; if (!(zp_addr_0C & 1)) {
				lda	#$0F
				sta	eng_surface_traction
			: ; }

			jsr	eng_tile_jar_check
		; }
	: ; } if () {
		lda	#0
		sta	veloc_y_player
		sta	accel_y_player
		lda	timer_player_star
		bne	:+ ; if (timer_player_star == 0) {
			lsr	zp_byte_0E
			bcc	:+ ; if (zp_byte_0E % 1) {
				lda	eng_player_screen_x
				sta	pos_x_rel_actor
				ror	actor_index
				jsr	eng_col_player_hurt
			; }
		: ; }
	; }

col_player_tiles_end:
	ldy	#2
	lda	veloc_x_player
	clc
	adc	accel_x_player
	bmi	:+ ; if ((player.veloc_x + player.accel_x) >= 0) {
		dey
		jsr	col_player_tile_increment
	: ; }

	sty	disp_dir_player
	jsr	col_player_tile_check

	lda	collision_player
	and	#(actor_collisions::left|actor_collisions::right)
	beq	:+ ; if (collision_player & (left|right))) {
		jmp	eng_player_bounds_physics
	: ; }

	rts
; ----------------------------
col_player_tile_check:
	jsr	col_player_tile_check_do

col_player_tile_check_do:
	ldx	#0
	ldy	zp_byte_08
	jsr	eng_collision_tile_type
	ldx	zp_byte_07
	ldy	col_player_tiles_page, x
	lda	eng_tile_type
	jsr	col_player_type
	bcc	:++++ ; if (col_player_type(eng_tile_type)) {
		cmp	#$13 ; switch (eng_player_tile_type) {
		bne	:+ ; case 0x13:
			lda	col_player_tiles_page, x
			sta	zp_byte_0E
			bne	:+++
		: cmp	#$10
		bne	:+ ; case 0x10:
			lda	col_player_tiles_page, x
			sta	zp_addr_0C
			bne	:++
		: sec
		sbc	#($66)-1
		cmp	#2 ; case <0x68:
		bcs	:+
			asl	a
			ora	col_player_tiles_page, x
			sta	zp_addr_0A
		: ; }

		lda	col_player_tiles_bits, x
		ora	collision_player
		sta	collision_player
	: ; }

	jmp	col_player_tile_increment_do

col_player_tile_increment:
	jsr	col_player_tile_increment_do

col_player_tile_increment_do:
	inc	zp_byte_07
	inc	zp_byte_08
	rts
; -----------------------------------------------------------
col_player_cherry_climb:
	ldx	#0
	ldy	eng_other_hitboxes
	jsr	col_player_climbtile
	lda	eng_tile_type
	sta	byte_4EB
	bcs	:++
	cmp	#$4E
	bne	:++++ ; if (byte_4EB == 0x4E) {
		inc	byte_62A
		lda	byte_62A
		sbc	#ENG_STARMAN_CHERRIES
		bne	:+ ; if (++byte_62A >= ENG_STARMAN_CHERRIES) {
			sta	byte_62A
			jsr	actor_init_starman
		: ; }

		lda	#4
		sta	byte_602
		lda	#$40
		jmp	sub_8FF3
	: ; } else {
		lda	joypad_p1h
		and	#(joypad_button::up|joypad_button::down)
		beq	:+
		ldy	player_item_held
		bne	:+
		lda	pos_x_lo_player
		clc
		adc	#ENG_MARGIN_CLIMB_X
		and	#ENG_POS_MASK
		cmp	#ENG_PADDING_CLIMB_X
		bcs	:+ ; if ((joypad_p1h_up|joypad_p1h_down) && !player_item_held && ((pos_x_lo_player+margin)&mask) < padding) {
			lda	#player_state::climb
			sta	proc_id_player
			sty	player_airborne
			sty	player_crouch
			lda	#$0A
			sta	eng_player_ani_frame
			pla
			pla
			pla
			pla
		: ; }
	: ; }

	rts
; -----------------------------------------------------------
col_player_type:
	pha
	and	#$C0
	asl	a
	rol	a
	rol	a
	adc	col_player_type_pages, y
	tay
	pla
	cmp	tbl_C063, y
	rts

col_player_type_pages:
	.byte	$00, $04, $08
; -----------------------------------------------------------
eng_lift_item_idx:
	.byte   $37
	.byte   $37
	.byte   $37
	.byte	$38
	.byte   $34
	.byte	$31
	.byte	$30
	.byte	$36
	.byte	$33
	.byte	$35
	.byte	$3C
	.byte	$3E
	.byte	$38
	.byte	$09
	.byte   $37
eng_lift_item_idx_end:
; ----------------------------
eng_lift_item_allocate:
	ldx	#6
	: ; for (x = 6; x < 9; x++) {
		lda	proc_id_actor, x
		beq	eng_lift_item_create_sprite ; if (actor[x].proc_id != null) {
			inx
			cpx	#9
			bcc	:-
		; } else eng_lift_item_create_sprite();
	; }

	rts
; ----------------------------
eng_lift_item_create_sprite:
	lda	zp_addr_00
	sta	zp_actor_work_byte_1, x
	lda	zp_byte_03
	sta	pos_x_hi_actor, x
	lda	zp_byte_04
	sta	pos_y_hi_actor, x
	lda	zp_byte_05
	sta	pos_x_lo_actor, x
	lda	zp_byte_06
	sta	pos_y_lo_actor, x
	lda	#0
	sta	timer_actor_throw, x
	sta	timer_ani_actor, x
	sta	zp_actor_work_byte_2, x
	jsr	actor_erase

	lda	#actor_states::normal
	ldy	zp_byte_09
	cpy	#(eng_lift_item_idx_end-eng_lift_item_idx)-1
	bne	:+ ; if (zp_byte_09 == 0x0E) {
		lda	#32
		sta	timer_actor, x
		lda	#actor_states::sand
	: ; }
	sta	proc_id_actor, x

	lda	eng_lift_item_idx, y
	sta	obj_id_actor, x
	ldy	#$FF
	cmp	#actor::bomb
	beq	:+
	cmp	#actor::bobomb
	bne	:++ ; if (eng_player_lift_item_idx == bobomb) {
		ldy	#80
	: ; } if (eng_player_lift_item_idx.in(bobomb, bomb)) {
		sty	timer_actor, x
		bne	:++++
	; }
	: cmp	#actor::one_up
	bne	:+
	lda	byte_620
	beq	:+++ ; } else if ((eng_player_lift_item_idx == one_up) && byte_620) {
		lda	#actor::vegetable_small
		sta	obj_id_actor, x
		jmp	:+++
	: cmp	#actor::vegetable_large
	bne	:++ ; } else if (eng_player_lift_item_idx == vegetable_large) {
		ldy	byte_62C
		iny
		cpy	#5
		bcc	:+ ; if ((++byte_62C) >= 5) {
			lda	#actor::stopwatch
			sta	obj_id_actor, x
			ldy	#0
		: ; }

		sty	byte_62C
	: ; }

	jsr	actor_init_base_noreset
	lda	#(actor_collisions::floor)
	sta	collision_actor, x
	lda	#$40
	jsr	eng_item_lift_replace
	lda	#7
	sta	timer_actor_held, x
	stx	player_item_held_id

eng_lift_item_create_sprite_ani:
	lda	#player_state::lift
	sta	proc_id_player
	lda	#6
	sta	eng_timer_player_state
	lda	#8
	sta	eng_player_ani_frame
	inc	player_item_held
	rts
; -----------------------------------------------------------
eng_tile_jar_check:
	ldy	player_item_held
	bne	eng_tile_item_check_nobutton
	lda	player_crouch
	beq	eng_tile_item_check
	lda	zp_addr_00
	ldx	eng_inner_space
	cpx	#2
	bne	:+
	cmp	#$72
	beq	:++
	bne	eng_tile_item_check_nobutton
	: ; if (!player_item_held && player_crouch && eng_inner_space != 2) {
		iny
		cmp	#$71
		beq	:+
		cmp	#$6D
		bne	eng_tile_item_check_nobutton ; if (zp_addr_00 == 0x6D) {
			iny
		; }
	: ; }

	lda	pos_x_lo_player
	clc
	adc	#ENG_MARGIN_JARENTER_X
	and	#ENG_POS_MASK
	cmp	#ENG_PADDING_JARENTER_X
	bcs	eng_tile_item_check_nobutton ; if (((player.pos_x + margin)&mask) >= padding) {
		lda	#0
		sta	veloc_x_player
		lda	#player_state::jarenter
		sta	proc_id_player
		sty	eng_jar_type
	; }

eng_tile_snap_to:
	lda	pos_x_lo_player
	clc
	adc	#ENG_SNAPTO_DISP_X
	and	#ENG_SNAPTO_MASK_X
	sta	pos_x_lo_player
	bcc	:+
	lda	eng_level_h
	beq	:+ ; if () {
	eng_tile_snap_to_page:
		inc	pos_x_hi_player
	: ; }

	rts
; -----------------------------------------------------------
eng_tile_item_check:
	bit	joypad_p1
	bvc	:+++
	lda	pos_x_lo_player
	clc
	adc	#ENG_MARGIN_ITEM_X
	and	#ENG_POS_MASK
	cmp	#ENG_PADDING_ITEM_X
	bcs	:+++ ; if (joypad_p1_face_b && (((pos_x_lo_player + margin) & mask)) < padding) {
		lda	zp_addr_00
		cmp	#$93
		bne	:+ ; if (zp_addr_00 == 0x93) {
			lda	#$0E
			bne	:++
		: ; } else {
			cmp	byte_C070
			bcs	:++
			sec
			sbc	byte_C06F
			bcc	:++
		: ; } if (zp_addr_00 == 0x93 || ((zp_addr_00 < byte_C070) && (a >= (byte_C06F+1)))) {
			sta	zp_byte_09
			jmp	eng_lift_item_allocate
		; }
	: ; }

eng_tile_item_check_nobutton:
	lda	player_crouch
	bne	eng_checks_rts
	lda	zp_byte_06
	sec
	sbc	#($11)-1
	sta	zp_byte_06
	sta	eng_map_bg_tile_off_v
	lda	zp_byte_04
	sbc	#0
	sta	zp_byte_04
	sta	zp_addr_01
	lda	zp_byte_03
	sta	zp_byte_02
	jsr	sub_8F38
	bcs	eng_checks_rts
	jsr	eng_map_bg_tile_off_calc
	ldy	eng_map_bg_tile_off
	lda	(zp_addr_01), y

	ldx	player_item_held
	beq	:+ ; if (player_item_held) {
		ldx	player_item_held_id
		ldy	obj_id_actor, x
		cpy	#actor::key
		bne	eng_checks_rts
	: ; }

	ldx	eng_inner_space
	cpx	#2
	beq	:++ ; if (eng_inner_space != 2) {
		ldy	#(eng_item_check_list_end-eng_item_check_list)-1
		: ; for (y = 4; y >= 0; y--) {
			cmp	eng_item_check_list, y
			beq	eng_tile_door_check

			dey
			bpl	:-
		; }
	: ; }

	bit	joypad_p1
	bvc	eng_checks_rts
	sta	zp_addr_00
	cmp	#$4D
	bcs	eng_checks_rts
	sec
	sbc	#($44)-1
	bcs	:+ ; if (joypad_p1_face_b && (zp_addr_00 < 0x4D) && (zp_addr_00 > 44)) {
	eng_checks_rts:
		rts
	: ; }

	ldx	eng_inner_space
	cpx	#2
	bne	:++ ; if (eng_inner_space == 2) {
		lda	word_621
		cmp	#2
		bcs	:+ ; if (word_621 < 2) {
			inc	word_621+1
			ldx	#0
		: ; }

		txa
	: ; }

	clc
	adc	#4
	sta	zp_byte_09
	jmp	eng_lift_item_allocate
; ----------------------------
	.export eng_tile_door_check_finish
eng_tile_door_check:
	lda	joypad_p1
	and	#joypad_button::up
	beq	eng_checks_rts
	lda	pos_x_lo_player
	clc
	adc	#ENG_MARGIN_DOOR_X
	and	#ENG_POS_MASK
	cmp	#ENG_PADDING_DOOR_X
	bcs	eng_checks_rts ; if (joypad_p1_up && ((pos_x_lo_player + margin) & mask) > padding) {
		cpy	#system_modes::warp
		bne	:+ ; if (y == warp) {
			dey
			sty	proc_id_system
			rts
		: ; }

		lda	#transition_state::door
		sta	eng_transition_type
		tya
		jsr	tbljmp
		.addr	eng_tile_door_check_start
		.addr	eng_tile_door_check_key
		.addr	eng_tile_door_check_transition
		.addr	eng_tile_door_check_transition
	; }

eng_tile_door_check_start:
	jsr	actor_door_subsp_spawn_0

eng_tile_door_check_finish:
	inc	eng_door_timer_ani
	inc	player_lock
	jsr	eng_tile_snap_to
	lda	#1
	sta	apu_dpcm_queue

:
	rts

eng_tile_door_check_key:
	lda	player_item_held
	beq	:-
	ldy	player_item_held_id
	lda	obj_id_actor, y
	cmp	#actor::key
	bne	:- ; if (player_item_held && eng_held_actor_type == key) { 
		inc	eng_key_used
		tya
		tax
		jsr	actor_proc_lamp_end
		jsr	actor_door_subsp_spawn_1
		jmp	eng_tile_door_check_finish
	; }

eng_tile_door_check_transition:
	inc	eng_transition_area
	jmp	eng_map_reset
; ----------------------------
	.export eng_item_check_list, eng_item_check_list_end
eng_item_check_list:
	.byte	$51
	.byte	$50
	.byte   $83
	.byte   $52
	.byte   $56
eng_item_check_list_end:
; -----------------------------------------------------------
eng_collision_tile_type:
	txa
	pha
	lda	#0
	sta	zp_byte_00
	sta	zp_byte_01

	lda	eng_tile_cols_x, y
	bpl	:+ ; if (eng_tile_cols_x[y] < 0) {
		dec	zp_byte_00
	: ; }

	clc
	adc	pos_x_lo_obj, x
	and	#$F0
	sta	zp_byte_05
	php
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	eng_map_bg_tile_off_h
	plp
	lda	pos_x_hi_obj, x
	adc	zp_byte_00
	sta	zp_addr_01+1
	sta	zp_byte_03

	lda	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		sta	zp_addr_01+1
		sta	zp_byte_03
	: ; }

	lda	eng_tile_cols_y, y
	bpl	:+ ; if (eng_tile_cols_y[y] < 0) {
		dec	zp_addr_01
	: ; }

	clc
	adc	pos_y_lo_obj, x
	and	#$F0
	sta	zp_byte_06
	sta	eng_map_bg_tile_off_v
	lda	pos_y_hi_obj, x
	adc	zp_addr_01
	sta	zp_addr_01
	sta	zp_byte_04

	jsr	sub_8F38
	bcc	:+ ; if (sub_8F38()) {
		lda	#0
		beq	:++
	: ; } else {
		jsr	eng_map_bg_tile_off_calc
		ldy	eng_map_bg_tile_off
		lda	(zp_addr_01), y
	: ; }
	sta	eng_tile_type

	pla
	tax
	rts
