.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"mem.i"
.include	"modes.i"
.include	"actor.i"
.include	"misc.i"
.include	"tunables.i"
.include	"macros.i"

	.export player_proc_lift, player_proc_climb
	.export player_proc_jarenter, player_proc_jarexit
	.export player_proc_climbtrans, player_proc_maskexit
	.export eng_player_crouch_start, eng_player_gravity
	.export eng_obj_dophysics_y, eng_obj_dophysics
; -----------------------------------------------------------
player_proc_lift:
	lda	eng_timer_player_state
	bne	:++++++ ; if (!eng_timer_player_state) {
		ldx	player_item_held_id
		ldy	timer_actor_held, x
		cpy	#2
		bcc	:+++++ ; if (eng_player_timer_item_held >= 2) {
			cpy	#7
			bne	:+ ; if (eng_player_timer_item_held == 7) {
				lda	#8
				sta	apu_dpcm_queue
			: ; }

			dec	timer_actor_held, x
			lda	player_proc_lift_frames, y
			sta	eng_player_ani_frame

			lda	proc_id_actor, x
			cmp	#actor_states::sand
			beq	:+
			lda	obj_id_actor, x
			cmp	#actor::vegetable_small
			bne	:++
			: ; if (actor_held.proc_id == sand || actor_held.id == vegetable_small) {
				lda	eng_timer_item_lift-2, y
				bpl	:++
			: ; } if ((actor_held.proc_id != sand && actor_held.id != vegetable_small) || timer_item_lift[y-2] < 0) {
				lda	byte_544, y
			: ; }

			sta	eng_timer_player_state
			rts
		: ; } else {
			sta	proc_id_player
			inc	player_airborne
			inc	player_crouch
		; }
	: ; }

	rts
; ----------------------------
eng_timer_item_lift:
	.byte	0, 1, 1, 1

player_proc_lift_frames:
	.byte	1, 2, 4, 4, 4, 4, 8
; -----------------------------------------------------------
	.export pos_y_lo_throw, pos_y_hi_throw
pos_y_lo_throw:	.byte	$08, $FB, $FF, $00, $08, $0C, $18
pos_y_hi_throw:	.byte   $1A, $FF, $FF, $00, $00, $00, $00

	.byte	0
; -----------------------------------------------------------
eng_climb_dirveloc_x:
	.byte	0, ENG_PHYSICS_CLIMB_VELOC_X, <(-ENG_PHYSICS_CLIMB_VELOC_X)

	.export	eng_player_climb_veloc_y_down, eng_player_climb_veloc_y_up
eng_player_climb_veloc_y:
	.byte	0
eng_player_climb_veloc_y_down:
	.byte	ENG_PHYSICS_CLIMB_VELOC_Y_DOWN
eng_player_climb_veloc_y_up:
	.byte	ENG_PHYSICS_CLIMB_VELOC_Y_UP
; ----------------------------
player_proc_climb:
	lda	joypad_p1h
	and	#(joypad_button::up|joypad_button::down)
	lsr	a
	lsr	a
	tay
	cpy	#(joypad_button::up>>2)
	bne	:+ ; if (joypad_p1h_up) {
		jsr	eng_player_animate_climb
	: ; }

	lda	eng_player_climb_veloc_y, y
	sta	veloc_y_player
	lda	joypad_p1h
	and	#(joypad_button::left|joypad_button::right)
	tay
	lda	eng_climb_dirveloc_x, y
	sta	veloc_x_player

	lda	pos_x_lo_player
	clc
	adc	#4
	and	#ENG_POS_MASK
	cmp	#8
	bcs	:++++ ; if (((pos_x_lo_player+4)&0xF) < 8) {
		ldy	eng_other_hitboxes+1
		lda	veloc_y_player
		bmi	:+ ; if (eng_layer_veloc_y >= 0) {
			iny
		: ; }

		ldx	#0
		jsr	col_player_climbtile
		bcs	:+
		lda	veloc_y_player
		bpl	:++ ; if (veloc_y_player < 0) {
			; if () {
				stx	veloc_y_player
			: ; }

			jsr	eng_player_dophysics_x
			jmp	eng_obj_dophysics_y
		: ; }
	: ; }

	lda	#player_state::normal
	sta	proc_id_player
	rts
; ----------------------------
eng_player_animate_climb:
	lda	frame_count
	and	#$07
	bne	:+ ; if (!(frame_count % 8)) {
		lda	player_dir
		eor	#ENG_DIRECTION_RIGHT
		sta	player_dir
		lda	#2
		sta	apu_sfx_queue2
	: ; }

	rts
; -----------------------------------------------------------
eng_player_tiles_climbable:
	.byte   $C2
	.byte   $D1
	.byte   $C3
	.byte	$C4
	.byte	$07
	.byte   $80
	.byte	$81
	.byte	$94
	.byte	$95
	.byte	$11
eng_player_tiles_climbable_end:
; ----------------------------
	.export col_player_climbtile
col_player_climbtile:
	jsr	eng_collision_tile_type
	lda	eng_tile_type
	ldy	#(eng_player_tiles_climbable_end-eng_player_tiles_climbable)-1
	: ; for (y = 9; eng_player_tiles_climbable[y] && y > 0; y--) {
		cmp	eng_player_tiles_climbable, y
		beq	:+
		dey
		bpl	:- ; if (y < 0) {
			clc
		; }
	: ; }

	rts
; -----------------------------------------------------------
player_proc_jarenter:
	lda	#obj_attr::priority_low|obj_attr::color0
	sta	attr_player

	inc	pos_y_lo_player
	lda	pos_y_lo_player
	and	#ENG_POS_MASK
	bne	:+++ ; if (!(++pos_y_lo_player & ENG_POS_MASK)) {
		sta	proc_id_player
		jsr	eng_map_reset
		pla
		pla
		jsr	eng_player_backup
		lda	#transition_state::jar
		sta	eng_transition_type

		lda	eng_jar_type ; switch (eng_jar_type) {
		bne	:+ ; case warp:
			lda	#system_modes::warp
			sta	proc_id_system
			rts

		: cmp	#1
		beq	:+ ; default:
			sta	eng_transition_area
			rts

		: ; case inner_space:
			sta	eng_inner_space
		: ; }
	; }

	rts
; -----------------------------------------------------------
player_proc_jarexit:
	lda	#obj_attr::priority_low|obj_attr::color0
	sta	attr_player

	dec	pos_y_lo_player
	lda	pos_y_lo_player
	and	#ENG_POS_MASK
	bne	:+ ; if (!(--en_player_y_lo & 0xF)) {
		sta	proc_id_player
	: ; }

	rts
; -----------------------------------------------------------
climbedge_screen_y_hi:
	.byte	$00, $FF
climbedge_screen_y_lo:
	.byte   $EF, $DF
clib_ani_y_lo:
	.byte	$08, $A0
; ----------------------------
player_proc_climbtrans:
	lda	veloc_y_player
	asl	a
	rol	a
	and	#$01
	tay
	lda	eng_player_screen_y_hi
	cmp	climbedge_screen_y_hi, y
	bne	:+
	lda	eng_player_screen_y_lo
	cmp	climbedge_screen_y_lo, y
	bne	:+ ; if (eng_player_screen_y == climbedge_screen_y) {
		jsr	eng_map_reset
		inc	eng_transition_area
		lda	#transition_state::climb
		sta	eng_transition_type
		rts
	: ; }

	lda	eng_player_screen_y_hi
	bne	:+
	lda	eng_player_screen_y_lo
	cmp	clib_ani_y_lo, y
	beq	:+++
	: ; if (eng_player_screen_y_hi || eng_player_screen_y_lo != clib_ani_y_lo[y]) {
		tya
		beq	:+ ; if (y) {
			jsr	eng_player_animate_climb
		: ; }

		jmp	eng_obj_dophysics_y
	: ; }

	lda	#player_state::climb
	sta	proc_id_player
	rts
; -----------------------------------------------------------
player_proc_maskexit:
	lda	eng_timer_player_state
	beq	:+
	jsr	eng_obj_dophysics_y
	lda	a:collision_player
	beq	:++ ; if (eng_timer_player_state && collision_player) {
		lda	#obj_attr::priority_low|obj_attr::color0
		sta	attr_player
		lda	#ENG_MASKEXIT_VELOC_X
		sta	veloc_x_player
		lda	#ENG_DIRECTION_RIGHT
		sta	player_dir
		jsr	eng_player_dophysics_x
		jmp	eng_player_animate_movement
	: ; } else if (!eng_timer_player_state) {
		sta	proc_id_player
	: ; }

	rts
; -----------------------------------------------------------
player_crouch_frictions:
	.byte	<(-ENG_CROUCH_FRICTION), ENG_CROUCH_FRICTION
; ----------------------------
eng_player_crouch_start:
	jsr	eng_player_animate_movement
	lda	player_airborne
	bne	:++++ ; if (!player_airborne) {
		lda	player_crouch
		beq	:+ ; if (player_crouch) {
			lda	eng_timer_player_state
			bne	player_crouch_friction_finish
			dec	player_crouch
		: ; }

		lda	joypad_p1
		bpl	:+ ; if (joypad_p1_face_a) {
			inc	player_airborne
			lda	#6
			sta	eng_player_ani_frame
			jsr	eng_player_jump_start
			lda	#1
			sta	apu_sfx_queue2
		: ; }

		lda	eng_player_on_carpet
		bne	player_crouch_friction_finish
		lda	joypad_p1h
		and	#(joypad_button::down)
		beq	:+ ; if (joypad_p1h_down) {
			inc	player_crouch
			lda	#4
			sta	eng_player_ani_frame

			lda	player_airborne
			bne	:+ ; if (!player_airborne) {
				lda	eng_timer_crouch_jump
				cmp	#ENG_CROUCH_JUMP_TIME
				bcs	player_crouch_friction_finish
				inc	eng_timer_crouch_jump
				bne	player_crouch_friction_finish
			; }
		: ; }
	: ; }

	lda	eng_timer_crouch_jump
	cmp	#ENG_CROUCH_JUMP_TIME
	bcs	:+ ; if (eng_timer_crouch_jump < ENG_CROUCH_JUMP_TIME) {
		lda	#0
		sta	eng_timer_crouch_jump
	: ; }

	lda	joypad_p1h
	and	#(joypad_button::left|joypad_button::right)
	beq	player_crouch_friction_finish ; if (joypad_p1h_left|joypad_p1h_right) {
		and	#(joypad_button::right)
		sta	player_dir
		tay

		lda	eng_surface_traction
		lsr	a
		lsr	a
		and	frame_count
		bne	:+ ; if (!((eng_surface_traction/4)&frame_count)) {
			lda	veloc_x_player
			clc
			adc	player_crouch_frictions, y
			sta	veloc_x_player
		: ; }

		lda	#0
		sta	eng_timer_crouch_jump
		beq	:+
	; }

player_crouch_friction_finish:
	jsr	player_crouch_friction_last
	: jsr	eng_player_veloc_calc
	rts
; -----------------------------------------------------------
eng_player_jump_start:
	lda	eng_player_quicksand
	cmp	#2
	bcc	:+
	lda	eng_jump_height_quicksand
	sta	veloc_y_player
	bne	:++++
	: ; if (eng_player_quicksand < 2 || !eng_jump_height_quicksand) {
		lda	veloc_x_player
		bpl	:+
		abs_m
		:

		cmp	#8
		lda	#0
		sta	subpix_y_player
		rol	a
		ldy	eng_timer_crouch_jump
		cpy	#ENG_CROUCH_JUMP_TIME
		bcc	:+ ; if (eng_timer_crouch_jump >= ENG_CROUCH_JUMP_TIME) {
			lda	#0
			sta	joypad_p1h
		: ; }

		rol	a
		asl	a
		ora	player_item_held
		tay
		lda	eng_jump_height_standing, y
		sta	veloc_y_player
		lda	eng_jump_float_max
		sta	eng_timer_jump_float
	: ; }

	lda	#0
	sta	eng_timer_crouch_jump
	rts
; -----------------------------------------------------------
eng_player_gravity:
	lda	eng_player_quicksand
	cmp	#2
	bcc	:+ ; if (eng_player_quicksand > 2) {
		lda	eng_gravity_quicksand
		bne	:+++
	: ; } else {
		lda	eng_gravity_normal
		ldy	joypad_p1h
		bpl	:+
		lda	eng_gravity_jump
		ldy	veloc_y_player
		cpy	#<(-ENG_FLOAT_VELOC_Y)
		bmi	:+
		ldy	eng_timer_jump_float
		beq	:+ ; if (joypad_p1h_face_a && veloc_y_player == -0x4 && eng_timer_jump_float) {
			dec	eng_timer_jump_float
			lda	frame_count
			lsr	a
			lsr	a
			lsr	a
			and	#($24>>3)-1
			tay
			lda	eng_float_veloc_y, y
			sta	veloc_y_player
			rts
		: ; }

		ldy	veloc_y_player
		bmi	:+
		cpy	#$39
		bcs	:++
	: ; } if (eng_player_quicksand > 2 || veloc_y_player < 0 || veloc_y_player > 39) {
		clc
		adc	veloc_y_player
		sta	veloc_y_player
	: ; }

	lda	eng_timer_jump_float
	cmp	eng_jump_float_max
	beq	:+ ; if (eng_timer_jump_float != eng_jump_float_max) {
		lda	#0
		sta	eng_timer_jump_float
	: ; }

	rts
; ----------------------------
eng_float_veloc_y:
	.byte   <(-ENG_FLOAT_VELOC_Y), 0
	.byte	ENG_FLOAT_VELOC_Y, 0
; -----------------------------------------------------------
crouch_accel_x:
	.byte	<(-ENG_CROUCH_ACCEL_X), ENG_CROUCH_ACCEL_X
; ----------------------------
player_crouch_friction_last:
	lda	player_airborne
	bne	:++++ ; if (!player_airborne) {
		lda	frame_count
		and	eng_surface_traction
		bne	:++ ; if (!(frame_count & eng_surface_traction)) {
			lda	veloc_x_player
			and	#$80
			asl	a
			rol	a
			tay
			lda	veloc_x_player
			adc	crouch_accel_x, y
			tax
			eor	player_crouch_frictions, y
			bmi	:+ ; if (((veloc_x_player+crouch_accel_x[y])^(player_crouch_frictions[y])) >= 0) {
				ldx	#0
			: ; }

			stx	veloc_x_player
		: ; }

		lda	player_crouch
		bne	:+
		lda	eng_player_ani_frame
		cmp	#9
		beq	:+ ; if (!player_crouch && eng_player_ani_frame != 9) {
			lda	#2
			sta	eng_player_ani_frame
		: ; }
	: ; }

	rts
; -----------------------------------------------------------
eng_player_mov_velcontrib:
	.byte   $0C
	.byte	$0A
	.byte	$08
	.byte	$05
	.byte	$04
	.byte   $03
	.byte   $02

eng_player_mov_frames:
	.byte   $02
	.byte	$00
	.byte   $09
; ----------------------------
eng_player_animate_movement:
	lda	player_airborne
	ora	player_crouch
	bne	:+++++ ; if (!player_airborne && !player_crouch) {
		lda	timer_player_traction
		bne	:++++ ; if (!timer_player_traction) {
			lda	#5
			ldy	eng_surface_traction
			bne	:++ ; if (!eng_surface_traction) {
				lda	veloc_x_player
				bpl	:+
				abs_m
				:

				lsr	a
				lsr	a
				lsr	a
				tay
				lda	eng_player_mov_velcontrib, y
			: ; }
			sta	timer_player_traction

			dec	player_mov_phase
			bpl	:+ ; if (--player_mov_phase < 0) {
				lda	#1
				sta	player_mov_phase
			: ; }
		: ; }

		ldy	player_mov_phase
		lda	eng_player_mov_frames, y
		sta	eng_player_ani_frame
	: ; }

	rts
; -----------------------------------------------------------
tbl_87B2:
	.byte	$00
	.byte	$00
	.byte	<(-$30)
	.byte	$30
	.byte	<(-$30)
	.byte	$30
	.byte	<(-$30)
	.byte	$30

tbl_87BA:
	.byte	$18
	.byte	$00
	.byte	$18
	.byte	<(-$8)

tbl_87BE:
	.byte	<(-$10)
	.byte	$10
; ----------------------------
eng_player_veloc_calc:
	ldy	#2
	lda	eng_player_quicksand
	cmp	#2
	bcs	:++ ; if (eng_player_quicksand < 2) {
		dey

		lda	player_item_held
		beq	:+

		ldx	player_item_held_id
		lda	obj_id_actor, x
		cmp	#actor::vegetable_small
		bcc	:++
		cmp	#actor::mask_block
		bcc	:+
		cmp	#actor::waterfall_log
		bcc	:++
		: ; if (!player_item_held || (actor.obj_id >= vegetable_small && actor.obj_id < mask_block) || actor.obj_id >= waterfall_log) {
			dey
		; }
	: ; }

	lda	veloc_x_player
	cmp	tbl_557, y
	bmi	:+ ; if (tbl_557[y] < veloc_x_player) {
		lda	tbl_557, y
	: ; }

	cmp	tbl_55A, y
	bpl	:+ ; if (tbl_55A[y] >= veloc_x_player) {
		lda	tbl_55A, y
	: ; }

	sta	veloc_x_player
	bit	joypad_p1
	bvc	:+++
	lda	player_item_held
	beq	:+++
	ldy	#0
	ldx	player_item_held_id
	lda	proc_id_actor, x
	cmp	#actor_states::sand
	beq	:+++ ; if (!joypad[0].b || !item_held || actor_held.proc_id != sand) {
		lda	obj_id_actor, x
		cmp	#actor::mask_block
		bcc	:+
		cmp	#actor::pow_block
		bcc	:++
		: cmp	#actor::bomb
		bcc	sub_8831 ; if () {
			ldy	#2
		: ; }
		sty	zp_byte_07

		lda	player_dir
		asl	a
		ora	player_crouch
		tax
		ldy	eng_itemdir_hitboxes, x
		ldx	#0
		jsr	eng_collision_tile_type
		lda	eng_tile_type
		ldy	zp_byte_07
		jsr	col_player_type
		bcc	sub_8831
	: ; }

	rts
; ----------------------------
sub_8831:
	lda	#9
	sta	eng_player_ani_frame
	lda	#2
	sta	player_mov_phase
	lda	#$0A
	sta	timer_player_traction
	dec	player_item_held
	lda	#8
	sta	byte_602
	lda	#0
	sta	player_crouch
	sta	joypad_p1
	sta	zp_work+$01
	ldx	player_item_held_id
	lda	#actor::coin
	cmp	obj_id_actor, x
	rol	zp_work+$01

	lda	veloc_x_player
	bpl	:+
	abs_m
	:

	cmp	#8
	rol	zp_work+$01
	bne	:+ ; if () {
		ldy	player_dir
		lda	tbl_87BE, y
		clc
		adc	pos_x_lo_actor, x
		sta	pos_x_lo_actor, x

		lda	eng_level_h
		beq	:+ ; if (eng_level_h) {
			dey
			tya
			adc	pos_x_hi_actor, x
			sta	pos_x_hi_actor, x
		: ; }
	; }

	ldy	zp_work+$01
	lda	tbl_87BA, y
	sta	veloc_y_actor, x
	lda	zp_work+$01
	asl	a
	ora	player_dir
	tay
	lda	tbl_87B2, y
	sta	veloc_x_actor, x
	lda	#1
	sta	timer_actor_throw, x
	lsr	a
	sta	timer_actor_held, x
	rts
; ----------------------------
eng_obj_dophysics_y:
	ldx	#PHYSICS_X_Y_OFFSET

eng_obj_dophysics:
	lda	veloc_x_obj, x
	clc
	adc	accel_x_obj, x
	php
	bpl	:+
	abs_m
	:

	pha
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	tay
	pla
	asl	a
	asl	a
	asl	a
	asl	a
	clc
	adc	subpix_x_obj, x
	sta	subpix_x_obj, x
	tya
	adc	#0
	plp
	bpl	:+
	abs_m
	:

	ldy	#0
	cmp	#0
	bpl	:+ ; if () {
		dey
	: ; }

	clc
	adc	pos_x_lo_obj, x
	sta	pos_x_lo_obj, x
	tya
	adc	pos_x_hi_obj, x
	sta	pos_x_hi_obj, x
	lda	#0
	sta	accel_x_obj, x
	rts
