.include	"mem.i"
.include	"modes.i"

	.export eng_player_kill
eng_player_kill:
	lda	#player_state::death
	sta	proc_id_player
	lda	#0
	sta	hp_player
	sta	eng_timer_crouch_jump
	sta	timer_player_star
	lda	#7
	sta	eng_player_ani_frame
	lda	player_item_held
	beq	:+ ; if (player_item_held) {
		dec	player_item_held
		ldy	player_item_held_id
		sta	timer_actor_throw, y
		lsr	a
		sta	a:timer_actor_held, y
		sta	a:veloc_x_actor, y
		lda	#<-32
		sta	a:veloc_y_actor, y
	: ; }

	lda	#%00001000
	sta	apu_music_queue_2
	lda	#%10000000
	sta	byte_602
	sta	apu_music_base_req
	rts
