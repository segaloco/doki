.include	"actor.i"
.include	"modes.i"

.include	"mem.i"

.include	"unk.i"

	.export sub_8F38, sub_8F5A
	.export col_player_bounds, eng_player_bounds_physics
	.export eng_item_lift_replace, sub_8FF3
; -----------------------------------------------------------
sub_8F38:
	ldy	zp_addr_00+1
	lda	eng_map_bg_tile_off_v
	jsr	lib_calc_page_a_y
	sty	zp_addr_00+1
	sta	eng_map_bg_tile_off_v
	ldy	eng_level_h
	lda	zp_addr_01, y
	sta	eng_map_fg_page_no
	lda	zp_byte_02
	cmp	tbl_8F57+1, y
	bcs	:+
		lda	zp_addr_00+1
		cmp	tbl_8F57, y
	:

	rts
; ----------------------------
tbl_8F57:
	.byte	$0A, $01, $0B
; -----------------------------------------------------------
sub_8F5A:
	ldx	ppu_scroll_req
	bne	:+++++
	lda	proc_id_player
	cmp	#player_state::lift
	bcs	:+++++ ; if (!ppu_scroll_req && player.proc_id <= lift) {
		lda	eng_player_screen_y_lo
		ldy	eng_player_screen_y_hi
		bmi	:+
		bne	:++
		cmp	#$B4
		bcs	:++
		cmp	#$21
		bcs	:++++
		: ldy	player_airborne
		bne	:+++
		beq	:++
		: ; if () {
			inx
		: ; } if () {
			inx
		: ; }

		lda	game_scrolldir
		stx	game_scrolldir
		bne	:+ ; if (!game_scrolldir) {
			stx	ppu_scroll_req
		; }
	: ; }

	rts
; -----------------------------------------------------------
eng_player_bounds_physics_tbl:
	.byte   (actor_collisions::right)
	.byte   (actor_collisions::left)

	.byte   $80, $00
; ----------------------------
col_player_bounds:
	lda	eng_level_h
	beq	:+
	lda	eng_player_screen_x
	ldy	disp_dir_player
	cpy	#1
	beq	:++
	cmp	#8
	bcc	:+++
	: ; if (!eng_level_h || (disp_dir_player == 1 && eng_player_screen_x < 0xE8) || (eng_player_screen_x > 8)) {
		rts
	: ; } else if () {
		cmp	#$E8
		bcc	:--
	: ; }

	lda	collision_player
	ora	eng_player_bounds_physics_tbl-1, y
	sta	collision_player

eng_player_bounds_physics:
	ldx	#0
	ldy	disp_dir_player

	lda	veloc_x_player
	eor	eng_player_bounds_physics_tbl+1, y
	bpl	:+ ; if ((player.veloc_x ^ tbl_8F89[y+1]) < 0) {
		stx	veloc_x_player
	: ; }

	lda	accel_x_player
	eor	eng_player_bounds_physics_tbl+1, y
	bpl	:+ ; if (player.accel_x ^ tbl_8F89[y+1] < 0) {
		stx	accel_x_player
	: ; }

	stx	subpix_x_player

col_player_bounds_rts:
	rts
; -----------------------------------------------------------
eng_item_lift_replace:
	pha
	lda	pos_x_lo_actor, x
	clc
	adc	#8
	php
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	eng_map_bg_tile_off_h
	plp
	lda	pos_x_hi_actor, x

	ldy	eng_level_h
	beq	:+ ; if (eng_level_h) {
		adc	#0
	: ; }

	sta	zp_byte_02
	lda	pos_y_lo_actor, x
	clc
	adc	#8
	and	#$F0
	sta	eng_map_bg_tile_off_v
	lda	pos_y_hi_actor, x
	adc	#0
	sta	zp_addr_00+1
	jsr	sub_8F38
	pla
	bcs	col_player_bounds_rts

sub_8FF3:
	stx	zp_work+$03
	pha
	jsr	eng_map_bg_tile_off_calc
	pla
	ldy	eng_map_bg_tile_off
	sta	(zp_addr_01), y
	pha
	ldx	ppu_displist
	lda	#0
	sta	ppu_displist_addr_dbyt, x
	tya
	and	#$F0
	asl	a
	rol	ppu_displist_addr_dbyt, x
	asl	a
	rol	ppu_displist_addr_dbyt, x
	sta	ppu_displist_addr_dbyt+1, x
	tya
	and	#$0F
	asl	a
	adc	ppu_displist_addr_dbyt+1, x
	sta	ppu_displist_addr_dbyt+1, x
	clc
	adc	#$20
	sta	ppu_displist_payload+3, x
	lda	eng_level_h
	asl	a
	tay
	lda	zp_addr_00+1
	and	#$10
	bne	:+ ; if () {
		iny
	: ; }

	lda	tbl_9081, y
	clc
	adc	ppu_displist_addr_dbyt, x
	sta	ppu_displist_addr_dbyt, x
	sta	ppu_displist_payload+2, x
	lda	#2
	sta	ppu_displist_count, x
	sta	ppu_displist_payload+4, x
	pla
	pha
	and	#$C0
	asl	a
	rol	a
	rol	a
	tay
	lda	tbl_BFD3, y
	sta	zp_addr_00
	lda	tbl_BFD7, y
	sta	zp_addr_00+1
	pla
	asl	a
	asl	a
	tay
	lda	(zp_addr_00), y
	sta	ppu_displist_payload, x
	iny
	lda	(zp_addr_00), y
	sta	ppu_displist_payload+1, x
	iny
	lda	(zp_addr_00), y
	sta	ppu_displist_payload+5, x
	iny
	lda	(zp_addr_00), y
	sta	ppu_displist_payload+6, x
	lda	#0
	sta	ppu_displist_payload+7, x
	txa
	clc
	adc	#$0A
	sta	ppu_displist_offset
	ldx	zp_work+$03
	rts
; ----------------------------
tbl_9081:
	.byte	$20, $28, $20, $24
