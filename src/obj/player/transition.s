.include	"system/ppu.i"

.include	"modes.i"
.include	"tunables.i"

.include	"mem.i"



	.export eng_player_backup, eng_player_restore
	.export eng_apply_transition
	.export eng_player_transition_load, eng_player_transition_restore
	.export	eng_player_transition_reload_hearts_max
; -----------------------------------------------------------
eng_player_backup:
	lda	eng_inner_space
	bne	:+ ; if (!eng_inner_space) {
		lda	pos_x_hi_player
		sta	pos_x_hi_player_b
		lda	pos_x_lo_player
		sta	pos_x_lo_player_b
		lda	pos_y_hi_player
		sta	pos_y_hi_player_b
		lda	pos_y_lo_player
		sta	pos_y_lo_player_b
	: ; }

	rts
; ----------------------------
eng_player_restore:
	lda	pos_x_hi_player_b
	sta	pos_x_hi_player
	lda	pos_x_lo_player_b
	sta	pos_x_lo_player
	lda	pos_y_hi_player_b
	sta	pos_y_hi_player
	lda	pos_y_lo_player_b
	sta	pos_y_lo_player
	lda	eng_transition_type
	sec
	sbc	#(transition_state::rocket)-1
	bne	:- ; if (eng_transition_type == rocket) {
		sta	proc_id_player
		sta	player_lock
		sta	eng_timer_subspace_2
		jsr	actor_door_subsp_spawn_0
		lda	#$0A
		sta	eng_timer_subspace
		rts
	; } else return;
; -----------------------------------------------------------
eng_apply_transition:
	lda	eng_transition_type
	cmp	#transition_state::jar
	bne	:+
	lda	eng_jar_type
	bne	:+ ; if (eng_transition_type == jar && eng_jar_type == warp) {
		jsr	eng_player_restore
		jmp	:+++++
	: ; } else {
		lda	eng_level_page
		ldy	#0
		ldx	eng_level_h
		bne	:+ ; if (!eng_level_h) {
			sty	pos_x_hi_player
			sta	pos_y_hi_player
			beq	:++
		: ; ; } else {
			sta	pos_x_hi_player
			sty	pos_y_hi_player
		: ; }

		jsr	eng_player_transition
		ldy	pos_y_hi_player
		lda	pos_y_lo_player
		jsr	lib_calcpage_y
		sty	pos_y_hi_player
		sta	pos_y_lo_player

		lda	eng_transition_type
		cmp	#transition_state::subspace
		bne	:+ ; if (eng_transition_type == subspace) {
			jsr	actor_door_subsp_spawn_0
		: ; }
	: ; }

	lda	pos_x_lo_player
	sec
	sbc	#ENG_PADDING_CAMERA_X
	sta	eng_camera_move_x
	rts
; -----------------------------------------------------------
eng_player_transition:
	lda	eng_transition_type
	jsr	tbljmp
	.addr	eng_player_transition_reset
	.addr	eng_player_transition_door
	.addr	eng_player_transition_jar
	.addr	eng_player_transition_climb
	.addr	eng_player_transition_subspace
	.addr	eng_player_transition_rocket
; ----------------------------
eng_player_transition_reset:
	lda	#ENG_DIRECTION_RIGHT
	sta	player_dir
	jsr	eng_player_transition_jar_end
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	eng_map_bg_tile_off_h
	lda	#$D0
	sta	pos_y_lo_player
	sta	eng_map_bg_tile_off_v
	lda	eng_level_page
	sta	eng_map_fg_page_no

	lda	#$0C
	sta	zp_work+$03
	: ; for (i = 0xC; i > 0; i--) {
		jsr	eng_map_bg_tile_off_calc
		ldy	eng_map_bg_tile_off
		lda	(zp_addr_01), y
		cmp	#$40
		beq	eng_player_transition_reset_end
		jsr	eng_player_transition_reset_end
		sta	eng_map_bg_tile_off_v
		dec	zp_work+$03
		bne	:-
	; }

eng_player_transition_reset_end:
	lda	pos_y_lo_player
	sec
	sbc	#($11)-1
	sta	pos_y_lo_player
	rts
; ----------------------------
eng_player_transition_door:
	lda	pos_x_lo_player
	clc
	adc	#8
	and	#$F0
	eor	#$F0
	sta	pos_x_lo_player
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	eng_map_bg_tile_off_h
	lda	#$E0
	sta	pos_y_lo_player
	sta	eng_map_bg_tile_off_v
	lda	eng_level_page
	sta	eng_map_fg_page_no

	lda	#$0D
	sta	zp_work+$03
	: ; for (i = 0xD; i > 0; i--) {
		jsr	eng_map_bg_tile_off_calc
		ldy	eng_map_bg_tile_off
		lda	(zp_addr_01), y
		ldy	#<(eng_item_check_list_end-eng_item_check_list)
		: ; for (y = 5; y > 0; y--) {
			cmp	eng_item_check_list-1, y
			beq	eng_player_transition_door_end

			dey
			bne	:-
		; }

		dec	zp_work+$03
		beq	:+ ; if (i > 0) {
			jsr	eng_player_transition_reset_end
			sta	eng_map_bg_tile_off_v
			jmp	:--
		: ; } else break;
	; }

	jsr	eng_player_transition_jar_end

eng_player_transition_door_end:
	jsr	eng_player_transition_reset_end
	lda	#0
	sta	player_lock
	rts
; ----------------------------
eng_player_transition_jar:
	lda	#0
	sta	pos_y_lo_player

eng_player_transition_jar_end:
	lda	#1
	sta	player_airborne
	lda	#ENG_PADDING_CAMERA_X
	sta	pos_x_lo_player
	rts
; ----------------------------
eng_player_transition_climb:
	lda	pos_x_lo_player
	clc
	adc	#8
	and	#$F0
	eor	#$F0
	sta	pos_x_lo_player
	lda	eng_player_screen_y_lo
	clc
	adc	#8
	and	#$F0
	eor	#$10
	sta	pos_y_lo_player
	cmp	#$F0
	beq	:+ ; if () {
		dec	pos_y_hi_player
	: ; }

	lda	#$0A
	sta	eng_player_ani_frame
	rts
; ----------------------------
eng_player_transition_subspace:
	lda	eng_player_screen_x
	sec
	sbc	eng_camera_move_x
	eor	#$FF
	clc
	adc	#$F1
	sta	pos_x_lo_player
	lda	eng_player_screen_y_lo
	sta	pos_y_lo_player
	dec	player_lock
	lda	#$60
	sta	eng_timer_subspace_2
	rts
; ----------------------------
eng_player_transition_rocket:
	jsr	eng_player_transition_jar_end
	lda	#$60
	sta	pos_y_lo_player
	rts
; ----------------------------------------------------------
eng_player_transition_load:
	lda	eng_transition_area
	cmp	#2
	beq	:++ ; if (eng_transition_area != 2) {
		ldy	#(ENG_CHARACTERS)-1
		: ; for (y = 3; y >= 0; y--) {
			lda	eng_level, y
			sta	eng_levels_old, y
			dey
			bpl	:-
		; }

		lda	pos_x_lo_player
		sta	pos_x_lo_player_old
		lda	pos_y_lo_player
		sta	pos_y_lo_player_old
		lda	eng_player_screen_x
		sta	pos_x_lo_screen_old
		lda	eng_player_screen_y_lo
		sta	pos_y_lo_screen_old
		lda	veloc_y_player
		sta	veloc_y_player_old
		lda	proc_id_player
		sta	proc_id_player_b
	: ; }

	rts
; ----------------------------
eng_player_transition_restore:
	ldy	#(ENG_CHARACTERS)-1
	: ; for (y = 3; y >= 0; y--) {
		lda	eng_levels_old, y
		sta	eng_level, y

		dey
		bpl	:-
	; }

	lda	pos_x_lo_player_old
	sta	pos_x_lo_player
	lda	pos_y_lo_player_old
	sta	pos_y_lo_player
	lda	pos_x_lo_screen_old
	sta	eng_player_screen_x
	lda	pos_y_lo_screen_old
	sta	eng_player_screen_y_lo
	lda	veloc_y_player_old
	sta	veloc_y_player
	lda	proc_id_player_b
	sta	proc_id_player
	lda	#0
	sta	eng_inner_space
	sta	eng_jar_type
	sta	player_airborne
	sta	timer_player

eng_player_transition_reload_hearts_max:
	ldy	hp_player_max
	lda	eng_hearts_max_values, y
	sta	hp_player
	rts
; ----------------------------
	.export eng_hearts_max_values
eng_hearts_max_values:
	.byte   $1F, $2F, $3F, $04
