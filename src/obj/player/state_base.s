.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"mem.i"
.include	"misc.i"
.include	"modes.i"
.include	"tunables.i"

	.export player_proc, eng_player_dophysics_x
	.export	eng_obj_dophysics_x
player_proc:
	lda	#obj_attr::priority_high|obj_attr::color0
	sta	attr_player

	lda	proc_id_player
	jsr	tbljmp
	.addr	player_proc_normal
	.addr	player_proc_climb
	.addr	player_proc_lift
	.addr	player_proc_climbtrans
	.addr	player_proc_jarenter
	.addr	player_proc_jarexit
	.addr	player_proc_maskexit
	.addr	player_proc_death

player_proc_normal:
	jsr	eng_player_gravity
	jsr	eng_player_crouch_start
	jsr	col_player_tiles
	jsr	col_player_bounds
	jsr	eng_obj_dophysics_y

eng_player_dophysics_x:
	ldx	#0

eng_obj_dophysics_x:
	jsr	eng_obj_dophysics
	lda	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		sta	pos_x_hi_obj
	: ; }

	rts

player_proc_death:
	lda	eng_timer_player_state
	bne	:++++ ; if (!eng_timer_player_state) {
		lda	eng_player_screen_y_hi
		cmp	#2
		beq	:++ ; if (eng_player_screen_y_hi != 2) {
			jsr	eng_obj_dophysics_y
			lda	veloc_y_player
			bmi	:+ ; if (veloc_y_player < 0x80) {
				cmp	#$39
				bcs	:++++
			: ; }

			inc	veloc_y_player
			inc	veloc_y_player
			rts
		: ; }

		lda	#2
		sta	eng_player_ani_frame
		ldy	#system_modes::title_card
		dec	player_lives
		bne	:+ ; if (--player.lives == 0) {
			iny
			.assert((system_modes::game_over-system_modes::title_card) = 1), error, "system_modes game_over does not follow title_card"
		: ; }

		sty	proc_id_system
	: ; }

	rts
