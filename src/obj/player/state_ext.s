.include	"system/cpu.i"
.include	"system/ppu.i"
.include	"system/ctrl.i"

.include	"tunables.i"
.include	"modes.i"
.include	"actor.i"
.include	"misc.i"

.include	"mem.i"



	.export eng_player_frame_anim, eng_player_setpos
; -----------------------------------------------------------
	.export eng_player_frames, eng_player_frames_end
eng_player_frames:
	.byte	$00, $02, $04, $06
	.byte	$0C, $0E, $10, $12
	.byte	$00, $02, $08, $0A
	.byte   $0C, $0E, $14, $16
	.byte   $FB, $FB, $2C, $2C
	.byte	$FB, $FB, $2E, $2E
	.byte	$0C, $0E, $10, $12
	.byte	$30, $30, $32, $32
	.byte	$20, $22, $24, $26
	.byte	$00, $02, $28, $2A
	.byte	$18, $1A, $1C, $1E
eng_player_frames_end:

eng_player_frame_anim_super:
	.byte	$01, $01, $01, $02
	.byte   $02, $04, $04, $04

eng_player_one_up_pos_v:
	.byte	$06
	.byte	$07
	.byte	$01
	.byte	$04
eng_player_one_up_pos_v_end:

	.assert ((eng_player_one_up_pos_v_end-eng_player_one_up_pos_v) = ENG_CHARACTERS), error, "eng_player_one_up_pos_v player count mismatch"
; ----------------------------
eng_player_frame_anim:
	jsr	eng_player_frame_anim_gen
	lda	byte_4FE
	bne	:+++++ ; if (!byte_4FE) {
		lda	joypad_p2h
		bmi	:+ ; if (!joypad_p2h_face_a) {
			rts
		: ; }

		ldy	#$28
		: ; for (y = 0x28; y > 0; y++) {
			lda	CTRL0
			and	#$04
			bne	:+
			dey
			bne	:-
			beq	:++
		: ; } if ((CTRL0 & 0x04)) {
			lda	#$40
			sta	byte_4FE
		: ; }

		rts
	: ; } else {
		dec	byte_4FE
		lda	player_item_held
		bne	:+
		lda	eng_player_ani_frame
		cmp	#3
		bcs	:+ ; if (!player_item_held && eng_player_ani_frame < 3) {
			ldy	obj_id_player
			lda	oam_buffer+(OBJ_SIZE*8)+OBJ_POS_V
			clc
			adc	eng_player_one_up_pos_v, y
			sta	oam_buffer+(OBJ_SIZE*8)+OBJ_POS_V
			sta	oam_buffer+(OBJ_SIZE*9)+OBJ_POS_V
			lda	oam_buffer+(OBJ_SIZE*8)+OBJ_ATTR
			and	#<(~obj_attr::h_flip)
			sta	oam_buffer+(OBJ_SIZE*8)+OBJ_ATTR
			ora	#obj_attr::h_flip
			sta	oam_buffer+(OBJ_SIZE*9)+OBJ_ATTR
			lda	eng_actor_chr_idx_page_0+2*(actor_sprite::one_up)
			sta	oam_buffer+(OBJ_SIZE*8)+OBJ_CHR_NO
			sta	oam_buffer+(OBJ_SIZE*9)+OBJ_CHR_NO
		: ; }

		rts
	; }
; ----------------------------
eng_player_frame_anim_gen:
	ldy	timer_player_star
	bne	:+
	lda	timer_player
	beq	:+++ ; if (timer_player_star == 0 && timer_player != 0) {
		lsr	a
		lsr	a
		lsr	a
		lsr	a
		tay
		lda	timer_player
		and	eng_player_frame_anim_super, y
		bne	:+++
			rts
	: ; } else {
		lda	frame_count
		cpy	#24
		bcs	:+ ; if (timer_player_star < 0x18) {
			lsr	a
			lsr	a
		: ; }

		and	#obj_attr::color3
		ora	attr_player
		sta	attr_player
	: ; }

	lda	eng_player_quicksand
	beq	:+ ; if (eng_player_quicksand) {
		lda	#obj_attr::priority_low
		ora	attr_player
		sta	attr_player
	: ; }

	lda	eng_player_screen_x
	sta	oam_buffer+(OBJ_SIZE*8)+OBJ_POS_H
	sta	oam_buffer+(OBJ_SIZE*10)+OBJ_POS_H
	clc
	adc	#8
	sta	oam_buffer+(OBJ_SIZE*9)+OBJ_POS_H
	sta	oam_buffer+(OBJ_SIZE*11)+OBJ_POS_H
	lda	eng_player_screen_y_hi
	bne	:+ ; if (!eng_player_screen_y_hi) {
		lda	eng_player_screen_y_lo
		sta	oam_buffer+(OBJ_SIZE*8)+OBJ_POS_V
		sta	oam_buffer+(OBJ_SIZE*9)+OBJ_POS_V
	: ; }

	lda	eng_player_screen_y_lo
	clc
	adc	#$10
	sta	zp_addr_00
	lda	eng_player_screen_y_hi
	adc	#0
	bne	:+ ; if (!(eng_player_screen_y_hi + c)) {
		lda	zp_addr_00
		sta	oam_buffer+(OBJ_SIZE*10)+OBJ_POS_V
		sta	oam_buffer+(OBJ_SIZE*11)+OBJ_POS_V
	: ; }

	lda	eng_timer_crouch_jump
	cmp	#$3C
	bcc	:+ ; if (eng_timer_crouch_jump > 0x3C) {
		lda	frame_count
		and	#obj_attr::color1
		ora	attr_player
		sta	attr_player
	: ; }

	lda	eng_player_ani_frame
	cmp	#7
	beq	:+
	cmp	#4
	bne	:++
	: ; if (eng_player_ani_frame.in(4, 7)) {
		lda	attr_player
		sta	oam_buffer+(OBJ_SIZE*8)+OBJ_ATTR
		sta	oam_buffer+(OBJ_SIZE*10)+OBJ_ATTR
		ora	#obj_attr::h_flip
		bne	:++
	: ; } else {
		lda	player_dir
		lsr	a
		ror	a
		ror	a
		ora	attr_player
		sta	oam_buffer+(OBJ_SIZE*8)+OBJ_ATTR
		sta	oam_buffer+(OBJ_SIZE*10)+OBJ_ATTR
	: ; }

	sta	oam_buffer+(OBJ_SIZE*9)+OBJ_ATTR
	sta	oam_buffer+(OBJ_SIZE*11)+OBJ_ATTR
	lda	eng_player_ani_frame
	cmp	#6
	bcs	:+ ; if (eng_player_ani_frame < 6) {
		ora	player_item_held
	: ; }

	asl	a
	asl	a
	tax
	lda	player_dir
	bne	:+ ; if (player_direction_left) {
		lda	eng_player_frames, x
		sta	oam_buffer+(OBJ_SIZE*8)+OBJ_CHR_NO
		lda	eng_player_frames+1, x
		sta	oam_buffer+(OBJ_SIZE*9)+OBJ_CHR_NO
		lda	eng_player_frames+2, x
		sta	oam_buffer+(OBJ_SIZE*10)+OBJ_CHR_NO
		lda	eng_player_frames+3, x
		bne	:++
	: ; } if (!eng_player_frames[x+3] || player_direction_right) {
		lda	eng_player_frames+1, x
		sta	oam_buffer+(OBJ_SIZE*8)+OBJ_CHR_NO
		lda	eng_player_frames, x
		sta	oam_buffer+(OBJ_SIZE*9)+OBJ_CHR_NO
		lda	eng_player_frames+3, x
		sta	oam_buffer+(OBJ_SIZE*10)+OBJ_CHR_NO
		lda	eng_player_frames+2, x
	: ; }

	sta	oam_buffer+(OBJ_SIZE*11)+OBJ_CHR_NO
	rts
; ----------------------------
eng_player_setpos:
	lda	pos_x_lo_player
	sec
	sbc	pos_x_lo_screen_left
	sta	eng_player_screen_x
	lda	pos_y_lo_player
	sec
	sbc	pos_y_lo_screen
	sta	eng_player_screen_y_lo
	lda	pos_y_hi_player
	sbc	pos_y_hi_screen
	sta	eng_player_screen_y_hi

	lda	proc_id_player
	cmp	#player_state::lift
	bcs	eng_player_setpos_rts
	lda	eng_player_screen_y_hi
	beq	:+++
	bmi	:+ ; if (eng_player_screen_y_hi > 0) {
		lda	#0
		sta	eng_timer_player_state
		jmp	eng_player_kill
	: ; } else if (eng_player_screen_y_hi < 0) {
		lda	pos_y_hi_player
		bpl	eng_player_setpos_rts
		lda	eng_jar_type
		beq	:++
		lda	pos_y_lo_player
		cmp	#$F0
		bcs	eng_player_setpos_rts ; if (pos_y_hi_player < 0 && eng_jar_type && pos_y_lo_player < 0xF0) {
			jsr	eng_map_reset
			pla
			pla
			ldy	#0
			sty	player_crouch
			sty	veloc_y_player
			sty	veloc_x_player
			lda	#player_state::jarexit
			sta	proc_id_player
			lda	#4
			sta	eng_player_ani_frame

			lda	eng_jar_type
			sty	eng_jar_type
			cmp	#2
			bne	:+ ; if (eng_jar_type == 2) {
				sta	eng_transition_area
				rts
			: ; } else {
				sty	eng_inner_space

			eng_player_setpos_rts:
				rts
			; }
		; }
	: ; }

	lda	proc_id_player
	cmp	#player_state::climb
	bne	eng_player_setpos_rts
	lda	eng_inner_space
	cmp	#$02
	beq	eng_player_setpos_rts
	lda	eng_player_climb_veloc_y_up
	ldy	pos_y_hi_player
	bmi	:+
	lda	eng_player_screen_y_lo
	cmp	#$B8
	bcc	eng_player_setpos_rts ; if (player.proc_id == climb && eng_inner_space != 2 && player.pos_y >= 0 && eng_player_screen_y_lo >= 0xB8) {
		lsr	pos_y_lo_player
		sec
		rol	pos_y_lo_player
		lda	eng_player_climb_veloc_y_down
	: ; }
	sta	veloc_y_player

	lda	#player_state::climbtrans
	sta	proc_id_player
	rts
