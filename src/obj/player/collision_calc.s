.include	"mem.i"
.include	"macros.i"

	.export col_player_calc
col_player_calc_v:
	lda	zp_map_pointer, y
	sec
	sbc	zp_map_pointer+1, y
	bpl	:+
	abs_m
	dex
	:

	sec
	sbc	zp_byte_09, x
	rts
; ----------------------------
col_player_calc_h:
	lda	zp_map_pointer, y
	sec
	sbc	zp_map_pointer+1, y
	sta	zp_map_pointer+1, y
	lda	zp_addr_01, y
	sbc	zp_byte_02, y
	bpl	:+ ; if ((zp_addr_01[y] - zp_byte_02[y]) < 0) {
		eor	#$FF
		pha
		lda	zp_map_pointer+1, y
		abs_m
		sta	zp_map_pointer+1, y
		pla
		adc	#0
		dex
	: ; }

	cmp	#0
	beq	:+ ; if (zp_addr_01[y] != 0) {
		sec
		rts
	: ; } else {
		lda	zp_map_pointer+1, y
		sbc	zp_byte_09, x
		rts
	; }
; ----------------------------
col_player_calc:
	txa
	pha

	ldy	#2
	: ; for (y = 2; y >= 0; y -= 2) {
		tya
		tax
		inx

		cpy	#0
		bne	:+
		lda	eng_level_h
		bne	:+ ; if (!y && !eng_level_h) {
			jsr	col_player_calc_v
			jmp	:++
		: ; } else {
			jsr	col_player_calc_h
		: ; }

		bcs	col_player_calc_rts

		pha
		tya
		lsr	a
		tax
		pla
		sta	eng_col_res, x
		dey
		dey
		bpl	:---
	; } if () {
		clc
	; }

col_player_calc_rts:
	pla
	tax
	rts
