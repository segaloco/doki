.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"modes.i"
.include	"joypad.i"
.include	"macros.i"

	.export nmi, nmi_apu_call, int_null, irq_sprite, irq_bg

	: ; if (ppu_nmi_hold) {
		lda	ppu_ctlr1_b
		sta	PPU_CTLR1
		jmp	nmi_apu_call
	; }

nmi:
	php
	pha
	txa
	pha
	tya
	pha

	lda	#(ppu_ctlr1::objblk_on|ppu_ctlr1::bgblk_on|ppu_ctlr1::objlblk_on|ppu_ctlr1::bglblk_on|ppu_ctlr1::color)
	sta	PPU_CTLR1
	jsr	fdsbios_set_obj_dma
	lda	ppu_nmi_hold
	bne	:-

	lda	eng_level_h
	beq	:++++++ ; if (eng_level_h) {
		lda	ppu_has_tilescroll
		beq	:+++ ; if (ppu_has_tilescroll) {
			lda	#0
			sta	ppu_has_tilescroll
			ldx	#PPU_CTLR0_INC_PAGESIZE-2
			ldy	#0
			lda	PPU_SR
			lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_32|ppu_ctlr0::bg1|ppu_ctlr0::bg_even)
			sta	PPU_CTLR0
			: ; for (y = 0; y < 0x3C; y += 31) {
				lda	a:ppu_tilescroll_buffer_addr
				sta	PPU_VRAM_AR
				lda	a:ppu_tilescroll_buffer_addr+1
				sta	PPU_VRAM_AR

				: ; for (x = 31; x > 0; x--) {
					lda	ppu_tilescroll_buffer, y
					sta	PPU_VRAM_IO
					iny
					dex
					bne	:-
				; }

				ldx	#PPU_CTLR0_INC_PAGESIZE-2
				inc	a:ppu_tilescroll_buffer_addr+1
				cpy	#2*(PPU_CTLR0_INC_PAGESIZE-2)
				bne	:--
			; }
		: ; }
		
		lda	ppu_bg_attr
		beq	:+++ ; if (ppu_bg_attr) {
			lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_32|ppu_ctlr0::bg1|ppu_ctlr0::bg_even)
			sta	PPU_CTLR0
			ldy	#0
			ldx	#4
			: ; for (x = 4; x > 0; x--) {
				lda	PPU_SR
				lda	ppu_bg_attr
				sta	PPU_VRAM_AR
				lda	ppu_bg_attr+1
				sta	PPU_VRAM_AR
				: ; for (y; y % 1; y++) {
					lda	ppu_hscroll_attr, y
					sta	PPU_VRAM_IO

					iny
					tya
					lsr	a
					bcs	:-
				; }

				lda	ppu_bg_attr+1
				clc
				adc	#2*4
				sta	ppu_bg_attr+1
				dex
				bne	:--
			; }

			stx	ppu_bg_attr
		; }
	: ; }

	jsr	ppu_nmi_addrsync
	jsr	int_vram_ar_reset

	lda	#$B0
	ora	ppu_scc_h_b
	ldy	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		and	#$FE
		ora	ppu_scc_v_b
	: ; }
	sta	PPU_CTLR0

	lda	ppu_scc_h
	sta	PPU_SCC_H_V
	lda	ppu_scc_v
	clc
	adc	eng_bg_y
	sta	PPU_SCC_H_V
	lda	ppu_ctlr1_b
	sta	PPU_CTLR1

	inc	frame_count

nmi_finish:
	lda	nmi_buffer_ptr
	bne	:+ ; if (!nmi_buffer_ptr) {
		sta	ppu_displist_offset
		sta	ppu_displist_addr_dbyt
	: ; }

	lda	#nmi_buffer_modes::mode_00
	sta	nmi_buffer_ptr

	jsr	joypad_read

	dec	ppu_nmi_hold

nmi_apu_call:
	jsr	apu_cycle

nmi_exit:
	pla
	tay
	pla
	tax
	pla
	plp

int_null:
	rti
; -----------------------------------------------------------
irq_sprite:
	php
	pha
	txa
	pha
	tya
	pha

	jsr	fdsbios_set_obj_dma

	lda	ppu_ctlr1_b
	sta	PPU_CTLR1

	jsr	apu_cycle

	lda	ppu_ctlr0_b
	sta	PPU_CTLR0

	dec	ppu_nmi_hold

	jmp	nmi_exit
; -----------------------------------------------------------
irq_bg:
	php
	pha
	txa
	pha
	tya
	pha

	lda	#0
	sta	PPU_CTLR1

	jsr	fdsbios_set_obj_dma

	jsr	ppu_displist_write
	jsr	int_vram_ar_reset

	lda	ppu_scc_h
	sta	PPU_SCC_H_V
	lda	#0
	sta	PPU_SCC_H_V

	lda	ppu_ctlr1_b
	sta	PPU_CTLR1

	jmp	nmi_finish
; -----------------------------------------------------------
int_vram_ar_reset:
	lda	PPU_SR
	lda	#>(PPU_VRAM_COLOR)
	sta	PPU_VRAM_AR
	lda	#<(PPU_VRAM_COLOR)
	sta	PPU_VRAM_AR
	sta	PPU_VRAM_AR
	sta	PPU_VRAM_AR
	rts
