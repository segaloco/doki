.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"

.segment	"ENDING_1DATA"

L0000		:= $0000
L000C		:= $000C
L741D		:= $741D
L7A1F		:= $7A1F
L8146		:= $8146
L815C		:= $815C
L8450		:= $8450
L86B2		:= $86B2
L877F		:= $877F
L8893		:= $8893
L890F		:= $890F
L995F		:= $995F
L9E00		:= $9E00
L9E01		:= $9E01
L9E1E		:= $9E1E
L9E1F		:= $9E1F
LA956		:= $A956
LAF80		:= $AF80
LB178		:= $B178
LB95D		:= $B95D
LC755		:= $C755
LC100:	.byte	$01
LC101:	.byte	$03
	php
	cmp	($C1, x)
	.byte	$C2
	.byte	$EF
	.byte	$C2
	jsr	L9E00
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	jsr	L9E01
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$22
	.byte	$02
	stx	$7372
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$22
	.byte	$03
	stx	$7273
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$23
	.byte	$44
	clc
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$23
	.byte	$64
	clc
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$23
	sty	$18
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$23
	ldy	$18
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$22
	.byte	$1C
	stx	$7372
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
LC1E1:	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$22
	ora	$738E, x
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	jsr	L9E1E
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	jsr	L9E1F
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$73
	.byte	$72
	.byte	$22
	dec	$C4
	.byte	$FC
	.byte	$22
	.byte	$C7
	cpy	$FC
	.byte	$22
	iny
	sty	$AD
	ldy	$ACAC
	.byte	$22
	sbc	#$83
	lda	$ACAC
	.byte	$23
	asl	a
	.byte	$82
	lda	$23AC
	.byte	$2B
	ora	($AD, x)
	.byte	$22
	bcc	LC1E1
	dey
	.byte	$89
	.byte	$89
	sty	$9122
	sty	$8A
	.byte	$8B
	.byte	$8B
	sta	$0E23
	asl	$74
	ror	$74, x
	ror	$74, x
	ror	$23, x
	rol	$7506
	.byte	$77
	adc	$77, x
	adc	$77, x
	.byte	$23
	cpy	#$20
	.byte	$22
	brk
	brk
	brk
	brk
	brk
	brk
	dey
	.byte	$22
	brk
	brk
	brk
	brk
LC28A:	brk
	brk
	dey
	.byte	$22
	brk
	brk
	brk
	brk
	brk
	brk
	dey
	.byte	$22
	brk
	brk
	brk
	brk
	brk
	brk
	dey
	.byte	$23
	cpx	#$20
	tax
	brk
	brk
	brk
	brk
	brk
	brk
	tax
	tax
	brk
	brk
	brk
	ora	(L0000), y
	brk
	tax
	tax
	ldy	#$A0
	.byte	$A4
LC2B4:	lda	$A0
	ldy	#$AA
	asl	a
	asl	a
	asl	a
	asl	a
	asl	a
	asl	a
	asl	a
	asl	a
	brk
	and	(L000C, x)
	ora	#$ED
	sbc	($3A, x)
	.byte	$E7
	cpx	$FB
	.byte	$F2
	inx
	inc	$4D21
	.byte	$06
LC2D0:	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	brk
LC2D7:	.byte	$E2
	inc	$3A
	.byte	$E3
	.byte	$E2
	.byte	$E7
	.byte	$FB
	sbc	$E2
	.byte	$E7
	.byte	$3A
	.byte	$FB
	.byte	$FB
	inc	$3A
	inc	$3A
	.byte	$FB
	.byte	$FB
	sbc	#$3A
	sbc	#$3A
	.byte	$FB
	.byte	$22
	.byte	$54
	asl	$E9
	sbc	$DE
	.byte	$3A
	cpx	$22DE
	sta	$03, x
	cpx	$EDDE
	.byte	$22
	.byte	$D4
	asl	$EC
	.byte	$E2
	cmp	$FBDE, x
	.byte	$3A
	brk
LC308:	bmi	LC28A
	.byte	$80
	.byte	$80
	.byte	$80
	.byte	$80
	.byte	$80
	.byte	$80
	.byte	$80
	.byte	$80
LC312:	.byte	$B0
LC313:	ldy	#$A0
	ldy	#$A0
	ldy	#$A0
	ldy	#$A0
	.byte	$95
LC31C:	bpl	LC312
	.byte	$0C
	inx
	clc
	cpx	$F814
	php
	brk
LC326:	brk
	cpy	$C4
	clv
	clv
	tay
	tay
	ldy	#$A0
	brk
LC330:	brk
	beq	LC313
	cpy	#$A0
	.byte	$80
	rts

	rti

	.byte	$20
	brk
LC33A:	brk
LC33B:	and	($61, x)
	and	($61, x)
	and	($61, x)
	and	($61, x)
	.byte	$22
LC344:	lda	#$00
	beq	LC34A
LC348:	lda	#$1E
LC34A:	sta	$FE
LC34C:	lda	$11
	asl	a
	tax
	lda	LC100, x
	sta	$F0
	lda	LC101, x
	sta	$F1
	lda	#$00
	sta	$EB
LC35E:	lda	$EB
	bpl	LC35E
	rts

LC363:	.byte	$DF
LC364:	.byte	$C3
	.byte	$F2
	.byte	$C3
	jsr	LC344
	jsr	LB95D
	jsr	LC344
	jsr	L8146
	lda	#$40
	sta	$0100
	lda	#$B0
	sta	PPU_CTLR0
	jsr	LC34C
	lda	#$01
	sta	$11
	jsr	LC34C
	lda	#$60
	sta	$83
	lda	#$01
	sta	$9D
	lsr	a
	sta	$50
	sta	$F3
	sta	$8E
	ldx	#$09
LC398:	lda	LC308, x
	sta	$28, x
	lda	LC312, x
	sta	$32, x
	lda	LC31C, x
	sta	$3C, x
	lda	LC326, x
	sta	$46, x
	lda	LC330, x
	sta	$85, x
	lda	LC33A, x
	sta	$64, x
	dex
	bpl	LC398
	jsr	LC348
LC3BC:	jsr	LC34C
	inc	$10
	jsr	L815C
	jsr	LC433
	jsr	LC54A
	lda	$83
	bne	LC3FF
	lda	$F3
	asl	a
	tay
	lda	LC363, y
	sta	L000C
	lda	LC364, y
	sta	$0D
	jmp	(L000C)

	lda	FDS_DISK_SR
	lsr	a
	bcc	LC3EB
	dec	$F2
	bpl	LC3EF
	inc	$F3
LC3EB:	lda	#$1E
	sta	$F2
LC3EF:	jmp	LC3BC

	lda	FDS_DISK_SR
	lsr	a
	bcs	LC3EB
	dec	$F2
	bpl	LC3EF
	jmp	LC430

LC3FF:	lda	$10
	and	#$07
	bne	LC3BC
	dec	$83
	bne	LC40D
	lda	#$03
	sta	$11
LC40D:	lda	$83
	cmp	#$18
	bne	LC3BC
	lda	$8F
	asl	a
	asl	a
	adc	$8F
	adc	$8F
	tay
	ldx	#$00
LC41E:	lda	LC2D7, y
	sta	LC2D0, x
	iny
	inx
	cpx	#$06
	bcc	LC41E
	lda	#$02
	sta	$11
	bne	LC3BC
LC430:	jmp	L7A1F

LC433:	lda	$84
	beq	LC439
	dec	$84
LC439:	lda	$82
	beq	LC43F
	dec	$82
LC43F:	lda	$28
	sta	$0428
	lda	$32
	sta	$042B
	jsr	L890F
	lda	$50
	jsr	L741D
	.byte	$5B
	cpy	$75
	cpy	$9C
	cpy	$C2
	cpy	$E7
	cpy	$20
	.byte	$7F
	.byte	$87
	jsr	L8450
	lda	$28
	cmp	#$3E
	bcc	LC49B
	inc	$50
	lda	#$06
	sta	$C7
LC46D:	lda	#$01
	sta	apu_sfx_queue2
	jmp	L86B2

	jsr	L8450
	jsr	LC502
	jsr	L8893
	lda	$46
	bmi	LC49B
	lda	$32
	cmp	#$A0
	bcc	LC48F
	lda	#$0C
	sta	$3C
	jmp	LC46D

LC48F:	cmp	#$75
	bcc	LC49B
	lda	$28
	cmp	#$70
	bcc	LC49B
	inc	$50
LC49B:	rts

	jsr	L877F
	jsr	L8450
	lda	$28
	cmp	#$80
	bcc	LC49B
	inc	$50
	inc	$9C
	lda	#$08
	sta	$C7
	lda	#$05
	sta	$8E
	lda	#$28
	sta	$82
LC4B8:	rts

	.byte	$14
	asl	a
	.byte	$14
LC4BC:	asl	a
	.byte	$1C
	.byte	$1B
	asl	$1F1D, x
	lda	$82
	bne	LC4E6
	dec	$8E
	bne	LC4DF
	inc	$50
	lda	#$06
	sta	$C7
	lda	#$08
	sta	apu_dpcm_queue
	lda	#$01
	sta	apu_music_base_req
	lda	#$A0
	sta	$4F
	rts

LC4DF:	ldy	$8E
	lda	LC4B8, y
	sta	$82
LC4E6:	rts

	jsr	LC50D
	jsr	LC502
	jsr	L8893
	lda	$46
	bmi	LC4FD
	lda	$32
	cmp	#$80
	bcc	LC4FD
	jmp	L86B2

LC4FD:	rts

LC4FE:	.byte	$04
	.byte	$04
	ora	($04, x)
LC502:	ldy	$8F
	lda	LC4FE, y
	clc
	adc	$46
	sta	$46
	rts

LC50D:	ldx	#$07
LC50F:	stx	$12
	lda	$86, x
	beq	LC520
	cmp	#$01
	bne	LC544
	lda	#$08
	sta	byte_602
	bne	LC544
LC520:	jsr	L995F
	lda	$47, x
	cmp	#$08
	bmi	LC538
	lda	#$00
	sta	$3D, x
	lda	#$F9
	sta	$47, x
	lda	LC33B, x
	eor	#$40
	sta	$65, x
LC538:	lda	$10
	asl	a
	and	#$02
	sta	$0F
	jsr	LC57A
	inc	$86, x
LC544:	dec	$86, x
	dex
	bpl	LC50F
	rts

LC54A:	lda	#$04
	sta	$0F
	ldx	#$08
	stx	$12
	jsr	LC57A
	ldy	$8E
	bne	LC562
	lda	$3B
	cmp	#$F0
	bcs	LC56A
	jmp	LB178

LC562:	lda	LC4BC, y
	clc
	adc	$32
	sta	$3B
LC56A:	rts

LC56B:	inx
LC56C:	nop
	cpx	$61EE
	.byte	$63
LC571:	bmi	LC5AB
	rti

	pha
	bvc	LC5CF
	rts

	pla
	brk
LC57A:	ldy	LC571, x
	lda	$33, x
	sta	$0200, y
	sta	$0204, y
	lda	$29, x
	sta	$0203, y
	clc
	adc	#$08
	sta	$0207, y
	lda	$65, x
	sta	$0202, y
	sta	$0206, y
	ldx	$0F
	and	#$40
	bne	LC5A9
	lda	LC56B, x
	sta	$0201, y
	lda	LC56C, x
	bne	LC5B2
LC5A9:	.byte	$BD
	.byte	$6C
LC5AB:	cmp	$99
	ora	($02, x)
	lda	LC56B, x
LC5B2:	sta	$0205, y
	ldx	$12
	rts

	sta	$B1, x
	sta	$04C7
	lda	#$02
	sta	apu_dpcm_queue
	lda	#$FE
	sta	$47, x
	jsr	LA956
	lda	$042C
	sta	L0000
	.byte	$AD
LC5CF:	and	#$04
	sec
	sbc	#$08
	sta	$01
	lda	#$02
	sta	$02
	sta	$05
	sta	L000C
	lda	$65, x
	and	#$23
	sta	$03
	ldy	#$00
	ldx	#$35
	jsr	LAF80
	lda	$01
	clc
	adc	#$10
	sta	$01
	dec	$02
	lda	$042C
	sta	L0000
	ldy	#$10
	ldx	#$35
	jmp	LAF80

	inc	$FEFE, x
	inc	$B6B4, x
	lda	$B7, x
	clv
	.byte	$FA
	lda	$FAFA, y
	.byte	$FA
	.byte	$B2
	.byte	$B3
	ldx	$BFBE, y
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	lsr	a
LC619:	lsr	a
	.byte	$4B
	.byte	$4B
	.byte	$5E
LC61D:	.byte	$5F
	lsr	$465F, x
LC621:	.byte	$FC
	lsr	$FC
	.byte	$FC
LC625:	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
LC629:	pha
	.byte	$FC
	pha
	tay
LC62D:	eor	$A9A9, x
	.byte	$74
LC631:	ror	$75, x
	.byte	$77
	tya
LC635:	txs
	sta	$9C9B, y
LC639:	txs
	sta	$9C9B, x
LC63D:	.byte	$9E
	.byte	$9B
	.byte	$9F
	cli
LC641:	.byte	$5A
	eor	$5E5B, y
	.byte	$5F
	lsr	$725F, x
	.byte	$73
	.byte	$73
	.byte	$72
	ldx	$A6
	.byte	$A7
	.byte	$A7
	.byte	$72
	.byte	$73
	.byte	$73
	.byte	$72
	.byte	$74
	ror	$75, x
	.byte	$77
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	bcs	LC619
	.byte	$FA
	.byte	$FA
	bcs	LC61D
	.byte	$FA
	.byte	$FA
	bcs	LC621
	.byte	$FA
	.byte	$FA
	bcs	LC625
	.byte	$FA
	.byte	$FA
	bcs	LC629
	.byte	$FA
	.byte	$FA
	bcs	LC62D
	.byte	$FA
	.byte	$FA
	bcs	LC631
	.byte	$FA
	.byte	$FA
	bcs	LC635
	.byte	$FA
	.byte	$FA
	bcs	LC639
	.byte	$FA
	.byte	$FA
	bcs	LC63D
	.byte	$FA
	.byte	$FA
	bcs	LC641
	ldy	#$A2
	lda	($A3, x)
	.byte	$80
	.byte	$82
	sta	($83, x)
	.byte	$F4
	stx	$F5
	.byte	$87
	sty	$86
	sta	$87
	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
	lda	$ACFB
	lda	$ACAC
	ldy	$FBAC
	.byte	$3B
	.byte	$3B
	ldy	$FCFC
	.byte	$FC
	.byte	$FC
	.byte	$F4
	stx	$F5
	.byte	$87
	.byte	$FB
	eor	#$49
	.byte	$FB
	inc	$FEFE, x
	inc	$3E3C, x
	and	$583F, x
	inc	$5A59, x
	.byte	$5B
	.byte	$5A
	inc	$FEFE, x
	eor	$5C5B, x
	inc	$5BFE, x
	.byte	$5A
	.byte	$1C
	inc	$FEFE, x
	inc	$FEFE, x
	inc	$1EFE, x
	inc	$6E1F, x
	.byte	$6F
	bvs	LC6EA+$6B
	.byte	$57
	.byte	$57
	.byte	$FB
	.byte	$FB
	.byte	$57
	.byte	$57
LC6EA:	inc	$30FE, x
	bmi	LC6EA
	.byte	$FB
	and	($31), y
	.byte	$FB
	.byte	$FB
	.byte	$7C
	ror	$7F7D, x
	dex
	cpy	$CDCB
	dex
	cpy	$CDCB
