.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"sound.i"

.segment	"ENDING_2DATA"

L0028		:= $0028
L0100		:= $0100
L0120		:= $0120
L01C3		:= $01C3
L01CF		:= $01CF
L0200		:= $0200
L06A6		:= $06A6
L07C6		:= $07C6
L080F		:= $080F
L08D2		:= $08D2
L08E6		:= $08E6
L180F		:= $180F
L2040		:= $2040
L21C8		:= $21C8
L21FB		:= $21FB
L2802		:= $2802
L342B		:= $342B
L3812		:= $3812
L45BE		:= $45BE
L4700		:= $4700
L48E4		:= $48E4
L4BC4		:= $4BC4
L6F6E		:= $6F6E
L6F6F		:= $6F6F
L7120		:= $7120
L7270		:= $7270
L741D		:= $741D
L7436		:= $7436
L7520		:= $7520
L753B		:= $753B
L7674		:= $7674
L7A15		:= $7A15
L7E4E		:= $7E4E
L8081		:= $8081
L80F9		:= $80F9
L8120		:= $8120
L8146		:= $8146
L815C		:= $815C
L8180		:= $8180
L8304		:= $8304
L8450		:= $8450
L8893		:= $8893
L8B63		:= $8B63
L995F		:= $995F
LAE00		:= $AE00
LB178		:= $B178
LB17D		:= $B17D
LD41D		:= $D41D
apu_fds_mod_get	:= $D43E
LE239		:= $E239
LB800:	.byte	$01
LB801:	.byte	$03
	.byte	$53
	clv
	eor	$A1BB, x
	ldy	LBD52, x
	ror	$BD, x
	sta	$BD, x
	.byte	$3A
	cpy	#$16
	.byte	$BB
	lda	($BD), y
	cmp	$BD, x
	sbc	$D1BD, y
	.byte	$C3
	.byte	$F2
	.byte	$C3
	jsr	L45BE
	ldx	LBE64, y
	.byte	$7A
	ldx	LBE9F, y
	cpy	$F1BE
	ldx	LBF29, y
	eor	$BF, x
	.byte	$93
	.byte	$BF
	cmp	$BF
	.byte	$FD
	.byte	$BF
LB834:	lda	#$00
	beq	LB83A
LB838:	lda	#$1E
LB83A:	sta	$FE
LB83C:	lda	$11
	asl	a
	tax
	lda	LB800, x
	sta	$F0
	lda	LB801, x
	sta	$F1
	lda	#$00
	sta	$EB
LB84E:	lda	$EB
	bpl	LB84E
	rts

	.byte	$20
	.byte	$00
	.byte	$20
	.byte	$80
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($20, x)
	jsr	L8120
	.byte	$80
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	jsr	L2040
	.byte	$80
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($20, x)
	rts

	jsr	L8081
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	jsr	LC68E
	lsr	$8F20, x
	dec	$5F
	jsr	LC698
	lsr	$9920, x
	dec	$5F
	and	($4E, x)
	.byte	$82
	plp
	and	#$21
	eor	$2C82, y
	and	$4F21
	lsr	a
	rol	a
	and	($6F, x)
	lsr	a
	.byte	$2B
LB901:	and	($8E, x)
	dec	$2E
	and	($8F, x)
	dec	$2E
	and	($90, x)
	dec	$2E
	and	($91, x)
	dec	$2E
	and	($92, x)
	dec	$2E
	and	($93, x)
	dec	$2E
	and	($94, x)
	dec	$2E
	and	($95, x)
	dec	$2E
	and	($96, x)
	dec	$2E
	and	($97, x)
	dec	$2E
	and	($98, x)
	stx	$2E
	rol	$3130
	rol	$212E
	sta	$2E86, y
	rol	$3332
	rol	$222E
	lsr	$2882
	and	#$22
	eor	$2C82, y
	and	$4F22
	lsr	a
	rol	a
	.byte	$22
	.byte	$6F
	lsr	a
	.byte	$2B
	.byte	$23
	brk
	jsr	L0200
	php
	asl	a
	.byte	$0C
	asl	$0604
	php
	asl	a
	.byte	$04
	asl	$0C
	asl	$0604
	php
	asl	a
	brk
	.byte	$02
	.byte	$0C
	asl	$0E0C
	brk
	.byte	$02
	.byte	$04
	asl	$04
	asl	$08
	asl	a
	.byte	$23
	jsr	L0120
	.byte	$03
	ora	#$0B
	ora	$050F
	.byte	$07
	ora	#$0B
	ora	$07
	ora	$050F
	.byte	$07
	ora	#$0B
	ora	($03, x)
	ora	$0D0F
	.byte	$0F
	ora	($03, x)
	ora	$07
	ora	$07
	ora	#$0B
	.byte	$27
	brk
	jsr	L7674
	.byte	$74
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$74, x
	ror	$27, x
	jsr	L7520
	.byte	$77
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	adc	$77, x
	.byte	$23
	rti

	jsr	L8180
	.byte	$80
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($23, x)
	rts

	jsr	L8081
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	.byte	$23
	.byte	$80
	jsr	L8180
	.byte	$80
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($23, x)
	ldy	#$20
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	.byte	$27
	rti

	jsr	L8180
	.byte	$80
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($27, x)
	rts

	jsr	L8081
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	.byte	$27
	.byte	$80
	jsr	L8180
	.byte	$80
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($27, x)
	ldy	#$20
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	sta	($80, x)
	.byte	$23
	.byte	$CB
	.byte	$04
	dey
	brk
	brk
	.byte	$22
	.byte	$23
	.byte	$D3
	.byte	$04
	dey
	ldy	#$A0
	.byte	$22
	.byte	$23
	.byte	$DB
	.byte	$04
	dey
	tax
	tax
	.byte	$22
	.byte	$23
	.byte	$E3
	.byte	$04
	dey
	tax
	tax
	.byte	$22
	.byte	$23
	beq	LBB58
	ora	$27
	beq	LBB5C
	ora	$00
	.byte	$23
	brk
	jsr	L7270
	bvs	LBB8F
	bvs	LBB91
	bvs	LBB93
	bvs	LBB95
	bvs	LBB97
	bvs	LBB99
	bvs	LBB9B
	bvs	LBB9D
	bvs	LBB9F
	bvs	LBBA1
	bvs	LBBA3
	bvs	LBBA5
	bvs	LBBA7
	bvs	LBBA9
	bvs	LBBAB
	.byte	$23
	jsr	L7120
	.byte	$73
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
	adc	($73), y
LBB58:	adc	($73), y
	adc	($73), y
LBB5C:	brk
	jsr	$0586
	.byte	$97
	sta	$9997, y
	.byte	$93
	jsr	L06A6
	tya
	txs
	tya
	txs
	.byte	$FB
	.byte	$93
	jsr	L07C6
	.byte	$97
	sta	$9997, y
	.byte	$FB
	.byte	$FB
	.byte	$93
	jsr	L08E6
	tya
	txs
	tya
	txs
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$93
	and	($06, x)
	ora	#$97
	sta	$9997, y
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$93
LBB8F:	and	($26, x)
LBB91:	asl	a
	tya
LBB93:	txs
	tya
LBB95:	txs
	.byte	$FB
LBB97:	.byte	$FB
	.byte	$FB
LBB99:	.byte	$FB
	.byte	$FB
LBB9B:	.byte	$93
	.byte	$21
LBB9D:	lsr	$4A
LBB9F:	.byte	$FB
	.byte	$21
LBBA1:	bvc	LBBA4
LBBA3:	.byte	$93
LBBA4:	.byte	$21
LBBA5:	ror	$4B
LBBA7:	.byte	$FB
	.byte	$21
LBBA9:	adc	($01), y
LBBAB:	.byte	$93
	and	($86, x)
	jmp	L21FB

	.byte	$92
	ora	($93, x)
	and	($A6, x)
	eor	L21FB
	.byte	$B3
	ora	($93, x)
	and	($C6, x)
	lsr	L21FB
	.byte	$D4
	ora	($93, x)
	and	($E6, x)
	.byte	$4F
	.byte	$FB
	and	($F5, x)
	ora	($93, x)
	.byte	$22
	asl	$50
	.byte	$FB
	.byte	$22
	asl	$01, x
	.byte	$93
	.byte	$22
	rol	$51
	.byte	$FB
	.byte	$22
	.byte	$37
	ora	($93, x)
	and	($E8, x)
	.byte	$83
	stx	$8E8F
	.byte	$22
	.byte	$33
	.byte	$83
	stx	$8E8F
	.byte	$22
	pha
	dec	$8F
	.byte	$22
	.byte	$93
	cpy	$8F
	.byte	$22
	lsr	$C8
	sta	$22, x
	.byte	$47
	iny
	sta	$22, x
	eor	#$4A
	sta	$22, x
	.byte	$54
	.byte	$44
	sta	$22, x
	cli
	ora	($96, x)
	.byte	$22
	adc	#$4A
	bcc	LBC2B
	.byte	$74
	eor	$95
	.byte	$22
	adc	$9601, y
	.byte	$22
	.byte	$89
	lsr	a
	.byte	$FC
	.byte	$22
	sty	$46, x
	sta	$22, x
	txs
	ora	($96, x)
	.byte	$22
	lda	#$4A
	.byte	$92
	.byte	$22
	lda	$46, x
	sta	$22, x
	.byte	$BB
	ora	($96, x)
	.byte	$22
	cmp	#$4A
LBC2B:	sta	($22), y
	cmp	$47, x
	sta	$22, x
	.byte	$DC
	ora	($96, x)
	.byte	$22
	sbc	$48, x
	sta	$22, x
	sbc	$9601, x
	.byte	$23
	ora	$49, x
	sta	$23, x
	asl	$9601, x
	.byte	$23
	plp
	.byte	$57
	sta	$23, x
	.byte	$3F
	ora	($96, x)
	.byte	$22
	bit	$8204
	sta	$88
	.byte	$8B
	.byte	$22
	jmp	L8304

	stx	$89
	sty	$6C22
	.byte	$04
	sty	$87
	txa
	sta	LC023
	eor	#$00
	.byte	$23
	cmp	#$03
	.byte	$44
	ora	$10, x
	.byte	$23
	cpy	a:$45
	.byte	$23
	cmp	($04), y
	.byte	$04
	ora	($04, x)
	bpl	LBC9A
	cmp	$45, x
	brk
	.byte	$23
	.byte	$DA
	asl	$10
	brk
	.byte	$04
	bpl	LBC82
LBC82:	brk
	.byte	$23
	cpx	#$12
	brk
	rti

	eor	($00), y
	.byte	$54
	.byte	$54
	bpl	LBC8E
LBC8E:	brk
	.byte	$44
	eor	($50), y
	.byte	$54
	eor	$55, x
	bpl	LBC97
LBC97:	.byte	$04
	.byte	$23
	.byte	$F2
LBC9A:	lsr	$05
	.byte	$23
	sed
	pha
	brk
	brk
	jsr	L01C3
	.byte	$3A
	jsr	L01CF
	.byte	$3C
	.byte	$22
	.byte	$C3
	.byte	$82
	.byte	$3F
	.byte	$42
	.byte	$22
	.byte	$CF
	.byte	$82
	eor	($44, x)
	jsr	L4BC4
	.byte	$3B
	jsr	LCFE3
	and	$E420, x
	.byte	$CF
	sbc	$E520, x
	.byte	$CF
	sbc	$E620, x
	.byte	$CF
	sbc	$E720, x
	.byte	$CF
	sbc	$E820, x
	.byte	$CF
	sbc	$E920, x
	.byte	$CF
	sbc	$EA20, x
	.byte	$CF
	sbc	$EB20, x
	.byte	$CF
	sbc	$EC20, x
	.byte	$CF
	sbc	$ED20, x
	.byte	$CF
	sbc	$EE20, x
	.byte	$CF
	sbc	$EF20, x
	.byte	$CF
	rol	LC422, x
	.byte	$4B
	rti

	.byte	$22
	cpx	$4B
	.byte	$43
	jsr	L48E4
	lsr	$21, x
	.byte	$04
	pha
	sbc	$2421, x
	pha
	sbc	$4421, x
	pha
	sbc	$6421, x
	pha
	sbc	$8421, x
	pha
	sbc	$A421, x
	pha
	sbc	LC421, x
	pha
	.byte	$57
	and	($35, x)
	ora	$EC
	sbc	$DF7C
	.byte	$DF
	.byte	$23
	cpy	#$48
	eor	$23, x
	iny
	.byte	$0C
	sta	$A5, x
	lda	$A5
	brk
	brk
	brk
	brk
	sta	$FFFF, y
	tax
	.byte	$23
	.byte	$D4
	.byte	$44
	brk
	.byte	$23
	cld
	.byte	$14
	sta	$AFAF, y
	tax
	eor	$55, x
	eor	$55, x
	sta	$AAAA, y
	tax
	eor	$55, x
	eor	$55, x
	sta	$AAAA, y
	tax
	.byte	$23
	cpx	$554C
	.byte	$23
	sed
	pha
	ora	$00
LBD52:	.byte	$3F
	brk
	jsr	L3812
	plp
	.byte	$0F
	.byte	$12
	bmi	LBD72
	.byte	$0F
	.byte	$12
	.byte	$37
	.byte	$27
	.byte	$0F
	.byte	$12
	bmi	LBD85
	.byte	$0F
	.byte	$12
	bmi	LBD8F
	.byte	$0F
	.byte	$12
	ora	($36, x)
	.byte	$0F
	.byte	$12
	and	$36
	.byte	$0F
	.byte	$12
LBD72:	bmi	LBD9E
	.byte	$0F
	brk
	.byte	$3F
	brk
	php
	and	($30), y
	.byte	$37
	.byte	$07
	and	($30), y
	.byte	$27
	.byte	$07
	.byte	$3F
	bpl	LBD94
	.byte	$31
LBD85:	bmi	LBDAE
	.byte	$0F
	and	($01), y
	rol	$0F, x
	and	($25), y
	.byte	$36
LBD8F:	.byte	$0F
	and	($30), y
	rol	a
	.byte	$0F
LBD94:	brk
	.byte	$3F
	brk
	clc
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
LBD9E:	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$37
	.byte	$27
	.byte	$0F
	.byte	$0B
	bmi	LBDCC
	ora	$0B
	.byte	$37
	.byte	$27
	.byte	$0F
	.byte	$0B
	.byte	$30
LBDAE:	and	$05
	brk
	.byte	$3F
	brk
	jsr	L080F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$07
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
LBDCC:	.byte	$0F
	.byte	$0F
	asl	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$0F
	brk
	.byte	$3F
	brk
	jsr	L180F
	php
	.byte	$0F
	.byte	$0F
	brk
	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$17
	.byte	$07
	.byte	$0F
	.byte	$0F
	brk
	ora	($0F, x)
	.byte	$0F
	brk
	.byte	$07
	.byte	$0F
	.byte	$0F
	.byte	$0F
	asl	$0F, x
	.byte	$0F
	ora	$16
	.byte	$0F
	.byte	$0F
	brk
	asl	a
	.byte	$0F
	brk
	.byte	$3F
	brk
	jsr	L2802
	clc
	.byte	$0F
	.byte	$02
	bpl	LBE09
	.byte	$0F
	.byte	$02
	.byte	$27
	.byte	$17
	.byte	$0F
	.byte	$02
LBE09:	bpl	LBE1C
	.byte	$0F
	.byte	$02
	bpl	LBE26
	.byte	$0F
	.byte	$02
	.byte	$0F
	rol	$0F
	.byte	$02
	ora	$26, x
	.byte	$0F
	.byte	$02
	bpl	LBE35
	.byte	$0F
LBE1C:	brk
LBE1D:	ora	#$0A
	.byte	$0B
	and	($32, x)
	ora	#$DE
	sbc	($DE), y
LBE26:	.byte	$DC
	inc	$E2ED
	.byte	$EF
	dec	$5621, x
	php
	sbc	#$EB
	inx
	cmp	$DCEE, x
LBE35:	dec	$21EB, x
	.byte	$B3
	asl	a
	sbc	($9D, x)
	.byte	$F2
	.byte	$7C
	inc	$7C
	inc	$E1DC
	.byte	$E2
	brk
	and	($32, x)
	ora	#$FB
	sbc	#$EB
	inx
	cmp	$DCEE, x
	dec	$21EB, x
	lsr	$48, x
	.byte	$FB
	and	($95, x)
	.byte	$07
	inc	$E2
	.byte	$F2
	.byte	$7C
	sbc	($E8, x)
	.byte	$E7
	and	($B3, x)
	lsr	a
	.byte	$FB
	brk
LBE64:	and	($33, x)
	php
	cmp	$EBE2, x
	dec	$EDDC, x
	inx
	.byte	$EB
	and	($95, x)
	.byte	$07
	.byte	$DC
	sbc	($7C, x)
	sbc	#$E9
	.byte	$F2
	.byte	$FB
	brk
	and	($32, x)
	ora	#$7C
	cpx	$E2EC
	cpx	$7CED
	.byte	$E7
	sbc	$5621
	php
	cmp	$EBE2, x
	dec	$EDDC, x
	inx
	.byte	$EB
	and	($95, x)
	.byte	$47
	.byte	$FB
	and	($B5, x)
	asl	$E1
	.byte	$E2
	cmp	$FBDE, x
	.byte	$7E
	brk
LBE9F:	and	($32, x)
	ora	#$DC
	sbc	($7C, x)
	.byte	$EB
	.byte	$7C
	.byte	$DC
	sbc	$EBDE
	and	($56, x)
	php
	.byte	$FB
	.byte	$FB
	cmp	$ECDE, x
	.byte	$E2
	cpx	#$E7
	and	($B3, x)
	php
	cpx	$E0EE
	.byte	$E2
	.byte	$F2
	.byte	$7C
	.byte	$E7
	.byte	$FB
	.byte	$22
	.byte	$13
	.byte	$07
	.byte	$DF
	inc	$E2E3
	.byte	$FB
	sbc	a:$EF
	and	($32, x)
	.byte	$0C
	sbc	$EDE2
	sbc	$DE
	.byte	$FB
	cmp	$ECDE, x
	.byte	$E2
	cpx	#$E7
	and	($58, x)
	lsr	$FB
	and	($95, x)
	asl	$E2
	cpx	$E1EC
	.byte	$E2
	.byte	$E7
	and	($B3, x)
	pha
	.byte	$FB
	.byte	$22
	.byte	$13
	.byte	$47
	.byte	$FB
	brk
	and	($32, x)
	.byte	$0C
	.byte	$FB
	.byte	$DC
	inx
	inc	$ECEB
	dec	$FBFB, x
	.byte	$FB
	.byte	$FB
	.byte	$FB
	and	($56, x)
	asl	$DD
	dec	$E2EC, x
	cpx	#$E7
	and	($95, x)
	lsr	$FB
	and	($B5, x)
	asl	$ED
	.byte	$7C
	.byte	$E7
	.byte	$7C
	.byte	$7D
	.byte	$DE
LBF16:	and	($F5, x)
	.byte	$07
	.byte	$F2
LBF1A:	.byte	$7C
	inc	$7C
	sbc	($DE, x)
	inc	$22
	and	$05, x
	cpx	$E8
	.byte	$E7
	.byte	$E7
	inx
	brk
LBF29:	and	($32, x)
	.byte	$07
	.byte	$FB
	cpx	$EEE8
	.byte	$E7
	cmp	L21FB, x
	eor	$08, x
	.byte	$DC
	inx
	inc	$E9
	inx
	cpx	$EBDE
	and	($95, x)
	lsr	$FB
	and	($B5, x)
	.byte	$07
	cpx	$E8
	.byte	$E7
	.byte	$DC
	sbc	($7C, x)
	.byte	$E7
	and	($F5, x)
	.byte	$47
	.byte	$FB
	.byte	$22
	and	$45, x
	.byte	$FB
	brk
	and	($32, x)
	asl	a
	sbc	#$EB
	inx
	cpx	#$EB
	.byte	$7C
	inc	$E6
	dec	$21EB, x
	eor	$48, x
	.byte	$FB
	and	($94, x)
	.byte	$07
	.byte	$E7
	.byte	$7C
	cpx	$7C
	.byte	$7F
	inx
	inc	$B521
	.byte	$47
	.byte	$FB
	and	($D4, x)
	asl	$ED
	.byte	$7C
	sbc	($F2, x)
	.byte	$7C
	.byte	$E7
	.byte	$22
	.byte	$14
	.byte	$07
	.byte	$E2
	beq	LBFFF
	.byte	$DC
	sbc	($7C, x)
	.byte	$E7
	.byte	$22
	.byte	$54
	php
	inc	$9B
	.byte	$EB
	.byte	$E2
	cpx	$7C
	.byte	$7F
	inc	$2100
	.byte	$32
	lsr	a
	.byte	$FB
	and	($94, x)
	php
	sbc	$E6E8
	.byte	$E2
	.byte	$DC
	sbc	($7C, x)
	.byte	$E7
	and	($D4, x)
	ora	#$EC
	sbc	($E2, x)
	cpx	#$DE
	.byte	$DC
	sbc	($7C, x)
	.byte	$E7
	.byte	$22
	.byte	$14
	php
	.byte	$E7
	sta	$E47C, x
	.byte	$E2
	.byte	$E2
	.byte	$EB
	.byte	$7C
	.byte	$22
	.byte	$54
	php
	inc	$7C
	.byte	$EB
	inc	$7CE6
	.byte	$EB
	inc	$2100
	.byte	$34
	.byte	$07
	inc	$DE
	cmp	$E5DD, x
	dec	$21EB, x
	sty	$08, x
	sbc	$E7DE
	.byte	$FB
	sbc	$E7DE
	.byte	$FB
	and	($D4, x)
	ora	#$E8
	sbc	$7D7C
	dec	$FBFB, x
	.byte	$FB
	.byte	$FB
	.byte	$22
	.byte	$14
	php
	inc	$7C
	cpx	$E8
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$FB
	.byte	$22
	.byte	$54
	php
	adc	$E7DE, x
	cpx	$DE
	.byte	$E2
	.byte	$FB
	.byte	$FB
	brk
	and	($32, x)
LBFFF:	ora	#$ED
	sbc	($7C, x)
	.byte	$E7
	cpx	$EC
	.byte	$FB
	.byte	$7C
	.byte	$FB
	and	($56, x)
	php
	inc	$E2
	sbc	$E5
	.byte	$E2
	inx
	.byte	$E7
	sta	$9421, x
	.byte	$47
	.byte	$FB
	and	($D3, x)
	asl	a
	sbc	#$EE
	cpx	$FBE1
	cpx	$7CED
LC023:	.byte	$EB
	sbc	$1322
	ora	#$ED
	inx
	.byte	$FB
	.byte	$EB
	dec	$E5E9, x
	.byte	$7C
	.byte	$F2
	.byte	$22
	.byte	$54
	lsr	$FB
	brk
LC036:	.byte	$8F
	.byte	$FC
	.byte	$20
	iny
LC03A:	.byte	$21
LC03B:	.byte	$97
	dec	$FB
	brk
LC03F:	.byte	$97
	stx	$95, y
LC042:	sty	$93, x
	.byte	$92
	sta	($90), y
	.byte	$8F
LC048:	bmi	LC042
	ora	($80, x)
	bmi	LC048
	ora	($88, x)
	rti

	.byte	$FC
	ora	($80, x)
	rti

	inc	$8801, x
	bmi	LC03A
LC05A:	brk
	bcc	LC08D
	.byte	$E2
	brk
	tya
	rti

	.byte	$E4
LC062:	brk
	bcc	LC0A5
	inc	$00
	tya
	bmi	LC05A
	.byte	$02
	ldy	#$30
	.byte	$F2
	.byte	$02
	tay
	rti

	.byte	$F4
	.byte	$02
	ldy	#$40
	inc	$02, x
	tay
	bmi	LC062
	brk
	bcs	LC0AD
	nop
	brk
	clv
	rti

	cpx	$B000
	rti

	inc	LB800
	bvs	LC08A
LC08A:	and	($90, x)
	.byte	$70
LC08D:	.byte	$02
	and	($98, x)
	.byte	$80
	.byte	$04
	and	($90, x)
	.byte	$80
	asl	$21
	tya
	bvs	LC09A
LC09A:	.byte	$22
	ldy	#$70
	.byte	$02
	.byte	$22
	tay
	.byte	$80
	.byte	$04
	.byte	$22
	ldy	#$80
LC0A5:	asl	$22
	tay
	jsr	LB834
	lda	#$47
LC0AD:	sta	FDS_CTLR
	jsr	L8146
	lda	#$40
	sta	L0100
	lda	#$B0
	sta	PPU_CTLR0
	jsr	LB83C
	lda	#$01
	sta	$11
	jsr	LB83C
	lda	#$04
	sta	$11
	jsr	LB838
	lda	#$4F
	sta	FDS_CTLR
	ldy	#$03
LC0D5:	lda	LC036, y
	sta	L0200, y
	dey
	bpl	LC0D5
	lda	#$00
	sta	$F3
	sta	$E5
	sta	$E6
	ldy	#$60
LC0E8:	lda	LC048, y
	sta	$0210, y
	dey
	bpl	LC0E8
	lda	#$FF
	sta	$14
	lda	#$A0
	sta	L0028
	lda	#$08
	sta	$3C
	lda	#$01
	sta	$EC
	lda	#$00
	sta	$7F39
	lda	#$CC
	sta	$7F3A
	lda	#$01
	sta	apu_music_base_req
LC110:	jsr	LB83C
	inc	$F3
	inc	$10
	jsr	LC3BA
	lda	$E6
	cmp	#$05
	bcs	LC154
LC120:	bit	PPU_SR
	bvs	LC120
LC125:	bit	PPU_SR
	bvc	LC125
	ldy	#$00
LC12C:	lda	$00
	lda	$00
	dey
	bne	LC12C
	lda	PPU_SR
	lda	$F2
	sta	PPU_SCC_H_V
	lda	#$00
	sta	PPU_SCC_H_V
	lda	$F3
	cmp	#$0A
	bcc	LC151
	lda	#$00
	sta	$F3
	lda	$F2
	sec
	sbc	#$30
	sta	$F2
LC151:	jmp	LC110

LC154:	lda	#$47
	sta	FDS_CTLR
	lda	#$01
	sta	$F2
	lsr	a
	sta	$F3
	sta	$07
	lda	#$08
	sta	$11
LC166:	jsr	LB83C
	lda	#$B0
	sta	PPU_CTLR0
	inc	$F3
LC170:	bit	PPU_SR
	bvs	LC170
LC175:	bit	PPU_SR
	bvc	LC175
	ldy	#$00
LC17C:	lda	$00
	lda	$00
	dey
	bne	LC17C
	lda	#$B0
	ora	$F2
	sta	PPU_CTLR0
	lda	PPU_SR
	lda	#0
	sta	PPU_SCC_H_V
	lda	#0
	sta	PPU_SCC_H_V
	lda	$F3
	cmp	#$14
	bcc	LC1A9
	lda	#$00
	sta	$F3
	lda	$F2
	eor	#$01
	sta	$F2
	inc	$07
LC1A9:	lda	$07
	cmp	#$10
	bcc	LC166
	lda	#$02
	sta	$07
LC1B3:	ldy	$07
	lda	LBE1D, y
	sta	$11
	jsr	LC31C
	dec	$07
	bpl	LC1B3
	jsr	L815C
	jsr	LB834
	lda	#$20
	sta	$07
LC1CB:	jsr	LB83C
	dec	$07
	bne	LC1CB
	lda	#$30
	sta	PPU_CTLR0
	ldx	#$00
	lda	PPU_SR
	lda	#$20
	ldy	#$00
	sta	PPU_VRAM_AR
	sty	PPU_VRAM_AR
	lda	#$FE
LC1E8:	sta	PPU_VRAM_IO
	iny
	bne	LC1E8
	inx
	cpx	#$03
	bcc	LC1E8
LC1F3:	sta	PPU_VRAM_IO
	iny
	cpy	#$C0
	bcc	LC1F3
	lda	#$00
LC1FD:	sta	PPU_VRAM_IO
	iny
	bne	LC1FD
	jsr	LC65C
	lda	#$B0
	sta	PPU_CTLR0
	jsr	LB83C
	lda	#$02
	sta	$11
	jsr	LB83C
	lda	#$05
	sta	$11
	jsr	LB838
LC21C:	jsr	LB83C
	jsr	LC67C
	lda	$12
	cmp	#$05
	beq	LC22B
	jmp	LC21C

LC22B:	jsr	LB834
	jsr	L8146
	lda	#$B0
	sta	PPU_CTLR0
	jsr	LB83C
	lda	#$03
	sta	$11
	jsr	LB83C
	jsr	LC8DC
	lda	#$01
	sta	$0B
	sta	$0C
	lda	#$06
	sta	$11
	jsr	LB838
	lda	#$20
	jsr	LC309
	lda	#$00
	sta	$07
LC259:	jsr	LC33A
	jsr	LC31C
	inc	$07
	lda	$07
	cmp	#$05
	bne	LC259
	dec	$07
	lda	#$60
	jsr	LC309
	lda	#$00
	sta	$08
LC272:	jsr	LC372
	jsr	LC31C
	dec	$07
	bpl	LC272
	inc	$07
	lda	#$40
	jsr	LC309
	lda	$08
	clc
	adc	#$0E
	sta	$11
LC28A:	jsr	LC33A
	jsr	LC31C
	inc	$07
	lda	$07
	cmp	#$05
	bne	LC28A
	ldy	$08
	lda	LC3A2, y
	jsr	LC309
	lda	#$00
	sta	$07
LC2A4:	jsr	LC36A
	jsr	LC31C
	inc	$07
	lda	$07
	cmp	#$05
	bne	LC2A4
	dec	$07
	ldy	$08
	lda	LC3AE, y
	jsr	LC309
	inc	$08
	lda	$08
	cmp	#$0C
	bne	LC272
LC2C4:	jsr	LB83C
	jsr	LC8EF
	lda	$F5
	and	#$10
	beq	LC2C4
	lda	#$80
	sta	apu_music_base_req
	sta	FDS_SND_VOL
	asl	a
	sta	DMCSTATUS
	lda	#$B1
	sta	$7F39
	lda	#$D1
	sta	$7F3A
	jsr	LB83C
	jmp	L7A15

	lda	$10
	and	#$03
	bne	LC308
	ldy	$E5
	lda	LC03F, y
	sta	LC03B
	lda	#$07
	sta	$11
	inc	$E5
	lda	$E5
	cmp	#$09
	bcc	LC308
	inc	$E6
LC308:	rts

LC309:	sta	$09
LC30B:	jsr	LB83C
	jsr	LC8EF
	jsr	LB83C
	jsr	LC8EF
	dec	$09
	bne	LC30B
	rts

LC31C:	jsr	LB83C
	jsr	LC8EF
	jsr	LB83C
	jsr	LC8EF
	jsr	LB83C
	jsr	LC8EF
	jsr	LB83C
	jsr	LC8EF
	jsr	LB83C
	jmp	LC8EF

LC33A:	ldx	#$03
	ldy	#$03
	lda	#$00
LC340:	sty	$00
	sta	$0302
	lda	#$00
	sta	$0305, x
	stx	$0303
	inc	$0303
	lda	#$3F
	sta	$0301
	lda	$07
	asl	a
	asl	a
	asl	a
	adc	$00
	tay
LC35D:	lda	LC37A, y
	sta	$0304, x
	dey
	dex
	dec	$00
	bpl	LC35D
	rts

LC36A:	ldx	#$03
	ldy	#$07
	lda	#$04
	bne	LC340
LC372:	ldx	#$07
	ldy	#$07
	lda	#$00
	beq	LC340
LC37A:	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	.byte	$0B
	ora	($01, x)
	.byte	$0B
	.byte	$0B
	.byte	$07
	.byte	$07
	.byte	$0B
	.byte	$0B
	ora	($11), y
	.byte	$0B
	.byte	$0B
	.byte	$17
	.byte	$17
	.byte	$0B
	.byte	$0B
	and	($21, x)
	.byte	$0B
	.byte	$0B
	.byte	$27
	.byte	$27
	.byte	$0B
	.byte	$0B
	and	($21, x)
	.byte	$0B
	.byte	$0B
	.byte	$37
	.byte	$37
	.byte	$0B
LC3A2:	bmi	LC3D4
	bmi	LC3D6
	bmi	LC3D8
	bmi	LC3DA
	bmi	LC3AD
	.byte	$30
LC3AD:	.byte	$30
LC3AE:	bvs	LC400
	bvc	LC402
	bvc	LC404
	bcc	LC406
	clv
	clv
	clv
	.byte	$01
LC3BA:	jsr	LC415
	jsr	LC59C
	lda	$E6
	jsr	L741D
	.byte	$53
	cpy	$D0
	cpy	$3F
	cmp	$7D
	cmp	$EC
	.byte	$C2
	php
	.byte	$C3
	asl	$0236
LC3D4:	.byte	$67
	.byte	$0C
LC3D6:	.byte	$0E
	.byte	$3E
LC3D8:	.byte	$02
	.byte	$DF
LC3DA:	.byte	$FF
	asl	$0160
	sty	$680E
	ora	($FF, x)
	.byte	$0F
	.byte	$14
	.byte	$04
	.byte	$3F
	rol	$96, x
	bvc	LC3FA
	.byte	$1C
	.byte	$04
	.byte	$4F
	cmp	$7FFF, x
	brk
	asl	$0236
	ror	$0E12
	.byte	$3E
	.byte	$02
LC3FA:	dec	$0EFE, x
	rts

	ora	($92, x)
LC400:	.byte	$0E
	pla
LC402:	ora	($FE, x)
LC404:	.byte	$0F
	.byte	$14
LC406:	.byte	$04
	.byte	$77
	ror	$6E
	pha
	.byte	$0F
	.byte	$1C
	.byte	$04
	.byte	$5F
	eor	$7F5F, x
	brk
LC413:	.byte	$0C
	.byte	$0D
LC415:	lda	$10
LC417:	lsr	a
	lsr	a
	lsr	a
	and	#$01
	tay
	lda	LC413, y
	.byte	$85
LC421:	.byte	$11
LC422:	rts

LC423:	cpy	#$C8
	clv
	clv
	iny
	.byte	$C0
LC429:	cpy	#$08
	cpx	#$F0
	bne	LC417
LC42F:	ora	($13), y
	ora	$211B, y
	.byte	$23
	ora	$17, x
	ora	$251F, x
	.byte	$27
LC43B:	brk
	php
	bpl	LC457
	jsr	L0028
	php
	bpl	LC45D
	.byte	$20
	plp
LC447:	brk
	brk
	brk
	brk
	brk
	brk
	bpl	LC45F
	bpl	LC461
	bpl	LC463
	jsr	L8450
	.byte	$A5
LC457:	.byte	$14
	cmp	#$01
	bne	LC480
	.byte	$A5
LC45D:	plp
	.byte	$C9
LC45F:	.byte	$20
	.byte	$90
LC461:	.byte	$1E
	.byte	$EE
LC463:	inc	$00
	lda	#$A0
	sta	$10
	ldx	#$05
LC46B:	lda	#$20
	sta	$29, x
	lda	#$A8
	sta	$33, x
	lda	LC423, x
	sta	$3D, x
	lda	LC429, x
	sta	$47, x
	dex
	bpl	LC46B
LC480:	ldy	#$A3
	lda	$10
	and	#$38
	bne	LC489
	dey
LC489:	and	#$08
	bne	LC48E
	dey
LC48E:	sty	$32
	ldx	#$0B
	ldy	#$70
LC494:	lda	$32
	clc
	adc	LC447, x
	sta	L0200, y
	lda	LC42F, x
	sta	$0201, y
	lda	#$23
	sta	$0202, y
	lda	L0028
	clc
	adc	LC43B, x
	sta	$0203, y
	lda	$14
	adc	#$00
	beq	LC4BC
	lda	#$F0
	sta	L0200, y
LC4BC:	iny
	iny
	iny
	iny
	dex
	bpl	LC494
	rts

LC4C4:	and	$3735, y
	and	$37, x
	.byte	$39
LC4CA:	brk
	asl	$03
	ora	#$0F
	.byte	$0C
	lda	$10
	bne	LC4EB
	sta	$040E
	sta	$0418
	sta	$2F
	lda	#$6F
	sta	$39
	lda	#$E6
	sta	$43
	lda	#$DA
	sta	$4D
	inc	a:$E6
LC4EB:	ldx	#$05
LC4ED:	stx	$12
	jsr	LB17D
	jsr	LB178
	ldy	#$F0
	lda	$10
	beq	LC508
	and	#$0F
	cmp	LC4CA, x
	bne	LC50A
	lda	#$20
	sta	$29, x
	ldy	#$A8
LC508:	sty	$33, x
LC50A:	txa
	asl	a
	asl	a
	tay
	lda	$29, x
	cmp	#$80
	bcs	LC518
	lda	#$F0
	bne	LC51D
LC518:	sta	$0273, y
	lda	$33, x
LC51D:	sta	$0270, y
	lda	LC4C4, x
	sta	$0271, y
	lda	#$00
	sta	$0272, y
	dex
	bpl	LC4ED
	rts

LC52F:	.byte	$5B
	eor	$5B59, y
	eor	$5B5B, y
	.byte	$59
LC537:	brk
	brk
	rti

	rti

	cpy	#$C0
	.byte	$80
	.byte	$80
	ldx	#$06
	stx	$12
	jsr	L995F
	lda	$2F
	cmp	#$C5
	bcs	LC553
	inc	a:$E6
	lda	#$C0
	.byte	$85
LC552:	.byte	$10
LC553:	lda	$10
	lsr	a
	lsr	a
	and	#$06
	tax
	ldy	#$04
LC55C:	lda	$39
	sta	$0270, y
	lda	LC52F, x
	sta	$0271, y
	lda	LC537, x
LC56A:	sta	$0272, y
	tya
	asl	a
	clc
	adc	$2F
	sta	$0273, y
	inx
	dey
	dey
	dey
	dey
	bpl	LC55C
	rts

	lda	$10
	bne	LC58A
	inc	a:$E6
	dec	$0273
	dec	$0277
LC58A:	ldy	$39
LC58C:	and	#$10
	bne	LC591
	iny
LC591:	sty	$0270
	.byte	$8C
	.byte	$74
LC596:	.byte	$02
	rts

LC598:	.byte	$6F
	and	($00), y
	.byte	$C0
LC59C:	ldx	#$03
LC59E:	lda	LC598, x
	sta	$0208, x
	dex
	bpl	LC59E
	rts

LC5A8:	clv
	.byte	$1C
	.byte	$03
	cpy	#$B8
	asl	LC803, x
	sta	$2100
	rts

	sta	$2102
	pla
	sta	$2104, x
	rts

	sta	$2106, x
	pla
	sta	$2200
	bvs	LC552
	.byte	$02
	.byte	$22
	sei
	sta	$2204, x
	bvs	LC56A
	asl	$22
	sei
	.byte	$8B
	.byte	$0C
	jsr	L8B63
	.byte	$0C
	rts

	.byte	$6B
	.byte	$9B
	asl	$6320
	.byte	$9B
	asl	$6B60
	sta	$2030
	.byte	$67
	sta	$6030
	.byte	$6F
	sta	$2032, x
	.byte	$67
	sta	$6032, x
	.byte	$6F
	sta	$2210
	pla
	sta	$6210
	bvs	LC596
	.byte	$12
	.byte	$22
	pla
	sta	$6212, x
	bvs	LC58C
	.byte	$14
	and	($6D, x)
	.byte	$8B
	.byte	$14
	adc	($75, x)
	.byte	$9B
	asl	$21, x
	adc	$169B
	adc	($75, x)
LC610:	.byte	$8F
	.byte	$8F
	.byte	$6F
	.byte	$6F
	.byte	$6F
	.byte	$6F
LC616:	sta	$6C8D
	jmp	(L6F6F)

LC61C:	sta	$8D8D
	sta	$8B8B
LC622:	php
	clc
	sec
	pha
	plp
	cli
LC628:	rts

	bvs	LC692
	pla
	.byte	$63
	.byte	$6D
LC62E:	brk
	brk
	sbc	$F803, x
	php
LC634:	.byte	$FF
	.byte	$01
LC636:	bcs	LC610-8
LC638:	.byte	$3C
	dec	$40
	dec	$1C
	asl	$1A18, x
	asl	$1A1C, x
	clc
LC644:	jsr	L08D2
	rts

	.byte	$62
	.byte	$64
	inc	$6967, x
	.byte	$6B
	adc	$F220
	ora	#$61
	.byte	$63
	adc	$66
	pla
	ror	a
	jmp	(L6F6E)

	brk
LC65C:	ldy	#$67
LC65E:	lda	LC5A8, y
	sta	L0200, y
	dey
	bpl	LC65E
	lda	#$00
	sta	$12
	sta	$10
	sta	$78
	lda	#$FF
	sta	$05
	lda	#$C0
LC675:	sta	$29
	lda	#$B8
	sta	$33
	rts

LC67C:	lda	$12
	jsr	L741D
	.byte	$8B
	dec	$70
	.byte	$C7
	txs
	.byte	$C7
	and	$55C8, x
	iny
	dec	$05
	.byte	$A5
LC68E:	ora	$D0
	.byte	$19
	.byte	$E6
LC692:	.byte	$12
	lda	#$1C
	sta	$0201
LC698:	lda	#$1E
	sta	$0205
	lda	#$03
	sta	$0202
	sta	$0206
	lda	#$08
	sta	$10
	rts

	and	#$01
	bne	LC705
	lda	$78
	and	#$01
	tax
	asl	a
	tay
	jsr	LC706
	lda	$29
	clc
	adc	LC634, x
	sta	$29
	cmp	LC636, x
	bne	LC6EB
	inc	$78
	lda	$78
	and	#$01
	tax
	asl	a
	tay
	jsr	LC706
	ldy	#$00
	ldx	#$00
LC6D5:	lda	($02), y
	sta	$0201, x
	lda	$0202, x
	eor	#$40
	sta	$0202, x
	inx
	inx
	inx
	inx
	iny
	cpy	#$02
	bne	LC6D5
LC6EB:	jsr	LC75C
	ldy	#$00
	inc	$10
	lda	$10
	and	#$04
	beq	LC6FA
	ldy	#$02
LC6FA:	lda	($02), y
	sta	$0201
	iny
	lda	($02), y
	sta	$0205
LC705:	rts

LC706:	lda	LC638, y
	sta	$02
	iny
	lda	LC638, y
	sta	$03
	rts

LC712:	lda	#$2C
	sta	$0201
	lda	#$2E
	sta	$0205
	lda	#$B8
	sta	$33
	lda	#$D0
	sta	$47
	lda	#$03
	sta	$3D
	lda	#$01
	sta	$78
	sta	$6E
	rts

LC72F:	ldx	#$00
	jsr	LB178
	lda	$47
	clc
	adc	#$03
	sta	$47
	ldx	#$00
	jsr	LB17D
	lda	$33
	cmp	#$B8
	bcc	LC75C
	lda	#$1C
	sta	$0201
	lda	#$1E
	sta	$0205
	lda	#$03
	sta	$0202
	sta	$0206
	lda	#$00
	sta	$78
LC75C:	lda	$33
	sta	L0200
	sta	$0204
	lda	$29
	sta	$0203
	clc
	adc	#$08
	sta	$0207
	rts

	dec	$10
	lda	$10
	bne	LC783
	inc	$12
LC778:	lda	#$00
LC77A:	sta	$78
	sta	$6E
	sta	$05
	.byte	$20
	.byte	$84
LC782:	.byte	$C7
LC783:	rts

LC784:	ldy	$05
	lda	LC61C, y
	sta	$32
	lda	LC628, y
	sta	L0028
	lda	#$A0
	sta	$46
	lda	LC62E, y
	sta	$3C
	rts

	jsr	L8893
	lda	$46
	clc
	adc	#$04
	sta	$46
	jsr	L8450
	lda	$6E
	bne	LC7B8
	lda	$78
	bne	LC7B8
	lda	$32
	cmp	#$70
	bcs	LC7B8
	jsr	LC712
LC7B8:	lda	$46
	bmi	LC7E6
	ldx	$05
	lda	$32
	.byte	$DD
	.byte	$16
LC7C2:	dec	$90
	.byte	$44
	lda	LC610, x
	sta	$32
	jsr	LC814
	lda	#$00
	sta	$6E
	inc	$05
	lda	$05
	cmp	#$06
	bne	LC7E0
LC7D9:	inc	$12
	lda	#$15
LC7DD:	sta	$10
	rts

LC7E0:	jsr	LC784
LC7E3:	jmp	LC809

LC7E6:	.byte	$A5
LC7E7:	.byte	$32
	.byte	$C9
LC7E9:	eor	($B0), y
	ora	$05A5, x
	and	#$02
	bne	LC809
	ldy	$05
	lda	LC622, y
	tax
	ldy	#$03
LC7FA:	lda	$0202, x
	and	#$DF
LC7FF:	.byte	$9D
	.byte	$02
LC801:	.byte	$02
	inx
LC803:	inx
	inx
	inx
	dey
	bpl	LC7FA
LC809:	jsr	LC814
	lda	$78
	.byte	$F0
LC80F:	.byte	$03
	.byte	$20
LC811:	.byte	$2F
	.byte	$C7
LC813:	rts

LC814:	ldy	$05
	lda	LC622, y
	tax
	lda	$32
	sta	L0200, x
	sta	$0204, x
	clc
	adc	#$10
	sta	$0208, x
	sta	$020C, x
	lda	L0028
	sta	$0203, x
	sta	$020B, x
	clc
	adc	#$08
	sta	$0207, x
	sta	$020F, x
	rts

	dec	$10
	lda	$10
	bne	LC854
	ldy	#$17
LC845:	lda	LC644, y
	sta	$0301, y
	dey
	bpl	LC845
	inc	$12
	lda	#$FF
	sta	$10
LC854:	rts

	dec	$10
	lda	$10
	bne	LC85F
	lda	#$05
	sta	$12
LC85F:	rts

	.byte	$3F
	.byte	$53
	brk
	clc
	.byte	$4F
	.byte	$53
	brk
	clc
	.byte	$5F
	.byte	$53
	brk
	clc
	.byte	$3F
	eor	$00, x
	rts

	.byte	$4F
	eor	$00, x
	rts

	.byte	$5F
	eor	$00, x
	rts

	.byte	$3F
	.byte	$47
	ora	($20, x)
	.byte	$3F
	eor	#$01
	plp
	.byte	$3F
	.byte	$4B
	ora	($30, x)
	.byte	$3F
	eor	$5001
	eor	$4F
	ora	($71, x)
	eor	$51
	ora	($79, x)
	.byte	$47
	.byte	$47
	ora	($86, x)
	.byte	$47
	eor	#$01
	stx	$4B47
	ora	($96, x)
	.byte	$47
	eor	$AC01
	rti

	eor	LB901
	eor	($47, x)
	ora	($CA, x)
	eor	($49, x)
	ora	($D2, x)
	eor	($4B, x)
	ora	($DA, x)
	lsr	a
	.byte	$4F
	ora	($E6, x)
	lsr	a
	eor	($01), y
	inc	$4D4B
	ora	($0B, x)
	.byte	$54
	.byte	$4F
	ora	($40, x)
	.byte	$54
	eor	($01), y
	pha
	lsr	$014D, x
	.byte	$73
	.byte	$53
	eor	$9F01
	.byte	$5F
	.byte	$4F
	ora	($C7, x)
	.byte	$5F
	eor	($01), y
	.byte	$CF
	eor	$4D, x
	ora	($E3, x)
	.byte	$5C
	eor	$1001
LC8DC:	ldy	#$7C
LC8DE:	lda	LC85F, y
	sta	$03FF, y
	cpy	#$19
	bcs	LC8EB
	sta	$01FF, y
LC8EB:	dey
	bne	LC8DE
	rts

LC8EF:	dec	$0B
	dec	$0C
	lda	$0B
	bne	LC8FE
	lda	#$08
	sta	$0B
	jsr	LC910
LC8FE:	lda	$0C
	bne	LC90F
	lda	#$0C
LC904:	sta	$0C
	ldy	#$5C
LC908:	jsr	LC91A
	cpy	#$18
	bne	LC908
LC90F:	rts

LC910:	ldy	#$7C
LC912:	jsr	LC91A
	cpy	#$5C
	bne	LC912
	rts

LC91A:	lda	$03FF, y
	sec
	sbc	#$01
	sta	$03FF, y
	sta	$01FF, y
	cmp	#$18
	bcc	LC92E
	cmp	#$60
	bcc	LC932
LC92E:	lda	#$F8
	bne	LC935
LC932:	lda	$03FC, y
LC935:	sta	$01FC, y
	lda	$03FD, y
	sta	$01FD, y
	lda	$03FE, y
	sta	$01FE, y
	dey
	dey
	dey
	dey
	rts

	ldy	$78
	lda	$33
	clc
	adc	LC778, y
	sta	$33
	ldy	$78
	ldx	#$00
LC957:	lda	$33
	sta	$0208, x
	sta	$020C, x
	sta	$0218, x
	sta	$021C, x
	clc
	adc	#$10
	sta	$0210, x
	sta	$0214, x
	sta	$0220, x
	sta	$0224, x
	dey
	bmi	LC97C
	ldx	#$20
	jmp	LC957

LC97C:	lda	$6E
	beq	LC983
	jsr	LC984
LC983:	rts

LC984:	jsr	L8893
	lda	$46
	clc
	adc	#$03
	sta	$46
	jsr	L8450
	lda	$32
	cmp	#$B8
	bcc	LC999
	inc	$12
LC999:	lda	$32
	sta	L0200
	sta	$0204
	lda	L0028
	sta	$0203
	clc
	adc	#$08
	sta	$0207
	rts

	ldy	#$48
	ldx	#$00
LC9B1:	lda	LC77A, x
	sta	L0200, y
	iny
	inx
	cpx	#$08
	bne	LC9B1
	inc	$12
	lda	#$40
	sta	$10
	lda	#$00
	sta	$6E
	lda	#$20
	sta	$04
	rts

	dec	$10
	lda	$10
	bne	LC9D4
	inc	$12
LC9D4:	dec	$04
	lda	$04
	bne	LC9F1
	lda	$6E
	and	#$01
	tax
	ldy	#$48
	lda	LC80F, x
	sta	L0200, y
	sta	$0204, y
	inc	$6E
	lda	LC811, x
	sta	$04
LC9F1:	rts

	dec	L0028
	lda	L0028
	cmp	#$2B
	bcs	LCA04
	lda	$0202
	ora	#$20
	sta	$0202
	lda	L0028
LCA04:	cmp	#$28
	bcs	LCA16
	lda	#$F0
	sta	L0200
	sta	$0204
	inc	$12
	lda	#$9F
	sta	$10
LCA16:	ldy	#$00
	jsr	LC904
	lda	L0028
	sta	$0203
	clc
	adc	#$08
	sta	$0207
	ldy	#$00
	inc	$10
	lda	$10
	and	#$04
	beq	LCA32
	ldy	#$02
LCA32:	lda	($02), y
	sta	$0201
	iny
	lda	($02), y
	sta	$0205
	lda	$12
	beq	LCA44
	jsr	LC9D4
LCA44:	rts

	dec	$10
	lda	$10
	bne	LCA4D
	inc	$12
LCA4D:	jsr	LC9D4
	rts

	jsr	L815C
	inc	$12
	lda	#$60
	sta	$10
	rts

	dec	$10
	lda	$10
	bne	LCA63
	inc	$12
LCA63:	rts

	ldy	#$3F
LCA66:	lda	LC782, y
	sta	L0200, y
	dey
	bpl	LCA66
	inc	$12
	lda	#$40
	sta	$10
	rts

	dec	$10
	lda	$10
	bne	LCA87
	ldx	#$00
	jsr	LCA88
	lda	#$FF
	sta	$10
	inc	$12
LCA87:	rts

LCA88:	lda	LC7FF, x
	sta	$0211
	sta	$0215
	lda	LC801, x
	sta	$0219
	sta	$021D
	rts

	dec	$10
	lda	$10
	bne	LCAAA
	lda	#$00
	sta	$05
	jsr	LCAB8
	inc	$12
LCAAA:	ldx	#$00
	lda	$10
	and	#$40
	bne	LCAB4
	ldx	#$01
LCAB4:	jsr	LCA88
	rts

LCAB8:	ldy	$05
	lda	LC7D9, y
	sta	$32
	lda	LC7E3, y
	sta	L0028
	lda	#$C0
	sta	$46
	lda	LC7E9, y
	sta	$3C
	rts

	jsr	L8893
	lda	$46
	clc
	adc	#$04
	sta	$46
	jsr	L8450
	lda	$32
	cmp	#$8E
	bcc	LCAF2
	inc	$05
	lda	$05
	cmp	#$04
	bne	LCAEC
	inc	$12
	rts

LCAEC:	jsr	LCAB8
	jmp	LCB14

LCAF2:	lda	$46
	bmi	LCB14
	lda	$05
	cmp	#$02
	bcc	LCB14
	and	#$01
	tay
	lda	LC7E7, y
	tax
	ldy	#$03
LCB05:	lda	$0202, x
	ora	#$20
	sta	$0202, x
	inx
	inx
	inx
	inx
	dey
	bpl	LCB05
LCB14:	ldy	$05
	lda	LC7DD, y
	tax
	lda	$32
	sta	L0200, x
	sta	$0204, x
	clc
	adc	#$10
	sta	$0208, x
	sta	$020C, x
	lda	L0028
	sta	$0203, x
	sta	$020B, x
	clc
	adc	#$08
	sta	$0207, x
	sta	$020F, x
	rts

	ldy	#$16
LCB3F:	lda	LC7C2, y
	sta	$0301, y
	dey
	bpl	LCB3F
	inc	$12
	lda	#$00
	sta	$10
	rts

	dec	$10
	lda	$10
	bne	LCB59
	lda	#$0F
	sta	$12
LCB59:	rts

LCB5A:	and	($8D, x)
	.byte	$07
	beq	LCB5A
	.byte	$DA
	.byte	$FB
	.byte	$E2
	.byte	$FB
	.byte	$ED
LCB64:	brk
	asl	$53
	eor	($56, x)
	eor	$2D
	.byte	$44
	eor	($54, x)
	brk
	ror	$06
	brk
	brk
	brk
	ror	$00
	ldy	#$03
LCB78:	lda	$062D, y
	sta	$6600, y
	dey
	bpl	LCB78
	lda	#$5A
	sta	$6604
	lda	#$A5
	sta	$6605
	ldy	#$00
	sty	$7F9D
	sty	$FD
	sty	$FC
LCB94:	lda	#$30
	sta	PPU_CTLR0
	jsr	L8146
	jsr	L7436
	lda	#$B0
	sta	PPU_CTLR0
	jsr	LBF1A
	lda	#$0A
	sta	$11
	jsr	LBF16
	lda	#$30
	sta	$FF
	sta	PPU_CTLR0
	lda	#$06
	jsr	LE239
	.byte	$97
	.byte	$7F
	adc	$CB
	bne	LCBCA
	lda	#$00
	sta	$FE
	sta	PPU_CTLR1
	jmp	L753B

LCBCA:	jsr	L80F9
	jsr	L7E4E
	jmp	LCB94

	.byte	$80
	.byte	$07
	.byte	$80
	beq	LCB78
	inc	$02, x
	sbc	($01), y
	sbc	($6C), y
	adc	$11
	.byte	$37
	.byte	$42
	bmi	LCB64
	beq	LCC08
	sbc	($CA), y
	sbc	$0A, x
	brk
	.byte	$FF
	.byte	$80
LCBED:	nop
	bpl	LCC00
	ldy	$F03C
	sty	$88F1
	iny
	.byte	$12
	sbc	$0A, x
	bpl	LCBED
	rol	a
	.byte	$FF
	.byte	$80
	inx
LCC00:	lda	#$FF
	sta	DMCFRAMECOUNT
	lda	#$0F
	.byte	$8D
LCC08:	ora	$40, x
	jsr	LCC16
	lda	#$00
	sta	apu_music_base_req
	rts

LCC13:	jmp	LCC98

LCC16:	lda	apu_music_base_req
	bmi	LCC23
	bne	LCC26
	lda	apu_music_base_current
	bne	LCC13
	rts

LCC23:	jmp	LCCAE

LCC26:	ldy	#$00
LCC28:	sty	byte_61D
LCC2B:	sta	apu_music_base_current
	inc	byte_61D
	ldy	byte_61D
	cpy	#$0D
	bne	LCC3C
	ldy	#$06
	bne	LCC28
; --------------
LCC3C:
	lda	$601B, y
	tay
	lda	$601C, y
	sta	apu_note_len_offset

	lda	$601D, y
	sta	apu_track
	lda	$601E, y
	sta	apu_track+1

	lda	$601F, y
	sta	apu_triangle_offset
	lda	$6020, y
	sta	apu_pulse_1_offset
	sta	byte_61A
	lda	$6021, y
	sta	apu_noise_offset
	sta	apu_noise_loop_offset
	lda	$6022, y
	sta	apu_wave_offset
	lda	$6023, y
	sta	zp_byte_C5
	lda	$6024, y
	sta	byte_5F4
	lda	$6025, y
	sta	byte_61C
	jsr	LCE7C
	lda	#$01
	sta	apu_pulse_2_note_len
	sta	apu_pulse_1_note_len
	sta	apu_triangle_note_len
	sta	apu_noise_note_len
	sta	byte_5F1
	lda	#$00
	sta	apu_pulse_2_offset
LCC98:	dec	apu_pulse_2_note_len
	bne	LCCE1
	ldy	apu_pulse_2_offset
	inc	apu_pulse_2_offset
	lda	(apu_track), y
	bmi	LCCBF
	bne	LCCCD
	lda	apu_music_base_current
	bne	LCCBC
LCCAE:	lda	#$00
	sta	apu_music_base_current
	sta	DMCSTATUS
	lda	#$80
	sta	FDS_SND_VOL
	rts

LCCBC:	jmp	LCC2B

LCCBF:	jsr	LCED4
	sta	apu_pulse_2_note_len_b
	ldy	apu_pulse_2_offset
	inc	apu_pulse_2_offset
	lda	(apu_track), y
LCCCD:	jsr	LCF2F
	beq	LCCD5
	jsr	LCEDF
LCCD5:	sta	apu_pulse_2_env_ctl
	jsr	LCF25
	lda	apu_pulse_2_note_len_b
	sta	apu_pulse_2_note_len
LCCE1:	ldy	apu_pulse_2_env_ctl
	beq	LCCE9
	dec	apu_pulse_2_env_ctl
LCCE9:	jsr	LCEF6
	sta	PULSE2_VOL
	ldx	#$7F
	stx	PULSE2_SWEEP
	ldy	apu_pulse_1_offset
	beq	LCD3A
	dec	apu_pulse_1_note_len
	bne	LCD27
LCCFE:	ldy	apu_pulse_1_offset
	inc	apu_pulse_1_offset
	lda	(apu_track), y
	bne	LCD10
	lda	byte_61A
	sta	apu_pulse_1_offset
	bne	LCCFE
LCD10:	jsr	LCECE
	sta	apu_pulse_1_note_len
	txa
	and	#SND_CMP_NOTE_MASK
	jsr	LCF11
	beq	LCD21
	jsr	LCEDF
LCD21:	sta	apu_pulse_1_env_ctl
	jsr	LCF07
LCD27:	ldy	apu_pulse_1_env_ctl
	beq	LCD2F
	dec	apu_pulse_1_env_ctl
LCD2F:	jsr	LCEEF
	sta	PULSE1_VOL
	lda	#$7F
	sta	PULSE1_SWEEP
LCD3A:	lda	apu_triangle_offset
	beq	LCD7D
	dec	apu_triangle_note_len
	bne	LCD7D
	ldy	apu_triangle_offset
	inc	apu_triangle_offset
	lda	(apu_track), y
	beq	LCD7A
	bpl	LCD60
	jsr	LCED4
	sta	apu_triangle_note_len_b
	ldy	apu_triangle_offset
	inc	apu_triangle_offset
	lda	(apu_track), y
	beq	LCD7A
LCD60:	jsr	LCF33
	beq	LCD74
	ldx	apu_triangle_note_len_b
	stx	apu_triangle_note_len
	txa
	cmp	#$12
	bcs	LCD78
	lda	#$0F
	bne	LCD7A
LCD74:	lda	#$80
	bne	LCD7A
LCD78:	lda	#$FF
LCD7A:	sta	TRIANGLE_VOL
LCD7D:	lda	apu_wave_offset
	bne	LCD85
	jmp	LCE3D

LCD85:	lda	byte_5F1
	cmp	#$02
	bne	LCD91
	lda	#$00
	sta	FDS_SND_VOL
LCD91:	dec	byte_5F1
	bne	LCDF2
	ldy	apu_wave_offset
	inc	apu_wave_offset
	lda	(apu_track), y
	bpl	LCDAE
	jsr	LCED4
	sta	byte_5F2
	ldy	apu_wave_offset
	inc	apu_wave_offset
	lda	(apu_track), y
LCDAE:	jsr	LCF37
	tay
	bne	LCDBB
	ldx	#$80
	stx	FDS_SND_VOL
	bne	LCDC1
LCDBB:	jsr	apu_fds_mod_get
	ldy	byte_5F7
LCDC1:	sty	byte_5F3
	ldy	#$00
	sty	byte_5F9
	sty	byte_5FB
	lda	(zp_addr_BF), y
	sta	FDS_SND_VOL
	lda	(zp_addr_C1), y
	sta	FDS_SND_MOD_ENV
	lda	#$00
	sta	FDS_SND_MOD_COUNT
	iny
	lda	(zp_addr_BF), y
	sta	byte_5F8
	lda	(zp_addr_C1), y
	sta	byte_5FA
	sty	byte_5F9
	sty	byte_5FB
	lda	byte_5F2
	sta	byte_5F1
LCDF2:	lda	byte_5F3
	beq	LCE3D
	dec	byte_5F3
	dec	byte_5F8
	bne	LCE18
	ldy	byte_5F9
LCE02:	iny
	lda	(zp_addr_BF), y
	bpl	LCE0C
	sta	FDS_SND_VOL
	bne	LCE02
LCE0C:	sta	FDS_SND_VOL
	iny
	lda	(zp_addr_BF), y
	sta	byte_5F8
	sty	byte_5F9
LCE18:	dec	byte_5FA
	bne	LCE3D
	inc	byte_5FB
	ldy	byte_5FB
	lda	(zp_addr_C1), y
	sta	FDS_SND_MOD_ENV
	iny
	lda	(zp_addr_C1), y
	sta	FDS_SND_MOD_FREQ_LO
	iny
	lda	(zp_addr_C1), y
	sta	FDS_SND_MOD_FREQ_HI
	iny
	lda	(zp_addr_C1), y
	sta	byte_5FA
	sty	byte_5FB
LCE3D:	lda	apu_music_base_current
	beq	LCE7B
	dec	apu_noise_note_len
	bne	LCE7B
LCE47:	ldy	apu_noise_offset
	inc	apu_noise_offset
	lda	(apu_track), y
	bne	LCE59
	lda	apu_noise_loop_offset
	sta	apu_noise_offset
	bne	LCE47
LCE59:	jsr	LCECE
	sta	apu_noise_note_len
	txa
	and	#SND_CMP_NOTE_MASK
	beq	LCE70
	and	#$10
	beq	LCE70
	lda	#$18
	ldx	#$03
	ldy	#$18
	bne	LCE72
LCE70:	lda	#$10
LCE72:	sta	NOISE_VOL
	stx	NOISE_PERIOD
	sty	NOISE_COUNTER
LCE7B:	rts

LCE7C:	lda	zp_byte_C5
	bne	LCE81
	rts

LCE81:	ldy	#$00
LCE83:	iny
	lsr	a
	bcc	LCE83
	lda	LCF99, y
	tay
	lda	LCF9A, y
	sta	zp_addr_BD
	lda	LCF9B, y
	sta	zp_addr_BD+1
	lda	LCF9C, y
	sta	byte_5F7
	lda	LCF9D, y
	sta	zp_addr_BF
	lda	LCF9E, y
	sta	zp_addr_BF+1
	lda	LCF9F, y
	sta	zp_addr_C1
	lda	LCFA0, y
	sta	zp_addr_C1+1
	lda	LCFA1, y
	sta	byte_5F6
	jsr	LD41D
	lda	#$02
	sta	FDS_SND_WAVE_VOL
	rts

	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$07
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$07
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LCECE:	tax
	ror	a
	txa
	rol	a
	rol	a
	rol	a
; --------------
LCED4:	and	#$07
	clc
	adc	apu_note_len_offset
	tay
	.byte	$B9
	.byte	$C3
LCEDD:	adc	$60
LCEDF:	lda	byte_5F4
	bmi	LCEE8
	lda	#$1F
	bne	LCEEA
LCEE8:	lda	#$2F
LCEEA:	ldx	#$82
	ldy	#$7F
	rts

LCEEF:	lda	byte_61C
	cmp	#$01
	beq	LCF03
LCEF6:	lda	byte_5F4
	bmi	LCEFF
	lda	LCF4A, y
	rts

LCEFF:	.byte	$B9
LCF00:	ror	a
	.byte	$CF
	rts

LCF03:	lda	LCF4A, y
	rts

LCF07:	sty	PULSE1_SWEEP
	stx	PULSE1_VOL
	rts

	jsr	LCF07
LCF11:	ldx	#$00
LCF13:	tay
	lda	$6501, y
	beq	LCF24
	sta	CHANNEL_TIMER, x
	lda	$6500, y
	ora	#$08
	sta	CHANNEL_COUNTER, x
LCF24:	rts

LCF25:	stx	PULSE2_VOL
	sty	PULSE2_SWEEP
	rts

	jsr	LCF25
LCF2F:	ldx	#$04
	bne	LCF13
LCF33:	ldx	#$08
	bne	LCF13
LCF37:	ldx	#$80
	stx	FDS_SND_FREQ+1
	tay
	lda	$6000, y
	sta	FDS_SND_FREQ+1
	lda	$6001, y
	sta	FDS_SND_FREQ
	rts

LCF4A:	bcc	LCEDD
	sta	($92), y
	.byte	$93
	.byte	$93
	sty	$94, x
	sty	$95, x
	sta	$95, x
	sta	$96, x
	stx	$96, y
	stx	$97, y
	.byte	$97
	tya
	tya
	sta	$9A9A, y
	.byte	$9B
	.byte	$9B
	.byte	$9C
	.byte	$9C
	.byte	$9C
	sta	$9E9D, x
LCF6A:	bcc	LCF00
	sty	$94, x
	sty	$95, x
	sta	$95, x
	sta	$96, x
	stx	$96, y
	stx	$97, y
	.byte	$97
	.byte	$97
	.byte	$97
	.byte	$97
	tya
	tya
	tya
	sta	$9999, y
	txs
	txs
	txs
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	sta	$9D9D, x
	sta	$9E9D, x
	.byte	$9E
	.byte	$9E
	.byte	$9E
	.byte	$9E
LCF99:	.byte	$9E
LCF9A:	.byte	$02
LCF9B:	.byte	$0B
LCF9C:	.byte	$CE
LCF9D:	.byte	$CF
LCF9E:	.byte	$44
LCF9F:	.byte	$EE
LCFA0:	.byte	$CF
LCFA1:	sed
	.byte	$CF
	jsr	LAE00
	.byte	$CF
	.byte	$44
	.byte	$F2
	.byte	$CF
	sed
	.byte	$CF
	jsr	L0100
	.byte	$02
	.byte	$03
	.byte	$04
	asl	$07
	ora	#$0B
	asl	$1310
	clc
	jsr	L342B
	.byte	$3C
	.byte	$3F
	.byte	$3F
	rol	$3A3D, x
	rol	$32, x
	.byte	$2F
	bit	$2629
	bit	$21
	asl	$1918, x
	ora	($0C, x)
	asl	$20, x
	bit	$27
	.byte	$27
	rol	a
	.byte	$2F
	.byte	$2F
	.byte	$32
	bmi	LD00E
	rol	$3A, x
	rol	$37, x
	and	($32), y
	.byte	$34
	.byte	$3D
LCFE3:	.byte	$3F
	.byte	$3F
	.byte	$3B
	sec
	and	$33, x
	bmi	LD01A
	and	#$23
	.byte	$23
	ldy	#$02
	clc
	rts

	ldy	#$02
	rol	$30, x
	.byte	$80
	.byte	$34
	.byte	$80
	and	$0A81, y
	brk
	rts

	sta	$9999, y
	txs
	txs
	txs
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
LD00E:	sta	$9D9D, x
	ora	$040D
	ora	LD027
	.byte	$44
	adc	$D0, x
LD01A:	sta	($D0, x)
	jsr	L4700
	bne	LD081
	.byte	$67
	bne	LD09F
	bne	LD026
LD026:	brk
LD027:	ora	($0C, x)
	asl	$20, x
	bit	$27
	.byte	$27
	rol	a
	.byte	$2F
	.byte	$2F
	.byte	$32
	bmi	LD067
	rol	$3A, x
	rol	$37, x
	and	($32), y
	.byte	$34
	and	$3F3F, x
	.byte	$3B
	sec
	and	$33, x
	bmi	LD073
	and	#$23
	.byte	$23
	bpl	LD075
	rol	$2927
	.byte	$2B
	rol	a
	plp
	and	$29
	.byte	$2F
	and	$2A2C
	.byte	$22
	bit	$34
	.byte	$3F
	and	($2D), y
	.byte	$3A
	.byte	$3B
	.byte	$27
	.byte	$12
	asl	a
	.byte	$1F
	bit	$2327
	plp
	.byte	$22
	.byte	$1E
LD067:	ldy	#$02
	clc
	rts

	ldy	#$02
	asl	L0028
	.byte	$80
	.byte	$03
	.byte	$82
	.byte	$10
LD073:	brk
	.byte	$50
LD075:	ldy	#$02
	rol	$25, x
	.byte	$80
	.byte	$34
	.byte	$80
	.byte	$02
	.byte	$80
	brk
	brk
	rts

LD081:	.byte	$80
	.byte	$2B
	sta	($0A, x)
	brk
	.byte	$04
	.byte	$82
	bpl	LD08A
LD08A:	rts

	ora	($07, x)
	ora	($C8, x)
	.byte	$03
	cld
	.byte	$02
	inx
	ora	($01, x)
	ora	($07, x)
	.byte	$27
	.byte	$72
	.byte	$27
	.byte	$82
	.byte	$27
	ror	$0301, x
LD09F:	.byte	$27
	cmp	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$3B
	.byte	$74
	.byte	$27
	.byte	$57
	.byte	$27
	.byte	$97
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	$27
	adc	$27
	sta	$05, x
	.byte	$42
	rol	a
	eor	($87, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	pla
	bit	$01
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($08, x)
	dey
	dey
	dey
	clv
	.byte	$80
	brk
	rti

	.byte	$9C
