.include	"mem.i"

        .export lib_calcpage_y
lib_calcpage_y:
	cpy	#0
	bmi	:+ ; if (y >= 0) {
		pha
		tya
		asl	a
		asl	a
		asl	a
		asl	a
		sta	lib_calc_page_offset
		pla
		sec
		sbc	lib_calc_page_offset
		bcs	:+ ; if (a < ((y * 16)+1)) {
			dey
		: ; }
	; }

	rts
