.include	"mem.i"

	.export lib_calc_page_a_y
lib_calc_page_a_y:
	sta	lib_calc_page_offset
	tya
	bmi	:++ ; if (!(y & 0x80)) {
		asl	a
		asl	a
		asl	a
		asl	a
		clc
		adc	lib_calc_page_offset
		bcs	:+
		cmp	#$F0
		bcc	:++
		: ; if ((((a << 4) + lib_calc_page_offset) > 0xFF) || (((a << 4) + lib_calc_page_offset) <= 0xF0)) {
			clc
			adc	#$10
			iny
		: ; }
	; }

	rts
