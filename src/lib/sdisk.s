.include	"system/ppu.i"
.include	"system/fds.i"

.include	"charmap.i"
.include	"tunables.i"

.include	"fileids.i"
.include	"mem.i"

sfiletbl_chapters:
sfiletbl_chapter1:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_1
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_01
	.byte	FILE_ID_DUMMY_DT
sfiletbl_chapter1_end:
	.assert	(sfiletbl_chapter1_end-sfiletbl_chapter1) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_chapter1 filelist length mismatch"
	.byte	FILE_ID_NULL

sfiletbl_chapter2:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_2
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_02
	.byte	FILE_ID_DUMMY_DT
sfiletbl_chapter2_end:
	.assert	(sfiletbl_chapter2_end-sfiletbl_chapter2) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_chapter2 filelist length mismatch"
	.byte	FILE_ID_NULL

sfiletbl_chapter3:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_1
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_03
	.byte	FILE_ID_DUMMY_DT
sfiletbl_chapter3_end:
	.assert	(sfiletbl_chapter3_end-sfiletbl_chapter3) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_chapter3 filelist length mismatch"
	.byte	FILE_ID_NULL

sfiletbl_chapter4:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_3
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_04
	.byte	FILE_ID_DUMMY_DT
sfiletbl_chapter4_end:
	.assert	(sfiletbl_chapter4_end-sfiletbl_chapter4) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_chapter4 filelist length mismatch"
	.byte	FILE_ID_NULL

sfiletbl_chapter5:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_1
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_05
	.byte	FILE_ID_DUMMY_DT
sfiletbl_chapter5_end:
	.assert	(sfiletbl_chapter5_end-sfiletbl_chapter5) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_chapter5 filelist length mismatch"
	.byte	FILE_ID_NULL

sfiletbl_chapter6:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_2
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_06
	.byte	FILE_ID_DUMMY_DT
sfiletbl_chapter6_end:
	.assert	(sfiletbl_chapter6_end-sfiletbl_chapter6) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_chapter6 filelist length mismatch"
	.byte	FILE_ID_NULL

sfiletbl_chapter7:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_4
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_07
	.byte	FILE_ID_DUMMY_DT
sfiletbl_chapter7_end:
	.assert	(sfiletbl_chapter7_end-sfiletbl_chapter7) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_chapter7 filelist length mismatch"
	.byte	FILE_ID_NULL

sfiletbl_credits:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_4
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_ENDING_1
	.byte	FILE_ID_SOUND_DT
	.byte	FILE_ID_DUMMY_DT
sfiletbl_credits_end:
	.assert	(sfiletbl_credits_end-sfiletbl_credits) = (ENG_FILELIST_SIDEB_LEN + 1), error, "sfiletbl_credits filelist length mismatch"
	.byte	FILE_ID_NULL

sfilelist_chapters:
	.addr   sfiletbl_chapter1
	.addr   sfiletbl_chapter2
	.addr   sfiletbl_chapter3
	.addr   sfiletbl_chapter4
	.addr   sfiletbl_chapter5
	.addr	sfiletbl_chapter6
	.addr	sfiletbl_chapter7
	.addr	sfiletbl_credits
sfilelist_chapters_end:
	.assert	((sfilelist_chapters_end-sfilelist_chapters)/2) = (ENG_CHAPTER_COUNT + 1), error, "sfilelist_chapters chapter count mismatch"
; ----------------------------
sdisk_load_files_disk_swap:
	jsr	main_disk_swap

	.export	sdisk_load_files_sideb
sdisk_load_files_sideb:
	ldy	#1
	sty	disk_sideb_current
	jsr	disk_ppu_init
	lda	course_no
	asl	a
	tax
	adc	course_no
	asl	a
	adc	course_no
	tay
	lda	#FILE_ID_CHAR_C
	clc
	adc	obj_id_player
	sta	sfiletbl_chapters, y
	lda	sfilelist_chapters, x
	sta	sfileptr_chapter
	lda	sfilelist_chapters+1, x
	sta	sfileptr_chapter+1
	jsr	sdisk_filetbl_load
		.addr	disk_header_id
	sfileptr_chapter:
		.addr	sfiletbl_chapters

	bne	:++
	cpy	#ENG_FILELIST_SIDEB_LEN+1
	bne	:+ ; if (y == ENG_FILELIST_SIDEB_LEN+1) {
		lda	#0
		sta	ppu_ctlr1_b
		sta	PPU_CTLR1
		lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
		jsr	disk_sta_ppu_ctlr0
		jmp	sub_6169
	: ; } else if () {
		lda	#$40
		: cmp	#$24
		beq	:+
	; } else {
		jsr	disk_perror
		jmp	sdisk_load_files_disk_swap
	: ; }

	jsr	nt_init_all
	lda	PPU_SR
	lda	#>(PPU_VRAM_COLOR)
	ldy	#<(PPU_VRAM_COLOR)
	sta	PPU_VRAM_AR
	sta	PPU_VRAM_AR
	lda	#PPU_COLOR_LEV0|PPU_COLOR_PITCH
	: ; for (y = 0; y < PPU_COLOR_PAGE_SIZE*2; y++) {
		sta	PPU_VRAM_IO
		iny
		cpy	#(PPU_COLOR_PAGE_SIZE*2)
		bcc	:-
	; }

	lda	PPU_SR
	lda	#>(PPU_VRAM_BG1+SDISK_ERROR_BG_CLEAROUT)
	sta	PPU_VRAM_AR
	lda	#<(PPU_VRAM_BG1+SDISK_ERROR_BG_CLEAROUT)
	sta	PPU_VRAM_AR
	lda	#SDISK_ERROR_BG_CLEAROUT_TILE
	sta	PPU_VRAM_IO
	sta	PPU_VRAM_IO
	lda	PPU_SR
	lda	#>(PPU_VRAM_BG_CHR+SDISK_ERROR_BG_CHR)
	sta	PPU_VRAM_AR
	lda	#<(PPU_VRAM_BG_CHR+SDISK_ERROR_BG_CHR)
	sta	PPU_VRAM_AR
	ldy	#0
	: ; for (y = 0; y < 0x20; y++) {
		lda	sdisk_fail_bg_chr, y
		sta	PPU_VRAM_IO
		iny
		cpy	#(sdisk_fail_bg_chr_end-sdisk_fail_bg_chr)
		bcc	:-
	; }

	lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
	jsr	disk_sta_ppu_ctlr0
	jsr	nmi_disp_disable

	ldy	#(sdisk_fail_oam_end-sdisk_fail_oam)-1
	: ; for (y = 0x0B; y >= 0; y--) {
		lda	sdisk_fail_oam, y
		sta	oam_buffer, y

		dey
		bpl	:-
	; }

	ldy	#(sdisk_fail_message_end-sdisk_fail_message)-1
	: ; for (y = 0x31; y >= 0; y--) {
		lda	sdisk_fail_message, y
		sta	ppu_displist_data, y

		dey
		bpl	:-
	; }

	jsr	nmi_disp_enable

	lda	#0
	sta	zp_byte_04
	: ; for (zp_byte_04 = 0; zp_byte_04 < 5; zp_byte_04++) {
		lda	#0
		sta	zp_byte_06
		: ; for (zp_byte_06 = 0; zp_byte_06 < 0x0C; zp_byte_06 += 4) {
			lda	#5
			sta	zp_byte_07
			jsr	sdisk_fail_pal_load
			: ; for (zp_byte_07 = 5; zp_byte_07 > 0; zp_byte_07--) {
				jsr	nmi_buffer_transfer
				dec	zp_byte_07
				bne	:-
			; }

			lda	zp_byte_06
			clc
			adc	#4
			sta	zp_byte_06
			cmp	#$0C
			bcc	:--
		; }

		jsr	sdisk_load_files_failwait
		inc	zp_byte_04
		lda	zp_byte_04
		cmp	#5
		bcc	:---
	; }

	jmp	sdisk_load_files_disk_swap
; ----------------------------
sdisk_load_files_failwait:
	lda	#$20
	sta	zp_byte_08
	: ; for (zp_byte_08 = 0x20; zp_byte_08 > 0; zp_byte_08--) {
		jsr	nmi_buffer_transfer
		dec	zp_byte_08
		bne	:-
	; }

	rts
; ----------------------------
sdisk_fail_message:
	.dbyt	PPU_VRAM_BG1+SDISK_ERROR_BG_SORRY
	.byte	(:++)-(:+)
:
	.byte	"SORRY"
:

	.dbyt	PPU_VRAM_BG1+SDISK_ERROR_BG_PLEASE_USE
	.byte	(:++)-(:+)
:
	.byte	"PLEASE USE"
:

	.dbyt	PPU_VRAM_BG1+SDISK_ERROR_BG_WRITER
	.byte	(:++)-(:+)
:
	.byte	"OFFICIAL DISK WRITER SHOP"
:

	.byte	NMI_LIST_END
sdisk_fail_message_end:

sdisk_fail_oam:
	.byte   SDISK_ERROR_OBJ_POS_V, SDISK_ERROR_OBJ_TILE, (obj_attr::priority_high|obj_attr::color0), SDISK_ERROR_OBJ_POS_H_0
	.byte   SDISK_ERROR_OBJ_POS_V, SDISK_ERROR_OBJ_TILE, (obj_attr::priority_high|obj_attr::color1), SDISK_ERROR_OBJ_POS_H_1
	.byte   SDISK_ERROR_OBJ_POS_V, SDISK_ERROR_OBJ_TILE, (obj_attr::priority_high|obj_attr::color2), SDISK_ERROR_OBJ_POS_H_2
sdisk_fail_oam_end:
; ----------------------------
sdisk_fail_pal_load:
	lda	zp_byte_04
	tax
	asl	a
	asl	a
	sta	zp_map_pointer
	asl	a
	adc	zp_map_pointer
	adc	zp_map_pointer+1
	tay
	lda	#>(PPU_VRAM_COLOR)
	sta	ppu_displist_addr_dbyt
	lda	sdisk_fail_palette_row, x
	sta	ppu_displist_addr_dbyt+1
	lda	#PPU_COLOR_ROW_SIZE
	sta	ppu_displist_count

	ldx	#0
	: ; for (x = 0; x <= 4; x++) {
		lda	sdisk_fail_palette, y
		sta	ppu_displist_payload, x
		iny
		inx
		cpx	#PPU_COLOR_ROW_SIZE
		bcc	:-
	; }

	lda	#NMI_LIST_END
	sta	ppu_displist_payload, x
	rts
; ----------------------------
sdisk_fail_palette:
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV0|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV0|PPU_COLOR_RED,    PPU_COLOR_LEV0|PPU_COLOR_RED,    PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV1|PPU_COLOR_RED,    PPU_COLOR_LEV1|PPU_COLOR_RED,    PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV0|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV1|PPU_COLOR_ORANGE, PPU_COLOR_LEV1|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV2|PPU_COLOR_ORANGE, PPU_COLOR_LEV2|PPU_COLOR_ORANGE, PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV0|PPU_COLOR_GREEN,  PPU_COLOR_LEV0|PPU_COLOR_GREEN,  PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV1|PPU_COLOR_GREEN,  PPU_COLOR_LEV1|PPU_COLOR_GREEN,  PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV2|PPU_COLOR_GREEN,  PPU_COLOR_LEV2|PPU_COLOR_GREEN,  PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV0|PPU_COLOR_AZURE,  PPU_COLOR_LEV0|PPU_COLOR_AZURE,  PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV2|PPU_COLOR_AZURE,  PPU_COLOR_LEV2|PPU_COLOR_AZURE,  PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV3|PPU_COLOR_AZURE,  PPU_COLOR_LEV3|PPU_COLOR_AZURE,  PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV0|PPU_COLOR_GREY,   PPU_COLOR_LEV0|PPU_COLOR_GREY,   PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV1|PPU_COLOR_GREY,   PPU_COLOR_LEV1|PPU_COLOR_GREY,   PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte   PPU_COLOR_LEV0|PPU_COLOR_PITCH, PPU_COLOR_LEV3|PPU_COLOR_GREY,   PPU_COLOR_LEV3|PPU_COLOR_GREY,   PPU_COLOR_LEV0|PPU_COLOR_PITCH

sdisk_fail_palette_row:
	.byte   PPU_COLOR_ROW_SIZE*1
	.byte	PPU_COLOR_ROW_SIZE*4
	.byte	PPU_COLOR_ROW_SIZE*5
	.byte	PPU_COLOR_ROW_SIZE*6
	.byte	PPU_COLOR_ROW_SIZE*0

sdisk_fail_bg_chr:
	.incbin	"data/ppu/sdisk_chr.bin"
sdisk_fail_bg_chr_end:
; -----------------------------------------------------------
sdisk_filetbl_load:
	lda	#0
	sta	zp_byte_0E
	lda	#FDS_DISKSET_2ADDR
	jsr	fdsbios_disk_ptr_nocheck

	lda	FDS_STACK_IRQMODE
	pha
	lda	#FDSBIOS_FILETBL_LOAD_TRY_COUNT
	sta	zp_byte_05
	: ; for (zp_byte_05 = 2; sdisk_filetbl_load_try() && zp_byte_05 > 0; zp_byte_05--) {
		jsr	sdisk_filetbl_load_try
		beq	:+
		dec	zp_byte_05
		bne	:-
	: ; }
	pla
	sta	FDS_STACK_IRQMODE

	ldy	zp_byte_0E
	txa
	rts
; ----------------------------
sdisk_filetbl_load_try:
	jsr	fdsbios_diskhead_check
	jsr	fdsbios_filecount_get

	lda	#ENG_SIDEB_FILECOUNT
	sta	zp_byte_06
	: ; for (zp_byte_06 = 0x14; zp_byte_06 > 0; zp_byte_06--) {
		lda	#FDS_FILEHEADER_ID
		jsr	fdsbios_blocktype_check
		jsr	fdsbios_file_match
		jsr	fdsbios_data_load

		dec	zp_byte_06
		bne	:-
	; }

	jmp	fdsbios_load_done
