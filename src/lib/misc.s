.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"modes.i"
.include	"charmap.i"
.include	"tunables.i"

	.export eng_palload_mono
pal_mono:
	.byte	PPU_COLOR_LEV0|PPU_COLOR_PITCH
	.byte	PPU_COLOR_LEV3|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV3|PPU_COLOR_GREY
	.byte	PPU_COLOR_LEV0|PPU_COLOR_PITCH

eng_palload_mono:
	lda	PPU_SR
	lda	#>(PPU_COLOR_PAGE_BG)
	ldy	#<(PPU_COLOR_PAGE_BG)
	sta	PPU_VRAM_AR
	sty	PPU_VRAM_AR
	:
		tya
		and	#PPU_COLOR_INDEX_MASK
		tax
		lda	pal_mono, x
		sta	PPU_VRAM_IO
		iny
		cpy	#PPU_COLOR_PAGE_SIZE+PPU_COLOR_ROW_SIZE
		bcc	:-

	rts
; -----------------------------------------------------------
	.export	book_save_check
book_save_check:
	ldy	obj_id_player
	lda	game_save_state, y
	sta	zp_byte_04
	lda	book_save_bits, x
	bit	zp_byte_04
	rts
; -----------------------------------------------------------
BOOK_PAGE_OFFSET	= $340
BOOK_PAGE_SIZE		= $400

	.export book_page_init, book_page_pause, book_page_text
book_page_init:
	jsr	ppu_scroll_zero
	jsr	nmi_disp_disable
	lda	#nmi_buffer_modes::mode_02
	sta	nmi_buffer_ptr
	jsr	nmi_buffer_transfer
	lda	#(fds_ctlr::irq_no|fds_ctlr::drive_ready|fds_ctlr::mirror_v|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset|fds_ctlr::motor_on)
	sta	FDS_CTLR
	jsr	nt_init_all

	lda	#>(PPU_VRAM_BG1)
	ldx	#$FE
	: ; for (a = PPU_VRAM_BG1; a < PPU_VRAM_BG3; a += PAGE_SIZE) {
		ldy	PPU_SR
		ldy	#<(PPU_VRAM_BG1)
		sta	PPU_VRAM_AR
		sty	PPU_VRAM_AR
		: ; for (y = 0; y < 0x80; y++) {
			stx	PPU_VRAM_IO
			iny
			bpl	:-
		; }

		clc
		adc	#>(BOOK_PAGE_SIZE)
		cmp	#>(PPU_VRAM_BG3)
		bne	:--
	; }

	lda	#>(PPU_VRAM_BG1+BOOK_PAGE_OFFSET)
	: ; for (a = PPU_VRAM_BG1+BOOK_PAGE_OFFSET; a < PPU_VRAM_BG3+BOOK_PAGE_OFFSET; a += PAGE_SIZE) {
		ldy	PPU_SR
		ldy	#<(PPU_VRAM_BG1+BOOK_PAGE_OFFSET)
		sta	PPU_VRAM_AR
		sty	PPU_VRAM_AR
		ldy	#0
		: ; for (y = 0; y < 0x80; y++) {
			stx	PPU_VRAM_IO
			iny
			bpl	:-
		; }

		clc
		adc	#>(BOOK_PAGE_SIZE)
		cmp	#>(PPU_VRAM_BG3+BOOK_PAGE_OFFSET)
		bne	:--
	; }

	rts
; ----------------------------
book_page_pause:
	jsr	main_nmi_enable
	lda	#fds_nmimode::game_vec_0
	sta	FDS_STACK_NMIMODE
	lda	#nmi_buffer_modes::mode_03
	sta	nmi_buffer_ptr
	jmp	nmi_buffer_transfer
; ----------------------------
BOOK_PAGE_NUMBER_TENS		= 0
BOOK_PAGE_NUMBER_ONES		= 1
BOOK_PAGE_LIVES_TENS		= 1
BOOK_PAGE_LIVES_ONES		= 2
BOOK_PAGE_CHAPTER_COURSE	= 8
BOOK_PAGE_CHAPTER_SUB		= 10

book_page_text:
	iny
	tya
	jsr	main_counter_tiles
	sty	book_page_number_start+BOOK_PAGE_NUMBER_TENS
	sta	book_page_number_start+BOOK_PAGE_NUMBER_ONES
	inx
	txa
	ora	#ENG_FONT_TILE_BASE
	sta	book_page_chapter_text_start+BOOK_PAGE_CHAPTER_COURSE
	ldy	player_lives
	dey
	tya
	jsr	main_counter_tiles
	sty	book_page_lives_text_start+BOOK_PAGE_LIVES_TENS
	sta	book_page_lives_text_start+BOOK_PAGE_LIVES_ONES

	ldy	#<((book_page_subchapter_icons_end-book_page_subchapter_icons)-1)
	lda	#' '
	: ; for (y = strlen(book_page_subchapter_icons); y >= 0; y--) {
		sta	book_page_subchapter_icons, y
		dey
		bpl	:-
	; }

	ldy	course_no
	lda	eng_level
	sec
	sbc	tbl_chapterstarts, y
	sta	course_sub
	clc
	adc	#ENG_FONT_TILE_BASE+1
	sta	book_page_chapter_text_start+BOOK_PAGE_CHAPTER_SUB

	lda	tbl_chapterstarts+1, y
	sec
	sbc	tbl_chapterstarts, y
	sta	zp_work+$03
	ldx	#0
	ldy	#0
	: ; for (x = 0; x <= (tbl_chapterstarts[y+1]-tbl_chapterstarts[y]); x++) {
		lda	#ENG_SUBCHAPTER_OTHER_TILE
		cpx	course_sub
		bne	:+ ; if (course_sub == x) {
			lda	#ENG_SUBCHAPTER_CURRENT_TILE
		: ; }
		sta	book_page_subchapter_icons, y

		iny
		iny
		inx
		cpx	zp_work+$03
		bcc	:--
	; }

	lda	#nmi_buffer_modes::mode_06
	sta	nmi_buffer_ptr
	rts
; -----------------------------------------------------------
	.export set_default_nmi
set_default_nmi:
	lda	#fds_nmimode::game_vec_2
	sta	FDS_STACK_NMIMODE
	rts
