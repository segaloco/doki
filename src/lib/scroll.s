.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"tunables.i"

.include	"unk.i"

chapter_bg_ptr		= zp_byte_00
scroll_bound_offset	= zp_byte_01

	.align	$1B0

	.export eng_areainit_v
eng_areainit_v:
	lda	byte_502
	bne	:++++ ; if (!byte_502) {
		lda	#(fds_ctlr::irq_no|fds_ctlr::drive_ready|fds_ctlr::mirror_h|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset|fds_ctlr::motor_on)
		sta	FDS_CTLR

		lda	eng_level_page
		bne	:+ ; if (!eng_level_page) {
			lda	#9
			bne	:++
		: ; } else {
			sec
			sbc	#(2)-1
		: ; }

		ora	#$C0
		sta	eng_map_bg_bound_lo
		sec
		sbc	#($41)-1
		sta	eng_map_bg_bound

		lda	eng_level_page
		clc
		adc	#1
		cmp	#$0A
		bne	:+ ; if ((eng_level_page + 1) == 0x0A) {
			lda	#0
		: ; }
		ora	#$10
		sta	eng_map_bg_bound_hi

		lda	eng_level_page
		ldy	#0
		jsr	eng_scroll_reset
		lda	#$20
		sta	ppu_tilescroll_buffer_addr_lo_left
		lda	#$60
		sta	ppu_tilescroll_buffer_addr_lo_right
		inc	byte_502
		lda	#$E0
		sta	zp_byte_E2
		lda	#$01
		sta	zp_byte_E4
		sta	byte_53A
		lsr	a
		sta	ppu_tilescroll_buffer_addr+1
		ldy	eng_level_page
		jsr	lib_calcpage_y
		sta	pos_y_lo_screen
		sty	pos_y_hi_screen
		jsr	eng_apply_transition
	: ; }

	lda	#0
	sta	zp_byte_06
	lda	#$FF
	sta	byte_505
	lda	#$A0
	sta	byte_507
	jsr	eng_bg_redraw_v
	lda	byte_53A
	bne	:+ ; if (!byte_53A) {
		inc	areainit_done
		lda	#$E8
		sta	zp_byte_E1
		lda	#$C8
		sta	zp_byte_E2
		lda	#0
		sta	byte_502
	: ; }

	rts
; -----------------------------------------------------------
	.export eng_vscroll_do
eng_vscroll_do:
	lda	ppu_scroll_req
	and	#$04
	bne	:++ ; if (!(ppu_scroll_req & 0x04)) {
		lda	ppu_scroll_req
		and	#$07
		bne	:+ ; if (!(ppu_scroll_req & 0x07)) {
			jmp	eng_vscroll_done
		: ; }

		lda	ppu_scroll_req
		ora	#$04
		sta	ppu_scroll_req
		lda	#$12
		sta	byte_504
	: ; }

	lda	ppu_scroll_req
	lsr	a
	lda	ppu_scc_v
	bcc	L6A63 ; if () {
		bne	:+
		lda	eng_map_bg_bound_lo
		and	#ENG_POS_MASK
		cmp	#9
		bne	:+ ; if ((eng_map_bg_bound_lo & 0x0F) == 0x09) {
			jmp	eng_vscroll_done
		: ; }

		lda	#1
		jsr	eng_lock_set
		lda	ppu_scc_v
		sec
		sbc	#(5)-1
		sta	ppu_scc_v

		lda	pos_y_lo_screen
		sec
		sbc	#(5)-1
		sta	pos_y_lo_screen
		bcs	:+
			dec	pos_y_hi_screen
		:

		lda	ppu_scc_v
		cmp	#$FC
		bne	:+ ; if (ppu_scc_v == 0xFC) {
			lda	#$EC
			sta	ppu_scc_v
			lda	ppu_scc_v_b
			eor	#$02
			sta	ppu_scc_v_b
			lsr	a
			sta	ppu_scc_h_b
		: ; }

		lda	ppu_scc_v
		and	#$07
		beq	:+ ; if (ppu_scc_v & 0x07) {
			rts
		: ; }

		ldx	#0
		jsr	eng_tile_rowdec
		inx
		jsr	eng_tile_rowdec

		lda	ppu_scc_v
		and	#ENG_POS_MASK
		bne	:+ ; if (!(ppu_scc_v & 0x0F)) {
			ldx	#0
			jsr	eng_vscroll_rowdec
			ldx	#1
			jsr	eng_vscroll_rowdec
		: ; }

		ldx	#1
		jsr	eng_bgprep_v
		jmp	L6AD0
	L6A63: ; } else {
		bne	:+++ ; if () {
			lda	byte_53F
			sta	eng_scroll_skip_value
			cmp	#9
			bne	:+ ; if (eng_scroll_skip_value == 9) {
				lda	#0
				sta	eng_scroll_skip_value
				beq	:++
			: ; } else {
				inc	eng_scroll_skip_value
			: ; }

			lda	eng_map_bg_bound_hi
			and	#ENG_POS_MASK
			cmp	eng_scroll_skip_value
			bne	:+ ; if ((eng_map_bg_bound_hi & 0xF) == eng_scroll_skip_value) {
				jmp	eng_vscroll_done
			: ; }
		; }

		lda	#1
		jsr	eng_lock_set
		lda	ppu_scc_v
		clc
		adc	#4
		sta	ppu_scc_v

		lda	pos_y_lo_screen
		clc
		adc	#4
		sta	pos_y_lo_screen
		bcc	:+
			inc	pos_y_hi_screen
		:

		lda	ppu_scc_v
		and	#$07
		beq	:+ ; if (!(ppu_scc_v & 0x07)) {
			rts
		: ; }

		lda	ppu_scc_v
		cmp	#$F0
		bne	:+ ; if (ppu_scc_v == 0xF0) {
			lda	#0
			sta	ppu_scc_v
			lda	ppu_scc_v_b
			eor	#$02
			sta	ppu_scc_v_b
			lsr	a
			sta	ppu_scc_h_b
		: ; }

		ldx	#2
		jsr	eng_tile_rowinc
		dex
		jsr	eng_tile_rowinc

		lda	ppu_tilescroll_buffer_addr_lo_right
		and	#$20
		bne	:+ ; if (ppu_tilescroll_buffer_addr_lo_right & 0x20) {
			ldx	#2
			jsr	eng_vscroll_rowinc
			ldx	#1
			jsr	eng_vscroll_rowinc
		: ; }

		ldx	#$02
		jsr	eng_bgprep_v
	L6AD0: ; }

	lda	byte_504
	cmp	#$12
	bne	:+++ ; if (byte_504 == 0x12) {
		lda	#$01
		sta	zp_byte_E4

		lda	ppu_scroll_req
		lsr	a
		bcc	:+ ; if (ppu_scroll_req & 1) {
			ldx	#1
			lda	#0
		beq	:++
		: ; } else {
			ldx	#2
			lda	#$10
		: ; }

		sta	scroll_bound_offset
		jsr	sub_6C74
	: ; }

	jsr	eng_bgcopy_v
	dec	byte_504
	bne	:+ ; if () {
		lda	#0
		jsr	eng_lock_set

	eng_vscroll_done:
		lda	#0
		sta	ppu_scroll_req
	: ; }

	rts

	.byte   $01
; ----------------------------
	.export ppu_scroll_backup
ppu_scroll_backup:
	lda	ppu_scc_v
	sta	ppu_scc_v_backup
	lda	ppu_scc_h
	sta	ppu_scc_h_backup
	lda	ppu_scc_v_b
	sta	ppu_scc_v_b_backup
	lda	ppu_scc_h_b
	sta	ppu_scc_h_b_backup
	lda	pos_y_hi_screen
	sta	pos_y_hi_screen_backup
	lda	pos_y_lo_screen
	sta	pos_y_lo_screen_backup
	lda	pos_x_hi_screen_left
	sta	pos_x_hi_screen_left_backup
	lda	zp_byte_E1
	sta	zp_byte_E1_backup
; ----------------------------
	.export ppu_scroll_zero
ppu_scroll_zero:
	lda	#0
	sta	ppu_scc_v
	sta	ppu_scc_h
	sta	ppu_scc_v_b
	sta	ppu_scc_h_b
	rts
; ----------------------------
	.export ppu_scroll_restore
ppu_scroll_restore:
	lda	ppu_scc_v_backup
	sta	ppu_scc_v
	lda	ppu_scc_h_backup
	sta	ppu_scc_h
	sta	pos_x_lo_screen_left
	lda	ppu_scc_v_b_backup
	sta	ppu_scc_v_b
	lda	ppu_scc_h_b_backup
	sta	ppu_scc_h_b
	lda	pos_x_hi_screen_left_backup
	sta	pos_x_hi_screen_left
	lda	pos_y_hi_screen_backup
	sta	pos_y_hi_screen
	lda	pos_y_lo_screen_backup
	sta	pos_y_lo_screen
	rts
; ----------------------------
	.export eng_bg_unpause_v
eng_bg_unpause_v:
	lda	eng_map_bg_bound_lo
	and	#$10
	beq	:+ ; if (eng_map_bg_bound_lo & 0x10) {
		lda	zp_byte_E1
		sec
		sbc	#(9)-1
		sta	zp_byte_E1
	: ; }

	lda	#1
	sta	zp_byte_E4
	lda	eng_map_bg_bound_lo
	sta	eng_map_bg_bound
	lda	#$10
	sta	scroll_bound_offset
	ldx	#0
	jsr	sub_6C74
	lda	ppu_tilescroll_buffer_addr_lo_left
	sta	ppu_tilescroll_buffer_addr+1
	lda	zp_byte_E1
	sta	zp_byte_E2
	ldx	#1
	jsr	zp_byte_E1_inc
	lda	#$F0
	sta	byte_506
	sta	byte_507
	lda	eng_map_bg_bound_hi
	sta	byte_505
	inc	zp_byte_D5
	lda	#1
	sta	zp_byte_06
	rts
; ----------------------------
	.export eng_bg_redraw_v
eng_bg_redraw_v:
	ldx	#0
	stx	byte_537
	jsr	eng_bgprep_v
	jsr	eng_bgcopy_v
	ldx	#0
	jsr	eng_tile_rowinc

	lda	word_506
	cmp	ppu_tilescroll_buffer_addr
	bne	:+++
	lda	word_506+1
	clc
	adc	#>(PPU_VRAM_BG1)
	cmp	ppu_tilescroll_buffer_addr+1
	bne	:+++
	; while () {
		: ; if () {
			lda	zp_byte_06
			tax
			beq	:+ ; if () {
				lda	zp_byte_E1_backup
				sta	zp_byte_E1
			: ; }

			inc	byte_537
			lda	#0
			sta	byte_53A, x
			sta	byte_53D
			sta	byte_53E
			rts
		: ; }
		lda	ppu_tilescroll_buffer_addr+1
		and	#$20
		bne	L6BEE
		lda	eng_map_bg_bound
		cmp	byte_505
		beq	:---
	; }

	jmp	eng_vscroll_rowinc
; ----------------------------
eng_tile_rowdec:
	lda	ppu_tilescroll_buffer_addr_lo_tbl, x
	sec
	sbc	#PPU_VRAM_COLUMN_COUNT
	sta	ppu_tilescroll_buffer_addr_lo_tbl, x
L6BEE:
	rts
; ----------------------------
eng_tile_rowinc:
	lda	ppu_tilescroll_buffer_addr+1, x
	clc
	adc	#PPU_VRAM_COLUMN_COUNT
	sta	ppu_tilescroll_buffer_addr+1, x
	rts
; ----------------------------
eng_vscroll_rowdec:
	lda	eng_map_bg_bound_parts_tbl, x
	sec
	sbc	#$10
	sta	eng_map_bg_bound_parts_tbl, x
	and	#$F0
	cmp	#$F0
	bne	:++ ; if () {
		lda	eng_map_bg_bound_parts_tbl, x
		and	#ENG_POS_MASK
		clc
		adc	#$E0
		sta	eng_map_bg_bound_parts_tbl, x
		dec	eng_map_bg_bound_parts_tbl, x

		lda	eng_map_bg_bound_parts_tbl, x
		cmp	#$DF
		bne	:+ ; if (eng_map_bg_bound_parts_tbl[x] == 0xDF) {
			lda	#$E9
			sta	eng_map_bg_bound_parts_tbl, x
		: ; }

		lda	#$A0
		sta	ppu_tilescroll_buffer_addr_lo_tbl, x
	: ; }

	rts
; ----------------------------
eng_vscroll_rowinc:
	lda	eng_map_bg_bound_tbl, x
	clc
	adc	#$10
	sta	eng_map_bg_bound_tbl, x
	and	#$F0
	cmp	#$F0
	bne	:++ ; if () {
		lda	eng_map_bg_bound_tbl, x
		and	#ENG_POS_MASK
		sta	eng_map_bg_bound_tbl, x
		inc	eng_map_bg_bound_tbl, x

		lda	eng_map_bg_bound_tbl, x
		cmp	#$0A
		bne	:+ ; if (eng_map_bg_bound_tbl[x] == 0x0A) {
			lda	#0
			sta	eng_map_bg_bound_tbl, x
		: ; }

		lda	#0
		sta	ppu_tilescroll_buffer_addr+1, x
	: ; }

	rts
; ----------------------------
eng_bgprep_v:
	lda	eng_map_bg_bound_tbl, x
	and	#ENG_POS_MASK
	tay
	lda	eng_map_page_decoded_lo, y
	sta	course_data_addr
	lda	eng_map_page_decoded_hi, y
	sta	course_data_addr+1
	lda	eng_map_bg_bound_tbl, x
	and	#$F0
	sta	eng_scroll_data_offset

	lda	eng_map_bg_bound_tbl, x
	lsr	a
	bcc	:+ ; if (eng_map_bg_bound_tbl[x] & 1) {
		lda	#>(PPU_VRAM_BG1)
		bne	:++
	: ; } else {
		lda	#>(PPU_VRAM_BG3)
	: ; }

	sta	ppu_tilescroll_buffer_addr

	lda	eng_map_bg_bound_tbl, x
	and	#$C0
	asl	a
	rol	a
	rol	a
	adc	ppu_tilescroll_buffer_addr
	sta	ppu_tilescroll_buffer_addr
	lda	ppu_tilescroll_buffer_addr+1, x
	sta	ppu_tilescroll_buffer_addr+1

L6C73:
	rts
; ----------------------------
sub_6C74:
	lda	eng_map_bg_bound_tbl, x
	and	#$10
	beq	L6C73

	lda	eng_map_bg_bound_tbl, x
	sta	zp_work+$03
	sec
	sbc	scroll_bound_offset
	sta	eng_map_bg_bound_tbl, x
	jsr	eng_bgprep_v

	lda	#$0F
	sta	zp_byte_E3
	lda	#0
	sta	eng_bgcopy_counter
	: ; while (zp_byte_E3 >= 0) {
		jsr	sub_6E0C
		lda	zp_byte_E3
		bpl	:-
	; }

	lda	zp_work+$03
	sta	eng_map_bg_bound_tbl, x
	dec	zp_byte_E4
	jmp	eng_bgprep_v
; ----------------------------
eng_bgcopy_v:
	ldx	ppu_displist_offset
	lda	ppu_tilescroll_buffer_addr
	sta	ppu_displist_addr_dbyt, x
	inx
	lda	ppu_tilescroll_buffer_addr+1
	sta	ppu_displist_addr_dbyt+1-1, x
	inx
	lda	#PPU_VRAM_COLUMN_COUNT
	sta	ppu_displist_count-2, x
	inx
	lda	#0
	sta	eng_bgcopy_counter
	lda	#$0F
	sta	zp_byte_E3

	lda	zp_byte_D5
	beq	:+
	ldy	eng_scroll_data_offset
	cpy	#$E0
	bne	:+ ; if (zp_byte_D5 && eng_scroll_data_offset == 0xE0) {
		lda	#0
		sta	zp_byte_E4
		inc	byte_539
	: ; }

	: ; while () {
		ldy	eng_scroll_data_offset
		lda	(course_data_addr), y
		sta	byte_51B
		and	#$C0
		asl	a
		rol	a
		rol	a
		tay
		lda	tbl_BFD3, y
		sta	chapter_bg_ptr
		lda	tbl_BFD7, y
		sta	chapter_bg_ptr+1
		ldy	eng_scroll_data_offset
		lda	(course_data_addr), y
		asl	a
		asl	a
		tay

		lda	zp_byte_D5
		beq	:+ ; if (zp_byte_D5) {
			iny
			iny
		: ; }

		: ; while () {
			lda	(chapter_bg_ptr), y
			sta	ppu_displist_data, x
			inc	eng_bgcopy_counter
			inx
			iny
			lda	eng_bgcopy_counter
			lsr	a
			bcs	:-
		; }

		inc	eng_scroll_data_offset

		lda	zp_byte_D5
		beq	:+ ; if (zp_byte_D5) {
			jsr	eng_tile_palset
		: ; }

		lda	eng_bgcopy_counter
		cmp	#$20
		bcc	:----
	; }

	lda	#0
	sta	ppu_displist_data, x
	stx	ppu_displist_offset

	lda	zp_byte_D5
	beq	L6D6B ; if (zp_byte_D5) {
		lda	zp_byte_E4
		beq	:+ ; if () {
			dec	zp_byte_E4
			jmp	:+++
		: lda	ppu_scroll_req
		lsr	a
		bcs	:+ ; } else if () {
			ldx	#1
			jsr	eng_attrcopy_v
			ldx	#1
			jsr	zp_byte_E1_inc
			jmp	:++
		: ; } else {
			ldx	#0
			jsr	eng_attrcopy_v
			ldx	#0
			jsr	zp_byte_E1_dec
		: ; }

		ldx	#0
		lda	ppu_scroll_req
		lsr	a
		bcc	:++ ; if () {
			inx
			lda	eng_map_bg_bound_parts_tbl, x
			and	#$F0
			cmp	#$E0
			beq	:+
			lda	eng_map_bg_bound_parts_tbl, x
			and	#$10
			bne	L6D6B
			: ; if () {
				jsr	zp_byte_E1_dec
				jmp	L6D6B
			: ; }
		; }

		lda	eng_map_bg_bound_parts_tbl, x
		and	#$F0
		cmp	#$E0
		beq	:+
		lda	eng_map_bg_bound_parts_tbl, x
		and	#$10
		beq	L6D6B
		: ; if () {
			jsr	zp_byte_E1_inc
		L6D6B: ; }
	; }

	lda	zp_byte_D5
	eor	#$01
	sta	zp_byte_D5
	rts
; ----------------------------
eng_attrcopy_v:
	ldy	ppu_displist_offset
	lda	ppu_tilescroll_buffer_addr
	ora	#$03
	sta	ppu_displist_addr_dbyt, y
	iny
	lda	zp_byte_E1, x
	sta	ppu_displist_addr_dbyt+1-1, y
	iny
	lda	#8
	sta	ppu_displist_count-2, y
	iny
	ldx	#7
	: ; for (x = 7; x >= 0; x--) {
		lda	byte_539
		beq	:+ ; if () {
			lda	ppu_scroll_attr_buffer, x
			lsr	a
			lsr	a
			lsr	a
			lsr	a
			sta	ppu_scroll_attr_buffer, x
			jmp	:++
		: ; } else {
			lda	ppu_scroll_req
			lsr	a
			bcc	:+ ; if () {
				lda	ppu_scroll_attr_buffer, x
				asl	a
				asl	a
				asl	a
				asl	a
				sta	zp_work+$01
				lda	ppu_scroll_attr_buffer, x
				lsr	a
				lsr	a
				lsr	a
				lsr	a
				ora	zp_work+$01
				sta	ppu_scroll_attr_buffer, x
			: ; }
		; }

		lda	ppu_scroll_attr_buffer, x
		sta	ppu_displist_payload-3, y
		iny
		dex
		bpl	:---
	; }

	lda	#1
	sta	zp_byte_E4
	lsr	a
	sta	byte_539
	sta	ppu_displist_payload-3, y
	sty	ppu_displist_offset
	rts
; ----------------------------
zp_byte_E1_inc:
	lda	zp_byte_E1, x
	clc
	adc	#8
	sta	zp_byte_E1, x
	bcc	:+ ; if () {
		lda	#$C0
		sta	zp_byte_E1, x
	: ; }

	rts
; ----------------------------
zp_byte_E1_dec:
	lda	zp_byte_E1, x
	sec
	sbc	#8
	sta	zp_byte_E1, x
	cmp	#$C0
	bcs	:+ ; if () {
		lda	#$F8
		sta	zp_byte_E1, x
	: ; }

	rts
; ----------------------------
eng_tile_palset:
	lda	zp_byte_E3
	lsr	a
	tay
	lda	ppu_scroll_attr_buffer, y
	lsr	a
	lsr	a
	sta	ppu_scroll_attr_buffer, y
	lda	byte_51B
	and	#$C0
	ora	ppu_scroll_attr_buffer, y
	sta	ppu_scroll_attr_buffer, y
	dec	zp_byte_E3
	rts
; ----------------------------
	ldx	#7
	lda	#0
	: ; for (x = 7; x > 0; x--) {
		sta	ppu_scroll_attr_buffer, x
		dex
		bne	:-
	; }

	rts
; ----------------------------
sub_6E0C:
	ldy	eng_scroll_data_offset
	lda	(course_data_addr), y
	sta	byte_51B
	inc	eng_scroll_data_offset
	jmp	eng_tile_palset
; ----------------------------
	.byte	0, 0
; ----------------------------
	.export eng_areainit_h
eng_areainit_h:
	lda	byte_502
	bne	:++++ ; if (!byte_502) {
		lda	#(fds_ctlr::irq_no|fds_ctlr::drive_ready|fds_ctlr::mirror_v|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset|fds_ctlr::motor_on)
		sta	FDS_CTLR
		jsr	eng_apply_transition
		lda	#0
		sta	ppu_scc_v

		lda	eng_level_page
		bne	:+
		lda	#9
		bne	:++
		: ; if (eng_level_page) {
			sec
			sbc	#1
		: ; }

		ora	#$D0
		sta	eng_map_bg_bound_lo
		sec
		sbc	#$20
		sta	eng_map_bg_bound
		lda	eng_level_page
		clc
		adc	#1
		cmp	#$0A
		bne	:+ ; if () {
			lda	#0
		: ; }

		ora	#$10
		sta	eng_map_bg_bound_hi
		lda	eng_level_page
		ldy	#$01
		jsr	eng_scroll_reset
		inc	byte_502
		lda	eng_level_page
		sta	pos_x_hi_screen_left
		lda	#$01
		sta	byte_53A
		lsr	a
		sta	zp_byte_06
		lda	#$FF
		sta	byte_505
		lda	#$0F
		sta	byte_507
		jsr	sub_6E84
	: ; }

	jsr	sub_70C4
	lda	byte_53A
	bne	:+ ; if (!byte_53A) {
		sta	byte_502
		inc	areainit_done
	: ; }

	rts
; ----------------------------
sub_6E84:
	lda	eng_level_page
	bne	:+ ; if (!eng_level_page) {
		lda	eng_camera_move_x
		bmi	:+++++++
		lda	eng_level_page
	: ; }

	cmp	byte_53F
	bne	:+
	lda	eng_camera_move_x
	bpl	:++++++
	: ; if (eng_level_page == byte_53F && eng_camera_move_x < 0) {
		ldx	#$02
		lda	eng_camera_move_x
		bpl	:+
		lda	#$FF
		sta	zp_addr_0A+1
		bne	:++
		: ; if () {
			lda	#$00
			sta	zp_addr_0A+1
		: ; }

		:
			lda	eng_camera_move_x
			and	#$F0
			clc
			adc	eng_map_bg_bound_tbl, x
			php
			adc	zp_addr_0A+1
			plp
			sta	zp_addr_0C

			lda	zp_addr_0A+1
			bne	L6ECB
			bcc	L6EDC
			lda	eng_map_bg_bound_tbl, x
			and	#ENG_POS_MASK
			cmp	#$09
			bne	L6EDC ; if () {
				lda	zp_addr_0C
				and	#$F0
				jmp	L6EDE
			L6ECB: bcs	L6EDC
			lda	eng_map_bg_bound_tbl, x
			and	#ENG_POS_MASK
			bne	L6EDC ; } else if () {
				lda	zp_addr_0C
				and	#$F0
				adc	#$09
				jmp	L6EDE
			L6EDC: ; } else {
				lda	zp_addr_0C
			L6EDE: ; }

			sta	eng_map_bg_bound_tbl, x
			dex
			bpl	:-

		lda	eng_camera_move_x
		sta	ppu_scc_h
		sta	pos_x_lo_screen_left
		and	#$F0
		sta	eng_map_page_x
		lda	eng_camera_move_x
		bpl	:+ ; if (eng_camera_move_x < 0) {
			dec	pos_x_hi_screen_left
			lda	ppu_scc_h_b
			eor	#$01
			sta	ppu_scc_h_b
			lda	#$01
			sta	byte_507
		: ; }
	: ; }

	lda	#0
	sta	eng_camera_move_x
	rts
; ----------------------------
	.export eng_hscroll_do
eng_hscroll_do:
	lda	#0
	sta	ppu_has_tilescroll
	lda	byte_538
	beq	:++
	lda	byte_538
	lsr	a
	bcs	:+
		ldx	#2
		stx	zp_byte_09
		lda	#$10
		sta	scroll_bound_offset
		dex
		lda	byte_538
		sta	ppu_scroll_req
		jsr	eng_attrcopy_h
		lda	eng_map_bg_bound_b
		sta	eng_map_bg_bound_hi
		lda	#0
		sta	byte_538
		beq	:++
	:
		ldx	#1
		stx	zp_byte_09
		dex
		stx	scroll_bound_offset
		lda	byte_538
		sta	ppu_scroll_req
		jsr	eng_attrcopy_h
		lda	#0
		sta	byte_538
	:

	lda	eng_camera_move_x
	bne	:+
		rts
	:

	lda	eng_camera_move_x
	bpl	:+
	lda	#1
	sta	ppu_scroll_req
	jmp	loc_6FB4
	: ; if () {
		lda	#2
		sta	ppu_scroll_req
		ldx	eng_camera_move_x
		: ; for (x = eng_camera_move_x; x > 0; x--) {
			lda	ppu_scc_h
			bne	:+
			lda	pos_x_hi_screen_left
			cmp	byte_53F
			bne	:+ ; if (pos_x_hi_screen_left == byte_53F) {
				jmp	L7003
			: ; }

			lda	ppu_scc_h
			clc
			adc	#1
			sta	ppu_scc_h
			sta	pos_x_lo_screen_left
			bcc	:+ ; if (++ppu_scc_h > 0xFF) {
				inc	pos_x_hi_screen_left
				lda	ppu_scc_h_b
				eor	#$01
				sta	ppu_scc_h_b
				asl	a
				sta	ppu_scc_v_b
			: ; }

			lda	pos_x_hi_screen_left
			cmp	byte_53F
			beq	:++ ; if (pos_x_hi_screen_left != byte_53F) {
				lda	ppu_scc_h
				and	#$F0
				cmp	eng_map_page_x
				beq	:+ ; if ((ppu_scc_h & 0xF0) != eng_map_page_x) {
					sta	eng_map_page_x
					lda	#1
					sta	ppu_has_tilescroll
				: ; }

				dex
				bne	:----
				; } else break;
		: ; }

		lda	ppu_has_tilescroll
		beq	L7003
		ldx	#2
		: ; for (x = 2; x > 0; x--) {
			jsr	eng_hscroll_colinc
			dex
			bne	:-
		; }

		ldx	#2
		jsr	eng_bgprep_h
		jmp	L7000
	loc_6FB4: ; } else {
		ldx	eng_camera_move_x
		: ; for () {
			lda	ppu_scc_h
			bne	:+
			lda	pos_x_hi_screen_left
			bne	:+ ; if () {
				jmp	L7003
			: ; }

			lda	ppu_scc_h
			sec
			sbc	#1
			sta	ppu_scc_h
			sta	pos_x_lo_screen_left
			bcs	:+ ; if () {
				dec	pos_x_hi_screen_left
				lda	ppu_scc_h_b
				eor	#$01
				sta	ppu_scc_h_b
				asl	a
				sta	ppu_scc_v_b
			: ; }

			lda	ppu_scc_h
			and	#$F0
			cmp	eng_map_page_x
			beq	:+ ; if () {
				sta	eng_map_page_x
				lda	#$01
				sta	ppu_has_tilescroll
			: ; }

			inx
			bne	:----
		; }

		lda	ppu_has_tilescroll
		beq	L7003
		ldx	#2
		: ; for (x = 2; x > 0; x--) {
			jsr	eng_hscroll_coldec
			dex
			bne	:-
		; }

		ldx	#1
		jsr	eng_bgprep_h
	L7000: ; } if (ppu_has_tilescroll) {
		jsr	eng_bgcopy_h
	L7003: ; }

	lda	#0
	sta	ppu_scroll_req
	rts
; ----------------------------
	.export eng_scroll_reset
eng_scroll_reset:
	lsr	a
	bcs	:+
	lda	#$01
	sta	ppu_scc_h_b
	asl	a
	sta	ppu_scc_v_b
	lda	#>PPU_VRAM_BG1
	bne	:++
	: ; if () {
		lda	#0
		sta	ppu_scc_h_b
		sta	ppu_scc_v_b
		lda	eng_scroll_reset_bg_tbl, y
	: ; }

	sta	byte_506
	rts

eng_scroll_reset_bg_tbl:
	.byte	>PPU_VRAM_BG3
	.byte   >PPU_VRAM_BG2
; ----------------------------
eng_level_page_ispace:
	.byte	$0A

	.export eng_bound_ispace, eng_bound_normal
eng_bound_ispace:
	lda	ppu_scc_h
	sta	ppu_scc_h_backup
	lda	ppu_scc_h_b
	sta	ppu_scc_h_b_backup
	lda	pos_x_hi_screen_left
	sta	pos_x_hi_screen_left_backup
	inc	byte_53D
	lda	eng_level_page_ispace
	sta	eng_level_page
	jsr	eng_scroll_reset
	lda	#0
	sta	ppu_scc_h
	sta	pos_x_lo_screen_left
	lda	eng_level_page_ispace
	sta	pos_x_hi_screen_left
	jsr	eng_apply_transition
	lda	eng_level_page_ispace
	sta	eng_map_bg_bound
	lda	#$E0
	sta	byte_506
	lda	eng_level_page_ispace
	clc
	adc	#$F0
	sta	byte_505
	rts

eng_bound_normal:
	lda	ppu_scc_h_backup
	sta	ppu_scc_h
	sta	pos_x_lo_screen_left
	lda	ppu_scc_h_b_backup
	sta	ppu_scc_h_b
	lda	pos_x_hi_screen_left_backup
	sta	pos_x_hi_screen_left

	lda	byte_53D
	bne	:+ ; if (byte_53D) {
		inc	byte_53E
		inc	byte_53D
		inc	zp_byte_D5
		jsr	eng_player_restore
		lda	eng_map_bg_bound_lo
		sta	eng_map_bg_bound
		lda	#$10
		sta	scroll_bound_offset
		lda	#$F0
		sta	byte_506
		sta	byte_507
		lda	eng_map_bg_bound_hi
		sta	byte_505
	: ; }

	rts
; ----------------------------
	.export sub_709F
sub_709F:
	lda	eng_map_bg_bound_lo
	sta	eng_map_bg_bound
	lda	#$10
	sta	scroll_bound_offset
	lda	#$F0
	sta	byte_506
	sta	byte_507

	lda	eng_map_bg_bound_hi
	clc
	adc	#$10
	adc	#0
	cmp	#$0A
	bne	:+ ; if (eng_map_bg_bound_hi + 0x10 == 0x0A) {
		lda	#0
	: ; }
	sta	byte_505

	lda	#1
	sta	zp_byte_06
	rts
; ----------------------------
	.export sub_70C4
sub_70C4:
	ldx	#0
	stx	byte_537
	stx	ppu_has_tilescroll
	stx	ppu_scroll_req
	jsr	eng_bgprep_h
	jsr	eng_bgcopy_h
	lda	word_506
	cmp	ppu_tilescroll_buffer_addr
	bne	L70F4
	lda	word_506+1
	clc
	adc	#1
	cmp	ppu_tilescroll_buffer_addr+1
	bne	L70F4
	L70E5:
		lda	#0
		sta	byte_53A
		sta	byte_53D
		sta	byte_53E
		inc	byte_537
		rts

	L70F4:
		lda	eng_map_bg_bound
		cmp	byte_505
		beq	L70E5

	ldx	#0
	jmp	eng_hscroll_colinc
; ----------------------------
eng_hscroll_coldec:
	lda	eng_map_bg_bound_tbl, x
	sec
	sbc	#PPU_CHR_SIZE
	sta	eng_map_bg_bound_tbl, x
	bcs	:+ ; if ((eng_map_bg_bound -= PPU_CHR_SIZE) < 0) {
		dec	eng_map_bg_bound_tbl, x
		lda	eng_map_bg_bound_tbl, x
		cmp	#<(ENG_HSCROLL_COLDEC_BASE-ENG_HSCROLL_COLWIDTH)
		bne	:+ ; if (!(--eng_map_bg_bound_tbl[x]) == (dec_base-width)) {
			lda	#<ENG_HSCROLL_COLDEC_BASE
			sta	eng_map_bg_bound_tbl, x
		; }
	: ; }

	rts
; ----------------------------
eng_hscroll_colinc:
	lda	eng_map_bg_bound_tbl, x
	clc
	adc	#PPU_CHR_SIZE
	sta	eng_map_bg_bound_tbl, x
	bcc	:+ ; if ((eng_map_bg_bound_tbl[x] += PPU_CHR_SIZE) > 0xFF) {
		inc	eng_map_bg_bound_tbl, x
		lda	eng_map_bg_bound_tbl, x
		cmp	#ENG_HSCROLL_COLINC_BASE+ENG_HSCROLL_COLWIDTH
		bne	:+ ; if (++eng_map_bg_bound_tbl[x] == (inc_base+width)) {
			lda	#<ENG_HSCROLL_COLINC_BASE
			sta	eng_map_bg_bound_tbl, x
		; }
	: ; }

	rts
; ----------------------------
eng_bgprep_h:
	sty	zp_work+$0F
	lda	eng_map_bg_bound_tbl, x
	and	#ENG_POS_MASK
	tay
	lda	eng_map_page_decoded_lo, y
	sta	course_data_addr
	lda	eng_map_page_decoded_hi, y
	sta	course_data_addr+1
	lda	eng_map_bg_bound_tbl, x
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	eng_scroll_data_offset
	asl	a
	sta	ppu_tilescroll_buffer_addr+1
	ldy	#$20
	lda	eng_map_bg_bound_tbl, x
	lsr	a
	bcs	:+
		ldy	#$24
	:
	sty	ppu_tilescroll_buffer_addr

	ldy	zp_work+$0F
	rts
; ----------------------------
sub_7156:
	stx	zp_byte_08
	ldx	zp_byte_09
	ldy	#2
	lda	eng_map_bg_bound_tbl, x
	sta	eng_map_bg_bound_b
	sec
	sbc	scroll_bound_offset
	sta	eng_map_bg_bound_tbl, x
	jsr	eng_bgprep_h
	lda	#7
	sta	zp_byte_E3
	lda	#0
	sta	eng_bgcopy_counter
	: ; for (zp_byte_E3 = 7; zp_byte_E3 >= 0; ???) {
		jsr	sub_723F
		lda	zp_byte_E3
		bpl	:-
	; }

	lda	ppu_tilescroll_buffer_addr+1
	and	#$1C
	lsr	a
	lsr	a
	ora	#$C0
	sta	ppu_bg_attr+1
	lda	ppu_tilescroll_buffer_addr
	ora	#$03
	sta	ppu_bg_attr
	ldx	zp_byte_08
	rts
; ----------------------------
eng_bgcopy_h:
	lda	#$0F
	sta	zp_byte_E3
	lda	#0
	sta	eng_bgcopy_counter
	sta	zp_byte_D5
	tax
	: ; for () {
		ldy	eng_scroll_data_offset
		lda	(course_data_addr), y
		sta	byte_51B
		and	#$C0
		asl	a
		rol	a
		rol	a
		tay
		lda	tbl_BFD3, y
		sta	chapter_bg_ptr
		lda	tbl_BFD7, y
		sta	chapter_bg_ptr+1
		ldy	eng_scroll_data_offset
		lda	(course_data_addr), y
		asl	a
		asl	a
		tay
		lda	zp_byte_D5
		beq	:+ ; if (zp_byte_D5) {
			iny
		: ; }

		lda	(chapter_bg_ptr), y
		sta	ppu_tilescroll_buffer, x
		iny
		lda	(chapter_bg_ptr), y
		sta	ppu_tilescroll_buffer+30, x
		iny
		lda	(chapter_bg_ptr), y
		sta	ppu_tilescroll_buffer+1, x
		iny
		lda	(chapter_bg_ptr), y
		sta	ppu_tilescroll_buffer+30+1, x
		inc	eng_bgcopy_counter
		inx
		inx
		lda	eng_scroll_data_offset
		clc
		adc	#$10
		sta	eng_scroll_data_offset
		lda	eng_bgcopy_counter
		cmp	#$0F
		bcc	:--
	; }

	lda	#0
	sta	ppu_bg_attr
	lda	ppu_scroll_req
	lsr	a
	bcs	:+
	lda	ppu_tilescroll_buffer_addr+1
	and	#$02
	beq	:+++
	lda	ppu_scroll_req
	bne	:++ ; if () {
		lda	#$10
		sta	scroll_bound_offset
		ldx	#0
		stx	zp_byte_09
		inx
		jsr	eng_attrcopy_h
		lda	eng_map_bg_bound_b
		sta	eng_map_bg_bound
		jsr	eng_bgprep_h
		jmp	:+++
	: ; } else if () {
		lda	ppu_tilescroll_buffer_addr+1
		and	#$02
		bne	:++
	: ; } else {
		lda	ppu_scroll_req
		sta	byte_538
	: ; }

	inc	ppu_has_tilescroll
	rts
; ----------------------------
eng_attrcopy_h:
	jsr	sub_7156
	ldx	#7
	stx	zp_byte_0E
	ldy	#0
	: ; for (zp_byte_0E = 7; zp_byte_0E > 3; zp_byte_0E--) {
		ldx	zp_byte_0E
		lda	ppu_scroll_attr_buffer, x
		sta	ppu_hscroll_attr, y
		iny
		dex
		dex
		dex
		dex
		lda	ppu_scroll_attr_buffer, x
		sta	ppu_hscroll_attr, y
		iny
		dec	zp_byte_0E
		lda	zp_byte_0E
		cmp	#3
		bne	:-
	; }

	rts
; ----------------------------
sub_723F:
	sty	zp_work+$0F
	lda	#1
	sta	zp_byte_04
	ldy	eng_scroll_data_offset
	ldx	zp_byte_E3
	: ; for () {
		lda	ppu_scroll_attr_buffer, x
		lsr	a
		lsr	a
		sta	ppu_scroll_attr_buffer, x
		lda	(course_data_addr), y
		and	#$C0
		ora	ppu_scroll_attr_buffer, x
		sta	ppu_scroll_attr_buffer, x
		iny
		lda	ppu_scroll_attr_buffer, x
		lsr	a
		lsr	a
		sta	ppu_scroll_attr_buffer, x
		lda	(course_data_addr), y
		and	#$C0
		ora	ppu_scroll_attr_buffer, x
		sta	ppu_scroll_attr_buffer, x
		lda	eng_scroll_data_offset
		clc
		adc	#16
		tay
		sta	eng_scroll_data_offset

		dec	zp_byte_04
		bpl	:-
	; }

	dec	zp_byte_E3
	ldy	zp_work+$0F
	rts
; ----------------------------
eng_lock_set:
	ldx	#7
	: ; for (x = 7; x >= 0; x--) {
		sta	player_lock, x
		dex
		bpl	:-
	; }

	rts
