.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"modes.i"
.include	"tunables.i"

.include	"fileids.i"

disk_header_save:
	.byte	FILE_ID_SAVE, "SAVE-DAT"
	.addr	save_dat
	.byte	6, 0
	.byte	0
	.addr	save_dat
	.byte	0
; -----------------------------
	.export	disk_loader_file_write_save
disk_loader_file_write_save:
	ldy	#<((save_dat_end-save_dat)-1)
	: ; for (byte of save_dat) {
		lda	game_save_dat, y
		sta	save_dat, y

		dey
		bpl	:-
	; }
	lda	#<ENG_SAVE_MAGIC
	sta	save_magic
	lda	#>ENG_SAVE_MAGIC
	sta	save_magic+1

	ldy	#0
	sty	disk_sideb_current
	sty	ppu_scc_h
	sty	ppu_scc_v

	: ; while (disk_error) {
		lda	#ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1
		sta	PPU_CTLR0

		jsr	nt_init_all
		jsr	eng_palload_mono

		lda	#ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1
		sta	PPU_CTLR0

		jsr	nmi_title_buffer_transfer
		lda	#nmi_title_buffer_modes::wait
		sta	nmi_buffer_ptr
		jsr	nmi_title_buffer_transfer_show

		lda	#ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1
		sta	ppu_ctlr0_b
		sta	PPU_CTLR0

		lda	#FILE_NO_SAVE
		jsr	fdsbios_file_write
		.addr	disk_header_id
		.addr	disk_header_save
		bne	:+ ; if (fdsbios_file_write() == fds_errnos::none) {
			lda	#ppu_ctlr1::objblk_on|ppu_ctlr1::bgblk_on|ppu_ctlr1::objlblk_on|ppu_ctlr1::bglblk_on|ppu_ctlr1::color
			sta	ppu_ctlr1_b
			sta	PPU_CTLR1

			jmp	start_hot
		: ; } else {
			jsr	disk_perror
			jsr	main_disk_swap
			jmp	:--
		; }
	; }
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LCBD3:
	.byte	$80
	.byte	$07
	.byte	$80
	.byte	$F0
	.byte	$A0
	.byte	$F6
	.byte	$02
	.byte	$F1
	.byte	$01
	.byte	$F1
	.byte	$6C
	.byte	$65
	.byte	$11
	.byte	$37
	.byte	$42
	.byte	$30
	.byte	$80
	.byte	$F0
	.byte	$22
	.byte	$F1
	.byte	$CA
	.byte	$F5
	.byte	$0A
	.byte	$00
	.byte	$FF
	.byte	$80
	.byte	$EA
	.byte	$10
	.byte	$10
	.byte	$AC
	.byte	$3C
	.byte	$F0
	.byte	$8C
	.byte	$F1
	.byte	$88
	.byte	$C8
	.byte	$12
	.byte	$F5
	.byte	$0A
	.byte	$10
	.byte	$F1
	.byte	$2A
	.byte	$FF
	.byte	$80
	.byte	$E8
