.include	"mem.i"

	.export lib_calc_page
lib_calc_page:
	lda	eng_level_h
	bne	:+
	ldy	pos_y_hi_player
	lda	pos_y_lo_player
	jsr	lib_calc_page_a_y
	tya
	bpl	:++ ; if (!eng_level_h && ((a = lib_calc_page_a_y(pos_y_lo_player, pos_y_hi_player)) > 0x7F)) {
		lda	#0
		beq	:++
	: ; } else if (eng_level_h) {
		lda	pos_x_hi_player
	: ; }

	sta	eng_page_current
	rts
