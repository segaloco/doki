.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"modes.i"
.include	"tunables.i"

.include	"fileids.i"
; -----------------------------------------------------------
	.export	disk_header_id
	.export	disk_sideb_current
disk_header_id:
disk_header_vendor:	.byte FDS_VENDOR_NINTENDO
disk_header_name:	.byte "DRM"
disk_header_type:	.byte " "
disk_header_ver:	.byte 0
disk_sideb_current:	.byte 1
disk_header_no:		.byte 0
disk_header_disk_type:	.byte 0
disk_header_unk:	.byte 0
; ----------------------------
filetbl_chapters:

filetbl_chapter1:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_1
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_01
filetbl_chapter1_end:
	.assert	(filetbl_chapter1_end-filetbl_chapter1) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_chapter1 filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_chapter2:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_2
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_02
filetbl_chapter2_end:
	.assert	(filetbl_chapter2_end-filetbl_chapter2) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_chapter2 filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_chapter3:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_1
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_03
filetbl_chapter3_end:
	.assert	(filetbl_chapter3_end-filetbl_chapter3) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_chapter3 filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_chapter4:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_3
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_04
filetbl_chapter4_end:
	.assert	(filetbl_chapter4_end-filetbl_chapter4) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_chapter4 filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_chapter5:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_1
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_05
filetbl_chapter5_end:
	.assert	(filetbl_chapter5_end-filetbl_chapter5) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_chapter5 filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_chapter6:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_2
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_06
filetbl_chapter6_end:
	.assert	(filetbl_chapter6_end-filetbl_chapter6) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_chapter6 filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_chapter7:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_4
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_TEMP_PRG
	.byte	FILE_ID_CHAPTER_07
filetbl_chapter7_end:
	.assert	(filetbl_chapter7_end-filetbl_chapter7) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_chapter7 filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_credits:
	.byte	FILE_ID_CHAR_C
	.byte	FILE_ID_BG_CHR_4
	.byte	FILE_ID_BG_CHR_C
	.byte	FILE_ID_ENDING_1
	.byte	FILE_ID_SOUND_DT
filetbl_credits_end:
	.assert	(filetbl_credits_end-filetbl_credits) = (ENG_FILELIST_SIDEB_LEN), error, "filetbl_credits filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_ending:

filetbl_credits_load:
	.byte	FILE_ID_TITLE_C
	.byte	FILE_ID_EN_SND_D
	.byte   FILE_ID_MAIN_PRO
	.byte	FILE_ID_SAVE
filetbl_credits_load_end:
	.assert	(filetbl_credits_load_end-filetbl_credits_load) = (ENG_FILELIST_SIDEA_LEN), error, "filetbl_credits_load filelist length mismatch"
	.byte	FILE_ID_NULL

filetbl_credits2:
	.byte   FILE_ID_TITLE_C
	.byte	FILE_ID_ENDING_C
	.byte	FILE_ID_ENDING_2
	.byte	FILE_ID_EN_SND_D
filetbl_credits2_end:
	.assert	(filetbl_credits2_end-filetbl_credits2) = (ENG_FILELIST_SIDEA_LEN), error, "filetbl_credits2 filelist length mismatch"
	.byte	FILE_ID_NULL

filelist_chapters:
	.addr	filetbl_chapter1
	.addr	filetbl_chapter2
	.addr	filetbl_chapter3
	.addr	filetbl_chapter4
	.addr	filetbl_chapter5
	.addr	filetbl_chapter6
	.addr	filetbl_chapter7
	.addr	filetbl_credits
filelist_chapters_end:
	.assert	((filelist_chapters_end-filelist_chapters)/2) = (ENG_CHAPTER_COUNT + 1), error, "filelist_chapters chapter count mismatch"

filetbls_ending:
	.addr	filetbl_credits_load
	.addr	filetbl_credits2
; ----------------------------
.include	"charmap.i"

disk_text_errorbase:
	.dbyt	PPU_VRAM_BG1+ENG_ERR_HEADER_BG_ADDR
	.byte   disk_text_errorbase_end-disk_text_errorbase_start
disk_text_errorbase_start:
	.byte	"ERR "
	.byte	0, 1
disk_text_errorbase_end:

disk_text_errors:
disk_text_error_disk_set:
	.dbyt	PPU_VRAM_BG1+ENG_ERR_REASON_BG_ADDR
	.byte	(:++)-(:+)
:	.byte	"DISK SET"
:	.assert(((:-)-disk_text_error_disk_set) < ENG_ERR_MSG_SIZE_MAX), error, "disk_text_error_disk_set size mismatch"
	.byte	NMI_LIST_END

disk_text_error_battery:
	.dbyt	PPU_VRAM_BG1+ENG_ERR_REASON_BG_ADDR
	.byte	(:++)-(:+)
:	.byte	"BATTERY"
:	.assert(((:-)-disk_text_error_battery) < ENG_ERR_MSG_SIZE_MAX), error, "disk_text_error_battery size mismatch"
	.byte	NMI_LIST_END

disk_text_error_ab_side:
	.dbyt	PPU_VRAM_BG1+ENG_ERR_REASON_BG_ADDR
	.byte	(:++)-(:+)
:	.byte	"A B SIDE"
:	.assert(((:-)-disk_text_error_ab_side) < ENG_ERR_MSG_SIZE_MAX), error, "disk_text_error_ab_side size mismatch"

disk_text_error_wait:
	.byte	NMI_LIST_END

	.export	disk_text_buffer
disk_text_buffer:
	.dbyt	PPU_VRAM_BG1+ENG_ERR_WAIT_BG_ADDR
	.byte	(:++)-(:+)
:	.byte	"W A I T"
:	.assert(((:-)-disk_text_error_wait) < ENG_ERR_MSG_SIZE_MAX), error, "disk_text_error_wait size mismatch"

disk_text_error_entries:
	.byte	disk_text_error_disk_set-disk_text_errors
	.byte	disk_text_error_disk_set-disk_text_errors
	.byte   disk_text_error_battery-disk_text_errors
	.byte   disk_text_error_wait-disk_text_errors
	.byte   disk_text_error_wait-disk_text_errors
	.byte   disk_text_error_wait-disk_text_errors
	.byte   disk_text_error_wait-disk_text_errors
	.byte   disk_text_error_ab_side-disk_text_errors
	.byte   disk_text_error_wait-disk_text_errors
disk_text_error_entries_end:
; ----------------------------
disk_load_files_disk_swap:
	jsr	main_disk_swap

	.export	disk_load_files_sideb
disk_load_files_sideb:
	ldy	#1
	sty	disk_sideb_current
	jsr	disk_ppu_init
	lda	course_no
	asl	a
	tax
	adc	course_no
	asl	a
	tay
	lda	#FILE_ID_CHAR_C
	clc
	adc	obj_id_player
	sta	filetbl_chapters, y
	lda	filelist_chapters, x
	sta	fileptr_chapter
	lda	filelist_chapters+1, x
	sta	fileptr_chapter+1
	jsr	fdsbios_filetbl_load
		.addr	disk_header_id
	fileptr_chapter:
		.addr	filetbl_chapters

	bne	:++
	cpy	#ENG_FILELIST_SIDEB_LEN
	bne	:+
	disk_load_files_return: ; if (!fdsbios_filetbl_load(disk_header_id, filetbl_chapters) && y == ENG_FILELIST_SIDEB_LEN) {
		lda	#0
		sta	ppu_ctlr1_b
		sta	PPU_CTLR1
		lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
		jmp	disk_sta_ppu_ctlr0
	: ; } if (!fdsbios_filetbl_load(disk_header_id, filetbl_chapters) && y != 5) {
		lda	#fds_errnos::errno_40
	: ; }

	jsr	disk_perror
	jmp	disk_load_files_disk_swap
; ----------------------------
	.export disk_load_files_sidea, disk_load_files_sidea_nocheck
disk_load_files_sidea:
	lda	disk_sideb_current
	beq	:+ ; if (disk_sideb_current) {
		jsr	nmi_disp_disable
		jsr	nt_init_all
		jsr	eng_palload_mono
		jsr	ppu_scroll_zero
		jsr	main_nmi_enable
		jsr	nmi_buffer_transfer
		lda	#nmi_buffer_modes::mode_19
		sta	nmi_buffer_ptr
		jsr	nmi_disp_enable

	disk_load_files_sidea_disk_swap:
		jsr	main_disk_swap
	: ; }

disk_load_files_sidea_nocheck:
	lda	#$80
	sta	apu_music_base_req
	asl	a
	sta	disk_sideb_current
	jsr	disk_ppu_init
	ldx	pos_x_lo_actor+0
	lda	filetbls_ending, x
	sta	fileptr_ending
	lda	filetbls_ending+1, x
	sta	fileptr_ending+1
	jsr	fdsbios_filetbl_load
		.addr	disk_header_id
	fileptr_ending:
		.addr	filetbl_ending

	bne	:++ ; if (!fdsbios_filetbl_load(disk_header_id, filetbl_ending)) {
		cpy	#ENG_FILELIST_SIDEA_LEN
		beq	disk_load_files_return
		ldx	pos_x_lo_actor+0
		bne	:+
		cpy	#(ENG_FILELIST_SIDEA_LEN-1)
		bne	:+ ; if (actor[0].pos_x_lo == 0 && y == 3) {
			ldx	save_magic
			cpx	#<(ENG_SAVE_MAGIC)
			bne	disk_load_files_return
			ldx	save_magic+1
			cpx	#>(ENG_SAVE_MAGIC)
			bne	disk_load_files_return
		: ; }

		lda	#$40
	: ; }

	jsr	disk_perror
	jmp	disk_load_files_sidea_disk_swap
; ----------------------------
	.export disk_ppu_init, disk_sta_ppu_ctlr0
disk_ppu_init:
	jsr	ppu_scroll_zero
	jsr	nt_init_all
	jsr	eng_palload_mono
	jsr	main_nmi_enable
	lda	#nmi_buffer_modes::mode_16
	sta	nmi_buffer_ptr
	jsr	nmi_disp_enable
	lda	#(ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)

disk_sta_ppu_ctlr0:
	sta	ppu_ctlr0_b
	sta	PPU_CTLR0
	rts
; ----------------------------
	.export disk_perror
disk_perror:
	sta	zp_byte_04
	jsr	nt_init_all
	jsr	eng_palload_mono
	lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
	jsr	disk_sta_ppu_ctlr0
	jsr	nmi_disp_disable

	ldy	#(disk_text_errorbase_end-disk_text_errorbase)-1
	: ; for (a of disk_text_errorbase) {
		lda	disk_text_errorbase, y
		sta	ppu_displist_data, y

		dey
		bpl	:-
	; }

	ldy	#$0B
	lda	zp_byte_04
	and	#$0F
	ora	#ENG_FONT_TILE_BASE
	sta	ppu_displist_payload+5
	lda	zp_byte_04
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	ora	#ENG_FONT_TILE_BASE
	sta	ppu_displist_payload+4

	ldy	zp_byte_04
	cpy	#(disk_text_error_entries_end-disk_text_error_entries)-1
	bcc	:+ ; if (zp_byte_04 >= sizeof (disk_text_error_entries) / sizeof (*disk_text_error_entries)) {
		ldy	#(disk_text_error_entries_end-disk_text_error_entries)-1
	: ; }
	ldx	disk_text_error_entries, y
	ldy	#0
	: ; for (a of tbl_7FF8_entry) {
		lda	disk_text_errors, x
		sta	ppu_displist_payload+6, y

		inx
		iny
		cpy	#ENG_ERR_MSG_SIZE_MAX
		bcc	:-
	; }

	jmp	nmi_disp_enable
