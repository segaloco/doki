.include	"system/ppu.i"

.include	"tunables.i"
.include	"charmap.i"

.include	"mem.i"

	.export nt_init_all, oam_init, ppu_nmi_addrsync, ppu_displist_write
nt_init_all:
	lda	#(ppu_ctlr1::objblk_on|ppu_ctlr1::bgblk_on|ppu_ctlr1::objlblk_on|ppu_ctlr1::bglblk_on|ppu_ctlr1::color)
	sta	ppu_ctlr1_b
	sta	PPU_CTLR1
	lda	#>(PPU_VRAM_BG1)
	jsr	nt_init
	lda	#>(PPU_VRAM_BG2)
	jsr	nt_init
	lda	#>(PPU_VRAM_BG3)
	jsr	nt_init
; --------------
oam_init:
	ldy	#0
	lda	#OAM_INIT_SCANLINE
	: ; for (y = 0x100; y > 0; y -= 4) {
		sta	oam_buffer, y
		dey
		dey
		dey
		dey
		bne	:-
	; }

	rts
; --------------
nt_init:
	ldy	PPU_SR
	ldy	#(ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
	sty	PPU_CTLR0

	ldy	#0
	sta	PPU_VRAM_AR
	sty	PPU_VRAM_AR
	ldx	#>(PPU_VRAM_BG_ATTR)
	lda	#' '
	: ; for (nametable_page.in(1, 2, 3)) {
		; for (tile in nametable_page) {
			sta	PPU_VRAM_IO

			iny
			bne	:-
		; }
		dex
		bne	:-
	; }

	: ; for (tile of nametable_page_4) {
		sta	PPU_VRAM_IO

		iny
		cpy	#<(PPU_VRAM_BG_ATTR)
		bne	:-
	; }

	lda	#0
	: ; for (attr of nametable_attrs) {
		sta	PPU_VRAM_IO

		iny
		bne	:-
	; }

ppu_base_done:
	rts

ppu_nmi_addrsync:
	lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
	sta	PPU_CTLR0

	ldy	#0
	: ; for (y = 0; ppu_displist_write_addr[y]; y++) {
		lda	(ppu_displist_write_addr), y
		beq	ppu_base_done

		ldx	PPU_SR
		sta	PPU_VRAM_AR
		iny
		lda	(ppu_displist_write_addr), y
		sta	PPU_VRAM_AR
		iny
		lda	(ppu_displist_write_addr), y
		tax
		: ; for (x = ppu_displist_write_addr[y]; x > 0; x--) {
			iny
			lda	(ppu_displist_write_addr), y
			sta	PPU_VRAM_IO

			dex
			bne	:-
		; }

		iny
		jmp	:--
	; }

ppu_displist_write:
	; for (y; ppu_displist_write_addr[y]; y++) {
		ldy	#0
		lda	(ppu_displist_write_addr), y
		beq	ppu_base_done

		ldx	PPU_SR

		sta	PPU_VRAM_AR
		iny
		lda	(ppu_displist_write_addr), y
		sta	PPU_VRAM_AR

		iny
		lda	(ppu_displist_write_addr), y
		asl	a
		pha

		lda	ppu_ctlr0_b
		ora	#(ppu_ctlr0::int|ppu_ctlr0::inc_32)
		bcs	:+ ; if () {
			and	#<~(ppu_ctlr0::inc_32)
		: ; }
		sta	PPU_CTLR0

		pla
		asl	a
		bcc	:+ ; if ((this.length & DISPLIST_REPEAT)) {
			ora	#1<<1
			iny
		: ; }
		
		lsr	a
		lsr	a
		tax
		: ; for (x; x > 0; x--) {
			bcs	:+ ; if () {
				iny
			: ; }

			lda	(ppu_displist_write_addr), y
			sta	PPU_VRAM_IO

			dex
			bne	:--
		; }
		
		iny
		tya
		clc
		adc	ppu_displist_write_addr
		sta	ppu_displist_write_addr
		lda	ppu_displist_write_addr+1
		adc	#0
		sta	ppu_displist_write_addr+1
		jmp	ppu_displist_write
	; }
; -----------------------------------------------------------
sub_8207:
	.byte	$4C, $C3, $81	; jmp $81C3 ? jumps into the middle of an operation

sub_820A:
        sbc     ppu_scc_v_h_b, x
        tya
        clc
        adc     ppu_displist_write_addr
        sta     ppu_displist_write_addr
        lda     ppu_displist_write_addr+1
        adc     #0
        sta     ppu_displist_write_addr+1
	.byte	$4C, $D4, $81	; jmp $81D4 ? jumps into the middle of an operation
; -----------------------------------------------------------
