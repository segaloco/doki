.include	"mem.i"

tbljmp_tbl_ptr		= zp_addr_0A
tbljmp_target_ptr	= zp_addr_0C

	.export tbljmp
tbljmp:
	asl	a
	tay
	pla
	sta	tbljmp_tbl_ptr
	pla
	sta	tbljmp_tbl_ptr+1
	iny
	lda	(tbljmp_tbl_ptr), y
	sta	tbljmp_target_ptr
	iny
	lda	(tbljmp_tbl_ptr), y
	sta	tbljmp_target_ptr+1
	jmp	(tbljmp_target_ptr)
