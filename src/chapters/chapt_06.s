.include	"mem.i"

.segment	"CHAPT_06DATA"

L02F6		:= $02F6
L301B		:= $301B
L4BF1		:= $4BF1
L5910		:= $5910
L6CFA		:= $6CFA
L6D6B		:= $6D6B
L6D6C		:= $6D6C
L741D		:= $741D
L831A		:= $831A
L8487		:= $8487
L9839		:= $9839
L9855		:= $9855
L988A		:= $988A
L995F		:= $995F
L9967		:= $9967
L9C4C		:= $9C4C
LA116		:= $A116
LA319		:= $A319
LA662		:= $A662
LA93A		:= $A93A
LABB2		:= $ABB2
LAC1F		:= $AC1F
LAC28		:= $AC28
LACCC		:= $ACCC
LAE56		:= $AE56
LAED4		:= $AED4
LAEE0		:= $AEE0
LB01F		:= $B01F
LB168		:= $B168
LB178		:= $B178
LB17D		:= $B17D
LB1D6		:= $B1D6
LB207		:= $B207
LBE01		:= $BE01
LBE0C		:= $BE0C
LBE24		:= $BE24
LF50B		:= $F50B
LF514		:= $F514
	and	($30, x)
	rol	a
	.byte	$0F
	and	($30, x)
	asl	$0F, x
	and	($28, x)
	clc
	.byte	$0F
	and	($17, x)
	.byte	$07
	.byte	$0F
	.byte	$0F
	bmi	LBF3D
	ora	($0F, x)
	bmi	LBF2D
	.byte	$02
	.byte	$0F
	plp
	clc
	php
	.byte	$0F
	.byte	$17
	.byte	$07
	php
	.byte	$0F
	bmi	LBF35
	ora	($0F, x)
	bmi	LBF3D
	.byte	$02
	.byte	$0F
	plp
	clc
	php
	.byte	$0F
LBF2D:	.byte	$27
	.byte	$17
	.byte	$07
	.byte	$07
	bmi	LBF5A
	.byte	$0F
	.byte	$07
LBF35:	bmi	LBF4D
	.byte	$0F
	.byte	$07
	plp
	.byte	$17
	.byte	$0F
	.byte	$07
LBF3D:	and	($01), y
	.byte	$0F
	.byte	$0C
	rol	a
	.byte	$1A
	.byte	$0F
	.byte	$0C
	bmi	LBF5D
	.byte	$0F
	.byte	$0C
	.byte	$17
	.byte	$07
	.byte	$0F
	.byte	$0C
LBF4D:	and	$15
	.byte	$0F
	.byte	$0C
	bmi	LBF6E
	.byte	$0F
	.byte	$0C
	bmi	LBF6D
	.byte	$0F
	.byte	$0C
	.byte	$30
LBF5A:	.byte	$2B
	.byte	$0F
	.byte	$0C
LBF5D:	bmi	LBF9A
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	.byte	$01
LBF6D:	.byte	$0F
LBF6E:	.byte	$0F
	.byte	$0F
	.byte	$FF
LBF71:	bmi	LBF89
	.byte	$0F
	.byte	$FF
	sec
	rol	a
	.byte	$0F
	.byte	$FF
	bmi	LBFA0
	.byte	$0F
	.byte	$FF
	bmi	LBF95
	.byte	$02
	.byte	$FF
	sec
	rol	a
	.byte	$02
	.byte	$FF
	bmi	LBFAC
	.byte	$02
	.byte	$FF
LBF89:	bmi	LBFA1
	.byte	$0F
	.byte	$FF
	bmi	LBF9F
	.byte	$0F
	.byte	$FF
	bmi	LBFB6
	.byte	$0F
	brk
LBF95:	asl	a
	.byte	$14
	.byte	$CF
	bvs	LBF71
LBF9A:	.byte	$FB
	bit	$43
	.byte	$7B
	.byte	$7B
LBF9F:	.byte	$7B
LBFA0:	.byte	$7B
LBFA1:	.byte	$9F
	.byte	$B2
	.byte	$F3
	.byte	$F3
	.byte	$F3
	.byte	$F3
	.byte	$F3
	.byte	$F3
	.byte	$F3
	.byte	$F3
	.byte	$1F
LBFAC:	and	$60A3
	.byte	$3A
	ror	$93, x
	.byte	$93
	.byte	$93
	.byte	$93
	dex
LBFB6:	.byte	$CB
	.byte	$CB
	.byte	$CB
	cpy	LCCCC
	.byte	$CC
	.byte	$CC
LBFBE:	cpy	LCCCC
	cpy	LCCCC
	cpy	LCCCC
	cpy	$CDCC
	cmp	LCECD
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$5C
	ldy	$2C94, x
	cmp	$C5
	dec	$C7
	brk
	sta	$9999, y
	brk
	txa
	txa
	txa
	brk
	.byte	$8B
	.byte	$8B
	.byte	$8B
	brk
	.byte	$9F
	.byte	$9F
	.byte	$9F
	brk
	ldy	$A4
	ldy	$00
	.byte	$D3
LBFF1:	.byte	$9B
	.byte	$12
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
LBFFC:	.byte	$9C
	.byte	$9D
LBFFE:	dec	$00
LC000:	.byte	$93
	.byte	$9F
	brk
	brk
	rti

	.byte	$12
	rti

	brk
	.byte	$93
	sta	a:$C6, x
	rti

LC00D:	.byte	$9D
	.byte	$C6
LC00F:	brk
	brk
LC011:	brk
	brk
LC013:	brk
	brk
LC015:	brk
	brk
	brk
	brk
	brk
	brk
	cpy	#$C0
	cpy	#$27
	.byte	$3B
	.byte	$4F
LC021:	cpy	#$C0
LC023:	cpy	#$31
LC025:	eor	$59
LC027:	.byte	$CF
	.byte	$CF
LC029:	.byte	$CF
	bne	LBFFC
	bne	LBFFE
	bne	LC000
	bne	LBFF1
	nop
	sbc	$0E03, y
	ora	$2626, x
	rol	$26
	bne	LC00D
	bne	LC00F
	bne	LC011
	bne	LC013
	bne	LC015
	rol	a
	.byte	$2F
	.byte	$64
	.byte	$64
	.byte	$64
	.byte	$64
	.byte	$64
	.byte	$64
	.byte	$64
	.byte	$64
	bne	LC021
	bne	LC023
	bne	LC025
	bne	LC027
	bne	LC029
	ror	$8072
	.byte	$9B
	iny
	cmp	$D6D6
	dec	$D6, x
	ora	($43, x)
	.byte	$80
	cpy	#$0C
	.byte	$5F
	sta	($CA), y
	.byte	$12
	.byte	$67
	tya
	.byte	$D2
	.byte	$67
	.byte	$6B
	ora	$20
	ora	LC574, x
	.byte	$C7
	cmp	$C7
	cmp	$C7
	cmp	$C7
	cmp	$C7
	.byte	$57
	iny
	.byte	$57
	iny
	.byte	$57
	iny
	.byte	$57
	iny
	cld
	iny
	ldx	$C8
	ldx	$C8
	sec
	dex
	jsr	L741D
	txa
	iny
	txa
	iny
	txa
	iny
	txa
	iny
	txa
	iny
	txa
	iny
	cpy	LCCBE
	ldx	$BECC, y
	cmp	$D9C7, y
	.byte	$C7
	cmp	$B0C7, y
	cmp	#$B0
	cmp	#$B0
	cmp	#$6D
	cmp	#$AD
	asl	$2005
	ora	$F374, x
	ldx	LC9A9, y
	dec	$C9, x
	.byte	$02
	iny
	rol	$C8, x
	.byte	$02
	iny
	ora	($CA, x)
	ora	($CA, x)
	.byte	$12
	dex
	.byte	$12
	dex
	.byte	$12
	dex
	lsr	$01CA, x
	dex
	ora	($CA, x)
	ora	($CA, x)
	.byte	$AF
	dex
	jmp	LCA01

	txa
	pha
	and	#$F0
	sta	$33
	txa
	asl	a
	asl	a
	asl	a
	asl	a
	sta	$29
	lda	#$0A
	sta	$15
	ldx	#$00
	stx	$12
	stx	$1F
	lda	#$3D
	sta	$90
	lda	#$01
	sta	$51
	tya
	pha
	jsr	LA116
	pla
	sta	$79
	lda	#$FF
	sta	$0441, x
	pla
	tax
	rts

	.byte	$0F
	ora	($13), y
	ora	$21, x
	ora	($13), y
	ora	$17, x
	ora	$171B, y
	ora	$091D, y
	.byte	$0B
	ora	($03, x)
	ora	$07
	eor	$59, x
	.byte	$5B
	eor	$F2F0, x
	.byte	$F4
	inc	$45, x
	eor	$5D5B, y
	eor	$59
	.byte	$5B
	eor	$EAE8, x
	cpx	$ECEE
	inc	$EEEC
	jsr	L9855
	lda	#$40
	sta	$0477, x
	lda	#$02
	sta	$0465, x
	jmp	LA319

LC146:	brk
	.byte	$FF
	inc	$FCFD, x
	.byte	$FB
	.byte	$FA
	sbc	$F9F8, y
	.byte	$FA
	.byte	$FB
	.byte	$FC
	sbc	$FFFE, x
LC156:	.byte	$0B
	.byte	$0C
	ora	$100F
	.byte	$12
	.byte	$14
	.byte	$17
	.byte	$1A
	ora	$201F, x
LC162:	.byte	$E2
	.byte	$E2
	.byte	$E2
	.byte	$E3
	cpx	$E5
	.byte	$E7
	sbc	#$ED
	sbc	($F8), y
	brk
LC16E:	rts

	jsr	LACCC
	ldy	#$00
	lda	$0477, x
	asl	a
	bcc	LC181
	ldy	#$02
	asl	a
	bcc	LC181
	ldy	#$FE
LC181:	sty	$3D, x
	jsr	LB17D
	inc	$0477, x
	lda	$B1, x
	clc
	adc	#$D0
	sta	$B1, x
	bcc	LC195
	inc	$0480, x
LC195:	lda	$EF
	bne	LC16E
	lda	#$49
	sta	$65, x
	lda	#$2F
	ldy	$045C, x
	sty	$07
	beq	LC1A8
	lda	#$33
LC1A8:	ldy	#$30
	sty	a:$F4
	jsr	LAEE0
	lda	#$09
	sta	$65, x
	lda	#$33
	sta	$046E, x
	lda	$29, x
	pha
	sec
	sbc	#$08
	sta	$29, x
	jsr	L9C4C
	ldx	#$37
	lda	$10
	and	#$20
	bne	LC1D4
	lda	#$04
	and	$10
	beq	LC1D4
	ldx	#$3A
LC1D4:	lda	$01
	sec
	sbc	#$08
	sta	$01
	lda	#$20
	sta	$0C
	ldy	#$E0
	jsr	LB01F
	ldx	$12
	lda	$29, x
	sec
	sbc	#$08
	sta	$29, x
	jsr	L9C4C
	pla
	sta	$29, x
	lda	#$13
	sta	$046E, x
	lda	$33, x
	sta	$00
	lda	$0477, x
	and	#$78
	lsr	a
	lsr	a
	lsr	a
	tay
	lda	LC146, y
	adc	$0429
	adc	#$F0
	sta	$01
	ldx	#$3D
	lda	$07
	bne	LC221
	ldx	#$3F
	dey
	dey
	dey
	dey
	cpy	#$07
	bcs	LC221
	ldx	#$41
LC221:	ldy	#$00
	jsr	LB01F
	ldx	$12
	lda	$33, x
	clc
	adc	#$10
	sta	$00
	lda	$0480, x
	and	#$78
	lsr	a
	lsr	a
	lsr	a
	tay
	lda	LC146, y
	adc	$0429
	adc	#$F0
	sta	$01
	lda	#$00
	sta	$0C
	ldx	#$3D
	lda	$07
	bne	LC258
	ldx	#$3F
	dey
	dey
	dey
	dey
	cpy	#$07
	bcs	LC258
	ldx	#$41
LC258:	ldy	#$08
	jsr	LB01F
	ldx	$12
	lda	#$13
	sta	$046E, x
	lda	$EE
	bne	LC296
	lda	$33, x
	clc
	adc	#$10
	sta	$0258
	lda	#$0D
	sta	$0259
	sta	$025D
	lda	#$01
	sta	$025A
	sta	$025E
	lda	$01
	clc
	adc	#$10
	sta	$025B
	lda	$33, x
	sta	$025C
	lda	$0429
	sec
	sbc	#$08
	sta	$025F
LC296:	lda	#$00
	sta	$05
	lda	$0477, x
	jsr	LC2A5
	inc	$05
	lda	$0480, x
LC2A5:	and	#$67
	cmp	#$40
	bne	LC2FC
	lda	$045C, x
	bne	LC2FC
	jsr	LA662
	bmi	LC2FC
	lda	#$01
	sta	byte_602
	ldy	$00
	lda	#$20
	sta	$90, y
	sta	$79, y
	sta	$B1, y
	lda	$29, x
	sbc	#$18
	sta	$29, y
	lda	$05
	beq	LC2DA
	lda	$33, x
	clc
	adc	#$10
	sta	$33, y
LC2DA:	lda	$28
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	and	#$0F
	cmp	#$0B
	bcc	LC2E8
	lda	#$0B
LC2E8:	tax
	lda	LC156, x
	sta	$47, y
	lda	LC162, x
	sta	$3D, y
LC2F5:	tya
	tax
	jsr	L9839
	ldx	$12
LC2FC:	rts

	jsr	L9855
	lda	$33, x
	sec
	sbc	#$08
	sta	$79, x
	rts

	jsr	LACCC
	jsr	LAC28
	jsr	LAC1F
	jsr	LB207
	lda	$0480, x
	bne	LC349
	sta	$3D, x
	jsr	LB168
	lda	$0F
	adc	#$40
	cmp	#$80
	bcs	LC32F
	inc	$0480, x
	lda	#$C0
	sta	$47, x
	bne	LC349
LC32F:	inc	$0477, x
	ldy	#$FC
	lda	$0477, x
	and	#$20
	beq	LC33D
	ldy	#$04
LC33D:	sty	$47, x
	jsr	LB178
	lda	#$61
	sta	$65, x
	jmp	LAE56

LC349:	lda	$47, x
	bmi	LC35C
	lda	$79, x
	sec
	sbc	#$18
	cmp	$33, x
	bcs	LC35C
	sta	$33, x
	lda	#$00
	sta	$47, x
LC35C:	jsr	L995F
	inc	$9F, x
	lda	$9F, x
	pha
	and	#$3F
	bne	LC36B
	jsr	L988A
LC36B:	pla
	bne	LC373
	lda	#$18
	sta	$0453, x
LC373:	lda	$5B, x
	and	#$03
	beq	LC37C
	jsr	LB1D6
LC37C:	lda	#$41
	ldy	$47, x
	bpl	LC384
	lda	#$61
LC384:	jmp	LC409

	jsr	LACCC
	jsr	LAC28
	jsr	LAC1F
	jsr	LB207
	lda	$5B, x
	and	#$08
	beq	LC39E
	lda	$79, x
	sta	$33, x
	rts

LC39E:	jsr	LB168
	iny
	sty	$6F, x
	lda	$B1, x
	bne	LC3C7
	lda	$86, x
	bne	LC3B5
	lda	#$D0
	sta	$47, x
	inc	$B1, x
	jmp	LC404

LC3B5:	ldy	#$FC
	lda	$10
	and	#$20
	beq	LC3BF
	ldy	#$04
LC3BF:	sty	$47, x
	jsr	LB178
	jmp	LC407

LC3C7:	inc	$9F, x
	lda	$47, x
	bmi	LC404
	bne	LC3D4
	lda	#$10
	sta	$0453, x
LC3D4:	lda	$47, x
	bmi	LC3F4
	lda	$5B, x
	and	#$04
	beq	LC3F4
	lda	$0E
	sec
	sbc	#$6D
	cmp	#$06
	bcc	LC3F4
	lda	#$02
	sta	$51, x
	lda	#$E0
	sta	$47, x
	lda	#$40
	sta	byte_602
LC3F4:	lda	$79, x
	cmp	$33, x
	bcs	LC404
	sta	$33, x
	lda	#$00
	sta	$B1, x
	lda	#$A0
	sta	$86, x
LC404:	jsr	L9967
LC407:	lda	#$61
LC409:	sta	$65, x
	lda	$0453, x
	beq	LC41C
	cmp	#$05
	bne	LC417
	jsr	LA93A
LC417:	lda	#$4B
	jmp	LAEE0

LC41C:	jmp	LAE56

	jsr	L9855
	lda	#$03
	sta	$79, x
	rts

LC427:	.byte	$02
	.byte	$04
	ora	$B50E
	adc	$09D0, y
	jsr	LACCC
	jsr	LAC1F
	jsr	LAC28
	lda	$5B, x
	and	#$10
	beq	LC445
	jsr	LC449
	inc	$042F, x
	rts

LC445:	lda	$A8, x
	beq	LC4A4
LC449:	lda	$79, x
	beq	LC4A4
	sta	$0477, x
	lda	#$00
	sta	$79, x
	lda	#$02
	sta	$0489, x
	lda	$0441, x
	sta	$06
	lda	#$FF
	sta	$0441, x
	jsr	LA662
	bmi	LC4A4
	ldy	$00
	lda	#$1A
	sta	$90, y
	jsr	LC2F5
	ldy	$00
	lda	$06
	sta	$0441, y
	lda	$0477, x
	sec
	sbc	#$01
	sta	$79, y
	tay
	lda	LC427, y
	ldy	$00
	sta	$0489, y
	lda	$29, x
	sta	$29, y
	lda	$15, x
	sta	$15, y
	lda	$33, x
	clc
	adc	#$10
	sta	$33, y
	lda	$1F, x
	adc	#$00
	sta	$1F, y
LC4A4:	inc	$9F, x
	lda	$9F, x
	and	#$3F
	bne	LC4AF
	jsr	L988A
LC4AF:	jsr	LB17D
	jmp	LAE56

LC4B5:	brk
LC4B6:	.byte	$01
LC4B7:	brk
LC4B8:	.byte	$FF
	brk
	ora	($00, x)
LC4BC:	brk
	ldy	#$00
	lda	$EE
	bne	LC4CB
	lda	$10
	and	#$18
	lsr	a
	lsr	a
	lsr	a
	tay
LC4CB:	sty	$07
	lda	$0429
	sta	LC4BC
	clc
	adc	LC4B5, y
	sta	$0429
	jsr	LAED4
	lda	$79, x
	sta	$09
	beq	LC522
	tya
	clc
	adc	#$10
	tay
	ldx	$07
	lda	LC4BC
	adc	LC4B6, x
	sta	$01
	ldx	#$57
	jsr	LB01F
	dec	$09
	beq	LC522
	jsr	LABB2
	ldx	$07
	lda	LC4BC
	clc
	adc	LC4B7, x
	sta	$01
	ldx	#$57
	jsr	LB01F
	dec	$09
	beq	LC522
	ldx	$07
	lda	LC4BC
	clc
	adc	LC4B8, x
	sta	$01
	ldx	#$57
	jsr	LB01F
LC522:	ldx	$12
	rts

	lda	#$01
	ldy	$00
	cpy	#$8A
	beq	LC533
	cpy	#$8B
	bne	LC55B
	lda	#$08
LC533:	sta	$46
	lda	$32
	and	#$0F
	tay
	lda	$04EB
	sec
	sbc	#$8A
	cmp	#$02
	bcs	LC551
	cpy	#$0C
	bne	LC54F
	lda	#$00
	sta	$82
	jsr	L8487
LC54F:	ldy	#$04
LC551:	cpy	#$04
	bcs	LC557
	ldy	#$01
LC557:	sty	$04DF
	dex
LC55B:	rts

	inc	$FEFE, x
	inc	$B6B4, x
	lda	$B7, x
	clv
	.byte	$FA
	lda	$FAFA, y
	.byte	$FA
	.byte	$B2
	.byte	$B3
	ldx	LBFBE, y
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
LC574:	lsr	a
	lsr	a
	.byte	$4B
	.byte	$4B
	lsr	$5E5F, x
	.byte	$5F
	tay
LC57D:	tay
	lda	#$A9
	.byte	$A9
LC581:	lda	#$A9
	lda	#$45
LC585:	eor	$A9
	lda	#$A8
LC589:	eor	$A9A9, x
	.byte	$74
LC58D:	ror	$75, x
	.byte	$77
	tya
LC591:	txs
	sta	$9C9B, y
LC595:	txs
	sta	$9C9B, x
LC599:	.byte	$9E
	.byte	$9B
	.byte	$9F
	cli
LC59D:	.byte	$5A
	eor	$5E5B, y
LC5A1:	.byte	$5F
	lsr	$8E5F, x
LC5A5:	.byte	$8F
	.byte	$8F
	stx	$A6A6
	.byte	$A7
	.byte	$A7
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	.byte	$74
	ror	$75, x
	.byte	$77
	bvs	LC628
	adc	($73), y
	adc	($73), y
	adc	($73), y
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	bcs	LC57D
	.byte	$FA
	.byte	$FA
	bcs	LC581
	.byte	$FA
	.byte	$FA
	bcs	LC585
	.byte	$FA
	.byte	$FA
	bcs	LC589
	.byte	$FA
	.byte	$FA
	bcs	LC58D
	.byte	$FA
	.byte	$FA
	bcs	LC591
	.byte	$FA
	.byte	$FA
	bcs	LC595
	.byte	$FA
	.byte	$FA
	bcs	LC599
	.byte	$FA
	.byte	$FA
	bcs	LC59D
	.byte	$FA
	.byte	$FA
	bcs	LC5A1
	.byte	$FA
	.byte	$FA
	bcs	LC5A5
	ldy	#$A2
	lda	($A3, x)
	.byte	$80
	.byte	$82
	sta	($83, x)
	.byte	$F4
	stx	$F5
	.byte	$87
	sty	$86
	sta	$87
	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
	lda	$ACFB
	lda	$ACAC
	ldy	$FBAC
	.byte	$3B
	.byte	$3B
	ldy	$FCFC
	.byte	$FC
	.byte	$FC
	.byte	$F4
	stx	$F5
	.byte	$87
	inc	$FEFE, x
	inc	$FEFE, x
	ora	$3CFE, x
	rol	$3F3D, x
LC628:	cli
	inc	$5A59, x
	.byte	$5B
	.byte	$5A
	inc	$FEFE, x
	eor	$5C5B, x
	inc	$5BFE, x
	.byte	$5A
	.byte	$1C
	inc	$FEFE, x
	inc	$FEFE, x
	inc	$1EFE, x
	inc	$201F, x
	.byte	$22
	and	($23, x)
	.byte	$57
	.byte	$57
	.byte	$FB
	.byte	$FB
	.byte	$57
	.byte	$57
	inc	$ABFE, x
	.byte	$AB
	.byte	$FB
	.byte	$FB
	nop
	nop
	.byte	$FB
	.byte	$FB
	.byte	$7C
	ror	$7F7D, x
	dex
	cpy	LCDCB
	dex
	cpy	LCDCB
	cpy	#$C2
	cmp	($C3, x)
	bit	$2D2E
	.byte	$2F
	stx	$8F8F
	stx	$8A88
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	sty	$888D
	txa
	sty	$888D
	txa
	.byte	$89
	.byte	$8B
	dey
	txa
	.byte	$89
	.byte	$8B
	ror	a
	jmp	(L6D6B)

	jmp	(L6D6C)

	adc	$6E6C
	adc	$946F
	sta	$94, x
	sta	$96, x
	.byte	$97
	stx	$97, y
	pha
	eor	#$48
	eor	#$FE
	inc	$FEFE, x
	.byte	$FB
	.byte	$32
	.byte	$32
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	sbc	$FDFD, x
	sbc	$FB34, x
LC6B2:	sbc	$FB34, x
	bmi	LC6B2
	.byte	$FB
	.byte	$FB
	.byte	$FB
	and	($FB), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	$56, x
	lsr	$56, x
	.byte	$64
	ror	$65
	.byte	$67
	pla
	ror	a
	adc	#$6B
	.byte	$FA
	jmp	(L6CFA)

	adc	$6DFA
	.byte	$FA
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	ldx	$AEAF
	.byte	$AF
	sei
	.byte	$7A
	adc	$A87B, y
	tay
	.byte	$AF
	ldx	$9594
	sty	$95, x
	stx	$97, y
	stx	$97, y
	.byte	$22
	bit	$23
	and	$92
	.byte	$93
	.byte	$93
	.byte	$92
	bvc	LC747
	bvc	LC749
	ldx	$AEAF
	.byte	$AF
	bvc	LC74F
	bvc	LC751
	stx	$8F8F
	stx	$5250
	eor	($53), y
	sbc	$FDFD, x
	sbc	$36FB, x
	rol	$4F, x
	.byte	$4F
	lsr	$4F4E
	lsr	$4F4F
	lsr	$374F
	lsr	$4FFF
	.byte	$3A
	lsr	$4FFF
	lsr	$3837
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	stx	$8F8F
	stx	$4240
	eor	($43, x)
	rti

	.byte	$42
	eor	($43, x)
	tsx
	ldy	$BDBB, x
	tsx
	ldy	$9190, x
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	sbc	$FDFD, x
LC747:	.byte	$FD
	.byte	$61
LC749:	.byte	$63
	adc	($63, x)
	adc	$63
	.byte	$65
LC74F:	.byte	$63
	.byte	$65
LC751:	.byte	$67
	adc	$67
	rts

	.byte	$62
	adc	($63, x)
	.byte	$64
	.byte	$62
	adc	$63
	.byte	$64
	ror	$65
	.byte	$67
	pla
	.byte	$62
	adc	($63, x)
	.byte	$64
	adc	#$65
	.byte	$67
	lsr	$62
	adc	($63, x)
	.byte	$64
	.byte	$47
	adc	$67
	tsx
	ldy	$BDBB, x
	bvs	LC7E8
	adc	($73), y
	stx	$8F8F
	stx	$4544
	eor	$44
LC780:	stx	$92, y
	.byte	$93
	.byte	$0C
	ora	$15, x
	ora	$15, x
	eor	$45
	eor	$45
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$9F
	rti

	.byte	$9C
	.byte	$12
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$80
	.byte	$07
	sta	($80, x)
	sta	($81, x)
	sta	($81, x)
LC7A4:	lda	$050E
	asl	a
	asl	a
	sta	$0F
	lda	$050E
	cmp	#$07
	bcc	LC7B8
	lda	$0543
	jmp	LC7BB

LC7B8:	lda	$0542
LC7BB:	clc
	adc	$0F
	tax
	lda	LC780, x
	sta	($01), y
	rts

	ldy	$E7
LC7C7:	jsr	LC7A4
	jsr	LBE0C
	dec	$050D
	bpl	LC7C7
	rts

LC7D3:	.byte	$4F
	.byte	$4F
	.byte	$83
LC7D6:	bvc	LC829
	.byte	$83
	ldy	$E7
	lda	$050E
	cmp	#$09
	bne	LC7ED
	lda	$04B0
	beq	LC7ED
	.byte	$EE
LC7E8:	asl	$EE05
	.byte	$0E
	.byte	$05
LC7ED:	lda	$050E
	sec
	sbc	#$09
	tax
	lda	LC7D3, x
	sta	($01), y
	jsr	LBE24
	lda	LC7D6, x
	sta	($01), y
	rts

	ldy	$E7
	lda	#$00
	sta	$08
LC808:	lda	($01), y
	cmp	#$40
	bne	LC835
	lda	#$52
	sta	($01), y
	lda	$08
	tax
LC815:	cpx	#$00
	beq	LC822
	iny
	lda	#$54
	sta	($01), y
	dex
	jmp	LC815

LC822:	iny
	lda	#$53
	sta	($01), y
	inc	$08
LC829:	ldy	$E7
	tya
	clc
	adc	#$10
	tay
	sta	$E7
	jmp	LC808

LC835:	rts

	ldy	$E7
	lda	#$52
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	ldy	$E7
	tya
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	dey
	lda	#$54
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	rts

	ldy	$E7
	lda	$050E
	cmp	#$06
	bne	LC867
	lda	#$9E
	sta	($01), y
	jmp	LC86A

LC867:	jsr	LC7A4
LC86A:	jsr	LBE24
	dec	$050D
	bpl	LC867
	rts

	ldy	$E7
LC875:	jsr	LC7A4
	tya
	clc
	adc	#$11
	tay
	dec	$050D
	bpl	LC875
	rts

LC883:	.byte	$67
	ror	a
	.byte	$9C
	cmp	($70), y
	sty	$95, x
	lda	$050E
	tax
	cmp	#$05
	bne	LC898
	lda	$0543
	beq	LC898
	inx
LC898:	ldy	$E7
	lda	LC883, x
	sta	($01), y
	rts

LC8A0:	.byte	$73
	.byte	$0D
LC8A2:	.byte	$74
	.byte	$0E
LC8A4:	adc	$0F, x
	ldy	$E7
	lda	$050E
	sec
	sbc	#$0A
	tax
	lda	LC8A0, x
	sta	($01), y
	dec	$050D
	beq	LC8C6
LC8B9:	jsr	LBE0C
	lda	LC8A2, x
	sta	($01), y
	dec	$050D
	bne	LC8B9
LC8C6:	.byte	$20
LC8C7:	.byte	$0C
	ldx	$A4BD, y
	iny
	sta	($01), y
	rts

LC8CF:	dex
	.byte	$CB
	cpy	LC8C7
	cmp	#$40
	.byte	$04
	ora	$A2
	brk
LC8DA:	stx	$0B
	ldx	$E8
	jsr	LBE01
	ldx	#$05
	ldy	$E7
	ldx	$0B
	ldy	$E7
	lda	$050D
	sta	$07
	jsr	LC91F
	inx
	lda	$07
	beq	LC8FE
LC8F6:	jsr	LBE0C
	jsr	LC91F
	bne	LC8F6
LC8FE:	jsr	LBE0C
	inx
	jsr	LC91F
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LC915
	ldx	#$03
	sta	$E7
	jmp	LC8DA

LC915:	rts

LC916:	.byte	$C7
	iny
	.byte	$C9
LC919:	.byte	$CF
	.byte	$CD
	.byte	$CD
LC91C:	dec	$D0CE
LC91F:	stx	$08
	txa
	bne	LC937
	ldx	#$02
	lda	($01), y
LC928:	cmp	LC916, x
	beq	LC932
	dex
	bpl	LC928
	bmi	LC950
LC932:	lda	LC919, x
	bne	LC966
LC937:	ldx	$08
	cpx	#$02
	bne	LC950
	ldx	#$02
	lda	($01), y
LC941:	cmp	LC916, x
	beq	LC94B
	dex
	bpl	LC941
	bmi	LC950
LC94B:	lda	LC91C, x
	bne	LC966
LC950:	ldx	#$08
LC952:	lda	($01), y
	cmp	LC8CF, x
	beq	LC961
	dex
	bpl	LC952
	ldx	$08
	jmp	LC968

LC961:	ldx	$08
	lda	LC8CF, x
LC966:	sta	($01), y
LC968:	ldx	$08
	dec	$07
	rts

	ldy	$E7
	lda	#$00
	sta	$08
LC973:	lda	($01), y
	cmp	#$40
	beq	LC97A
	rts

LC97A:	lda	#$84
	sta	($01), y
	ldx	$08
	beq	LC994
LC982:	iny
	lda	#$85
	sta	($01), y
	dex
	bne	LC982
	ldx	$08
LC98C:	iny
	lda	#$86
	sta	($01), y
	dex
	bne	LC98C
LC994:	iny
	lda	#$87
	sta	($01), y
	inc	$08
	lda	$E7
	clc
	adc	#$10
	sta	$E7
	sec
	sbc	$08
	tay
	jmp	LC973

LC9A9:	ldy	$E7
	lda	#$03
	sta	($01), y
	rts

	ldy	$E7
	lda	$050E
	cmp	#$0D
	beq	LC9C0
	lda	#$C1
	sta	($01), y
LC9BD:	jsr	LBE24
LC9C0:	lda	($01), y
	cmp	#$40
	bne	LC9D5
	lda	#$C2
	sta	($01), y
	lda	$EC
	beq	LC9D2
	cpy	#$E0
	bcs	LC9D5
LC9D2:	jmp	LC9BD

LC9D5:	rts

	ldy	$E7
	lda	#$C3
	sta	($01), y
LC9DC:	tya
	sec
	sbc	#$10
	tay
	cmp	#$F0
	bcs	LC9F2
	lda	($01), y
	cmp	#$40
	bne	LC9F2
	lda	#$C2
	sta	($01), y
	jmp	LC9DC

LC9F2:	rts

LC9F3:	.byte	$43
	.byte	$44
	eor	$46
	.byte	$47
	pha
	eor	#$4A
	.byte	$4B
	lsr	$414C
	.byte	$5A
	.byte	$42
LCA01:	ldy	$E7
	ldx	$050E
	lda	LC9F3, x
	sta	($01), y
	rts

LCA0C:	.byte	$52
	asl	$C0, x
LCA0F:	.byte	$52
	.byte	$17
	.byte	$82
	ldy	$E7
	lda	$050E
	sec
	sbc	#$08
	tax
	lda	LCA0C, x
	sta	($01), y
LCA20:	jsr	LBE24
	lda	($01), y
	cmp	#$40
	beq	LCA2A
	rts

LCA2A:	lda	$050E
	sec
	sbc	#$08
	tax
	lda	LCA0F, x
	sta	($01), y
	bne	LCA20
LCA38:	lda	$09
	asl	a
	asl	a
	sec
	adc	$09
	sta	$09
	asl	$0A
	lda	#$20
	bit	$0A
	bcs	LCA4D
	bne	LCA4F
	beq	LCA51
LCA4D:	bne	LCA51
LCA4F:	inc	$0A
LCA51:	lda	$0A
	eor	$09
	rts

LCA56:	rti

	dey
	rti

	rti

	rti

	rti

	.byte	$89
	rti

	lda	$E8
	sta	$0D
	lda	#$80
	sta	$0A
	lda	#$31
	sta	$09
LCA6A:	jsr	LCA38
	and	#$07
	tax
	lda	LCA56, x
	sta	($01), y
	jsr	LBE24
	cpy	#$30
	bcc	LCA6A
	tya
	and	#$0F
	tay
	jsr	LBE0C
	lda	$0D
	sta	$E8
	cmp	#$0A
	bne	LCA6A
	lda	#$00
	sta	$E8
	sta	$E6
	sta	$E5
	rts

LCA94:	.byte	$9E
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	lda	($A2, x)
	.byte	$9F
	.byte	$A3
	.byte	$A3
	.byte	$9F
	.byte	$83
	.byte	$83
	.byte	$9F
	.byte	$83
	.byte	$83
	.byte	$9F
	.byte	$83
	.byte	$83
	.byte	$9F
	.byte	$83
	.byte	$83
	.byte	$9F
	ldx	#$00
LCAB1:	ldy	$E7
	lda	#$02
	sta	$09
LCAB7:	lda	LCA94, x
	sta	($01), y
	inx
	iny
	dec	$09
	bpl	LCAB7
	ldy	$E7
	tya
	clc
	adc	#$10
	sta	$E7
	cpx	#$1B
	bne	LCAB1
	rts

	.byte	$80
	sbc	$1890
	brk
	sty	$8C01
	.byte	$02
	sty	$8C03
	jmp	L5910

	.byte	$1A
	sbc	($6D), y
	inc	$01, x
	lda	$2C10
	ora	($64), y
	cmp	($09), y
	cmp	($68), y
	.byte	$2B
	rol	a
	bpl	LCB45
	asl	$2A
	cmp	($F5), y
	.byte	$0F
	rts

	sbc	($CD), y
	inc	$00, x
	ror	$10
	adc	$1019, y
	ora	$1911, y
	.byte	$07
	ora	$8DF1, y
	inc	$02, x
	.byte	$64
	ora	($6F), y
	.byte	$1A
	.byte	$32
	cmp	($07), y
	cmp	($F1), y
	cmp	a:$F6
	pla
	bpl	LCB95
	.byte	$07
	rol	a
	cmp	($F0), y
	eor	$01F6
	.byte	$72
	ora	($14), y
	bpl	LCB74
	.byte	$07
	asl	$2694
	cmp	($F0), y
	ora	a:$F6
	sbc	($EC), y
	inc	$03, x
	lsr	$10, x
	.byte	$43
	ora	$1909, y
	asl	$19, x
	ora	($19), y
	.byte	$07
	ora	$2114, y
	php
	rol	$05
	ora	$190A, y
	.byte	$F0
LCB45:	.byte	$0C
	inc	$00, x
	sty	$10, x
	and	$2F19
	ora	$1917, y
	.byte	$0B
	ora	$897C, y
	ora	$0E89
	.byte	$89
	.byte	$0F
	.byte	$89
	.byte	$44
	bpl	LCB74
	.byte	$54
	ora	#$2D
	.byte	$17
	.byte	$34
	clc
	.byte	$33
	.byte	$17
	.byte	$34
	ora	#$0B
	sbc	$0F, x
	bpl	LCB82
	.byte	$32
	asl	a
	and	($FF), y
	.byte	$89
	nop
	bmi	LCB8D
LCB74:	lsr	$13
	sbc	$0F, x
	ora	#$2D
	sta	($0E, x)
	sta	($0F, x)
	.byte	$80
	.byte	$3B
	sta	($F0, x)
LCB82:	lsr	$75F1
	sbc	($BC), y
	cpy	#$80
	ora	($80, x)
	.byte	$44
	brk
LCB8D:	ora	#$06
	.byte	$0F
	brk
	bpl	LCB9A
	.byte	$02
LCB94:	.byte	$07
LCB95:	.byte	$04
	asl	$05
LCB98:	php
	.byte	$06
LCB9A:	php
	.byte	$07
	.byte	$07
	asl	a
	.byte	$07
	.byte	$0B
	php
	.byte	$0C
	asl	$0D
	.byte	$07
	asl	$0F07
	php
	sbc	$0F, x
	bvc	LCB94
	brk
	bpl	LCBB7
	ora	($07, x)
	.byte	$02
LCBB3:	php
	.byte	$03
	asl	$04
LCBB7:	.byte	$07
	ora	$07
	.byte	$06
LCBBB:	php
	.byte	$07
	php
	sbc	$0F, x
LCBC0:	bmi	LCBB3
	adc	($AB), y
	.byte	$32
	.byte	$1B
	.byte	$32
	.byte	$1B
	.byte	$32
	.byte	$0C
	ora	#$F5
	.byte	$0F
	jsr	L301B
	ora	$F130
	.byte	$4F
	sbc	($CA), y
	.byte	$FF
	.byte	$89
	nop
	jsr	L831A
	.byte	$13
	sbc	$0F, x
	.byte	$13
LCBE0:	beq	LCC33
	.byte	$B7
LCBE3:	.byte	$83
	rol	$81
	and	$32
	.byte	$12
	brk
	.byte	$03
	brk
LCBEC:	.byte	$04
	brk
	ora	$32
	beq	LCBFE
	.byte	$F2
	beq	LCC04
	beq	LCC61
	sbc	$10, x
	brk
	.byte	$FF
	clc
	.byte	$E3
	.byte	$21
LCBFE:	ora	$0FF5, y
	.byte	$12
	.byte	$65
LCC03:	.byte	$31
LCC04:	ora	#$31
	rol	$33, x
	bit	$31
	asl	a
	and	($F0), y
	.byte	$44
	beq	LCC75
	beq	LCB98
	beq	LCBBB
	sbc	($86), y
	sbc	($A8), y
	sbc	($CF), y
LCC1A:	.byte	$F2
	beq	LCC67
	beq	LCBEC
	sbc	($6B), y
	sbc	($A0), y
	.byte	$FF
	clc
	.byte	$E3
	ora	($01, x)
	.byte	$64
	.byte	$32
	ora	#$32
	.byte	$34
	bmi	LCC3A
	bmi	LCC46
	and	$28, x
LCC33:	bit	$F0
	.byte	$44
	beq	LCC9D
	beq	LCBC0
LCC3A:	beq	LCBE3
	.byte	$F1
LCC3D:	ror	$F1
	.byte	$83
	sbc	($A0), y
	.byte	$FF
	clc
	.byte	$E3
	.byte	$21
LCC46:	ora	$0FF5, y
	ora	($65), y
	and	($09), y
	and	($36), y
	.byte	$33
LCC50:	bit	$31
	asl	a
	and	($F0), y
	.byte	$44
	beq	LCCBD
	beq	LCBE0
	beq	LCC03
	sbc	($86), y
	sbc	($A8), y
	.byte	$F1
LCC61:	.byte	$CF
	dec	$82
	.byte	$07
	.byte	$82
	php
LCC67:	.byte	$82
	ora	#$82
	rol	$27
	.byte	$07
	.byte	$22
	php
	.byte	$22
	ora	#$27
	beq	LCCBE
	.byte	$F0
LCC75:	cmp	$6BF1
	sbc	($A0), y
	.byte	$FF
	clc
	.byte	$E3
LCC7D:	ora	($01, x)
	adc	$31
	ora	#$31
	.byte	$33
	and	($0B), y
	and	($17), y
	and	($26), y
	rol	$F5
	.byte	$0F
	.byte	$02
	beq	LCCD4
	beq	LCCF7
	beq	LCC1A
	beq	LCC3D
	sbc	($86), y
	sbc	($A8), y
	sbc	($CF), y
	.byte	$F6
LCC9D:	.byte	$04
	.byte	$FF
	sta	($EA), y
	bpl	LCCBC
	beq	LCCF9
	sbc	($8F), y
	.byte	$F2
	jmp	LF514

	bpl	LCCBD
	beq	LCC7D
	sbc	($EA), y
	.byte	$FF
	sta	($EA), y
	bcc	LCCCE
	brk
	.byte	$1B
	.byte	$93
	.byte	$0B
	sbc	$10, x
LCCBC:	.byte	$01
LCCBD:	.byte	$19
LCCBE:	and	($F0, x)
	sta	($F1, x)
	dey
	sbc	($E0), y
	.byte	$F2
	beq	LCC50
	.byte	$F2
	cmp	#$82
	asl	a
LCCCC:	.byte	$82
	.byte	$0B
LCCCE:	.byte	$82
	.byte	$F3
	.byte	$8F
	stx	$F2
	.byte	$70
LCCD4:	rol	$01
	and	($04, x)
	.byte	$2B
	bpl	LCC61
	ora	($86, x)
	.byte	$04
	stx	$05
	stx	$F3
	.byte	$F3
	jmp	LF50B

	bpl	LCD08
	rti

	and	($10, x)
	sta	$01
	sta	$F0
	.byte	$8B
	sbc	($8A), y
	.byte	$FF
	sta	($EA), y
	.byte	$20
	.byte	$1A
LCCF7:	.byte	$43
	.byte	$13
LCCF9:	sbc	$10, x
	ora	$4EF0, y
	.byte	$F2
	ror	a
	brk
	clc
	.byte	$32
	asl	$3431
	and	($28), y
LCD08:	brk
	ora	#$44
	beq	LCC9D
	tax
	.byte	$82
	.byte	$0B
	.byte	$82
	.byte	$0C
	.byte	$82
	ora	$F582
	ora	($00), y
	beq	LCD28
	sbc	($0F), y
	sbc	($CA), y
	.byte	$FF
	.byte	$89
	nop
	brk
	ora	($07, x)
	lda	#$F5
	ora	($11), y
LCD28:	beq	LCD7A
	sbc	($CA), y
	.byte	$FF
	.byte	$80
	cpx	a:$40
	brk
	.byte	$89
	ora	($89, x)
	asl	a
	.byte	$89
	.byte	$0B
	.byte	$89
	.byte	$0C
	.byte	$89
	ora	$0E89
	.byte	$89
LCD3F:	.byte	$0F
	.byte	$89
	adc	$0A, x
	sbc	$11, x
	.byte	$43
	bit	$42
	sbc	($0C), y
	inc	$01, x
	rts

	.byte	$89
	ora	($89, x)
	.byte	$02
	.byte	$89
	.byte	$03
	.byte	$89
	plp
	bpl	LCD71
	ora	($3E), y
	ora	$A628, y
	.byte	$0C
	ora	$190F, y
	sbc	$11, x
	brk
	.byte	$1A
	rol	$18
	ora	$38
	ora	$F0
	cpy	a:$F6
	pha
	ora	($1D), y
	.byte	$11
LCD71:	.byte	$14
	bpl	LCDC8
	cmp	($08), y
	cmp	($0D), y
	.byte	$19
	.byte	$F0
LCD7A:	jmp	L02F6

	txa
	bpl	LCD94
	bpl	LCD8F
	ora	($50), y
	.byte	$2B
	bpl	LCDA0
	.byte	$04
	cmp	($0B), y
	cmp	($0F), y
	rol	a
	sbc	($CC), y
LCD8F:	inc	$00, x
LCD91:	.byte	$6B
	.byte	$89
	.byte	$0C
LCD94:	.byte	$89
	ora	$0E89
	.byte	$89
	.byte	$0F
	.byte	$89
	asl	a
	.byte	$97
	ora	$F51F, y
LCDA0:	ora	($20), y
	.byte	$FF
	sta	($EA), y
	.byte	$72
	ora	$1865, y
	asl	$13
	sbc	$11, x
	.byte	$14
	beq	LCD3F
	cpy	$A4
	.byte	$13
	and	#$05
	and	#$2D
	lda	($34, x)
	.byte	$04
LCDBA:	.byte	$0F
	.byte	$04
	.byte	$1A
	and	$391A, y
	.byte	$1A
	and	$5BF1, y
	sta	$A1, x
	asl	a
	.byte	$A1
LCDC8:	.byte	$3C
	.byte	$04
	clc
LCDCB:	rol	$18, x
LCDCD:	rol	$18, x
	rol	$02, x
	and	#$0E
	and	#$F0
	sta	($F1), y
	.byte	$1B
	sbc	($F1), y
	eor	$3683, x
	and	$07
	and	$12
	and	($12), y
	and	($12), y
	and	($10), y
	eor	($02, x)
LCDE9:	and	($04), y
	.byte	$83
	.byte	$12
	and	($07), y
	and	#$12
	and	($05), y
	.byte	$3A
	.byte	$12
	and	($05), y
	.byte	$3A
	beq	LCE56
	beq	LCD91
	bcc	LCE35
	bpl	LCE37
	bpl	LCE39
	bpl	LCE3B
	php
	.byte	$83
	ora	#$83
	.byte	$0F
	.byte	$83
	bpl	LCE43
	bpl	LCE45
	.byte	$1A
	.byte	$54
	brk
	.byte	$37
	beq	LCE30
	sbc	($15), y
	.byte	$34
	dey
	ora	$5088
	and	$02
	and	$06
	rol	$0A
	and	$0C
	and	$0E
	and	$38
	.byte	$80
	ora	$0A83, y
	.byte	$83
	.byte	$32
	and	$0F
	.byte	$25
LCE30:	beq	LCDBA
	beq	LCDE9
	.byte	$F1
LCE35:	.byte	$1C
	.byte	$F1
LCE37:	and	$F1, x
LCE39:	tay
	.byte	$F1
LCE3B:	cmp	$7D, x
	tay
	.byte	$17
	and	$1D
	ora	$12
LCE43:	sta	($23, x)
LCE45:	.byte	$83
	and	($25), y
	brk
	and	$5204
	php
	.byte	$52
	beq	LCEAC
	beq	LCEC7
	sbc	($BC), y
	sbc	($D1), y
LCE56:	clv
	.byte	$12
	sbc	($0C), y
	sbc	($2A), y
	sbc	$11, x
	and	$FF, y
	.byte	$F3
	bcc	LCE6C
	.byte	$34
	sbc	($48, x)
	sbc	($67, x)
	.byte	$12
	sbc	$11, x
LCE6C:	rti

	.byte	$32
	sbc	($09, x)
	cpx	$54
	.byte	$12
	clc
	.byte	$12
	ora	#$12
	asl	a
	.byte	$12
	.byte	$0B
	.byte	$12
	.byte	$0C
	.byte	$12
	sty	$E1
	.byte	$3A
	.byte	$E2
	.byte	$22
	.byte	$12
	ora	$1612, x
	sbc	($54, x)
	.byte	$12
	.byte	$3C
	.byte	$12
	.byte	$2B
	.byte	$12
	rol	$12
	asl	a
	.byte	$12
	.byte	$44
	.byte	$E7
	.byte	$34
	.byte	$12
	ora	$12
	asl	$12, x
	.byte	$27
	.byte	$12
	php
	.byte	$12
	.byte	$0C
	sbc	($24, x)
	brk
	asl	a
	brk
	.byte	$13
	sbc	($0A, x)
	.byte	$E2
	eor	$E4, x
	rol	$E2, x
	.byte	$21
LCEAC:	sbc	($4A, x)
	.byte	$12
	ora	$3412
	.byte	$12
	asl	$E2, x
	.byte	$52
	.byte	$12
	.byte	$04
	sbc	($40, x)
	.byte	$EB
	.byte	$0C
	.byte	$03
	ora	$51E2
	nop
	.byte	$0C
	.byte	$03
	ora	$32E1
	.byte	$12
LCEC7:	.byte	$03
	.byte	$12
	.byte	$04
	.byte	$12
	ora	$12
LCECD:	php
	.byte	$12
	ora	#$12
	asl	a
	.byte	$12
	bit	$1712
	.byte	$12
	.byte	$87
	sbc	($11, x)
	.byte	$12
	ora	$3212
	.byte	$E2
	ora	#$E2
	rol	$E1, x
	clc
	.byte	$12
	eor	$55E1, y
	.byte	$12
	lsr	$12
	and	#$12
	beq	LCF23
	.byte	$F2
	.byte	$07
	.byte	$0C
	sbc	$11, x
	.byte	$27
	beq	LCF0F
	beq	LCF50
	.byte	$F4
	.byte	$44
	ora	$0D48
	.byte	$92
	ora	$0D0D
	.byte	$F2
	lsr	a
	ora	$0D0B
	.byte	$0C
	ora	$0D46
	.byte	$B3
	.byte	$0C
	.byte	$BD
	.byte	$0D
LCF0F:	.byte	$34
	ora	$0D0A
	ldx	#$0D
	ldy	$6C0C, x
	ora	$0D51
	ora	$B50D
	.byte	$0C
	ora	$0A0C, y
	.byte	$0C
LCF23:	.byte	$0B
	.byte	$0C
	stx	$0D, y
	dec	$2D86
	sty	$2B
	.byte	$82
	.byte	$0C
	.byte	$82
	.byte	$59
LCF30:	sty	$8A26
	php
	txa
	.byte	$97
	.byte	$03
	.byte	$17
	.byte	$03
	.byte	$FF
	.byte	$80
	inx
	bmi	LCF3E
LCF3E:	bit	$10
	plp
	.byte	$0C
	sbc	$11, x
	bmi	LCF54
	ora	($90), y
	.byte	$E7
	php
	.byte	$03
	ora	#$EF
	bvc	LCF5F
	plp
LCF50:	ora	($6C), y
	.byte	$3F
	.byte	$1A
LCF54:	rol	$3F1E, x
	rti

	bpl	LCF7C
	ora	($07), y
	.byte	$0F
	.byte	$59
	.byte	$0B
LCF5F:	sbc	$11, x
	bvc	LCF8F
	.byte	$37
	ora	$1E3C, y
	.byte	$33
LCF68:	.byte	$13
	and	$0A3A, y
	sbc	$11, x
	bpl	LCF88
	ora	($19), y
	.byte	$E2
	.byte	$12
	.byte	$10
LCF75:	.byte	$FF
	ldy	#$EA
	.byte	$13
	.byte	$22
	stx	$13
LCF7C:	sbc	$11, x
	.byte	$42
	beq	LCF30
	.byte	$C3
	.byte	$32
	.byte	$07
	rol	$46, x
	brk
	.byte	$F0
LCF88:	ora	($F0), y
	jmp	(L4BF1)

	sbc	($CA), y
LCF8F:	sbc	$11, x
	rts

	.byte	$FF
	tax
	sed
	.byte	$13
	rol	a
	beq	LCFE9
	beq	LCF68
	sbc	($0C), y
	sbc	($4F), y
	sbc	($8E), y
	.byte	$F2
	.byte	$8B
	sta	($15, x)
	.byte	$32
	.byte	$32
	brk
	.byte	$03
	brk
	.byte	$04
	brk
	ora	$00
	asl	$00
	.byte	$07
	brk
	php
	brk
	beq	LD006
	sbc	($37), y
	sbc	($6F), y
	sbc	($97), y
	sbc	($CA), y
	.byte	$FF
	.byte	$03
	rti

	lsr	a
	.byte	$07
	ora	$031D, y
	.byte	$AB
	.byte	$19
LCFC8:	cmp	$1803, x
	.byte	$5A
	.byte	$03
	.byte	$1A
LCFCE:	eor	#$07
	ora	$191D, y
	eor	$7B03, x
	.byte	$03
	clc
	nop
	.byte	$07
	clc
	.byte	$1A
	ora	$19AD, y
	lda	$1A03, x
	inc	$05
	.byte	$1A
	rol	$22, x
	.byte	$99
	.byte	$01
LCFE9:	.byte	$01
LCFEA:	ora	($07, x)
	clc
	.byte	$4B
	clc
	txs
LCFF0:	ora	$FB, x
	ora	$18
	.byte	$2B
	clc
	.byte	$7B
	ora	($01, x)
	ora	($05, x)
	.byte	$1C
	iny
	.byte	$42
	iny
	.byte	$03
	eor	($27, x)
	ora	($03, x)
	ora	($5A, x)
LD006:	.byte	$07
	ora	($41, x)
	ora	($B4, x)
	.byte	$3B
	adc	#$01
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	$01
	rol	a
	ora	($DA, x)
	.byte	$03
	ora	($5A, x)
	ora	$01
	eor	($01, x)
	ldy	$01, x
	.byte	$03
	ora	($B8, x)
	ora	($03, x)
	rti

	rol	a
	ora	($01, x)
	ora	$0B
	.byte	$0C
	.byte	$0B
	rol	$07
	.byte	$0C
	.byte	$AB
	.byte	$0C
	cmp	#$0C
	.byte	$E7
	.byte	$03
	.byte	$23
	.byte	$AB
	.byte	$07
	bpl	LD096
	bpl	LCFC8
	bpl	LCFEA
	ora	$0C
	tya
	.byte	$0C
	iny
	.byte	$03
	bpl	LCFF0
	.byte	$07
	.byte	$0C
	.byte	$63
	.byte	$0C
	adc	$0C
	.byte	$67
	.byte	$07
	.byte	$0C
	adc	$0C
	.byte	$93
	.byte	$0C
	cpy	$03
	.byte	$0C
	.byte	$EB
	ora	$0C
	and	#$0C
	lda	#$01
	ora	($05, x)
	.byte	$1C
	dex
	.byte	$42
	.byte	$CB
	.byte	$03
	eor	($D4, x)
	ora	($03, x)
	rti

	rol	a
	ora	($01, x)
	ora	($03, x)
	.byte	$03
	tya
	.byte	$07
	.byte	$03
	pha
	.byte	$1A
	ror	$19
	tax
	ora	($01, x)
	ora	($05, x)
	asl	$49, x
	asl	$F9, x
	ora	$09
	lda	#$16
	cmp	#$03
	ora	$07F5
	ora	$0D27
	sec
	ora	$0156
LD096:	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	#$14
	sta	$14, x
	ldy	$14
	.byte	$B3
	.byte	$14
	.byte	$C2
	.byte	$07
	.byte	$14
	.byte	$A7
	.byte	$14
	tsx
	.byte	$14
	iny
	.byte	$03
	.byte	$14
	.byte	$34
	.byte	$07
	.byte	$14
	lsr	a
	asl	$75
	asl	$7E
	.byte	$03
	ora	($28, x)
	.byte	$07
	.byte	$14
	.byte	$9C
	.byte	$14
	lda	$BE14
	.byte	$03
	.byte	$14
	.byte	$5C
	.byte	$03
	.byte	$14
	.byte	$93
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	eor	($97, x)
	.byte	$1C
	.byte	$B3
	.byte	$42
	ldy	$01, x
	ora	($03, x)
	.byte	$5F
	lda	$01, x
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	php
	dey
	dey
	dey
	clv
	.byte	$80
	brk
	rti

	.byte	$9C
