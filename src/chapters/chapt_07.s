.include	"mem.i"

.segment	"CHAPT_07DATA"

L06E5		:= $06E5
L0837		:= $0837
L4FC4		:= $4FC4
L56F1		:= $56F1
L6CFA		:= $6CFA
L8000		:= $8000
L8113		:= $8113
L820D		:= $820D
L92B7		:= $92B7
L9839		:= $9839
L9855		:= $9855
LA109		:= $A109
LA116		:= $A116
LA1BD		:= $A1BD
LA4B6		:= $A4B6
LA551		:= $A551
LA65E		:= $A65E
LA662		:= $A662
LA78C		:= $A78C
LA956		:= $A956
LABB2		:= $ABB2
LAE56		:= $AE56
LAEE0		:= $AEE0
LAF80		:= $AF80
LB168		:= $B168
LB178		:= $B178
LB17D		:= $B17D
LBE01		:= $BE01
LBE0C		:= $BE0C
LBE24		:= $BE24
	and	($30, x)
	.byte	$12
	.byte	$0F
	and	($30, x)
	asl	$0F, x
	and	($27, x)
	.byte	$17
	.byte	$0F
	and	($29, x)
	.byte	$1A
	.byte	$0F
	.byte	$0F
	bmi	LBF25
	ora	($0F, x)
	bmi	LBF2D
	.byte	$02
	.byte	$0F
	.byte	$27
	.byte	$17
	php
	.byte	$0F
	and	#$1A
	asl	a
	.byte	$0F
	bit	$0C1C
	.byte	$0F
LBF25:	bmi	LBF3D
	.byte	$02
	.byte	$0F
	.byte	$27
	.byte	$17
	php
	.byte	$0F
LBF2D:	rol	a
	.byte	$1A
	asl	a
	.byte	$07
	bmi	LBF49
	.byte	$0F
	.byte	$07
	bmi	LBF4D
	.byte	$0F
	.byte	$07
	.byte	$27
	.byte	$17
	.byte	$0F
	.byte	$07
LBF3D:	and	($01), y
	.byte	$0F
	.byte	$0F
	.byte	$3C
	bit	$0F0C
	bmi	LBF5D
	.byte	$02
	.byte	$0F
LBF49:	plp
	clc
	php
	.byte	$0F
LBF4D:	and	$15
	ora	$0C
	bmi	LBF5B
	.byte	$0F
	.byte	$0C
	bmi	LBF6D
	.byte	$0F
	.byte	$0C
	sec
	clc
LBF5B:	.byte	$0F
	.byte	$0C
LBF5D:	plp
	php
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	.byte	$01
LBF6D:	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$FF
	bmi	LBF89
	.byte	$0F
	.byte	$FF
	sec
	bpl	LBF87
	.byte	$FF
	bmi	LBFA0
	.byte	$0F
	.byte	$FF
	bmi	LBF95
	.byte	$02
	.byte	$FF
	sec
	bpl	LBF86
	.byte	$FF
	.byte	$30
LBF86:	.byte	$25
LBF87:	.byte	$02
	.byte	$FF
LBF89:	bmi	LBFA1
	.byte	$0F
	.byte	$FF
	bmi	LBF9F
	.byte	$0F
	.byte	$FF
	bmi	LBFBD
	.byte	$0F
	brk
LBF95:	asl	a
	.byte	$14
	asl	$1C
	sty	$E1, x
	eor	#$6B
	.byte	$6B
	.byte	$6B
LBF9F:	.byte	$6B
LBFA0:	.byte	$6B
LBFA1:	sta	$29C7
	dec	$8D16, x
	iny
	eor	($9A), y
	.byte	$1F
	ror	$6E6E
	ror	$6E6E
	ror	$6E6E
	ror	LCACA
	dex
	dex
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
LBFBD:	.byte	$CB
LBFBE:	.byte	$CB
	.byte	$CB
	.byte	$CB
	cpy	LCDCC
	cmp	LCECD
	dec	LCFCF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	brk
	cli
	bit	$C0
	dec	$C6
	.byte	$C7
	.byte	$C7
	brk
	.byte	$9B
	.byte	$9B
	.byte	$9B
	brk
	.byte	$D3
	.byte	$9B
	.byte	$12
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$9B
	.byte	$9B
	.byte	$9B
	brk
	ora	$9F
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$9B
	brk
	brk
	brk
	.byte	$C2
	brk
	brk
LC010:	brk
	.byte	$9F
LC012:	brk
	brk
LC014:	brk
	brk
	brk
	brk
	brk
	brk
	brk
	cpy	#$C0
LC01D:	cpy	#$27
	.byte	$3B
	.byte	$4F
	.byte	$C0
LC022:	cpy	#$C0
LC024:	and	($45), y
LC026:	.byte	$59
	.byte	$CF
LC028:	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	ror	$9E72
	.byte	$B3
	iny
	cmp	($D1), y
	.byte	$D1
LC039:	cmp	($D1), y
	.byte	$CF
	.byte	$CF
	.byte	$CF
	bne	LC010
	bne	LC012
	bne	LC014
	bne	LC01D
	.byte	$E3
	.byte	$FF
	asl	$5C37, x
	.byte	$64
	.byte	$8F
	ldx	LD0D4
	bne	LC022
	bne	LC024
	bne	LC026
	bne	LC028
	bne	LC039
	cpx	#$E1
	.byte	$E2
	.byte	$E3
	.byte	$E3
	.byte	$E3
	.byte	$E3
	.byte	$E3
	.byte	$E3
	ora	($43, x)
	.byte	$80
	cpy	#$0C
	.byte	$5F
	sta	($CA), y
	.byte	$12
	.byte	$67
	tya
	.byte	$D2
	.byte	$67
	.byte	$6B
	asl	$20
	ora	$5974, x
	iny
	eor	$59C8, y
	iny
	eor	$59C8, y
	iny
	.byte	$DB
	iny
	.byte	$DB
	iny
	.byte	$DB
	iny
	.byte	$DB
	iny
	eor	$30C9, y
	cmp	#$30
	cmp	#$F5
	.byte	$C9
LC08F:	.byte	$20
	.byte	$1D
LC091:	.byte	$74
	.byte	$0E
LC093:	cmp	#$0E
	cmp	#$0E
	cmp	#$0E
	cmp	#$0E
	cmp	#$0E
	cmp	#$0E
	cmp	#$0E
	cmp	#$0E
	.byte	$C9
LC0A4:	adc	($C8), y
	adc	($C8), y
	adc	($C8), y
	adc	($C8), y
	adc	($C8), y
	dec	$BE
	.byte	$DC
	ldx	$0EAD, y
	ora	$20
	ora	$8F74, x
	cmp	#$9B
	cmp	#$9B
	cmp	#$9A
	iny
	tsx
	iny
	txs
	iny
	bcs	LC08F
	bcs	LC091
	bcs	LC093
	lda	$BDC9, x
	cmp	#$B0
	cmp	#$B0
	cmp	#$B0
	cmp	#$B0
	cmp	#$B0
	cmp	#$4C
	bcs	LC0A4
	txa
	pha
	and	#$F0
	sta	$33
	txa
	asl	a
	asl	a
	asl	a
	asl	a
	sta	$29
	lda	#$0A
	sta	$15
	ldx	#$00
	stx	$12
	stx	$1F
	lda	#$3D
	sta	$90
	lda	#$01
	sta	$51
	tya
	pha
	jsr	LA116
	pla
	sta	$79
	lda	#$FF
	sta	$0441, x
	pla
	tax
	rts

	jmp	LC4F4

	jmp	LC5C9

	cpx	#$E2
	cpx	$E6
	ora	($03, x)
	ora	$07
	.byte	$4F
	eor	$0705, x
	ora	#$0B
	ora	$270F
	adc	$2D7B, y
	.byte	$4F
	.byte	$2F
	eor	$55
	ora	($13), y
	ora	$17, x
	.byte	$1F
	and	($23, x)
	and	$11
	.byte	$13
	.byte	$23
	and	$59
	eor	$5B5B, y
	.byte	$20
LC139:	ora	#$A1
	lda	#$03
	sta	$0465, x
	rts

LC141:	ora	($FF, x)
LC143:	plp
	cld
LC145:	ora	($FF, x)
LC147:	bpl	LC139
	jsr	LC239
	lda	#$06
	sta	$046E, x
	lda	#$02
	sta	$981F
	lda	$04B5
	beq	LC1B0
	cmp	#$01
	bne	LC171
	sta	$0480, x
	lda	#$90
	sta	$86, x
	lda	#$40
	sta	$0438, x
	sta	$045C, x
	sta	$04B5
LC171:	lda	$0480, x
	cmp	#$02
	bcc	LC1EC
	lda	$B1, x
	bne	LC190
	inc	$0480, x
	lda	$0480, x
	cmp	#$31
	bne	LC1B1
	lda	$0453, x
	bne	LC190
	inc	$B1, x
	jsr	LC1E6
LC190:	dec	$0480, x
	ldy	$0480, x
	dey
	bne	LC1B1
	dec	$B1, x
	lda	$50
	cmp	#$06
	bne	LC1B1
	lda	#$01
	sta	$0534
	jsr	L92B7
	lda	#$09
	sta	$14
	inc	$0627
LC1B0:	rts

LC1B1:	lda	$0480, x
	cmp	#$30
	bne	LC1EB
	lda	$5B, x
	and	#$40
	beq	LC1EB
	lda	$9C
	bne	LC1EB
	sta	$5A
	inc	$B1, x
	inc	$04B4
	dec	$0480, x
	lda	$29, x
	sta	$28
	lda	$15, x
	sta	$14
	lda	$33, x
	adc	#$10
	sta	$32
	lda	#$06
	sta	$50
	lda	#$60
	sta	$82
	lda	#$FC
	sta	$46
LC1E6:	lda	#$80
	sta	apu_dpcm_queue
LC1EB:	rts

LC1EC:	lda	#$03
	sta	$046E, x
	lda	#$00
	sta	$981F
	lda	$0465, x
	bne	LC20B
	lda	#$03
	sta	$0465, x
	jsr	LC1E6
	inc	$0480, x
	lda	#$FF
	sta	$0453, x
LC20B:	lda	$10
	lsr	a
	bcc	LC234
	lda	$79, x
	and	#$01
	tay
	lda	$47, x
	clc
	adc	LC145, y
	sta	$47, x
	cmp	LC147, y
	bne	LC224
	inc	$79, x
LC224:	jsr	LB168
	lda	$3D, x
	cmp	LC143, y
	beq	LC234
	clc
	adc	LC141, y
	sta	$3D, x
LC234:	.byte	$4C
	.byte	$8C
LC236:	.byte	$A7
	sed
	.byte	$10
LC239:	lda	$0480, x
	jsr	LA1BD
	lda	$04B5
	beq	LC2BF
	lda	$EE
	and	#$0C
	bne	LC2BF
	lda	$86, x
	sta	$07
	ldy	#$90
	ldx	$02
	dex
	beq	LC257
	ldy	#$B0
LC257:	sty	LC266
	jsr	LABB2
	ldx	$02
	lda	$0429
	clc
	adc	LC236, x
LC266:	bcs	LC2BF
	sta	$0203, y
	sta	$0207, y
	sta	$020B, y
	sta	$020F, y
	ldx	$04BD
	beq	LC27B
	ldx	#$10
LC27B:	lda	$0200, x
	sta	$0200, y
	clc
	adc	#$10
	sta	$0204, y
	lda	$07
	beq	LC28D
	lda	#$20
LC28D:	ora	$0202, x
	sta	$0202, y
	sta	$0206, y
	sta	$020A, y
	sta	$020E, y
	ldx	a:$F4
	lda	$0200, x
	sta	$0208, y
	clc
	adc	#$10
	sta	$020C, y
	lda	#$F0
	sta	$0201, y
	lda	#$F2
	sta	$0205, y
	lda	#$F4
	sta	$0209, y
	lda	#$F6
	sta	$020D, y
LC2BF:	ldx	$12
	rts

LC2C2:	php
	plp
	pha
	plp
LC2C6:	sty	$84, x
	sty	$84, x
LC2CA:	sed
	php
	sed
	php
	php
	sed
	php
	sed
	lda	$9C
	bne	LC31E
	lda	$10
	and	#$FF
	bne	LC31E
	inc	$04F9
	jsr	LA65E
	bmi	LC31E
	ldx	$00
	lda	$04F9
	and	#$07
	tay
	lda	LC2CA, y
	sta	$3D, x
	tya
	and	#$03
	tay
	lda	#$02
	sta	$15, x
	lda	LC2C2, y
	sta	$29, x
	lda	LC2C6, y
	sta	$33, x
	lda	#$00
	sta	$1F, x
	lda	$04BB
	and	#$03
	cmp	#$02
	bcc	LC313
	asl	a
	sta	$B1, x
LC313:	ldy	#$31
	sty	$90, x
	jsr	L9839
	lda	#$D0
	sta	$47, x
LC31E:	rts

	jsr	L9855
	lda	#$04
	sta	$0465, x
	lda	$15, x
	sta	$04EF, x
	rts

LC32D:	cpx	#$F0
LC32F:	inx
	cpx	$B5
	lda	($D0), y
	.byte	$73
	lda	$0465, x
	bne	LC342
	lda	#$80
	sta	$86, x
	sta	$B1, x
	bne	LC3A5
LC342:	inc	$79, x
	lda	$10
	and	#$FF
	bne	LC351
	lda	#$5F
	sta	$86, x
	inc	$0480, x
LC351:	lda	#$00
	sta	$3D, x
	lda	$79, x
	and	#$40
	beq	LC368
	inc	$0477, x
	lda	#$F8
	ldy	$79, x
	bpl	LC366
	lda	#$08
LC366:	sta	$3D, x
LC368:	jsr	LB17D
	lda	$045C, x
	bne	LC3A5
	lda	$86, x
	beq	LC3A5
	and	#$0F
	bne	LC3A5
	jsr	LA662
	bmi	LC3A5
	lda	#$80
	sta	apu_dpcm_queue
	lda	$0480, x
	and	#$03
	tay
	lda	$86, x
	ldx	$00
	lsr	a
	eor	#$FF
	sta	$3D, x
	lda	LC32D, y
	sta	$47, x
	lda	#$11
	sta	$90, x
	lda	$33, x
	adc	#$08
	sta	$33, x
	jsr	L9839
	ldx	$12
LC3A5:	jmp	LAE56

	lda	$86, x
	beq	LC3BB
	sta	$045C, x
	inc	$0477, x
	inc	$0477, x
	lda	#$F0
	sta	$47, x
	bne	LC3F1
LC3BB:	lda	#$04
	sta	$3D, x
	jsr	LB17D
	jsr	LB178
	lda	$10
	lsr	a
	bcs	LC3E7
	inc	$47, x
	bmi	LC3E7
	lda	$10
	and	#$1F
	bne	LC3E7
	lda	#$10
	sta	byte_602
	jsr	LA662
	ldx	$00
	lda	$33, x
	adc	#$08
	sta	$33, x
	jsr	LA551
LC3E7:	lda	$33, x
	cmp	#$D0
	bcc	LC3F1
	lda	#$02
	sta	$51, x
LC3F1:	jmp	LAE56

	inc	$9F, x
	jsr	LB17D
	jsr	LB178
	inc	$47, x
	jmp	LAE56

LC401:	rts

LC402:	brk
LC403:	.byte	$E0
LC404:	.byte	$FF
LC405:	bne	LC407
LC407:	.byte	$E0
LC408:	.byte	$FF
	lda	a:$F4
	sta	LC404
	sta	LC408
	lda	$10
	and	#$03
	sta	$07
	tay
	lda	LC402, y
	sta	a:$F4
	lda	$EF
	bne	LC401
	ldy	$0465, x
	bne	LC42C
	lda	#$4E
	sta	$65, x
LC42C:	lda	$EE
	pha
	pha
	ldy	#$49
	lda	$B1, x
	bne	LC445
	lda	$045C, x
	beq	LC448
	cmp	#$30
	bcs	LC445
	and	#$08
	bne	LC445
	ldy	#$39
LC445:	tya
	bne	LC450
LC448:	lda	#$39
	ldy	$86, x
	beq	LC450
	lda	#$3D
LC450:	jsr	LAEE0
	lda	$00
	sta	$042C
	ldy	$07
	lda	LC403, y
	sta	a:$F4
	ldy	#$41
	lda	$B1, x
	bne	LC479
	lda	$045C, x
	beq	LC475
	cmp	#$30
	bcs	LC479
	and	#$08
	bne	LC479
	beq	LC47B
LC475:	lda	$86, x
	beq	LC47B
LC479:	ldy	#$45
LC47B:	pla
	sta	$EE
	tya
	jsr	LAEE0
	lda	$00
	sta	$042C
	ldy	$07
	lda	LC404, y
	sta	a:$F4
	ldy	#$55
	lda	$3D, x
	beq	LC4A0
	ldy	#$4D
	lda	$0477, x
	and	#$10
	beq	LC4A0
	ldy	#$51
LC4A0:	pla
	sta	$EE
	tya
	jsr	LAEE0
	lda	$EE
	bne	LC4F1
	ldy	$07
	ldx	LC404, y
	lda	LC405, y
	tay
	lda	$0429
	clc
	adc	#$20
	bcs	LC4F1
	sta	$0203, y
	sta	$0207, y
	sta	$020B, y
	lda	$00
	sbc	#$2F
	sta	$0200, y
	adc	#$0F
	sta	$0204, y
	adc	#$10
	sta	$0208, y
	lda	$0202, x
	sta	$0202, y
	sta	$0206, y
	sta	$020A, y
	lda	#$19
	sta	$0201, y
	lda	#$1B
	sta	$0205, y
	lda	#$1D
	sta	$0209, y
LC4F1:	ldx	$12
	rts

LC4F4:	lda	$B1, x
	bne	LC4FB
	jmp	LC5B2

LC4FB:	ldy	#$03
	lda	$47, x
	beq	LC505
	cmp	#$FD
	bcc	LC516
LC505:	ldy	#$3F
	inc	$0429
	lda	$10
	and	#$02
	bne	LC516
	dec	$0429
	dec	$0429
LC516:	tya
	and	$10
	bne	LC51D
	dec	$47, x
LC51D:	jsr	LB178
	lda	$0477, x
	bne	LC53F
	ldy	$1F, x
	bpl	LC563
	jsr	L92B7
	lda	#$36
	sta	$04AF
	inc	$0627
	lda	#$05
	sta	$0534
	lda	#$00
	sta	a:$50
	rts

LC53F:	lda	$33, x
	cmp	#$30
	bcs	LC563
	ldy	$04C7
	bne	LC551
	cmp	#$18
	bcs	LC563
	jmp	LA4B6

LC551:	lda	#$00
	sta	$04C7
	sta	$9C
	sta	$3C
	lda	$33, x
	adc	#$20
	sta	$32
	sta	$042B
LC563:	jsr	LC5C9
	lda	$0429
	sec
	sbc	#$04
	sta	$0293
	adc	#$07
	sta	$0297
	adc	#$08
	sta	$029B
	lda	#$20
	ldy	$47, x
	cpy	#$FD
	bmi	LC583
	lda	#$15
LC583:	adc	$042C
	sta	$0290
	sta	$0294
	sta	$0298
	lda	#$8C
	sta	$0291
	sta	$0295
	sta	$0299
	lda	$10
	lsr	a
	and	#$03
	sta	$00
	lsr	a
	ror	a
	ror	a
	and	#$C0
	ora	$00
	sta	$0292
	sta	$0296
	sta	$029A
	rts

LC5B2:	lda	$A8, x
	cmp	#$01
	bne	LC5C6
	sta	$B1, x
	sta	$04C7
	lda	#$02
	sta	apu_dpcm_queue
	lda	#$FE
	sta	$47, x
LC5C6:	jsr	LA956
LC5C9:	lda	$042C
	sta	$00
	lda	$0429
	sec
	sbc	#$08
	sta	$01
	lda	#$02
	sta	$02
	sta	$05
	sta	$0C
	lda	$65, x
	and	#$23
	sta	$03
	ldy	#$00
	ldx	#$35
	jsr	LAF80
	lda	$01
	clc
	adc	#$10
	sta	$01
	dec	$02
	lda	$042C
	sta	$00
	ldy	#$10
	ldx	#$35
	jmp	LAF80

	inc	$FEFE, x
	inc	$B6B4, x
	lda	$B7, x
	clv
	.byte	$FA
	lda	$FAFA, y
	.byte	$FA
	.byte	$B2
	.byte	$B3
	ldx	LBFBE, y
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	lsr	a
LC619:	lsr	a
	.byte	$4B
	.byte	$4B
	.byte	$5E
LC61D:	.byte	$5F
	lsr	$465F, x
LC621:	.byte	$FC
	lsr	$FC
	.byte	$FC
LC625:	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
LC629:	pha
	.byte	$FC
	pha
	tay
LC62D:	eor	$A9A9, x
	.byte	$74
LC631:	ror	$75, x
	.byte	$77
	tya
LC635:	txs
	sta	$9C9B, y
LC639:	txs
	sta	$9C9B, x
LC63D:	.byte	$9E
	.byte	$9B
	.byte	$9F
	cli
LC641:	.byte	$5A
	eor	$5E5B, y
	.byte	$5F
	lsr	$725F, x
	.byte	$73
	.byte	$73
	.byte	$72
	ldx	$A6
	.byte	$A7
	.byte	$A7
	.byte	$72
	.byte	$73
	.byte	$73
	.byte	$72
	.byte	$74
	ror	$75, x
	.byte	$77
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	bcs	LC619
	.byte	$FA
	.byte	$FA
	bcs	LC61D
	.byte	$FA
	.byte	$FA
	bcs	LC621
	.byte	$FA
	.byte	$FA
	bcs	LC625
	.byte	$FA
	.byte	$FA
	bcs	LC629
	.byte	$FA
	.byte	$FA
	bcs	LC62D
	.byte	$FA
	.byte	$FA
	bcs	LC631
	.byte	$FA
	.byte	$FA
	bcs	LC635
	.byte	$FA
	.byte	$FA
	bcs	LC639
	.byte	$FA
	.byte	$FA
	bcs	LC63D
	.byte	$FA
	.byte	$FA
	bcs	LC641
	ldy	#$A2
	lda	($A3, x)
	.byte	$80
	.byte	$82
	sta	($83, x)
	.byte	$F4
	stx	$F5
	.byte	$87
	sty	$86
	sta	$87
	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
	lda	$ACFB
	lda	$ACAC
	ldy	$FBAC
	.byte	$3B
	.byte	$3B
	ldy	$FCFC
	.byte	$FC
	.byte	$FC
	.byte	$F4
	stx	$F5
	.byte	$87
	.byte	$FB
	eor	#$49
	.byte	$FB
	inc	$FEFE, x
	inc	$3E3C, x
	and	$583F, x
	inc	$5A59, x
	.byte	$5B
	.byte	$5A
	inc	$FEFE, x
	eor	$5C5B, x
	inc	$5BFE, x
	.byte	$5A
	.byte	$1C
	inc	$FEFE, x
	inc	$FEFE, x
	inc	$1EFE, x
	inc	$6E1F, x
	.byte	$6F
	bvs	LC755
	.byte	$57
	.byte	$57
	.byte	$FB
	.byte	$FB
	.byte	$57
	.byte	$57
LC6EA:	inc	$30FE, x
	bmi	LC6EA
	.byte	$FB
	and	($31), y
	.byte	$FB
	.byte	$FB
	.byte	$7C
	ror	$7F7D, x
	dex
	cpy	LCDCB
	dex
	cpy	LCDCB
	cpy	#$C2
	cmp	($C3, x)
	bit	$2D2E
	.byte	$2F
	.byte	$72
	.byte	$73
	.byte	$73
	.byte	$72
	dey
	txa
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	sty	$888D
	txa
	sty	$888D
	txa
	.byte	$89
	.byte	$8B
	dey
	txa
	.byte	$89
	.byte	$8B
	sty	$95, x
	sty	$95, x
	stx	$97, y
	stx	$97, y
	pha
	eor	#$48
	eor	#$FE
	inc	$FEFE, x
	.byte	$FB
	.byte	$32
	.byte	$32
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	sbc	$FDFD, x
	sbc	$FB34, x
LC742:	sbc	$FB34, x
	bmi	LC742
	.byte	$FB
	.byte	$FB
	.byte	$FB
	and	($FB), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	$56, x
	lsr	$56, x
	.byte	$64
LC755:	ror	$65
	.byte	$67
	pla
	ror	a
	adc	#$6B
	.byte	$FA
	jmp	(L6CFA)

	adc	$6DFA
	.byte	$FA
	.byte	$72
	.byte	$73
	.byte	$73
	.byte	$72
	ldx	$AEAF
	.byte	$AF
	sei
	.byte	$7A
	adc	$EF7B, y
	.byte	$EF
	.byte	$AF
	ldx	$9594
	sty	$95, x
	stx	$97, y
	stx	$97, y
	.byte	$22
	bit	$23
	and	$72
	.byte	$73
	.byte	$73
	.byte	$72
	bvc	LC7D7
	bvc	LC7D9
	ldx	$AEAF
	.byte	$AF
	bvc	LC7DF
	bvc	LC7E1
	.byte	$72
	.byte	$73
	.byte	$73
	.byte	$72
	bvc	LC7E8
	eor	($53), y
	bvc	LC7EC
	eor	($53), y
	.byte	$FB
	rol	$36, x
	.byte	$4F
	.byte	$4F
	lsr	$4F4E
	lsr	$4F4F
	lsr	$7372
	.byte	$73
	.byte	$72
	.byte	$72
	.byte	$73
	.byte	$73
	.byte	$72
	inc	$FE4B, x
	inc	$7372, x
	lsr	a
	.byte	$4B
	rti

	.byte	$42
	eor	($43, x)
	eor	($43, x)
	eor	($43, x)
	rti

	.byte	$42
	eor	($43, x)
	rti

	.byte	$42
	eor	($43, x)
	tsx
	ldy	$BDBB, x
	tsx
	ldy	$9190, x
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
LC7D7:	.byte	$FA
	.byte	$FD
LC7D9:	sbc	$FDFD, x
	lsr	$FC
	.byte	$46
LC7DF:	.byte	$FC
	.byte	$FC
LC7E1:	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
	pha
	.byte	$FC
	pha
LC7E8:	.byte	$32
	.byte	$34
	.byte	$33
	.byte	$35
LC7EC:	rol	$34, x
	.byte	$37
	and	$36, x
	sec
	.byte	$37
	and	$6268, y
	adc	($63, x)
	.byte	$64
	adc	#$65
	.byte	$67
	lsr	$62
	adc	($63, x)
	.byte	$64
	.byte	$47
	adc	$67
	tsx
	ldy	$BDBB, x
	bvs	LC87C
	adc	($73), y
	.byte	$72
	.byte	$73
	.byte	$73
	.byte	$72
	.byte	$44
	eor	$45
	.byte	$44
LC814:	.byte	$0C
	ror	$0C
	.byte	$9C
	.byte	$9B
	adc	$62
	.byte	$67
	eor	$45
	eor	$45
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$80
	.byte	$07
	sta	($80, x)
	sta	($81, x)
	sta	($81, x)
LC838:	lda	$050E
	asl	a
	asl	a
	sta	$0F
	lda	$050E
	cmp	#$07
	bcc	LC84C
	lda	$0543
	jmp	LC84F

LC84C:	lda	$0542
LC84F:	clc
	adc	$0F
	tax
	lda	LC814, x
	sta	($01), y
	rts

	ldy	$E7
LC85B:	jsr	LC838
	jsr	LBE0C
	dec	$050D
	bpl	LC85B
	rts

LC867:	.byte	$4F
	.byte	$4F
	.byte	$83
	ldy	$A3
LC86C:	bvc	LC8BF
	.byte	$83
	.byte	$83
	.byte	$83
	ldy	$E7
	lda	$050E
	cmp	#$09
	bne	LC885
	.byte	$AD
	.byte	$B0
LC87C:	.byte	$04
	beq	LC885
	inc	$050E
	inc	$050E
LC885:	lda	$050E
	sec
	sbc	#$09
	tax
	lda	LC867, x
	sta	($01), y
	jsr	LBE24
	lda	LC86C, x
	sta	($01), y
	rts

	ldy	$E7
	lda	#$52
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	lda	$E7
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	iny
	lda	#$54
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	rts

	ldy	$E7
	lda	#$52
	.byte	$91
LC8BF:	ora	($88, x)
	lda	#$55
	sta	($01), y
	ldy	$E7
	tya
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	dey
	lda	#$54
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	rts

	ldy	$E7
	lda	$050E
	cmp	#$06
	bne	LC8EB
	lda	#$9E
	sta	($01), y
	jmp	LC8EE

LC8EB:	jsr	LC838
LC8EE:	jsr	LBE24
	dec	$050D
	bpl	LC8EB
	rts

	ldy	$E7
LC8F9:	jsr	LC838
	tya
	clc
	adc	#$11
	tay
	dec	$050D
	bpl	LC8F9
	rts

LC907:	.byte	$67
	ror	a
	.byte	$9C
	ora	($70), y
	sty	$95, x
	lda	$050E
	tax
	cmp	#$05
	bne	LC91C
	lda	$0543
	beq	LC91C
	inx
LC91C:	ldy	$E7
	lda	LC907, x
	sta	($01), y
	rts

LC924:	brk
	ora	$08CA
LC928:	brk
	asl	$09CB
LC92C:	brk
	.byte	$0F
	.byte	$CC
	asl	a
LC930:	ldy	$E7
	lda	$050E
	sec
	sbc	#$0A
	tax
	lda	LC924, x
	sta	($01), y
	dec	$050D
	beq	LC950
LC943:	jsr	LBE0C
	lda	LC928, x
	sta	($01), y
	dec	$050D
	bne	LC943
LC950:	jsr	LBE0C
	lda	LC92C, x
	sta	($01), y
	rts

	lda	$050D
	sta	$07
	lda	#$0C
	sta	$050E
	jsr	LC930
LC966:	lda	$E7
	clc
	adc	#$10
	sta	$E7
	lda	#$0D
	sta	$050E
	lda	a:$07
	sta	$050D
	ldx	$E8
	jsr	LBE01
	ldy	$E7
	lda	($01), y
	cmp	#$40
	bne	LC98E
	jsr	LC930
	lda	$E7
	cmp	#$E0
	bcc	LC966
LC98E:	rts

	ldy	$E7
	lda	#$01
	sta	($01), y
	iny
	lda	#$02
	sta	($01), y
	rts

	ldy	$E7
	lda	#$03
	sta	($01), y
	rts

LC9A2:	.byte	$43
	.byte	$44
	eor	$46
	.byte	$47
	pha
	eor	#$4A
	.byte	$4B
	lsr	$414C
	.byte	$5A
	.byte	$42
	ldy	$E7
	ldx	$050E
	lda	LC9A2, x
	sta	($01), y
	rts

LC9BB:	.byte	$9B
	.byte	$9B
LC9BD:	lda	$050E
	sec
	sbc	#$09
	sta	$08
	ldy	$E7
	ldx	$E8
	jsr	LBE01
	ldy	$E7
	lda	#$05
	sta	$07
	lda	($01), y
	cmp	#$40
	bne	LC9F4
LC9D8:	ldx	$08
	lda	LC9BB, x
	sta	($01), y
	jsr	LBE0C
	dec	$07
	bpl	LC9D8
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LC9F4
	sta	$E7
	jmp	LC9BD

LC9F4:	rts

	ldy	$E7
LC9F7:	lda	#$58
	sta	($01), y
	tya
	clc
	adc	#$0F
	tay
	dec	$050D
	bne	LC9F7
	rts

	brk
	.byte	$F3
	brk
	brk
LCA0A:	.byte	$03
	.byte	$A3
	sbc	$12, x
	.byte	$12
	.byte	$43
	sbc	$20
	stx	$01
	stx	$18
	.byte	$E3
	.byte	$3A
	.byte	$E3
	bmi	LCA0A
	.byte	$FF
	.byte	$80
	inx
	.byte	$63
	brk
	.byte	$27
	lda	#$74
	.byte	$23
	sbc	$12, x
	jsr	L8113
	.byte	$04
	sta	($05, x)
	sta	($0A, x)
	.byte	$E3
	.byte	$22
	sbc	#$64
	.byte	$E3
	ldx	$80, y
	.byte	$0C
	.byte	$80
	rol	$6C11, x
	ldx	$F5
	.byte	$12
	brk
	and	$36E2
	sbc	$0C
	ora	$0D
	cpx	$41
	bpl	LCAC7
	ora	($31, x)
	.byte	$3C
	.byte	$0F
	.byte	$33
	rol	$3480
	bpl	LCA6A
	ora	($76), y
	.byte	$52
	.byte	$07
	.byte	$2B
	asl	a
	.byte	$22
	ora	$C6, x
	asl	$233A, x
	.byte	$80
	ora	$3D80
	bpl	LCACE
	rol	$1D
	brk
	.byte	$1C
	.byte	$E7
LCA6A:	eor	#$80
	.byte	$57
	and	($33, x)
	.byte	$04
	.byte	$13
	brk
	ora	($EE), y
	.byte	$F4
	.byte	$42
	asl	$0E06
	.byte	$DC
	asl	$0E26
	.byte	$E7
	asl	$90F2
	asl	$0E1E
	.byte	$E3
	asl	$0E1D
	sbc	#$0E
	ldx	$C4, y
	ora	$180E, x
	asl	a
	sbc	$12, x
	.byte	$50
LCA93:	.byte	$FF
	.byte	$80
	inx
	bmi	LCA98
LCA98:	.byte	$3C
	ora	($19), y
	bpl	LCAFC
	.byte	$EB
	.byte	$34
	.byte	$E7
	.byte	$0C
	sbc	#$54
	bpl	LCAC0
	.byte	$29
LCAA6:	.byte	$5C
	inc	$31
	.byte	$29
LCAAA:	.byte	$02
	inx
	ora	$5CEA, y
	and	#$43
	sbc	$0423
	asl	$EA
	bpl	LCAA6
	clc
	.byte	$E7
	.byte	$12
	.byte	$EF
	.byte	$23
	cpx	$A729
LCAC0:	sbc	$12, x
	.byte	$33
	bvc	LCAAA
	and	($29, x)
LCAC7:	.byte	$12
	.byte	$EB
	.byte	$20
LCACA:	and	#$02
	sbc	$28
LCACE:	sbc	$10
	.byte	$E3
	.byte	$F4
	sta	$0E, x
	.byte	$DB
	asl	$0ECF
	.byte	$12
	asl	$0EA8
	.byte	$44
	asl	$0E41
	.byte	$FF
LCAE1:	brk
	.byte	$F3
	bmi	LCAE5
LCAE5:	.byte	$64
	ldx	$0A
	ldx	$22
	sbc	($04, x)
	ora	$05
	cpx	$0A
LCAF0:	ora	$0B
	sbc	($37, x)
	tax
	and	($E5, x)
	.byte	$07
LCAF8:	ora	$08
	sbc	$66
LCAFC:	.byte	$AB
	php
LCAFE:	.byte	$AB
	.byte	$14
	tax
	asl	a
	tax
	and	($E2, x)
	.byte	$0B
	.byte	$E2
	bcc	LCAF8
	ror	$00
	.byte	$07
	brk
	php
	brk
	ora	#$00
	.byte	$12
	.byte	$A7
	ora	$00
	asl	a
	brk
	.byte	$15
LCB18:	brk
	asl	a
LCB1A:	brk
	bpl	LCAFE
	.byte	$02
	ora	$03
	cpx	a:$85
	asl	a
	brk
	.byte	$0C
	brk
	bpl	LCB18
	bmi	LCB1A
	.byte	$3A
	.byte	$E3
	asl	$A3, x
	sbc	$12, x
	.byte	$23
	jsr	L06E5
	ora	$07
	inx
	.byte	$F4
	and	$C4, x
	.byte	$23
	asl	$0E0B
	.byte	$17
	asl	a
	sbc	$12, x
	rti

	.byte	$C2
	asl	$0E0C
	.byte	$FF
	lda	($EA, x)
	bpl	LCB4F
	pha
	asl	a
LCB4F:	sbc	$12, x
	bmi	LCB5F
	.byte	$33
	beq	LCAE1
	sbc	($6C), y
	.byte	$F2
	.byte	$74
	and	$431A, y
	.byte	$1A
	.byte	$43
LCB5F:	brk
	brk
	beq	LCAF0
	sbc	($4C), y
	sbc	($CA), y
	sbc	$13, x
	brk
	.byte	$FF
	lda	($EA, x)
	brk
	ora	($4C, x)
	brk
	and	($2D, x)
	.byte	$02
	and	($03, x)
	and	($04, x)
	and	($05, x)
	and	($06, x)
	rol	$07
	eor	($11), y
	sec
	.byte	$3A
	.byte	$32
	.byte	$17
	asl	a
	sbc	$12, x
	asl	$F0, x
	.byte	$3C
	sbc	($EA), y
	.byte	$FF
	.byte	$80
	.byte	$FF
	jsr	L8000
	sbc	$0D
	sbc	$2A
	sbc	$36
	sbc	$52
	bpl	LCBB6
	bpl	LCBBA
	bpl	LCC03
	inc	$0B
	.byte	$E6
LCBA3:	.byte	$62
	bpl	LCBED
	sbc	$52, x
	adc	$27
	inx
	sbc	$13, x
	bpl	LCBA3
	jsr	L4FC4
	asl	$95F2
	.byte	$0E
LCBB6:	asl	$680E
	.byte	$1A
LCBBA:	asl	$3B1A
	.byte	$0C
	asl	$270C
	rti

	plp
	ora	$0B28
	.byte	$FF
	lda	($EA, x)
	adc	$01
	.byte	$87
	.byte	$13
	sbc	$13, x
	.byte	$02
	.byte	$22
	and	$50F0, x
	sta	$84
	.byte	$0B
	sty	$6F
	.byte	$04
	bpl	LCC1A
	beq	LCC12
	beq	LCC50
	sbc	($14), y
	sbc	($50), y
	sbc	($94), y
	sbc	($B0), y
	sbc	($EF), y
	cmp	($4E), y
LCBEC:	.byte	$42
LCBED:	adc	$17F0, x
	beq	LCC42
	cmp	$1004
	jmp	L820D

	asl	$2043
	.byte	$7C
	asl	$F175
	.byte	$14
	sbc	($50), y
	.byte	$A2
LCC03:	and	$8027, x
	.byte	$17
	ldy	$F5
	.byte	$13
	rts

	asl	$144F
	sta	($27, x)
	.byte	$03
	.byte	$90
LCC12:	and	$3E, x
	lsr	$24
	ror	$0C, x
	sei
	sec
LCC1A:	ldx	$F5
	.byte	$13
	.byte	$22
	stx	$84
	.byte	$07
	sty	$08
	sty	$45
	.byte	$80
	sbc	($2A), y
	.byte	$FF
LCC29:	lda	($EA, x)
	sta	$01, x
	dey
	asl	a
	sbc	$13, x
	.byte	$93
	.byte	$13
LCC33:	.byte	$2B
	ora	$0E
	.byte	$0B
	asl	$0E13
	ora	$240E
	rol	a
	.byte	$0C
	rol	$F0
	.byte	$50
LCC42:	beq	LCC33
	sbc	($50), y
	sbc	($EA), y
	sec
	lda	#$F5
	.byte	$13
	stx	$3C
	sta	($0D, x)
LCC50:	.byte	$80
	.byte	$3A
	.byte	$A3
	.byte	$5C
LCC54:	sta	($1D, x)
	.byte	$80
	beq	LCC69
	beq	LCBEC
	sbc	($90), y
	.byte	$62
	.byte	$80
	.byte	$03
	sta	($35, x)
	.byte	$A3
	.byte	$07
	tay
	sbc	$13, x
	asl	$47, x
LCC69:	.byte	$03
LCC6A:	.byte	$13
	sta	($12, x)
	.byte	$80
	.byte	$17
	.byte	$03
	beq	LCC03
	sbc	($0A), y
	plp
	lda	#$F5
	.byte	$13
	.byte	$73
	.byte	$3C
	sta	($0D, x)
	.byte	$80
	.byte	$3A
	.byte	$A3
	.byte	$5C
	sta	($1D, x)
	.byte	$80
	sbc	($11), y
	sbc	($90), y
	.byte	$62
	.byte	$80
	.byte	$03
	sta	($35, x)
	.byte	$A3
	.byte	$53
	sta	($08, x)
	asl	a
	sbc	$13, x
	ror	$12
LCC95:	.byte	$80
	beq	LCC29
	beq	LCC95
LCC9A:	sbc	($4A), y
	sec
	.byte	$A7
	sbc	$13, x
LCCA0:	eor	($98, x)
	.byte	$4E
	.byte	$F1
LCCA4:	ora	($F1), y
	.byte	$CF
	.byte	$97
	.byte	$AB
	.byte	$F5
LCCAA:	.byte	$13
	bvs	LCC54
	.byte	$03
LCCAE:	.byte	$F0
LCCAF:	beq	LCCA0+2
	dex
	tya
	asl	a
	sbc	$13, x
	.byte	$43
	and	$3D
	beq	LCC6A
	.byte	$62
	.byte	$A7
	sbc	$13, x
	.byte	$44
	stx	$2D
	asl	$0E, x
	ora	$0F0E, y
	asl	$0E1C
	.byte	$1B
	rol	$0D
	rol	a
	beq	LCD3A
	beq	LCCAE
	.byte	$7C
	asl	a
	sbc	$13, x
	.byte	$32
	.byte	$52
	asl	$6EF0
	sbc	($CA), y
	.byte	$FF
	lda	($EA, x)
	.byte	$34
	ora	($5C, x)
	rti

	.byte	$1C
	ldy	$25
	asl	a
	sbc	$13, x
	.byte	$63
	bit	$33
	beq	LCD3F
	.byte	$80
	.byte	$4F
	bpl	LCC9A
	ora	$A7
	.byte	$0C
	.byte	$A7
	.byte	$80
	.byte	$4F
	rts

	.byte	$4F
	bpl	LCD4C
	bpl	LCCA4
	and	$35, x
	.byte	$13
	asl	a
	sbc	$13, x
	and	#$20
	.byte	$4F
	bpl	LCD59
	bcc	LCCAF
	plp
	asl	a
	sbc	$13, x
	adc	#$F0
	ora	($F1), y
	lsr	a
	.byte	$FF
	lda	($EA, x)
	sta	$02, x
	.byte	$3F
	.byte	$A7
	rol	$80, x
	asl	$A3, x
	.byte	$43
	asl	a
	sbc	$13, x
	.byte	$80
	ora	$F001, y
	bvc	LCCAA
	.byte	$87
	ora	($87, x)
	.byte	$02
	and	$8047, x
	.byte	$0B
	sta	($0E, x)
	sta	($0F, x)
	sta	($17, x)
	ldy	$F5
LCD3A:	.byte	$13
	and	$37
	.byte	$03
	.byte	$70
LCD3F:	.byte	$3F
	eor	$0E, x
	.byte	$0B
	.byte	$0E
	.byte	$14
LCD45:	asl	$0908
	sbc	$13, x
	.byte	$47
	.byte	$0C
LCD4C:	asl	$3F90
	.byte	$52
	.byte	$80
	ora	$80
	.byte	$17
	asl	a
	sbc	$13, x
	.byte	$27
	.byte	$7A
LCD59:	ldx	$0D
	.byte	$AB
	jsr	L0837
	.byte	$87
	ora	#$87
	sta	$F503
	.byte	$13
	plp
	sbc	($CA), y
	.byte	$F3
	.byte	$F2
	.byte	$B7
	asl	a
	sbc	$13, x
	.byte	$42
	beq	LCDC2
	.byte	$E3
	brk
	php
	brk
	.byte	$13
	asl	$0E08
	.byte	$F2
	ldy	$0E
	.byte	$12
	asl	$0E03
	ora	$0E
	asl	$0E
	sbc	($4F), y
	sbc	($8A), y
	sbc	$13, x
	.byte	$50
LCD8C:	.byte	$FF
	tax
	nop
	.byte	$22
	.byte	$0B
	and	$622C, x
	pha
	beq	LCDE6
	beq	LCD45
	sbc	($6E), y
	bcc	LCDC9
	.byte	$03
	bit	$2C06
	ora	$4F2C, x
	.byte	$32
	and	#$44
	sbc	($3A), y
	sbc	($DC), y
	.byte	$90
LCDAC:	bit	$2C03
	asl	$2C
	ora	#$2C
	.byte	$34
	.byte	$32
	.byte	$22
	.byte	$0F
	bpl	LCDC8
	.byte	$04
	.byte	$0F
	php
	asl	$4409
	.byte	$12
	eor	($10, x)
LCDC2:	eor	$F1
	.byte	$7A
	sbc	($D8), y
	.byte	$FF
LCDC8:	.byte	$21
LCDC9:	cpx	#$97
LCDCB:	.byte	$01
LCDCC:	php
	lda	$F5
	.byte	$13
	.byte	$14
	.byte	$92
	.byte	$4B
	.byte	$42
	.byte	$4B
	beq	LCE2C
	sbc	($DC), y
LCDD9:	.byte	$33
	and	#$22
	.byte	$4B
	.byte	$42
	.byte	$4B
	.byte	$3C
	and	#$22
	.byte	$4B
	beq	LCE5A
	.byte	$F1
LCDE6:	.byte	$1C
	sbc	($95), y
	.byte	$42
	.byte	$4B
	.byte	$3C
	and	#$22
	.byte	$4B
	.byte	$42
	.byte	$4B
	.byte	$33
	and	#$F0
	.byte	$5C
LCDF5:	beq	LCDCC
	sbc	($7C), y
	.byte	$22
	.byte	$4B
	.byte	$37
	and	#$12
	.byte	$4B
	.byte	$7A
	asl	a
	sbc	$13, x
	bmi	LCDF5
	ora	$F1, x
	ldy	#$F3
	.byte	$F2
	pha
	tax
	.byte	$27
	asl	a
	sbc	$13, x
	bit	$28
	.byte	$03
	beq	LCDAC
	sbc	($00), y
	sei
	ldx	$295A
	ora	$29, x
	sec
LCE1E:	.byte	$03
	.byte	$3A
	and	#$16
	and	#$F0
	ror	$F1, x
	jsr	L56F1
	plp
	ldy	$18
LCE2C:	.byte	$03
	.byte	$3B
	and	#$14
	and	#$18
	lda	$28
	.byte	$03
	lsr	$29
	asl	a
LCE38:	and	#$18
	.byte	$AB
	beq	LCE5D
	beq	LCE95
	sbc	($00), y
	sbc	($36), y
	plp
	.byte	$03
	.byte	$B7
	asl	a
	sbc	$13, x
	.byte	$33
	beq	LCE4C
LCE4C:	sbc	($17), y
	sbc	($A0), y
	.byte	$FF
	.byte	$21
LCE52:	cpx	#$35
	ora	($08, x)
	ldy	$F5
	.byte	$13
	.byte	$26
LCE5A:	ror	$33, x
	.byte	$34
LCE5D:	.byte	$37
	and	($33), y
	.byte	$0B
	.byte	$43
	beq	LCEB8
	and	$36, x
	ora	$46, x
	.byte	$25
LCE69:	lsr	$15
	rol	$22, x
	.byte	$42
	.byte	$33
	.byte	$44
	sec
	.byte	$33
	beq	LCE69
	.byte	$3B
	.byte	$42
	rol	$73
	.byte	$0C
	and	($46), y
	and	$33, x
	and	$3336, y
	beq	LCE1E
	beq	LCE38
	.byte	$33
	eor	#$32
	eor	#$2C
	.byte	$42
	bit	$37
	.byte	$17
	.byte	$80
	ora	($32), y
	.byte	$07
	ldy	$F5
	.byte	$13
	.byte	$23
LCE95:	.byte	$37
	.byte	$03
	sbc	($A0), y
	.byte	$FF
	and	($E0, x)
	.byte	$64
	ora	($28, x)
LCE9F:	tay
	bit	$F50A
	.byte	$13
	rti

	and	#$45
	beq	LCEFD
	sta	$35, x
	.byte	$52
	.byte	$32
	eor	$33
	pha
	.byte	$34
	.byte	$34
	.byte	$37
	.byte	$32
	.byte	$33
	bit	$4631
LCEB8:	.byte	$32
	sec
	.byte	$33
	beq	LCE52
	and	$35, x
	.byte	$52
	.byte	$33
	ora	$AD, x
	.byte	$07
	.byte	$33
	.byte	$12
	asl	a
	sbc	$13, x
	bcc	LCEED
	.byte	$32
	.byte	$05
LCECD:	.byte	$03
	asl	a
	and	($1A), y
	lda	#$2A
	.byte	$03
	.byte	$0B
	.byte	$32
	sbc	($5C), y
	sta	($86, x)
	.byte	$14
	bmi	LCEE3
	.byte	$33
	.byte	$0B
	bmi	LCF15
	rol	$31, x
LCEE3:	.byte	$AF
	.byte	$04
	rol	$F0, x
	.byte	$D4
	.byte	$31
LCEE9:	.byte	$03
	.byte	$74
	ldx	$06
LCEED:	ldx	$07
	.byte	$82
	php
	ldx	$0A
	ldx	$0D
	ldx	$20F0
	sbc	($14), y
	adc	$4E03, x
LCEFD:	tay
	.byte	$37
	rti

	.byte	$17
	ldx	$F5
	.byte	$13
	and	($57, x)
	.byte	$03
	beq	LCF09
LCF09:	beq	LCE9F
	sbc	($A0), y
	.byte	$F4
	dec	$0E, x
	ora	#$0E
	.byte	$53
	.byte	$0E
	.byte	$46
LCF15:	asl	$0E3A
	.byte	$B7
	asl	$0E2A
	ldx	$0E, y
	.byte	$FF
	ora	($FE, x)
	bmi	LCF23
LCF23:	plp
	bpl	LCF73
	sta	($2B, x)
	lda	$0A0D
	sbc	$13, x
	.byte	$83
	.byte	$2B
	ora	$0C
	eor	($22, x)
	bpl	LCF7B
	bpl	LCF83
	tax
	.byte	$2B
	.byte	$80
	.byte	$0C
	ora	$11
	bpl	LCF67
	ora	($4B), y
	tax
	ora	$10, x
	.byte	$1B
	ora	$0C
	.byte	$80
	.byte	$7C
	tax
	ora	($10), y
	.byte	$1B
	.byte	$80
	.byte	$0C
	ora	$24
	and	$14, x
	and	$14, x
	and	$14, x
	and	$14, x
	.byte	$33
	php
	.byte	$AB
	.byte	$14
	.byte	$33
	.byte	$14
	.byte	$33
	clc
	ora	$77
	asl	a
	sbc	$13, x
	.byte	$20
LCF67:	beq	LCEE9
	sbc	($57), y
	sbc	($A0), y
	.byte	$FF
	.byte	$03
	rti

	rol	a
	ora	($01, x)
LCF73:	ora	($01, x)
	ora	$8B09
	.byte	$0C
	sty	$09, x
LCF7B:	sty	$0D, x
	clv
	.byte	$0C
	cpx	$09
	cpx	$07
LCF83:	ora	#$2B
	.byte	$0C
	cpx	$09
	cpx	$0D
	asl	$0C3B
	.byte	$54
	ora	#$54
	ora	#$6B
	.byte	$0C
	sty	$09, x
	sty	$07, x
	ora	$37, x
	.byte	$0C
	.byte	$F3
	ora	#$F3
	ora	($01, x)
	.byte	$07
	.byte	$02
	tay
	asl	$B6
	asl	$CB
	ora	$15
	sec
	.byte	$03
	.byte	$DC
	.byte	$07
	asl	$43
	ora	$0659
	.byte	$AB
	ora	($01, x)
	.byte	$07
	asl	$21
	.byte	$14
	ror	$14
	.byte	$8B
	.byte	$07
	.byte	$2F
	.byte	$87
	.byte	$2F
	.byte	$D7
	and	$05D9
	.byte	$2F
	cpx	#$2F
	.byte	$63
	ora	($01, x)
	.byte	$07
	eor	($9A, x)
	.byte	$1C
	cmp	$42
LCFCF:	dec	$01
	ora	$2F
	lda	LC32F, y
	ora	($03, x)
	asl	$F5
	ora	$06
	cli
	asl	$E8
	.byte	$03
	asl	$0174
	ora	($07, x)
	ora	($99, x)
	ora	($D9, x)
	ora	$F9, x
	.byte	$07
	asl	$0E77
	.byte	$A7
	asl	$03C7
	asl	$D9, x
	ora	($05, x)
	.byte	$23
	.byte	$1C
	.byte	$23
	ldy	$2303, x
	.byte	$5B
	ora	($01, x)
	ora	$2F
LD002:	sta	$EC2F, y
	ora	$2F
	bit	$2F
	adc	#$05
	and	$2D96
	.byte	$E3
	.byte	$03
	and	$033A
	.byte	$1C
	.byte	$B7
	ora	($05, x)
	.byte	$1C
	.byte	$B7
	.byte	$3B
	clv
	ora	($01, x)
	ora	($07, x)
	and	$2EB8
	.byte	$F3
	bit	$09FC
	and	$2D34
	.byte	$74
	and	$2EA4
	.byte	$AB
	.byte	$07
	rol	$2D1A
	adc	$F52E, y
	ora	($01, x)
	ora	#$2D
	.byte	$23
	and	$2D83
	.byte	$E3
	.byte	$2F
	cpy	$2D05
	.byte	$BB
	and	$03FB
	and	$0953
	.byte	$2F
	and	#$2C
	.byte	$43
	and	$2FA3
	eor	$0101, y
	ora	($01, x)
	ora	($05, x)
	.byte	$42
	eor	#$2B
	.byte	$B7
	ora	($01, x)
	ora	($05, x)
	pha
	.byte	$1A
	ror	a
	.byte	$A7
	ora	($03, x)
	and	$078A
	bit	$2F73
	sta	$2D
	sty	$2F05
	.byte	$87
	and	$058E
	.byte	$2F
	bvc	LD0A5
	ldy	#$01
	ora	($01, x)
	ora	$612F
	.byte	$2F
	lda	($2D, x)
	.byte	$A3
	.byte	$2F
	pla
	and	$2D5A
	tax
	ora	$2D
	adc	($2D, x)
	lda	($01, x)
	ora	($09, x)
	.byte	$03
	ror	$07, x
	adc	#$07
	sta	$8F2E, y
	ora	#$2D
	ldx	#$2F
	.byte	$73
	.byte	$2F
	pla
	bit	$07AD
	.byte	$2F
	.byte	$80
	.byte	$2F
LD0A5:	.byte	$97
	bit	$057F
	.byte	$07
	.byte	$80
	ora	($84, x)
	ora	($05, x)
	.byte	$2F
	.byte	$9C
	rol	$077E
	and	$2E35
	.byte	$77
	.byte	$2F
	tay
	ora	$2F
	.byte	$67
	.byte	$2F
	tsx
	ora	($09, x)
	rol	$2F42
	.byte	$82
	rol	$2DB2
	.byte	$74
	ora	#$2D
	.byte	$53
	and	$2E93
	.byte	$54
	and	$0176
	.byte	$01
LD0D4:	ora	($01, x)
	.byte	$03
	and	($79, x)
	ora	$02
	jsr	LD002
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($08, x)
	dey
	dey
	dey
	clv
	.byte	$80
	brk
	rti

	.byte	$9C
