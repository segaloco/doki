.segment	"CHAPT_05DATA"

L0000           := $0000
L0188           := $0188
L0D00           := $0D00
L0F7C           := $0F7C
L14E0           := $14E0
L2614           := $2614
L320F           := $320F
L6CFA           := $6CFA
L6D6B           := $6D6B
L6D6C           := $6D6C
L8212           := $8212
L9839           := $9839
L9887           := $9887
L988A           := $988A
L995F           := $995F
L9967           := $9967
L9C4C           := $9C4C
L9D61           := $9D61
LA116           := $A116
LA2FC           := $A2FC
LA65E           := $A65E
LA662           := $A662
LA929           := $A929
LABB2           := $ABB2
LAC1F           := $AC1F
LACCC           := $ACCC
LAE56           := $AE56
LAED4           := $AED4
LAEE0           := $AEE0
LB01F           := $B01F
LB168           := $B168
LB178           := $B178
LB17D           := $B17D
LB1D6           := $B1D6
LB207           := $B207
LBE01           := $BE01
LBE0C           := $BE0C
LBE24           := $BE24
LBE38           := $BE38
        .byte   $0F
        bmi     LBF15
        ora     ($0F, x)
        bmi     LBF1D
        ora     ($0F, x)
        .byte   $27
        .byte   $17
        .byte   $07
        .byte   $0F
        .byte   $2B
        .byte   $1B
        .byte   $0B
        .byte   $0F
        bmi     LBF25
        ora     ($0F, x)
LBF15:  bmi     LBF2D
        .byte   $02
        .byte   $0F
        .byte   $27
        .byte   $17
        php
        .byte   $0F
LBF1D:  and     #$1A
        asl     a
        .byte   $0F
        and     ($12), y
        ora     ($0F, x)
LBF25:  bmi     LBF3D
        .byte   $02
        .byte   $0F
        .byte   $3C
        .byte   $1C
        .byte   $0C
        .byte   $0F
LBF2D:  rol     a
        .byte   $1A
        asl     a
        .byte   $07
        bmi     LBF5A
        .byte   $0F
        .byte   $07
        bmi     LBF4D
        .byte   $0F
        .byte   $07
        .byte   $27
        .byte   $17
        .byte   $0F
        .byte   $07
LBF3D:  and     ($01), y
        .byte   $0F
        ora     ($2A, x)
        .byte   $1A
        .byte   $0F
        ora     ($30, x)
        asl     $0F, x
        ora     ($3C, x)
        .byte   $1C
        .byte   $0F
        .byte   $01
LBF4D:  and     $15
        ora     $0C
        bmi     LBF67
        .byte   $0F
        .byte   $0C
        bmi     LBF6D
        .byte   $0F
        .byte   $0C
        .byte   $30
LBF5A:  bit     $0F
        .byte   $0C
        bmi     LBF93
        .byte   $0F
        ora     ($0F, x)
        .byte   $0F
        .byte   $0F
        ora     ($0F, x)
        .byte   $0F
LBF67:  .byte   $0F
        ora     ($0F, x)
        .byte   $0F
        .byte   $0F
        .byte   $01
LBF6D:  .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $FF
        bmi     LBF89
        .byte   $0F
        .byte   $FF
        sec
        bpl     LBF87
        .byte   $FF
        bmi     LBFA0
        .byte   $0F
        .byte   $FF
        bmi     LBF95
        .byte   $02
        .byte   $FF
        sec
        bpl     LBF86
        .byte   $FF
        .byte   $30
LBF86:  .byte   $25
LBF87:  .byte   $02
        .byte   $FF
LBF89:  bmi     LBFA1
        .byte   $0F
        .byte   $FF
        bmi     LBF9F
        .byte   $0F
        .byte   $FF
        asl     $30, x
LBF93:  .byte   $0F
        brk
LBF95:  asl     a
        .byte   $14
        lda     ($E9, x)
        sta     $9B
        .byte   $9B
        .byte   $9B
        .byte   $9B
        .byte   $9B
LBF9F:  .byte   $9B
LBFA0:  .byte   $9B
LBFA1:  .byte   $9B
        ldy     $9B53
        php
        rol     $6868, x
        pla
        pla
        pla
        adc     $6E21, y
        ora     ($75), y
        adc     $75, x
        adc     $75, x
        dex
        dex
        .byte   $CB
        .byte   $CB
        .byte   $CB
        .byte   $CB
        .byte   $CB
        .byte   $CB
        .byte   $CB
LBFBE:  .byte   $CB
        .byte   $CB
        .byte   $CB
        cpy     LCDCC
        cmp     LCDCD
        cmp     LCDCD
        cmp     LCECE
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        asl     $4A66
        .byte   $DA
        cmp     $C5
        dec     $C6
        brk
        sta     $40D2, y
        brk
        sta     $9999, y
        brk
        .byte   $9F
        .byte   $9F
        .byte   $9F
        brk
        lda     ($A1, x)
        lda     (L0000, x)
        .byte   $D3
        .byte   $9B
        .byte   $12
        brk
        .byte   $9F
        .byte   $9F
        sta     L0000, y
        brk
        brk
        brk
        brk
        brk
        brk
        brk
        .byte   $9C
        sta     a:$C6, x
        ora     $9F
        brk
        brk
        rti

        .byte   $A3
        brk
        brk
        brk
        lda     (L0000, x)
        brk
        brk
        .byte   $C2
        brk
LC00F:  brk
        brk
LC011:  .byte   $9F
        brk
LC013:  brk
        .byte   $93
LC015:  sta     a:$C6, x
        rti

        sta     LC0C6, x
        cpy     #$C0
        .byte   $27
        .byte   $3B
        .byte   $4F
LC021:  cpy     #$C0
LC023:  cpy     #$31
LC025:  eor     $59
LC027:  .byte   $CF
        .byte   $CF
LC029:  .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $9E
        tay
        cmp     ($DA), y
        .byte   $DA
        .byte   $DA
        .byte   $DA
        .byte   $DA
        .byte   $DA
        .byte   $DA
        .byte   $CF
        .byte   $CF
        bne     LC00F
        bne     LC011
        bne     LC013
        bne     LC015
        .byte   $DA
        .byte   $DF
        asl     $2F, x
        .byte   $3B
        .byte   $44
        bvc     LC09D
        bvc     LC09F
        bne     LC021
LC051:  bne     LC023
        bne     LC025
        bne     LC027
        bne     LC029
        bvc     LC0B0
        .byte   $72
        sty     $A2
        .byte   $C3
        .byte   $C3
        .byte   $C3
        .byte   $C3
        .byte   $C3
        ora     ($43, x)
        .byte   $80
        cpy     #$0C
        .byte   $5F
        sta     ($CA), y
        .byte   $12
        .byte   $67
        tya
        .byte   $D2
        .byte   $67
        .byte   $6B
        asl     $20
        ora     $7374, x
        .byte   $C7
        .byte   $73
        .byte   $C7
        .byte   $73
        .byte   $C7
        .byte   $73
        .byte   $C7
        .byte   $73
        .byte   $C7
        .byte   $FA
        .byte   $C7
        .byte   $FA
        .byte   $C7
        .byte   $FA
        .byte   $C7
        .byte   $FA
        .byte   $C7
        bcc     LC051
        .byte   $4B
        iny
        .byte   $4B
        iny
        ror     $20CA
        ora     $2D74, x
        iny
        and     $2DC8
        iny
        and     $2DC8
        iny
        .byte   $2D
LC09D:  iny
        .byte   $AE
LC09F:  cmp     #$AE
        cmp     #$AE
        cmp     #$87
        .byte   $C7
        .byte   $87
        .byte   $C7
        .byte   $87
        .byte   $C7
        dec     $C9, x
        dec     $C9, x
        and     #$C9
LC0B0:  .byte   $54
        cmp     #$AD
        asl     $2005
        ora     $9874, x
        cmp     #$A4
        cmp     #$FC
        cmp     #$B3
        .byte   $C7
        dec     $C7, x
        .byte   $B3
        .byte   $C7
        .byte   $27
        dex
LC0C6:  .byte   $27
        dex
        .byte   $27
        dex
        .byte   $34
        dex
        .byte   $34
        dex
        and     #$C9
        .byte   $27
        dex
        .byte   $27
        dex
        .byte   $27
        dex
        .byte   $27
        dex
        jmp     LCA27

        txa
        pha
        and     #$F0
        sta     $33
        txa
        asl     a
        asl     a
        asl     a
        asl     a
        sta     $29
        lda     #$0A
        sta     $15
        ldx     #$00
        stx     $12
        stx     $1F
        lda     #$3D
        sta     $90
        lda     #$01
        sta     $51
        tya
        pha
        jsr     LA116
        pla
        sta     $79
        lda     #$FF
        sta     $0441, x
        pla
        tax
        rts

        and     $2D2F
        .byte   $2F
        cpx     #$E2
        cpx     $E6
        cpx     #$E2
        cpx     $E6
        inx
        nop
        .byte   $EC
        .byte   $EE
LC11A:  inx
        nop
        cpx     $01EE
        .byte   $03
        ora     #$05
        .byte   $07
        .byte   $0B
        ora     $150F
        ora     ($13), y
        .byte   $17
        ora     ($03, x)
        ora     #$05
        ora     $011B, y
        .byte   $03
        ora     #$05
        ora     $1D1B, y
        .byte   $1F
        and     $21
        .byte   $23
        .byte   $27
        ora     $251F, x
        and     ($23, x)
        .byte   $27
        .byte   $9C
        .byte   $9E
        .byte   $9C
        .byte   $9E
        bne     LC11A
        .byte   $D4
        dec     $F0, x
        .byte   $F2
        .byte   $F4
        inc     $F0, x
        .byte   $F2
        sed
        .byte   $FA
LC152:  brk
        ora     $EB, x
        brk
LC156:  brk
        ora     ($FF, x)
        brk
        jsr     LB207
        lda     $10
        and     #$03
        bne     LC16D
        dec     $B1, x
        bne     LC16D
        sta     $04B2
        jmp     L9D61

LC16D:  lda     $04B2
        beq     LC180
        lda     $46
        bpl     LC180
        lda     #$00
        sta     $47, x
        sta     $04B2
        jmp     LC343

LC180:  lda     $5B, x
        and     #$20
        sta     $04B2
        bne     LC18C
        jmp     LC21B

LC18C:  lda     $3D, x
        beq     LC196
        lda     $6F, x
        and     #$01
        sta     $9D
LC196:  lda     $33, x
        sec
        sbc     #$1A
        sta     $32
        lda     $1F, x
        sbc     #$00
        sta     $1E
        lda     $28
        sec
        sbc     #$08
        sta     $29, x
        lda     $14
        sbc     #$00
        sta     $15, x
        ldy     #$01
        lda     $3D, x
        bmi     LC1B8
        ldy     #$FF
LC1B8:  sty     LC156
        lda     $F7
        and     #$03
        tay
        and     a:$5A
        bne     LC1D7
        lda     LC152, y
        cmp     $3D, x
        beq     LC1D4
        lda     $3D, x
        clc
        adc     LC156, y
        sta     $3D, x
LC1D4:  jmp     LC1DB

LC1D7:  lda     #$00
        sta     $3D, x
LC1DB:  ldy     #$01
        lda     $47, x
        bmi     LC1E3
        ldy     #$FF
LC1E3:  sty     LC156
        lda     #$20
        cmp     $042C
        lda     #$00
        rol     a
        asl     a
        asl     a
        asl     a
        and     $F7
        bne     LC208
        lda     $5B, x
        lsr     a
        lsr     a
        and     #$03
        sta     L0000
        lda     $F7
        lsr     a
        lsr     a
        and     #$03
        tay
        and     L0000
        beq     LC20C
LC208:  lda     #$00
        beq     LC219
LC20C:  lda     LC152, y
        cmp     $47, x
        beq     LC21B
        lda     $47, x
        clc
        adc     LC156, y
LC219:  sta     $47, x
LC21B:  jsr     LB17D
        jsr     LB178
        lda     $B1, x
        cmp     #$20
        bcs     LC22E
        lda     $10
        and     #$02
        bne     LC22E
        rts

LC22E:  jmp     LC343

        jsr     LA65E
        bmi     LC269
        ldx     L0000
        ldy     $12
        lda     #$00
        sta     $3D, x
        sta     $47, x
        lda     #$3F
        sta     $90, x
        lda     $29, y
        sec
        sbc     #$08
        sta     $29, x
        lda     $15, y
        sbc     #$00
        sta     $15, x
        lda     $33, y
        clc
        adc     #$0E
        sta     $33, x
        lda     $1F, y
        adc     #$00
        sta     $1F, x
        jsr     L9839
        lda     #$A0
        sta     $B1, x
LC269:  ldx     $12
        rts

LC26C:  .byte   $02
        .byte   $02
        ora     ($01, x)
LC270:  .byte   $33
        .byte   $3B
        .byte   $3B
        .byte   $33
LC274:  ora     ($FF, x)
LC276:  php
        sed
LC278:  ora     ($FF, x)
LC27A:  .byte   $20
        .byte   $E0
LC27C:  .byte   $14
        cpx     LCC20
        ldy     $9FF6
        lda     $042F, x
        beq     LC294
        lda     $65, x
        ora     #$80
        sta     $65, x
        jsr     LC2F9
        jmp     L995F

LC294:  jsr     LAC1F
        lda     $B1, x
        beq     LC2B2
        dec     $47, x
        bpl     LC2AF
        lda     $33, x
        cmp     #$30
        bcs     LC2AF
        lda     #$00
        sta     $B1, x
        sta     $3D, x
        sta     $47, x
        dec     $86, x
LC2AF:  jmp     LC2F3

LC2B2:  lda     $86, x
        bne     LC2C7
        lda     #$30
        sta     $47, x
        jsr     LB168
        lda     LC27C, y
        sta     $3D, x
        inc     $B1, x
        jmp     LC2F9

LC2C7:  lda     $0480, x
        and     #$01
        tay
        lda     $47, x
        clc
        adc     LC274, y
        sta     $47, x
        cmp     LC276, y
        bne     LC2DD
        inc     $0480, x
LC2DD:  lda     $0477, x
        and     #$01
        tay
        lda     $3D, x
        clc
        adc     LC278, y
        sta     $3D, x
        cmp     LC27A, y
        bne     LC2F3
        inc     $0477, x
LC2F3:  jsr     LB178
        jsr     LB17D
LC2F9:  jsr     LAED4
        lda     $51, x
        sec
        sbc     #$01
        ora     $042F, x
        ora     $A8, x
        bne     LC342
        jsr     LABB2
        sty     a:$F4
        lda     #$45
        sta     $65, x
        lda     $29, x
        pha
        sec
        sbc     #$08
        sta     $29, x
        lda     $15, x
        pha
        sbc     #$00
        sta     $15, x
        jsr     L9C4C
        pla
        sta     $15, x
        pla
        sta     $29, x
        lda     $042C
        clc
        adc     #$0C
        sta     $042C
        lda     $0429
        sbc     #$07
        sta     $0429
        jsr     LC343
        lda     #$0D
        sta     $65, x
LC342:  rts

LC343:  lda     $10
        lsr     a
        lsr     a
        lsr     a
        and     #$03
        ldy     $3D, x
        bmi     LC350
        eor     #$03
LC350:  tay
        lda     LC26C, y
        sta     $6F, x
        lda     LC270, y
        jmp     LAEE0

        jsr     LA2FC
        lda     #$05
        sta     $0465, x
        rts

        jsr     LACCC
        lda     $045C, x
        beq     LC372
        inc     $9F, x
        jmp     LAE56

LC372:  jsr     LB207
        lda     #$02
        sta     $6F, x
        jsr     LAE56
        lda     $5B, x
        and     #$04
        beq     LC3E1
        jsr     LA929
        lda     $10
        and     #$FF
        bne     LC391
        lda     #$D8
        sta     $47, x
        bne     LC3E1
LC391:  lda     $10
        and     #$3F
        bne     LC39B
        lda     #$20
        sta     $86, x
LC39B:  ldy     $86, x
        bne     LC3BA
        inc     $B1, x
        lda     $B1, x
        and     #$20
        beq     LC3E0
        inc     $9F, x
        inc     $9F, x
        ldy     #$18
        lda     $B1, x
        and     #$40
        bne     LC3B5
        ldy     #$E8
LC3B5:  sty     $3D, x
        jmp     LB17D

LC3BA:  cpy     #$10
        bne     LC3E0
        jsr     LA65E
        bmi     LC3E0
        ldx     L0000
        lda     #$35
        sta     $90, x
        lda     #$E0
        sta     $47, x
        lda     $33, x
        adc     #$03
        sta     $33, x
        jsr     L9839
        lda     #$FF
        sta     $86, x
        lda     #$E0
        sta     $3D, x
        ldx     $12
LC3E0:  rts

LC3E1:  jmp     L9967

        lda     $51, x
        cmp     #$01
        bne     LC3F3
        lda     $045C, x
        beq     LC3FC
        lda     #$4A
        sta     $65, x
LC3F3:  lda     #$B3
        sta     $046E, x
        lda     #$5B
        bne     LC405
LC3FC:  ldy     $86, x
        dey
        cpy     #$10
        bcs     LC40B
        lda     #$4F
LC405:  jsr     LAEE0
        jmp     LC432

LC40B:  jsr     LAED4
        lda     $86, x
        cmp     #$10
        bcc     LC432
        lda     #$01
        sta     $65, x
        lda     #$10
LC41A:  sta     $046E, x
        lda     $0429
        clc
        adc     #$0B
        sta     $0429
        asl     $EE
        ldy     #$00
        sty     a:$F4
        lda     #$67
        jsr     LAEE0
LC432:  lda     #$43
        sta     $65, x
        lda     #$33
        sta     $046E, x
LC43B:  rts

        .byte   $FB
        ora     $20
        cmp     $A5AE, x
        inc     $0E29
        ora     $EF
        ora     $B1, x
        bne     LC466
        lda     $33, x
        sec
        sbc     #$02
        sta     L0000
        ldy     $6F, x
        lda     $01
        clc
        adc     LC43B, y
        sta     $01
        jsr     LABB2
        ldx     #$6B
        jsr     LB01F
        ldx     $12
LC466:  rts

        lda     $B1, x
        bne     LC4C5
        lda     $A8, x
        beq     LC4A7
        lda     #$01
        sta     $90, x
        jsr     L9839
        jsr     LA662
        bmi     LC4A6
        ldy     L0000
        lda     #$08
        sta     $90, y
        sta     $B1, y
        lda     $29, x
        sta     $29, y
        lda     $15, x
        sta     $15, y
        lda     $0441, x
        sta     $0441, y
        lda     #$FF
        sta     $0441, x
        lda     $3D, x
        sta     $3D, y
        tya
        tax
        jsr     L9839
        ldx     $12
LC4A6:  rts

LC4A7:  lda     $5B, x
        and     #$10
        beq     LC4C5
        inc     $B1, x
        sta     $9F, x
        jsr     LA662
        bmi     LC4C5
        ldy     L0000
        lda     $3D, x
        sta     $3D, y
        lda     #$20
        sta     $0453, y
        jmp     LC4CB

LC4C5:  jsr     LAC1F
        jsr     LACCC
LC4CB:  jsr     LB207
        lda     $5B, x
        and     $6F, x
        beq     LC4D7
        jsr     LB1D6
LC4D7:  lda     $5B, x
        and     #$04
        beq     LC4F7
        lda     $042F, x
        beq     LC4EA
        lda     #$00
        sta     $042F, x
        jsr     L9887
LC4EA:  lda     $9F, x
        eor     #$08
        sta     $9F, x
        jsr     LA929
        lda     #$F0
        sta     $47, x
LC4F7:  inc     $0477, x
        lda     $B1, x
        bne     LC508
        lda     $0477, x
        and     #$3F
        bne     LC508
        jsr     L988A
LC508:  jsr     L995F
        jmp     LAE56

        inc     $FEFE, x
        inc     $B6B4, x
        lda     $B7, x
        clv
        .byte   $FA
        lda     $FAFA, y
        .byte   $FA
LC51C:  .byte   $B2
        .byte   $B3
        ldx     LBFBE, y
        .byte   $BF
        .byte   $BF
        .byte   $BF
        .byte   $BF
        .byte   $BF
        lsr     a
LC527:  lsr     a
        .byte   $4B
        .byte   $4B
        .byte   $5E
LC52B:  .byte   $5F
        lsr     $A85F, x
LC52F:  tay
        lda     #$A9
        .byte   $A9
LC533:  lda     #$A9
        lda     #$45
LC537:  eor     $A9
        lda     #$A8
LC53B:  eor     $A9A9, x
        .byte   $74
LC53F:  ror     $75, x
        .byte   $77
        tya
LC543:  txs
        sta     $9C9B, y
LC547:  txs
        sta     $9C9B, x
LC54B:  .byte   $9E
        .byte   $9B
        .byte   $9F
        cli
LC54F:  .byte   $5A
        eor     $5E5B, y
        .byte   $5F
        lsr     $8E5F, x
        .byte   $8F
        .byte   $8F
        stx     $A6A6
        .byte   $A7
        .byte   $A7
        .byte   $92
        .byte   $93
        .byte   $93
        .byte   $92
        .byte   $74
        ror     $75, x
        .byte   $77
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        bcs     LC527
        .byte   $FA
        .byte   $FA
        bcs     LC52B
        .byte   $FA
        .byte   $FA
        bcs     LC52F
        .byte   $FA
        .byte   $FA
        bcs     LC533
        .byte   $FA
        .byte   $FA
        bcs     LC537
        .byte   $FA
        .byte   $FA
        bcs     LC53B
        .byte   $FA
        .byte   $FA
        bcs     LC53F
        .byte   $FA
        .byte   $FA
        bcs     LC543
        .byte   $FA
        .byte   $FA
        bcs     LC547
        .byte   $FA
        .byte   $FA
        bcs     LC54B
        .byte   $FA
        .byte   $FA
        bcs     LC54F
        ldy     #$A2
        lda     ($A3, x)
        .byte   $80
        .byte   $82
        sta     ($83, x)
        .byte   $F4
        stx     $F5
        .byte   $87
        sty     $86
        sta     $87
        .byte   $FC
        .byte   $FC
        .byte   $FC
        .byte   $FC
        lda     $ACFB
        lda     $ACAC
        ldy     $FBAC
        .byte   $3B
        .byte   $3B
        ldy     $FCFC
        .byte   $FC
        .byte   $FC
        .byte   $F4
        stx     $F5
        .byte   $87
        inc     $FEFE, x
        inc     $FEFE, x
        ora     $3CFE, x
        rol     $3F3D, x
        cli
        sbc     $5A59, x
        .byte   $5B
        .byte   $5A
        sbc     $5BFD, x
        .byte   $5C
        sbc     $FD5D, x
        sbc     $5A5B, x
        .byte   $1C
        inc     $FEFE, x
        inc     $FEFE, x
        inc     $1EFE, x
        inc     $201F, x
        .byte   $22
        and     ($23, x)
        .byte   $57
        .byte   $57
        .byte   $FB
        .byte   $FB
        .byte   $57
        .byte   $57
        inc     $ABFE, x
        .byte   $AB
        .byte   $FB
        .byte   $FB
        nop
        nop
        .byte   $FB
        .byte   $FB
        .byte   $7C
        ror     $7F7D, x
        dex
        cpy     LCDCB
        dex
        cpy     LCDCB
        cpy     #$C2
        cmp     ($C3, x)
        bit     $2D2E
        .byte   $2F
        stx     $8F8F
        stx     $8A88
        .byte   $89
        .byte   $8B
        .byte   $89
        .byte   $8B
        .byte   $89
        .byte   $8B
        .byte   $89
        .byte   $8B
        sty     $888D
        txa
        sty     $888D
        txa
        .byte   $89
        .byte   $8B
        dey
        txa
        .byte   $89
        .byte   $8B
        ror     a
        jmp     (L6D6B)

        jmp     (L6D6C)

        adc     $6E6C
        adc     $6C6F
        .byte   $54
        adc     $3255
        .byte   $34
        .byte   $33
        and     $33, x
        and     $33, x
        and     $94, x
        sta     $94, x
        sta     $96, x
        .byte   $97
        stx     $97, y
        pha
        eor     #$48
        eor     #$FE
        inc     $FEFE, x
        .byte   $FB
        .byte   $32
        .byte   $32
        .byte   $33
        .byte   $33
        .byte   $33
        .byte   $33
        .byte   $33
        sbc     $FDFD, x
        sbc     $FB34, x
LC668:  sbc     $FB34, x
        bmi     LC668
        .byte   $FB
        .byte   $FB
        .byte   $FB
        and     ($FB), y
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        lsr     $56, x
        lsr     $56, x
        .byte   $64
        ror     $65
        .byte   $67
        pla
        ror     a
        adc     #$6B
        .byte   $FA
        jmp     (L6CFA)

        adc     $6DFA
        .byte   $FA
        .byte   $92
        .byte   $93
        .byte   $93
        .byte   $92
        ldx     $AEAF
        .byte   $AF
        sei
        .byte   $7A
        adc     $A87B, y
        tay
        .byte   $AF
        ldx     $9594
        sty     $95, x
        stx     $97, y
        stx     $97, y
        .byte   $22
        bit     $23
        and     $92
        .byte   $93
        .byte   $93
        .byte   $92
        bvc     LC6FD
        bvc     LC6FF
        ldx     $AEAF
        .byte   $AF
        bvc     LC705
        bvc     LC707
        stx     $8F8F
        stx     $5250
        eor     ($53), y
        sbc     $FDFD, x
        sbc     $36FB, x
        rol     $4F, x
        .byte   $4F
        lsr     $4F4E
        lsr     $4F4F
        lsr     $9392
        .byte   $93
        .byte   $92
        stx     $8F8F
        stx     $4544
        eor     $44
        rti

        .byte   $42
        eor     ($43, x)
        rti

        .byte   $42
        eor     ($43, x)
        tsx
        ldy     $BDBB, x
        tsx
        ldy     $9190, x
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        sbc     $FDFD, x
        sbc     $6361, x
        adc     ($63, x)
        adc     $63
        .byte   $65
LC6FD:  .byte   $63
        .byte   $65
LC6FF:  .byte   $67
        adc     $67
        rts

        .byte   $62
        .byte   $61
LC705:  .byte   $63
        .byte   $64
LC707:  .byte   $62
        adc     $63
        .byte   $64
        ror     $65
        .byte   $67
        pla
        .byte   $62
        adc     ($63, x)
        .byte   $64
        adc     #$65
        .byte   $67
        lsr     $62
        adc     ($63, x)
        .byte   $64
        .byte   $47
        adc     $67
        tsx
        ldy     $BDBB, x
        bvs     LC796
        adc     ($73), y
        stx     $8F8F
        stx     $4544
        eor     $44
LC72E:  .byte   $97
        .byte   $92
        .byte   $0C
        .byte   $0C
        ora     $99, x
        ora     $15, x
        eor     $45
        eor     $45
        .byte   $63
        .byte   $63
        .byte   $63
        .byte   $63
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $9F
        .byte   $A3
        .byte   $9C
        .byte   $12
        .byte   $9F
        .byte   $9F
        .byte   $9F
        .byte   $9F
        .byte   $80
        .byte   $07
        sta     ($80, x)
        sta     ($81, x)
        sta     ($81, x)
LC752:  lda     $050E
        asl     a
        asl     a
        sta     $0F
        lda     $050E
        cmp     #$07
        bcc     LC766
        lda     $0543
        jmp     LC769

LC766:  lda     $0542
LC769:  clc
        adc     $0F
        tax
        lda     LC72E, x
        sta     ($01), y
        rts

        ldy     $E7
LC775:  jsr     LC752
        jsr     LBE0C
        dec     $050D
        bpl     LC775
        rts

LC781:  .byte   $4F
        .byte   $4F
        .byte   $83
LC784:  bvc     LC7D7
        .byte   $83
        ldy     $E7
        lda     $050E
        cmp     #$09
        bne     LC79B
        lda     $04B0
        beq     LC79B
        .byte   $EE
LC796:  asl     $EE05
        .byte   $0E
        .byte   $05
LC79B:  lda     $050E
        sec
        sbc     #$09
        tax
        lda     LC781, x
        sta     ($01), y
        jsr     LBE24
        lda     LC784, x
        sta     ($01), y
        jsr     LBE38
        rts

        ldy     $E7
        lda     #$52
        sta     ($01), y
        iny
        lda     #$53
        sta     ($01), y
        lda     $E7
        clc
        adc     #$10
        tay
        lda     #$52
        sta     ($01), y
        iny
        lda     #$54
        sta     ($01), y
        iny
        lda     #$53
        sta     ($01), y
        jsr     LBE38
        rts

        .byte   $A4
LC7D7:  .byte   $E7
        lda     #$52
        sta     ($01), y
        dey
        lda     #$55
        sta     ($01), y
        ldy     $E7
        tya
        clc
        adc     #$10
        tay
        lda     #$52
        sta     ($01), y
        dey
        lda     #$54
        sta     ($01), y
        dey
        lda     #$55
        sta     ($01), y
        jsr     LBE38
        rts

        ldy     $E7
        lda     $050E
        cmp     #$06
        bne     LC80A
        lda     #$9E
        sta     ($01), y
        jmp     LC80D

LC80A:  jsr     LC752
LC80D:  jsr     LBE24
        dec     $050D
        bpl     LC80A
        rts

        ldy     $E7
LC818:  jsr     LC752
        tya
        clc
        adc     #$11
        tay
        dec     $050D
        bpl     LC818
        rts

LC826:  .byte   $67
        ror     a
        .byte   $9C
        cmp     ($70), y
        sty     $95, x
        lda     $050E
        tax
        cmp     #$05
        bne     LC83B
        lda     $0543
        beq     LC83B
        inx
LC83B:  ldy     $E7
        lda     LC826, x
        sta     ($01), y
        rts

LC843:  .byte   $73
        .byte   $0D
        .byte   $74
LC846:  .byte   $74
        .byte   $0E
LC848:  adc     $0F, x
LC84A:  ror     $A4, x
        .byte   $E7
        lda     $050E
        sec
        sbc     #$0A
        tax
        jsr     LC87C
        lda     LC843, x
        sta     ($01), y
        ldx     $07
        dec     $050D
        beq     LC870
LC863:  jsr     LBE0C
        lda     LC846, x
        sta     ($01), y
        dec     $050D
        bne     LC863
LC870:  jsr     LBE0C
        jsr     LC87C
        lda     LC848, x
        sta     ($01), y
        rts

LC87C:  stx     $07
        lda     ($01), y
        cmp     #$40
        beq     LC886
        ldx     #$02
LC886:  rts

LC887:  dex
        .byte   $CB
        cpy     LC8C7
        cmp     #$40
        .byte   $04
        ora     $A2
        brk
LC892:  stx     $0B
        ldx     $E8
        jsr     LBE01
        ldx     #$05
        ldy     $E7
        ldx     $0B
        ldy     $E7
        lda     $050D
        sta     $07
        jsr     LC8D7
        inx
        lda     $07
        beq     LC8B6
LC8AE:  jsr     LBE0C
        jsr     LC8D7
        bne     LC8AE
LC8B6:  jsr     LBE0C
        inx
        jsr     LC8D7
        lda     $E7
        clc
        adc     #$10
        cmp     #$F0
        bcs     LC8CD
        .byte   $A2
LC8C7:  .byte   $03
        sta     $E7
        jmp     LC892

LC8CD:  rts

LC8CE:  .byte   $C7
        iny
        .byte   $C9
LC8D1:  .byte   $CF
        .byte   $CD
        .byte   $CD
LC8D4:  dec     $D0CE
LC8D7:  stx     $08
        txa
        bne     LC8EF
        ldx     #$02
        lda     ($01), y
LC8E0:  cmp     LC8CE, x
        beq     LC8EA
        dex
        bpl     LC8E0
        bmi     LC908
LC8EA:  lda     LC8D1, x
        bne     LC91E
LC8EF:  ldx     $08
        cpx     #$02
        bne     LC908
        ldx     #$02
        lda     ($01), y
LC8F9:  cmp     LC8CE, x
        beq     LC903
        dex
        bpl     LC8F9
        bmi     LC908
LC903:  lda     LC8D4, x
        bne     LC91E
LC908:  ldx     #$08
LC90A:  lda     ($01), y
        cmp     LC887, x
        beq     LC919
        dex
        bpl     LC90A
        ldx     $08
        jmp     LC920

LC919:  ldx     $08
        lda     LC887, x
LC91E:  sta     ($01), y
LC920:  ldx     $08
        dec     $07
        rts

LC925:  cpy     #$77
LC927:  .byte   $82
        sei
        ldx     #$00
        lda     $050E
        cmp     #$0E
        beq     LC933
        inx
LC933:  stx     $07
        ldy     $E7
        lda     LC925, x
        sta     ($01), y
LC93C:  jsr     LBE24
        lda     ($01), y
        cmp     #$40
        bne     LC950
        ldx     $07
        lda     LC927, x
        sta     ($01), y
        cpy     #$E0
        bcc     LC93C
LC950:  rts

LC951:  eor     $5B5C, x
LC954:  lda     #$04
        sta     $07
        ldy     $E7
        ldx     $E8
        jsr     LBE01
        ldx     #$02
        lda     ($01), y
        cmp     #$40
        bne     LC984
LC967:  lda     LC951, x
        sta     ($01), y
        jsr     LBE0C
        dex
        cpx     #$01
        bne     LC977
        jsr     LC985
LC977:  dex
        bpl     LC967
        ldy     $E7
        jsr     LBE24
        sty     $E7
        jmp     LC954

LC984:  rts

LC985:  lda     #$5C
        sta     ($01), y
        jsr     LBE0C
        lda     #$5E
        sta     ($01), y
        jsr     LBE0C
        dec     $07
        bpl     LC985
        rts

        ldy     $E7
        lda     #$01
        sta     ($01), y
        iny
        lda     #$02
        sta     ($01), y
        rts

        ldy     $E7
        lda     #$03
        sta     ($01), y
        rts

LC9AB:  adc     $7271
        ldy     $E7
        lda     $050E
        sec
        sbc     #$06
        tax
        lda     LC9AB, x
        sta     ($01), y
LC9BC:  jsr     LBE24
        lda     ($01), y
        cmp     #$40
        bne     LC9CC
        lda     #$6E
        sta     ($01), y
        jmp     LC9BC

LC9CC:  tya
        sec
        sbc     #$10
        tay
        lda     #$6F
        sta     ($01), y
        rts

LC9D6:  ldy     $E7
        lda     $050E
        cmp     #$0D
        beq     LC9E6
        lda     #$C1
        sta     ($01), y
LC9E3:  jsr     LBE24
LC9E6:  lda     ($01), y
        cmp     #$40
        bne     LC9FB
        lda     #$C2
        sta     ($01), y
        lda     $EC
        beq     LC9F8
        cpy     #$E0
        bcs     LC9FB
LC9F8:  jmp     LC9E3

LC9FB:  rts

        ldy     $E7
        lda     #$C3
        sta     ($01), y
LCA02:  tya
        sec
        sbc     #$10
        tay
        cmp     #$F0
        bcs     LCA18
        lda     ($01), y
        cmp     #$40
        bne     LCA18
        lda     #$C2
        sta     ($01), y
        jmp     LCA02

LCA18:  rts

LCA19:  .byte   $43
        .byte   $44
        eor     $46
        .byte   $47
        pha
        eor     #$4A
        .byte   $4B
        lsr     $414C
        .byte   $5A
        .byte   $42
LCA27:  ldy     $E7
        ldx     $050E
        lda     LCA19, x
        sta     ($01), y
        rts

LCA32:  bcc     LC9D6
LCA34:  lda     $050E
        sec
        sbc     #$09
        sta     $08
        ldy     $E7
        ldx     $E8
        jsr     LBE01
        ldy     $E7
        lda     #$05
        sta     $07
        lda     ($01), y
        cmp     #$40
        bne     LCA6B
LCA4F:  ldx     $08
LCA51:  lda     LCA32, x
        sta     ($01), y
        jsr     LBE0C
        dec     $07
        bpl     LCA4F
        lda     $E7
        clc
        adc     #$10
        cmp     #$F0
        bcs     LCA6B
        sta     $E7
        jmp     LCA34

LCA6B:  rts

LCA6C:  .byte   $04
        ora     $A9
        brk
        sta     $08
LCA72:  ldy     $E7
        ldx     $E8
        jsr     LBE01
        ldy     $E7
        lda     $050D
        sta     $07
        ldx     $08
LCA82:  lda     LCA6C, x
        sta     ($01), y
        jsr     LBE0C
        dec     $07
        bpl     LCA82
        lda     #$01
        sta     $08
        lda     $E7
        clc
        adc     #$10
        cmp     #$F0
        bcs     LCAA0
        sta     $E7
        jmp     LCA72

LCAA0:  rts

        sta     ($E2, x)
        jsr     L0000
        stx     $01
        stx     $02
        stx     $1B
        bpl     LCAD3
        asl     LC51C
        inx
        asl     LC84A
        .byte   $3C
        .byte   $0B
        .byte   $0C
        bpl     LCAE5
        .byte   $82
        .byte   $0C
        .byte   $82
        ora     $0E82
        .byte   $82
        .byte   $0F
        .byte   $82
        sbc     ($48), y
        eor     ($10, x)
        asl     $1310
        ora     ($53), y
        iny
        bvc     LCA51
        ora     ($82, x)
        .byte   $04
        .byte   $82
LCAD3:  ora     $82
        asl     $82
        ora     #$82
        asl     a
        .byte   $82
        .byte   $0B
        .byte   $82
        .byte   $0C
        .byte   $82
        ora     $F482
        .byte   $F2
        .byte   $3C
        .byte   $0E
LCAE5:  .byte   $F2
        .byte   $37
        asl     $91FF
        nop
        .byte   $80
        ora     ($83), y
        .byte   $13
        .byte   $0C
        ora     ($1B, x)
        rol     a
        ora     $3E26
        .byte   $54
        beq     LCB6A
        sbc     ($D4), y
        .byte   $73
        .byte   $FC
        sec
        .byte   $87
        ora     #$87
        asl     a
        .byte   $87
        and     $84, x
        asl     $84
        .byte   $07
        sty     $F0
        pla
        .byte   $90
LCB0C:  .byte   $FF
        .byte   $F2
        rti

        .byte   $FF
        .byte   $62
        sty     $03
        sty     $04
        sty     $05
        sty     $06
        sty     $07
        sty     $90
        .byte   $FF
        and     $0E88
        dey
        .byte   $0F
        dey
        bit     $86
        ora     $86
        asl     $86
        .byte   $07
        stx     $B0
        .byte   $FF
        jsr     L0188
        dey
        .byte   $4F
        sty     $79
        .byte   $2B
        ora     ($27), y
        .byte   $02
        rol     $10
        .byte   $FF
        brk
        sta     ($01, x)
        sta     ($02, x)
        sta     ($2D, x)
        dey
        .byte   $0E
LCB45:  dey
        .byte   $0F
        dey
        rti

LCB49:  sty     $01
        sty     $02
        sty     $03
        sty     $26
        .byte   $82
        .byte   $07
        .byte   $82
        php
        .byte   $82
        ora     #$82
LCB58:  .byte   $70
LCB59:  .byte   $FF
        .byte   $23
        dey
        .byte   $04
        dey
        ora     $88
        txs
        and     a:$36
        asl     a
        brk
        bpl     LCB58
        .byte   $32
        .byte   $80
LCB6A:  .byte   $03
        .byte   $82
        .byte   $04
        .byte   $80
        php
        sta     ($4E, x)
        .byte   $0B
LCB72:  .byte   $0C
        jsr     L2614
        asl     $53
        beq     LCBAE
        beq     LCB49
        beq     LCB72
        sbc     ($4D), y
        sbc     ($74), y
        sbc     ($92), y
        .byte   $FF
        sta     ($F0), y
        .byte   $13
        .byte   $12
        .byte   $6B
        .byte   $32
        eor     ($0B), y
        .byte   $0C
        clc
        cmp     (L0000), y
        beq     LCBA2
        beq     LCB45
        sbc     ($AA), y
        sbc     $0D, x
        brk
        .byte   $FF
        sta     ($EA), y
        bpl     LCBB0
        beq     LCBFC
        .byte   $F2
LCBA2:  beq     LCBB5
        sbc     ($AA), y
        .byte   $0C
        lda     #$F5
        ora     $FF10
        sta     ($E0, x)
LCBAE:  bcc     LCBB0
LCBB0:  bit     $10
        .byte   $17
        ora     ($59), y
LCBB5:  rol     a
        asl     a
        .byte   $52
        clc
        cmp     $13
        ldy     $33
        ora     $F5
        ora     $5201
        bpl     LCBCD
        asl     $0E27
        php
        asl     $0E1B
        sbc     ($21), y
LCBCD:  lda     $0F0E
        asl     $1015
        asl     $880E
        and     ($F1, x)
        ldx     #$5D
        .byte   $2B
        .byte   $3C
        bpl     LCBFD
        ora     ($17), y
        asl     $F5
        ora     $1540
        rol     a
        php
        .byte   $22
        .byte   $09
LCBE9:  .byte   $22
        asl     a
        and     ($F1, x)
        dey
        sbc     ($A0), y
        .byte   $C7
        ora     ($0B), y
        bpl     LCC0F
        bpl     LCBE9
        clc
        asl     $0E1B
        .byte   $2F
LCBFC:  .byte   $11
LCBFD:  .byte   $22
        asl     $0E14
        .byte   $5C
        rol     a
        beq     LCC06
        .byte   $F1
LCC06:  plp
        sbc     ($60), y
        sbc     ($A8), y
        sbc     ($E2), y
        eor     #$0E
LCC0F:  .byte   $22
        bpl     LCC17
        asl     $865F
        eor     $81, x
LCC17:  ora     #$81
        beq     LCC63
LCC1B:  sbc     ($62), y
        sbc     ($C8), y
        .byte   $3C
LCC20:  bpl     LCC41
        ora     ($21), y
        asl     $0E05
        .byte   $0B
        and     ($0C, x)
        rol     a
        ora     $0E21
        and     LC41A
        .byte   $43
        sta     $15
        sty     $21
        .byte   $82
        beq     LCC1B
        sta     $22, x
        asl     $28
        sbc     ($88), y
        .byte   $9D
        txa
LCC41:  asl     $0F8A
        txa
        .byte   $22
        bpl     LCC6D
        ora     ($1B), y
        asl     $1227
        sbc     $0D, x
        .byte   $23
        beq     LCC53
        .byte   $FF
LCC53:  ora     ($F3, x)
        bmi     LCC7F
        .byte   $0F
LCC58:  sta     $48
        .byte   $0C
        lsr     $0D0B
        bmi     LCC89
        .byte   $E2
        .byte   $0C
        .byte   $84
LCC63:  ora     $3284
        bpl     LCC58
        dec     $1124, x
        .byte   $2F
        .byte   $85
LCC6D:  .byte   $1A
        bpl     LCCC2
        sbc     ($F0, x)
        .byte   $53
        .byte   $F1
LCC74:  asl     $0D78, x
        ora     $238C, x
        bpl     LCCA0
        ora     ($39), y
        .byte   $10
LCC7F:  .byte   $0C
        .byte   $83
        .byte   $12
LCC82:  sbc     ($55, x)
        sbc     ($52, x)
        bpl     LCCF2
        .byte   $10
LCC89:  and     $11
        bpl     LCC74
        php
        .byte   $03
        ora     #$E4
        sbc     $0D, x
        ora     $46F4, y
        .byte   $0C
        .byte   $F3
        asl     $0D
        .byte   $FF
        ora     ($F5, x)
        rts

        and     #$14
LCCA0:  bpl     LCCCE
        ora     ($11), y
        .byte   $0B
        ora     $2220
        .byte   $6B
        .byte   $07
        .byte   $01
LCCAB:  rol     $10, x
        and     #$11
        .byte   $32
        bvs     LCCBF
        bvs     LCCC6
        stx     $8E0D
        .byte   $E3
        bvs     LCCC6
        bvs     LCCE7
        bvs     LCCD9
        .byte   $83
LCCBF:  .byte   $4B
LCCC0:  .byte   $8F
        .byte   $17
LCCC2:  .byte   $70
LCCC3:  .byte   $17
        stx     $18
LCCC6:  bvs     LCCE0
        .byte   $87
        .byte   $04
        bvs     LCCE0
        sta     ($3A, x)
LCCCE:  bvs     LCCC0
        asl     $1A, x
        .byte   $89
        .byte   $74
        bvs     LCCE1
        .byte   $82
LCCD7:  .byte   $14
        .byte   $81
LCCD9:  ora     $70, x
LCCDB:  rol     $70, x
        asl     $85, x
        .byte   $F1
LCCE0:  .byte   $57
LCCE1:  sta     $1970, y
        dey
        pha
        .byte   $70
LCCE7:  clc
        .byte   $83
        .byte   $F2
        beq     LCC82
        beq     LCCC3
        sbc     ($14), y
        .byte   $F2
        .byte   $3E
LCCF2:  bvs     LCD12
        sta     $2D
        sta     ($2D, x)
        .byte   $0B
        ora     $1150
        .byte   $70
LCCFD:  .byte   $14
        rts

        .byte   $07
        rts

        asl     a
        rts

        bit     $F7
        sbc     ($56), y
        .byte   $FF
        clc
        .byte   $E3
        ora     ($01), y
        sbc     $0D, x
        .byte   $13
        .byte   $64
        and     ($0A), y
LCD12:  and     ($13), y
        bmi     LCD1C
        bmi     LCD21
        bmi     LCD26
        bmi     LCD51
LCD1C:  and     $28, x
        and     $F0
        .byte   $44
LCD21:  beq     LCD88
        beq     LCCAB
        .byte   $F0
LCD26:  .byte   $A7
        sbc     ($86), y
        sbc     ($A8), y
        sbc     ($C4), y
        eor     $35
        .byte   $23
        .byte   $32
        asl     a
        .byte   $32
        and     $21
        php
        rol     $F0
        .byte   $02
        beq     LCD5C
        beq     LCCFD
        .byte   $FF
        sta     ($EA, x)
        jsr     L8212
        .byte   $0B
        ora     $2A36
        .byte   $67
        rol     a
        .byte   $FF
        beq     LCDB8
        sbc     ($48), y
        .byte   $53
        bpl     LCD77
LCD51:  ora     ($3E), y
        adc     $6934
        rol     a
LCD57:  .byte   $F3
        sbc     ($CC), y
        eor     $10, x
LCD5C:  ldy     #$F9
        beq     LCD68
        sbc     ($4C), y
        sbc     ($8A), y
        .byte   $F5
LCD65:  asl     $FF00
LCD68:  sta     ($EA), y
        bpl     LCD7D
        beq     LCDDE
        .byte   $F2
        ora     #$A9
        sbc     $0E, x
        bpl     LCD65
        .byte   $0F
LCD76:  .byte   $F1
LCD77:  txa
        .byte   $FF
        sta     ($E8, x)
        .byte   $92
        brk
LCD7D:  .byte   $22
        bpl     LCD96
        ora     ($28), y
        php
        ora     $21, x
        asl     $21
        .byte   $0B
LCD88:  rol     $46
        ldy     $F5
        asl     $2601
        ora     $F0
        .byte   $82
        beq     LCD57
        sbc     ($C0), y
LCD96:  adc     $3F11, x
        .byte   $29
LCD9A:  ora     $1329, x
        asl     $0E04
        clc
        and     #$0C
        .byte   $82
        ora     $2782
        rol     $08
        and     ($09, x)
        and     ($52, x)
        bpl     LCDC5
        ora     ($55), y
        and     #$21
        .byte   $82
        .byte   $04
        .byte   $82
        asl     a
        .byte   $82
LCDB8:  .byte   $0C
        and     #$0F
        rol     $17
        sta     ($09, x)
LCDBF:  and     #$1C
        .byte   $2B
        sbc     ($C1), y
        cli
LCDC5:  bpl     LCDE2
        ora     ($1C), y
        bpl     LCE2B
LCDCB:  .byte   $21
LCDCC:  .byte   $01
LCDCD:  and     ($02, x)
        and     ($03, x)
        and     ($0C, x)
        .byte   $D2
        .byte   $17
        and     #$0D
        .byte   $1B
        beq     LCD9A
        .byte   $44
        .byte   $0F
        .byte   $9C
        .byte   $D5
LCDDE:  jmp     (L320F)

        .byte   $10
LCDE2:  clc
        ora     ($3B), y
        and     ($11, x)
        and     ($0A, x)
        .byte   $D3
        and     $C2
        beq     LCD76
        sbc     ($20), y
        .byte   $F2
        txs
        asl     $0E0B
        bcc     LCE07
        .byte   $13
        ora     ($08), y
        bpl     LCE59
        .byte   $83
        .byte   $3F
LCDFE:  and     $F1
        cpy     $3C
        .byte   $0F
        .byte   $63
        rol     a
        ora     $26
LCE07:  .byte   $07
        rol     a
        .byte   $34
        .byte   $83
        .byte   $0C
        .byte   $DC
        plp
        .byte   $A3
        sbc     $0E, x
        bit     $11
        and     $0518
        sbc     ($80), y
        .byte   $5B
        bpl     LCE39
        ora     ($5A), y
        asl     $88F1
        .byte   $FF
        sta     ($EA), y
        .byte   $42
        ora     ($04), y
        .byte   $0F
        ror     $18D7, x
        .byte   $0B
LCE2B:  asl     $F033
        jmp     L0F7C

        ror     a
        and     ($18, x)
        .byte   $DB
LCE35:  beq     LCDBF
        sbc     ($2C), y
LCE39:  .byte   $F2
        adc     $1232, x
        .byte   $D2
        .byte   $13
        .byte   $D2
        ora     #$21
        rol     $F124, x
        pla
        sbc     ($8D), y
        .byte   $C2
        .byte   $83
        and     $25, x
        php
        and     ($0A, x)
        and     ($0C, x)
        and     ($0E, x)
        and     ($F0, x)
        ora     $37, x
        lda     $F5
LCE59:  asl     $4A18
        brk
        .byte   $1A
        .byte   $04
        .byte   $12
        .byte   $80
        .byte   $03
        .byte   $80
        rts

        rol     $F0
        .byte   $5A
        beq     LCDFE
LCE69:  beq     LCE59
        sbc     ($CA), y
        .byte   $FF
        clc
LCE6F:  cpx     #$61
        ora     ($68), y
        .byte   $13
        asl     $2840
        .byte   $33
        .byte   $12
        and     ($24), y
        and     ($26), y
        and     ($F0), y
        txa
        beq     LCE35
        sbc     ($2A), y
        sbc     ($D3), y
        and     #$31
        bit     $1231
        and     ($25), y
        and     ($27), y
        .byte   $32
LCE90:  .byte   $22
        .byte   $32
        .byte   $0C
        and     ($35), y
        and     ($28), y
        .byte   $32
        beq     LCEE4
        beq     LCE6F
LCE9C:  sbc     ($2A), y
        sbc     ($B3), y
        bit     $1231
LCEA3:  .byte   $32
        and     $32
        sec
        .byte   $32
        .byte   $3B
        .byte   $32
        .byte   $14
        .byte   $34
        beq     LCED8
        beq     LCEA3
        sbc     ($4A), y
        jmp     L0D00

        brk
        .byte   $1A
        .byte   $33
        .byte   $33
        and     ($26), y
        sta     ($07, x)
        .byte   $13
        asl     $2320
        .byte   $32
        .byte   $23
        .byte   $32
        .byte   $33
        .byte   $32
        asl     a
        .byte   $33
        ora     $1600
        sta     $07
LCECD:  .byte   $85
LCECE:  beq     LCE69
        beq     LCE9C
        sbc     ($0B), y
        sbc     ($D5), y
        .byte   $23
        .byte   $32
LCED8:  .byte   $1B
        .byte   $32
        .byte   $23
        and     ($28), y
        .byte   $04
        .byte   $14
        sec
        ora     $3300
        .byte   $31
LCEE4:  .byte   $1A
        brk
        .byte   $0B
        .byte   $32
        .byte   $23
        and     ($F1), y
        .byte   $0B
        .byte   $33
        and     ($06), y
        stx     $07
        stx     $08
        .byte   $34
        ora     $3300
        and     ($1B), y
        .byte   $32
        .byte   $23
        and     ($28), y
        .byte   $04
        .byte   $14
        and     $0B, y
        .byte   $32
        brk
        .byte   $12
        brk
        beq     LCF32
        .byte   $12
        brk
        asl     $33, x
        beq     LCF44
        beq     LCE90
        .byte   $FF
        sta     ($E8, x)
        bcc     LCF27
        .byte   $02
        .byte   $0F
        .byte   $67
        .byte   $0B
        asl     $1F30
        and     ($16, x)
        .byte   $DF
        .byte   $7A
        .byte   $0F
        and     ($10, x)
        ora     $11, x
        sec
        .byte   $D9
LCF27:  bpl     LCF4A
        ora     ($21, x)
        .byte   $02
        and     ($AD, x)
LCF2E:  bpl     LCF93
        dec     $E3, x
LCF32:  ora     ($50), y
        bpl     LCF43
        ora     ($3A), y
        .byte   $0F
        ldy     $10, x
        .byte   $C7
        and     ($09, x)
        and     ($0B, x)
        and     ($14, x)
        .byte   $D9
LCF43:  tya
LCF44:  .byte   $E2
        eor     #$E2
        .byte   $13
        .byte   $E3
        .byte   $0F
LCF4A:  .byte   $E2
LCF4B:  bmi     LCF2E
        ldy     $E3
        .byte   $1A
        .byte   $D3
        .byte   $1C
        .byte   $1B
        sec
        bpl     LCF64
        .byte   $D2
        .byte   $1F
        .byte   $1B
        .byte   $54
        bpl     LCF84
        ora     ($52), y
        dec     $1B1A, x
        .byte   $5C
        .byte   $89
        .byte   $0D
LCF64:  .byte   $89
        asl     $0F89
        .byte   $89
        txa
        sta     ($0B, x)
        sta     ($F0, x)
        sta     $0CF1
        sbc     $0E, x
        .byte   $50
LCF74:  .byte   $FF
        tax
        nop
        .byte   $12
        .byte   $22
        stx     $25, y
        php
        and     $0A
        and     $0C
        and     $F0
        .byte   $4F
        .byte   $F0
LCF84:  .byte   $CC
        .byte   $F1
LCF86:  .byte   $4F
        sbc     ($D7), y
        sbc     ($EF), y
        .byte   $C3
        .byte   $83
        .byte   $32
        .byte   $80
        ora     #$33
        .byte   $47
        .byte   $70
LCF93:  .byte   $0B
        bvs     LCF86
        and     ($F0), y
        .byte   $8F
        beq     LCF4B
        sbc     ($CA), y
        .byte   $FF
        .byte   $03
        rti

        and     ($03), y
        php
        eor     $03
        and     ($56, x)
        ora     ($01, x)
        .byte   $03
        and     $09E4, y
        and     $3934, y
        sty     $13
        .byte   $B2
        and     $05D4, y
        .byte   $13
        .byte   $82
        .byte   $13
LCFB9:  .byte   $C2
        ora     $13
        .byte   $02
        and     $07A4, y
        .byte   $13
        .byte   $42
        .byte   $13
        .byte   $82
        .byte   $13
        .byte   $C2
        .byte   $03
        .byte   $13
        .byte   $B2
        ora     $39
        sty     $39, x
        cpx     $01
        ora     ($01, x)
        ora     $1C
        cpy     $42
        cmp     $03
        eor     ($CA, x)
        ora     ($03, x)
        rti

        rol     a
        ora     ($01, x)
        ora     ($07, x)
        .byte   $14
        sei
        .byte   $14
        stx     $14, y
        clv
        ora     #$08
        and     #$14
        dec     $14, x
        inc     $14
        inc     $01, x
        ora     #$04
        sty     $9C04
        php
        .byte   $AB
        .byte   $04
        ldy     $1405, x
        .byte   $89
        .byte   $14
        lda     $1405, y
        eor     $14, x
        sta     $07, x
        .byte   $14
        .byte   $17
        .byte   $23
        sec
        .byte   $14
        .byte   $57
LD00B:  ora     $0D
        rol     $0D
        lsr     $05
        .byte   $04
        ror     a
        php
        sta     $0301, y
        .byte   $14
        .byte   $8B
        ora     #$10
        .byte   $F2
        .byte   $14
        pla
        .byte   $14
        stx     $DE06
        .byte   $0B
        bpl     LD027
        asl     $C5
LD027:  asl     $36
        bpl     LCFB9
        bpl     LD00B
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     $13
        eor     $13, x
        sta     $01
        .byte   $03
        ora     ($2B, x)
        ora     $04
        bit     $04
        ldy     $01, x
        ora     ($07, x)
        .byte   $13
        .byte   $73
        .byte   $1C
        clv
        .byte   $42
        lda     $4103, y
        ldy     $01, x
        .byte   $03
        rti

        .byte   $3A
        ora     ($01, x)
        .byte   $03
        .byte   $47
        .byte   $80
        ora     ($03, x)
        eor     L0000
        ora     ($03, x)
        eor     $40
        .byte   $03
        .byte   $47
        cpy     #$01
        .byte   $07
        ora     #$2C
        ora     #$7C
        ora     #$AC
        ora     ($05, x)
        ora     #$58
        ora     #$78
        ora     ($01, x)
        ora     $21
        rol     $09, x
        dec     $03
        ora     #$36
        ora     $09
        and     $09
        eor     $03
        asl     $A5, x
        ora     ($01, x)
        ora     $22
        .byte   $52
        .byte   $22
        and     $0901, x
        ora     #$90
        ora     #$B0
        and     $2ED5
        .byte   $4F
        ora     $15
        .byte   $82
        and     $07A8
        rol     $1541
        sty     $2F
        txa
        ora     ($01, x)
        ora     ($01, x)
        ora     $07
        .byte   $67
        .byte   $12
        .byte   $D2
        ora     ($03, x)
        .byte   $12
        .byte   $F2
        ora     ($07, x)
        .byte   $0F
        rts

        .byte   $03
        .byte   $92
        .byte   $03
        ldx     $05
        .byte   $03
        .byte   $07
        .byte   $03
        eor     $05, x
        .byte   $1C
        .byte   $B7
        .byte   $42
        clv
        .byte   $03
        eor     ($B5, x)
        ora     ($01, x)
        .byte   $03
        eor     $01C7, x
        rol     a
        eor     ($87, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($03, x)
        pla
        bit     $01
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($08, x)
        dey
        dey
        dey
        clv
        .byte   $80
        brk
        rti

        .byte   $9C
