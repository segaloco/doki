.include	"system/fds.i"

.include	"mem.i"
.include	"actor.i"
.include	"math.i"

.segment	"CHAPT_01DATA"

L9C4C		:= $9C4C
LBE38		:= $BE38

LBF00:
	and	($30, x)
	.byte	$12
	.byte	$0F
	and	($30, x)
	asl	$0F, x
	and	($27, x)
	.byte	$17
	.byte	$0F
	and	($29, x)
	.byte	$1A
	.byte	$0F
	.byte	$0F
	bmi	LBF25
	ora	($0F, x)
	bmi	LBF2D
	.byte	$02
	.byte	$0F
	.byte	$27
	.byte	$17
	php
	.byte	$0F
	and	#$1A
	asl	a
	.byte	$0F
	bit	$0C1C
	.byte	$0F
LBF25:	bmi	LBF3D
	.byte	$02
	.byte	$0F
	.byte	$27
	.byte	$17
	php
	.byte	$0F
LBF2D:	rol	a
	.byte	$1A
	asl	a
	.byte	$07
	bmi	LBF5A
	.byte	$0F
	.byte	$07
	bmi	LBF4D
	.byte	$0F
	.byte	$07
	.byte	$27
	.byte	$17
	.byte	$0F
	.byte	$07
LBF3D:	and	($21), y
	.byte	$0F
	.byte	$03
	bit	$0F1C
	.byte	$03
	bmi	LBF5D
	.byte	$0F
	.byte	$03
	.byte	$3C
	.byte	$1C
	.byte	$0F
	.byte	$03
LBF4D:	and	$15
	ora	$0C
	bmi	LBF59
	.byte	$0F
	.byte	$0C
	bmi	LBF6D
	.byte	$0F
	.byte	$0C
LBF59:	.byte	$30
LBF5A:	asl	$0F, x
	.byte	$0C
LBF5D:	bmi	LBF85
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	.byte	$01
LBF6D:	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$FF
	bmi	LBF89
	.byte	$0F
	.byte	$FF
	sec
	bpl	LBF87
	.byte	$FF
	bmi	LBFA0
	.byte	$0F
	.byte	$FF
	bmi	LBF95
	.byte	$02
	.byte	$FF
	sec
	bpl	LBF86
	.byte	$FF
LBF85:	.byte	$30
LBF86:	.byte	$25
LBF87:	.byte	$02
	.byte	$FF
LBF89:	bmi	LBFA1
	.byte	$0F
	.byte	$FF
	bmi	LBF9F
	.byte	$0F
	.byte	$FF
	and	$10
	.byte	$0F
	brk
LBF95:	asl	a
	.byte	$14
	sec
	eor	$52A9, y
	sty	$A4
	ldy	$A4
LBF9F:	.byte	$A4
LBFA0:	.byte	$A4
LBFA1:	sbc	($60, x)
	.byte	$B3
	.byte	$FA
	jsr	$4141
	eor	($41, x)
	eor	($41, x)
	.byte	$F2
	.byte	$0B
	bcc	LBFB5
	ora	$05
	ora	$05
	.byte	$05
LBFB5:	dex
	dex
	dex
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
LBFBE:	.byte	$CB
	.byte	$CB
	cpy	LCCCC
	cmp	LCDCD
	cmp	LCDCD
	cmp	LCECD
	dec	LCFCF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	asl	$4666
	.byte	$D2
	cmp	$C5
	dec	$C6
	brk
	sta	$D2, y
	brk
	sta	$9999, y
	brk
	.byte	$9F
	.byte	$9F
	.byte	$9F
	brk
	lda	($A1, x)
	lda	($00, x)
	.byte	$D3
	.byte	$9B
	.byte	$12
	brk
	.byte	$9F
	.byte	$9F
	sta	$00, y
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$9C
	sta	a:$C6, x
	ora	$9F
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	lda	($00, x)
	brk
	brk
	.byte	$C2
	brk
	brk
	brk
	.byte	$9F
	brk
	brk
	.byte	$93
	sta	a:$C6, x
	rti

	sta	$C0C6, x
	cpy	#$C0
	.byte	$27
	.byte	$3B
	.byte	$4F
	.byte	$C0
LC022:	cpy	#$C0
LC024:	and	($45), y
LC026:	.byte	$59
	.byte	$CF
LC028:	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	bit	$4A35
	.byte	$87
	sta	$9292
	.byte	$92
	.byte	$92
	.byte	$92
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
LC045:	.byte	$9E
	ldx	$D7CD, y
	.byte	$DB
	inx
	inx
	inx
	inx
	inx
	.byte	$CF
	bne	LC022
	bne	LC024
	bne	LC026
	bne	LC028
	bne	LC045
	.byte	$20
	.byte	$26
	.byte	$40
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$63
	ora	($43, x)
	.byte	$80
	cpy	#$0C
	.byte	$5F
	sta	($CA), y
	.byte	$12
	.byte	$67
	tya
	.byte	$D2
	.byte	$67
	.byte	$6B
	.byte	$03
	jsr	$741D
	.byte	$6B
	.byte	$C7
	.byte	$6B
	.byte	$C7
	.byte	$6B
	.byte	$C7
	.byte	$6B
	.byte	$C7
	.byte	$6B
	.byte	$C7
	.byte	$F2
	.byte	$C7
	.byte	$F2
	.byte	$C7
	.byte	$F2
	.byte	$C7
	.byte	$F2
	.byte	$C7
	.byte	$73
	iny
	eor	($C8, x)
	eor	($C8, x)
	ora	$CA
	jsr	$741D
	and	$C8
	and	$C8
	and	$C8
	and	$C8
	and	$C8
	and	$C8
	.byte	$45
LC09F:	cmp	#$45
	cmp	#$45
	cmp	#$7F
	.byte	$C7
	.byte	$7F
	.byte	$C7
	.byte	$7F
	.byte	$C7
	adc	$6DC9
	cmp	#$0C
	cmp	#$0C
	cmp	#$AD
	asl	$2005
	ora	$2F74, x
	cmp	#$3B
	cmp	#$93
	cmp	#$AB
	.byte	$C7
	dec	$ABC7
	.byte	$C7
	.byte	$BE
	.byte	$C9
LC0C6:	ldx	$BEC9, y
	cmp	#$CB
	cmp	#$CB
	cmp	#$BE
	cmp	#$BE
	cmp	#$BE
	cmp	#$BE
	cmp	#$BE
	cmp	#$4C
	.byte	$BE
	.byte	$C9
; ------------------------------------------------------------
	.export	sub_C0DB
sub_C0DB:
	txa
	pha

	and	#CLAMP_16
	sta	pos_y_lo_actor+0
	txa
	asl	a
	asl	a
	asl	a
	asl	a
	sta	pos_x_lo_actor+0
	lda	#10
	sta	pos_x_hi_actor+0
	ldx	#0
	stx	actor_index
	stx	pos_y_hi_actor+0
	lda	#actor::life_heart
	sta	obj_id_actor+0
	lda	#$01
	sta	proc_id_actor+0
	tya

	pha
	jsr	actor_init_stationary
	pla

	sta	zp_actor_work_byte_1+0
	lda	#$FF
	sta	eng_actor_data_offset, x

	pla
	tax
	rts
; ------------------------------------------------------------
LC10A:
	and	$2D2F
	.byte	$2F
	cpx	#$E2
	cpx	$E6
	cpx	#$E2
	cpx	$E6
	inx
	nop
	.byte	$EC
	.byte	$EE
LC11A:	inx
	nop
	cpx	$01EE
	.byte	$03
	ora	#$05
	.byte	$07
	.byte	$0B
	ora	$150F
	ora	($13), y
	.byte	$17
	ora	($03, x)
	ora	#$05
	ora	$011B, y
	.byte	$03
	ora	#$05
	ora	$1D1B, y
	.byte	$1F
	and	$21
	.byte	$23
	.byte	$27
	ora	$251F, x
	and	($23, x)
	.byte	$27
	.byte	$9C
	.byte	$9E
	.byte	$9C
	.byte	$9E
	bne	LC11A
	.byte	$D4
	dec	$F0, x
	.byte	$F2
	.byte	$F4
	inc	$F0, x
	.byte	$F2
	sed
	.byte	$FA
LC152:	brk
	ora	$EB, x
	brk
LC156:	brk
	ora	($FF, x)
	brk
; ------------------------------------------------------------
	.export actor_C15A
actor_C15A:
	jsr	col_actor_tile_proc
	lda	$10
	and	#$03
	bne	LC16D
	dec	$B1, x
	bne	LC16D
	sta	$04B2
	jmp	actor_destroy

LC16D:	lda	$04B2
	beq	LC180
	lda	$46
	bpl	LC180
	lda	#$00
	sta	$47, x
	sta	$04B2
	jmp	LC343

LC180:	lda	$5B, x
	and	#$20
	sta	$04B2
	bne	LC18C
	jmp	LC21B

LC18C:	lda	$3D, x
	beq	LC196
	lda	$6F, x
	and	#$01
	sta	$9D
LC196:	lda	$33, x
	sec
	sbc	#$1A
	sta	$32
	lda	$1F, x
	sbc	#$00
	sta	$1E
	lda	$28
	sec
	sbc	#$08
	sta	$29, x
	lda	$14
	sbc	#$00
	sta	$15, x
	ldy	#$01
	lda	$3D, x
	bmi	LC1B8
	ldy	#$FF
LC1B8:	sty	LC156
	lda	$F7
	and	#$03
	tay
	and	a:$5A
	bne	LC1D7
	lda	LC152, y
	cmp	$3D, x
	beq	LC1D4
	lda	$3D, x
	clc
	adc	LC156, y
	sta	$3D, x
LC1D4:	jmp	LC1DB

LC1D7:	lda	#$00
	sta	$3D, x
LC1DB:	ldy	#$01
	lda	$47, x
	bmi	LC1E3
	ldy	#$FF
LC1E3:	sty	LC156
	lda	#$20
	cmp	$042C
	lda	#$00
	rol	a
	asl	a
	asl	a
	asl	a
	and	$F7
	bne	LC208
	lda	$5B, x
	lsr	a
	lsr	a
	and	#$03
	sta	$00
	lda	$F7
	lsr	a
	lsr	a
	and	#$03
	tay
	and	$00
	beq	LC20C
LC208:	lda	#$00
	beq	LC219
LC20C:	lda	LC152, y
	cmp	$47, x
	beq	LC21B
	lda	$47, x
	clc
	adc	LC156, y
LC219:	sta	$47, x
LC21B:	jsr	motion_x
	jsr	motion_y
	lda	$B1, x
	cmp	#$20
	bcs	LC22E
	lda	$10
	and	#$02
	bne	LC22E
	rts

LC22E:	jmp	LC343

	jsr	actor_init_shyguy_prj
	bmi	LC269
	ldx	$00
	ldy	$12
	lda	#$00
	sta	$3D, x
	sta	$47, x
	lda	#$3F
	sta	$90, x
	lda	$29, y
	sec
	sbc	#$08
	sta	$29, x
	lda	$15, y
	sbc	#$00
	sta	$15, x
	lda	$33, y
	clc
	adc	#$0E
	sta	$33, x
	lda	$1F, y
	adc	#$00
	sta	$1F, x
	jsr	actor_attr_set
	lda	#$A0
	sta	$B1, x
LC269:	ldx	$12
	rts

LC26C:	.byte	$02
	.byte	$02
	ora	($01, x)
LC270:	.byte	$33
	.byte	$3B
	.byte	$3B
	.byte	$33
LC274:	ora	($FF, x)
LC276:	php
	sed
LC278:	ora	($FF, x)
LC27A:	.byte	$20
	.byte	$E0
LC27C:	.byte	$14
	cpx	LCC20
	ldy	$9FF6
	lda	$042F, x
	beq	LC294
	lda	$65, x
	ora	#$80
	sta	$65, x
	jsr	LC2F9
	jmp	eng_actor_domove

LC294:	jsr	actor_timer_held_tick
	lda	$B1, x
	beq	LC2B2
	dec	$47, x
	bpl	LC2AF
	lda	$33, x
	cmp	#$30
	bcs	LC2AF
	lda	#$00
	sta	$B1, x
	sta	$3D, x
	sta	$47, x
	dec	$86, x
LC2AF:	jmp	LC2F3

LC2B2:	lda	$86, x
	bne	LC2C7
	lda	#$30
	sta	$47, x
	jsr	motion_side_get
	lda	LC27C, y
	sta	$3D, x
	inc	$B1, x
	jmp	LC2F9

LC2C7:	lda	$0480, x
	and	#$01
	tay
	lda	$47, x
	clc
	adc	LC274, y
	sta	$47, x
	cmp	LC276, y
	bne	LC2DD
	inc	$0480, x
LC2DD:	lda	$0477, x
	and	#$01
	tay
	lda	$3D, x
	clc
	adc	LC278, y
	sta	$3D, x
	cmp	LC27A, y
	bne	LC2F3
	inc	$0477, x
LC2F3:	jsr	motion_y
	jsr	motion_x
LC2F9:	jsr	render_actor_base_rocket_check
	lda	$51, x
	sec
	sbc	#$01
	ora	$042F, x
	ora	$A8, x
	bne	LC342
	jsr	actor_sprite_alloc
	sty	a:$F4
	lda	#$45
	sta	$65, x
	lda	$29, x
	pha
	sec
	sbc	#$08
	sta	$29, x
	lda	$15, x
	pha
	sbc	#$00
	sta	$15, x
	jsr	L9C4C
	pla
	sta	$15, x
	pla
	sta	$29, x
	lda	$042C
	clc
	adc	#$0C
	sta	$042C
	lda	$0429
	sbc	#$07
	sta	$0429
	jsr	LC343
	lda	#$0D
	sta	$65, x
LC342:	rts

LC343:	lda	$10
	lsr	a
	lsr	a
	lsr	a
	and	#$03
	ldy	$3D, x
	bmi	LC350
	eor	#$03
LC350:	tay
	lda	LC26C, y
	sta	$6F, x
	lda	LC270, y
	jmp	render_actor_base_draw

	jsr	actor_init_birdo
	lda	#$02
	sta	$0465, x
	rts

	jsr	eng_enemy_damage_check
	lda	$045C, x
	beq	LC372
	inc	$9F, x
	jmp	render_actor_base

LC372:	jsr	col_actor_tile_proc
	lda	#$02
	sta	$6F, x
	jsr	render_actor_base
	lda	$5B, x
	and	#$04
	beq	LC3E1
	jsr	actor_proc_base_veloc_reset
	lda	$10
	and	#$FF
	bne	LC391
	lda	#$D8
	sta	$47, x
	bne	LC3E1
LC391:	lda	$10
	and	#$3F
	bne	LC39B
	lda	#$20
	sta	$86, x
LC39B:	ldy	$86, x
	bne	LC3BA
	inc	$B1, x
	lda	$B1, x
	and	#$20
	beq	LC3E0
	inc	$9F, x
	inc	$9F, x
	ldy	#$18
	lda	$B1, x
	and	#$40
	bne	LC3B5
	ldy	#$E8
LC3B5:	sty	$3D, x
	jmp	motion_x

LC3BA:	cpy	#$10
	bne	LC3E0
	jsr	actor_init_shyguy_prj
	bmi	LC3E0
	ldx	$00
	lda	#$35
	sta	$90, x
	lda	$33, x
	adc	#$03
	sta	$33, x
	lda	#$E0
	sta	$47, x
	jsr	actor_attr_set
	lda	#$FF
	sta	$86, x
	lda	#$E0
	sta	$3D, x
	ldx	$12
LC3E0:	rts

LC3E1:	jmp	eng_actor_move_v

	lda	$51, x
	cmp	#$01
	bne	LC3F3
	lda	$045C, x
	beq	LC3FC
	lda	#$4A
	sta	$65, x
LC3F3:	lda	#$B3
	sta	$046E, x
	lda	#$5B
	bne	LC405
LC3FC:	ldy	$86, x
	dey
	cpy	#$10
	bcs	LC40B
	lda	#$4F
LC405:	jsr	render_actor_base_draw
	jmp	LC432

LC40B:	jsr	render_actor_base_rocket_check
	lda	$86, x
	cmp	#$10
	bcc	LC432
	lda	#$01
	sta	$65, x
	lda	#$10
	sta	$046E, x
	lda	$0429
	clc
	adc	#$0B
	sta	$0429
	asl	$EE
	ldy	#$00
	sty	a:$F4
	lda	#$67
	jsr	render_actor_base_draw
LC432:	lda	#$43
	sta	$65, x
	lda	#$33
	sta	$046E, x
LC43B:	rts

	.byte	$FB
	.byte	$05
LC43E:	jsr	$AEDD
	lda	$EE
	and	#$0E
	ora	$EF
	ora	$B1, x
	bne	LC466
	lda	$33, x
	sec
	sbc	#$02
	sta	$00
	ldy	$6F, x
	lda	$01
	clc
	adc	LC43B, y
	sta	$01
	jsr	actor_sprite_alloc
	ldx	#$6B
	jsr	render_actor_base_tiles
	ldx	$12
LC466:	rts

	lda	$B1, x
	bne	LC4C5
	lda	$A8, x
	beq	LC4A7
	lda	#$01
	sta	$90, x
	jsr	actor_attr_set
	jsr	actor_init_shyguy_prj_lo
	bmi	LC4A6
	ldy	$00
	lda	#$08
	sta	$90, y
	sta	$B1, y
	lda	$29, x
	sta	$29, y
	lda	$15, x
	sta	$15, y
	lda	$0441, x
	sta	$0441, y
	lda	#$FF
	sta	$0441, x
	lda	$3D, x
	sta	$3D, y
	tya
	tax
	jsr	actor_attr_set
	ldx	$12
LC4A6:	rts

LC4A7:	lda	$5B, x
	and	#$10
	beq	LC4C5
	inc	$B1, x
	sta	$9F, x
	jsr	actor_init_shyguy_prj_lo
	bmi	LC4C5
	ldy	$00
	lda	$3D, x
	sta	$3D, y
	lda	#$20
	sta	$0453, y
	jmp	LC4CB

LC4C5:	jsr	actor_timer_held_tick
	jsr	eng_enemy_damage_check
LC4CB:	jsr	col_actor_tile_proc
	lda	$5B, x
	and	$6F, x
	beq	LC4D7
	jsr	motion_dir_flip
LC4D7:	lda	$5B, x
	and	#$04
	beq	LC4F7
	lda	$042F, x
	beq	LC4EA
	lda	#$00
	sta	$042F, x
	jsr	actor_init_base_track_attr
LC4EA:	lda	$9F, x
	eor	#$08
	sta	$9F, x
	jsr	actor_proc_base_veloc_reset
	lda	#$F0
	sta	$47, x
LC4F7:	inc	$0477, x
	lda	$B1, x
	bne	LC508
	lda	$0477, x
	and	#$3F
	bne	LC508
	jsr	actor_init_base_tracking
LC508:	jsr	eng_actor_domove
	jmp	render_actor_base
; ------------------------------------------------------------
LC50E:
	inc	$FEFE, x
	inc	$B6B4, x
	lda	$B7, x
	clv
	.byte	$FA
	lda	$FAFA, y
	.byte	$FA
	.byte	$B2
	.byte	$B3
	ldx	LBFBE, y
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	lsr	a
LC527:	lsr	a
	.byte	$4B
	.byte	$4B
	.byte	$5E
LC52B:	.byte	$5F
	lsr	$A85F, x
LC52F:	tay
	lda	#$A9
	.byte	$A9
LC533:	lda	#$A9
	lda	#$45
LC537:	eor	$A9
	lda	#$A8
LC53B:	eor	$A9A9, x
	.byte	$74
LC53F:	ror	$75, x
	.byte	$77
	tya
LC543:	txs
	sta	$9C9B, y
LC547:	txs
	sta	$9C9B, x
LC54B:	.byte	$9E
	.byte	$9B
	.byte	$9F
	cli
LC54F:	.byte	$5A
	eor	$5E5B, y
	.byte	$5F
	lsr	$8E5F, x
	.byte	$8F
	.byte	$8F
	stx	$A6A6
	.byte	$A7
	.byte	$A7
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	.byte	$74
	ror	$75, x
	.byte	$77
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	bcs	LC527
	.byte	$FA
	.byte	$FA
	bcs	LC52B
	.byte	$FA
	.byte	$FA
	bcs	LC52F
	.byte	$FA
	.byte	$FA
	bcs	LC533
	.byte	$FA
	.byte	$FA
	bcs	LC537
	.byte	$FA
	.byte	$FA
	bcs	LC53B
	.byte	$FA
	.byte	$FA
	bcs	LC53F
	.byte	$FA
	.byte	$FA
	bcs	LC543
	.byte	$FA
	.byte	$FA
	bcs	LC547
	.byte	$FA
	.byte	$FA
	bcs	LC54B
	.byte	$FA
	.byte	$FA
	bcs	LC54F
	ldy	#$A2
	lda	($A3, x)
	.byte	$80
	.byte	$82
	sta	($83, x)
	.byte	$F4
	stx	$F5
	.byte	$87
	sty	$86
	sta	$87
	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
	lda	$ACFB
	lda	$ACAC
	ldy	$FBAC
	.byte	$3B
	.byte	$3B
	ldy	$FCFC
	.byte	$FC
	.byte	$FC
	.byte	$F4
	stx	$F5
	.byte	$87
	inc	$FEFE, x
	inc	$FEFE, x
	ora	$3CFE, x
	rol	$3F3D, x
	cli
	inc	$5A59, x
	.byte	$5B
	.byte	$5A
	inc	$FEFE, x
	eor	$5C5B, x
	inc	$5BFE, x
	.byte	$5A
	.byte	$1C
	inc	$FEFE, x
	inc	$FEFE, x
	inc	$1EFE, x
	inc	$201F, x
	.byte	$22
	and	($23, x)
	.byte	$57
	.byte	$57
	.byte	$FB
	.byte	$FB
	.byte	$57
	.byte	$57
	inc	$ABFE, x
	.byte	$AB
	.byte	$FB
	.byte	$FB
	nop
	nop
	.byte	$FB
	.byte	$FB
	.byte	$7C
LC603:	ror	$7F7D, x
	dex
	cpy	LCDCB
	dex
	cpy	LCDCB
	cpy	#$C2
	cmp	($C3, x)
	bit	$2D2E
	.byte	$2F
	stx	$8F8F
	stx	$8A88
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	sty	$888D
	txa
	sty	$888D
	txa
	.byte	$89
	.byte	$8B
	dey
	txa
	.byte	$89
	.byte	$8B
	ror	a
	jmp	($6D6B)

	jmp	($6D6C)

	adc	$6E6C
	adc	$326F
	.byte	$34
	.byte	$33
	and	$33, x
	and	$33, x
	and	$94, x
	sta	$94, x
	sta	$96, x
	.byte	$97
	stx	$97, y
	pha
	eor	#$48
	eor	#$FE
	inc	$FEFE, x
	.byte	$FB
	.byte	$32
	.byte	$32
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	sbc	$FDFD, x
	sbc	$FB34, x
LC664:	sbc	$FB34, x
	bmi	LC664
	.byte	$FB
	.byte	$FB
	.byte	$FB
	and	($FB), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	$56, x
	lsr	$56, x
	.byte	$64
	ror	$65
	.byte	$67
	pla
	ror	a
	adc	#$6B
	.byte	$FA
	jmp	($6CFA)

	adc	$6DFA
	.byte	$FA
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	ldx	$AEAF
	.byte	$AF
	sei
	.byte	$7A
	adc	$A87B, y
	tay
	.byte	$AF
	ldx	$9594
	sty	$95, x
	stx	$97, y
	stx	$97, y
	.byte	$22
	bit	$23
	and	$92
	.byte	$93
	.byte	$93
	.byte	$92
	bvc	LC6F9
	bvc	LC6FB
	ldx	$AEAF
	.byte	$AF
	bvc	LC701
	bvc	LC703
	stx	$8F8F
	stx	$5250
	eor	($53), y
	sbc	$FDFD, x
	sbc	$36FB, x
	rol	$4F, x
	.byte	$4F
	lsr	$4F4E
	lsr	$4F4F
	lsr	$9392
	.byte	$93
	.byte	$92
	stx	$8F8F
	stx	$4240
	eor	($43, x)
	rti

	.byte	$42
	eor	($43, x)
	tsx
	ldy	$BDBB, x
	tsx
	ldy	$9190, x
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	sbc	$FDFD, x
	sbc	$6361, x
	adc	($63, x)
	adc	$63
	adc	$63
	adc	$67
	.byte	$65
LC6F9:	.byte	$67
	rts

LC6FB:	.byte	$62
	adc	($63, x)
	.byte	$64
	.byte	$62
	.byte	$65
LC701:	.byte	$63
	.byte	$64
LC703:	ror	$65
	.byte	$67
	pla
	.byte	$62
	adc	($63, x)
	.byte	$64
	adc	#$65
	.byte	$67
	lsr	$62
	adc	($63, x)
	.byte	$64
	.byte	$47
	adc	$67
	tsx
	ldy	$BDBB, x
	bvs	LC78E
	adc	($73), y
	stx	$8F8F
	stx	$4544
	eor	$44
LC726:	.byte	$97
	.byte	$92
	.byte	$0C
	.byte	$0C
	ora	$99, x
	ora	$15, x
	eor	$45
	eor	$45
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$9F
	brk
	.byte	$9C
	lda	($9F, x)
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$80
	.byte	$07
	sta	($80, x)
	sta	($81, x)
	sta	($81, x)
; ------------------------------------------------------------
LC74A:	lda	$050E
	asl	a
	asl	a
	sta	$0F
	lda	$050E
	cmp	#$07
	bcc	LC75E
	lda	$0543
	jmp	LC761

LC75E:	lda	$0542
LC761:	clc
	adc	$0F
	tax
	lda	LC726, x
	sta	($01), y
	rts

	ldy	$E7
LC76D:	jsr	LC74A
	jsr	eng_map_area_increment_h
	dec	$050D
	bpl	LC76D
	rts

LC779:	.byte	$4F
	.byte	$4F
	.byte	$83
LC77C:	bvc	LC7CF
	.byte	$83
	ldy	$E7
	lda	$050E
	cmp	#$09
	bne	LC793
	lda	$04B0
	beq	LC793
	.byte	$EE
LC78E:	asl	$EE05
	.byte	$0E
	.byte	$05
LC793:	lda	$050E
	sec
	sbc	#$09
	tax
	lda	LC779, x
	sta	($01), y
	jsr	eng_map_area_increment_v
	lda	LC77C, x
	sta	($01), y
	jsr	LBE38
	rts

	ldy	$E7
	lda	#$52
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	lda	$E7
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	iny
	lda	#$54
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	jsr	LBE38
	rts

	.byte	$A4
LC7CF:	.byte	$E7
	lda	#$52
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	ldy	$E7
	tya
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	dey
	lda	#$54
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	jsr	LBE38
	rts

	ldy	$E7
	lda	$050E
	cmp	#$06
	bne	LC802
	lda	#$9E
	sta	($01), y
	jmp	LC805

LC802:	jsr	LC74A
LC805:	jsr	eng_map_area_increment_v
	dec	$050D
	bpl	LC802
	rts

	ldy	$E7
LC810:	jsr	LC74A
	tya
	clc
	adc	#$11
	tay
	dec	$050D
	bpl	LC810
	rts

LC81E:	.byte	$67
	ror	a
	.byte	$9C
	cmp	($70), y
	sty	$95, x
	lda	$050E
	tax
	cmp	#$05
	bne	LC833
	lda	$0543
	beq	LC833
	inx
LC833:	ldy	$E7
	lda	LC81E, x
	sta	($01), y
	rts

LC83B:	.byte	$73
	.byte	$0D
LC83D:	.byte	$74
	.byte	$0E
LC83F:	adc	$0F, x
	ldy	$E7
	lda	$050E
	sec
	sbc	#$0A
	tax
	lda	LC83B, x
	sta	($01), y
	dec	$050D
	beq	LC861
LC854:	jsr	eng_map_area_increment_h
	lda	LC83D, x
	sta	($01), y
	dec	$050D
	bne	LC854
LC861:	jsr	eng_map_area_increment_h
	lda	LC83F, x
	sta	($01), y
	rts

LC86A:	dex
	.byte	$CB
	cpy	LC8C7
	cmp	#$40
	.byte	$04
	ora	$A2
	brk
LC875:	stx	$0B
	ldx	$E8
	jsr	eng_map_bg_decoded_off_get
	ldx	#$05
	ldy	$E7
	ldx	$0B
	ldy	$E7
	lda	$050D
	sta	$07
	jsr	LC8BA
	inx
	lda	$07
	beq	LC899
LC891:	jsr	eng_map_area_increment_h
	jsr	LC8BA
	bne	LC891
LC899:	jsr	eng_map_area_increment_h
	inx
	jsr	LC8BA
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LC8B0
	ldx	#$03
	sta	$E7
	jmp	LC875

LC8B0:	rts

LC8B1:	.byte	$C7
	iny
	.byte	$C9
LC8B4:	.byte	$CF
	.byte	$CD
	.byte	$CD
LC8B7:	dec	$D0CE
LC8BA:	stx	$08
	txa
	bne	LC8D2
	ldx	#$02
	lda	($01), y
LC8C3:	cmp	LC8B1, x
	.byte	$F0
LC8C7:	ora	$CA
	bpl	LC8C3
	bmi	LC8EB
	lda	LC8B4, x
	bne	LC901
LC8D2:	ldx	$08
	cpx	#$02
	bne	LC8EB
	ldx	#$02
	lda	($01), y
LC8DC:	cmp	LC8B1, x
	beq	LC8E6
	dex
	bpl	LC8DC
	bmi	LC8EB
LC8E6:	lda	LC8B7, x
	bne	LC901
LC8EB:	ldx	#$08
LC8ED:	lda	($01), y
	cmp	LC86A, x
	beq	LC8FC
	dex
	bpl	LC8ED
	ldx	$08
	jmp	LC903

LC8FC:	ldx	$08
	lda	LC86A, x
LC901:	sta	($01), y
LC903:	ldx	$08
	dec	$07
	rts

LC908:	cpy	#$76
LC90A:	.byte	$82
	.byte	$77
	lda	$050E
	sec
	sbc	#$0E
	sta	$07
	tax
	ldy	$E7
	lda	LC908, x
	sta	($01), y
LC91C:	jsr	eng_map_area_increment_v
	lda	($01), y
	cmp	#$40
	bne	LC92E
	ldx	$07
	lda	LC90A, x
	sta	($01), y
	bne	LC91C
LC92E:	rts

	ldy	$E7
	lda	#$01
	sta	($01), y
	iny
	lda	#$02
	sta	($01), y
	rts

	ldy	$E7
	lda	#$03
	sta	($01), y
	rts

LC942:	adc	$7271
	ldy	$E7
	lda	$050E
	sec
	sbc	#$06
	tax
	lda	LC942, x
	sta	($01), y
LC953:	jsr	eng_map_area_increment_v
	lda	($01), y
	cmp	#$40
	bne	LC963
	lda	#$6E
	sta	($01), y
	jmp	LC953

LC963:	tya
	sec
	sbc	#$10
	tay
	lda	#$6F
	sta	($01), y
	rts

LC96D:	ldy	$E7
	lda	$050E
	cmp	#$0D
	beq	LC97D
	lda	#$C1
	sta	($01), y
LC97A:	jsr	eng_map_area_increment_v
LC97D:	lda	($01), y
	cmp	#$40
	bne	LC992
	lda	#$C2
	sta	($01), y
	lda	$EC
	beq	LC98F
	cpy	#$E0
	bcs	LC992
LC98F:	jmp	LC97A

LC992:	rts

	ldy	$E7
	lda	#$C3
	sta	($01), y
LC999:	tya
	sec
	sbc	#$10
	tay
	cmp	#$F0
	bcs	LC9AF
	lda	($01), y
	cmp	#$40
	bne	LC9AF
	lda	#$C2
	sta	($01), y
	jmp	LC999

LC9AF:	rts

LC9B0:	.byte	$43
	.byte	$44
	eor	$46
	.byte	$47
	pha
	eor	#$4A
	.byte	$4B
	lsr	$414C
	.byte	$5A
	.byte	$42
	ldy	$E7
	ldx	$050E
	lda	LC9B0, x
	sta	($01), y
	rts

LC9C9:	bcc	LC96D
LC9CB:	lda	$050E
	sec
	sbc	#$09
	sta	$08
	ldy	$E7
	ldx	$E8
	jsr	eng_map_bg_decoded_off_get
	ldy	$E7
	lda	#$05
	sta	$07
	lda	($01), y
	cmp	#$40
	bne	LCA02
LC9E6:	ldx	$08
	lda	LC9C9, x
	sta	($01), y
	jsr	eng_map_area_increment_h
	dec	$07
	bpl	LC9E6
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LCA02
	sta	$E7
	jmp	LC9CB

LCA02:	rts

LCA03:	.byte	$04
	ora	$A9
	brk
	sta	$08
LCA09:	ldy	$E7
	ldx	$E8
	jsr	eng_map_bg_decoded_off_get
	ldy	$E7
	lda	$050D
	sta	$07
	ldx	$08
LCA19:	.byte	$BD
	.byte	$03
LCA1B:	dex
	sta	($01), y
	jsr	eng_map_area_increment_h
	dec	$07
	bpl	LCA19
	lda	#$01
	sta	$08
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LCA37
	sta	$E7
	jmp	LCA09

LCA37:	rts
; ------------------------------------------------------------
	ora	#$F3
	bmi	LCA3C
LCA3C:	.byte	$B7
	asl	a
	brk
	brk
	lsr	$10, x
	.byte	$1A
	bpl	LCA1B
	.byte	$C2
	and	#$10
	adc	$C4, x
	.byte	$5A
	and	($12, x)
	cmp	#$75
	.byte	$C7
	asl	$3EC1, x
	asl	a
	brk
	jsr	LCF20
	.byte	$FF
	brk
	.byte	$F3
	bmi	LCA5D
LCA5D:	sbc	$00, x
	bvc	LCA95
	sbc	($19, x)
	.byte	$E2
	lda	$12, x
	asl	a
	.byte	$12
	.byte	$17
	.byte	$12
	asl	$E5, x
	.byte	$32
	sbc	($26, x)
	.byte	$E3
	.byte	$57
	sbc	$38
	.byte	$C2
	.byte	$27
	.byte	$22
	.byte	$0C
	.byte	$E2
	.byte	$17
	cmp	($11, x)
	.byte	$E2
	asl	$22, x
	asl	$C3, x
	.byte	$2B
	and	($18, x)
	cpy	$37
	.byte	$C2
	rol	$C7, x
	.byte	$12
	.byte	$E2
	and	$C8, x
	.byte	$3B
	.byte	$C3
	bit	$22
	.byte	$12
	.byte	$0C
	sbc	$00, x
	.byte	$29
LCA95:	.byte	$04
	cpy	$30
	sbc	($02, x)
	.byte	$03
	.byte	$03
	cpx	$07F4
	ora	$0D45
	.byte	$1A
	ora	$E326
	tsx
	.byte	$0C
	.byte	$FF
	.byte	$80
	cpx	#$92
	brk
	adc	$100E, y
	.byte	$C2
LCAB1:	.byte	$07
	asl	$0A31
	brk
	.byte	$03
	ora	$21, x
	php
	.byte	$22
	.byte	$0F
	.byte	$22
	txa
	.byte	$2B
	.byte	$0B
	.byte	$52
	asl	$1026
	bpl	LCAD0
	cmp	$63
	and	($60, x)
	.byte	$0C
	.byte	$44
	and	#$3E
	.byte	$0F
	.byte	$14
LCAD0:	.byte	$0F
	ora	#$01
	ora	$780F, y
	eor	($17), y
	.byte	$DC
	ora	$C2, x
	.byte	$14
	.byte	$22
	bpl	LCB08
	.byte	$03
	.byte	$C2
	clc
	.byte	$FA
	ora	$C3, x
	and	($22, x)
	beq	LCAB1
	.byte	$5A
	bpl	LCB0A
	bpl	LCB21
	.byte	$C2
	asl	$F9, x
	clv
	.byte	$27
	asl	a
	ora	($17, x)
	.byte	$C3
	ora	($22), y
	.byte	$0F
	and	#$10
	.byte	$C2
	.byte	$17
	cmp	$33
	.byte	$22
	.byte	$07
	dec	$11
	.byte	$C3
	.byte	$1F
	and	$F0
LCB08:	cpx	#$8D
LCB0A:	and	($0F, x)
	.byte	$27
	.byte	$43
	cmp	($06, x)
	and	#$0A
	and	#$31
	bit	$06
	and	($0A, x)
	eor	($51), y
	.byte	$CF
	asl	$F3
	bmi	LCB41
	adc	$D5, x
LCB21:	eor	($CB), y
	plp
	.byte	$0B
	brk
	.byte	$32
	asl	$1A26, x
	cmp	LC09F
LCB2D:	bvc	LCB50
	ora	($21, x)
	.byte	$03
	.byte	$2D
LCB33:	php
	.byte	$54
	ora	$F512
	brk
	.byte	$13
	clc
	dec	$F4
	.byte	$F3
	.byte	$F3
	.byte	$DF
	.byte	$C6
LCB41:	.byte	$F3
	adc	$F2C8
	ror	a
	.byte	$CF
	lsr	a:$0B, x
	eor	($F4, x)
	.byte	$F3
	.byte	$F3
	.byte	$F3
	.byte	$2E
LCB50:	asl	$91FF
	nop
	.byte	$22
	ora	($47), y
	tay
	ora	$0B25, y
	and	$16
	.byte	$83
	bit	$0A
	brk
	.byte	$54
	.byte	$13
	and	$F0
	adc	($F0), y
	.byte	$FC
	beq	LCB78
	sbc	($8F), y
	sbc	($B1), y
	sbc	($F4), y
	.byte	$64
	.byte	$F3
	tya
	.byte	$0C
	sbc	$00, x
	rti

	pha
LCB78:	.byte	$03
	clc
	.byte	$03
	.byte	$C7
	.byte	$13
	brk
	plp
	beq	LCB33
	sbc	($4A), y
	.byte	$FF
	sta	($EA), y
	.byte	$22
	ora	($03), y
	.byte	$F3
	.byte	$97
	.byte	$12
	sbc	$00, x
	and	($34), y
	.byte	$80
	ora	$80
	asl	$0F21
	and	($F0, x)
	cpx	$0FF1
	sbc	($50), y
	sbc	($13, x)
	brk
	.byte	$27
	beq	LCB2D
	.byte	$FF
	.byte	$80
	inx
	rti

	.byte	$12
	.byte	$7C
	bpl	LCBC3
	.byte	$0C
	sbc	$00, x
	bpl	LCBC2
	bpl	LCBC7
	ora	($30), y
	.byte	$E7
	php
	.byte	$03
	ora	#$E6
	.byte	$9C
	bpl	LCBDB
	ora	($50), y
	.byte	$E7
	asl	a
	sta	($F1), y
LCBC2:	rts

LCBC3:	ror	$4310, x
	pha
LCBC7:	sbc	$01, x
	brk
	.byte	$F0
LCBCB:	jmp	$EBF1

	.byte	$F2
	beq	LCBFB
	.byte	$F2
	bit	$83
	and	$81
	.byte	$4B
	.byte	$0B
	brk
	bmi	LCBCB
LCBDB:	.byte	$8B
	sbc	($8C), y
	sbc	($CA), y
	.byte	$FF
	.byte	$80
	nop
	bvs	LCBF5
	stx	$3BC3
	cmp	($F0, x)
	cmp	$48F1
	cmp	$1310, x
LCBF0:	.byte	$C2
	.byte	$EB
	bpl	LCC04
	.byte	$11
LCBF5:	asl	$10
	asl	$2E11
	.byte	$C1
LCBFB:	.byte	$7F
	and	#$5D
	asl	$0E0E
	rol	a
	.byte	$07
	.byte	$0F
LCC04:	asl	$F5
	ora	($30, x)
	clc
	rol	$0C
	.byte	$2B
	.byte	$12
	.byte	$C2
	beq	LCBF0
	inc	$00, x
	.byte	$43
	bpl	LCC2A
	ora	($1A), y
	.byte	$CD
LCC18:	and	$0E, x
	and	($0E), y
	php
	and	#$1D
	.byte	$09
LCC20:	ora	($10, x)
	bpl	LCC46
	.byte	$02
	eor	($40), y
	and	#$0B
	.byte	$10
LCC2A:	.byte	$1C
	ora	($12), y
	rol	$18
	cpy	$08F0
	sbc	($C2), y
	.byte	$F2
	.byte	$3C
	asl	$0E1D
	.byte	$13
	.byte	$0B
	ora	($13, x)
	ora	$56, x
	asl	$9C51
	sty	$8C0D
	.byte	$0E
LCC46:	sty	$8C0F
	.byte	$27
	bpl	LCC61
	ora	($4A), y
	asl	$0E14
	.byte	$0C
	.byte	$0B
	ora	($20, x)
	rol	$0E
	ora	#$82
	asl	a
	.byte	$82
	.byte	$0B
	.byte	$82
	beq	LCC5F
LCC5F:	.byte	$FF
	.byte	$89
LCC61:	nop
	.byte	$32
	.byte	$11
LCC64:	sta	$1629, y
	.byte	$29
LCC68:	.byte	$0C
	and	#$12
	.byte	$13
	ora	($04, x)
	.byte	$1A
	and	$0C
	and	($0E, x)
	and	($F0, x)
	bmi	LCC68
	.byte	$F7
	adc	$1CA8, x
	and	$0E
	rol	$1D
	.byte	$05
LCC80:	.byte	$3A
LCC81:	.byte	$83
	.byte	$32
	and	$03
	and	$06
	and	$07
	and	$09
	.byte	$80
	.byte	$0F
	and	($F0, x)
	bmi	LCC81
	.byte	$97
	beq	LCC64
	sbc	($35), y
	.byte	$80
	and	$02
	.byte	$25
LCC9A:	.byte	$17
	.byte	$82
	asl	$2429, x
	.byte	$83
	.byte	$37
	and	$F0F0
	sbc	($1B), y
	sbc	($51), y
	.byte	$7C
	.byte	$14
	ora	($06, x)
	beq	LCCBD
	sbc	($4E), y
	sbc	($CA), y
	.byte	$FF
	.byte	$80
	cpx	#$20
	.byte	$02
	brk
	sty	$8C01
	.byte	$02
	.byte	$8C
LCCBD:	.byte	$03
LCCBE:	sty	$1028
	.byte	$1B
	ora	($53), y
	.byte	$0B
	ora	($07, x)
	.byte	$1F
	asl	$8214
	.byte	$05
LCCCC:	.byte	$82
	ora	$790E
	ora	($16), y
	bpl	LCD0A
	asl	$0E31
	.byte	$04
	pha
	.byte	$13
	.byte	$82
	.byte	$04
	.byte	$82
	ora	$82
	asl	$82
LCCE1:	.byte	$07
	.byte	$82
	php
	.byte	$82
	ora	#$82
	asl	a
	.byte	$82
	.byte	$0B
	.byte	$82
	.byte	$0C
	.byte	$82
	.byte	$F2
	beq	LCCF1
	.byte	$F6
LCCF1:	.byte	$02
	beq	LCD4D
	beq	LCC80
	sbc	$02, x
	brk
	.byte	$FF
	clc
	.byte	$E3
	ora	($01, x)
	.byte	$64
	.byte	$32
	ora	#$32
	.byte	$17
	bit	$3222
	.byte	$0B
	.byte	$32
	.byte	$14
	.byte	$2C
LCD0A:	asl	$32
	asl	a
	bit	$44F0
	beq	LCD77
	beq	LCC9A
	beq	LCCBD
	sbc	($66), y
	sbc	($83), y
	sbc	($A0), y
	sbc	$01, x
	.byte	$03
	.byte	$FF
	clc
	.byte	$E3
	ora	($01, x)
	.byte	$64
	.byte	$32
	ora	#$32
	.byte	$32
	.byte	$32
	.byte	$0B
	.byte	$32
	bit	$31
	asl	a
	and	($17), y
	.byte	$27
	beq	LCD78
	beq	LCD9B
	beq	LCCBE
LCD38:	beq	LCCE1
	sbc	($86), y
	sbc	($A8), y
	sbc	($C4), y
	.byte	$FF
	.byte	$80
	sbc	($90, x)
	brk
LCD45:	brk
	txa
	ora	($8A, x)
	.byte	$02
	txa
	.byte	$2D
	.byte	$29
LCD4D:	.byte	$1A
	and	#$28
	cmp	$2E
	.byte	$F2
	sbc	($C8), y
	ldx	$29
	asl	$1310
	and	#$21
	.byte	$C2
	.byte	$07
	cmp	$24
	.byte	$F2
	ora	$26F2
	.byte	$C7
	jsr	$79C4
	and	#$0B
	and	#$18
	.byte	$2B
	ora	($22), y
	.byte	$02
	rol	$03
	.byte	$DB
	bpl	LCD38
	.byte	$0E
	.byte	$C1
LCD77:	.byte	$14
LCD78:	sbc	LC43E, y
	.byte	$1A
	.byte	$C3
	bpl	LCD45
	lsr	$10, x
	.byte	$1A
	ora	($4C), y
	.byte	$D3
	ora	$D3, x
	ora	$16C1
	cmp	($0F, x)
	.byte	$F3
	clc
	.byte	$F4
	.byte	$13
	.byte	$F2
	.byte	$64
	bpl	LCDBC
	.byte	$D3
	ora	$12C1, y
	.byte	$D3
	.byte	$0B
	.byte	$FE
LCD9B:	.byte	$13
	cmp	($15, x)
	.byte	$F3
	.byte	$A7
	ora	($11), y
	and	#$05
	and	#$0B
	and	($0C, x)
	and	($0D, x)
	and	($0E, x)
	and	($0F, x)
	and	($1A, x)
	dec	$60
	cmp	($04, x)
	cmp	($08, x)
	dex
	.byte	$80
	and	$100A
	.byte	$42
LCDBC:	and	#$43
	sbc	$A1F1, y
	rol	$2319, x
	ora	($18), y
	bpl	LCDDE
	and	#$24
	.byte	$22
LCDCB:	asl	$21
LCDCD:	php
	.byte	$22
	.byte	$13
	cmp	$12, x
	cmp	($08, x)
	cmp	($24, x)
	rol	$06
	.byte	$22
LCDD9:	.byte	$2B
	sbc	($F1), y
	pla
	.byte	$F1
LCDDE:	lda	($34, x)
	ora	$190A, y
	.byte	$9C
	asl	a
	.byte	$02
	.byte	$23
	tya
	bpl	LCE3F
	cmp	($12, x)
	php
	bpl	LCE40
	beq	LCDD9
	.byte	$FF
	jsr	$03E0
	ora	$2C38, y
	and	$2C
	.byte	$0B
	bit	$2C33
	ora	$162C
	.byte	$34
	.byte	$27
	asl	a
	.byte	$02
	jsr	$54F0
	sbc	($A0), y
	.byte	$FF
	jsr	$67E0
	ora	$0A58, y
	.byte	$02
	bpl	LCE37
	and	$3232, y
	.byte	$0B
	.byte	$32
	and	$33, x
	asl	a
	bmi	LCE38
	.byte	$AF
	beq	LCE75
	.byte	$34
	and	($0C), y
	bmi	LCE42
	ldy	$3112
	.byte	$34
	and	($32), y
	and	($34), y
	and	($F0), y
	.byte	$5C
	.byte	$32
	and	($0C), y
	bmi	LCE6A
	.byte	$37
LCE37:	.byte	$62
LCE38:	.byte	$37
	clc
	lda	#$0D
	brk
	.byte	$0E
	brk
LCE3F:	.byte	$1D
LCE40:	brk
	.byte	$0E
LCE42:	brk
	.byte	$14
	and	($09), y
	and	$F0, x
	.byte	$14
	ror	a
	.byte	$33
	.byte	$3C
	and	($13), y
	.byte	$13
	.byte	$02
	php
LCE51:	bit	$2731
	.byte	$32
	sbc	($5B), y
	sbc	($74), y
	.byte	$44
	and	$36, x
	and	$34, x
	and	$19, x
	ldx	$27
	and	($0A), y
	and	($17), y
	.byte	$AF
	php
	.byte	$AD
	.byte	$F0
LCE6A:	asl	$49, x
	ldx	$A965
	.byte	$0B
	.byte	$AB
	eor	($33), y
	.byte	$0C
	.byte	$32
LCE75:	.byte	$12
	ldy	$A80D
	beq	LCE92
	beq	LCE51
	.byte	$63
	and	($14), y
	.byte	$A7
	bit	$0209
	bmi	LCEAC
	and	($51), y
	.byte	$73
	asl	$73
	sbc	($18), y
	sbc	($C0), y
	.byte	$FF
	ldy	#$EA
LCE92:	.byte	$57
	ora	$2939, y
	.byte	$13
	.byte	$13
	.byte	$02
	rol	$6B
	.byte	$32
	plp
	.byte	$77
	beq	LCEEB
	.byte	$F1
LCEA1:	ora	$307F
	asl	$31, x
	asl	a
	.byte	$32
	.byte	$23
	and	($0F), y
	.byte	$30
LCEAC:	jsr	$1F31
	bmi	LCEA1
	php
	bcc	LCF1A
	.byte	$07
	.byte	$31
LCEB6:	.byte	$17
	and	($09), y
	ror	$20
LCEBB:	ror	$07
	bmi	LCEC7
	.byte	$01
LCEC0:	.byte	$17
	and	($09), y
	ror	$20
	ror	$07
LCEC7:	and	($17), y
	and	($09), y
	ror	$80
LCECD:	and	#$10
	bmi	LCEDB
	and	#$20
	and	#$10
LCED5:	bmi	LCEE0
	.byte	$82
	asl	a
	.byte	$82
	.byte	$0B
LCEDB:	.byte	$82
	asl	$0F82
	.byte	$82
LCEE0:	jsr	$1029
	bmi	LCED5
	eor	$A8F0
LCEE8:	beq	LCEB6
	.byte	$F1
LCEEB:	php
	.byte	$93
	and	#$32
	sta	($03, x)
	sta	($06, x)
	sta	($F0, x)
	bcs	LCEE8
	.byte	$0F
	beq	LCF2B
	.byte	$F2
	sta	$F142, y
	.byte	$0F
	sbc	($CA), y
	sbc	$02, x
	rti

LCF04:	.byte	$FF
LCF05:	tax
	sed
	asl	$22, x
	ror	$0F25
	and	$1E
	.byte	$32
	.byte	$3F
	and	$F0
	bvc	LCF04
	lda	$5BF1, x
	sbc	($9A), y
	.byte	$B0
LCF1A:	and	$04
	sty	$33
	sta	($0A, x)
LCF20:	.byte	$33
	ora	($25), y
	ora	$80
	beq	LCEC0
	beq	LCF05
	sbc	($F8), y
LCF2B:	.byte	$FF
	ora	($01, x)
	ora	$03
	ror	$03, x
	jmp	($0101)

	ora	$14
	lsr	$14, x
	lda	$0303
	sta	$0E07, x
	.byte	$83
	asl	$0379
	ldy	$0E05, x
	bcc	LCF56
	adc	$01, x
	.byte	$03
	ora	($EC, x)
	.byte	$07
	ora	($1C, x)
	.byte	$02
	.byte	$5C
	.byte	$03
	sbc	$07
	.byte	$01
LCF56:	jmp	($8C02)

	ora	($CC, x)
	ora	$03
	adc	$D401, y
	ora	#$01
	.byte	$04
	.byte	$02
	.byte	$34
	and	$3987, y
	.byte	$C7
	.byte	$07
	.byte	$03
	lda	#$03
	ldy	LC603, x
	ora	$01
	.byte	$9C
	.byte	$03
	sbc	$05
	ora	($2C, x)
	.byte	$02
	jmp	$0107

	.byte	$1C
	.byte	$03
	eor	$02
	jmp	$0105

	sta	$14, x
	cmp	($01), y
	.byte	$03
	.byte	$03
	sta	$01
	ora	($01, x)
	.byte	$03
	asl	$01BC
	ora	($01, x)
	ora	($07, x)
	.byte	$1C
	.byte	$A7
	.byte	$42
	tay
	eor	($E7, x)
	ora	($01, x)
	ora	($03, x)
	rti

	ror	a
	.byte	$03
	.byte	$12
	.byte	$22
	ora	#$0F
	brk
	.byte	$0F
	rti

	.byte	$0F
	bcc	LCFBB
	cpx	#$01
	ora	$0E
	.byte	$3C
	asl	$056C
	asl	$53
	ora	$0583
	ora	($B6, x)
LCFBB:	ora	($F6, x)
	ora	($03, x)
	ora	($DC, x)
	.byte	$03
	.byte	$07
	.byte	$3C
	.byte	$03
	asl	$2C
	ora	$0D
	adc	$890D, y
	ora	($01, x)
	.byte	$05
LCFCF:	.byte	$1C
	.byte	$A7
	.byte	$42
	tay
	.byte	$03
	eor	($38, x)
	ora	($03, x)
	.byte	$3B
	adc	$0101, y
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	.byte	$02
	plp
	ora	($01, x)
	ora	($03, x)
	rti

	sec
	ora	$07
	sty	$07, x
	ldy	$03, x
	ora	($74, x)
	ora	$13
	.byte	$34
	.byte	$13
	ldy	$07
	.byte	$13
	.byte	$02
	.byte	$13
	.byte	$62
	and	$05E6, y
	and	$3926, y
	ror	$09
	.byte	$13
	lsr	a
	and	$135C, y
	txa
	and	$059C, y
	.byte	$03
	rol	a
	.byte	$03
	.byte	$7A
	.byte	$09
LD016:	ora	$0D29
	eor	#$02
	.byte	$7A
	.byte	$02
	txs
	ora	($01, x)
	ora	$3B
	dey
	bit	$018A
	.byte	$07
	bit	$2E4B
	.byte	$7C
	rol	$01C9
	.byte	$07
	rol	$2F61
	.byte	$5A
	.byte	$03
	tsx
	.byte	$03
	ora	($DB, x)
	.byte	$03
	asl	$4A
	.byte	$03
	.byte	$03
	and	$01, x
	ora	($03, x)
	.byte	$2F
	cmp	#$03
	and	$09C6
	.byte	$02
	lsr	$07, x
	.byte	$D4
	.byte	$07
	.byte	$D7
	.byte	$07
	.byte	$DA
	ora	$2E
	.byte	$A7
	rol	$05F7
	bit	$2C3A
	ror	a
	ora	#$0D
	ora	#$0D
	and	#$42
	tay
	eor	($D7, x)
	ora	($03, x)
	rol	$03F6
	eor	$01D7, x
	rts

	ldy	#$02
	asl	$28
	.byte	$80
	.byte	$03
	.byte	$82
	bpl	LD074
LD074:	bvc	LD016
	.byte	$02
	rol	$25, x
	.byte	$80
	.byte	$34
	.byte	$80
	.byte	$02
	.byte	$80
	brk
	brk
	rts

	.byte	$80
	.byte	$2B
	sta	($0A, x)
	brk
	.byte	$04
	.byte	$82
	bpl	LD08A
LD08A:	rts

	ora	($07, x)
	ora	($C8, x)
	.byte	$03
	cld
	.byte	$02
	inx
	ora	($01, x)
	ora	($07, x)
	.byte	$27
	.byte	$72
	.byte	$27
	.byte	$82
	.byte	$27
	ror	$0301, x
	.byte	$27
	cmp	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$3B
	.byte	$74
	.byte	$27
	.byte	$57
	.byte	$27
	.byte	$97
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	$27
	adc	$27
	sta	$05, x
	.byte	$42
	rol	a
	eor	($87, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	pla
	bit	$01
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($08, x)
	dey
	dey
	dey
	clv
	.byte	$80
	brk
	rti
	.byte	$9C
