.segment	"CHAPT_03DATA"

L0101           := $0101
L07E0           := $07E0
L0D81           := $0D81
L12C4           := $12C4
L14E0           := $14E0
L1AF0           := $1AF0
L1BC3           := $1BC3
L37E0           := $37E0
L5210           := $5210
L67E0           := $67E0
L6CFA           := $6CFA
L6D6B           := $6D6B
L6D6C           := $6D6C
L741D           := $741D
L8711           := $8711
L93E0           := $93E0
L9839           := $9839
L9887           := $9887
L988A           := $988A
L995F           := $995F
L9967           := $9967
L9C4C           := $9C4C
L9D61           := $9D61
LA116           := $A116
LA2FC           := $A2FC
LA65E           := $A65E
LA662           := $A662
LA929           := $A929
LABB2           := $ABB2
LAC1F           := $AC1F
LACCC           := $ACCC
LAE56           := $AE56
LAED4           := $AED4
LAEE0           := $AEE0
LB01F           := $B01F
LB168           := $B168
LB178           := $B178
LB17D           := $B17D
LB1D6           := $B1D6
LB207           := $B207
LBE01           := $BE01
LBE0C           := $BE0C
LBE24           := $BE24
LBE38           := $BE38
LEC04           := $EC04
        .byte   $22
        bmi     LBF15
        .byte   $0F
        .byte   $22
        bmi     LBF1D
        .byte   $0F
        .byte   $22
        .byte   $27
        .byte   $17
        .byte   $0F
        .byte   $22
        and     #$1A
        .byte   $0F
        .byte   $0F
        bmi     LBF25
        ora     ($0F, x)
LBF15:  bmi     LBF2D
        .byte   $02
        .byte   $0F
        .byte   $27
        .byte   $17
        php
        .byte   $0F
LBF1D:  and     #$1A
        .byte   $04
        .byte   $0F
        bmi     LBF3F
        .byte   $0C
        .byte   $0F
LBF25:  bmi     LBF3D
        .byte   $02
        .byte   $0F
        .byte   $27
        .byte   $17
        php
        .byte   $0F
LBF2D:  rol     $16
        asl     $07
        bmi     LBF5A
        .byte   $0F
        .byte   $07
        bmi     LBF4D
        .byte   $0F
        .byte   $07
        .byte   $27
        .byte   $17
        .byte   $0F
        .byte   $07
LBF3D:  and     ($31), y
LBF3F:  .byte   $0F
        .byte   $03
        and     ($21), y
        .byte   $0F
        .byte   $03
        bmi     LBF5D
        .byte   $0F
        .byte   $03
        .byte   $3C
        .byte   $1C
        .byte   $0F
        .byte   $03
LBF4D:  rol     a
        .byte   $1A
        .byte   $0F
        .byte   $0C
        bmi     LBF64
        .byte   $0F
        .byte   $0C
        bmi     LBF6D
        .byte   $0F
        .byte   $0C
        .byte   $30
LBF5A:  and     ($0F, x)
        .byte   $0C
LBF5D:  bmi     LBF90
        .byte   $0F
        ora     ($0F, x)
        .byte   $0F
        .byte   $0F
LBF64:  ora     ($0F, x)
        .byte   $0F
        .byte   $0F
        ora     ($0F, x)
        .byte   $0F
        .byte   $0F
        .byte   $01
LBF6D:  .byte   $0F
        .byte   $0F
        .byte   $0F
        .byte   $FF
        bmi     LBF89
        .byte   $0F
        .byte   $FF
        sec
        bpl     LBF87
        .byte   $FF
        bmi     LBFA0
        .byte   $0F
        .byte   $FF
        bmi     LBF95
        .byte   $02
        .byte   $FF
        sec
        bpl     LBF86
        .byte   $FF
        .byte   $30
LBF86:  .byte   $25
LBF87:  .byte   $02
        .byte   $FF
LBF89:  bmi     LBFA1
        .byte   $0F
        .byte   $FF
        bmi     LBF9F
        .byte   $0F
LBF90:  .byte   $FF
        .byte   $2B
        bpl     LBFA3
        brk
LBF95:  asl     a
        .byte   $14
        sec
        eor     LCAAF, y
        .byte   $3F
        adc     ($71), y
        .byte   $71
LBF9F:  .byte   $71
LBFA0:  .byte   $71
LBFA1:  adc     ($16), y
LBFA3:  cpy     #$E6
        .byte   $E7
        inx
        inx
        inx
        inx
        inx
        sbc     #$FF
        rol     $4174
        .byte   $9E
        sbc     $5310, x
        .byte   $53
        dex
        dex
        dex
        dex
        .byte   $CB
        .byte   $CB
        .byte   $CB
        .byte   $CB
        .byte   $CB
LBFBE:  .byte   $CB
        .byte   $CB
        cpy     LCCCC
        cpy     LCCCC
        cpy     LCCCC
        cpy     LCDCC
        cmp     LCECE
        dec     LCFCF
        .byte   $CF
        asl     $4666
        .byte   $D2
        cmp     $C5
        dec     $C6
        brk
        sta     $D2, y
        brk
        sta     $9999, y
        brk
        .byte   $9F
        .byte   $9F
        .byte   $9F
        brk
        lda     ($A1, x)
        lda     ($00, x)
        .byte   $D3
        .byte   $9B
        .byte   $12
        brk
        .byte   $9F
        .byte   $9F
        sta     $00, y
        brk
        brk
        brk
        brk
        brk
        brk
        brk
        dec     $9D
        .byte   $9C
        brk
        ora     $9F
        brk
        brk
        .byte   $93
        sta     a:$C6, x
        brk
        lda     ($00, x)
        brk
        brk
        .byte   $C2
        brk
        brk
        brk
        .byte   $9F
        brk
        brk
        rti

        sta     a:$C6, x
        asl     $9F
        brk
        cpy     #$C0
        cpy     #$27
        .byte   $3B
        .byte   $4F
        cpy     #$C0
LC023:  cpy     #$31
LC025:  eor     $59
LC027:  .byte   $CF
        .byte   $CF
LC029:  .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $6B
        ror     $8D89
        .byte   $9E
        lda     #$A9
        lda     #$A9
        lda     #$CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        .byte   $CF
        lda     #$CC
        sbc     $F0
        sbc     $F5, x
        sbc     $F5, x
        sbc     $F5, x
        .byte   $CF
        .byte   $CF
        bne     LC023
        bne     LC025
        bne     LC027
        bne     LC029
        sbc     $FA, x
        ora     #$1C
        .byte   $4F
        .byte   $6B
        sei
        .byte   $82
        stx     $96, y
        ora     ($43, x)
        .byte   $80
        cpy     #$0C
        .byte   $5F
        sta     ($CA), y
        .byte   $12
        .byte   $67
        tya
        .byte   $D2
        .byte   $67
        .byte   $6B
        .byte   $04
        jsr     L741D
        .byte   $6B
        .byte   $C7
        .byte   $6B
        .byte   $C7
        .byte   $6B
        .byte   $C7
        .byte   $6B
        .byte   $C7
        .byte   $6B
        .byte   $C7
        .byte   $F2
        .byte   $C7
        .byte   $F2
        .byte   $C7
        .byte   $F2
        .byte   $C7
        .byte   $F2
        .byte   $C7
        .byte   $73
        iny
        eor     ($C8, x)
        eor     ($C8, x)
        ora     $CA
        jsr     L741D
        and     $C8
        and     $C8
        and     $C8
        and     $C8
        and     $C8
        and     $C8
        eor     $C9
        eor     $C9
        eor     $C9
        .byte   $7F
        .byte   $C7
        .byte   $7F
        .byte   $C7
        .byte   $7F
        .byte   $C7
        adc     $6DC9
        cmp     #$0C
        cmp     #$0C
        cmp     #$AD
        asl     $2005
        ora     $2F74, x
        cmp     #$3B
        cmp     #$93
        cmp     #$AB
        .byte   $C7
        dec     $ABC7
        .byte   $C7
        ldx     $BEC9, y
        cmp     #$BE
        cmp     #$CB
        cmp     #$CB
        cmp     #$BE
        cmp     #$BE
        cmp     #$BE
        cmp     #$BE
        cmp     #$BE
        cmp     #$4C
        ldx     $8AC9, y
        pha
        and     #$F0
        sta     $33
        txa
        asl     a
        asl     a
        asl     a
        asl     a
        sta     $29
        lda     #$0A
        sta     $15
        ldx     #$00
        stx     $12
        stx     $1F
        lda     #$3D
        sta     $90
        lda     #$01
        sta     $51
        tya
        pha
        jsr     LA116
        pla
        sta     $79
        lda     #$FF
        sta     $0441, x
        pla
        tax
        rts

        and     $2D2F
        .byte   $2F
        cpx     #$E2
        cpx     $E6
        cpx     #$E2
        cpx     $E6
        inx
        nop
        .byte   $EC
        .byte   $EE
LC11A:  inx
        nop
        cpx     $01EE
        .byte   $03
        ora     #$05
        .byte   $07
        .byte   $0B
        ora     $150F
        ora     ($13), y
        .byte   $17
        ora     ($03, x)
        ora     #$05
        ora     $011B, y
        .byte   $03
        ora     #$05
        ora     $1D1B, y
        .byte   $1F
        and     $21
        .byte   $23
        .byte   $27
        ora     $251F, x
        and     ($23, x)
        .byte   $27
        .byte   $9C
        .byte   $9E
        .byte   $9C
        .byte   $9E
        bne     LC11A
        .byte   $D4
        dec     $F0, x
        .byte   $F2
        .byte   $F4
        inc     $F0, x
        .byte   $F2
        sed
        .byte   $FA
LC152:  brk
        ora     $EB, x
        brk
LC156:  brk
        ora     ($FF, x)
        brk
        jsr     LB207
        lda     $10
        and     #$03
        bne     LC16D
        dec     $B1, x
        bne     LC16D
        sta     $04B2
        jmp     L9D61

LC16D:  lda     $04B2
        beq     LC180
        lda     $46
        bpl     LC180
        lda     #$00
        sta     $47, x
        sta     $04B2
        jmp     LC343

LC180:  lda     $5B, x
        and     #$20
        sta     $04B2
        bne     LC18C
        jmp     LC21B

LC18C:  lda     $3D, x
        beq     LC196
        lda     $6F, x
        and     #$01
        sta     $9D
LC196:  lda     $33, x
        sec
        sbc     #$1A
        sta     $32
        lda     $1F, x
        sbc     #$00
        sta     $1E
        lda     $28
        sec
        sbc     #$08
        sta     $29, x
        lda     $14
        sbc     #$00
        sta     $15, x
        ldy     #$01
        lda     $3D, x
        bmi     LC1B8
        ldy     #$FF
LC1B8:  sty     LC156
        lda     $F7
        and     #$03
        tay
        and     a:$5A
        bne     LC1D7
        lda     LC152, y
        cmp     $3D, x
        beq     LC1D4
        lda     $3D, x
        clc
        adc     LC156, y
        sta     $3D, x
LC1D4:  jmp     LC1DB

LC1D7:  lda     #$00
        sta     $3D, x
LC1DB:  ldy     #$01
        lda     $47, x
        bmi     LC1E3
        ldy     #$FF
LC1E3:  sty     LC156
        lda     #$20
        cmp     $042C
        lda     #$00
        rol     a
        asl     a
        asl     a
        asl     a
        and     $F7
        bne     LC208
        lda     $5B, x
        lsr     a
        lsr     a
        and     #$03
        sta     $00
        lda     $F7
        lsr     a
        lsr     a
        and     #$03
        tay
        and     $00
        beq     LC20C
LC208:  lda     #$00
        beq     LC219
LC20C:  lda     LC152, y
        cmp     $47, x
        beq     LC21B
        lda     $47, x
        clc
        adc     LC156, y
LC219:  sta     $47, x
LC21B:  jsr     LB17D
        jsr     LB178
        lda     $B1, x
        cmp     #$20
        bcs     LC22E
        lda     $10
        and     #$02
        bne     LC22E
        rts

LC22E:  jmp     LC343

        jsr     LA65E
        bmi     LC269
        ldx     $00
        ldy     $12
        lda     #$00
        sta     $3D, x
        sta     $47, x
        lda     #$3F
        sta     $90, x
        lda     $29, y
        sec
        sbc     #$08
        sta     $29, x
        lda     $15, y
        sbc     #$00
        sta     $15, x
        lda     $33, y
        clc
        adc     #$0E
        sta     $33, x
        lda     $1F, y
        adc     #$00
        sta     $1F, x
        jsr     L9839
        lda     #$A0
        sta     $B1, x
LC269:  ldx     $12
        rts

LC26C:  .byte   $02
        .byte   $02
        ora     ($01, x)
LC270:  .byte   $33
        .byte   $3B
        .byte   $3B
        .byte   $33
LC274:  ora     ($FF, x)
LC276:  php
        sed
LC278:  ora     ($FF, x)
LC27A:  .byte   $20
        .byte   $E0
LC27C:  .byte   $14
        cpx     LCC20
        ldy     $9FF6
        lda     $042F, x
        beq     LC294
        lda     $65, x
        ora     #$80
        sta     $65, x
        jsr     LC2F9
        jmp     L995F

LC294:  jsr     LAC1F
        lda     $B1, x
        beq     LC2B2
        dec     $47, x
        bpl     LC2AF
        lda     $33, x
        cmp     #$30
        bcs     LC2AF
        lda     #$00
        sta     $B1, x
        sta     $3D, x
        sta     $47, x
        dec     $86, x
LC2AF:  jmp     LC2F3

LC2B2:  lda     $86, x
        bne     LC2C7
        lda     #$30
        sta     $47, x
        jsr     LB168
        lda     LC27C, y
        sta     $3D, x
        inc     $B1, x
        jmp     LC2F9

LC2C7:  lda     $0480, x
        and     #$01
        tay
        lda     $47, x
        clc
        adc     LC274, y
        sta     $47, x
        cmp     LC276, y
        bne     LC2DD
        inc     $0480, x
LC2DD:  lda     $0477, x
        and     #$01
        tay
        lda     $3D, x
        clc
        adc     LC278, y
        sta     $3D, x
        cmp     LC27A, y
        bne     LC2F3
        inc     $0477, x
LC2F3:  jsr     LB178
        jsr     LB17D
LC2F9:  jsr     LAED4
        lda     $51, x
        sec
        sbc     #$01
        ora     $042F, x
        ora     $A8, x
        bne     LC342
        jsr     LABB2
        sty     a:$F4
        lda     #$45
        sta     $65, x
        lda     $29, x
        pha
        sec
        sbc     #$08
        sta     $29, x
        lda     $15, x
        pha
        sbc     #$00
        sta     $15, x
        jsr     L9C4C
        pla
        sta     $15, x
        pla
        sta     $29, x
        lda     $042C
        clc
        adc     #$0C
        sta     $042C
        lda     $0429
        sbc     #$07
        sta     $0429
        jsr     LC343
        lda     #$0D
        sta     $65, x
LC342:  rts

LC343:  lda     $10
        lsr     a
        lsr     a
        lsr     a
        and     #$03
        ldy     $3D, x
        bmi     LC350
        eor     #$03
LC350:  tay
        lda     LC26C, y
        sta     $6F, x
        lda     LC270, y
        jmp     LAEE0

        jsr     LA2FC
        lda     #$05
        sta     $0465, x
        rts

        jsr     LACCC
        lda     $045C, x
        beq     LC372
        inc     $9F, x
        jmp     LAE56

LC372:  jsr     LB207
        lda     #$02
        sta     $6F, x
        jsr     LAE56
        lda     $5B, x
        and     #$04
        beq     LC3E1
        jsr     LA929
        lda     $10
        and     #$FF
        bne     LC391
        lda     #$D8
        sta     $47, x
        bne     LC3E1
LC391:  lda     $10
        and     #$3F
        bne     LC39B
        lda     #$20
        sta     $86, x
LC39B:  ldy     $86, x
        bne     LC3BA
        inc     $B1, x
        lda     $B1, x
        and     #$20
        beq     LC3E0
        inc     $9F, x
        inc     $9F, x
        ldy     #$18
        lda     $B1, x
        and     #$40
        bne     LC3B5
        ldy     #$E8
LC3B5:  sty     $3D, x
        jmp     LB17D

LC3BA:  cpy     #$10
        bne     LC3E0
        jsr     LA65E
        bmi     LC3E0
        ldx     $00
        lda     #$35
        sta     $90, x
        lda     #$E0
        sta     $47, x
        lda     $33, x
        adc     #$03
        sta     $33, x
        jsr     L9839
        lda     #$FF
        sta     $86, x
        lda     #$E0
        sta     $3D, x
        ldx     $12
LC3E0:  rts

LC3E1:  jmp     L9967

        lda     $51, x
        cmp     #$01
        bne     LC3F3
        lda     $045C, x
        beq     LC3FC
        lda     #$4A
        sta     $65, x
LC3F3:  lda     #$B3
        sta     $046E, x
        lda     #$5B
        bne     LC405
LC3FC:  ldy     $86, x
        dey
        cpy     #$10
        bcs     LC40B
        lda     #$4F
LC405:  jsr     LAEE0
        jmp     LC432

LC40B:  jsr     LAED4
        lda     $86, x
        cmp     #$10
        bcc     LC432
        lda     #$01
        sta     $65, x
        lda     #$10
        sta     $046E, x
        lda     $0429
        clc
        adc     #$0B
        sta     $0429
        asl     $EE
        ldy     #$00
        sty     a:$F4
        lda     #$67
        jsr     LAEE0
LC432:  lda     #$43
        sta     $65, x
        lda     #$33
        sta     $046E, x
LC43B:  rts

        .byte   $FB
        ora     $20
        cmp     $A5AE, x
        inc     $0E29
        ora     $EF
        ora     $B1, x
        bne     LC466
        lda     $33, x
        sec
        sbc     #$02
        sta     $00
        ldy     $6F, x
        lda     $01
        clc
        adc     LC43B, y
        sta     $01
        jsr     LABB2
        ldx     #$6B
        jsr     LB01F
        ldx     $12
LC466:  rts

        lda     $B1, x
        bne     LC4C5
        lda     $A8, x
        beq     LC4A7
        lda     #$01
        sta     $90, x
        jsr     L9839
        jsr     LA662
        bmi     LC4A6
        ldy     $00
        lda     #$08
        sta     $90, y
        sta     $B1, y
        lda     $29, x
        sta     $29, y
        lda     $15, x
        sta     $15, y
        lda     $0441, x
        sta     $0441, y
        lda     #$FF
        sta     $0441, x
        lda     $3D, x
        sta     $3D, y
        tya
        tax
        jsr     L9839
        ldx     $12
LC4A6:  rts

LC4A7:  lda     $5B, x
        and     #$10
        beq     LC4C5
        inc     $B1, x
        sta     $9F, x
        jsr     LA662
        bmi     LC4C5
        ldy     $00
        lda     $3D, x
        sta     $3D, y
        lda     #$20
        sta     $0453, y
        jmp     LC4CB

LC4C5:  jsr     LAC1F
        jsr     LACCC
LC4CB:  jsr     LB207
        lda     $5B, x
        and     $6F, x
        beq     LC4D7
        jsr     LB1D6
LC4D7:  lda     $5B, x
        and     #$04
        beq     LC4F7
        lda     $042F, x
        beq     LC4EA
        lda     #$00
        sta     $042F, x
        jsr     L9887
LC4EA:  lda     $9F, x
        eor     #$08
        sta     $9F, x
        jsr     LA929
        lda     #$F0
        sta     $47, x
LC4F7:  inc     $0477, x
        lda     $B1, x
        bne     LC508
        lda     $0477, x
        and     #$3F
        bne     LC508
        jsr     L988A
LC508:  jsr     L995F
        jmp     LAE56

        inc     $FEFE, x
        inc     $B6B4, x
        lda     $B7, x
        clv
        .byte   $FA
        lda     $FAFA, y
        .byte   $FA
        .byte   $B2
        .byte   $B3
        ldx     LBFBE, y
        .byte   $BF
        .byte   $BF
        .byte   $BF
        .byte   $BF
        .byte   $BF
        lsr     a
LC527:  lsr     a
        .byte   $4B
        .byte   $4B
        .byte   $5E
LC52B:  .byte   $5F
        lsr     $A85F, x
LC52F:  tay
        lda     #$A9
        .byte   $A9
LC533:  lda     #$A9
        lda     #$45
LC537:  eor     $A9
        lda     #$A8
LC53B:  eor     $A9A9, x
        .byte   $74
LC53F:  ror     $75, x
        .byte   $77
        tya
LC543:  txs
        sta     $9C9B, y
LC547:  txs
        sta     $9C9B, x
LC54B:  .byte   $9E
        .byte   $9B
        .byte   $9F
        cli
LC54F:  .byte   $5A
        eor     $5E5B, y
        .byte   $5F
        lsr     $8E5F, x
        .byte   $8F
        .byte   $8F
        stx     $A6A6
        .byte   $A7
        .byte   $A7
        .byte   $92
        .byte   $93
        .byte   $93
        .byte   $92
        .byte   $74
        ror     $75, x
        .byte   $77
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        bcs     LC527
        .byte   $FA
        .byte   $FA
        bcs     LC52B
        .byte   $FA
        .byte   $FA
        bcs     LC52F
        .byte   $FA
        .byte   $FA
        bcs     LC533
        .byte   $FA
        .byte   $FA
        bcs     LC537
        .byte   $FA
        .byte   $FA
        bcs     LC53B
        .byte   $FA
        .byte   $FA
        bcs     LC53F
        .byte   $FA
        .byte   $FA
        bcs     LC543
        .byte   $FA
        .byte   $FA
        bcs     LC547
        .byte   $FA
        .byte   $FA
        bcs     LC54B
        .byte   $FA
        .byte   $FA
        bcs     LC54F
        ldy     #$A2
        lda     ($A3, x)
        .byte   $80
        .byte   $82
        sta     ($83, x)
        .byte   $F4
        stx     $F5
        .byte   $87
        sty     $86
        sta     $87
        .byte   $FC
        .byte   $FC
        .byte   $FC
        .byte   $FC
        lda     $ACFB
        lda     $ACAC
        ldy     $FBAC
        .byte   $3B
        .byte   $3B
        ldy     $FCFC
        .byte   $FC
        .byte   $FC
        .byte   $F4
        stx     $F5
        .byte   $87
        inc     $FEFE, x
        inc     $FEFE, x
        ora     $3CFE, x
        rol     $3F3D, x
        cli
        inc     $5A59, x
        .byte   $5B
        .byte   $5A
        inc     $FEFE, x
        eor     $5C5B, x
        inc     $5BFE, x
        .byte   $5A
        .byte   $1C
        inc     $FEFE, x
        inc     $FEFE, x
        inc     $1EFE, x
        inc     $201F, x
        .byte   $22
        and     ($23, x)
        .byte   $57
        .byte   $57
        .byte   $FB
        .byte   $FB
        .byte   $57
        .byte   $57
        inc     $ABFE, x
        .byte   $AB
        .byte   $FB
        .byte   $FB
        nop
        nop
        .byte   $FB
        .byte   $FB
        .byte   $7C
        ror     $7F7D, x
        dex
        cpy     LCDCB
        dex
        cpy     LCDCB
        cpy     #$C2
        cmp     ($C3, x)
        bit     $2D2E
        .byte   $2F
        stx     $8F8F
        stx     $8A88
        .byte   $89
        .byte   $8B
        .byte   $89
        .byte   $8B
        .byte   $89
        .byte   $8B
        .byte   $89
        .byte   $8B
        sty     $888D
        txa
        sty     $888D
        txa
        .byte   $89
        .byte   $8B
        dey
        txa
        .byte   $89
        .byte   $8B
        ror     a
        jmp     (L6D6B)

        jmp     (L6D6C)

        adc     $6E6C
        adc     $326F
        .byte   $34
        .byte   $33
        and     $33, x
        and     $33, x
        and     $94, x
        sta     $94, x
        sta     $96, x
        .byte   $97
        stx     $97, y
        pha
        eor     #$48
        eor     #$FE
        inc     $FEFE, x
        .byte   $FB
        .byte   $32
        .byte   $32
        .byte   $33
        .byte   $33
        .byte   $33
        .byte   $33
        .byte   $33
        sbc     $FDFD, x
        sbc     $FB34, x
LC664:  sbc     $FB34, x
        bmi     LC664
        .byte   $FB
        .byte   $FB
        .byte   $FB
        and     ($FB), y
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        lsr     $56, x
        lsr     $56, x
        .byte   $64
        ror     $65
        .byte   $67
        pla
        ror     a
        adc     #$6B
        .byte   $FA
        jmp     (L6CFA)

        adc     $6DFA
        .byte   $FA
        .byte   $92
        .byte   $93
        .byte   $93
        .byte   $92
        ldx     $AEAF
        .byte   $AF
        sei
        .byte   $7A
        adc     $A87B, y
        tay
        .byte   $AF
        ldx     $9594
        sty     $95, x
        stx     $97, y
        stx     $97, y
        .byte   $22
        bit     $23
        and     $92
        .byte   $93
        .byte   $93
        .byte   $92
        bvc     LC6F9
        bvc     LC6FB
        ldx     $AEAF
        .byte   $AF
        bvc     LC701
        bvc     LC703
        stx     $8F8F
        stx     $5250
        eor     ($53), y
        sbc     $FDFD, x
        sbc     $36FB, x
        rol     $4F, x
        .byte   $4F
        lsr     $4F4E
        lsr     $4F4F
        lsr     $9392
        .byte   $93
        .byte   $92
        stx     $8F8F
        stx     $4240
        eor     ($43, x)
        rti

        .byte   $42
        eor     ($43, x)
        tsx
        ldy     $BDBB, x
        tsx
        ldy     $9190, x
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        .byte   $FA
        sbc     $FDFD, x
        sbc     $6361, x
        adc     ($63, x)
        adc     $63
        adc     $63
        adc     $67
        .byte   $65
LC6F9:  .byte   $67
        rts

LC6FB:  .byte   $62
        adc     ($63, x)
        .byte   $64
        .byte   $62
        .byte   $65
LC701:  .byte   $63
        .byte   $64
LC703:  ror     $65
        .byte   $67
        pla
        .byte   $62
        adc     ($63, x)
        .byte   $64
        adc     #$65
        .byte   $67
        lsr     $62
        adc     ($63, x)
        .byte   $64
        .byte   $47
        adc     $67
        tsx
        ldy     $BDBB, x
        bvs     LC78E
        adc     ($73), y
        stx     $8F8F
        stx     $4544
        eor     $44
LC726:  .byte   $97
        .byte   $92
        .byte   $0C
        .byte   $0C
        ora     $99, x
        .byte   $9F
        ora     $45, x
        eor     $45
        eor     $63
        .byte   $63
        .byte   $63
        .byte   $63
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $13
        .byte   $9F
        brk
        .byte   $9C
        lda     ($9F, x)
        .byte   $9F
        .byte   $9F
        .byte   $9F
        .byte   $80
        .byte   $07
        sta     ($80, x)
        sta     ($81, x)
        sta     ($81, x)
LC74A:  lda     $050E
        asl     a
        asl     a
        sta     $0F
        lda     $050E
        cmp     #$07
        bcc     LC75E
        lda     $0543
        jmp     LC761

LC75E:  lda     $0542
LC761:  clc
        adc     $0F
        tax
        lda     LC726, x
        sta     ($01), y
        rts

        ldy     $E7
LC76D:  jsr     LC74A
        jsr     LBE0C
        dec     $050D
        bpl     LC76D
        rts

LC779:  .byte   $4F
        .byte   $4F
        .byte   $83
LC77C:  bvc     LC7CF
        .byte   $83
        ldy     $E7
        lda     $050E
        cmp     #$09
        bne     LC793
        lda     $04B0
        beq     LC793
        .byte   $EE
LC78E:  asl     $EE05
        .byte   $0E
        .byte   $05
LC793:  lda     $050E
        sec
        sbc     #$09
        tax
        lda     LC779, x
        sta     ($01), y
        jsr     LBE24
        lda     LC77C, x
        sta     ($01), y
        jsr     LBE38
        rts

        ldy     $E7
        lda     #$52
        sta     ($01), y
        iny
        lda     #$53
        sta     ($01), y
        lda     $E7
        clc
        adc     #$10
        tay
        lda     #$52
        sta     ($01), y
        iny
        lda     #$54
        sta     ($01), y
        iny
        lda     #$53
        sta     ($01), y
        jsr     LBE38
        rts

        .byte   $A4
LC7CF:  .byte   $E7
        lda     #$52
        sta     ($01), y
        dey
        lda     #$55
        sta     ($01), y
        ldy     $E7
        tya
        clc
        adc     #$10
        tay
        lda     #$52
        sta     ($01), y
        dey
        lda     #$54
        sta     ($01), y
        dey
        lda     #$55
        sta     ($01), y
        jsr     LBE38
        rts

        ldy     $E7
        lda     $050E
        cmp     #$06
        bne     LC802
        lda     #$9E
        sta     ($01), y
        jmp     LC805

LC802:  jsr     LC74A
LC805:  jsr     LBE24
        dec     $050D
        bpl     LC802
        rts

        ldy     $E7
LC810:  jsr     LC74A
        tya
        clc
        adc     #$11
        tay
        dec     $050D
        bpl     LC810
        rts

LC81E:  .byte   $67
        ror     a
        .byte   $9C
        cmp     ($70), y
        sty     $95, x
        lda     $050E
        tax
        cmp     #$05
        bne     LC833
        lda     $0543
        beq     LC833
        inx
LC833:  ldy     $E7
        lda     LC81E, x
        sta     ($01), y
        rts

LC83B:  .byte   $73
        .byte   $0D
LC83D:  .byte   $74
        .byte   $0E
LC83F:  adc     $0F, x
        ldy     $E7
        lda     $050E
        sec
        sbc     #$0A
        tax
        lda     LC83B, x
        sta     ($01), y
        dec     $050D
        beq     LC861
LC854:  jsr     LBE0C
        lda     LC83D, x
        sta     ($01), y
        dec     $050D
        bne     LC854
LC861:  jsr     LBE0C
        lda     LC83F, x
        sta     ($01), y
        rts

LC86A:  dex
        .byte   $CB
        cpy     LC8C7
        cmp     #$40
        .byte   $04
        ora     $A2
        brk
LC875:  stx     $0B
        ldx     $E8
        jsr     LBE01
        ldx     #$05
        ldy     $E7
        ldx     $0B
        ldy     $E7
        lda     $050D
        sta     $07
        jsr     LC8BA
        inx
        lda     $07
        beq     LC899
LC891:  jsr     LBE0C
        jsr     LC8BA
        bne     LC891
LC899:  jsr     LBE0C
        inx
        jsr     LC8BA
        lda     $E7
        clc
        adc     #$10
        cmp     #$F0
        bcs     LC8B0
        ldx     #$03
        sta     $E7
        jmp     LC875

LC8B0:  rts

LC8B1:  .byte   $C7
        iny
        .byte   $C9
LC8B4:  .byte   $CF
        .byte   $CD
        .byte   $CD
LC8B7:  dec     $D0CE
LC8BA:  stx     $08
        txa
        bne     LC8D2
        ldx     #$02
        lda     ($01), y
LC8C3:  cmp     LC8B1, x
        .byte   $F0
LC8C7:  ora     $CA
        bpl     LC8C3
        bmi     LC8EB
        lda     LC8B4, x
        bne     LC901
LC8D2:  ldx     $08
        cpx     #$02
        bne     LC8EB
        ldx     #$02
        lda     ($01), y
LC8DC:  cmp     LC8B1, x
        beq     LC8E6
        dex
        bpl     LC8DC
        bmi     LC8EB
LC8E6:  lda     LC8B7, x
        bne     LC901
LC8EB:  ldx     #$08
LC8ED:  lda     ($01), y
        cmp     LC86A, x
        beq     LC8FC
        dex
        bpl     LC8ED
        ldx     $08
        jmp     LC903

LC8FC:  ldx     $08
        lda     LC86A, x
LC901:  sta     ($01), y
LC903:  ldx     $08
        dec     $07
        rts

LC908:  cpy     #$76
LC90A:  .byte   $82
        .byte   $77
        lda     $050E
        sec
        sbc     #$0E
        sta     $07
        tax
        ldy     $E7
        lda     LC908, x
        sta     ($01), y
LC91C:  jsr     LBE24
        lda     ($01), y
        cmp     #$40
        bne     LC92E
        ldx     $07
        lda     LC90A, x
        sta     ($01), y
        bne     LC91C
LC92E:  rts

        ldy     $E7
        lda     #$01
        sta     ($01), y
        iny
        lda     #$02
        sta     ($01), y
        rts

        ldy     $E7
        lda     #$03
        sta     ($01), y
        rts

LC942:  adc     $7271
        ldy     $E7
        lda     $050E
        sec
        sbc     #$06
        tax
        lda     LC942, x
        sta     ($01), y
LC953:  jsr     LBE24
        lda     ($01), y
        cmp     #$40
        bne     LC963
        lda     #$6E
        sta     ($01), y
        jmp     LC953

LC963:  tya
        sec
        sbc     #$10
        tay
        lda     #$6F
        sta     ($01), y
        rts

LC96D:  ldy     $E7
        lda     $050E
        cmp     #$0D
        beq     LC97D
        lda     #$C1
        sta     ($01), y
LC97A:  jsr     LBE24
LC97D:  lda     ($01), y
        cmp     #$40
        bne     LC992
        lda     #$C2
        sta     ($01), y
        lda     $EC
        beq     LC98F
        cpy     #$E0
        bcs     LC992
LC98F:  jmp     LC97A

LC992:  rts

        ldy     $E7
        lda     #$C3
        sta     ($01), y
LC999:  tya
        sec
        sbc     #$10
        tay
        cmp     #$F0
        bcs     LC9AF
        lda     ($01), y
        cmp     #$40
        bne     LC9AF
        lda     #$C2
        sta     ($01), y
        jmp     LC999

LC9AF:  rts

LC9B0:  .byte   $43
        .byte   $44
        eor     $46
        .byte   $47
        pha
        eor     #$4A
        .byte   $4B
        lsr     $414C
        .byte   $5A
        .byte   $42
        ldy     $E7
        ldx     $050E
        lda     LC9B0, x
        sta     ($01), y
        rts

LC9C9:  bcc     LC96D
LC9CB:  lda     $050E
        sec
        sbc     #$09
        sta     $08
        ldy     $E7
        ldx     $E8
        jsr     LBE01
        ldy     $E7
        lda     #$05
        sta     $07
        lda     ($01), y
        cmp     #$40
        bne     LCA02
LC9E6:  ldx     $08
        lda     LC9C9, x
        sta     ($01), y
        jsr     LBE0C
        dec     $07
        bpl     LC9E6
        lda     $E7
        clc
        adc     #$10
        cmp     #$F0
        bcs     LCA02
        sta     $E7
        jmp     LC9CB

LCA02:  rts

LCA03:  .byte   $04
        ora     $A9
        brk
LCA07:  sta     $08
LCA09:  ldy     $E7
        ldx     $E8
        jsr     LBE01
        ldy     $E7
        lda     $050D
        sta     $07
        ldx     $08
LCA19:  lda     LCA03, x
        sta     ($01), y
        jsr     LBE0C
        dec     $07
        bpl     LCA19
        lda     #$01
        sta     $08
        lda     $E7
        clc
        adc     #$10
        cmp     #$F0
        bcs     LCA37
        sta     $E7
        jmp     LCA09

LCA37:  rts

        .byte   $80
        nop
        bpl     LCA4C
        .byte   $13
        bpl     LCA53
        ora     ($1A), y
        asl     $0E0B
        beq     LCA88
        nop
        .byte   $C2
        asl     $C1, x
        clc
        .byte   $F1
LCA4C:  .byte   $0C
        .byte   $C3
LCA4E:  .byte   $14
        cpy     $0D
        .byte   $0B
        .byte   $06
LCA53:  asl     $F1, x
        plp
        sbc     ($42), y
        .byte   $FF
        brk
        .byte   $F3
        bcc     LCA65
        jmp     (L5210)

        bpl     LCA7A
        .byte   $12
        sbc     $06, x
LCA65:  and     ($C3), y
        bpl     LCAAF
        bpl     LCA07
        bpl     LCAA1
        bpl     LCADF
        .byte   $FF
        ldy     $E2, x
        beq     LCAE0
        .byte   $34
        .byte   $E2
        sec
        sbc     ($3A, x)
        .byte   $E1
LCA7A:  .byte   $3C
        .byte   $E2
        .byte   $3A
        sbc     ($38, x)
        sbc     ($35, x)
        sbc     ($32, x)
        .byte   $E3
        .byte   $37
        sbc     ($37, x)
        .byte   $E4
LCA88:  .byte   $3B
        .byte   $E2
LCA8A:  bpl     LCA4E
        .byte   $2B
        .byte   $E2
        jsr     L1BC3
        .byte   $E2
        jsr     L12C4
        .byte   $0B
        asl     $01
        php
        .byte   $E3
        sbc     ($91), y
        .byte   $F2
        sbc     ($8A), y
        .byte   $F3
        .byte   $76
LCAA1:  cmp     ($27, x)
        .byte   $C2
        clc
        .byte   $0B
        asl     $20
        sbc     ($8B), y
        sbc     ($AB), y
        inc     $07, x
        .byte   $FF
LCAAF:  .byte   $89
        nop
        jsr     L8711
        .byte   $13
        asl     $19
        beq     LCA8A
LCAB9:  .byte   $F2
        sty     $5E, x
        ora     $F226
        .byte   $BB
LCAC0:  php
        ora     $060A
        asl     $F1, x
        bpl     LCAB9
        dex
        .byte   $FF
        .byte   $80
        inx
        .byte   $53
        bpl     LCB25
        bpl     LCAFB
        ora     ($2E), y
        bpl     LCAE7
LCAD5:  asl     a
        asl     $43
        .byte   $17
        ora     ($10), y
        cpx     $08
        bpl     LCAFB
LCADF:  .byte   $10
LCAE0:  .byte   $7C
        brk
        .byte   $1C
        .byte   $E2
        .byte   $27
        .byte   $0C
        .byte   $F5
LCAE7:  asl     $10
        .byte   $1F
        .byte   $E2
        .byte   $2F
        brk
        bpl     LCAD5
        .byte   $07
LCAF0:  .byte   $03
        php
        nop
        rts

        .byte   $E2
        rol     $E2
        .byte   $12
        .byte   $2B
        rol     $26
LCAFB:  .byte   $07
        .byte   $22
        .byte   $0D
        .byte   $E2
LCAFF:  asl     $C1, x
        jsr     $0100
        brk
        asl     a
        .byte   $E3
        adc     $22E3, y
        brk
        bpl     LCAF0
        .byte   $07
        .byte   $E3
        rol     $E2, x
        .byte   $34
        cmp     ($0A, x)
        .byte   $E2
        bvc     LCB44
        and     #$A8
        clc
        and     ($19, x)
        ora     $31
        and     ($3D, x)
        rol     $0E
        and     ($0F, x)
        .byte   $21
LCB25:  beq     LCB33
        beq     LCB74
        beq     LCAC0
        .byte   $F2
        lsr     $060B
        rti

        .byte   $80
        and     ($01, x)
LCB33:  and     ($02, x)
        and     ($F0, x)
        .byte   $8B
        sbc     ($28), y
        sbc     ($8B), y
        sbc     ($CA), y
        .byte   $FF
        sta     ($EA), y
        .byte   $32
        .byte   $12
        .byte   $41
LCB44:  .byte   $13
        asl     $35
        beq     LCB77
        .byte   $F2
        .byte   $67
        rol     $3D, x
        brk
        ora     $1400, x
        and     $10, x
        and     ($F0), y
        php
        sbc     ($59), y
        sbc     ($CA), y
        sbc     $07, x
        brk
        .byte   $F3
        rol     $83
LCB60:  jsr     L0D81
        .byte   $14
        asl     $30
        clc
        and     $09
        and     $F0
        .byte   $0B
        beq     LCBBC
        sbc     ($EA), y
        .byte   $FF
        .byte   $80
        nop
        tya
LCB74:  bpl     LCBA5
        .byte   $0E
LCB77:  .byte   $9C
        asl     a
        .byte   $07
        .byte   $23
        beq     LCAFF
        inc     $00, x
        sbc     ($04), y
        ror     $0E
        ora     $2A0E
        .byte   $0F
        .byte   $17
        .byte   $0F
        .byte   $53
        ldy     $33
        ora     $F5
        .byte   $07
        ora     ($F0), y
        .byte   $62
        .byte   $44
LCB93:  asl     LCD1C
        ora     ($0F), y
        asl     $0F, x
        ora     #$C4
        .byte   $52
        ldy     $32
        ora     $F5
LCBA1:  .byte   $07
        .byte   $12
        beq     LCC09
LCBA5:  .byte   $7C
        .byte   $0F
        asl     $4251, x
        ldy     $32
        ora     $F5
        .byte   $07
        .byte   $13
        beq     LCBF4
        lsr     $0E
        .byte   $0B
        asl     $0F31
        .byte   $52
        ldy     $32
        .byte   $05
LCBBC:  sbc     $07, x
        .byte   $14
        beq     LCC25
        rti

        asl     $0E05
        .byte   $92
        .byte   $A3
        .byte   $22
        ora     $F5
        .byte   $07
        ora     $F0, x
        dec     $F2
        rol     a
        asl     $0E1D
        .byte   $44
        .byte   $01
LCBD5:  ora     $01
        beq     LCBD5+8
        beq     LCB60
        beq     LCBA1
        sbc     ($03), y
        sbc     ($44), y
        .byte   $A3
        asl     $0E0C
        rol     $0F
        ora     ($0F), y
        asl     a
        .byte   $0F
        sbc     ($83), y
        sbc     ($C4), y
        .byte   $F2
        and     $0E
        .byte   $17
        .byte   $0E
LCBF4:  rol     $5225, x
        .byte   $A3
        .byte   $22
        ora     $F5
        .byte   $07
        .byte   $17
        .byte   $57
        rol     $09
        .byte   $22
        .byte   $0B
        .byte   $22
        .byte   $0C
        .byte   $2B
        asl     $C6, x
        .byte   $20
        .byte   $25
LCC09:  .byte   $12
        .byte   $02
        .byte   $03
LCC0C:  .byte   $02
LCC0D:  beq     LCC54
        beq     LCB93
        sbc     ($AA), y
        inc     $02, x
        .byte   $FF
        sta     ($EA), y
        .byte   $72
        ora     ($97), y
        and     $09
        and     ($0B, x)
        .byte   $21
LCC20:  ora     $1326
LCC23:  .byte   $02
        .byte   $04
LCC25:  .byte   $02
        ora     $02
        asl     $02
        asl     $0F02
        .byte   $02
        .byte   $2F
        and     $70F0
        beq     LCC23
        sbc     ($D0), y
        .byte   $3C
        ldy     $07F5
        ora     ($6C, x)
        ora     $40
        .byte   $02
        ora     ($02, x)
LCC41:  .byte   $0C
        ora     $F0
        .byte   $4F
        .byte   $F0
LCC46:  stx     $B1F1
        eor     $F5AC, x
        .byte   $07
        .byte   $02
        lsr     $25, x
        php
        and     $0A
        .byte   $25
LCC54:  .byte   $14
        .byte   $83
        ora     $F005
        ldy     $D5F0, x
        sta     $F5A9, x
        .byte   $07
        .byte   $03
        beq     LCC7A
        sbc     ($8F), y
        sbc     ($F7), y
        .byte   $F2
        ora     $F5A9
        .byte   $07
        .byte   $04
        stx     $41
        asl     $41, x
        beq     LCCA2
        beq     LCC0C
        beq     LCC46
        sbc     ($17), y
        .byte   $F1
LCC7A:  .byte   $4F
        sbc     ($CA), y
        adc     $F5A5
        .byte   $07
        ora     $65
        .byte   $32
        .byte   $3F
        .byte   $83
        .byte   $F0
LCC87:  .byte   $4F
        beq     LCC46
        sbc     ($15), y
        sbc     ($D3), y
        .byte   $89
        .byte   $83
        .byte   $23
        tay
        asl     $11A8
        ora     ($05, x)
        and     $06
        and     $0C
        and     $0D
        and     $13
        ora     $0E
        .byte   $05
LCCA2:  .byte   $32
        .byte   $83
        ora     $83
        ora     $3783
        .byte   $25
LCCAA:  ora     #$25
        beq     LCCCC-9
        .byte   $3D
LCCAF:  lda     $50
        and     $F5
        .byte   $07
        php
        beq     LCD33
        beq     LCC41
        beq     LCC87
        sbc     ($0E), y
        sbc     ($CA), y
        .byte   $FF
        sta     ($EA), y
        bmi     LCCD6
        .byte   $83
        brk
        .byte   $04
        brk
        ora     $00
        asl     $00
LCCCC:  .byte   $07
        brk
        php
        eor     #$F0
        .byte   $7C
        cld
        .byte   $47
        beq     LCD2B
LCCD6:  .byte   $F1
LCCD7:  .byte   $1C
        sbc     $08, x
        brk
        .byte   $F2
        beq     LCCEC
        .byte   $F2
        .byte   $43
        asl     a
        .byte   $07
        brk
        beq     LCCAF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        .byte   $FF
        sta     ($EA), y
        .byte   $10
LCCEC:  ora     ($F0), y
        sty     $F2, x
        pha
        .byte   $13
        php
        bpl     LCD49
LCCF5:  .byte   $83
        .byte   $13
        .byte   $82
        beq     LCD4A
        beq     LCCAA
        sbc     ($8A), y
        .byte   $FF
        .byte   $80
        sbc     ($30, x)
        brk
        and     $10, x
        and     #$11
        rol     $C2, x
        .byte   $17
        .byte   $0B
        php
        ora     ($1E, x)
        .byte   $0F
        .byte   $8B
        ora     ($17), y
        bpl     LCD46
        .byte   $2B
        .byte   $12
        .byte   $0F
        bpl     LCD28
        .byte   $13
        ora     ($05, x)
LCD1C:  .byte   $52
        php
        rol     $5C
        .byte   $19
LCD21:  eor     $10
        eor     #$0F
        .byte   $72
        .byte   $19
        php
LCD28:  ora     $0A96, y
LCD2B:  php
        .byte   $23
        .byte   $FF
        ldy     #$EA
        .byte   $33
        .byte   $19
        .byte   $55
LCD33:  ora     ($0B, x)
        and     $096C
        php
        and     $5CF0, y
        beq     LCDB3
        sbc     ($1C), y
        sbc     ($55), y
        .byte   $82
        tay
        .byte   $22
        .byte   $05
LCD46:  eor     $0A, x
        php
LCD49:  .byte   $46
LCD4A:  sbc     ($9C), y
        sbc     ($D5), y
        ldy     $080A
        .byte   $53
        ora     $1982
        sta     ($1A, x)
        .byte   $80
        .byte   $0B
        .byte   $80
        .byte   $0C
        .byte   $80
        beq     LCDBA
LCD5E:  beq     LCCF5
        beq     LCD5E
        .byte   $F2
        lda     $0814, y
        .byte   $13
        ora     ($2A), y
        .byte   $03
        rol     $05
        bit     $0C
        plp
        beq     LCD85
        sbc     ($CA), y
        .byte   $FF
        jsr     L93E0
        ora     $AB15, y
        clc
        asl     a
        php
        bvs     LCDA5
        .byte   $80
        .byte   $07
        .byte   $80
        php
        .byte   $80
        .byte   $09
LCD85:  .byte   $80
        asl     a
        .byte   $80
        beq     LCDBF
        cpy     $31
        rol     $33, x
        .byte   $3A
        .byte   $32
        .byte   $34
        and     #$16
        .byte   $33
        .byte   $33
        .byte   $32
        rol     $33, x
        .byte   $3A
        .byte   $33
        .byte   $36
LCD9B:  .byte   $33
        .byte   $42
        .byte   $34
        .byte   $12
        bmi     LCDA4
        and     #$05
        .byte   $29
LCDA4:  .byte   $06
LCDA5:  bmi     LCDB4
        and     #$12
        bmi     LCD9B
        .byte   $14
        .byte   $12
        bmi     LCDB3
        plp
        ora     #$32
        .byte   $12
LCDB3:  .byte   $34
LCDB4:  .byte   $1A
        and     #$16
        and     #$0C
        .byte   $84
LCDBA:  ora     $2884
        .byte   $83
        .byte   $09
LCDBF:  .byte   $83
        bit     $81
        ora     $81
        .byte   $42
        sec
        ora     $080A
        rti

        .byte   $0E
LCDCB:  tay
LCDCC:  rol     $F105
        .byte   $B7
        .byte   $87
        bit     $AF69
        beq     LCE2A
        sbc     ($D6), y
        ror     $AE, x
        sta     $F0A5, y
        .byte   $57
        .byte   $83
        brk
        .byte   $0C
        brk
        .byte   $17
        and     ($16), y
        .byte   $33
        .byte   $15
LCDE7:  and     $16, x
        .byte   $33
        ora     $080A, x
        .byte   $43
        and     $F0AC, x
        lsr     $F0, x
        sty     $F1, x
        ldx     $F1, y
        cmp     $16, x
        .byte   $33
        eor     $35
        ora     $30, x
        asl     a
        bmi     LCE16
        bmi     LCE0D
        bmi     LCE1A
        and     $46, x
        .byte   $33
        sec
        .byte   $82
        ora     ($A9), y
        .byte   $07
LCE0D:  .byte   $80
        ora     #$80
        .byte   $3B
        sty     $29
LCE13:  .byte   $80
        asl     a
        .byte   $80
LCE16:  .byte   $0C
        .byte   $80
        .byte   $0D
        .byte   $80
LCE1A:  .byte   $37
        sty     $25
        .byte   $80
        asl     $80
        php
        .byte   $80
        ora     #$80
        .byte   $32
        and     ($F0), y
        .byte   $14
        .byte   $3C
        .byte   $82
LCE2A:  .byte   $1B
        .byte   $80
        ora     $3D80
        and     ($3C), y
        .byte   $82
        .byte   $1B
        .byte   $80
        ora     $2380
        asl     a
        php
        jsr     L1AF0
        sbc     ($74), y
        sbc     ($A0), y
        .byte   $FF
        jsr     L67E0
        ora     $30AE, y
        .byte   $12
        asl     a
        php
LCE4A:  .byte   $33
        asl     $2AAE
        .byte   $87
        .byte   $0B
        .byte   $87
        beq     LCDE7
        sbc     ($BA), y
        bit     $1C31
        lda     #$52
        .byte   $AF
        .byte   $04
        ldy     $06
        ldy     $08
        ldy     $0A
        ldy     $F0
        .byte   $D4
        .byte   $93
        .byte   $3B
        tax
        and     ($3A), y
        .byte   $32
        sbc     ($5A), y
        .byte   $3C
        .byte   $32
        pla
        and     ($36), y
        .byte   $33
        .byte   $12
        asl     a
        php
        rol     $F0, x
        .byte   $1B
LCE79:  beq     LCE13
        .byte   $F1
LCE7C:  .byte   $BB
LCE7D:  ldx     $31
        pha
        and     ($36), y
        .byte   $33
        beq     LCE7D
        ror     $31
        rol     $33, x
        pla
        and     ($F0), y
        .byte   $3B
        sbc     ($58), y
        rol     $33, x
        ror     $31
        rol     $33, x
        .byte   $1A
        asl     a
        php
        and     ($F0, x)
        .byte   $9B
        sbc     ($A0), y
        .byte   $FF
        jsr     L37E0
LCEA1:  ora     $37A0, y
        .byte   $1B
        asl     a
        php
        rts

        .byte   $27
        sec
        beq     LCE4A
        beq     LCEA1
        sbc     ($5E), y
        sbc     ($D3), y
        rol     $04
        bpl     LCEED
        .byte   $37
        sec
        rol     $04
        bpl     LCEF3
        .byte   $37
        sec
        rol     $04
        bpl     LCEF9
        beq     LCF02
        beq     LCE79
LCEC6:  beq     LCEC6
        sbc     ($73), y
        sbc     ($BE), y
        .byte   $37
LCECD:  sec
LCECE:  rol     $04
        bpl     LCF09
        .byte   $37
        sec
        rol     $04
        bpl     LCF0F
        .byte   $37
        sec
        beq     LCF2F
        .byte   $F0
LCEDD:  .byte   $9E
        sbc     ($13), y
        sbc     ($5E), y
        sbc     ($D3), y
        rol     $04
        bpl     LCF1F
        .byte   $37
        sec
        rol     $04
        .byte   $10
LCEED:  .byte   $37
        .byte   $43
        asl     a
        php
        .byte   $22
        .byte   $F0
LCEF3:  rol     $B3F0, x
LCEF6:  beq     LCEF6
        .byte   $F1
LCEF9:  eor     $A0F1, y
LCEFC:  .byte   $FF
        jsr     L07E0
        .byte   $19
        .byte   $54
LCF02:  bit     $2C0C
        .byte   $57
        .byte   $32
        .byte   $14
        asl     a
LCF09:  php
        bvc     LCEFC
        eor     ($F1, x)
        .byte   $A0
LCF0F:  .byte   $FF
        .byte   $80
        sbc     $1A40, x
        .byte   $3A
        bpl     LCF3E
        ora     ($35), y
        .byte   $34
        ora     $34, x
        ora     $34, x
        .byte   $15
LCF1F:  .byte   $34
        .byte   $07
        asl     a
        php
        bmi     LCF3A
        and     ($08), y
        and     ($0E), y
        bit     $6A
        bpl     LCF52
        ora     ($54), y
LCF2F:  rti

        ora     $6B
        txa
        bpl     LCF63
        ora     ($50), y
        .byte   $6F
        beq     LCF42
LCF3A:  tya
        bpl     LCF9D
        .byte   $63
LCF3E:  asl     $68
        .byte   $0F
        rti

LCF42:  .byte   $F2
        .byte   $29
LCF44:  bpl     LCF52+8
        bpl     LCEDD
        .byte   $42
        beq     LCF98
        sbc     ($6B), y
        sbc     ($E8), y
        sbc     $08, x
        .byte   $80
LCF52:  .byte   $FF
        tax
        sed
        .byte   $17
        .byte   $22
        ror     $F0A4
        .byte   $5C
        beq     LCF3A
        sbc     ($5C), y
        sbc     ($D9), y
        .byte   $F2
        .byte   $65
LCF63:  ldy     $2A
        .byte   $33
        beq     LCF44
        sbc     ($F8), y
        .byte   $FF
        ora     ($01, x)
        ora     ($03, x)
        .byte   $47
        php
        .byte   $03
        lsr     $0E
        .byte   $03
        lsr     $02
        ora     $12
        .byte   $92
        .byte   $47
        ora     #$03
        .byte   $03
        sbc     $05
        .byte   $03
        and     ($03, x)
        .byte   $87
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        .byte   $03
        .byte   $03
        .byte   $2B
        .byte   $03
        .byte   $23
        .byte   $12
        .byte   $03
        and     ($95, x)
        .byte   $03
LCF98:  .byte   $03
        adc     $03
        .byte   $22
        .byte   $05
LCF9D:  ora     ($01, x)
        .byte   $07
        eor     ($93, x)
        .byte   $1C
        lda     $BA42, y
        ora     ($01, x)
        ora     ($05, x)
        rti

        .byte   $42
        .byte   $47
        rti

        ora     $08
        and     $46
        cpy     #$01
        .byte   $03
        lsr     $00
        ora     $23
        inc     $47
        .byte   $80
        ora     ($05, x)
        php
        and     $08, x
        eor     $05, x
        asl     $A4
        asl     $CB
        ora     ($03, x)
        asl     $96
        ora     ($07, x)
        .byte   $04
        .byte   $3C
LCFCF:  .byte   $04
        jmp     (LEC04)

        .byte   $03
        .byte   $04
        .byte   $1C
        ora     ($01, x)
        .byte   $03
        .byte   $03
        and     #$07
        .byte   $02
        and     #$02
        eor     #$02
        jmp     (L0101)

        ora     ($05, x)
        .byte   $1C
        ldx     $42, y
        ldx     $03, y
        eor     ($FA, x)
        ora     ($01, x)
        ora     ($01, x)
        .byte   $03
        .byte   $5C
        .byte   $C7
        ora     ($03, x)
        rti

        lsr     a
        ora     ($01, x)
        ora     ($01, x)
        .byte   $07
        asl     a
        .byte   $14
        php
        cli
        asl     a
        sty     $01
        ora     ($01, x)
        ora     ($01, x)
        ora     ($03, x)
        .byte   $2F
        adc     $05
        asl     $0E75
        sta     $09, x
        rol     $2F05
        eor     $0D, x
        ldy     LCC0D
        ora     ($01, x)
        ora     ($09, x)
        .byte   $2F
        .byte   $72
        bit     $2C84
        .byte   $B7
        rol     $074C
        bit     $2D81
        .byte   $1A
        .byte   $2F
        .byte   $DF
        .byte   $07
        bit     $2C35
        stx     $2C, y
        clc
        .byte   $03
        .byte   $2F
        adc     ($03), y
        .byte   $2F
        .byte   $CF
        .byte   $07
        and     $2F20
        bvs     LD06C
        ror     $2D07, x
        adc     ($2E), y
        sta     $7F2F
        ora     $2E
        .byte   $34
        rol     $01C7
        ora     ($03, x)
        asl     $6C
        ora     $21
        sta     LCD21
        ora     $02
        .byte   $57
        .byte   $02
        sbc     $0501
        ora     $C2
        .byte   $07
        .byte   $3B
        ora     $07
        cpy     $03
        and     $0E03, x
        dec     $01, x
        .byte   $03
LD06C:  ora     $6F, x
        .byte   $03
        ora     $6B, x
        .byte   $03
        ora     $67, x
        .byte   $03
        ora     $63, x
        ora     ($09, x)
        .byte   $2F
        .byte   $73
        and     $2F84
        adc     $893B, y
        ora     ($01, x)
        ora     $0E
        cmp     #$0E
        sbc     #$03
        ora     $05E9
        ora     $0D09
        and     #$05
        .byte   $42
        .byte   $6B
        eor     ($AA, x)
        ora     ($03, x)
        and     $0522
        and     $5D62
        dec     $01, x
        ora     ($C1, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($07, x)
        .byte   $3B
        .byte   $74
        .byte   $27
        .byte   $57
        .byte   $27
        .byte   $97
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     $27
        adc     $27
        sta     $05, x
        .byte   $42
        rol     a
        eor     ($87, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($03, x)
        pla
        bit     $01
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($01, x)
        ora     ($08, x)
        dey
        dey
        dey
        clv
        .byte   $80
        brk
        rti

        .byte   $9C
