.include	"mem.i"

.segment	"CHAPT_02DATA"

L00F6		:= $00F6
L0F05		:= $0F05
L6CFA		:= $6CFA
L6D6B		:= $6D6B
L6D6C		:= $6D6C
L741D		:= $741D
L8487		:= $8487
L8B19		:= $8B19
L971A		:= $971A
L9839		:= $9839
L9855		:= $9855
L988A		:= $988A
L995F		:= $995F
L9967		:= $9967
L9C4C		:= $9C4C
LA046		:= $A046
LA116		:= $A116
LA319		:= $A319
LA662		:= $A662
LA677		:= $A677
LA93A		:= $A93A
LABB2		:= $ABB2
LAC1F		:= $AC1F
LAC28		:= $AC28
LACCC		:= $ACCC
LADC9		:= $ADC9
LAE56		:= $AE56
LAED4		:= $AED4
LAEE0		:= $AEE0
LB01F		:= $B01F
LB168		:= $B168
LB178		:= $B178
LB17D		:= $B17D
LB1D6		:= $B1D6
LB207		:= $B207
LBE01		:= $BE01
LBE0C		:= $BE0C
LBE24		:= $BE24
LBE38		:= $BE38
LF4F0		:= $F4F0
	ora	($30), y
	rol	a
	.byte	$0F
	ora	($30), y
	asl	$0F, x
	ora	($28), y
	clc
	.byte	$0F
	ora	($17), y
	.byte	$07
	.byte	$0F
	.byte	$0F
	bmi	LBF3D
	asl	a
	.byte	$0F
	bmi	LBF2D
	.byte	$02
	.byte	$0F
	plp
	clc
	php
	.byte	$0F
	.byte	$17
	.byte	$07
	php
	.byte	$0F
	rol	a
	.byte	$1A
	asl	a
	.byte	$0F
	bmi	LBF3D
	.byte	$02
	.byte	$0F
	plp
	clc
	php
	.byte	$0F
LBF2D:	.byte	$27
	.byte	$17
	.byte	$07
	.byte	$07
	bmi	LBF5A
	.byte	$0F
	.byte	$07
	bmi	LBF4D
	.byte	$0F
	.byte	$07
	plp
	.byte	$17
	.byte	$0F
	.byte	$07
LBF3D:	and	($11), y
	.byte	$0F
	.byte	$0C
	rol	a
	.byte	$1A
	.byte	$0F
	.byte	$0C
	bmi	LBF5D
	.byte	$0F
	.byte	$0C
	.byte	$17
	.byte	$07
	.byte	$0F
	.byte	$0C
LBF4D:	and	$15
	.byte	$0F
	.byte	$0C
	bmi	LBF6D
	.byte	$0F
	.byte	$0C
	bmi	LBF6D
	.byte	$0F
	.byte	$0C
	.byte	$30
LBF5A:	rol	a
	.byte	$0F
	.byte	$0C
LBF5D:	bmi	LBF99
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	.byte	$01
LBF6D:	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$FF
	bmi	LBF89
	.byte	$0F
	.byte	$FF
	sec
	rol	a
	.byte	$0F
	.byte	$FF
	bmi	LBFA0
	.byte	$0F
	.byte	$FF
	bmi	LBF95
	.byte	$02
	.byte	$FF
	sec
	rol	a
	.byte	$02
	.byte	$FF
	bmi	LBFAC
	.byte	$02
	.byte	$FF
LBF89:	bmi	LBFA1
	.byte	$0F
	.byte	$FF
	bmi	LBF9F
	.byte	$0F
	.byte	$FF
	bmi	LBFB6
	.byte	$0F
	brk
LBF95:	asl	a
	.byte	$14
	.byte	$7D
	.byte	$0B
LBF99:	and	$35, x
	eor	$3535, y
	.byte	$35
LBF9F:	.byte	$35
LBFA0:	.byte	$35
LBFA1:	.byte	$82
	sta	$8139, y
	cpy	$F5F5
	sbc	$F5, x
	sbc	$20, x
LBFAC:	.byte	$32
	sbc	#$01
	lsr	$7D, x
	sbc	($0A, x)
	asl	a
	asl	a
	dex
LBFB6:	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
LBFBE:	.byte	$CB
	.byte	$CB
	.byte	$CB
	cpy	LCCCC
	cpy	LCCCC
	cpy	LCDCC
	cmp	LCECD
	dec	LCECE
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$5C
	ldy	$2094, x
	cmp	$C5
	dec	$C7
	brk
	sta	$9999, y
	brk
	txa
	txa
	txa
	brk
	.byte	$8B
	.byte	$8B
	.byte	$8B
	brk
	.byte	$9F
	.byte	$9F
	.byte	$9F
	brk
	lda	($A1, x)
	lda	($00, x)
	.byte	$D3
	.byte	$9B
	.byte	$12
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	.byte	$9C
	sta	a:$C6, x
	.byte	$93
	.byte	$9F
	brk
	brk
	rti

	.byte	$9B
	rti

	brk
	.byte	$93
	sta	a:$C6, x
	rti

	sta	a:$C6, x
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	brk
	cpy	#$C0
	cpy	#$27
	.byte	$3B
	.byte	$4F
	cpy	#$C0
	.byte	$C0
LC024:	and	($45), y
LC026:	.byte	$59
	.byte	$CF
LC028:	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	and	$44
	eor	$55, x
	.byte	$5F
	eor	$55, x
	.byte	$55
LC039:	eor	$55, x
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	jmp	(LA677)

	lda	$C8, x
	cmp	$D5, x
	cmp	$D5, x
	cmp	$CF, x
	.byte	$CF
	.byte	$CF
	bne	LC024
	bne	LC026
	bne	LC028
	bne	LC039
	.byte	$E3
	inc	$2301, x
	bmi	LC0B0
	.byte	$57
	.byte	$57
	.byte	$57
	ora	($43, x)
	.byte	$80
	cpy	#$0C
	.byte	$5F
	sta	($CA), y
	.byte	$12
	.byte	$67
	tya
	.byte	$D2
	.byte	$67
	.byte	$6B
	ora	($20, x)
	ora	$B974, x
	.byte	$C7
	lda	$B9C7, y
	.byte	$C7
	lda	$B9C7, y
	.byte	$C7
	rti

	iny
	rti

	iny
	rti

	iny
	rti

	iny
	cmp	($C8, x)
	.byte	$8F
	iny
	.byte	$8F
	iny
	and	($CA, x)
	jsr	L741D
	.byte	$73
	iny
	.byte	$73
	iny
	.byte	$73
	iny
	.byte	$73
	iny
	.byte	$73
	iny
	.byte	$73
	iny
	cpy	LCCBE
	ldx	$BECC, y
	cmp	LCDC7
	.byte	$C7
	cmp	$AFC7
	cmp	#$AF
	cmp	#$56
	.byte	$C9
LC0B0:	jmp	(LADC9)

	asl	$2005
	ora	$F374, x
	ldx	LC9A8, y
	cmp	$C9, x
	sbc	$1CC7, y
	iny
	sbc	$C7, y
	dex
	brk
	dex
	brk
	dex
	.byte	$0B
	dex
	.byte	$47
	dex
	brk
	dex
	brk
	dex
	brk
	dex
	brk
	dex
	brk
	dex
	jmp	LCA00

	txa
	pha
	and	#$F0
	sta	$33
	txa
	asl	a
	asl	a
	asl	a
	asl	a
	sta	$29
	lda	#$0A
	sta	$15
	ldx	#$00
	stx	$12
	stx	$1F
	lda	#$3D
	sta	$90
	lda	#$01
	sta	$51
	tya
	pha
	jsr	LA116
	pla
	sta	$79
	lda	#$FF
	sta	$0441, x
	pla
	tax
	rts

	.byte	$0F
	ora	($13), y
	ora	$21, x
	ora	($13), y
	ora	$17, x
	ora	$171B, y
	ora	$091D, y
	.byte	$0B
	ora	($03, x)
	ora	$07
	eor	$59, x
	.byte	$5B
	eor	$F2F0, x
	.byte	$F4
	inc	$45, x
	eor	$5D5B, y
	eor	$59
	.byte	$5B
	eor	$EAE8, x
	cpx	$ECEE
	inc	$EEEC
	jsr	L9855
	lda	#$40
	sta	$0477, x
	lda	#$02
	sta	$0465, x
	jmp	LA319

LC146:	brk
	.byte	$FF
	inc	$FCFD, x
	.byte	$FB
	.byte	$FA
	sbc	$F9F8, y
	.byte	$FA
	.byte	$FB
	.byte	$FC
	sbc	$FFFE, x
LC156:	.byte	$0B
	.byte	$0C
	ora	$100F
	.byte	$12
	.byte	$14
	.byte	$17
	.byte	$1A
	ora	$201F, x
LC162:	.byte	$E2
	.byte	$E2
	.byte	$E2
	.byte	$E3
	cpx	$E5
	.byte	$E7
	sbc	#$ED
	sbc	($F8), y
	brk
LC16E:	rts

	jsr	LACCC
	ldy	#$00
	lda	$0477, x
	asl	a
	bcc	LC181
	ldy	#$02
	asl	a
	bcc	LC181
	ldy	#$FE
LC181:	sty	$3D, x
	jsr	LB17D
	inc	$0477, x
	lda	$B1, x
	clc
	adc	#$D0
	sta	$B1, x
	bcc	LC195
	inc	$0480, x
LC195:	lda	$EF
	bne	LC16E
	lda	#$49
	sta	$65, x
	lda	#$2F
	ldy	$045C, x
	sty	$07
	beq	LC1A8
	lda	#$33
LC1A8:	ldy	#$30
	sty	a:$F4
	jsr	LAEE0
	lda	#$09
	sta	$65, x
	lda	#$33
	sta	$046E, x
	lda	$29, x
	pha
	sec
	sbc	#$08
	sta	$29, x
	jsr	L9C4C
	ldx	#$37
	lda	$10
	and	#$20
	bne	LC1D4
	lda	#$04
	and	$10
	beq	LC1D4
	ldx	#$3A
LC1D4:	lda	$01
	sec
	sbc	#$08
	sta	$01
	lda	#$20
	sta	$0C
	ldy	#$E0
	jsr	LB01F
	ldx	$12
	lda	$29, x
	sec
	sbc	#$08
	sta	$29, x
	jsr	L9C4C
	pla
	sta	$29, x
	lda	#$13
	sta	$046E, x
	lda	$33, x
	sta	$00
	lda	$0477, x
	and	#$78
	lsr	a
	lsr	a
	lsr	a
	tay
	lda	LC146, y
	adc	$0429
	adc	#$F0
	sta	$01
	ldx	#$3D
	lda	$07
	bne	LC221
	ldx	#$3F
	dey
	dey
	dey
	dey
	cpy	#$07
	bcs	LC221
	ldx	#$41
LC221:	ldy	#$00
	jsr	LB01F
	ldx	$12
	lda	$33, x
	clc
	adc	#$10
	sta	$00
	lda	$0480, x
	and	#$78
	lsr	a
	lsr	a
	lsr	a
	tay
	lda	LC146, y
	adc	$0429
	adc	#$F0
	sta	$01
	lda	#$00
	sta	$0C
	ldx	#$3D
	lda	$07
	bne	LC258
	ldx	#$3F
	dey
	dey
	dey
	dey
	cpy	#$07
	bcs	LC258
	ldx	#$41
LC258:	ldy	#$08
	jsr	LB01F
	ldx	$12
	lda	#$13
	sta	$046E, x
	lda	$EE
	bne	LC296
	lda	$33, x
	clc
	adc	#$10
	sta	$0258
	lda	#$0D
	sta	$0259
	sta	$025D
	lda	#$01
	sta	$025A
	sta	$025E
	lda	$01
	clc
	adc	#$10
	sta	$025B
	lda	$33, x
	sta	$025C
	lda	$0429
	sec
	sbc	#$08
	sta	$025F
LC296:	lda	#$00
	sta	$05
	lda	$0477, x
	jsr	LC2A5
	inc	$05
	lda	$0480, x
LC2A5:	and	#$67
	cmp	#$40
	bne	LC2FC
	lda	$045C, x
	bne	LC2FC
	jsr	LA662
	bmi	LC2FC
	lda	#$01
	sta	byte_602
	ldy	$00
	lda	#$20
	sta	$90, y
	sta	$79, y
	sta	$B1, y
	lda	$29, x
	sbc	#$18
	sta	$29, y
	lda	$05
	beq	LC2DA
	lda	$33, x
	clc
	adc	#$10
	sta	$33, y
LC2DA:	lda	$28
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	and	#$0F
	cmp	#$0B
	bcc	LC2E8
	lda	#$0B
LC2E8:	tax
	lda	LC156, x
	sta	$47, y
	lda	LC162, x
	sta	$3D, y
LC2F5:	tya
	tax
	jsr	L9839
	ldx	$12
LC2FC:	rts

	jsr	L9855
	lda	$33, x
	sec
	sbc	#$08
	sta	$79, x
	rts

	jsr	LACCC
	jsr	LAC28
	jsr	LAC1F
	jsr	LB207
	lda	$0480, x
	bne	LC349
	sta	$3D, x
	jsr	LB168
	lda	$0F
	adc	#$40
	cmp	#$80
	bcs	LC32F
	inc	$0480, x
	lda	#$C0
	sta	$47, x
	bne	LC349
LC32F:	inc	$0477, x
	ldy	#$FC
	lda	$0477, x
	and	#$20
	beq	LC33D
	ldy	#$04
LC33D:	sty	$47, x
	jsr	LB178
	lda	#$61
	sta	$65, x
	jmp	LAE56

LC349:	lda	$47, x
	bmi	LC35C
	lda	$79, x
	sec
	sbc	#$18
	cmp	$33, x
	bcs	LC35C
	sta	$33, x
	lda	#$00
	sta	$47, x
LC35C:	jsr	L995F
	inc	$9F, x
	lda	$9F, x
	pha
	and	#$3F
	bne	LC36B
	jsr	L988A
LC36B:	pla
	bne	LC373
	lda	#$18
	sta	$0453, x
LC373:	lda	$5B, x
	and	#$03
	beq	LC37C
	jsr	LB1D6
LC37C:	lda	#$41
	ldy	$47, x
	bpl	LC384
	lda	#$61
LC384:	jmp	LC409

	jsr	LACCC
	jsr	LAC28
	jsr	LAC1F
	jsr	LB207
	lda	$5B, x
	and	#$08
	beq	LC39E
	lda	$79, x
	sta	$33, x
	rts

LC39E:	jsr	LB168
	iny
	sty	$6F, x
	lda	$B1, x
	bne	LC3C7
	lda	$86, x
	bne	LC3B5
	lda	#$D0
	sta	$47, x
	inc	$B1, x
	jmp	LC404

LC3B5:	ldy	#$FC
	lda	$10
	and	#$20
	beq	LC3BF
	ldy	#$04
LC3BF:	sty	$47, x
	jsr	LB178
	jmp	LC407

LC3C7:	inc	$9F, x
	lda	$47, x
	bmi	LC404
	bne	LC3D4
	lda	#$10
	sta	$0453, x
LC3D4:	lda	$47, x
	bmi	LC3F4
	lda	$5B, x
	and	#$04
	beq	LC3F4
	lda	$0E
	sec
	sbc	#$6D
	cmp	#$06
	bcc	LC3F4
	lda	#$02
	sta	$51, x
	lda	#$E0
	sta	$47, x
	lda	#$40
	sta	byte_602
LC3F4:	lda	$79, x
	cmp	$33, x
	bcs	LC404
	sta	$33, x
	lda	#$00
	sta	$B1, x
	lda	#$A0
	sta	$86, x
LC404:	jsr	L9967
LC407:	lda	#$61
LC409:	sta	$65, x
	lda	$0453, x
	beq	LC41C
	cmp	#$05
	bne	LC417
	jsr	LA93A
LC417:	lda	#$4B
	jmp	LAEE0

LC41C:	jmp	LAE56

	jsr	L9855
	lda	#$03
	sta	$79, x
	rts

LC427:	.byte	$02
	.byte	$04
	ora	$B50E
	adc	$09D0, y
	jsr	LACCC
	jsr	LAC1F
	jsr	LAC28
	lda	$5B, x
	and	#$10
	beq	LC445
	jsr	LC449
	inc	$042F, x
	rts

LC445:	lda	$A8, x
	beq	LC4A4
LC449:	lda	$79, x
	beq	LC4A4
	sta	$0477, x
	lda	#$00
	sta	$79, x
	lda	#$02
	sta	$0489, x
	lda	$0441, x
	sta	$06
	lda	#$FF
	sta	$0441, x
	jsr	LA662
	bmi	LC4A4
	ldy	$00
	lda	#$1A
	sta	$90, y
	jsr	LC2F5
	ldy	$00
	lda	$06
	sta	$0441, y
	lda	$0477, x
	sec
	sbc	#$01
	sta	$79, y
	tay
	lda	LC427, y
	ldy	$00
	sta	$0489, y
	lda	$29, x
	sta	$29, y
	lda	$15, x
	sta	$15, y
	lda	$33, x
	clc
	adc	#$10
	sta	$33, y
	lda	$1F, x
	adc	#$00
	sta	$1F, y
LC4A4:	inc	$9F, x
	lda	$9F, x
	and	#$3F
	bne	LC4AF
	jsr	L988A
LC4AF:	jsr	LB17D
	jmp	LAE56

LC4B5:	brk
LC4B6:	.byte	$01
LC4B7:	brk
LC4B8:	.byte	$FF
	brk
	ora	($00, x)
LC4BC:	brk
	ldy	#$00
	lda	$EE
	bne	LC4CB
	lda	$10
	and	#$18
	lsr	a
	lsr	a
	lsr	a
	tay
LC4CB:	sty	$07
	lda	$0429
	sta	LC4BC
	clc
	adc	LC4B5, y
	sta	$0429
	jsr	LAED4
	lda	$79, x
	sta	$09
	beq	LC522
	tya
	clc
	adc	#$10
	tay
	ldx	$07
	lda	LC4BC
	adc	LC4B6, x
	sta	$01
	ldx	#$57
	jsr	LB01F
	dec	$09
	beq	LC522
	jsr	LABB2
	ldx	$07
	lda	LC4BC
	clc
	adc	LC4B7, x
	sta	$01
	ldx	#$57
	jsr	LB01F
	dec	$09
	beq	LC522
	ldx	$07
	lda	LC4BC
	clc
	adc	LC4B8, x
	sta	$01
	ldx	#$57
	jsr	LB01F
LC522:	ldx	$12
	rts

	lda	#$01
	ldy	$00
	cpy	#$8A
	beq	LC533
	cpy	#$8B
	bne	LC55B
	lda	#$08
LC533:	sta	$46
	lda	$32
	and	#$0F
	tay
	lda	$04EB
	sec
	sbc	#$8A
	cmp	#$02
	bcs	LC551
	cpy	#$0C
	bne	LC54F
	lda	#$00
	sta	$82
	jsr	L8487
LC54F:	ldy	#$04
LC551:	cpy	#$04
	bcs	LC557
	ldy	#$01
LC557:	sty	$04DF
	dex
LC55B:	rts

	inc	$FEFE, x
	inc	$B6B4, x
	lda	$B7, x
	clv
	.byte	$FA
	lda	$FAFA, y
	.byte	$FA
	.byte	$B2
	.byte	$B3
	ldx	LBFBE, y
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	lsr	a
	lsr	a
	.byte	$4B
	.byte	$4B
	lsr	$5E5F, x
	.byte	$5F
	tay
LC57D:	tay
	lda	#$A9
	.byte	$A9
LC581:	lda	#$A9
	lda	#$45
LC585:	eor	$A9
	lda	#$A8
LC589:	eor	$A9A9, x
	.byte	$74
LC58D:	ror	$75, x
	.byte	$77
	tya
LC591:	txs
	sta	$9C9B, y
LC595:	txs
	sta	$9C9B, x
LC599:	.byte	$9E
	.byte	$9B
	.byte	$9F
	cli
LC59D:	.byte	$5A
	eor	$5E5B, y
LC5A1:	.byte	$5F
	lsr	$8E5F, x
LC5A5:	.byte	$8F
	.byte	$8F
	stx	$A6A6
	.byte	$A7
	.byte	$A7
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	.byte	$74
	ror	$75, x
	.byte	$77
	bvs	LC628
	adc	($73), y
	adc	($73), y
	adc	($73), y
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	bcs	LC57D
	.byte	$FA
	.byte	$FA
	bcs	LC581
	.byte	$FA
	.byte	$FA
	bcs	LC585
	.byte	$FA
	.byte	$FA
	bcs	LC589
	.byte	$FA
	.byte	$FA
	bcs	LC58D
	.byte	$FA
	.byte	$FA
	bcs	LC591
	.byte	$FA
	.byte	$FA
	bcs	LC595
	.byte	$FA
	.byte	$FA
	bcs	LC599
	.byte	$FA
	.byte	$FA
	bcs	LC59D
	.byte	$FA
	.byte	$FA
	bcs	LC5A1
	.byte	$FA
	.byte	$FA
	bcs	LC5A5
	ldy	#$A2
	lda	($A3, x)
	.byte	$80
	.byte	$82
	sta	($83, x)
	.byte	$F4
	stx	$F5
	.byte	$87
	sty	$86
	sta	$87
	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
	lda	$ACFB
	lda	$ACAC
	ldy	$FBAC
	.byte	$3B
	.byte	$3B
	ldy	$FCFC
	.byte	$FC
	.byte	$FC
	.byte	$F4
	stx	$F5
	.byte	$87
	inc	$FEFE, x
	inc	$FEFE, x
	ora	$3CFE, x
	rol	$3F3D, x
LC628:	cli
	inc	$5A59, x
	.byte	$5B
	.byte	$5A
	inc	$FEFE, x
	eor	$5C5B, x
	inc	$5BFE, x
	.byte	$5A
	.byte	$1C
	inc	$FEFE, x
	inc	$FEFE, x
	inc	$1EFE, x
	inc	$201F, x
	.byte	$22
	and	($23, x)
	.byte	$57
	.byte	$57
	.byte	$FB
	.byte	$FB
	.byte	$57
	.byte	$57
	inc	$ABFE, x
	.byte	$AB
	.byte	$FB
	.byte	$FB
	nop
	nop
	.byte	$FB
	.byte	$FB
	.byte	$7C
	ror	$7F7D, x
	dex
	cpy	LCDCB
	dex
	cpy	LCDCB
	cpy	#$C2
	cmp	($C3, x)
	bit	$2D2E
	.byte	$2F
	stx	$8F8F
	stx	$8A88
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	sty	$888D
	txa
	sty	$888D
	txa
	.byte	$89
	.byte	$8B
	dey
	txa
	.byte	$89
	.byte	$8B
	ror	a
	jmp	(L6D6B)

	jmp	(L6D6C)

	adc	$6E6C
	adc	$946F
	sta	$94, x
	sta	$96, x
	.byte	$97
	stx	$97, y
	pha
	eor	#$48
	eor	#$FE
	inc	$FEFE, x
	.byte	$FB
	.byte	$32
	.byte	$32
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	sbc	$FDFD, x
	sbc	$FB34, x
LC6B2:	sbc	$FB34, x
	bmi	LC6B2
	.byte	$FB
	.byte	$FB
	.byte	$FB
	and	($FB), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	$56, x
	lsr	$56, x
	.byte	$64
	ror	$65
	.byte	$67
	pla
	ror	a
	adc	#$6B
	.byte	$FA
	jmp	(L6CFA)

	adc	$6DFA
	.byte	$FA
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	ldx	$AEAF
	.byte	$AF
	sei
	.byte	$7A
	adc	$A87B, y
	tay
	.byte	$AF
	ldx	$9594
	sty	$95, x
	stx	$97, y
	stx	$97, y
	.byte	$22
	bit	$23
	and	$92
	.byte	$93
	.byte	$93
	.byte	$92
	bvc	LC747
	bvc	LC749
	ldx	$AEAF
	.byte	$AF
	bvc	LC74F
	bvc	LC751
	stx	$8F8F
	stx	$5250
	eor	($53), y
	sbc	$FDFD, x
	sbc	$36FB, x
	rol	$4F, x
	.byte	$4F
	lsr	$4F4E
	lsr	$4F4F
	lsr	$9392
	.byte	$93
	.byte	$92
	stx	$8F8F
	stx	$4240
	eor	($43, x)
	rti

	.byte	$42
	eor	($43, x)
	tsx
	ldy	$BDBB, x
	tsx
	ldy	$9190, x
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	sbc	$FDFD, x
	sbc	$6361, x
	adc	($63, x)
	adc	$63
	adc	$63
	adc	$67
	.byte	$65
LC747:	.byte	$67
	rts

LC749:	.byte	$62
	adc	($63, x)
	.byte	$64
	.byte	$62
	.byte	$65
LC74F:	.byte	$63
	.byte	$64
LC751:	ror	$65
	.byte	$67
	pla
	.byte	$62
	adc	($63, x)
	.byte	$64
	adc	#$65
	.byte	$67
	lsr	$62
	adc	($63, x)
	.byte	$64
	.byte	$47
	adc	$67
	tsx
	ldy	$BDBB, x
	bvs	LC7DC
	adc	($73), y
	stx	$8F8F
	stx	$4544
	eor	$44
LC774:	stx	$92, y
	.byte	$93
	.byte	$0C
	ora	$15, x
	ora	$15, x
	eor	$45
	eor	$45
	adc	$6D6D
	adc	$1313
	.byte	$13
	.byte	$13
	.byte	$9F
	rti

	.byte	$9C
	.byte	$12
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$80
	.byte	$07
	sta	($80, x)
	sta	($81, x)
	sta	($81, x)
LC798:	lda	$050E
	asl	a
	asl	a
	sta	$0F
	lda	$050E
	cmp	#$07
	bcc	LC7AC
	lda	$0543
	jmp	LC7AF

LC7AC:	lda	$0542
LC7AF:	clc
	adc	$0F
	tax
	lda	LC774, x
	sta	($01), y
	rts

	ldy	$E7
LC7BB:	jsr	LC798
	jsr	LBE0C
	dec	$050D
	bpl	LC7BB
	rts

LC7C7:	.byte	$4F
	.byte	$4F
	.byte	$83
LC7CA:	bvc	LC81D
	.byte	$83
	ldy	$E7
	lda	$050E
	cmp	#$09
	bne	LC7E1
	lda	$04B0
	beq	LC7E1
	.byte	$EE
LC7DC:	asl	$EE05
	.byte	$0E
	.byte	$05
LC7E1:	lda	$050E
	sec
	sbc	#$09
	tax
	lda	LC7C7, x
	sta	($01), y
	jsr	LBE24
	lda	LC7CA, x
	sta	($01), y
	jsr	LBE38
	rts

	ldy	$E7
	lda	#$52
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	lda	$E7
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	iny
	lda	#$54
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	jsr	LBE38
	rts

	.byte	$A4
LC81D:	.byte	$E7
	lda	#$52
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	ldy	$E7
	tya
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	dey
	lda	#$54
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	jsr	LBE38
	rts

	ldy	$E7
	lda	$050E
	cmp	#$06
	bne	LC850
	lda	#$9E
	sta	($01), y
	jmp	LC853

LC850:	jsr	LC798
LC853:	jsr	LBE24
	dec	$050D
	bpl	LC850
	rts

	ldy	$E7
LC85E:	jsr	LC798
	tya
	clc
	adc	#$11
	tay
	dec	$050D
	bpl	LC85E
	rts

LC86C:	.byte	$67
	ror	a
	.byte	$9C
	cmp	($70), y
	sty	$95, x
	lda	$050E
	tax
	cmp	#$05
	bne	LC881
	lda	$0543
	beq	LC881
	inx
LC881:	ldy	$E7
	lda	LC86C, x
	sta	($01), y
	rts

LC889:	.byte	$73
	.byte	$0D
LC88B:	.byte	$74
	.byte	$0E
LC88D:	adc	$0F, x
	ldy	$E7
	lda	$050E
	sec
	sbc	#$0A
	tax
	lda	LC889, x
	sta	($01), y
	dec	$050D
	beq	LC8AF
LC8A2:	jsr	LBE0C
	lda	LC88B, x
	sta	($01), y
	dec	$050D
	bne	LC8A2
LC8AF:	jsr	LBE0C
	lda	LC88D, x
	sta	($01), y
	rts

LC8B8:	dex
	.byte	$CB
	cpy	LC8C7
	cmp	#$40
	.byte	$04
	ora	$A2
	brk
LC8C3:	stx	$0B
	ldx	$E8
LC8C7:	jsr	LBE01
	ldx	#$05
	ldy	$E7
	ldx	$0B
	ldy	$E7
	lda	$050D
	sta	$07
	jsr	LC908
	inx
	lda	$07
	beq	LC8E7
LC8DF:	jsr	LBE0C
	jsr	LC908
	bne	LC8DF
LC8E7:	jsr	LBE0C
	inx
	jsr	LC908
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LC8FE
	ldx	#$03
	sta	$E7
	jmp	LC8C3

LC8FE:	rts

LC8FF:	.byte	$C7
	iny
	.byte	$C9
LC902:	.byte	$CF
	.byte	$CD
	.byte	$CD
LC905:	dec	$D0CE
LC908:	stx	$08
	txa
	bne	LC920
	ldx	#$02
	lda	($01), y
LC911:	cmp	LC8FF, x
	beq	LC91B
	dex
	bpl	LC911
	bmi	LC939
LC91B:	lda	LC902, x
	bne	LC94F
LC920:	ldx	$08
	cpx	#$02
	bne	LC939
	ldx	#$02
	lda	($01), y
LC92A:	cmp	LC8FF, x
	beq	LC934
	dex
	bpl	LC92A
	bmi	LC939
LC934:	lda	LC905, x
	bne	LC94F
LC939:	ldx	#$08
LC93B:	lda	($01), y
	cmp	LC8B8, x
	beq	LC94A
	dex
	bpl	LC93B
	ldx	$08
	jmp	LC951

LC94A:	ldx	$08
	lda	LC8B8, x
LC94F:	sta	($01), y
LC951:	ldx	$08
	dec	$07
	rts

	ldy	$E7
	lda	#$C0
	sta	($01), y
LC95C:	jsr	LBE24
	lda	($01), y
	cmp	#$40
	bne	LC96B
	lda	#$82
	sta	($01), y
	bne	LC95C
LC96B:	rts

	ldy	$E7
	lda	#$00
	sta	$08
LC972:	lda	($01), y
	cmp	#$40
	beq	LC979
	rts

LC979:	lda	#$84
	sta	($01), y
	ldx	$08
	beq	LC993
LC981:	iny
	lda	#$85
	sta	($01), y
	dex
	bne	LC981
	ldx	$08
LC98B:	iny
	lda	#$86
	sta	($01), y
	dex
	bne	LC98B
LC993:	iny
	lda	#$87
	sta	($01), y
	inc	$08
	lda	$E7
	clc
	adc	#$10
	sta	$E7
	sec
	sbc	$08
	tay
	jmp	LC972

LC9A8:	ldy	$E7
	lda	#$03
	sta	($01), y
	rts

	ldy	$E7
	lda	$050E
	cmp	#$0D
	beq	LC9BF
	lda	#$C1
	sta	($01), y
LC9BC:	jsr	LBE24
LC9BF:	lda	($01), y
	cmp	#$40
	bne	LC9D4
	lda	#$C2
	sta	($01), y
	lda	$EC
	beq	LC9D1
	cpy	#$E0
	bcs	LC9D4
LC9D1:	jmp	LC9BC

LC9D4:	rts

	ldy	$E7
	lda	#$C3
	sta	($01), y
LC9DB:	tya
	sec
	sbc	#$10
	tay
	cmp	#$F0
	bcs	LC9F1
	lda	($01), y
	cmp	#$40
	bne	LC9F1
	lda	#$C2
	sta	($01), y
	jmp	LC9DB

LC9F1:	rts

LC9F2:	.byte	$43
	.byte	$44
	eor	$46
	.byte	$47
	pha
	eor	#$4A
	.byte	$4B
	lsr	$414C
	.byte	$5A
	.byte	$42
LCA00:	ldy	$E7
	ldx	$050E
	lda	LC9F2, x
	sta	($01), y
	rts

	ldy	$E7
	lda	#$16
	sta	($01), y
LCA11:	jsr	LBE24
	lda	($01), y
	cmp	#$40
	beq	LCA1B
	rts

LCA1B:	lda	#$17
	sta	($01), y
	bne	LCA11
LCA21:	lda	$09
	asl	a
	asl	a
	sec
	adc	$09
	sta	$09
	asl	$0A
	lda	#$20
	bit	$0A
	bcs	LCA36
	bne	LCA38
	beq	LCA3A
LCA36:	bne	LCA3A
LCA38:	inc	$0A
LCA3A:	lda	$0A
	eor	$09
	rts

LCA3F:	rti

	dey
	rti

	rti

	rti

	rti

	.byte	$89
	rti

	lda	$E8
	sta	$0D
	lda	#$80
	sta	$0A
	lda	#$31
	sta	$09
LCA53:	jsr	LCA21
	and	#$07
	tax
	lda	LCA3F, x
	sta	($01), y
	jsr	LBE24
	cpy	#$30
	bcc	LCA53
	tya
	and	#$0F
	tay
	jsr	LBE0C
	lda	$0D
	sta	$E8
	cmp	#$0A
	bne	LCA53
	lda	#$00
	sta	$E8
	sta	$E6
	sta	$E5
	rts

	.byte	$80
	cpx	a:$90
	brk
	.byte	$1A
	.byte	$14
	ora	($12), y
	bpl	LCAA4
	bpl	LCAD4
	.byte	$0F
	.byte	$12
	.byte	$0F
	txa
	bpl	LCAB7
	.byte	$0F
	.byte	$E2
	bpl	LCAA8
	ora	($4A), y
	.byte	$07
	.byte	$0F
	.byte	$D3
	ora	($D5), y
	asl	$10D1
	cmp	($06), y
	cmp	($0D), y
	cmp	($77), y
LCAA4:	bpl	LCABF
	ora	($48), y
LCAA8:	and	#$1C
	.byte	$D3
	.byte	$12
	cmp	($0B), y
	cmp	($0F), y
	cmp	($13), y
	cmp	($0A), y
	cmp	($B8), y
	.byte	$26
LCAB7:	asl	a
	.byte	$22
	.byte	$0B
	.byte	$2B
	.byte	$0C
	.byte	$22
	.byte	$0D
	.byte	$22
LCABF:	.byte	$17
	rol	$14, x
	.byte	$07
	php
	.byte	$34
	.byte	$17
	rol	$10, x
	cmp	($08), y
	.byte	$34
	stx	$3910
	and	#$37
	ora	$190A, y
	.byte	$1D
LCAD4:	.byte	$D2
	.byte	$F0
LCAD6:	.byte	$0C
	inc	$01, x
	.byte	$93
	ora	($47), y
	ora	$370F, y
	asl	$1D39, x
	.byte	$3B
LCAE3:	sbc	($8C), y
	inc	$00, x
	.byte	$89
	bpl	LCAFA
	and	#$05
	and	#$1F
	and	#$11
	.byte	$33
LCAF1:	bpl	LCB28
	.byte	$1C
	ora	$7DF2, y
	ora	$0CF0, y
LCAFA:	inc	$01, x
	sbc	($8C), y
	.byte	$F6
LCAFF:	brk
	.byte	$A3
	bpl	LCB14
	ora	($07), y
	.byte	$0F
	eor	$030B, y
	.byte	$10
LCB0A:	.byte	$FF
	ora	#$F6
	and	($09), y
LCB0F:	ror	$13
	.byte	$03
	ora	#$7D
LCB14:	and	#$F0
	eor	$F1, x
	.byte	$1C
	sbc	($4D), y
	.byte	$83
	and	#$F2
	.byte	$72
	and	#$1D
	.byte	$29
LCB22:	.byte	$32
	.byte	$A3
	.byte	$32
	ora	$F1
	.byte	$C0
LCB28:	.byte	$12
	tax
	ldy	$030A
	jsr	LF4F0
	sbc	($98), y
	sbc	($A0), y
	.byte	$FF
	sta	($EA), y
	jsr	L971A
	.byte	$80
	.byte	$23
	asl	a
	.byte	$03
	.byte	$13
	beq	LCB8E
LCB41:	beq	LCB0F
	sbc	($02), y
	.byte	$F2
	beq	LCB90
	beq	LCAD6
	.byte	$F2
	sty	$86
	ora	$86
	beq	LCB99
	sbc	($0B), y
	sbc	($CA), y
	.byte	$F5
LCB56:	.byte	$04
	brk
	.byte	$FF
	clc
	.byte	$E3
	ora	($01, x)
	.byte	$54
	.byte	$32
	ora	#$32
	ora	$30, x
	asl	a
	bmi	LCB8D
	and	($24), y
	and	($0A), y
	and	($15), y
	and	($09), y
	and	($16), y
	.byte	$33
	.byte	$1B
	bit	$F0
	.byte	$44
	beq	LCBDC
	beq	LCAFF
	beq	LCB22
	sbc	($86), y
	.byte	$F1
LCB7E:	.byte	$A3
	sbc	($C8), y
	.byte	$FF
	.byte	$89
	nop
	bpl	LCB9F
	stx	$0F81
	sta	($F0, x)
	bcs	LCB7E
LCB8D:	.byte	$6F
LCB8E:	.byte	$BD
	.byte	$14
LCB90:	.byte	$04
	bpl	LCBD3
	sta	($F0, x)
	rol	LCAF1
	.byte	$FF
LCB99:	.byte	$80
	nop
	bcc	LCBB5
	.byte	$2D
	.byte	$10
LCB9F:	.byte	$1B
	ora	($56), y
	asl	$0E0D
	.byte	$32
	.byte	$0B
	.byte	$04
	ora	($F0, x)
	adc	$8DF0
	inc	$00, x
	.byte	$7C
	bpl	LCB41
	ora	$D111, y
LCBB5:	.byte	$0C
	ora	$8DF0, y
	inc	$01, x
	.byte	$5A
	bpl	LCBE4
	ora	($33), y
	and	#$10
	and	#$22
	ora	$070F, y
	beq	LCB56
	inc	$00, x
	.byte	$74
	bpl	LCC13
	and	#$09
	and	#$34
	.byte	$26
LCBD3:	php
	and	($0C, x)
	and	($0D, x)
	.byte	$2B
	beq	LCBE7
	.byte	$AC
LCBDC:	ora	$1917, y
	ora	$1319, y
	.byte	$07
	.byte	$04
LCBE4:	.byte	$97
	bmi	LCBEE
LCBE7:	beq	LCBF6
	beq	LCB8D
	inc	$01, x
	pla
LCBEE:	bpl	LCC31
	.byte	$07
	.byte	$3F
	.byte	$0B
	.byte	$04
	.byte	$22
	.byte	$F0
LCBF6:	ldx	#$F6
	.byte	$02
	.byte	$F1
LCBFA:	.byte	$C2
	inc	$03, x
	.byte	$F2
	ror	$1507, x
	.byte	$07
	asl	a
	.byte	$07
	beq	LCC12
	sbc	$04, x
LCC08:	.byte	$22
	inc	$00, x
	.byte	$83
	ora	($10), y
	bpl	LCC5D
	.byte	$19
	.byte	$15
LCC12:	.byte	$07
LCC13:	ora	#$07
	.byte	$12
	.byte	$07
	stx	$11
	.byte	$12
	bpl	LCC66
	.byte	$37
	ora	$1839, y
	.byte	$3B
	.byte	$17
	and	$0C9D, x
	sbc	$04, x
	bmi	LCC73
	.byte	$87
	.byte	$0B
	.byte	$87
	.byte	$0C
	.byte	$87
	ora	$0E03
LCC31:	.byte	$87
	.byte	$0F
	.byte	$87
	.byte	$7D
LCC35:	.byte	$03
	sbc	($48), y
	.byte	$FF
	.byte	$89
	nop
	jsr	L8B19
	plp
	ora	$0E26
	.byte	$2D
LCC43:	.byte	$0F
	.byte	$22
	.byte	$1A
	rol	$19, x
	and	($0A, x)
	rol	$19, x
	sec
	clc
	.byte	$3A
	beq	LCC08
	.byte	$F0
LCC52:	beq	LCCBB-10
	.byte	$02
	ora	$1D02, x
	.byte	$02
	ora	$3C02, x
	.byte	$02
LCC5D:	.byte	$1C
	.byte	$02
	ora	($22), y
	beq	LCBFA
	beq	LCC35
	.byte	$F1
LCC66:	.byte	$4F
	sbc	($AE), y
	sbc	$04, x
	ora	$90, x
	.byte	$13
	.byte	$04
	ora	$24, x
	and	#$33
LCC73:	and	$04
	and	$05
	and	$37
	.byte	$27
	beq	LCCEB
	beq	LCC52
	sbc	($4A), y
	.byte	$FF
	ora	#$F2
	and	($09), y
	.byte	$02
	txa
	.byte	$03
	txa
	.byte	$04
	txa
	ora	$8A
	asl	$8A
	.byte	$07
	txa
	php
	txa
	ora	#$8A
	.byte	$62
	.byte	$12
	.byte	$77
	.byte	$80
	.byte	$07
	and	#$F5
	.byte	$04
LCC9D:	.byte	$19
LCC9E:	ldy	$0C80
	and	#$54
	.byte	$80
	.byte	$04
	and	#$F0
	eor	($F1), y
	adc	$803B
	.byte	$0B
	and	#$52
	.byte	$82
	.byte	$03
	.byte	$82
	.byte	$04
	.byte	$82
	ora	$82
	.byte	$23
	and	#$F1
	.byte	$6E
	.byte	$F2
LCCBB:	txs
	.byte	$83
	.byte	$0B
LCCBE:	.byte	$83
	.byte	$0C
	.byte	$83
	ora	$0E83
	.byte	$83
	bit	$040A
	bvc	LCCBB
	ldy	#$FF
LCCCC:	clc
	.byte	$E3
	ora	($01, x)
	.byte	$54
	.byte	$32
	ora	#$32
	ora	$30, x
	asl	a
	bmi	LCD00
	and	($24), y
	and	($0A), y
	and	($15), y
	and	($09), y
	and	($16), y
	.byte	$33
	.byte	$1B
	bit	$F0
	.byte	$44
	beq	LCD4F
	.byte	$F0
LCCEB:	stx	$F0
	.byte	$A7
	sbc	($86), y
	sbc	($A3), y
	sbc	($C8), y
	.byte	$FF
	.byte	$89
	nop
	.byte	$22
	.byte	$1A
LCCF9:	.byte	$43
	asl	a
	.byte	$04
	.byte	$33
	beq	LCD4D
	.byte	$F1
LCD00:	.byte	$4F
	sbc	($BC), y
	.byte	$F2
LCD04:	.byte	$64
	lsr	a:$64
	.byte	$07
	brk
	php
	brk
	beq	LCD21
	beq	LCC9D
	bvs	LCD58
	bpl	LCD56
	beq	LCC9E
	beq	LCD04
	sbc	($2B), y
	sbc	($8A), y
	sbc	$05, x
	brk
	.byte	$FF
	.byte	$89
LCD21:	nop
	brk
	ora	($09, x)
	lda	#$F5
	ora	$11
	beq	LCD5A
	beq	LCCF9
	sbc	($8F), y
	sbc	($EA), y
	.byte	$FF
	.byte	$80
	cpx	a:$90
	brk
	.byte	$89
	ora	($89, x)
	.byte	$02
	.byte	$89
	.byte	$03
	.byte	$89
	and	$29
	.byte	$1C
	and	#$18
	.byte	$2B
	asl	$C4, x
	.byte	$2B
	and	($1A, x)
	.byte	$C2
	sta	$11, x
	php
LCD4D:	bpl	LCDB5
LCD4F:	ldx	$F5
	ora	$00
	.byte	$12
	.byte	$32
	.byte	$09
LCD56:	.byte	$32
	.byte	$0D
LCD58:	rol	$0E
LCD5A:	and	($0F, x)
	and	($14, x)
	and	($06), y
	ora	$07
	.byte	$32
	.byte	$14
	and	($07), y
	.byte	$32
	.byte	$14
	and	($07), y
	.byte	$32
	.byte	$14
	and	($07), y
	.byte	$32
	.byte	$14
	and	($06), y
	ora	$07
	.byte	$32
	.byte	$53
	and	#$31
	ora	$290F, y
	.byte	$23
	and	($05, x)
	and	($71, x)
	.byte	$C2
	asl	$0229, x
	.byte	$0B
	ora	$20
LCD87:	asl	$22, x
	.byte	$09
LCD8A:	.byte	$22
	ora	($C2), y
	ora	$C3
	ora	#$C2
	asl	$DDC3
	ora	($5D), y
	ora	$29C3, y
	and	$1319, y
	.byte	$19
LCD9D:	asl	$F119
	jmp	$01F6

	sbc	($CC), y
	inc	$00, x
	bcc	LCDB9
	asl	$1210
	ora	($52), y
	ora	$1906, y
	asl	a
	ora	$D11E, y
LCDB5:	beq	LCE23
	inc	$01, x
LCDB9:	beq	LCD87
	inc	$00, x
	stx	$11
	and	($29), y
	php
	and	#$43
	.byte	$D2
	asl	a
	.byte	$D1
LCDC7:	beq	LCDD5
	inc	$02, x
LCDCB:	.byte	$F0
LCDCC:	jmp	(L00F6)

	beq	LCD9D
	inc	$02, x
	sbc	($4C), y
LCDD5:	inc	$00, x
	sty	$10
	.byte	$52
	ora	$190E, y
	iny
	.byte	$0F
	.byte	$47
	.byte	$0B
	ora	$30
	.byte	$F4
	rol	$0E
	ora	#$0E
	.byte	$FF
	sta	($EA), y
	bpl	LCE06
	txa
	.byte	$2D
	.byte	$0D
LCDF0:	.byte	$13
	ora	$13
	clc
	.byte	$54
	.byte	$0B
	and	($F1, x)
	.byte	$0F
	.byte	$F2
LCDFA:	bcc	LCE22
	ora	($52, x)
	beq	LCD8A
	.byte	$FF
	ora	#$E1
	adc	($09, x)
	pla
LCE06:	.byte	$14
	ora	$19
	.byte	$5C
	and	($13), y
	and	($F0), y
	and	$F1, x
	.byte	$1C
	.byte	$6B
	and	#$9D
	and	#$A8
	plp
	.byte	$3A
	.byte	$34
	beq	LCE90
	sbc	($1C), y
LCE1D:	sbc	($75), y
	.byte	$82
	.byte	$2C
LCE21:	.byte	$06
LCE22:	.byte	$2C
LCE23:	ora	#$2C
	ora	$242C
	bit	$2C0B
	.byte	$5C
	ora	#$05
	bvc	LCE21
	.byte	$33
	sbc	($92), y
	.byte	$BB
	and	#$54
	and	#$F0
	.byte	$6F
	.byte	$92
	and	#$5D
	and	#$F0
	ora	$83B6
	.byte	$07
	.byte	$83
	php
	.byte	$83
	ora	#$83
	plp
	asl	a
	ora	$70
	.byte	$1B
	and	#$F0
	asl	LCDF0
	sbc	($4E), y
	sbc	($A0), y
	.byte	$FF
	clc
	.byte	$E3
	ora	($01, x)
	.byte	$54
	and	($0A), y
	and	($15), y
	and	($09), y
	and	($26), y
	.byte	$33
	ora	$31, x
	ora	#$31
	.byte	$14
	bmi	LCE76
	bmi	LCE95
	plp
	beq	LCEB4
	beq	LCED7
	beq	LCDFA
	beq	LCE1D
LCE76:	sbc	($66), y
	sbc	($84), y
	sbc	($A0), y
	.byte	$FF
	.byte	$89
	nop
	.byte	$62
	.byte	$1A
	.byte	$43
	asl	a
	ora	$33
	bit	$1C35
	and	$1C, x
	and	$1C, x
	and	$0D, x
	and	#$F0
LCE90:	lsr	$8FF1
	.byte	$C2
	.byte	$3F
LCE95:	.byte	$12
	.byte	$3F
	.byte	$12
	.byte	$3F
	.byte	$0B
	and	#$12
	.byte	$3F
	bpl	LCEDE
	.byte	$03
	and	#$10
	.byte	$3F
	bpl	LCEE4
	php
	and	#$F0
	.byte	$1C
	.byte	$62
LCEAA:	and	#$2F
	.byte	$34
	.byte	$1F
	.byte	$34
	.byte	$1F
	.byte	$34
	asl	$1D36, x
LCEB4:	sec
	.byte	$1C
	.byte	$3A
	.byte	$1B
	.byte	$3C
	.byte	$1A
	rol	$0FF0, x
	beq	LCF0D
	beq	LCE90
	sbc	($0D), y
	.byte	$F2
	sbc	($CC), y
	.byte	$F2
	.byte	$73
	ora	$0706, y
LCECB:	ora	($19), y
LCECD:	.byte	$F1
LCECE:	and	$A0F2
	.byte	$07
	asl	$07
	.byte	$22
	.byte	$42
	.byte	$F0
LCED7:	bpl	LCECB
	beq	LCEAA
	sbc	($4A), y
	.byte	$F5
LCEDE:	ora	$60
	.byte	$FF
	tax
	nop
	.byte	$13
LCEE4:	rol	a
	stx	$F030
	cmp	$52F1
	sbc	($CD), y
	cpx	$32
	.byte	$1A
	.byte	$33
	.byte	$1C
	.byte	$83
	ora	$1083
	brk
	ora	($00, x)
	.byte	$02
	brk
	.byte	$03
	.byte	$33
	bpl	LCEFF
LCEFF:	ora	($00, x)
	.byte	$02
	brk
	beq	LCF1B
	beq	LCF74
	sbc	($CA), y
	.byte	$FF
	ora	#$E0
	.byte	$03
LCF0D:	ora	($32), y
	bit	$2C0D
	.byte	$22
	bit	$2C0D
	.byte	$22
	bit	$2C0D
	.byte	$15
LCF1B:	and	$27, x
	asl	a
	ora	$36
	beq	LCF63
	sbc	($80), y
	.byte	$FF
	ora	($03, x)
	ora	$03FA, y
	clc
	.byte	$A7
	ora	$01
	adc	#$03
	sta	$1803, y
	.byte	$47
	.byte	$03
	.byte	$03
	sed
	ora	($07, x)
	and	($24, x)
	ora	($27, x)
	ora	($49, x)
	.byte	$03
	ora	$014A, y
	ora	($05, x)
	ora	($49, x)
	ora	($6D, x)
	.byte	$03
	ora	($D3, x)
	.byte	$07
	ora	($40, x)
	asl	$16
	ora	($C8, x)
	ora	($01, x)
	ora	($05, x)
	.byte	$1C
	tay
	.byte	$42
	lda	#$03
	eor	($D3, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$01
LCF63:	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$03
	asl	$6C
	.byte	$03
	rti

	.byte	$5A
	ora	($01, x)
	ora	($01, x)
	.byte	$01
LCF74:	ora	($01, x)
	ora	($01, x)
	.byte	$03
	ora	$058D, y
	.byte	$0F
	bvc	LCF97
	.byte	$FA
	.byte	$03
	ora	($99, x)
	ora	$18
	asl	a
	clc
	.byte	$37
	.byte	$07
	.byte	$0F
	bpl	LCFA4
	ora	$39, x
	.byte	$97
	.byte	$07
	clc
	cli
	clc
	tay
	clc
	.byte	$E7
	.byte	$09
LCF97:	clc
	plp
	clc
	.byte	$57
	.byte	$1A
	stx	$18
	.byte	$97
	.byte	$03
	and	($B5, x)
	.byte	$03
	.byte	$21
LCFA4:	ora	$01
	.byte	$07
	.byte	$03
	clv
	.byte	$03
	sed
	.byte	$03
	.byte	$FB
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$03
	ora	($8A, x)
	ora	$01
	.byte	$C2
	ora	($67, x)
	.byte	$07
	asl	$01A0
	sei
	ora	($BD, x)
	.byte	$03
	asl	$E2
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	ora	($B9, x)
	ora	($05, x)
	.byte	$1C
	ldy	$42, x
	ldy	$03, x
	eor	($B3, x)
	ora	($03, x)
	rti

	.byte	$17
	ora	($03, x)
	.byte	$03
	.byte	$C7
	.byte	$03
	.byte	$47
	jsr	L0F05
	jsr	LA046
	ora	($01, x)
	.byte	$03
	lsr	$80
	ora	($03, x)
	.byte	$47
	brk
	ora	$19
	.byte	$7A
	.byte	$1A
	dec	$01, x
	ora	($01, x)
	ora	($01, x)
	.byte	$03
	.byte	$03
	.byte	$4B
	ora	$01
	cmp	$01, x
	tax
	ora	$03
	.byte	$D2
	ora	($D5, x)
	.byte	$03
	.byte	$02
	dey
	.byte	$07
	ora	($83, x)
	ora	($A9, x)
LD016:	ora	($1D, x)
	.byte	$07
	ora	($B1, x)
	ora	($89, x)
	ora	($3D, x)
	.byte	$03
	ora	($E3, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	ora	($B9, x)
	ora	($09, x)
	ora	($15, x)
	ora	($08, x)
	ora	($F8, x)
	ora	($CB, x)
	ora	($07, x)
	and	($14, x)
	.byte	$03
	and	$7B03, y
	.byte	$03
	clc
	.byte	$67
	.byte	$07
	clc
	asl	a
	.byte	$42
	.byte	$3B
	clc
	ror	a
	.byte	$03
	eor	($97, x)
	ora	($01, x)
	ora	$5F
	cmp	$02
	bne	LD058
	.byte	$05
LD058:	.byte	$3B
	.byte	$87
	and	$01A9
	ora	($42, x)
	tay
	eor	($D7, x)
	ora	($03, x)
	rol	$03F6
	eor	$01D7, x
	rts

	ldy	#$02
	asl	$28
	.byte	$80
	.byte	$03
	.byte	$82
	bpl	LD074
LD074:	bvc	LD016
	.byte	$02
	rol	$25, x
	.byte	$80
	.byte	$34
	.byte	$80
	.byte	$02
	.byte	$80
	brk
	brk
	rts

	.byte	$80
	.byte	$2B
	sta	($0A, x)
	brk
	.byte	$04
	.byte	$82
	bpl	LD08A
LD08A:	rts

	ora	($07, x)
	ora	($C8, x)
	.byte	$03
	cld
	.byte	$02
	inx
	ora	($01, x)
	ora	($07, x)
	.byte	$27
	.byte	$72
	.byte	$27
	.byte	$82
	.byte	$27
	ror	$0301, x
	.byte	$27
	cmp	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$3B
	.byte	$74
	.byte	$27
	.byte	$57
	.byte	$27
	.byte	$97
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	$27
	adc	$27
	sta	$05, x
	.byte	$42
	rol	a
	eor	($87, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	pla
	bit	$01
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($08, x)
	dey
	dey
	dey
	clv
	.byte	$80
	brk
	rti

	.byte	$9C
