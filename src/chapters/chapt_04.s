.include	"mem.i"

.segment	"CHAPT_04DATA"

L02E0		:= $02E0
L0A35		:= $0A35
L1E10		:= $1E10
L261F		:= $261F
L2E6F		:= $2E6F
L4533		:= $4533
L60E0		:= $60E0
L62E0		:= $62E0
L6CFA		:= $6CFA
L8132		:= $8132
L88F0		:= $88F0
L92B7		:= $92B7
L9839		:= $9839
L9855		:= $9855
L9887		:= $9887
L988A		:= $988A
L995F		:= $995F
L9967		:= $9967
L9D61		:= $9D61
LA116		:= $A116
LA3AE		:= $A3AE
LA4B6		:= $A4B6
LA551		:= $A551
LA662		:= $A662
LA7CC		:= $A7CC
LA90B		:= $A90B
LA929		:= $A929
LA93A		:= $A93A
LA956		:= $A956
LABB2		:= $ABB2
LAC1F		:= $AC1F
LACCC		:= $ACCC
LAE56		:= $AE56
LAED4		:= $AED4
LAEE0		:= $AEE0
LAF80		:= $AF80
LB168		:= $B168
LB178		:= $B178
LB17D		:= $B17D
LB1D6		:= $B1D6
LB207		:= $B207
LB72F		:= $B72F
LBE01		:= $BE01
LBE0C		:= $BE0C
LBE24		:= $BE24
LBE38		:= $BE38
	.byte	$23
	bmi	LBF15
	.byte	$0F
	.byte	$23
	bmi	LBF1D
	.byte	$0F
	.byte	$23
	.byte	$2B
	.byte	$1B
	.byte	$0F
	.byte	$23
	bmi	LBF41
	.byte	$0F
	.byte	$0F
	bmi	LBF25
	ora	($0F, x)
LBF15:	bmi	LBF2D
	.byte	$02
	.byte	$0F
	.byte	$2B
	.byte	$1B
	.byte	$0B
	.byte	$0F
LBF1D:	and	#$1A
	asl	a
	.byte	$0F
	.byte	$32
	.byte	$12
	ora	($0F, x)
LBF25:	bmi	LBF3D
	.byte	$02
	.byte	$0F
	.byte	$2B
	.byte	$1B
	.byte	$0B
	.byte	$0F
LBF2D:	.byte	$27
	.byte	$17
	.byte	$07
	.byte	$07
	bmi	LBF5A
	.byte	$0F
	.byte	$07
	bmi	LBF4D
	.byte	$0F
	.byte	$07
	.byte	$27
	.byte	$17
	.byte	$0F
	.byte	$07
LBF3D:	and	($21, x)
	.byte	$0F
	.byte	$03
LBF41:	bmi	LBF55
	.byte	$0F
	.byte	$03
	bmi	LBF5D
	.byte	$0F
	.byte	$03
	.byte	$3C
	.byte	$1C
	.byte	$0F
	.byte	$03
LBF4D:	plp
	clc
	.byte	$0F
	.byte	$0C
	bmi	LBF53
LBF53:	.byte	$0F
	.byte	$0C
LBF55:	bmi	LBF6D
	.byte	$0F
	.byte	$0C
	.byte	$30
LBF5A:	bpl	LBF6B
	.byte	$0C
LBF5D:	bmi	LBF8F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
	.byte	$0F
	ora	($0F, x)
	.byte	$0F
LBF6B:	.byte	$0F
	.byte	$01
LBF6D:	.byte	$0F
	.byte	$0F
	.byte	$0F
	.byte	$FF
	bmi	LBF89
	.byte	$0F
	.byte	$FF
	sec
	bpl	LBF87
	.byte	$FF
	bmi	LBFA0
	.byte	$0F
	.byte	$FF
	bmi	LBF95
	.byte	$02
	.byte	$FF
	sec
	bpl	LBF86
	.byte	$FF
	.byte	$30
LBF86:	.byte	$25
LBF87:	.byte	$02
	.byte	$FF
LBF89:	bmi	LBFA1
	.byte	$0F
	.byte	$FF
	bmi	LBF9F
LBF8F:	.byte	$0F
	.byte	$FF
	.byte	$27
	asl	$0F, x
	brk
LBF95:	asl	a
	.byte	$14
	.byte	$02
	adc	$ECEC
	cpx	$ECEC
	.byte	$EC
LBF9F:	.byte	$EC
LBFA0:	.byte	$EC
LBFA1:	cpx	$5EFE
	inc	$2121
	and	($21, x)
	and	($21, x)
	eor	#$5C
	dec	$9E4B
	cmp	$EA, x
	asl	$4545
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
	.byte	$CB
LBFBE:	.byte	$CB
	.byte	$CB
	.byte	$CB
	cpy	LCDCC
	cmp	LCDCD
	cmp	LCDCD
	cmp	LCECD
	dec	LCECE
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$04
	bvs	LC012
	iny
	dec	$C6
	.byte	$C7
	.byte	$C7
	brk
	sta	$D2, y
	brk
	sta	$10, y
	brk
	.byte	$9F
	.byte	$9F
	.byte	$9F
	brk
	lda	($A1, x)
	lda	($00, x)
	.byte	$D3
	.byte	$9B
	.byte	$12
	brk
	ora	#$09
	php
	brk
	clc
	clc
	clc
	brk
	brk
	brk
	brk
	brk
	dec	$99
	.byte	$9C
	brk
	lda	($A1, x)
	lda	($00, x)
	.byte	$9B
	.byte	$9B
	.byte	$9B
	brk
	.byte	$9F
	.byte	$9F
	.byte	$9F
	brk
	.byte	$D3
	.byte	$D3
	.byte	$D3
LC00F:	brk
	.byte	$12
LC011:	.byte	$12
LC012:	.byte	$12
LC013:	brk
	brk
LC015:	brk
	brk
	brk
	brk
	brk
	brk
	cpy	#$C0
	cpy	#$27
	.byte	$3B
	.byte	$4F
	.byte	$C0
LC022:	cpy	#$C0
LC024:	and	($45), y
LC026:	.byte	$59
	.byte	$CF
LC028:	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	.byte	$CF
	eor	$74
	sta	$95, x
	sta	$95, x
	sta	$95, x
	sta	$95, x
	.byte	$CF
	.byte	$CF
	bne	LC00F
	bne	LC011
	bne	LC013
	bne	LC015
	.byte	$C3
	iny
	.byte	$0F
	rol	$41, x
	eor	($41, x)
	eor	($41, x)
	eor	($D0, x)
	bne	LC022
	bne	LC024
	bne	LC026
	bne	LC028
	bne	LC0AA
	.byte	$54
	adc	$8C
	lda	$B0
	cmp	($D4, x)
	sbc	($EC, x)
	ora	($43, x)
	.byte	$80
	cpy	#$0C
	.byte	$5F
	sta	($CA), y
	.byte	$12
	.byte	$67
	tya
	.byte	$D2
	.byte	$67
	.byte	$6B
	ora	$20
	ora	$7274, x
	iny
	.byte	$72
	iny
	.byte	$72
	iny
	.byte	$72
	iny
	.byte	$72
	iny
	sed
	iny
	sed
	iny
	sed
	iny
	sed
	iny
	.byte	$82
	cmp	#$7D
	ldx	LC950, y
	.byte	$C7
	ldx	$1D20, y
	.byte	$74
	.byte	$2B
	cmp	#$2B
	cmp	#$2B
	cmp	#$2B
	cmp	#$2B
	cmp	#$2B
	cmp	#$43
	dex
	.byte	$43
	dex
	.byte	$43
	dex
	stx	$C8
	stx	$C8
	stx	$C8
LC0AA:	.byte	$6B
	dex
	.byte	$6B
	dex
	.byte	$17
	dex
	and	$ADCA
	asl	$2005
	ora	$2D74, x
	dex
	and	$91CA, y
	dex
	.byte	$B2
	iny
	cmp	$C8, x
	.byte	$B2
	iny
	lda	$BDCA, x
	dex
	lda	LCACA, x
	dex
	dex
	dex
	lda	$BDCA, x
	dex
	lda	$BDCA, x
	dex
	lda	$4CCA, x
	lda	$8ACA, x
	pha
	and	#$F0
	sta	$33
	txa
	asl	a
	asl	a
	asl	a
	asl	a
	sta	$29
	lda	#$0A
	sta	$15
LC0EC:	ldx	#$00
	stx	$12
	stx	$1F
	lda	#$3D
	sta	$90
	lda	#$01
	sta	$51
	tya
	pha
	jsr	LA116
	pla
	sta	$79
	lda	#$FF
LC104:	.byte	$9D
	.byte	$41
LC106:	.byte	$04
	pla
	tax
	rts

	jmp	LC13A

	jmp	LC20F

	beq	LC104
	beq	LC106
	.byte	$F4
	inc	$F8, x
	.byte	$FA
	bne	LC0EC
	.byte	$D4
	dec	$01, x
	.byte	$03
	ora	$07
	ora	#$0B
	ora	$010F
	ora	($05), y
	ora	$13, x
	.byte	$0B
	.byte	$17
	.byte	$0F
	ora	$2D1B, y
	.byte	$2F
	.byte	$3A
	.byte	$3A
	cpx	#$E2
	cpx	$E6
	inx
	nop
	.byte	$EC
	.byte	$EE
LC13A:	lda	$B1, x
	bne	LC141
	jmp	LC1F8

LC141:	ldy	#$03
	lda	$47, x
	beq	LC14B
	cmp	#$FD
	bcc	LC15C
LC14B:	ldy	#$3F
	inc	$0429
	lda	$10
	and	#$02
	bne	LC15C
	dec	$0429
	dec	$0429
LC15C:	tya
	and	$10
	bne	LC163
	dec	$47, x
LC163:	jsr	LB178
	lda	$0477, x
	bne	LC185
	ldy	$1F, x
	bpl	LC1A9
	jsr	L92B7
	lda	#$36
	sta	$04AF
	inc	$0627
	lda	#$05
	sta	$0534
	lda	#$00
	sta	a:$50
	rts

LC185:	lda	$33, x
	cmp	#$30
	bcs	LC1A9
	ldy	$04C7
	bne	LC197
	cmp	#$18
	bcs	LC1A9
	jmp	LA4B6

LC197:	lda	#$00
	sta	$04C7
	sta	$9C
	sta	$3C
	lda	$33, x
	adc	#$20
	sta	$32
	sta	$042B
LC1A9:	jsr	LC20F
	lda	$0429
	sec
	sbc	#$04
	sta	$0293
	adc	#$07
	sta	$0297
	adc	#$08
	sta	$029B
	lda	#$20
	ldy	$47, x
	cpy	#$FD
	bmi	LC1C9
	lda	#$15
LC1C9:	adc	$042C
	sta	$0290
	sta	$0294
	sta	$0298
	lda	#$8C
	sta	$0291
	sta	$0295
	sta	$0299
	lda	$10
	lsr	a
	and	#$03
	sta	$00
	lsr	a
	ror	a
	ror	a
	and	#$C0
	ora	$00
	sta	$0292
	.byte	$8D
	.byte	$96
LC1F3:	.byte	$02
	sta	$029A
	rts

LC1F8:	lda	$A8, x
	cmp	#$01
	bne	LC20C
	sta	$B1, x
	sta	$04C7
	lda	#$02
	sta	apu_dpcm_queue
	lda	#$FE
	sta	$47, x
LC20C:	jsr	LA956
LC20F:	lda	$042C
	sta	$00
	lda	$0429
	sec
	sbc	#$08
	sta	$01
	lda	#$02
	sta	$02
	sta	$05
	sta	$0C
	lda	$65, x
	and	#$23
	sta	$03
	ldy	#$00
	ldx	#$57
	jsr	LAF80
	lda	$01
	clc
	adc	#$10
	sta	$01
	dec	$02
	lda	$042C
	sta	$00
	ldy	#$10
	ldx	#$57
	jmp	LAF80

LC246:	.byte	$F0
LC247:	brk
LC248:	.byte	$F0
LC249:	lda	#$00
	sta	$EE
	lda	$9F, x
	and	#$08
	lsr	a
	lsr	a
	lsr	a
	sta	$07
	ldy	$07
	lda	$0429
	pha
	clc
	adc	LC246, y
	sta	$0429
	lda	#$41
	ldy	$045C, x
	beq	LC26C
	lda	#$49
LC26C:	jsr	LAEE0
	jsr	LABB2
	sty	a:$F4
	pla
	clc
	ldy	$07
	adc	LC247, y
	sta	$0429
	lda	#$45
	ldy	$045C, x
	beq	LC288
	lda	#$4D
LC288:	jmp	LAEE0

	jsr	L9855
	lda	#$04
	sta	$0465, x
	lda	#$00
	sta	$79, x
	rts

LC298:	cpx	#$20
	beq	LC2AC
LC29C:	.byte	$04
	.byte	$0C
	.byte	$04
	.byte	$0C
LC2A0:	.byte	$04
	.byte	$04
	.byte	$0C
	.byte	$0C
LC2A4:	ora	($FF, x)
LC2A6:	rol	a
	.byte	$D6
LC2A8:	ora	($FF, x)
LC2AA:	clc
	inx
LC2AC:	lda	#$02
	sta	$6F, x
	inc	$9F, x
	ldy	$0465, x
	dey
	bne	LC308
	lda	#$03
	sta	$09
	sta	$04F8
	jsr	L9D61
LC2C2:	jsr	LA662
	bmi	LC304
	ldy	$00
	lda	$1F, x
	sta	$04EF, y
	lda	#$F0
	sta	$47, y
	lda	#$29
	sta	$90, y
	lda	#$30
	sta	$0453, y
	lda	$33, x
	pha
	ldx	$09
	lda	LC298, x
	sta	$3D, y
	lda	$0429
	adc	LC29C, x
	sta	$29, y
	pla
	adc	LC2A0, x
	sta	$33, y
	lda	#$00
	sta	$15, y
	tya
	tax
	jsr	L9839
	ldx	$12
LC304:	dec	$09
	bpl	LC2C2
LC308:	lda	$10
	and	#$1F
	bne	LC328
	jsr	LA662
	ldx	$00
	lda	#$20
	sta	$90, x
	lda	$29, x
	sbc	#$08
	sta	$29, x
	lda	$33, x
	adc	#$18
	sta	$33, x
	jsr	L9887
	ldx	$12
LC328:	lda	$10
	and	#$01
	bne	LC358
	lda	$79, x
	and	#$01
	tay
	lda	$47, x
	clc
	adc	LC2A8, y
	sta	$47, x
	cmp	LC2AA, y
	bne	LC342
	inc	$79, x
LC342:	lda	$0477, x
	and	#$01
	tay
	lda	$3D, x
	clc
	adc	LC2A4, y
	sta	$3D, x
	cmp	LC2A6, y
	bne	LC358
	inc	$0477, x
LC358:	jsr	LC249
	jsr	LB178
	jmp	LB17D

LC361:	.byte	$3F
	.byte	$3F
	.byte	$3F
	.byte	$7F
LC365:	.byte	$D4
	cld
	.byte	$DA
	dec	$5BB5, x
	and	#$10
	beq	LC37A
	jsr	LB72F
	lda	#$00
	sta	$046E, x
	jmp	LA551

LC37A:	lda	#$02
	sta	$6F, x
	lda	$10
	sta	$044A, x
	inc	$9F, x
	inc	$9F, x
	jsr	LB207
	jsr	LAE56
	lda	$5B, x
	pha
	and	#$04
	beq	LC39B
	jsr	LA929
	lda	#$00
	sta	$3D, x
LC39B:	pla
	and	#$03
	beq	LC3A6
	jsr	LB1D6
	jsr	LA90B
LC3A6:	txa
	asl	a
	asl	a
	asl	a
	adc	$10
	ldy	$04F8
	and	LC361, y
	ora	$47, x
	bne	LC3CC
	lda	$04BB
	and	#$1F
	ora	LC365, y
	sta	$47, x
	jsr	L988A
	lda	$04F8
	cmp	#$02
	bcs	LC3CC
	asl	$3D, x
LC3CC:	jsr	LB17D
	jmp	L9967

	lda	$B1, x
	bne	LC41A
	lda	$5B, x
	and	#$10
	ora	$A8, x
	beq	LC41A
	lda	#$01
	sta	$90, x
	jsr	L9839
	lda	$0441, x
	sta	$06
	lda	#$FF
	sta	$0441, x
	jsr	LA662
	bmi	LC41A
	ldy	$00
	lda	$06
	sta	$0441, y
	lda	$29, x
	sta	$29, y
	lda	$15, x
	sta	$15, y
	ldx	$00
	lda	#$24
	sta	$90, x
	jsr	L9887
	inc	$B1, x
	jsr	L9839
	lda	#$04
	sta	$0489, x
	ldx	$12
LC41A:	jsr	LACCC
	jsr	LB207
	lda	$5B, x
	pha
	and	#$04
	beq	LC42A
	jsr	LA929
LC42A:	pla
	and	#$03
	beq	LC435
	jsr	LB1D6
	jsr	LB17D
LC435:	inc	$9F, x
	lda	$B1, x
	bne	LC466
	txa
	asl	a
	asl	a
	asl	a
	asl	a
	adc	$10
	and	#$7F
	bne	LC449
	jsr	L988A
LC449:	lda	$9F, x
	and	#$7F
	bne	LC466
	jsr	L988A
	jsr	LA93A
	bmi	LC466
	ldx	$00
	lda	#$25
	jsr	LA3AE
	ldx	$00
	dec	$33, x
	dec	$33, x
	ldx	$12
LC466:	jsr	L995F
	jmp	LAE56

	lda	$51, x
	cmp	#$01
	beq	LC47D
	lda	#$C1
	sta	$65, x
	sta	$9F, x
	lda	#$37
	jmp	LAEE0

LC47D:	lda	$B1, x
	bne	LC49D
	lda	a:$F4
	pha
	lda	$042C
	clc
	adc	#$F5
	sta	$042C
	jsr	LABB2
	sty	a:$F4
	lda	#$3D
	jsr	LAEE0
	pla
	sta	a:$F4
LC49D:	lda	$33, x
	sta	$042C
	jsr	LAED4
	lda	#$02
	sta	$6F, x
	tya
	clc
	adc	#$08
	sta	a:$F4
	lda	$00
	sta	$042C
	lda	#$D0
	sta	$046E, x
	lda	#$39
	jsr	LAEE0
	lda	#$50
	ldy	$B1, x
	beq	LC4C7
	lda	#$52
LC4C7:	sta	$046E, x
	rts

	jsr	L9855
	lda	$33, x
	sta	$B1, x
	rts

	inc	$9F, x
	inc	$9F, x
	inc	$79, x
	lda	$79, x
	cmp	#$40
	bcs	LC4E4
	lda	#$E0
	sta	$33, x
LC4E3:	rts

LC4E4:	bne	LC4EE
	lda	#$D0
	sta	$47, x
	lda	$B1, x
	sta	$33, x
LC4EE:	lda	#$08
	sta	apu_sfx_queue2
	lda	$79, x
	cmp	#$80
	bcc	LC507
	cmp	#$DC
	bcs	LC507
	ldy	#$03
	and	#$10
	beq	LC505
	ldy	#$FB
LC505:	sty	$47, x
LC507:	inc	$47, x
	jsr	LB178
	lda	$EE
	and	#$0C
	bne	LC4E3
	lda	$79, x
	sta	$07
	lda	#$29
	sta	$65, x
	lda	#$53
	ldy	$79, x
	cpy	#$DC
	bcc	LC524
	lda	#$55
LC524:	jsr	LAEE0
	jsr	LABB2
	lda	#$55
	ldx	$07
	cpx	#$E0
	bcc	LC534
	lda	#$3A
LC534:	sta	$0201, y
	lda	#$55
	cpx	#$E8
	bcc	LC53F
	lda	#$3A
LC53F:	sta	$0205, y
	lda	#$55
	cpx	#$F0
	bcc	LC54A
	lda	#$3A
LC54A:	sta	$0209, y
	lda	#$55
	cpx	#$F8
	bcc	LC555
	lda	#$3A
LC555:	sta	$020D, y
	ldx	a:$F4
	lda	$0202, x
	sta	$0202, y
	sta	$0206, y
	sta	$020A, y
	sta	$020E, y
	lda	$0429
	clc
	adc	#$04
	sta	$0203, y
	sta	$0207, y
	sta	$020B, y
	sta	$020F, y
	ldx	$12
	lda	$33, x
	clc
	adc	#$0F
	sta	$0200, y
	adc	#$10
	sta	$0204, y
	adc	#$10
	sta	$0208, y
	adc	#$10
	sta	$020C, y
LC595:	rts

	.byte	$1C
LC597:	cpx	$01
	.byte	$FF
	inc	$9F, x
	jsr	LACCC
	jsr	LAC1F
	jsr	LB207
	lda	$5B, x
	and	#$03
	beq	LC5AE
	jsr	LB1D6
LC5AE:	lda	$5B, x
	and	#$04
	beq	LC5D5
	lda	$47, x
	pha
	jsr	LA929
	pla
	ldy	$042F, x
	beq	LC5D5
	cmp	#$18
	bmi	LC5CD
	jsr	LA90B
	lda	#$F0
	sta	$47, x
	bne	LC5FB
LC5CD:	lda	#$00
	sta	$042F, x
	jsr	L9839
LC5D5:	lda	$0E
	cmp	#$10
	beq	LC5E0
	dec	$9F, x
	jmp	LA7CC

LC5E0:	jsr	LB168
	iny
	sty	$6F, x
	lda	$10
	and	#$01
	bne	LC5FB
	lda	$3D, x
	cmp	LC595, y
	beq	LC5FB
	clc
	adc	LC597, y
	sta	$3D, x
	inc	$9F, x
LC5FB:	jsr	L995F
	inc	$04A4, x
	jmp	LAE56

	inc	$FEFE, x
	inc	$B6B4, x
	lda	$B7, x
	clv
	.byte	$FA
	lda	$FAFA, y
	.byte	$FA
	.byte	$B2
	.byte	$B3
	ldx	LBFBE, y
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	.byte	$BF
	lsr	a
	lsr	a
	.byte	$4B
	.byte	$4B
	lsr	$5E5F, x
	.byte	$5F
	lsr	$56, x
	lda	#$A9
	lda	#$A9
	lda	#$A9
	eor	$45
	lda	#$A9
	.byte	$56
LC631:	eor	$A9A9, x
	.byte	$74
LC635:	ror	$75, x
	.byte	$77
	tya
LC639:	txs
	sta	$9C9B, y
LC63D:	txs
	sta	$9C9B, x
LC641:	.byte	$9E
	.byte	$9B
	.byte	$9F
	cli
LC645:	.byte	$5A
	eor	$5E5B, y
LC649:	.byte	$5F
	lsr	$8E5F, x
LC64D:	.byte	$8F
	.byte	$8F
	.byte	$8E
	.byte	$A6
LC651:	ldx	$A7
	.byte	$A7
	.byte	$57
LC655:	.byte	$57
	.byte	$93
	.byte	$92
	.byte	$74
LC659:	ror	$75, x
	.byte	$77
	bvs	LC6D0
	adc	($73), y
	adc	($73), y
	adc	($73), y
	bit	$26
	and	$27
	.byte	$32
	.byte	$34
	.byte	$33
	and	$33, x
	and	$33, x
	and	$FA, x
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	bcs	LC631
	.byte	$FA
	.byte	$FA
	bcs	LC635
	.byte	$FA
	.byte	$FA
	bcs	LC639
	.byte	$FA
	.byte	$FA
	bcs	LC63D
	.byte	$FA
	.byte	$FA
	bcs	LC641
	.byte	$FA
	.byte	$FA
	bcs	LC645
	.byte	$FA
	.byte	$FA
	bcs	LC649
	.byte	$FA
	.byte	$FA
	bcs	LC64D
	.byte	$FA
	.byte	$FA
	bcs	LC651
	.byte	$FA
	.byte	$FA
	bcs	LC655
	.byte	$FA
	.byte	$FA
	bcs	LC659
	ldy	#$A2
	lda	($A3, x)
	.byte	$80
	.byte	$82
	sta	($83, x)
	.byte	$F4
	stx	$F5
	.byte	$87
	sty	$86
	sta	$87
	.byte	$FC
	.byte	$FC
	.byte	$FC
	.byte	$FC
	lda	$ACFB
	lda	$ACAC
	ldy	$FBAC
	.byte	$3B
	.byte	$3B
	ldy	$FCFC
	.byte	$FC
	.byte	$FC
	.byte	$F4
	stx	$F5
	.byte	$87
LC6D0:	inc	$FEFE, x
	inc	$FEFE, x
	ora	$3CFE, x
	rol	$3F3D, x
	cli
	inc	$5A59, x
	.byte	$5B
	.byte	$5A
	inc	$FEFE, x
	eor	$5C5B, x
	inc	$5BFE, x
	.byte	$5A
	.byte	$1C
	inc	$FEFE, x
	inc	$FEFE, x
	inc	$1EFE, x
	inc	$201F, x
	.byte	$22
	and	($23, x)
	.byte	$57
	.byte	$57
	.byte	$FB
	.byte	$FB
	.byte	$57
	.byte	$57
	inc	$ABFE, x
	.byte	$AB
	.byte	$FB
	.byte	$FB
	nop
	nop
	.byte	$FB
	.byte	$FB
	.byte	$7C
	ror	$7F7D, x
	dex
	cpy	LCDCB
	dex
	cpy	LCDCB
	cpy	#$C2
	cmp	($C3, x)
	bit	$2D2E
	.byte	$2F
	stx	$8F8F
	stx	$8A88
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	.byte	$89
	.byte	$8B
	sty	$888D
	txa
	sty	$888D
	txa
	.byte	$89
	.byte	$8B
	dey
	txa
	.byte	$89
	.byte	$8B
	sty	$95, x
	sty	$95, x
	stx	$97, y
	stx	$97, y
	pha
	eor	#$48
	eor	#$FE
	inc	$FEFE, x
	.byte	$FB
	.byte	$32
	.byte	$32
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	sbc	$FDFD, x
	sbc	$FB34, x
LC75A:	sbc	$FB34, x
	bmi	LC75A
	.byte	$FB
	.byte	$FB
	.byte	$FB
	and	($FB), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	$56, x
	lsr	$56, x
	.byte	$64
	ror	$65
	.byte	$67
	pla
	ror	a
	adc	#$6B
	.byte	$FA
	jmp	(L6CFA)

	adc	$6DFA
	.byte	$FA
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$92
	ldx	$AEAF
	.byte	$AF
	sei
	.byte	$7A
	adc	$A87B, y
	tay
	.byte	$AF
	ldx	$9594
	sty	$95, x
	stx	$97, y
	stx	$97, y
	.byte	$22
	bit	$23
	and	$92
	.byte	$93
	.byte	$93
	.byte	$92
	bvc	LC7EF
	bvc	LC7F1
	ldx	$AEAF
	.byte	$AF
	bvc	LC7F7
	bvc	LC7F9
	stx	$8F8F
	stx	$5250
	eor	($53), y
LC7B0:	bvc	LC804
	eor	($53), y
	.byte	$FB
	rol	$36, x
	.byte	$4F
	.byte	$4F
	lsr	$4F4E
	lsr	$4F4F
	lsr	$9392
	.byte	$93
	.byte	$92
	stx	$8F8F
	stx	$4240
	eor	($43, x)
	rti

	.byte	$42
	eor	($43, x)
	tsx
	ldy	$BDBB, x
	tsx
	ldy	$9190, x
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	.byte	$FA
	sbc	$FDFD, x
	sbc	$6361, x
	adc	($63, x)
	adc	$63
	adc	$63
	adc	$67
	.byte	$65
LC7EF:	.byte	$67
	rts

LC7F1:	.byte	$62
	adc	($63, x)
	.byte	$64
	.byte	$62
	.byte	$65
LC7F7:	.byte	$63
	.byte	$64
LC7F9:	ror	$65
	.byte	$67
	pla
	.byte	$62
	adc	($63, x)
	.byte	$64
	adc	#$65
	.byte	$67
LC804:	lsr	$62
	adc	($63, x)
	.byte	$64
	.byte	$47
	adc	$67
	tsx
	ldy	$BDBB, x
	bvs	LC884
	adc	($73), y
	stx	$8F8F
	stx	$4544
	eor	$44
LC81C:	bpl	LC7B0
	bpl	LC82C
	ora	$99, x
	lda	($12, x)
	eor	$45
	eor	$45
	.byte	$63
	.byte	$63
	.byte	$63
	.byte	$63
LC82C:	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$13
	.byte	$9F
	clc
	.byte	$9C
	.byte	$12
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$9F
	.byte	$80
	.byte	$07
	sta	($80, x)
	sta	($81, x)
	sta	($81, x)
LC840:	lda	$050E
	asl	a
	asl	a
	sta	$0F
	lda	$050E
	cmp	#$07
	bcc	LC854
	lda	$0543
	jmp	LC857

LC854:	lda	$0542
LC857:	clc
	adc	$0F
	tax
	lda	$050E
	cmp	#$03
	bne	LC86C
	lda	($01), y
	cmp	#$90
	bne	LC86C
	lda	#$64
	bne	LC86F
LC86C:	lda	LC81C, x
LC86F:	sta	($01), y
	rts

	ldy	$E7
LC874:	jsr	LC840
	jsr	LBE0C
	dec	$050D
	bpl	LC874
	rts

LC880:	.byte	$4F
	.byte	$4F
	.byte	$83
LC883:	.byte	$50
LC884:	eor	($83), y
	ldy	$E7
	lda	$050E
	cmp	#$09
	bne	LC89A
	lda	$04B0
	beq	LC89A
	inc	$050E
	inc	$050E
LC89A:	lda	$050E
	sec
	sbc	#$09
	tax
	lda	LC880, x
	sta	($01), y
	jsr	LBE24
	lda	LC883, x
	sta	($01), y
	jsr	LBE38
	rts

	ldy	$E7
	lda	#$52
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	lda	$E7
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
LC8C7:	iny
	lda	#$54
	sta	($01), y
	iny
	lda	#$53
	sta	($01), y
	jsr	LBE38
	rts

	ldy	$E7
	lda	#$52
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	lda	$E7
	clc
	adc	#$10
	tay
	lda	#$52
	sta	($01), y
	dey
	lda	#$54
	sta	($01), y
	dey
	lda	#$55
	sta	($01), y
	jsr	LBE38
	rts

	ldy	$E7
	lda	$050E
	cmp	#$06
	bne	LC908
	lda	#$9E
	.byte	$91
LC904:	ora	($4C, x)
	.byte	$0B
	.byte	$C9
LC908:	jsr	LC840
LC90B:	jsr	LBE24
	dec	$050D
	bpl	LC908
	rts

	ldy	$E7
LC916:	jsr	LC840
	tya
	clc
	adc	#$11
	tay
	dec	$050D
	bpl	LC916
	rts

LC924:	.byte	$67
	ror	a
	.byte	$9C
	cmp	($70), y
	sty	$95, x
	lda	$050E
	tax
	cmp	#$05
	bne	LC939
	lda	$0543
	beq	LC939
	inx
LC939:	ldy	$E7
	lda	LC924, x
	sta	($01), y
	rts

LC941:	brk
	ora	$585F
	asl	a
LC946:	brk
	asl	$5860
	asl	a
LC94B:	brk
	.byte	$0F
	adc	($58, x)
	asl	a
LC950:	ldy	$E7
	lda	$050E
	sec
	sbc	#$0A
	tax
	lda	LC941, x
	sta	($01), y
	dec	$050D
	beq	LC970
LC963:	jsr	LBE0C
	lda	LC946, x
	sta	($01), y
	dec	$050D
	bne	LC963
LC970:	jsr	LBE0C
	lda	LC94B, x
	sta	($01), y
	rts

LC979:	dex
	.byte	$CB
	cpy	LC8C7
	cmp	#$40
	.byte	$04
	ora	$A2
	brk
LC984:	stx	$0B
	ldx	$E8
	jsr	LBE01
	ldx	#$05
	ldy	$E7
	ldx	$0B
	ldy	$E7
	lda	$050D
	sta	$07
	jsr	LC9C9
	inx
	lda	$07
	beq	LC9A8
LC9A0:	jsr	LBE0C
	jsr	LC9C9
	bne	LC9A0
LC9A8:	jsr	LBE0C
	inx
	jsr	LC9C9
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LC9BF
	ldx	#$03
	sta	$E7
	jmp	LC984

LC9BF:	rts

LC9C0:	.byte	$C7
	iny
	.byte	$C9
LC9C3:	.byte	$CF
	.byte	$CD
	.byte	$CD
LC9C6:	dec	$D0CE
LC9C9:	stx	$08
	txa
	bne	LC9E1
	ldx	#$02
	lda	($01), y
LC9D2:	cmp	LC9C0, x
	beq	LC9DC
	dex
	bpl	LC9D2
	bmi	LC9FA
LC9DC:	lda	LC9C3, x
	bne	LCA10
LC9E1:	ldx	$08
	cpx	#$02
	bne	LC9FA
	ldx	#$02
	lda	($01), y
LC9EB:	cmp	LC9C0, x
	beq	LC9F5
	dex
	bpl	LC9EB
	bmi	LC9FA
LC9F5:	lda	LC9C6, x
	bne	LCA10
LC9FA:	ldx	#$08
LC9FC:	lda	($01), y
	cmp	LC979, x
	beq	LCA0B
	dex
	bpl	LC9FC
	ldx	$08
	jmp	LCA12

LCA0B:	ldx	$08
	lda	LC979, x
LCA10:	sta	($01), y
LCA12:	ldx	$08
	dec	$07
	rts

	ldy	$E7
	lda	#$19
	sta	($01), y
LCA1D:	jsr	LBE24
	lda	($01), y
	cmp	#$40
	bne	LCA2C
	lda	#$1A
	sta	($01), y
	bne	LCA1D
LCA2C:	rts

	ldy	$E7
	lda	#$01
	sta	($01), y
	iny
	lda	#$02
	sta	($01), y
	rts

	ldy	$E7
	lda	#$03
	sta	($01), y
	rts

LCA40:	adc	$7271
	ldy	$E7
	lda	$050E
	sec
	sbc	#$06
	tax
	lda	LCA40, x
	sta	($01), y
LCA51:	jsr	LBE24
	lda	($01), y
	cmp	#$40
	bne	LCA61
	lda	#$6E
	sta	($01), y
	jmp	LCA51

LCA61:	tya
	sec
	sbc	#$10
	tay
	lda	#$6F
	sta	($01), y
	rts

	.byte	$A4
LCA6C:	.byte	$E7
	lda	$050E
	cmp	#$0D
	beq	LCA7B
	lda	#$C1
	sta	($01), y
LCA78:	jsr	LBE24
LCA7B:	lda	($01), y
	cmp	#$40
	bne	LCA90
	lda	#$C2
	sta	($01), y
	lda	$EC
	beq	LCA8D
	cpy	#$E0
	bcs	LCA90
LCA8D:	jmp	LCA78

LCA90:	rts

	.byte	$A4
LCA92:	.byte	$E7
	lda	#$C3
	sta	($01), y
LCA97:	tya
	sec
	sbc	#$10
	tay
	cmp	#$F0
	bcs	LCAAD
	lda	($01), y
	cmp	#$40
	bne	LCAAD
	lda	#$C2
	sta	($01), y
	jmp	LCA97

LCAAD:	rts

LCAAE:	.byte	$43
	.byte	$44
	eor	$46
	.byte	$47
	pha
	eor	#$4A
	.byte	$4B
	lsr	$414C
	.byte	$5A
	eor	$A442, y
	.byte	$E7
	ldx	$050E
	lda	LCAAE, x
	sta	($01), y
	rts

LCAC8:	bcc	LCA6C
LCACA:	lda	$050E
	sec
	sbc	#$09
	sta	$08
	ldy	$E7
	ldx	$E8
	jsr	LBE01
	ldy	$E7
	lda	#$05
	sta	$07
	lda	($01), y
	cmp	#$40
	bne	LCB01
LCAE5:	ldx	$08
	lda	LCAC8, x
	sta	($01), y
	jsr	LBE0C
	dec	$07
	bpl	LCAE5
	lda	$E7
	clc
	adc	#$10
	cmp	#$F0
	bcs	LCB01
	sta	$E7
	jmp	LCACA

LCB01:	rts

	.byte	$80
	nop
	bcc	LCB16
	bit	L1E10
	ora	($44), y
	and	$3F0E, y
	beq	LCA92
	sbc	($D6), y
	inc	$05, x
	txs
	.byte	$10
LCB16:	ror	$A832
	.byte	$11
LCB1A:	.byte	$1B
	bpl	LCB60
	.byte	$34
	and	($32), y
	php
	.byte	$3F
	.byte	$6B
	ora	($68), y
	.byte	$3F
	sec
	rol	$7C, x
	ora	($5E), y
	.byte	$37
	rol	a
	.byte	$3C
	.byte	$F2
	sty	$26
	asl	a
	.byte	$54
	clc
	and	$1072, y
	asl	$11, x
	eor	$3A
	asl	a
	.byte	$3B
	.byte	$32
	.byte	$3F
	sei
	bpl	LCB99
	.byte	$3F
	dec	$3710
	.byte	$33
	.byte	$1A
	and	($1B), y
	and	($1B), y
	.byte	$32
	.byte	$1B
	.byte	$33
	lsr	$0F8B, x
	.byte	$8B
	and	($11, x)
	.byte	$27
	rol	$08
	and	($16, x)
	.byte	$F3
	asl	a
	.byte	$32
	bit	$21
	.byte	$11
LCB60:	.byte	$2B
	.byte	$03
	inc	$10, x
LCB64:	.byte	$32
	.byte	$0B
	.byte	$23
	sbc	$09, x
	bpl	LCB85
	.byte	$F2
	.byte	$FF
	.byte	$80
	inx
	bcc	LCB83
	.byte	$22
	bpl	LCB8B
	ora	($52), y
	.byte	$39
	.byte	$F1
LCB78:	.byte	$82
	clv
	bpl	LCBA8
	ora	($F0), y
	cmp	($F6, x)
	ora	($E1, x)
	.byte	$10
LCB83:	.byte	$3B
	.byte	$0E
LCB85:	sbc	($A2), y
	inc	$02, x
	.byte	$A3
	.byte	$10
LCB8B:	.byte	$1A
	ora	($1E), y
	asl	$0E2A
	clc
	asl	$0E0D
	rol	$0E
	beq	LCB1A
LCB99:	inc	$01, x
	sta	L1E10, y
	ora	($3E), y
	asl	$10B0
	.byte	$1A
	bpl	LCBF0
	.byte	$0E
	plp
LCBA8:	.byte	$0E
	.byte	$F0
LCBAA:	.byte	$80
	.byte	$83
	ora	($17), y
	bpl	LCBFD
	.byte	$83
	.byte	$0E
LCBB2:	.byte	$83
	.byte	$0F
	.byte	$83
	.byte	$1C
	.byte	$82
	.byte	$1B
	sta	($1A, x)
	.byte	$80
	sbc	($4D), y
	inc	$02, x
	.byte	$6B
	bpl	LCBB2
	.byte	$02
	sbc	($40), y
	inc	$01, x
	.byte	$F2
	.byte	$47
	bpl	LCBE5
	ora	($4E), y
	.byte	$80
	.byte	$0F
	.byte	$80
	and	$80, x
	asl	$80
	.byte	$07
	.byte	$80
	beq	LCB78
	inc	$02, x
	sbc	($01), y
	sbc	($6C), y
	adc	$11
	.byte	$37
	.byte	$42
	bmi	LCB64
	.byte	$F0
LCBE5:	.byte	$22
	sbc	($CA), y
	sbc	$0A, x
	brk
	.byte	$FF
	.byte	$80
LCBED:	nop
	bpl	LCC00
LCBF0:	ldy	$F03C
	sty	$88F1
	iny
	.byte	$12
	sbc	$0A, x
	bpl	LCBED
	rol	a
LCBFD:	.byte	$FF
	.byte	$80
	inx
LCC00:	bcc	LCC12
	.byte	$32
	bpl	LCC19
	ora	($17), y
	.byte	$0C
	sbc	$0A, x
	ora	($0C, x)
	bpl	LCC5D
	sec
	.byte	$2B
	.byte	$34
	.byte	$23
LCC12:	.byte	$33
	.byte	$07
	.byte	$03
	php
	.byte	$33
	.byte	$4C
	.byte	$10
LCC19:	sei
	.byte	$3F
	.byte	$22
	bpl	LCC28
	ora	($7C), y
	bpl	LCC8A
	.byte	$3F
	ldy	#$11
	cli
	.byte	$3F
	.byte	$26
LCC28:	bpl	LCC9E
	bpl	LCC3E
	ora	($58), y
	.byte	$3F
	bit	$7610
	bpl	LCC72
	bpl	LCC6E
	.byte	$3F
	dey
	bpl	LCC54
	ora	($96), y
	.byte	$3F
	.byte	$74
LCC3E:	bpl	LCC92
	.byte	$3F
	.byte	$3C
	bpl	LCC98
	bpl	LCC5D
	ora	($62), y
	.byte	$3A
	ora	$1A34, y
	.byte	$34
	.byte	$1B
	and	$62, x
	bpl	LCC5D
	bpl	LCC6E
LCC54:	ora	($27), y
	asl	a
	asl	a
	and	($26, x)
	.byte	$E2
	.byte	$33
	.byte	$37
LCC5D:	.byte	$FF
	.byte	$80
	inc	$90, x
	plp
	jmp	L1E10

	ora	($49), y
	rol	$0A
	and	($0B, x)
	and	($18, x)
	.byte	$D5
LCC6E:	.byte	$0F
	.byte	$2B
	.byte	$19
	.byte	$2D
LCC72:	sei
	asl	a
	asl	a
	ora	$E426, y
	.byte	$1F
	.byte	$34
	.byte	$42
	cmp	$2D15, x
	.byte	$67
	and	#$0B
	and	#$2A
	.byte	$32
	jsr	L4533
	.byte	$D4
	.byte	$0D
	.byte	$D4
LCC8A:	asl	$2D, x
	asl	$632D
	and	#$22
	.byte	$33
LCC92:	.byte	$3B
	.byte	$52
	asl	$52, x
	.byte	$0B
	.byte	$F2
LCC98:	asl	$F2, x
	sta	$1229, x
	.byte	$26
LCC9E:	bpl	LCC92
	.byte	$14
	bmi	LCCB7
	and	($15), y
	and	($27), y
	and	($0B), y
	.byte	$D4
	.byte	$1C
	and	$2956
	.byte	$12
	bpl	LCCBF
	bpl	LCCCB
	ora	($19), y
	.byte	$34
	.byte	$54
LCCB7:	dec	$25, x
	and	$116F
	.byte	$1B
	and	#$0C
LCCBF:	bpl	LCD31
	.byte	$D4
	ora	#$D5
	ora	($2D), y
	asl	a
	and	$3465
	.byte	$2C
LCCCB:	.byte	$33
	.byte	$52
	cld
	and	$2D
LCCD0:	.byte	$80
	.byte	$3F
	.byte	$43
	php
	clc
	cmp	$12, x
	.byte	$F3
LCCD8:	ora	$752D, y
	bpl	LCD09
	bpl	LCCF7
	.byte	$23
	sbc	$0A, x
	bmi	LCCEF
	ora	($10), y
	and	($07), y
	.byte	$42
	ora	$46, x
	ora	$F6, x
	.byte	$FF
	.byte	$80
LCCEF:	inx
	bmi	LCD22
	plp
	bpl	LCD11
	ora	($52), y
LCCF7:	and	$87F1, y
	ldx	$10
	.byte	$53
	asl	$2912
	ora	$3E26
	.byte	$7F
	beq	LCD52
	sbc	($CD), y
	.byte	$C3
LCD09:	and	#$06
	and	#$09
	and	#$0C
	and	#$3E
LCD11:	.byte	$77
	beq	LCD47
	ldx	$0A0B, y
	rti

	clc
	rol	$5BF0
	beq	LCCEF
	sbc	($CA), y
	.byte	$FF
	.byte	$91
LCD22:	nop
	jsr	L8132
	.byte	$13
	asl	a
	.byte	$33
	.byte	$F0
LCD2A:	jmp	$ABF0

	sbc	($E8), y
	.byte	$F2
	.byte	$A3
LCD31:	.byte	$3B
	beq	LCD41
	beq	LCD7E
	beq	LCDA8
	sbc	($F7), y
	.byte	$F2
	beq	LCD6D
	beq	LCDB6
	beq	LCCD0
LCD41:	sbc	($10), y
	sbc	($8A), y
	sbc	$0B, x
LCD47:	brk
	.byte	$FF
	sta	($EA), y
	.byte	$02
	ora	($4B), y
	.byte	$14
	.byte	$0B
	ora	($0D), y
LCD52:	tay
	beq	LCDA9
	beq	LCD2A
	sbc	($15), y
	sbc	($CA), y
	.byte	$FF
	.byte	$80
	sbc	($91, x)
	brk
	brk
	txa
	rol	a
	bpl	LCD82
	ora	($14), y
	.byte	$2B
	.byte	$14
	asl	$0E33
	.byte	$05
LCD6D:	.byte	$0E
	tax
LCD6F:	.byte	$CB
	.byte	$27
	and	($16, x)
	iny
	.byte	$12
	rol	$04
LCD77:	and	($11, x)
	dec	$14
	.byte	$0B
	.byte	$0B
	brk
LCD7E:	ldy	$F110
	.byte	$16
LCD82:	inc	$05, x
	cpx	#$11
	and	$10
	.byte	$D4
	bpl	LCD97
	bpl	LCDA5
	ora	($F2), y
	.byte	$57
	eor	($0A), y
	bvc	LCDAA
	.byte	$F4
	.byte	$14
	.byte	$51
LCD97:	.byte	$14
	sbc	($13), y
	bvc	LCDAE
	sbc	($0E), y
	.byte	$64
	.byte	$53
	ora	$1909, y
	.byte	$8B
	asl	a
LCDA5:	.byte	$0B
	rol	$23
LCDA8:	.byte	$69
LCDA9:	.byte	$F0
LCDAA:	pla
	sbc	($F6), y
	.byte	$54
LCDAE:	ora	$190A, y
	.byte	$89
	asl	a
	.byte	$0B
	rol	$28, x
LCDB6:	jmp	(L88F0)

	eor	$19, x
	.byte	$0B
	ora	$0A89, y
	.byte	$0B
	rts

	and	$6B
	beq	LCDDB
	beq	LCD6F
	eor	($19), y
	lda	($62, x)
LCDCB:	.byte	$F0
LCDCC:	.byte	$D6
LCDCD:	.byte	$FF
	jsr	L60E0
	ora	#$44
	asl	a
	.byte	$0B
	eor	($21, x)
	.byte	$34
	asl	a
	.byte	$34
	.byte	$27
LCDDB:	and	($31), y
	and	$08, x
	rol	$21, x
	.byte	$33
	asl	$32
	asl	a
	.byte	$34
	beq	LCE3C
	bit	$3A
	and	($3A), y
	and	$35
	.byte	$2B
	.byte	$33
	.byte	$37
	.byte	$33
	.byte	$34
	.byte	$34
	and	($34), y
	php
	rol	$2A, x
	.byte	$34
	ora	($32), y
	.byte	$1C
	.byte	$32
	.byte	$12
	.byte	$33
	.byte	$23
	.byte	$34
	and	($39), y
	bit	$3932
	.byte	$32
	.byte	$34
	.byte	$32
	and	($33), y
	.byte	$0B
	.byte	$33
	.byte	$37
	and	($0D), y
	and	($21), y
	sei
	ora	$F131, x
	tsx
	sbc	($D4), y
	rol	$35, x
	and	($3A), y
	.byte	$34
	.byte	$3A
	and	($33), y
	.byte	$33
	.byte	$33
	ora	$2531, x
	.byte	$33
	.byte	$0C
	.byte	$32
	sec
	.byte	$33
	and	$33, x
	.byte	$33
	.byte	$32
	ora	$2131
	rol	$2A, x
	.byte	$32
	and	$32, x
	.byte	$0C
	and	($23), y
	.byte	$32
LCE3C:	and	($31, x)
	ora	#$33
	.byte	$14
	asl	a
	.byte	$0B
	asl	$66, x
	sei
	sbc	($3B), y
	sbc	($C0), y
	.byte	$FF
	jsr	L62E0
	ora	#$37
	asl	a
	.byte	$0B
	.byte	$42
	rol	$33
	.byte	$44
	.byte	$3A
	and	($78), y
	beq	LCE9C
	sbc	($BA), y
	sbc	($C1), y
	.byte	$5A
	.byte	$74
	.byte	$37
	and	#$99
	and	#$F0
	.byte	$77
	pla
	and	#$D9
	and	#$88
	asl	a
	.byte	$0B
	bvc	LCE82
	and	#$15
	and	$F0, x
	.byte	$D3
	jsr	L0A35
	and	$45, x
	and	$32, x
	.byte	$3B
	eor	($3A, x)
	beq	LCEA2
	.byte	$44
LCE82:	.byte	$3A
	eor	($3A, x)
	.byte	$34
	.byte	$3A
	and	($3A), y
	.byte	$3C
	.byte	$32
	sec
	.byte	$33
	.byte	$34
	.byte	$33
	and	($32), y
	asl	$09, x
	.byte	$0B
	.byte	$17
	.byte	$0B
	.byte	$33
	and	($38, x)
	sbc	($7A), y
	.byte	$F1
LCE9C:	cpy	#$FF
	.byte	$80
	inx
	.byte	$32
	clc
LCEA2:	eor	$4FE2, x
	sbc	($91, x)
	eor	($04, x)
	eor	($07, x)
	eor	($0A, x)
	eor	($0D, x)
	eor	($12, x)
	ora	$1908, y
	.byte	$5B
	asl	a
	.byte	$0B
LCEB7:	jsr	L261F
	.byte	$13
	jmp	(L2E6F)

	.byte	$12
	eor	($05, x)
	eor	($08, x)
	eor	($0B, x)
	eor	($0E, x)
	eor	($13, x)
	ora	$1909, y
	cli
LCECD:	asl	a
LCECE:	.byte	$0B
	bmi	LCEE2
	.byte	$22
	bpl	LCF40
	.byte	$FF
	jsr	L02E0
	ora	#$27
	bit	$3236
	.byte	$34
	rol	$27, x
	asl	a
	.byte	$0B
LCEE2:	.byte	$33
	and	($3D, x)
	beq	LCF08
	sbc	($A0), y
	.byte	$FF
	ldy	#$EA
	bpl	LCF08
	.byte	$62
	.byte	$3D
	.byte	$46
LCEF1:	asl	a
	.byte	$0B
	clc
	.byte	$22
	and	$50F0, x
	bcc	LCF2E
	eor	($42), y
	bpl	LCF32
	beq	LCEB7
	beq	LCEF1
	sbc	($2E), y
	.byte	$F1
LCF05:	.byte	$6F
	sbc	($97), y
LCF08:	sbc	($CA), y
	sbc	$0B, x
	.byte	$70
LCF0D:	.byte	$FF
	rol	a
	sbc	$0F, x
	.byte	$12
	.byte	$54
	.byte	$32
	ora	#$32
	.byte	$07
	brk
	php
	brk
	.byte	$43
	and	($0B), y
	and	($32), y
	rti

	.byte	$07
	brk
	php
	brk
	ora	#$00
	ora	$F040
LCF29:	and	$F6, x
	.byte	$04
	beq	LCF81
LCF2E:	beq	LCF05
	inc	$02, x
LCF32:	beq	LCF29
	inc	$04, x
	sbc	($13), y
	sbc	($95), y
	inc	$05, x
	sbc	($A0), y
	inc	$02, x
LCF40:	sbc	($C0), y
	inc	$04, x
	.byte	$FF
	ora	($05, x)
	.byte	$27
	stx	$13, y
	.byte	$B2
	.byte	$03
	.byte	$13
	.byte	$22
	.byte	$0B
	.byte	$27
	ora	#$27
	and	#$13
	and	$27, x
	cmp	$F627, y
	ora	($09, x)
	.byte	$27
	lsr	$27
	tya
	.byte	$27
	ldx	$27
	sed
	ora	$13
	.byte	$13
	.byte	$27
	clv
	ora	$27
	php
	.byte	$27
	lda	$05, x
	.byte	$27
	and	$13, x
	eor	($01, x)
	ora	($01, x)
	.byte	$01
LCF76:	.byte	$03
	bit	$58
	ora	($07, x)
	.byte	$27
	asl	a
	bit	$58
	.byte	$27
	txs
LCF81:	ora	$27
	.byte	$3A
	.byte	$27
	.byte	$DC
	.byte	$03
	.byte	$24
LCF88:	inc	$01, x
	ora	$27
LCF8C:	.byte	$3C
	bit	$D7
	ora	$42
	sta	$41
	.byte	$D4
	ora	($01, x)
	.byte	$03
	.byte	$5C
	clv
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
LCFA7:	.byte	$01
LCFA8:	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
LCFC2:	ora	($03, x)
	rti

	.byte	$47
LCFC6:	ora	($01, x)
	ora	$10
	.byte	$D3
	bpl	LCFC2
	.byte	$03
	bpl	LCFA8
	.byte	$09
LCFD1:	bpl	LCFEF-6
	bpl	LCFFD+15
	bpl	LD01B
	bpl	LD051
	.byte	$0B
	bpl	LD043
LCFDC:	bpl	LCF76
LCFDE:	bpl	LCFA7
	bpl	LCFC6
	bpl	LCFDC
	.byte	$07
	.byte	$27
	iny
	.byte	$27
	inx
	bpl	LCFDE
	ora	$10
	ldx	$10
LCFEF:	cld
	.byte	$0B
	bpl	LD02A
	bpl	LD048
	bpl	LCF8C
	bpl	LCFD1
	bpl	LCFEF
	.byte	$0B
	.byte	$10
LCFFD:	.byte	$3A
	bpl	LD056
	bpl	LD07D
	bpl	LCF88
	bpl	LCFFD
	.byte	$07
	bpl	LD02C
	bpl	LD071
	.byte	$27
	clv
	ora	($01, x)
	ora	($03, x)
	rol	$6A
	ora	$26
	.byte	$7A
	rol	$FA
	ora	($03, x)
	.byte	$26
LD01B:	.byte	$DA
	ora	$26
	adc	#$07
	tya
	ora	#$26
	rol	a
	.byte	$03
	and	$BA26, y
	.byte	$03
	.byte	$C9
LD02A:	ora	$26
LD02C:	adc	#$03
	dey
	ora	$26
	lda	#$27
	.byte	$D3
	ora	($01, x)
	ora	($03, x)
LD038:	bit	$C7
	ora	($05, x)
	.byte	$04
	sta	LC904, y
	ora	($01, x)
	.byte	$05
LD043:	.byte	$1C
	clv
	.byte	$42
	.byte	$B9
	.byte	$03
LD048:	eor	($BA, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$03
LD051:	rti

	rol	a
	ora	($01, x)
	.byte	$03
LD056:	.byte	$1C
	lda	#$01
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	bpl	LD098
	.byte	$03
	bpl	LD07C
	ora	($09, x)
	.byte	$27
	nop
	.byte	$27
	cpx	$DE27
	.byte	$27
	inc	$2703
	.byte	$EE
LD071:	ora	$27
	cpx	#$27
	.byte	$E2
	.byte	$03
	.byte	$27
	sty	$2709
	.byte	$92
LD07C:	.byte	$27
LD07D:	.byte	$B2
	.byte	$27
	sbc	#$27
	.byte	$EB
	ora	$27
	adc	($27), y
	cpx	$01
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	ora	($C8, x)
	.byte	$03
	cld
	.byte	$02
	inx
	ora	($01, x)
	ora	($07, x)
	.byte	$27
LD098:	.byte	$72
	.byte	$27
	.byte	$82
	.byte	$27
	ror	$0301, x
	.byte	$27
	cmp	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$3B
	.byte	$74
	.byte	$27
	.byte	$57
	.byte	$27
	.byte	$97
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	$27
	adc	$27
	sta	$05, x
	.byte	$42
	rol	a
	eor	($87, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	pla
	bit	$01
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($08, x)
	dey
	dey
	dey
	clv
	.byte	$80
	brk
	rti

	.byte	$9C
