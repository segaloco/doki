.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"mem.i"
.include	"tunables.i"
.include	"title.i"

start_clear_rambuf	= zp_addr_00

	.export	start_b
start_b:
	lda	save_magic
	cmp	#<ENG_SAVE_MAGIC
	bne	:+
	lda	save_magic+1
	cmp	#>ENG_SAVE_MAGIC
	beq	:+++
	: ; if (cold_boot) {
		ldy	#<((save_dat_end-save_dat)-1)
		lda	#0
		: ; for (byte of save_dat) {
			sta	save_dat, y

			dey
			bpl	:-
		; }
		lda	#<ENG_SAVE_MAGIC
		sta	save_magic
		lda	#>ENG_SAVE_MAGIC
		sta	save_magic+1
	: ; }

	lda	#<apu_alt_cycle
	sta	nmi_apu_call+OP_SIZE
	lda	#>apu_alt_cycle
	sta	nmi_apu_call+OP_SIZE+1

	lda	PPU_SR

	lda	#>(PPU_VRAM_BG_CHR_END-PPU_CHR_SIZE)
	ldy	#<(PPU_VRAM_BG_CHR_END-PPU_CHR_SIZE)
	sta	PPU_VRAM_AR
	sty	PPU_VRAM_AR
	lda	#$FF
	: ; for (y = 0xF0; y < 0x100; y++) {
		sta	PPU_VRAM_IO

		iny
		bne	:-
	; }

	lda	#0
	sta	PPU_VRAM_AR
	sta	PPU_VRAM_AR

	ldy	#>(RAM_END-$100)
	sty	start_clear_rambuf+1
	ldy	#<(RAM_END-$100)
	sty	start_clear_rambuf
	tya
	: ; for (page of ram) {
		; for (byte of page) {
			sta	(start_clear_rambuf), y

			dey
			bne	:-
		; }

		dec	start_clear_rambuf+1
		ldx	start_clear_rambuf+1
		cpx	#>oam_buffer
		bcs	:-
	; }

	lda	#TIMER_TITLE_WAIT_VAL
	sta	timer_title_wait
	lda	#0
	sta	title_no_disk

	ldy	#<((save_dat_end-save_dat)-1)
	: ; for (byte of save_dat) {
		lda	save_dat, y
		sta	game_save_dat, y

		dey
		bpl	:-
	; }

	; NOTE: Falls through to title_loop
