.include	"system/ppu.i"

.include	"mem.i"

nmi_title_buffer_mode_tbl:
	.addr	ppu_displist_data
	.addr	nmi_title_buffer_title_bg
	.addr	nmi_title_buffer_attr_1
	.addr	nmi_title_buffer_attr_2
	.addr	nmi_title_buffer_attr_3
	.addr	nmi_title_buffer_attr_4
	.addr	nmi_title_buffer_attr_5
	.addr	nmi_title_buffer_attr_6
	.addr	nmi_title_buffer_attr_0
	.addr	nmi_title_buffer_window
	.addr	nmi_title_buffer_wait
; -----------------------------
	.export	nmi_title_buffer_transfer, nmi_title_buffer_transfer_show
nmi_title_buffer_transfer_show:
	lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::objlblk_off|ppu_ctlr1::bglblk_off|ppu_ctlr1::color
	sta	ppu_ctlr1_b

nmi_title_buffer_transfer:
	lda	nmi_buffer_ptr
	asl	a
	tax
	lda	nmi_title_buffer_mode_tbl, x
	sta	ppu_displist_write_addr
	lda	nmi_title_buffer_mode_tbl+1, x
	sta	ppu_displist_write_addr+1

	lda	#0
	sta	ppu_nmi_hold
	: ; while (!(ppu_nmi_hold & 0x80)) {
		lda	ppu_nmi_hold
		bpl	:-
	; }

	rts
