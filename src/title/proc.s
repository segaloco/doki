.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"sound.i"
.include	"modes.i"
.include	"math.i"
.include	"tunables.i"
.include	"title.i"

title_clear_rambuf	= zp_addr_00

title_loop:
	lda	#0
	tay
	: ; for (work_ram) {
		sta	title_clear_rambuf, y

		iny
		cpy	#title_game_enter_clear_end
		bcc	:-
	; }

	jsr	nt_init_all

	lda	PPU_SR

	lda	#>PPU_VRAM_COLOR
	ldy	#<PPU_VRAM_COLOR
	sta	PPU_VRAM_AR
	sty	PPU_VRAM_AR
	: ; for (entry of palette) {
		lda	title_palette, y
		sta	PPU_VRAM_IO

		iny
		cpy	#<(title_palette_end-title_palette)
		bcc	:-
	; }

	lda	#<(ppu_displist+1)
	sta	ppu_displist_write_addr
	lda	#>(ppu_displist+1)
	sta	ppu_displist_write_addr+1

	lda	#fds_nmimode::game_vec_0
	sta	FDS_STACK_NMIMODE
	lda	#ppu_ctlr0::int|ppu_ctlr0::sprite_8|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1
	sta	ppu_ctlr0_b
	sta	PPU_CTLR0

	jsr	nmi_title_buffer_transfer

	lda	#nmi_title_buffer_modes::title_bg
	sta	nmi_buffer_ptr
	jsr	nmi_title_buffer_transfer

	lda	#music_base_alt::music_00
	sta	apu_music_base_req
	jsr	nmi_title_buffer_transfer_show

	lda	#3
	sta	frame_count
	lda	#37
	sta	timer_title

	lda	#176
	sta	pos_x_lo_obj+TITLE_BLIMP_OBJ_ID
	lda	#<-4
	sta	veloc_x_obj+TITLE_BLIMP_OBJ_ID

	lda	#144
	sta	pos_x_lo_obj+TITLE_BALLOON_A_OBJ_ID
	lda	#88
	sta	pos_y_lo_obj+TITLE_BALLOON_A_OBJ_ID
	lda	#72
	sta	pos_x_lo_obj+TITLE_BALLOON_B_OBJ_ID
	lda	#152
	sta	pos_y_lo_obj+TITLE_BALLOON_B_OBJ_ID
	lda	#192
	sta	pos_x_lo_obj+TITLE_BALLOON_C_OBJ_ID
	lda	#144
	sta	pos_y_lo_obj+TITLE_BALLOON_C_OBJ_ID
; -----------------------------
title_init_ptr	= zp_addr_0C
title_story_ptr	= zp_addr_0C

title_gfx_loop:
	jsr	nmi_title_buffer_transfer

	lda	title_no_disk
	asl	a
	tay
	lda	title_init_tbl, y
	sta	title_init_ptr
	lda	title_init_tbl+1, y
	sta	title_init_ptr+1
	jmp	(title_init_ptr)
; ------------------------------------------------------------
	.export	title_init_wait_eject, title_init_wait_insert
title_init_wait_eject:
	lda	FDS_DISK_SR
	lsr	a
	bcc	:+
	dec	timer_title_wait
	bpl	:++ ; if (drive_filled || --timer_title_wait < 0) {
		; if (drive_empty) {
			inc	title_no_disk
		: ; }

		lda	#TIMER_TITLE_WAIT_VAL
		sta	timer_title_wait
	: ; }

	jmp	title_init_body
; -----------------------------
title_init_wait_insert:
	lda	FDS_DISK_SR
	lsr	a
	bcs	:+
	dec	timer_title_wait
	bpl	:++
	jmp	title_game_enter ; if (drive_empty || --timer_title_wait < 0) {
		: ; if (drive_empty) {
			lda	#TIMER_TITLE_WAIT_VAL
			sta	timer_title_wait
		: ; }

	title_init_body:
		jsr	oam_init

		jsr	title_actor_blimp

		ldx	#TITLE_BALLOON_COUNT-1
		: ; for (balloon of balloons) {
			stx	actor_index
			jsr	title_actor_balloon

			lda	timer_title_balloon, x
			beq	:+ ; if (balloon.timer > 0) {
				dec	timer_title_balloon, x
			: ; }
		
			dex
			bpl	:--
		; }
	
		inc	frame_count
		lda	frame_count
		and	#MOD_16
		bne	title_gfx_loop
		dec	timer_title
		lda	timer_title
		cmp	#6
		bne	title_gfx_loop ; if ((frame_count++ % 16) == 0 && (--timer_title == 6)) {
			lda	#ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1
			sta	ppu_ctlr0_b
			sta	PPU_CTLR0
			ldx	#ppu_ctlr1::objblk_on|ppu_ctlr1::bgblk_on|ppu_ctlr1::objlblk_on|ppu_ctlr1::bglblk_on|ppu_ctlr1::color
			stx	ppu_ctlr1_b
			stx	PPU_CTLR1

			lda	PPU_SR

			lda	#>PPU_VRAM_BG1
			ldy	#<PPU_VRAM_BG1
			sta	PPU_VRAM_AR
			sty	PPU_VRAM_AR
			lda	#TITLE_BLANK_TILE
			: ; for (row of bg) {
				; for (byte of page) {
					sta	PPU_VRAM_IO

					iny
					bne	:-
				; }
		
				inx
				cpx	#>(PPU_VRAM_BG_ATTR)
				bcc	:-
			; }
		
			: ; for (remainder of bg) {
				sta	PPU_VRAM_IO

				iny
				cpy	#<(PPU_VRAM_BG_ATTR)
				bcc	:-
			; }
		
			lda	#PPU_BG_COLOR_LINE_0_ALL
			: ; for (entry of attrs) {
				sta	PPU_VRAM_IO

				iny
				bne	:-
			; }
		
			lda	PPU_SR

			lda	#>PPU_VRAM_COLOR
			ldy	#<PPU_VRAM_COLOR
			sta	PPU_VRAM_AR
			sty	PPU_VRAM_AR
			: ; for (entry of palette) {
				lda	title_wait_palette, y
				sta	PPU_VRAM_IO

				iny
				cpy	#<(title_wait_palette_end-title_wait_palette)
				bcc	:-
			; }
		
			jsr	oam_init
			lda	#ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1
			sta	ppu_ctlr0_b
			sta	PPU_CTLR0

			jsr	nmi_title_buffer_transfer
			lda	#nmi_title_buffer_modes::window
			sta	nmi_buffer_ptr
			jsr	nmi_title_buffer_transfer

			lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::objlblk_off|ppu_ctlr1::bglblk_off|ppu_ctlr1::color
			sta	ppu_ctlr1_b

			jsr	title_story_init
		
		title_story_continue:
			jsr	nmi_title_buffer_transfer
			lda	title_no_disk
			asl	a
			tay
			lda	title_story_tbl, y
			sta	title_story_ptr
			lda	title_story_tbl+1, y
			sta	title_story_ptr+1
			jmp	(title_story_ptr)
		; } else title_gfx_loop();
	; } else title_game_enter();
; -----------------------------
	.export	title_story_wait_eject, title_story_wait_insert
title_story_wait_eject:
	lda	FDS_DISK_SR
	lsr	a
	bcc	:+
	dec	timer_title_wait
	bpl	:++ ; if (drive_filled || --timer_title_wait < 0) {
		; if (drive_empty) {
			inc	title_no_disk
		: ; }

		lda	#TIMER_TITLE_WAIT_VAL
		sta	timer_title_wait
	: ; }

	jmp	title_continue
; -----------------------------
title_story_wait_insert:
	lda	FDS_DISK_SR
	lsr	a
	bcs	:++
	dec	timer_title_wait
	bpl	:+++ ; if (drive_filled && --timer_title_wait < 0) {
	title_game_enter:
		lda	#music_base_alt::none
		sta	apu_music_base_req
		jsr	nmi_title_buffer_transfer

		lda	#0
		tay
		: ; for (byte of title_game_enter_clear_ram) {
			sta	RAM_BASE, y
	
			iny
			cpy	#title_game_enter_clear_end
			bcc	:-
		; }
	
		lda	#<apu_cycle
		sta	nmi_apu_call+OP_SIZE
		lda	#>apu_cycle
		sta	nmi_apu_call+OP_SIZE+1

		jmp	oam_init
	; } else {
		: ; if (drive_empty) {
			lda	#TIMER_TITLE_WAIT_VAL
			sta	timer_title_wait
		: ; }
	
	title_continue:
		jsr	title_story_actor_proc

		lda	title_story_proc_id
		cmp	#<((title_story_actor_proc_tbl_end-title_story_actor_proc_tbl)/WORD_SIZE)
		beq	:+ ; if (proc_id < end) {
			jmp	title_story_continue
		: ; } else {
			lda	#ppu_ctlr0::sprite_8|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1
			sta	ppu_ctlr0_b
			sta	PPU_CTLR0
			jmp	title_loop
		; }
	; }
