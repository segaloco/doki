.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"actor.i"
.include	"math.i"
.include	"charmap.i"
.include	"modes.i"
.include        "sound.i"
.include	"chr.i"
.include	"tunables.i"
.include	"title.i"

; ------------------------------------------------------------
;
; ------------------------------------------------------------
TITLE_SPRITE_COUNT_BLIMP	= 14
TITLE_SPRITE_COUNT_BALLOON	= 6
TITLE_SPRITE_COUNT_BOOM		= 16

title_actor_blimp_oam_idx:	.byte	OBJ_SIZE*0
title_actor_balloon_oam_idx:	.byte	OBJ_SIZE*16, OBJ_SIZE*32, OBJ_SIZE*48
; --------------
title_actor_blimp_chr_no:
	.byte	chr_obj::chr_EA
	.byte	chr_obj::chr_EB
	.byte	chr_obj::chr_EB
	.byte	chr_obj::chr_EC
	.byte	chr_obj::chr_ED
	.byte	chr_obj::chr_EE
	.byte	chr_obj::chr_EF
	.byte	chr_obj::chr_EF
	.byte	chr_obj::chr_FA
	.byte	chr_obj::chr_FB
	.byte	chr_obj::chr_FC
	.byte	chr_obj::chr_FC
	.byte	chr_obj::chr_FE
	.byte	chr_obj::chr_FF
title_actor_blimp_chr_no_end:
	.assert	(title_actor_blimp_chr_no_end-title_actor_blimp_chr_no) = TITLE_SPRITE_COUNT_BLIMP, error, "title_actor_blimp_chr_no size mismatch"

title_actor_balloon_proc_00_chr_no:
	.byte	chr_obj::chr_F0
	.byte	chr_obj::chr_F1
	.byte	chr_obj::chr_F2
	.byte	chr_obj::chr_F3
	.byte	chr_obj::chr_F4
	.byte	chr_obj::chr_F5
title_actor_balloon_proc_00_chr_no_end:
	.assert	(title_actor_balloon_proc_00_chr_no_end-title_actor_balloon_proc_00_chr_no) = TITLE_SPRITE_COUNT_BALLOON, error, "title_actor_balloon_proc_00_chr_no size mismatch"

title_actor_balloon_proc_01_chr_no_a:
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_F8
	.byte	chr_obj::chr_F8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_F8
	.byte	chr_obj::chr_F8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8
	.byte	chr_obj::chr_E8

title_actor_balloon_proc_01_chr_no_b:
	.byte	chr_obj::chr_F8
	.byte	chr_obj::chr_F7
	.byte	chr_obj::chr_F7
	.byte	chr_obj::chr_F8
	.byte	chr_obj::chr_F9
	.byte	chr_obj::chr_E9
	.byte	chr_obj::chr_E9
	.byte	chr_obj::chr_F9
	.byte	chr_obj::chr_F9
	.byte	chr_obj::chr_E9
	.byte	chr_obj::chr_E9
	.byte	chr_obj::chr_F9
	.byte	chr_obj::chr_F8
	.byte	chr_obj::chr_F7
	.byte	chr_obj::chr_F7
	.byte	chr_obj::chr_F8
; --------------
title_actor_blimp_pos_x:	.byte	0,  8,  16, 24, 32, 0,  8,  16, 24, 32, 8,  16, 8,  16
title_actor_blimp_pos_y:	.byte	32, 32, 32, 32, 32, 40, 40, 40, 40, 40, 48, 48, 56, 56
; --------------
title_actor_balloon_proc_00_offset_x:	.byte	0, 8, 0, 8, 0,  8
title_actor_balloon_proc_00_offset_y:	.byte	0, 0, 8, 8, 16, 16
; --------------
title_actor_balloon_proc_01_offset_x:	.byte	<-8,   0,   8,   16, <-8, 0, 8, 16, <-8, 0, 8, 16, <-8,  0,  8,  16
title_actor_balloon_proc_01_offset_y:	.byte	<-8, <-8, <-8, <-8,    0, 0, 0, 0,    8, 8, 8, 8,    16, 16, 16, 16

title_actor_balloon_proc_01_attr:
	.byte	obj_attr::v_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::hv_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::v_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::hv_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::h_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::h_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::h_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::v_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::v_flip|obj_attr::priority_high|obj_attr::color0
	.byte	obj_attr::h_flip|obj_attr::priority_high|obj_attr::color0
; ------------------------------------------------------------
	.export	title_actor_blimp
title_actor_blimp:
	ldx	#TITLE_BLIMP_OBJ_ID
	jsr	eng_obj_dophysics_x
	ldy	title_actor_blimp_oam_idx
	ldx	#TITLE_SPRITE_COUNT_BLIMP-1
	: ; if (sprite of blimp) {
		lda	pos_x_lo_obj+TITLE_BLIMP_OBJ_ID
		clc
		adc	title_actor_blimp_pos_x, x
		sta	oam_buffer+OBJ_POS_H, y
		lda	title_actor_blimp_pos_y, x
		sta	oam_buffer+OBJ_POS_V, y
		lda	title_actor_blimp_chr_no, x
		sta	oam_buffer+OBJ_CHR_NO, y
		lda	#obj_attr::priority_low|obj_attr::color0
		sta	oam_buffer+OBJ_ATTR, y

		iny
		iny
		iny
		iny
		dex
		bpl	:-
	; }

	rts
; ------------------------------------------------------------
title_actor_balloon_proc_id	= zp_byte_03
title_actor_balloon_pos_x	= zp_byte_0C
title_actor_balloon_pos_y	= zp_byte_0D

	.export	title_actor_balloon
title_actor_balloon:
	lda	title_actor_balloon_proc_id, x
	asl	a
	tay
	lda	title_actor_balloon_proc_tbl+1, y
	pha
	lda	title_actor_balloon_proc_tbl, y
	pha
	rts
; --------------
	.enum	title_actor_balloon_proc
		proc_00	=	<((title_actor_balloon_proc_tbl_00-title_actor_balloon_proc_tbl)/WORD_SIZE)
		proc_01	=	<((title_actor_balloon_proc_tbl_01-title_actor_balloon_proc_tbl)/WORD_SIZE)
		proc_02	=	<((title_actor_balloon_proc_tbl_02-title_actor_balloon_proc_tbl)/WORD_SIZE)
	.endenum

title_actor_balloon_proc_tbl:
title_actor_balloon_proc_tbl_00:	.addr	title_actor_balloon_proc_00-1
title_actor_balloon_proc_tbl_01:	.addr	title_actor_balloon_proc_01-1
title_actor_balloon_proc_tbl_02:	.addr	title_actor_balloon_proc_02-1
; -----------------------------
title_actor_balloon_proc_00_frame:	.byte	48, 96, 144
; --------------
title_actor_balloon_proc_00:
	lda	frame_count
	clc
	adc	title_actor_balloon_proc_00_frame, x
	pha

	ldy	#1
	and	#MOD_32_16_UP
	bne	:+ ; if (((frame_count + this.proc_00_frame) % 32) < 16) {
		ldy	#<-1
	: ; }
	sty	veloc_x_obj, x

	ldy	#1

	pla
	and	#MOD_64_32_UP
	bne	:+ ; if (((frame_count + this.proc_00_frame) % 64) < 32) {
		ldy	#<-1
	: ; }
	sty	veloc_y_obj, x
	jsr	eng_obj_dophysics_x
	txa
	clc
	adc	#PHYSICS_X_Y_OFFSET
	tax
	jsr	eng_obj_dophysics

	ldx	actor_index
	ldy	title_actor_balloon_oam_idx, x
	lda	pos_x_lo_obj, x
	sta	title_actor_balloon_pos_x
	lda	pos_y_lo_obj, x
	sta	title_actor_balloon_pos_y

	ldx	#TITLE_SPRITE_COUNT_BALLOON-1
	: ; for (sprite of balloon) {
		lda	#obj_attr::priority_high|obj_attr::color3
		sta	oam_buffer+OBJ_ATTR, y
		lda	title_actor_balloon_pos_x
		clc
		adc	title_actor_balloon_proc_00_offset_x, x
		sta	oam_buffer+OBJ_POS_H, y
		lda	title_actor_balloon_pos_y
		clc
		adc	title_actor_balloon_proc_00_offset_y, x
		sta	oam_buffer+OBJ_POS_V, y
		lda	title_actor_balloon_proc_00_chr_no, x
		sta	oam_buffer+OBJ_CHR_NO, y

		iny
		iny
		iny
		iny
		dex
		bpl	:-
	; }

	lda	frame_count
	and	#MOD_8_4_UP
	beq	:+ ; if ((frame_count % 8) >= 4) {
		lda	#$F6
		sta	stack_varE9, y
	: ; }

	ldx	actor_index
	lda	timer_title
	cmp	title_actor_balloon_cues, x
	bne	:+ ; if (timer_title == title_actor_balloon_cues) {
		lda	#title_actor_balloon_proc::proc_01
		sta	title_actor_balloon_proc_id, x
		lda	#16
		sta	timer_title_balloon, x
		lda	#sfx_dpcm_alt::sfx_00
		sta	apu_dpcm_queue
	: ; }

	rts
; -----------------------------
title_actor_balloon_proc_01_phase	= zp_byte_0E

title_actor_balloon_proc_01:
	lda	timer_title_balloon, x
	asl	a
	asl	a
	asl	a
	sta	title_actor_balloon_proc_01_phase
	ldy	title_actor_balloon_oam_idx, x
	lda	pos_x_lo_obj, x
	sta	title_actor_balloon_pos_x
	lda	pos_y_lo_obj, x
	sta	title_actor_balloon_pos_y

	ldx	#TITLE_SPRITE_COUNT_BOOM-1
	: ; for (sprite of boom) {
		lda	title_actor_balloon_pos_x
		clc
		adc	title_actor_balloon_proc_01_offset_x, x
		sta	oam_buffer+OBJ_POS_H, y
		lda	title_actor_balloon_pos_y
		clc
		adc	title_actor_balloon_proc_01_offset_y, x
		sta	oam_buffer+OBJ_POS_V, y
		lda	title_actor_balloon_proc_01_attr, x
		sta	oam_buffer+OBJ_ATTR, y

		lda	title_actor_balloon_proc_01_chr_no_a, x
		bit	title_actor_balloon_proc_01_phase
		bvs	:+ ; if (!(title_actor_balloon_proc_01_phase & 0x40)) {
			lda	title_actor_balloon_proc_01_chr_no_b, x
		: ; }
		sta	oam_buffer+OBJ_CHR_NO, y

		iny
		iny
		iny
		iny
		dex
		bpl	:--
	; }

	ldx	actor_index
	lda	timer_title_balloon, x
	bne	:+ ; if (this.timer == 0) {
		lda	#title_actor_balloon_proc::proc_02
		sta	title_actor_balloon_proc_id, x
		lda	#100
		sta	timer_title_balloon, x
	: ; }

	rts
; -----------------------------
title_actor_balloon_proc_02_timer	= zp_byte_09

title_actor_balloon_proc_02:
	lda	timer_title_balloon, x
	beq	:++++++ ; if (this.timer != 0) {
		cmp	#40
		bcc	:+++
		cmp	#99
		bne	:+ ; if (this.timer == 99) {
			lda	title_cycle_attrs_a, x
			sta	nmi_buffer_ptr
			rts
		: cmp	#94
		bne	:+ ; ; } else if (this.timer == 94) {
			lda	title_cycle_attrs_b, x
			sta	nmi_buffer_ptr
			sta	obj_id_actor, x
		: ; }
	
			rts
		: cmp	#1
		bne	:+ ; } else if (this.timer == 1) {
			lda	#nmi_title_buffer_modes::attr_0
			sta	nmi_buffer_ptr
			rts
		: lda	title_actor_balloon_proc_02_timer, x
		bne	:+ ; } else {
		; if (this.timer_proc_02 == 0) {
			lda	obj_id_actor, x
			eor	#1
			sta	nmi_buffer_ptr
			sta	obj_id_actor, x
			lda	#5
			sta	title_actor_balloon_proc_02_timer, x
		: ; }

		dec	title_actor_balloon_proc_02_timer, x
	: ; }

	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
title_story_oam_init:
	.byte	184, chr_obj::chr_1C, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color3, 192
	.byte	184, chr_obj::chr_1E, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color3, 200
	.byte	138, chr_obj::chr_20, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color3,  96
	.byte	138, chr_obj::chr_22, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color3,  104
	.byte	154, chr_obj::chr_24, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color3,  96
	.byte	154, chr_obj::chr_26, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color3,  104
	.byte	138, chr_obj::chr_22, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color3,   112
	.byte	138, chr_obj::chr_20, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color3,   120
	.byte	154, chr_obj::chr_26, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color3,   112
	.byte	154, chr_obj::chr_24, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color3,   120
	.byte	115, chr_obj::chr_00, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color1,  96
	.byte	115, chr_obj::chr_02, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color1,  104
	.byte	131, chr_obj::chr_04, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color1,  96
	.byte	131, chr_obj::chr_06, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color1,  104
	.byte	115, chr_obj::chr_00, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color2,  112
	.byte	115, chr_obj::chr_02, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color2,  120
	.byte	131, chr_obj::chr_04, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color2,  112
	.byte	131, chr_obj::chr_06, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color2,  120
title_story_oam_init_end:
; -----------------------------
title_story_proc_01_veloc_y: .byte	<-1, 3
; -----------------------------
title_story_oam_proc_03:
	.byte	104, chr_obj::chr_28, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0, 104
	.byte	104, chr_obj::chr_2A, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0, 112
title_story_oam_proc_03_end:
; -----------------------------
title_story_oam_proc_09:
	.byte	115, chr_obj::chr_0C, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0, 80
	.byte	115, chr_obj::chr_0C, obj_attr::h_flip|obj_attr::priority_high|obj_attr::color0,  88
	.byte	131, chr_obj::chr_0E, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color0, 80
	.byte	131, chr_obj::chr_0E, obj_attr::h_flip|obj_attr::priority_high|obj_attr::color0,  88
	.byte	112, chr_obj::chr_30, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color0,  96
	.byte	112, chr_obj::chr_30, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color0,   104
	.byte	128, chr_obj::chr_32, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color0,  96
	.byte	128, chr_obj::chr_32, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color0,   104
	.byte	112, chr_obj::chr_10, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color2,  112
	.byte	112, chr_obj::chr_10, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color2,   120
	.byte	128, chr_obj::chr_12, obj_attr::no_flip|obj_attr::priority_low|obj_attr::color2,  112
	.byte	128, chr_obj::chr_12, obj_attr::h_flip|obj_attr::priority_low|obj_attr::color2,   120
	.byte	115, chr_obj::chr_14, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color1, 128
	.byte	115, chr_obj::chr_14, obj_attr::h_flip|obj_attr::priority_high|obj_attr::color1,  136
	.byte	131, chr_obj::chr_16, obj_attr::no_flip|obj_attr::priority_high|obj_attr::color1, 128
	.byte	131, chr_obj::chr_16, obj_attr::h_flip|obj_attr::priority_high|obj_attr::color1,  136
title_story_oam_proc_09_end:
; -----------------------------
title_story_proc_13_displist:
	.dbyt	PPU_VRAM_BG1+$B2
	.byte	(:++)-(:+)
	: .byte	$65, $66, $67, $68, $69, $67, $FE, $69, $67, $6A
	:

	.dbyt	PPU_VRAM_BG1+$F4
	.byte	(:++)-(:+)
	: .byte	$69, $6B, $6C, $67, $FE, $7E
	:

	.byte	NMI_LIST_END
title_story_proc_13_displist_end:
; -----------------------------
LCAB8_pos_y:				.byte	112, 112, 115, 115
title_story_proc_12_oam_offset_b:	.byte	(OBJ_SIZE*4), (OBJ_SIZE*8), (OBJ_SIZE*0), (OBJ_SIZE*12)
LC7E1: 					.byte	1, <-1
LCAB8_pos_x:				.byte	96, 112, 80, 128
title_story_proc_12_oam_offset:		.byte	(OBJ_SIZE*0), (OBJ_SIZE*12)
LCAB8_veloc_x:				.byte	3, <-3, 8, <-8
LC7EC:					.byte	<-1, 1
; -----------------------------
title_story_proc_00_veloc_x:	.byte	<-1, 1

title_story_proc_00_pos_x:	.byte	176, 208
; -----------------------------
LC904_chr_ptr:
	.addr	LC904_chr_ptr_a
	.addr	LC904_chr_ptr_b

LC904_chr_ptr_a:	.byte	chr_obj::chr_1C, chr_obj::chr_1E, chr_obj::chr_18, chr_obj::chr_1A
LC904_chr_ptr_b:	.byte	chr_obj::chr_1E, chr_obj::chr_1C, chr_obj::chr_1A, chr_obj::chr_18
; -----------------------------
LCA88_chr_lo:	.byte	chr_obj::chr_08, chr_obj::chr_30
LCA88_chr_hi:	.byte	chr_obj::chr_0A, chr_obj::chr_32
; -----------------------------
title_story_proc_00_chr_ptr:
	.addr	title_story_proc_00_chr_ptr_a
	.addr	title_story_proc_00_chr_ptr_b

title_story_proc_00_chr_ptr_a:	.byte	chr_obj::chr_00, chr_obj::chr_02, chr_obj::chr_04, chr_obj::chr_06
title_story_proc_00_chr_ptr_b:	.byte	chr_obj::chr_02, chr_obj::chr_00, chr_obj::chr_06, chr_obj::chr_04
; -----------------------------
title_story_proc_04_pos_y:	.byte	240, 104
title_story_proc_04_times:	.byte	8, 32
; ------------------------------------------------------------
LC904_chr_ptr_00		= zp_addr_02
timer_title_story_proc_04	= zp_byte_04
timer_title_story		= zp_byte_05
title_story_chr_ptr_00		= zp_addr_06
title_story_proc_00_attr_a	= zp_byte_0A
title_story_proc_00_attr_b	= zp_byte_0B

	.export	title_story_init
title_story_init:
	ldy	#<((title_story_oam_init_end-title_story_oam_init)-1)
	: ; for (entry of title_story_oam_init) {
		lda	title_story_oam_init, y
		sta	oam_buffer, y

		dey
		bpl	:-
	; }

	lda	#0
	sta	title_story_proc_id
	sta	timer_title_story_b
	sta	zp_player_work_byte_1
	lda	#255
	sta	timer_title_story
	lda	#192
	sta	pos_x_lo_obj+0
	rts
; ------------------------------------------------------------
	.export	title_story_actor_proc
	.export	title_story_actor_proc_tbl, title_story_actor_proc_tbl_end
title_story_actor_proc:
	lda	title_story_proc_id
	jsr	tbljmp
title_story_actor_proc_tbl:
	.addr	title_story_proc_00
	.addr	title_story_proc_01
	.addr	title_story_proc_02
	.addr	title_story_proc_03
	.addr	title_story_proc_04
	.addr	title_story_proc_05
	.addr	title_story_proc_06
	.addr	title_story_proc_07
	.addr	title_story_proc_08
	.addr	title_story_proc_09
	.addr	title_story_proc_10
	.addr	title_story_proc_11
	.addr	title_story_proc_12
	.addr	title_story_proc_13
	.addr	title_story_proc_14
title_story_actor_proc_tbl_end:
; ------------------------------------------------------------
title_story_proc_00:
	dec	timer_title_story
	lda	timer_title_story
	bne	:+ ; if (--timer == 0) {
		lda	#0
		sta	zp_obj_work_byte_1+0
		sta	disp_dir_obj+0
		lda	#136
		sta	pos_y_lo_obj+1
		inc	title_story_proc_id
		lda	#chr_obj::chr_1C
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_CHR_NO
		lda	#chr_obj::chr_1E
		sta	oam_buffer+(OBJ_SIZE*1)+OBJ_CHR_NO
		lda	#obj_attr::no_flip|obj_attr::priority_high|obj_attr::color3
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_ATTR
		sta	oam_buffer+(OBJ_SIZE*1)+OBJ_ATTR
		jmp	:++++
	: lda	timer_title_story
	and	#MOD_2
	bne	:+++ ; } else if ((timer % 2) == 0) {
		lda	zp_obj_work_byte_1+0
		and	#MOD_2
		tax
		asl	a
		tay
		jsr	LC904
		lda	pos_x_lo_obj+0
		clc
		adc	title_story_proc_00_veloc_x, x
		sta	pos_x_lo_obj+0
		cmp	title_story_proc_00_pos_x, x
		bne	:++ ; if ((obj[0].pos_x += proc_00_veloc_x[x]) == proc_00_pos_x[x]) {
			inc	zp_obj_work_byte_1+0
			lda	zp_obj_work_byte_1+0
			and	#MOD_2
			tax
			asl	a
			tay
			jsr	LC904
			ldy	#0
			ldx	#0
			: ; for (sprite of object) {
				lda	(LC904_chr_ptr_00), y
				sta	oam_buffer+OBJ_CHR_NO, x
				lda	oam_buffer+OBJ_ATTR, x
				eor	#obj_attr::h_flip
				sta	oam_buffer+OBJ_ATTR, x
		
				inx
				inx
				inx
				inx
				iny
				cpy	#2
				bne	:-
			; }
		: ; }
	
		jsr	LCA1B
	: ; }

	lda	#obj_attr::no_flip|obj_attr::priority_low|obj_attr::color1
	sta	title_story_proc_00_attr_a
	lda	#obj_attr::no_flip|obj_attr::priority_low|obj_attr::color2
	sta	title_story_proc_00_attr_b
	ldx	#(WORD_SIZE*0)
	lda	timer_title_story
	and	#MOD_64_32_UP
	bne	:+ ; if ((timer % 64) < 32) {
		ldx	#(WORD_SIZE*1)
		lda	#obj_attr::h_flip|obj_attr::priority_low|obj_attr::color1
		sta	title_story_proc_00_attr_a
		lda	#obj_attr::h_flip|obj_attr::priority_low|obj_attr::color2
		sta	title_story_proc_00_attr_b
	: ; }

	lda	title_story_proc_00_chr_ptr, x
	sta	title_story_chr_ptr_00
	inx
	lda	title_story_proc_00_chr_ptr, x
	sta	title_story_chr_ptr_00+1
	ldy	#0
	ldx	#(OBJ_SIZE*0)
	: ; for (sprite of object) {
		lda	(title_story_chr_ptr_00), y
		sta	oam_buffer+(OBJ_SIZE*10)+OBJ_CHR_NO, x
		sta	oam_buffer+(OBJ_SIZE*14)+OBJ_CHR_NO, x
		lda	title_story_proc_00_attr_a
		sta	oam_buffer+(OBJ_SIZE*10)+OBJ_ATTR, x
		lda	title_story_proc_00_attr_b
		sta	oam_buffer+(OBJ_SIZE*14)+OBJ_ATTR, x

		inx
		inx
		inx
		inx
		iny
		cpy	#4
		bne	:-
	; }

	rts
; ------------------------------------------------------------
LC904:
	lda	LC904_chr_ptr, y
	sta	LC904_chr_ptr_00
	iny
	lda	LC904_chr_ptr, y
	sta	LC904_chr_ptr_00+1
	rts
; ------------------------------------------------------------
title_story_proc_01:
	lda	zp_obj_work_byte_1+0
	bne	:+
	lda	pos_y_lo_obj+1
	cmp	#112
	bcs	:++ ; if (obj[0].work_byte_1 == 0 && obj[1].pos_y < 112) {
		inc	zp_obj_work_byte_1+0
		lda	#chr_obj::chr_2C
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_CHR_NO
		lda	#chr_obj::chr_2E
		sta	oam_buffer+(OBJ_SIZE*1)+OBJ_CHR_NO
		lda	#ACTOR_DIR_RIGHT
		sta	disp_dir_obj+0
		lda	#184
		sta	pos_y_lo_obj+0
		lda	#<-48
		sta	veloc_y_obj+0
		lda	#3
		sta	veloc_x_obj+0
		lda	#0
		sta	zp_byte_09
		jmp	:++
	: lda	pos_y_lo_obj+1
	cmp	#136
	bcc	:+ ; } else if (obj[1].pos_y >= 136) {
		inc	title_story_proc_id
		lda	#8
		sta	timer_title_story_b
	: ; }

	ldy	zp_obj_work_byte_1+0
	lda	pos_y_lo_obj+1
	clc
	adc	title_story_proc_01_veloc_y, y
	sta	pos_y_lo_obj+1
	ldy	zp_obj_work_byte_1+0
	ldx	#(OBJ_SIZE*0)
	: ; for (sprite of object) {
		lda	pos_y_lo_obj+1
		sta	oam_buffer+(OBJ_SIZE*2)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*3)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*6)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*7)+OBJ_POS_V, x
		clc
		adc	#PPU_CHR_HEIGHT_PX*2
		sta	oam_buffer+(OBJ_SIZE*4)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*5)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*8)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*9)+OBJ_POS_V, x

		dey
		bmi	:+ ; if (y >= 0) {
			ldx	#(OBJ_SIZE*8)
			jmp	:-
		; } else break;
	: ; }

	lda	disp_dir_obj+0
	beq	:+ ; if (obj[0].disp_dir != 0) {
		jsr	title_story_proc_02
	: ; }

	rts
; ------------------------------------------------------------
TITLE_STORY_PROC_02_ACCEL_Y	= 3

title_story_proc_02:
	jsr	eng_obj_dophysics_y
	lda	veloc_y_obj+0
	clc
	adc	#TITLE_STORY_PROC_02_ACCEL_Y
	sta	veloc_y_obj+0
	jsr	eng_player_dophysics_x
	lda	pos_y_lo_obj+0
	cmp	#184
	bcc	:+ ; if (obj[0].pos_y >= 184) {
		inc	title_story_proc_id
	: ; }

	lda	pos_y_lo_obj+0
	sta	oam_buffer+(OBJ_SIZE*0)+OBJ_POS_V
	sta	oam_buffer+(OBJ_SIZE*1)+OBJ_POS_V
	lda	pos_x_lo_obj+0
	sta	oam_buffer+(OBJ_SIZE*0)+OBJ_POS_H
	clc
	adc	#PPU_CHR_WIDTH_PX
	sta	oam_buffer+(OBJ_SIZE*1)+OBJ_POS_H
	rts
; ------------------------------------------------------------
title_story_proc_03:
	ldy	#(OBJ_SIZE*18)
	ldx	#0
	: ; for (entry of title_story_oam_proc_03) {
		lda	title_story_oam_proc_03, x
		sta	oam_buffer, y

		iny
		inx
		cpx	#<(title_story_oam_proc_03_end-title_story_oam_proc_03)
		bne	:-
	; }

	inc	title_story_proc_id
	lda	#64
	sta	timer_title_story_b
	lda	#0
	sta	disp_dir_obj+0
	lda	#32
	sta	timer_title_story_proc_04
	rts
; ------------------------------------------------------------
title_story_proc_04:
	dec	timer_title_story_b
	lda	timer_title_story_b
	bne	:+ ; if (--timer_b == 0) {
		inc	title_story_proc_id
	: ; }
; --------------
LC9D4:
	dec	timer_title_story_proc_04
	lda	timer_title_story_proc_04
	bne	:+ ; if (--timer_proc_04 == 0) {
		lda	disp_dir_obj+0
		and	#ACTOR_DIR_RIGHT
		tax
		ldy	#(OBJ_SIZE*18)
		lda	title_story_proc_04_pos_y, x
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_POS_V, y
		sta	oam_buffer+(OBJ_SIZE*1)+OBJ_POS_V, y
		inc	disp_dir_obj+0
		lda	title_story_proc_04_times, x
		sta	timer_title_story_proc_04
	: ; }

	rts
; ------------------------------------------------------------
title_story_proc_05:
	dec	pos_x_lo_obj+0
	lda	pos_x_lo_obj+0
	cmp	#43
	bcs	:+ ; if (--obj[0].pos_x < 43) {
		lda	oam_buffer+(OBJ_SIZE*0)+OBJ_ATTR
		ora	#obj_attr::priority_bit
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_ATTR
		lda	pos_x_lo_obj+0
	: ; }

	cmp	#40
	bcs	:+ ; if (obj[0].pos_x < 40) {
		lda	#OAM_INIT_SCANLINE-8
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_POS_V
		sta	oam_buffer+(OBJ_SIZE*1)+OBJ_POS_V
		inc	title_story_proc_id
		lda	#159
		sta	timer_title_story_b
	: ; }

	ldy	#0
	jsr	LC904
; --------------
LCA1B:
	lda	pos_x_lo_obj+0
	sta	oam_buffer+(OBJ_SIZE*0)+OBJ_POS_H
	clc
	adc	#PPU_CHR_WIDTH_PX
	sta	oam_buffer+(OBJ_SIZE*1)+OBJ_POS_H

	ldy	#WORD_SIZE*0
	inc	timer_title_story_b
	lda	timer_title_story_b
	and	#MOD_8_4_UP
	beq	:+ ; if ((++timer_b % 8) >= 4) {
		ldy	#WORD_SIZE*1
	: ; }
	lda	(LC904_chr_ptr_00), y
	sta	oam_buffer+(OBJ_SIZE*0)+OBJ_CHR_NO
	iny
	lda	(LC904_chr_ptr_00), y
	sta	oam_buffer+(OBJ_SIZE*1)+OBJ_CHR_NO

	lda	title_story_proc_id
	beq	:+ ; if () {
		jsr	LC9D4
	: ; }

	rts
; ------------------------------------------------------------
title_story_proc_06:
	dec	timer_title_story_b
	lda	timer_title_story_b
	bne	:+ ; if (--timer_b == 0) {
		inc	title_story_proc_id
	: ; }

	jsr	LC9D4
	rts
; ------------------------------------------------------------
title_story_proc_07:
	jsr	oam_init
	inc	title_story_proc_id
	lda	#96
	sta	timer_title_story_b
	rts
; ------------------------------------------------------------
title_story_proc_08:
	dec	timer_title_story_b
	lda	timer_title_story_b
	bne	:+ ; if (--timer_b == 0) {
		inc	title_story_proc_id
	: ; }

	rts
; ------------------------------------------------------------
title_story_proc_09:
	ldy	#<((title_story_oam_proc_09_end-title_story_oam_proc_09)-1)
	: ; for (entry of title_story_oam_proc_09) {
		lda	title_story_oam_proc_09, y
		sta	oam_buffer, y

		dey
		bpl	:-
	; }

	inc	title_story_proc_id
	lda	#64
	sta	timer_title_story_b
	rts
; ------------------------------------------------------------
title_story_proc_10:
	dec	timer_title_story_b
	lda	timer_title_story_b
	bne	:+ ; if (--timer_b == 0) {
		ldx	#0
		jsr	LCA88
		lda	#255
		sta	timer_title_story_b
		inc	title_story_proc_id
	: ; }

	rts
; ------------------------------------------------------------
LCA88:
	lda	LCA88_chr_lo, x
	sta	oam_buffer+(OBJ_SIZE*4)+OBJ_CHR_NO
	sta	oam_buffer+(OBJ_SIZE*5)+OBJ_CHR_NO
	lda	LCA88_chr_hi, x
	sta	oam_buffer+(OBJ_SIZE*6)+OBJ_CHR_NO
	sta	oam_buffer+(OBJ_SIZE*7)+OBJ_CHR_NO
	rts
; ------------------------------------------------------------
title_story_proc_11:
	dec	timer_title_story_b
	lda	timer_title_story_b
	bne	:+ ; if (--timer_b == 0) {
		lda	#0
		sta	timer_title_story
		jsr	LCAB8
		inc	title_story_proc_id
	: ; }

	ldx	#0
	lda	timer_title_story_b
	and	#MOD_128_64_UP
	bne	:+ ; if ((timer_b % 128) < 64) {
		ldx	#1
	: ; }
	jsr	LCA88
	rts
; ------------------------------------------------------------
LCAB8:
	ldy	timer_title_story
	lda	LCAB8_pos_y, y
	sta	pos_y_lo_obj+0
	lda	LCAB8_pos_x, y
	sta	pos_x_lo_obj+0
	lda	#<-64
	sta	veloc_y_obj+0
	lda	LCAB8_veloc_x, y
	sta	veloc_x_obj+0
	rts
; ------------------------------------------------------------
TITLE_STORY_PROC_12_ACCEL_Y	= 4

title_story_proc_12:
	jsr	eng_obj_dophysics_y
	lda	veloc_y_obj+0
	clc
	adc	#TITLE_STORY_PROC_12_ACCEL_Y
	sta	veloc_y_obj+0
	jsr	eng_player_dophysics_x
	lda	pos_y_lo_obj+0
	cmp	#142
	bcc	:++ ; if (obj[0].pos_y >= 142) {
		inc	timer_title_story
		lda	timer_title_story
		cmp	#4
		bne	:+ ; if (++timer == 4) {
			inc	title_story_proc_id
			rts
		: ; } else {
			jsr	LCAB8
			jmp	:+++
		; }
	: lda	veloc_y_obj+0
	bmi	:++
	lda	timer_title_story
	cmp	#2
	bcc	:++ ; } else if (obj[0].veloc_y >= 0 && timer >= 2) {
		and	#MOD_2
		tay
		lda	title_story_proc_12_oam_offset, y
		tax
		ldy	#4-1
		: ; for (sprite of object) {
			lda	oam_buffer+OBJ_ATTR, x
			ora	#obj_attr::priority_bit
			sta	oam_buffer+OBJ_ATTR, x
	
			inx
			inx
			inx
			inx
			dey
			bpl	:-
		; }
	: ; }

	; if (obj[0].pos_y < 142 || timer < 4) {
		ldy	timer_title_story
		lda	title_story_proc_12_oam_offset_b, y
		tax
		lda	pos_y_lo_obj+0
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*1)+OBJ_POS_V, x
		clc
		adc	#PPU_CHR_HEIGHT_PX*2
		sta	oam_buffer+(OBJ_SIZE*2)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*3)+OBJ_POS_V, x
		lda	pos_x_lo_obj+0
		sta	oam_buffer+(OBJ_SIZE*0)+OBJ_POS_H, x
		sta	oam_buffer+(OBJ_SIZE*2)+OBJ_POS_H, x
		clc
		adc	#PPU_CHR_WIDTH_PX
		sta	oam_buffer+(OBJ_SIZE*1)+OBJ_POS_H, x
		sta	oam_buffer+(OBJ_SIZE*3)+OBJ_POS_H, x
		rts
	; } else rts;
; ------------------------------------------------------------
title_story_proc_13:
	ldy	#<((title_story_proc_13_displist_end-title_story_proc_13_displist)-1)
	: ; for (entry of title_story_proc_13_displist) {
		lda	title_story_proc_13_displist, y
		sta	ppu_displist_data, y

		dey
		bpl	:-
	; }

	inc	title_story_proc_id
	lda	#<256
	sta	timer_title_story_b
	rts
; ------------------------------------------------------------
title_story_proc_14:
	dec	timer_title_story_b
	lda	timer_title_story_b
	bne	:+ ; if (--timer_b == 0) {
		lda	#<((title_story_actor_proc_tbl_end-title_story_actor_proc_tbl)/WORD_SIZE)
		sta	title_story_proc_id
	: ; }

	rts
