.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"tunables.i"

.include	"mem.i"



map_health_bar:
	.byte	ENG_HEALTH_EMPTY_TILE, ENG_HEALTH_EMPTY_TILE, ENG_HEALTH_EMPTY_TILE, ENG_HEALTH_EMPTY_TILE
	.byte	ENG_HEALTH_FULL_TILE, ENG_HEALTH_EMPTY_TILE, ENG_HEALTH_EMPTY_TILE, ENG_HEALTH_EMPTY_TILE
	.byte	ENG_HEALTH_FULL_TILE, ENG_HEALTH_FULL_TILE, ENG_HEALTH_EMPTY_TILE, ENG_HEALTH_EMPTY_TILE
	.byte	ENG_HEALTH_FULL_TILE, ENG_HEALTH_FULL_TILE, ENG_HEALTH_FULL_TILE, ENG_HEALTH_EMPTY_TILE
	.byte	ENG_HEALTH_FULL_TILE, ENG_HEALTH_FULL_TILE, ENG_HEALTH_FULL_TILE, ENG_HEALTH_FULL_TILE

eng_pow_offset_y:
	.byte	0, ENG_POW_OFFSET_Y, 0, <(-ENG_POW_OFFSET_Y)

pal_sky_flash:
	.byte	PPU_COLOR_LEV2|PPU_COLOR_RED, PPU_COLOR_LEV2|PPU_COLOR_GREEN, PPU_COLOR_LEV2|PPU_COLOR_BLUE, PPU_COLOR_LEV2|PPU_COLOR_RED

	.export eng_area_sec_routines
eng_area_sec_routines:
	lda	eng_timer_sky_flash
	beq	:++ ; if (eng_timer_sky_flash) {
		dec	eng_timer_sky_flash
		ldx	ppu_displist_offset
		lda	#>(PPU_COLOR_PAGE_OBJ)
		sta	ppu_displist_addr_dbyt, x
		lda	#<(PPU_COLOR_PAGE_OBJ)
		sta	ppu_displist_addr_dbyt+1, x
		lda	#PPU_COLOR_ROW_SIZE
		sta	ppu_displist_count, x
		lda	eng_sky_color
		ldy	eng_timer_sky_flash
		beq	:+ ; if (eng_timer_sky_flash) {
			tya
			and	#PPU_COLOR_INDEX_MASK
			tay
			lda	pal_sky_flash, y
		: ; }

		sta	ppu_displist_payload, x
		lda	palette_player+1
		sta	ppu_displist_payload+1, x
		lda	palette_player+2
		sta	ppu_displist_payload+2, x
		lda	palette_player+3
		sta	ppu_displist_payload+3, x
		lda	#NMI_LIST_END
		sta	ppu_displist_payload+PPU_COLOR_ROW_SIZE, x
		txa
		clc
		adc	#PPU_COLOR_ROW_SIZE+WORD_SIZE+1
		sta	ppu_displist
	: ; }

	lda	#ENG_HEALTH_METER_Y_POS
	sta	zp_work+$00
	jsr	actor_sprite_alloc

	lda	hp_player
	beq	:+ ; if (hp_player) {
		and	#$F0
		lsr	a
		lsr	a
		adc	#ENG_HEALTH_MAX
	: ; }
	tax
	lda	#$FE
	sta	zp_work+$03
	: ; for (i = 0xFE, x = ((hp_player & 0xF0) / 2) + ENG_HEALTH_MAX, y; i != *(hp_player+1); i++, x++, y += 4) {
		lda	map_health_bar, x
		sta	oam_buffer+OBJ_CHR_NO, y
		lda	#ENG_HEALTH_METER_POS_H
		sta	oam_buffer+OBJ_POS_H, y
		lda	#(obj_attr::priority_high|obj_attr::color1)
		sta	oam_buffer+OBJ_ATTR, y
		lda	zp_work+$00
		sta	oam_buffer+OBJ_POS_V, y
		clc
		adc	#ENG_HEALTH_METER_Y_OFFSET
		sta	zp_work+$00

		inx
		iny
		iny
		iny
		iny
		inc	zp_work+$03
		lda	zp_work+$03
		cmp	hp_player_max
		bne	:-
	; }

	lda	eng_timer_pow_quake
	beq	:++ ; if (eng_timer_pow_quake) {
		dec	eng_timer_pow_quake
		lsr	a
		and	#$01
		tay

		lda	ppu_scc_v
		bpl	:+ ; if (ppu_scc_v < 0) {
			iny
			iny
		: ; }

		lda	eng_pow_offset_y, y
		sta	eng_bg_y
		jmp	eng_actors_pow_clear
	: ; } else {
		rts
	; }
