.include	"system/ppu.i"

.include	"tunables.i"

.include	"mem.i"

.include	"unk.i"
; -----------------------------------------------------------
eng_map_floor_bytes_h:
	.incbin	"data/map_floor_h.bin"

eng_map_floor_bytes_v:
	.incbin "data/map_floor_v.bin"
; ----------------------------
	.export eng_map_page_decoded_lo, eng_map_page_decoded_hi
eng_map_page_decoded_lo:
	.byte	<map_6000
	.byte	<map_60F0
	.byte	<map_61E0
	.byte	<map_62D0
	.byte	<map_63C0
	.byte	<map_64B0
	.byte	<map_65A0
	.byte	<map_6690
	.byte	<map_6780
	.byte	<map_6870
	.byte	<map_700

eng_map_page_decoded_hi:
	.byte	>map_6000
	.byte	>map_60F0
	.byte	>map_61E0
	.byte	>map_62D0
	.byte	>map_63C0
	.byte	>map_64B0
	.byte	>map_65A0
	.byte	>map_6690
	.byte	>map_6780
	.byte	>map_6870
	.byte	>map_700
; ----------------------------
eng_map_ispace_sprite:
	.byte	$73, $75
	.byte	$CA, $CC
	.byte	$C7, $C8
	.byte	$CD, $CE
	.byte	$01, $02
	.byte	$84, $87
	.byte	$5F, $61
	.byte	$08, $0A
	.byte	$0D, $0F
	.byte	$53, $55

eng_map_ispace_sprite_rev:
	.byte	$75, $73
	.byte	$CC, $CA
	.byte	$C8, $C7
	.byte	$CE, $CD
	.byte	$02, $01
	.byte	$87, $84
	.byte	$61, $5F
	.byte	$0A, $08
	.byte	$0F, $0D
	.byte	$55, $53
; -----------------------------------------------------------
eng_map_reset_ADDR_0	= tbl_6900

eng_map_reset:
	lda	#<(eng_map_reset_ADDR_0)
	sta	zp_work+$0A
	ldy	#>(eng_map_reset_ADDR_0)
	sty	zp_work+$0A+1
	ldy	#$5F
	lda	#$40
	: ; for (y = 0x5F; y >= 0 && zp_addr_0A < 0x60; y--) {
		sta	(zp_work+$0A), y
		dey
		cpy	#$FF
		bne	:- ; if () {
			dec	zp_work+$0A+1
			ldx	zp_work+$0A+1
			cpx	#$60
			bcs	:-
		; }
	; }

	lda	#0
	sta	ppu_scc_h
	sta	ppu_scc_v
	sta	eng_map_page_x
	sta	zp_byte_D5
	sta	eng_map_bg_tile_off_v
	sta	pos_y_hi_screen
	sta	pos_y_lo_screen
	sta	pos_x_hi_screen_left
	sta	pos_x_lo_screen_left
	sta	a:ppu_scroll_req
	rts
; -----------------------------------------------------------
	.export eng_map_palette_set_outside
eng_map_palette_set_outside:
	jsr	eng_map_pointer_set
	ldy	#0
	lda	(zp_map_pointer), y

eng_map_palette_set: 
	sta	zp_byte_0F
	and	#$38
	asl	a
	tax
	lda	file_bg_color, x
	sta	eng_sky_color
	lda	#>(PPU_VRAM_COLOR)
	sta	ppu_displist_addr_dbyt
	lda	#<(PPU_VRAM_COLOR)
	sta	ppu_displist_addr_dbyt+1
	lda	#PPU_COLOR_TOTAL_SIZE
	sta	ppu_displist_count
	ldy	#0
	: ; for (color in bg) {
		lda	file_bg_color, x
		sta	ppu_displist_payload+<PPU_COLOR_PAGE_BG, y

		inx
		iny
		cpy	#PPU_COLOR_PAGE_SIZE
		bcc	:-
	; }

	lda	zp_byte_0F
	and	#$03
	asl	a
	sta	zp_byte_0F
	asl	a
	adc	zp_byte_0F
	asl	a
	tax
	ldy	#0
	: ; for (color in obj_2_thru_4) {
		lda	file_obj_color, x
		sta	ppu_displist_payload+<PPU_COLOR_PAGE_OBJ+PPU_COLOR_LINE_SIZE, y

		inx
		iny
		cpy	#(PPU_COLOR_PAGE_SIZE-PPU_COLOR_LINE_SIZE)
		bcc	:-
	; }

	lda	#0
	sta	ppu_displist_payload+<PPU_COLOR_PAGE_OBJ+PPU_COLOR_LINE_SIZE, y
	ldy	#PPU_COLOR_LINE_SIZE-1
	: ; for (color in obj_1) {
		lda	palette_player, y
		sta	ppu_displist_payload+<PPU_COLOR_PAGE_OBJ, y

		dey
		bpl	:-
	; }

	ldx	#PPU_COLOR_LINE_SIZE-1
	ldy	#<PPU_COLOR_PAGE_OBJ
	: ; for (x = PPU_COLOR_LINE_SIZE-1, y = <PPU_COLOR_PAGE_OBJ; x >= 0; x--, y -= PPU_COLOR_LINE_SIZE) {
		lda	eng_sky_color
		sta	ppu_displist_payload, y

		iny
		iny
		iny
		iny
		dex
		bpl	:-
	; }

	rts
; -----------------------------------------------------------
	.export eng_map_ispace_make
eng_map_ispace_make:
	lda	eng_level_area
	sta	eng_level_area_b
	lda	#$30
	sta	zp_byte_0F
	jsr	eng_map_palette_set
	lda	pos_x_hi_screen_left
	sta	eng_map_fg_page_no
	lda	pos_x_lo_screen_left
	clc
	adc	#$08
	bcc	:+ ; if (pos_x_lo_screen_left + 0x08 > 0xFF) {
		inc	eng_map_fg_page_no
	: ; }

	and	#$F0
	pha
	sec
	sbc	pos_x_lo_screen_left
	sta	eng_camera_move_x
	pla
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	eng_map_bg_tile_off_h
	lda	#0
	sta	eng_map_bg_tile_off_v
	lda	eng_map_fg_page_no
	sta	zp_addr_0C+1
	jsr	eng_map_bg_tile_off_calc

	ldy	eng_map_bg_tile_off
	ldx	#$10-1
	: ; for (x = 0x10-1; x >= 0; x--) {
		lda	(zp_addr_01), y
		jsr	eng_map_ispace_remap
		sta	map_700, x
		tya
		clc
		adc	#$10
		tay
		txa
		clc
		adc	#$10
		tax
		and	#$F0
		bne	:- ; if () {
			tya
			and	#$0F
			tay
			jsr	eng_map_area_increment_h

			dex
			bpl	:-
		; }
	; }

	rts
; ----------------------------
eng_map_ispace_remap:
	sty	zp_byte_08
	stx	zp_byte_07

	ldx	#$14-1
	: ; for (x = 0x13; x >= 0; x--) {
		cmp	eng_map_ispace_sprite, x
		beq	:+++

		dex
		bpl	:-
	; }

	cmp	#$41
	beq	:+
	cmp	#$42
	beq	:+
	jmp	:++++
	: ; if (a.in(0x41, 0x42)) {
		sec
		sbc	#$41
		tay
		lda	eng_actor_lifeheart_pull_tbl, y
		bne	:+ ; if (!eng_actor_lifeheart_pull_tbl[a-0x41]) {
			ldx	zp_byte_07
			jsr	sub_C0DB
		: ; }

		lda	#$41
		jmp	:++
	: ; } ??? {
		lda	eng_map_ispace_sprite_rev, x
	: ; }

	ldx	zp_byte_07
	ldy	zp_byte_08
	rts
; ----------------------------
	.export eng_map_ispace_clear
eng_map_ispace_clear:
	ldx	#0
	stx	eng_level_h
	: ; for (x = 0; x < 0x100; x++) {
		lda	#$40
		sta	map_700, x

		inx
		bne	:-
	; }

	lda	eng_level_area
	sta	eng_level_area_b
	lda	#$04
	sta	eng_level_area
	lda	#$0A
	jsr	eng_map_pointer_set
	ldy	#$00
	lda	#$0A
	sta	eng_map_fg_page_no
	sta	eng_map_bg_page_no
	sty	eng_map_bg_tile_off_v
	sty	eng_map_bg_tile_off_h
	sty	tbl_55E
	ldy	#$03
	sty	byte_541
	ldy	#$04
	jsr	eng_map_bg_read_page
	ldy	#$02
	lda	(zp_map_pointer), y
	and	#$03
	sta	byte_542
	lda	(zp_map_pointer), y
	lsr	a
	lsr	a
	and	#$03
	sta	byte_543
	jsr	eng_map_pointer_set
	lda	#$0A
	sta	eng_map_fg_page_no
	lda	#$00
	sta	eng_map_bg_tile_off_v
	sta	eng_map_bg_tile_off_h
	lda	#$03
	sta	zp_byte_04
	jsr	eng_map_fg_read_next
	lda	#$01
	sta	eng_level_h
	rts
; -----------------------------------------------------------
eng_map_music_load:
	ldy	#3
	lda	(zp_map_pointer), y
	and	#$03
	sta	byte_544
	cmp	apu_song_index
	beq	:+
	lda	timer_player_star
	cmp	#ENG_INVINC_MUSIC_CUTOFF_FRAME
	bcs	:+ ; if (timer_player_star < ENG_INVINC_MUSIC_CUTOFF_FRAME) {
		lda	#$80
		sta	apu_music_base_req
	: ; }

eng_map_music_load_rts:
	rts
; ----------------------------
eng_map_music_change_tbl:
	.byte	$01, $04, $08, $40, $20
; ----------------------------
	.export eng_map_music_change_tbl, eng_map_music_change
eng_map_music_change:
	lda	byte_544
	cmp	apu_song_index
	beq	:+ ; if (byte_544 != apu_song_index) {
		tax
		stx	apu_song_index
		lda	timer_player_star
		cmp	#ENG_INVINC_MUSIC_CUTOFF_FRAME
		bcs	eng_map_music_load_rts ; if (timer_player_star < ENG_INVIC_MUSIC_CUTOFF_FRAME) {
			lda	eng_map_music_change_tbl, x
			sta	apu_music_base_req
		; }
	: ; }

	rts
; -----------------------------------------------------------
	.export eng_map_set_stage
eng_map_set_stage:
	lda	eng_page_current
	asl	a
	tay
	lda	tbl_51D, y
	sta	eng_level
	iny
	lda	tbl_51D, y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	eng_level_area
	lda	tbl_51D, y
	and	#$0F
	sta	eng_level_page
	rts
; -----------------------------------------------------------
	.export eng_map_load_area
eng_map_load_area:
	jsr	eng_map_reset
	jsr	eng_scroll_reset
	jsr	eng_map_pointer_set
	jsr	eng_map_music_load
	ldy	#3
	lda	(zp_map_pointer), y
	lsr	a
	and	#$1C
	sta	tbl_55E
	jsr	eng_map_pointer_set
	ldy	#0
	lda	(zp_map_pointer), y
	asl	a
	lda	#0
	rol	a
	sta	eng_level_h
	lda	#0
	sta	eng_map_fg_page_no
	ldy	#2
	lda	(zp_map_pointer), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	byte_53F
	lda	(zp_map_pointer), y
	and	#$03
	sta	byte_542
	lda	(zp_map_pointer), y
	lsr	a
	lsr	a
	and	#$03
	sta	byte_543
	dey
	lda	(zp_map_pointer), y
	and	#$1F
	cmp	#$1F
	beq	:+ ; if () {
		sta	byte_541
		iny
		iny
		iny
		lda	#0
		sta	eng_map_bg_tile_off_h
		sta	eng_map_bg_tile_off_v
		jsr	eng_map_bg_read
	: ; }

	lda	#0
	sta	eng_map_bg_tile_off_v
	lda	#3
	sta	zp_byte_04
	jsr	eng_map_fg_read
	lda	#$22
	ora	frame_count
	sta	byte_4BA
	rts
; -----------------------------------------------------------
eng_map_pointer_set:
	ldy	course_sub
	lda	file_area_offsets, y
	clc
	adc	eng_level_area
	tay
	lda	file_map_pointer_lo, y
	sta	zp_map_pointer
	lda	file_map_pointer_hi, y
	sta	zp_map_pointer+1
	rts
; -----------------------------------------------------------
eng_map_fg_read:
	lda	#0
	sta	eng_map_fg_page_no
	ldy	#0
	jmp	eng_map_fg_read_next

eng_map_fg_read_next:
	ldy	zp_byte_04
	: ; for () {
		iny
		lda	(zp_map_pointer), y
		cmp	#$FF
		bne	:+
			rts
		:

		lda	(zp_map_pointer), y
		and	#$0F
		sta	eng_map_bg_tile_off_h
		lda	(zp_map_pointer), y
		and	#$F0
		cmp	#$F0
		bne	:+
			lda	eng_map_bg_tile_off_h
			sty	zp_byte_0F
			jsr	eng_map_fg_special
			ldy	zp_byte_0F
			jmp	:--
	: ; }

	jsr	eng_map_update_v
	iny
	lda	(zp_map_pointer), y
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	sta	byte_50E
	cmp	#3
	bcs	LBC01
	pha
	lda	(zp_map_pointer), y
	and	#$0F
	sta	byte_50E
	pla
	beq	LBC1C ; if () {
		pha
		jsr	eng_map_bg_tile_off_calc
		lda	(zp_map_pointer), y
		and	#$0F
		sty	zp_byte_04
		pla
		cmp	#$01
		bne	:+
			jsr	sub_C0B2
			jmp	LBC19
		: cmp	#$02
		bne	:+
			jsr	sub_C0D8
			jmp	LBC19
		: jmp	LBC19
		LBC01: ; if () {
			lda	(zp_map_pointer), y
			and	#$0F
			sta	byte_50D
			sty	zp_byte_04
			jsr	eng_map_bg_tile_off_calc
			lda	byte_50E
			sec
			sbc	#$03
			sta	byte_50E
			jsr	sub_C072
		LBC19: ; }

		jmp	eng_map_fg_read_next
	LBC1C: ; } else {
		jsr	eng_map_bg_tile_off_calc
		lda	(zp_map_pointer), y
		and	#$0F
		sty	zp_byte_04
		jsr	sub_C08F
		jmp	LBC19
	; }
; -----------------------------------------------------------
eng_map_update_v:
	clc
	adc	eng_map_bg_tile_off_v
	bcc	:+ ; if (wrap) {
		adc	#$0F
		jmp	:++
	: cmp	#$F0
	bne	:++ ; } else if ((a + eng_map_bg_tile_off_v) == 0xF0) {
		lda	#0
	: ; }
	; if (wrapped || (a + eng_map_bg_tile_off_v) == 0xF0) {
		inc	eng_map_fg_page_no
	: ; }

	sta	eng_map_bg_tile_off_v
	rts
; -----------------------------------------------------------
eng_map_fg_special:
	jsr	tbljmp
	.addr	eng_map_byte_skip_1
	.addr	eng_map_byte_skip_1
	.addr	eng_map_fg_page_skip_1
	.addr	eng_map_fg_page_skip_2
	.addr	eng_map_fg_rebase
	.addr	eng_map_page_pointer
	.addr	eng_map_byte_skip_1
eng_map_bg_special:
	jsr	tbljmp
	.addr	eng_map_floor_set_a
	.addr	eng_map_floor_set_b
	.addr	eng_map_bg_page_skip_1
	.addr	eng_map_bg_page_skip_2
	.addr	eng_map_bg_rebase
	.addr	eng_map_nop
	.addr	eng_map_floor_type
; -----------------------------------------------------------
eng_map_fg_page_skip_2:
	inc	eng_map_fg_page_no
eng_map_fg_page_skip_1:
	inc	eng_map_fg_page_no
	lda	#0
	sta	eng_map_bg_tile_off_v
	rts

eng_map_bg_page_skip_2:
	inc	eng_map_bg_page_no
eng_map_bg_page_skip_1:
	inc	eng_map_bg_page_no
	lda	#$00
	sta	zp_byte_0E
	sta	zp_byte_09
	rts
; -----------------------------------------------------------
eng_map_byte_skip_2:
	inc	zp_byte_0F

eng_map_byte_skip_1:
	inc	zp_byte_0F
	rts
; -----------------------------------------------------------
eng_map_page_pointer:
	ldy	zp_byte_0F
	iny
	lda	eng_map_fg_page_no
	asl	a
	tax
	lda	(zp_map_pointer), y
	sta	tbl_51D, x
	iny
	inx
	lda	(zp_map_pointer), y
	sta	tbl_51D, x
	sty	zp_byte_0F
	rts
; -----------------------------------------------------------
eng_map_ground_offset:
	ldy	zp_byte_0F
	iny
	lda	(zp_map_pointer), y
	and	#$E0
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	rts
; ----------------------------
eng_map_floor_set_a:
	jsr	eng_map_ground_offset
	jmp	eng_map_floor_set

eng_map_floor_set_b:
	jsr	eng_map_ground_offset
	clc
	adc	#$08

eng_map_floor_set:
	sta	zp_byte_0E
	lda	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		lda	zp_byte_0E
		asl	a
		asl	a
		asl	a
		asl	a
		sta	zp_byte_0E
	: ; }

	rts
; -----------------------------------------------------------
eng_map_fg_rebase:
	lda	#0
	sta	eng_map_fg_page_no
	sta	eng_map_bg_tile_off_v
	rts

eng_map_bg_rebase:
	lda	#0
	sta	eng_map_bg_page_no
	sta	zp_work+$0E
	rts
; -----------------------------------------------------------
eng_map_nop:
	rts
; -----------------------------------------------------------
eng_map_floor_type:
	ldy	zp_byte_0F
	iny
	lda	(zp_map_pointer), y
	and	#$0F
	asl	a
	asl	a
	sta	tbl_55E
	rts
; -----------------------------------------------------------
eng_map_bg_read:
	lda	#0
	sta	eng_map_bg_page_no

eng_map_bg_read_page:
	lda	#0
	sta	zp_byte_09

eng_map_bg_read_str:
	lda	(zp_map_pointer), y
	cmp	#$FF
	bne	:+ ; if () {
		lda	#$0A
		sta	eng_map_bg_page_no
		inc	eng_map_bg_page_no
		lda	#$00
		sta	zp_byte_0E
		jmp	eng_map_bg_floor_render
	: ; }

	lda	(zp_map_pointer), y
	and	#$F0
	beq	eng_map_bg_str_skip_2
	cmp	#$F0
	bne	eng_map_bg_str_norm
	lda	(zp_map_pointer), y
	and	#$0F
	sty	zp_byte_0F
	jsr	eng_map_bg_special
	ldy	zp_byte_0F
	lda	(zp_map_pointer), y
	and	#$0F
	cmp	#$02
	bcc	eng_map_bg_floor_render
	cmp	#$05
	bne	:+ ; if () {
		iny
		jmp	eng_map_bg_str_skip_2
	: ; }

	cmp	#$06
	bne	eng_map_bg_str_skip_1
eng_map_bg_str_skip_2:
	iny
eng_map_bg_str_skip_1:
	iny
	jmp	eng_map_bg_read_str

eng_map_bg_str_norm:
	clc
	adc	zp_byte_09
	bcc	eng_map_bg_str_page_skip_0
	adc	#$0F
	jmp	eng_map_bg_str_page_skip_1

eng_map_bg_str_page_skip_0:
	cmp	#$F0
	bne	eng_map_bg_page_done
	lda	#$00
eng_map_bg_str_page_skip_1:
	inc	eng_map_bg_page_no
eng_map_bg_page_done:
	sta	zp_byte_09
	jmp	eng_map_bg_str_skip_2
; -----------------------------------------------------------
eng_map_bg_floor_render:
	; while (eng_map_bg_tile_off < zp_work[0x0E]) {
		jsr	eng_map_bg_tile_off_calc
		jsr	eng_map_floor_data_write

		lda	eng_level_h
		beq	:+ ; if (eng_level_h) {
			inc	eng_map_bg_tile_off_h
			lda	eng_map_bg_tile_off_h
			cmp	#$10
			bne	:++ ; if (++eng_map_bg_tile_off_h == 0x10) {
				inc	eng_map_fg_page_no

				lda	#0
				sta	eng_map_bg_tile_off_h
				jmp	:++
			; }
		: ; } else { 
			lda	#$10
			jsr	eng_map_update_v
		: ; }

		lda	eng_map_fg_page_no
		cmp	eng_map_bg_page_no
		bne	eng_map_bg_floor_render ; if (eng_map_fg_page_no == eng_map_bg_page_no) {
			lda	eng_level_h
			beq	:+ ; if (eng_level_h) {
				lda	eng_map_bg_tile_off_h
				cmp	zp_work+$0E
				bcc	eng_map_bg_floor_render
				bcs	:++
			: ;  } else { 
				lda	eng_map_bg_tile_off_v
				cmp	zp_work+$0E
				bcc	eng_map_bg_floor_render
			: ; }
		; }
	; }

	lda	(zp_map_pointer), y
	cmp	#$FF
	beq	eng_map_floor_byte_rts ; if () {
		iny
		lda	(zp_map_pointer), y
		and	#$1F
		sta	byte_541
		jmp	eng_map_bg_str_skip_1
	; } else rts;
; ----------------------------
eng_map_floor_byte:
	lda	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		lda	eng_map_floor_bytes_v, x
		rts

	:
		lda	eng_map_floor_bytes_h, x
eng_map_floor_byte_rts:
		rts
	; }
; -----------------------------------------------------------
eng_map_floor_data_write:
	sty	zp_work+$04

	lda	byte_541
	asl	a
	asl	a
	tax
	ldy	eng_map_bg_tile_off
	: ; for (x = byte_541*4, y = eng_map_bg_tile_off; ??? && y & 0x0F; x++) {
		jsr	eng_map_floor_byte
		jsr	eng_map_floor_data_write_1
		jsr	eng_map_floor_byte
		jsr	eng_map_floor_data_write_2
		jsr	eng_map_floor_byte
		jsr	eng_map_floor_data_write_3
		jsr	eng_map_floor_byte
		jsr	eng_map_floor_data_write_4

		lda	eng_level_h
		beq	:+ ; if (eng_level_h) {
			inx
			bcs	:++
			bcc	:-
		: ; } else {
			inx
			tya
			and	#$0F
			bne	:--
		; }
	: ; }

	ldy	zp_work+$04
	rts
; -----------------------------------------------------------
eng_map_floor_data_write_1:
	lsr	a
	lsr	a

eng_map_floor_data_write_2:
	lsr	a
	lsr	a

eng_map_floor_data_write_3: 
	lsr	a
	lsr	a

eng_map_floor_data_write_4:
	and	#$03
	stx	zp_byte_03
	beq	:+++ ; if (zp_byte_03) {
		clc
		adc	tbl_55E
		tax
		lda	eng_level_h
		bne	:+ ; if (!eng_level_h) {
			lda	tbl_BFFB, x
			jmp	:++
		: ; } else {
			lda	tbl_BFDB, x
		: ; }
		sta	(zp_addr_01), y
	: ; }

	ldx	zp_byte_03
	lda	eng_level_h
	bne	:+ ; if (!eng_level_h) {
		iny
		rts
	: ; } else {
		tya
		clc
		adc	#$10
		tay
		rts
	; }
; -----------------------------------------------------------
	.export eng_map_bg_tile_off_calc
eng_map_bg_tile_off_calc:
	ldx	eng_map_fg_page_no
	jsr	eng_map_bg_decoded_off_get
	lda	eng_map_bg_tile_off_v
	clc
	adc	eng_map_bg_tile_off_h
	sta	eng_map_bg_tile_off
	rts
; ----------------------------
	.export	eng_map_bg_decoded_off_get
eng_map_bg_decoded_off_get:
	lda	eng_map_page_decoded_lo, x
	sta	map_bg_ptr
	lda	eng_map_page_decoded_hi, x
	sta	map_bg_ptr+1
	rts
; ----------------------------
	.export	eng_map_area_increment_h
eng_map_area_increment_h:
	iny
	tya
	and	#$0F
	bne	:+ ; if () {
		tya
		sec
		sbc	#$10
		tay
		stx	zp_addr_0A+1
		ldx	eng_map_fg_page_no
		inx
		stx	zp_addr_0C+1
		jsr	eng_map_bg_decoded_off_get
		ldx	zp_addr_0A+1
	: ; }

	rts
; ----------------------------
	.export	eng_map_area_increment_v
eng_map_area_increment_v:
	tya
	clc
	adc	#$10
	tay
	cmp	#$F0
	bcc	:+ ; if () {
		ldx	eng_map_fg_page_no
		inx
		jsr	eng_map_bg_decoded_off_get
		tya
		and	#$0F
		tay
	: ; }

	rts
; -----------------------------------------------------------
eng_map_door_skip:
	ldy	zp_byte_04
	iny
	lda	(zp_map_pointer), y
	sta	zp_byte_07
	iny
	lda	(zp_map_pointer), y
	sta	zp_byte_08
	sty	zp_byte_04
	lda	eng_map_fg_page_no
	asl	a
	tay
	lda	zp_byte_07
	sta	tbl_51D, y
	iny
	lda	zp_byte_08
	sta	tbl_51D, y
	rts
; -----------------------------------------------------------
	.export	eng_map_ppu_buffer_write_str_a
eng_map_ppu_buffer_write_str_a:
	.byte	$00, $11, $11, $BB, $EF, $C6, $00, $00
	.byte	$00, $11, $11, $BB, $EF, $C6, $00

eng_map_ppu_buffer_write_str_b:
	.byte   $FF, $EE, $EE, $44, $00, $10, $39, $FF
	.byte   $FF, $EE, $EE, $44, $00, $10, $39

eng_map_ppu_buffer_write_base:
	.byte	$40, $02
	.byte	$08, $21

	.byte	$02, $40
	.byte	$21, $08
; ----------------------------
ENG_MAP_PPU_BUFFER_WRITE_ADDR		= $1BE0
ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE	= 8

	.export eng_map_ppu_buffer_write
eng_map_ppu_buffer_write:
	ldx	ppu_displist_offset
	lda	#>(ENG_MAP_PPU_BUFFER_WRITE_ADDR)
	sta	ppu_displist_addr_dbyt, x
	sta	ppu_displist_addr_dbyt+5, x
	lda	frame_count
	asl	a
	and	#ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE
	ora	#<(ENG_MAP_PPU_BUFFER_WRITE_ADDR)
	sta	ppu_displist_addr_dbyt+1, x
	eor	#ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE
	sta	ppu_displist_addr_dbyt+5+1, x
	lda	#2
	sta	ppu_displist_count, x
	sta	ppu_displist_count+5, x
	lda	frame_count
	lsr	a
	lsr	a
	lsr	a
	and	#$03
	tay
	lda	eng_map_ppu_buffer_write_base, y
	sta	ppu_displist_payload, x
	lda	eng_map_ppu_buffer_write_base+1, y
	sta	ppu_displist_payload+1, x
	lda	frame_count
	eor	#$FF
	asl	a
	and	#$07
	tay
	sty	zp_byte_01
	lda	#>(ENG_MAP_PPU_BUFFER_WRITE_ADDR+2*(ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE))
	sta	ppu_displist_payload+7, x
	lda	#<(ENG_MAP_PPU_BUFFER_WRITE_ADDR+2*(ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE))
	sta	ppu_displist_payload+8, x
	lda	#(2*(ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE))
	sta	ppu_displist_payload+9, x
	lda	#0
	sta	ppu_displist_payload+26, x
	sta	ppu_displist_payload+5, x
	sta	ppu_displist_payload+6, x
	txa
	clc
	adc	#$1D
	sta	ppu_displist_offset

	lda	#ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE-1
	sta	zp_byte_00
	: ; for (i = 8-1, x, y; i >= 0; i--, x++, y++) {
		lda	eng_map_ppu_buffer_write_str_a, y
		sta	ppu_displist_payload+10, x

		inx
		iny
		dec	zp_byte_00
		bpl	:-
	; }

	ldy	zp_byte_01
	lda	#ENG_MAP_PPU_BUFFER_WRITE_PAGE_SIZE-1
	sta	zp_byte_00
	: ; for (i = 7, x, y = zp_byte_01; i >= 0; i--, x++, y++) {
		lda	eng_map_ppu_buffer_write_str_b, y
		sta	ppu_displist_payload+10, x

		inx
		iny
		dec	zp_byte_00
		bpl	:-
	; }

	rts
