.include	"tunables.i"

.include	"mem.i"

	.export eng_camera_veloc_x
eng_camera_veloc_x:
	lda	#0
	ldy	eng_scroll_lock_x
	bne	:+ ; if (!eng_scroll_lock_x) {
		lda	pos_x_lo_player
		sec
		sbc	#ENG_PADDING_CAMERA_X
		sec
		sbc	pos_x_lo_screen_left
	: ; }

	sta	eng_camera_move_x
	rts
