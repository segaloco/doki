.include	"system/cpu.i"
.include	"system/ppu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"charmap.i"
.include	"modes.i"
.include	"misc.i"
.include	"tunables.i"
.include	"macros.i"

.include	"unk.i"

	.export	main
main:
	lda	#ENG_STARTING_LIVES
	sta	player_lives

main_world_start:
	ldx	#<((save_dat_end-save_dat)-1)
	: ; for (byte of save_dat) {
		lda	game_save_dat, x
		sec
		rol	a
		ora	game_save_dat, x
		sta	game_save_state, x

		dex
		bpl	:-
	; }

	jsr	book_page_init

	lda	byte_636
	cmp	#BYTE_636_VAL_A5
	beq	:+ ; if (byte_636 != 0xa5) {
		jsr	sub_60B4
		lda	#BYTE_636_VAL_A5
		sta	byte_636
	: ; }

	jsr	book_page_pause
	lda	#(ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1)
	sta	PPU_CTLR0

	ldy	#<((save_dat_end-save_dat)-1)
	sty	zp_work+$02
	: ; for (i = (ENG_CHARACTERS-1); i > 0; i--) {
		lda	PPU_SR
		lda	book_charsel_dest_h, y
		sta	PPU_VRAM_AR
		lda	book_charsel_dest_l, y
		sta	PPU_VRAM_AR

		ldx	#0
		: ; for (x = 0; x < 7; x++) {
			lda	game_save_dat, y
			sta	zp_byte_04
	
			ldy	#$f8
			lda	book_save_bits, x
			bit	zp_byte_04
			beq	:+ ; if (book_savE_bits[x] & game_save_dat[i]) {
				ldy	#$8e
			: ; }
			sty	PPU_VRAM_IO
			iny
			sty	PPU_VRAM_IO

			ldy	zp_work+$02
			inx
			cpx	#7
			bcs	:+
				lda	#$f3
				sta	PPU_VRAM_IO
				jmp	:--
		: ; }

		dec	zp_work+$02
		ldy	zp_work+$02
		bpl	:----
	; }

	lda	#%00000010
	sta	apu_music_base_req
	lda	obj_id_player
	sta	obj_id_player_b
	lda	course_no
	sta	course_no_b

	ldy	#<(eng_charsel_oam_end-eng_charsel_oam)-1
	: ; for (entry of eng_charsel_oam) {
		lda	eng_charsel_oam, y
		sta	oam_buffer+(OBJ_SIZE*4), y
		dey
		bpl	:-
	; }

	jsr	main_nmi_enable
	sta	ppu_ctlr0_b

	main_lselect_loop0: ; while () {
		ldx	#6
		: ; for (x = 6; !book_save_check(x); x--) {
			jsr	book_save_check
			bne	:+

			dex
			jmp	:-
		: ; }
		stx	zp_byte_08

		main_lselect_loop1: ; while () {
			movw_m	stack_var5E, oam_buffer+(OBJ_SIZE*12)+OBJ_CHR_NO
			ldy	obj_id_player
			lda	eng_charsel_row_pos_v, y
			sta	oam_buffer+(OBJ_SIZE*12)+OBJ_POS_V
			lda	zp_byte_08
			asl	a
			asl	a
			asl	a
			sta	zp_byte_09
			asl	a
			adc	zp_byte_09
			adc	#ENG_CHARSEL_LEVELDIST_H
			sta	oam_buffer+(OBJ_SIZE*12)+OBJ_POS_H
			jsr	nmi_disp_enable

			lda	joypad_p1
			and	#(joypad_button::up|joypad_button::down)
			beq	:+ ; if (joypad_p1_up || joypad_p1_down) {
				inc	obj_id_player
				lda	#6
				sta	byte_401

				lda	joypad_p1
				and	#joypad_button::up
				bne	:++
				beq	:+++
			: ; } else {
				lda	joypad_p1h
				and	#(joypad_button::up|joypad_button::down)
				beq	:+++
				dec	byte_401
				bne	:+++ ; if ((joypad_p1h_up|joypad_p1h_down) && !(--byte_401)) {
					inc	obj_id_player
					and	#$08
					beq	:++
				; }
			: ; } if (joypad_p1_up || (player.obj_id & 0x08)) {
				dec	obj_id_player
				dec	obj_id_player
			: ; } if (joypad_p1_up || joypad_p1_down || (player.obj_id & 0x08) || byte_401) {
				lda	#4
				sta	byte_602
				lda	obj_id_player
				and	#$03
				sta	obj_id_player
				lda	byte_401
				clc
				adc	#8
				sta	byte_401
				lda	#$ff
				sta	byte_402
			: ; }

			inc	byte_402
			bpl	:+ ; if (byte_402 & 0x80) {
				lsr	byte_402
			: ; }

			ldy	#$a
			lda	#' '
			: ; for (y = 0xa; y >= 0; y--) {
				sta	ppu_displist_payload, y
				sta	ppu_displist_payload+14, y
				dey
				bpl	:-
			; }

			iny
			sty	ppu_displist_payload+25
			lda	#$8b
			sta	ppu_displist_count
			sta	ppu_displist_count+14
			lda	#>(PPU_VRAM_BG1+$164)
			sta	ppu_displist_addr_dbyt
			sta	ppu_displist_addr_dbyt+14
			ldy	#<(PPU_VRAM_BG1+$164)
			sty	ppu_displist_addr_dbyt+1
			iny
			sty	ppu_displist_addr_dbyt+14+1

			lda	byte_402
			and	#ENG_POS_MASK
			cmp	#$09
			bcs	:+ ; if ((byte_402 & 0xF) > 9) {
				lda	obj_id_player
				asl	a
				adc	obj_id_player
				tay
				lda	#$fc
				sta	ppu_displist_payload, y
				sta	ppu_displist_payload+1, y
				sta	ppu_displist_payload+14, y
				sta	ppu_displist_payload+15, y
			: ; }

			ldx	zp_byte_08
			stx	zp_byte_09
			lda	joypad_p1
			and	#(joypad_button::left|joypad_button::right)
			beq	:+ ; if (joypad_p1_left|joypad_p1_right) {
				inx
				lda	#6
				sta	byte_403
				lda	joypad_p1
				lsr	a
				bcc	:+++
				bcs	:++++
			: ; } else {
				lda	joypad_p1h
				and	#(joypad_button::left|joypad_button::right)
				beq	main_lselect_finish ; if (joypad_p1h_left|joypad_p1h_right) {
					dec	byte_403
					bne	main_lselect_finish
				; }

			:
				inx
				lsr	a
				bcs	:++
			; }

			: ; if () {
				dex
				dex
			: ; }

			txa
			bpl	:+ ; if (x & 0x80) {
				ldx	#ENG_CHAPTER_COUNT-1
			: ; }

			cpx	#ENG_CHAPTER_COUNT
			bcc	:+ ; if (x >= ENG_CHAPTER_COUNT) {
				ldx	#ENG_CHAPTER_MIN
			: ; }

			jsr	book_save_check
			bne	:+ ; if (!book_save_check()) {
				lda	joypad_p1h
				jmp	:-----
			: ; }

			stx	zp_byte_08
			cpx	zp_byte_09
			beq	:+ ; if () {
				lda	#4
				sta	byte_602
			: ; }

			lda	byte_403
			clc
			adc	#8
			sta	byte_403

		main_lselect_finish:
			lda	joypad_p1
			and	#joypad_button::start
			bne	:++ ; if (!joypad_p1_start) {
				lda	byte_402
				beq	:+ ; if (byte_402) {
					jmp	main_lselect_loop1
				: ; } else {
					jmp	main_lselect_loop0
				; }
			: ; } else break;
		; }
	; }

	ldx	zp_byte_08
	stx	course_no
	ldy	tbl_chapterstarts, x
	sty	eng_level
	sty	eng_levels_old
	jsr	book_page_text

	: ; for (ppu_scc_h; ppu_scc_h != 0xE0; ppu_scc_h += 4) {
		jsr     nmi_buffer_transfer
		ldy     #(4*8)
		: ; for (y = 0x20; y >= 0; y -= 4) {
			lda     oam_buffer+(OBJ_SIZE*4)+OBJ_POS_V, y
			cmp     #OAM_INIT_SCANLINE
			beq     :+
			lda     oam_buffer+(OBJ_SIZE*4)+OBJ_POS_H, y
			sec
			sbc     #(ENG_CHARSEL_WIPE_RATE)-1
			sta     oam_buffer+(OBJ_SIZE*4)+OBJ_POS_H, y
			bcs     :+ ; if () {
				lda     #OAM_INIT_SCANLINE
				sta     oam_buffer+(OBJ_SIZE*4)+OBJ_POS_V, y
			: ; }

			dey
			dey
			dey
			dey
			bpl     :--
		; }

		lda     ppu_scc_h
		clc
		adc     #4
		sta     ppu_scc_h
		jsr     sub_7D55
		lda     ppu_scc_h
		cmp     #PPU_SCC_H_E0
		bcc     :---
	; }

game_init:
	jsr     eng_player_transition_reload_hearts_max
        lda     #%10000000
        sta     apu_music_base_req
        asl     a
        sta     eng_level_area
        sta     eng_level_area_old
        sta     eng_level_page
        sta     eng_level_page_old
        sta     eng_transition_type
        sta     eng_transition_old
        sta     proc_id_player
        sta     proc_id_player_b
        sta     eng_inner_space
        sta     eng_jar_type
        sta     eng_timer_stopwatch

        lda     #$5C
        sta     zp_work+$02
	: ; for (zp_work+$02 = 0x5C; zp_work+$02 >= 0; zp_work+$02--) {
		jsr     nmi_buffer_transfer
		dec     zp_work+$02
		bpl     :-
	; }

        jsr     set_default_nmi
        lda     obj_id_player
        cmp     obj_id_player_b
        bne     :+
        lda     course_no
        cmp     course_no_b
        beq     :++
	: ; if (player.obj_id_old != player.obj_id || course_no_changed) {
		jsr     disk_load_files_sideb
		jsr     sub_6169
	: ; }

	lda     #$FF
        sta     apu_song_index
; ----------------------------
game_proc:
	jsr     nmi_disp_disable
        lda     #(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1)
        ora     ppu_scc_h_b
        ldy     eng_level_h
        bne     :+ ; if (eng_level_h) {
		and     #<~(ppu_ctlr0::bg_odd)
		ora     ppu_scc_v_b
	: ; }
	sta     PPU_CTLR0
        sta     ppu_ctlr0_b

        lda     #fds_nmimode::game_vec_1
        sta     FDS_STACK_NMIMODE

        jsr     eng_map_load_area
        jsr     eng_map_palette_set_outside

        jsr     oam_init
        jsr     nmi_buffer_transfer
        jsr     set_default_nmi
        lda     #(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1)
        sta     ppu_ctlr0_b

        lda     eng_level_h
	beq     :++++++ ; if (eng_level_h) {
		: ; while (!areainit_done) {
			jsr     nmi_buffer_transfer
			jsr     eng_areainit_h
			jsr     eng_map_music_change
			lda     areainit_done
			beq     :-
		; }

		lda     #0
		sta     areainit_done
		jsr     nmi_disp_enable

	game_scroll_h:
		: ; while (!eng_transition_area) {
			jsr     nmi_buffer_transfer
			lda     ppu_scroll_req
			and     #scroll_req::bit2
			bne     :+
			lda     joypad_p1
			and     #joypad_button::start
			beq     :+ ; if (!(ppu_scroll_req & bit2) && joypad_p1_start) {
				jmp     game_pause
			: ; }

			lda     eng_inner_space
			beq     :+ ; if (eng_inner_space) {
				jmp     game_ispace_init
			: ; }

			jsr     oam_init
			jsr     eng_framecycle_h
			ldy     proc_id_system
			beq     :+ ; if (proc_id_system != 0) {
				jmp     game_system_change
			: ; } else {
				lda     eng_transition_area
				beq     :----
			; }
		; }

		; if (!pause && !inner_space && proc_id_system == 0 && eng_transition_area) {
			jsr     eng_map_set_stage
			jsr     eng_player_transition_load
			lda     #0
			sta     eng_transition_area
			jmp     game_proc
		; }
	: ; } else {
		: ; while (!areainit_done) {
			jsr     nmi_buffer_transfer
			jsr     eng_areainit_v
			jsr     eng_map_music_change
			lda     areainit_done
			beq     :-
		; }

		lda     #0
		sta     areainit_done
		jsr     nmi_disp_enable

	game_scroll_v:
		: ; while (!eng_transition_area) {
			jsr     nmi_buffer_transfer
			lda     ppu_scroll_req
			and     #scroll_req::bit2
			bne     :+ ; if (!(ppu_scroll_req & bit2)) {
				lda     joypad_p1
				and     #joypad_button::start
				bne     game_pause
			: ; }

			jsr     oam_init
			jsr     eng_framecycle_v

			ldy     proc_id_system
			beq     :+ ; if (proc_id_system != game) {
				jmp     game_system_change
			: ; } else {
				lda     eng_transition_area
				beq     :---
			; }
		; }

		; if (!pause && !inner_space && proc_id_system == 0 && eng_transition_area) {
			jsr     eng_map_set_stage
			jsr     eng_player_transition_load
			lda     #0
			sta     eng_transition_area
			jmp     game_proc
		; }
	; }
; ------------------------------------------------------------
game_pause:
	jsr     sub_7CE0
        lda     #fds_nmimode::game_vec_0|FDS_NMIMODE_MUTESQ_BIT
        sta     FDS_STACK_NMIMODE

	: ; while (!(joypad_p1 & 0x10)) {
		lda     #$0E
		sta     zp_byte_06
		: ; for (i = 0xE; I >= 0; i--) {
			jsr     nmi_disp_enable
			jsr     sub_7D55
			lda     proc_id_player
			cmp     #player_state::death
			beq     :+
			lda     joypad_p2h
			cmp     #(joypad_button::face_a|joypad_button::face_b|joypad_button::up)
			bne     :+ ; if (joypad_p2h_face_a && joypad_p2h_face_b && joypad_p2h_up) {
				jsr     eng_player_kill
			: ; }

			lda     joypad_p1
			and     #joypad_button::start
			bne     :+

			dec     zp_byte_06
			bpl     :--
		; }

		inc     zp_byte_07
		lda     zp_byte_07
		and     #$01
		clc
		adc     #nmi_buffer_modes::mode_11
		sta     nmi_buffer_ptr
		jmp     :---
	: ; }

	jsr     nmi_disp_disable
        jsr     eng_map_palette_set_outside
        jsr     nmi_buffer_transfer
        jsr     set_default_nmi
        jsr     oam_init
        jsr     ppu_scroll_restore

        lda     eng_level_h
        bne     :++ ; if (!eng_level_h) {
		lda     #(fds_ctlr::irq_no|fds_ctlr::drive_ready|fds_ctlr::mirror_h|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset|fds_ctlr::motor_on)
		sta     FDS_CTLR
		jsr     eng_bg_unpause_v

		: ; while (!byte_537) {
			jsr     nmi_buffer_transfer
			jsr     eng_bg_redraw_v
			lda     byte_537
			beq     :-
		; }

		jsr     nmi_disp_enable
		jmp     game_scroll_v
	: ; } else {
		lda     #(fds_ctlr::irq_no|fds_ctlr::drive_ready|fds_ctlr::mirror_v|fds_ctlr::io_mode_r|fds_ctlr::transfer_reset|fds_ctlr::motor_on)
		sta     FDS_CTLR
		jsr     sub_709F

		: ; while (!byte_537) {
			jsr     nmi_buffer_transfer
			jsr     sub_70C4
			lda     byte_537
			beq     :-
		; }

		jsr     nmi_disp_enable
		jmp     game_scroll_h
	; }
; ------------------------------------------------------------
game_ispace_init:
	jsr     nt_init_all
        lda     #0
        sta     word_621+1

        lda     eng_inner_space
        cmp     #2
        beq     :+ ; if (eng_inner_space != 2) {
		jsr     eng_map_ispace_clear
		lda     #4
		sta     apu_music_base_req
		lda     #1
		sta     apu_song_index
		jmp     :++
	: ; } else {
		jsr     eng_map_ispace_make
		lda     #$20
		sta     apu_music_base_req
		lda     #4
		sta     apu_song_index
	: ; }

	jsr     eng_bound_ispace
        jsr     main_nmi_enable

	: ; while (!byte_537) {
		jsr     nmi_buffer_transfer
		jsr     sub_70C4
		lda     byte_537
		beq     :-
	; }

        lda     eng_inner_space
        cmp     #2
        beq     :+ ; if (eng_inner_space != 2) {
		jsr     eng_map_palette_set_outside
	: ; }

	jsr     nmi_disp_enable

	: ; while (eng_inner_space) {
		jsr     nmi_buffer_transfer
		jsr     oam_init
		jsr     sub_82FF

		ldy     proc_id_system
		beq     :+ ; if (proc_id_system != game) {
			jmp     game_system_change
		: ; } else {
			lda     eng_inner_space
			bne     :--
		; }
	; }

        lda     word_621+1
        beq     :+
		inc     word_621
	:

	lda     eng_level_area_b
        sta     eng_level_area
        jsr     eng_map_palette_set_outside
        jsr     nmi_disp_disable
        jsr     oam_init
        ldy     byte_544
        sty     apu_song_index

        lda     timer_player_star
        bne     :+ ; if (timer_player_star == 0) {
		lda     eng_map_music_change_tbl, y
		sta     apu_music_base_req
	: ; }

	jsr     eng_bound_normal

	: ; while (!byte_537) {
		jsr     nmi_buffer_transfer
		jsr     sub_70C4
		lda     byte_537
		beq     :-
	; }

        jsr     nmi_disp_enable
        jmp     game_scroll_h
; ------------------------------------------------------------
game_system_change:
	jsr     eng_map_reset
        ldy     proc_id_system
        lda     #system_modes::game
        sta	proc_id_system
        sta     timer_player_star
        sta     byte_62C
        sta     byte_62A
        sta     eng_timer_stopwatch

        dey
        bne     :+ ; if (proc_id_system == 1) {
		jsr     sub_7CD7
		jsr     eng_player_transition_restore
		lda     #$FF
		sta     apu_song_index
		jmp     game_proc
	: ; }

	movw_m	pos_y_hi_actor, hp_player
        sta     eng_key_used
        sta     byte_620
        sta     eng_actor_lifeheart_pull_tbl
        sta     eng_actor_lifeheart_pull_tbl+1
        sta     word_621
        sta     eng_actor_heart_counter

        dey
        beq     :+ ; if (proc_id_system != 1) {
		jmp     game_level_transition
	: ; }

	lda     #$10
        sta     apu_music_queue_2
        jsr     nmi_disp_disable
        jsr     nt_init_all
        jsr     eng_palload_mono
        jsr     ppu_scroll_zero
        jsr     main_nmi_enable
        jsr     nmi_disp_enable
        lda     #nmi_buffer_modes::mode_04
        sta     nmi_buffer_ptr

        lda     #$C0
        sta     zp_byte_06
	: ; for (zp_byte_06 = 0xC0; zp_byte_06 > 0; zp_byte_06--) {
		jsr     nmi_buffer_transfer
		dec     zp_byte_06
		bne     :-
	; }

        lda     #nmi_buffer_modes::mode_05
        sta     nmi_buffer_ptr
        lda     #0
        sta     zp_byte_08
	: ; while (!joypad_p1_start) {
		jsr     nmi_buffer_transfer
		lda     joypad_p1
		and     #joypad_button::select
		beq     :+ ; if (joypad_p1_select) {
			lda     zp_byte_08
			eor     #$01
			sta     zp_byte_08
			asl     a
			asl     a
			tay
			lda     #' '
			sta     tbl_7CB0_start
			sta     tbl_7CB4_start
			lda     #$F6
			sta     tbl_7CB0+WORD_SIZE+1, y
			lda     #nmi_buffer_modes::mode_18
			sta     nmi_buffer_ptr
		: ; }

		lda     joypad_p1
		and     #joypad_button::start
		beq     :--
	; }

        lda     zp_byte_08
        bne     :+ ; if (!zp_byte_08) {
		sta     bonus_coins
		jmp     main
	: ; }

	lda     #0
        sta     pos_x_lo_actor+0
        jsr     disk_load_files_sidea
        jmp     disk_loader_file_write_save

        lda     #0
        sta     pos_x_lo_actor+0
        jsr     disk_load_files_sidea_nocheck
        jmp     disk_loader_file_write_save
; ------------------------------------------------------------
game_level_transition:
	dey
        beq     :+ ; if ((--y)) {
		lda     course_no
		sta     course_no_b
		ldx     obj_id_player
		ldy     byte_C071
		sty     course_no
		lda     tbl_chapterstarts, y
		sta     eng_level
		sta     eng_levels_old
		lda     book_save_bits, y
		ora     game_save_state, x
		sta     game_save_state, x
		iny
		tya
		ora     #ENG_FONT_TILE_BASE
		sta     nmi_buffer_warp_chapter_end-1

		jsr     nmi_disp_disable
		jsr     ppu_scroll_zero
		sta     tbl_chapterstarts
		jsr     nt_init_all
		jsr     eng_palload_mono
		jsr     main_nmi_enable
		lda     #nmi_buffer_modes::mode_17
		sta     nmi_buffer_ptr
		lda     #$40
		sta     apu_music_queue_2
		jsr     main_delay_160
		jsr     sub_7CE3
		jsr     nmi_disp_enable

		jsr     sub_7D55
		jmp     game_init
	: ; }

	lda     #$80
        sta     apu_music_base_req

        lda     eng_level
        cmp     #ENG_LEVEL_END
        bne     :+ ; if (eng_level == ENG_LEVEL_END) {
		jmp     main_ending
	: ; }

	jsr     nmi_disp_disable
        jsr     nt_init_all

        jsr     main_bonus_docoin
        jsr     main_bonus_palload
        lda     #$40
        sta     apu_music_queue_2

        lda     bonus_coins
        bne     :+ ; if (!bonus_coins) {
		jmp     main_bonus_lose
	: ; }

	lda     #4
        sta     pos_x_lo_actor+3
        sta     pos_x_lo_actor+4
        sta     pos_x_lo_actor+5

        lda     #$0A
        sta     zp_byte_06
	: ; for (zp_byte_06 = 0x0A; zp_byte_06 >= 0; zp_byte_06--) {
		lda     zp_byte_06
		and     #$01
		tay
		lda     tbl_7CCF, y
		sta     nmi_buffer_ptr

		lda     #$0A
		sta     zp_byte_07
		: ; while (zp_byte_07 = 0x0A; zp_byte_07 > 0; zp_byte_07--) {
			jsr     nmi_disp_enable
			dec     zp_byte_07
			bne     :-
		; }

		dec     zp_byte_06
		bpl     :--
	; }

game_level_finish:
	lda     bonus_coins
        bne     main_bonus_round ; if (!bonus_coins) {
	main_level_next:
		ldy     course_no

		lda     tbl_chapterstarts+1, y
		sec
		sbc     #(2)-1
		cmp     eng_level
		bne     :+ ; if (--tbl_chapterstarts[course_no] == eng_level) {
			ldx     obj_id_player
			lda     game_save_dat, x
			ora     book_save_bits, y
			sta     game_save_dat, x
			jsr     set_default_nmi
			lda     #$FF
			sta     apu_song_index
			jmp     main_world_start
		: ; }

		jsr     eng_map_set_stage

		lda     eng_level
		ldy     #0
		: ; for (y = 0; eng_level > tbl_chapterstarts[y]; ++y) {
			iny
			cmp     tbl_chapterstarts, y
			bcs     :-
		; }

		dey
		sty     course_no
		ldy     course_no
		lda     eng_level
		sec
		sbc     tbl_chapterstarts, y
		sta     course_sub
		lda     eng_level
		sta     eng_levels_old
		lda     eng_level_area
		sta     eng_level_area_old
		lda     eng_level_page
		sta     eng_level_page_old
		ldy     #player_state::normal
		sty     proc_id_player_b
		sty     eng_transition_type
		sty     eng_transition_old
		dey
		sty     apu_song_index
		jsr     sub_7CE0

		lda     #$A0
		sta     zp_byte_07
		: ; for (zp_byte_07 = 0xA0; zp_byte_07 > 0; zp_byte_07--) {
			jsr     nmi_disp_enable
			jsr     sub_7D55
			dec     zp_byte_07
			bne     :-
		; }
		
		jsr     set_default_nmi
		jmp     game_proc
	; }
; ------------------------------------------------------------
;
; ------------------------------------------------------------
main_bonus_round:
	dec     bonus_coins
        jsr     main_bonus_counters

        lda     #1
        sta     pos_x_lo_actor+0
        sta     pos_x_lo_actor+1
        sta     pos_x_lo_actor+2
        lsr     a
        sta     pos_x_lo_actor+6
        sta     pos_x_lo_actor+7
        sta     pos_x_lo_actor+8
	: ; while (pos_x_lo_actor | pos_x_lo_actor+1 | pos_x_lo_actor+2) {
		jsr     nmi_buffer_transfer
		lda     #2
		sta     apu_sfx_queue2
		jsr     main_bonus_press
		jsr     main_bonus_postpress
		jsr     main_bonus_doresult
		jsr     main_bonus_flash
		lda     tbl_7CD1, y
		sta     nmi_buffer_ptr
		inc     zp_byte_06
		lda     pos_x_lo_actor+0
		ora     pos_x_lo_actor+1
		ora     pos_x_lo_actor+2
		bne     :-
	; }

        lda     #nmi_buffer_modes::mode_14
        sta     nmi_buffer_ptr
        jsr     nmi_buffer_transfer

        ldy     #0
        ldx     pos_x_lo_actor+6
        lda     tbl_63B, x
        bne     :+ ; if (pos_x_lo_actor+6 == tbl_63B[x]) {
		iny
	: ; }

	ldx     pos_x_lo_actor+7
        cmp     tbl_643, x
        bne     :++ ; if (pos_x_lo_actor+7 == tbl_643[x]) {
		cmp     #0
		bne     :+ ; if (!pos_x_lo_actor+7) {
			iny
		: ; }

		ldx     pos_x_lo_actor+8
		cmp     tbl_64B, x
		bne     :+ ; if (pos_x_lo_actor+8 == tbl_64B[x]) {
			iny
			cmp     #0
			bne     :+ ; if (!(y++)) {
				iny
				iny
			; }
		; }
	: ; }

	tya
        clc
        adc     player_lives
        bcc     :+ ; if (++player_lives > ENG_LIVES_MAX) {
		lda     #ENG_LIVES_MAX
	: ; }
	sta     player_lives

        lda     #8
        sta     apu_music_queue_2
        tya
        beq     :++ ; if (y) {
		ora     #ENG_FONT_TILE_BASE
		sta     byte_6C7
		lsr     apu_music_queue_2

		lda     #$A0
		sta     zp_byte_06
		jsr     main_bonus_counters
		: ; for (zp_byte_06 = 0xA0; zp_byte_06 > 0; zp_byte_06--) {
			jsr     nmi_buffer_transfer
			jsr     main_bonus_flash
			lda     tbl_7CD5, y
			sta     nmi_buffer_ptr
			dec     zp_byte_06
			bne     :-
		; }
		beq     :++
	: ; } else {
		jsr     nmi_buffer_transfer
		jsr     main_bonus_counters
		jsr     main_delay_160
	: ; }

	lda     #nmi_buffer_modes::mode_15
        sta     nmi_buffer_ptr
        jsr     nmi_buffer_transfer
        jmp     game_level_finish
; -------------------------------------------------------------------------------
main_bonus_flash:
	lda     zp_byte_06
        lsr     a
        lsr     a
        lsr     a
        lsr     a
        and     #$01
        tay
        rts
; -------------------------------------------------------------------------------
main_bonus_lose:
	jsr     main_delay_80

        lda     #8
        sta     apu_music_queue_2
        sta     zp_byte_06
	: ; for (zp_byte_06 = 8; zp_byte_06 >= 0; zp_byte_06--) {
		lda     zp_byte_06
		and     #$01
		tay
		lda     tbl_7CD3, y
		sta     nmi_buffer_ptr

		lda     #$0A
		sta     zp_byte_07
		: ; for (zp_byte_07 = 0x0A; zp_byte_07 > 0; zp_byte_07--) {
			jsr     nmi_disp_enable
			dec     zp_byte_07
			bne     :-
		; }

		dec     zp_byte_06
		bpl     :--
	; }

        jmp     main_level_next
; -----------------------------------------------------------
main_delay_80:
	lda     #80
        bne     :+

main_delay_160:
	lda     #160

:
	sta     zp_byte_07
	: ; for (i; i > 0; i--)
		jsr     nmi_disp_enable
		dec     zp_byte_07
		bne     :-

        rts
; -----------------------------------------------------------
main_ending:
	lda     #fds_snd_vol_env::off
        sta     FDS_SND_VOL
        asl     a
        sta     byte_607

        ldy     course_no
        ldx     obj_id_player
        lda     game_save_dat, x
        ora     book_save_bits, y
        sta     game_save_dat, x
        ldx     #<((save_dat_end-save_dat)-1)
	: ; for (x = 3; x >= 0; x--) {
		lda     game_save_dat, x
		and     book_save_bits, y
		beq     :+
		dex
		bpl     :-
		lda     #2
		sta     pos_x_lo_actor+0
		jsr     disk_load_files_sidea
		jmp     sub_C0A8
	: ; }

	lda     #ENG_CHAPTER_COUNT
        sta     course_no
        jsr     disk_load_files_sideb
        jmp     sub_C367
; -----------------------------------------------------------
;
; -----------------------------------------------------------
	.export	nmi_buffer_game_over
nmi_buffer_game_over:
	.dbyt	$21CB
	.byte	(:++)-(:+)
	: .byte	"GAME  OVER"
	:

	.byte	NMI_LIST_END
; --------------
	.export	nmi_buffer_continue_save
nmi_buffer_continue_save:
	.dbyt	$216A
	.byte	(:++)-(:+)
	: .byte	$F6
	.byte	" CONTINUE"
	:

	.dbyt	$21AC
	.byte	(:++)-(:+)
	: .byte	"SAVE"
	:

	.dbyt	$21CB
	.byte	(:++)-(:+)
	: .byte	"          "
	:

	.byte	NMI_LIST_END
; --------------
	.export	nmi_buffer_warp
nmi_buffer_warp:
	.dbyt	$218E
	.byte	(:++)-(:+)
	: .byte	"WARP"
	:

	.dbyt	$220C
	.byte	(:++)-(:+)
	: .byte	"CHAPTER 1"
	:
nmi_buffer_warp_chapter_end:

	.byte	NMI_LIST_END
; ------------------------------
	.export	tbl_7CB0
tbl_7CB0:
	.dbyt	PPU_VRAM_BG1+$16A
	.byte	tbl_7CB0_end-tbl_7CB0_start
tbl_7CB0_start:
	.byte	" "
tbl_7CB0_end:

tbl_7CB4:
	.dbyt	PPU_VRAM_BG1+$1AA
	.byte	tbl_7CB4_end-tbl_7CB4_start
tbl_7CB4_start:
	.byte	" "
tbl_7CB4_end:

	.byte	NMI_LIST_END
; -----------------------------
	.export	nmi_buffer_text_side_a
nmi_buffer_text_side_a:
	.dbyt	PPU_VRAM_BG1+$1E7
	.byte	(:++)-(:+)
	: .byte	"PLEASE SET SIDE A"
	:

	.byte	NMI_LIST_END
; -----------------------------------------------------------
tbl_7CCF:
	.byte	nmi_buffer_modes::mode_07, nmi_buffer_modes::mode_13

tbl_7CD1:
	.byte	nmi_buffer_modes::mode_09, nmi_buffer_modes::mode_14

tbl_7CD3:
	.byte	nmi_buffer_modes::mode_08, nmi_buffer_modes::mode_13

tbl_7CD5:
	.byte	nmi_buffer_modes::mode_10, nmi_buffer_modes::mode_15
; -----------------------------------------------------------
sub_7CD7:
	jsr	sub_7CE0
	jsr	sub_7D55
	jmp	main_delay_160
; -----------------------------------------------------------
sub_7CE0: 
	jsr	ppu_scroll_backup

sub_7CE3:
	jsr	book_page_init
	jsr	book_page_pause
	ldx	course_no
	ldy	eng_level
	jsr	book_page_text
	lda	#PPU_SCC_H_E0
	sta	ppu_scc_h
; -----------------------------------------------------------
	.export main_nmi_enable
main_nmi_enable:
	lda	#(ppu_ctlr0::int|ppu_ctlr0::sprite_16|ppu_ctlr0::bg_seg1|ppu_ctlr0::obj_seg0|ppu_ctlr0::inc_1|ppu_ctlr0::bg1|ppu_ctlr0::bg_even)
	sta	PPU_CTLR0
	rts
; -----------------------------------------------------------
main_bonus_palload:
	jsr	ppu_scroll_zero
	lda	PPU_SR
	ldy	#<(PPU_VRAM_COLOR)
	lda	#>(PPU_VRAM_COLOR)
	sta	PPU_VRAM_AR
	sty	PPU_VRAM_AR
	:
		lda	pal_5B8, y
		sta	PPU_VRAM_IO
		iny
		cpy	#PAL_5B8_SIZE0
		bcc	:-

	lda	PPU_SR
	lda	#>(PPU_VRAM_COLOR+$14)
	sta	PPU_VRAM_AR
	lda	#<(PPU_VRAM_COLOR+$14)
	sta	PPU_VRAM_AR
	:
		lda	pal_5B8, y
		sta	PPU_VRAM_IO
		iny
		cpy	#PAL_5B8_SIZE1
		bcc	:-
; -----------------------------------------------------------
main_bonus_counters:
	ldy	player_lives
	dey
	tya
	jsr	main_counter_tiles
	sty	byte_5A7
	sta	byte_5A8
	lda	bonus_coins
	clc
	adc	#ENG_FONT_TILE_BASE
	sta	byte_588
	lda	#nmi_buffer_modes::mode_01
	sta	nmi_buffer_ptr
	lda	#fds_nmimode::game_vec_0
	sta	FDS_STACK_NMIMODE
	jsr	main_nmi_enable
	jmp	nmi_buffer_transfer
; -----------------------------------------------------------
sub_7D55:
	lda	obj_id_player
	asl	a
	asl	a
	asl	a
	tay
	ldx	#0
	: ; for (x = 0; x < 8; x++) {
		lda	eng_charsel_oam, y
		sta	oam_buffer+(OBJ_SIZE*14), x
		iny
		inx
		cpx	#8
		bcc	:-
	; }

	ldy	#OBJ_SIZE
	ldx	#(tbl_7D8E_end-tbl_7D8E)-1
	: ; for (x = 1, y = 4; x >= 0; x--, y -= 4) {
		lda	#OAM_INIT_SCANLINE
		sta	oam_buffer+(OBJ_SIZE*14)+OBJ_POS_V, y
		lda	tbl_7D8E, x
		sec
		sbc	ppu_scc_h
		bcs	:+
		cmp	#$F9
		bcs	:+ ; if () {
			sta	oam_buffer+(OBJ_SIZE*14)+OBJ_POS_H, y
			lda	#83
			sta	oam_buffer+(OBJ_SIZE*14)+OBJ_POS_V, y
		: ; }

		dey
		dey
		dey
		dey
		dex
		bpl	:--
	; }

	rts

tbl_7D8E:
	.byte   $44
	.byte   $4C
tbl_7D8E_end:
; -----------------------------------------------------------
	.export main_counter_tiles
main_counter_tiles:
	ldy	#ENG_FONT_TILE_BASE
	:
		cmp	#10
		bcc	:+
		sbc	#10
		iny
		jmp	:-
	:

	ora	#ENG_FONT_TILE_BASE
	cpy	#ENG_FONT_TILE_BASE
	bne	:+
		ldy	#' '
	:

	rts
; -----------------------------------------------------------
	.export nmi_disp_disable, nmi_disp_enable, nmi_buffer_transfer
nmi_disp_disable:
	lda	#ppu_ctlr1::objblk_on|ppu_ctlr1::bgblk_on|ppu_ctlr1::objlblk_on|ppu_ctlr1::bglblk_on
	beq	:+

nmi_disp_enable:
	lda	#ppu_ctlr1::objblk_off|ppu_ctlr1::bgblk_off|ppu_ctlr1::objlblk_off|ppu_ctlr1::bglblk_off

:
	sta	ppu_ctlr1_b

nmi_buffer_transfer:
	lda	nmi_buffer_ptr
	asl	a
	tax
	lda	nmi_buffer_mode_tbl, x
	sta	ppu_displist_write_addr
	lda	nmi_buffer_mode_tbl+1, x
	sta	ppu_displist_write_addr+1

	lda	#0
	sta	ppu_nmi_hold
	: ; for (ppu_nmi_hold = 0; ppu_nmi_hold >= 0; interrupt) {
		lda	ppu_nmi_hold
		bpl	:-
	; }

	rts
; -----------------------------------------------------------
main_bonus_press:
	lda	joypad_p1
	bpl	:++ ; if (joypad_p1_a) {
		ldx	#0
		: ; for (x = 0; x < 3; x++) {
			lda	pos_x_lo_actor, x
			bne	:++
			inx
			cpx	#3
			bcc	:-
		; }
	: ; } else {
		rts
	: ; }
		lda	#0
		sta	pos_x_lo_actor, x
		lda	#4
		sta	byte_602
		rts
; -----------------------------------------------------------
main_bonus_postpress:
	ldx	#2
	: ; for (x = 2; x >= 0; x--) {
		lda	pos_x_lo_actor, x
		beq	:+
		dec	pos_x_lo_actor+3, x
		bne	:+
			lda	#4
			sta	pos_x_lo_actor+3, x
			dec	pos_x_lo_actor+6, x
			bpl	:+
				lda	#7
				sta	pos_x_lo_actor+6, x
		:

		dex
		bpl	:--
	; }

	rts
; -----------------------------------------------------------
main_bonus_doresult:
	lda	#2
	sta	zp_work+$00
	: ; for (i = 2; i >= 0; i--) {
		lda	zp_work+$00
		tay
		asl	a
		asl	a
		asl	a
		tax
		adc	pos_x_lo_actor+6, y
		tay
		lda	tbl_63B, y
		tay

		lda	#<((oam_73FD_end-oam_73FD)/ENG_CHARACTERS)-1
		sta	zp_work+$01
		: ; for (j = 7; j >= 0; j--, x++, y++) {
			lda	oam_73FD, y
			sta	oam_buffer+(OBJ_SIZE*4), x

			inx
			iny
			dec	zp_work+$01
			bpl	:-
		; }

		dec	zp_work+$00
		bpl	:--
	; }

	ldx	#$17
	: ; for (x = 0x17; x >= 0; x -= 4) {
		txa
		and	#$18
		asl	a
		asl	a
		adc	oam_buffer+(OBJ_SIZE*4)+OBJ_POS_V, x
		sta	oam_buffer+(OBJ_SIZE*4)+OBJ_POS_V, x
		dex
		dex
		dex
		dex
		bpl	:-
	; }

	rts
; -----------------------------------------------------------
main_bonus_docoin:
	lda	obj_id_player
	asl	a
	asl	a
	asl	a
	tax
	ldy	#0
	: ; for (x = player.obj_id * 8, y = 0; y < 8; x++, y++) { 
		lda	tbl_653, y
		sta	oam_buffer+(OBJ_SIZE*10), y
		lda	tbl_65B, x
		sta	oam_buffer+(OBJ_SIZE*12), y

		inx
		iny
		cpy	#2*(OBJ_SIZE)
		bcc	:-
	; }

	rts
; -----------------------------------------------------------
	.export main_disk_swap
main_disk_swap:
	: ; while (!drive_empty) {
		lda	#PPU_CTLR0_INC_PAGESIZE-2
		sta	zp_byte_07
		: ; for (i = 31; i >= 0; i--) {
			jsr	nmi_buffer_transfer
			lda	FDS_DISK_SR
			lsr	a
			bcc	:--
			dec	zp_byte_07
			bpl	:-
		; }
	; }

	: ; while (drive_empty) {
		lda	#PPU_CTLR0_INC_PAGESIZE-2
		sta	zp_byte_07
		: ; for (i = 31; i >= 0; i--) {
			jsr	nmi_buffer_transfer
			lda	FDS_DISK_SR
			lsr	a
			bcs	:--
			dec	zp_byte_07
			bpl	:-
		; }
	; }

	rts
