.include	"mem.i"
.include	"math.i"
.include	"tunables.i"

	.export	sub_82FF, eng_framecycle_h, eng_framecycle_v
sub_82FF:
	jsr	eng_player_setpos
	jmp	eng_framecycle_0

eng_framecycle_h:
	jsr	eng_camera_veloc_x
	jsr	eng_hscroll_do
	jsr	eng_player_setpos

eng_framecycle_0:
	jsr	eng_map_init

eng_framecycle_com:
	lda	eng_player_in_rocket
	bne	:+ ; if (!eng_player_in_rocket) {
		jsr	eng_player_frame_anim

		lda	apu_musicplay_2
		cmp	#2
		beq	:+
		lda	player_lock
		bne	:+ ; if (apu_musicplay_2 != 2 && !player_lock) {
			jsr	player_proc
		; }
	: ; }

	jsr	eng_area_sec_routines
	jsr	eng_map_ppu_buffer_write
	jsr	lib_calc_page

	ldx	#ENG_CHARACTERS-1
	: ; for (x = (ENG_CHARACTERS-1); x >= 0; x--) {
		lda	eng_timer_player_state, x
		beq	:+ ; if (eng_timer_player_state[x]) {
			dec	eng_timer_player_state, x
		: ; }

		dex
		bpl	:--
	; }

	ldy	timer_player_star
	beq	:+
	lda	frame_count
	and	#MOD_8
	bne	:+
	dec	timer_player_star
	cpy	#ENG_INVINC_MUSIC_CUTOFF_FRAME
	bne	:+ ; if (timer_player_star != 0 && !(frame_count % 8) && --timer_player_star == cutoff_frame) {
		ldy	apu_song_index
		lda	eng_map_music_change_tbl, y
		sta	apu_music_base_req
	: ; }

	rts

eng_framecycle_v:
	jsr	eng_player_setpos
	jsr	eng_map_init
	jsr	sub_8F5A
	jsr	eng_vscroll_do
	jmp	eng_framecycle_com
