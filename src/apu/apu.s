.include	"system/cpu.i"
.include	"system/ppu.i"
.include	"system/apu.i"

.include	"system/fds.i"

.include	"mem.i"
.include	"sound.i"
.include	"tunables.i"

apu_fds_wave_notes:
	brk
	ldx	#$00
	ldy	$F300
	.byte	$03
	stx	$04, y
	.byte	$07
	.byte	$04
	.byte	$44
	.byte	$04
	sta	$04
	dex
	ora	$13
	ora	$60
	ora	$B2
	asl	$08
	asl	$64
	.byte	$07
	and	$0E08
	ora	#$0A
	ora	#$95
	asl	a
	cpy	#$0C
	ora	(zp_addr_0C), y
	cmp	#$0E
	.byte	$5A
	brk
LD12B:
	brk
LD12C:
	rti
	bmi	LD13F
	.byte	$23
	eor	($2D), y
	lsr	$20
	.byte	$34
	.byte	$3A
	bmi	LD178
	.byte	$43
	and	$3016
	.byte	$87
	.byte	$1A
	.byte	$80
LD13F:	.byte	$22
	bit	$00
	.byte	$87
	asl	$2A, x
	rol	a
	asl	$2A12
	rol	a
	asl	$00
	sta	$14
	brk
	.byte	$80
	.byte	$1C
	asl	$2220, x
	bit	$26
	.byte	$82
	plp
	brk
	.byte	$82
	asl	$00
	.byte	$80
	.byte	$04
	.byte	$04
	.byte	$04
	.byte	$80
	.byte	$04
	rol	a
	.byte	$83
	.byte	$04
	brk
	.byte	$80
	.byte	$02
	rol	a
	dey
	.byte	$04
	brk
	.byte	$82
	bpl	LD16F
LD16F:	.byte	$80
	php
	brk
	.byte	$87
	php
	asl	a
	.byte	$80
	.byte	$0C
	.byte	$87
LD178:	asl	$14, x
	.byte	$80
	.byte	$12
	brk
	.byte	$83
	asl	$8000
	.byte	$02
	.byte	$80
	brk
	brk
	rts

	bcc	LD189
	.byte	$43
LD189:	php
	ldy	#$10
	.byte	$1F
	clc
	ldy	#$01
	.byte	$43
	php
	ora	($04, x)
	.byte	$3F
	rti

	brk
	clc
; -----------------------------
apu_note_lengths:
	.byte	$03
	asl	zp_addr_0C
	clc
	php
	.byte	$12
	bit	$04
	.byte	$80
	.byte	$04
	ora	#$12
	bit	$48
	.byte	$1B
	rol	$06, x
	asl	zp_addr_0C
	.byte	$18
	.byte	$30
	.byte	$60
	.byte	$24
	.byte	$48
	.byte	$08
; ------------------------------------------------------------
; src/apu/proc.s
; ------------------------------------------------------------
	.export	apu_cycle
apu_cycle:
	lda	FDS_STACK_NMIMODE
	cmp	#fds_nmimode::game_vec_0|1
	bne	:+ ; if (FDS_STACK_NMIMODE == game_vec_0|1) {
		lda	#dmcstatus::noise|dmcstatus::tri
		bne	:++
	: ; } else {
		lda	#dmcstatus::noise|dmcstatus::tri|dmcstatus::pulse_2|dmcstatus::pulse_1
	: ; }
	sta	DMCSTATUS

	lda	#$FF
	sta	DMCFRAMECOUNT

	jsr	LD2A5
	jsr	LD46B
	jsr	LD20C

	lda	#0
	sta	apu_sfx_queue2
	sta	apu_music_queue_2
	sta	byte_602
	sta	apu_dpcm_queue
	sta	apu_music_base_req
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LD1E1:
	sty	byte_605
	lda	#$24
	ldx	#$82
	ldy	#$A8
	jsr	apu_write_pulse_1
	lda	#$22
	sta	byte_618
LD1F2:	lda	byte_618
	cmp	#$20
	bne	LD1FF
	ldx	#$DF
	ldy	#$F6
	bne	LD207
LD1FF:	cmp	#$1A
	bne	LD258
	ldx	#$C1
	ldy	#$BC
LD207:
	jsr	apu_write_base_pulse_1
	bne	LD258
; -----------------------------
LD20C:
	lda	byte_605
	ldy	apu_sfx_queue2
	lsr	apu_sfx_queue2
	bcs	LD1E1
	lsr	a
	bcs	LD1F2
	lsr	a
	bcs	LD274
	lsr	apu_sfx_queue2
	bcs	LD263
	lsr	apu_sfx_queue2
	bcs	:+
	lsr	a
	bcs	LD24C
	lsr	apu_sfx_queue2
	bcs	LD289
	lsr	a
	bcs	LD291
	lsr	apu_sfx_queue2
	bcs	LD25A
	lsr	a
	bcs	LD274 ; if () {
		rts
	: ; } else {
		sty	byte_605
		lda	#$35
		ldx	#$8D
		sta	byte_618
		ldy	#$7F
		lda	#$3E
		jsr	apu_write_pulse_1
	LD24C:
		lda	byte_618
		cmp	#$30
		bne	LD258
		lda	#$54
		sta	PULSE1_TIMER
	LD258:
		bne	LD274
	LD25A:
		sty	byte_605
		lda	#$60
		ldy	#$A5
		bne	LD26A
	LD263:
		sty	byte_605
		lda	#$05
		ldy	#$9C
	LD26A:
		ldx	#$9E
		sta	byte_618
		lda	#$46
		jsr	apu_write_pulse_1
	
	LD274:
		dec	byte_618
		bne	LD288
			ldx	#$06
			stx	DMCSTATUS
			ldx	#$0F
			stx	DMCSTATUS
			ldx	#$00
			stx	byte_605
		LD288:
	
		rts
	; }
; --------------
	LD289:
		sty	byte_605
		lda	#$02
		sta	byte_618
	LD291:

	lda	#$1A
	sta	NOISE_VOL
	lda	#$04
	sta	NOISE_PERIOD
	sta	NOISE_COUNTER
	lda	#$0E
	sta	DMCSTATUS
	bne	LD274
; -----------------------------
	.export	LD2A5
LD2A5:
	lda	byte_607
	and	#$B0
	bne	:+++++
	lda	byte_602
	bne	:++
	lda	apu_dpcm_queue
	bne	:+
	lda	byte_607
	bne	:+++++
	lda	byte_60E
	bne	:++++++ ; if () {
		rts
	: ; } else if () {
		; if () {
			; if () {
				sta	byte_60E
				ldy	#$00
				sty	byte_607
				ldy	#$08
				bne	:++
			: ; } else {
				sta	byte_607
				ldy	#$00
				sty	byte_60E
			: ; }
		
			: ; while () {
				iny
				lsr	a
				bcc	:-
			; }
		
			sty	zp_byte_C5
			jsr	apu_fds_wave_proc
			ldy	zp_byte_C5
			lda	LD12B, y
			sta	apu_wave_offset
			lda	#$01
			sta	byte_5F1
		: ; }

		lda	byte_5F1
		cmp	#$02
		bne	:+ ; if () {
			lda	#$00
			sta	FDS_SND_VOL
		; }
	: ; }

	dec	byte_5F1
	bne	:++++++ ; if () {
		ldy	apu_wave_offset
		inc	apu_wave_offset
		lda	LD12C, y
		beq	:+
		bpl	:+++
		bne	:++
		: ; if (LD12C[apu_wave_offset] == 0) {
			lda	#$80
			sta	FDS_SND_VOL
			lda	#$00
			sta	byte_607
			sta	byte_60E
			rts
		: ; } else {
			; if ((LD12C[apu_wave_offset] & 0x80)) {
				jsr	apu_note_length_load_b
				sta	byte_5F2
				ldy	apu_wave_offset
				inc	apu_wave_offset
				lda	LD12C, y
			: ; }
	
			jsr	apu_fds_write_note_wave
			tay
			bne	:+
			ldx	#$80
			stx	FDS_SND_VOL
			bne	:++
			: ; if () {
				jsr	apu_fds_mod_get
				ldy	byte_5F7
			: ; }
			sty	byte_5F3
		
			ldy	#$00
			sty	byte_5F9
			sty	byte_5FB
			lda	(zp_addr_BF), y
			sta	FDS_SND_VOL
			lda	(zp_addr_C1), y
			sta	FDS_SND_MOD_ENV
			iny
			lda	(zp_addr_BF), y
			sta	byte_5F8
			lda	(zp_addr_C1), y
			sta	byte_5FA
			sty	byte_5F9
			sty	byte_5FB
			lda	byte_5F2
			sta	byte_5F1
		; }
	: ; }

	lda	byte_5F3
	beq	:++++ ; if () {
		dec	byte_5F3
		dec	byte_5F8
		bne	:+++ ; if () {
			: ; for () {
				inc	byte_5F9
				ldy	byte_5F9
				lda	(zp_addr_BF), y
				bpl	:+
				sta	FDS_SND_VOL
				bne	:-
			: ; }
			sta	FDS_SND_VOL
			iny
			lda	(zp_addr_BF), y
			sta	byte_5F8
			sty	byte_5F9
		: ; }
	
		dec	byte_5FA
		bne	:+ ; if () {
			inc	byte_5FB
			ldy	byte_5FB
			lda	(zp_addr_C1), y
			sta	FDS_SND_MOD_ENV
			iny
			lda	(zp_addr_C1), y
			sta	FDS_SND_MOD_FREQ_LO
			iny
			lda	(zp_addr_C1), y
			sta	FDS_SND_MOD_FREQ_HI
			iny
			lda	(zp_addr_C1), y
			sta	byte_5FA
			sty	byte_5FB
		: ; }
	; }

LD3B4:
	rts
; ------------------------------------------------------------
apu_fds_mod:	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$07
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$33
	.byte	$22
	and	($10, x)
	.byte	$07
	ror	$66, x
	eor	$55, x
	ror	$67
	bvs	LD3D3
	.byte	$12
LD3D3:	.byte	$22
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	eor	$55, x
	eor	$55, x
	eor	$55, x
	eor	$55, x
; -----------------------------
apu_fds_wave_proc:
	ldy	zp_byte_C5
	beq	LD3B4
	lda	apu_fds_wave_offsets-1, y
	tay
	lda	apu_fds_wave_offsets, y
	sta	zp_addr_BD
	lda	apu_fds_wave_offsets+1, y
	sta	zp_addr_BD+1
	lda	apu_fds_wave_offsets+2, y
	sta	byte_5F7
	lda	apu_fds_wave_offsets+3, y
	sta	zp_addr_BF
	lda	apu_fds_wave_offsets+4, y
	sta	zp_addr_BF+1
	lda	apu_fds_wave_offsets+5, y
	sta	zp_addr_C1
	lda	apu_fds_wave_offsets+6, y
	sta	zp_addr_C1+1
	lda	apu_fds_wave_offsets+7, y
	sta	byte_5FC
	lda	apu_fds_wave_offsets+8, y
	sta	byte_5FD
; --------------
	.export	LD41D
LD41D:
	lda	#fds_snd_wave_vol::bus_req|fds_snd_wave_vol::vol_full
	sta	FDS_SND_WAVE_VOL
	asl	a
	sta	FDS_SND_RAM

	tay
	ldx	#$3F
	: ; for () {
		lda	(zp_addr_BD), y
		sta	FDS_SND_RAM+1, y
		iny
		cpy	#$20
		beq	:+ ; if () {
			sta	FDS_SND_RAM, x

			dex
			bne	:-
		; } else break;
	: ; }

	lda	#fds_snd_wave_vol::bus_rel|fds_snd_wave_vol::vol_full
	sta	FDS_SND_WAVE_VOL
; --------------
	.export	apu_fds_mod_get
apu_fds_mod_get:
	lda	#$80
	sta	FDS_SND_MOD_FREQ_HI
	lda	byte_5FC
	sta	FDS_SND_MOD_COUNT
	ldx	#$20
	ldy	byte_5FD
	sty	zp_byte_C6
	LD450: ; for () {
		lda	zp_byte_C6
		lsr	a
		tay
		lda	apu_fds_mod, y
		bcs	:+ ; if () {
			lsr	a
			lsr	a
			lsr	a
			lsr	a
		: ; }
	
		and	#$0F
		sta	FDS_SND_MOD_WRITE
		inc	zp_byte_C6
		dex
		bne	LD450
	; }

	rts
; ------------------------------------------------------------
LD468:	jmp	LD511

LD46B:
	lda	apu_music_queue_2
	bne	LD47E
	lda	apu_music_base_req
	bne	LD48E
	lda	apu_musicplay_2
	ora	apu_music_base_current
	bne	LD468
	rts

LD47E:	sta	apu_musicplay_2
	ldx	apu_music_base_current
	stx	byte_61A
	ldy	#$00
	sty	apu_music_base_current
	beq	LD4CC
LD48E:	ldx	#$00
	stx	apu_musicplay_2
	cmp	#$01
	bne	LD49B
	ldy	#$13
	bne	LD49D
LD49B:	ldy	#$10
LD49D:	sty	byte_61D
LD4A0:	sta	apu_music_base_current
	cmp	#$02
	bne	LD4B5
	inc	byte_61D
	ldy	byte_61D
	cpy	#$14
	bne	LD4D0
	ldy	#$11
	bne	LD49D
LD4B5:	cmp	#$01
	bne	LD4C7
	inc	byte_61D
	ldy	byte_61D
	cpy	#$1A
	bne	LD4D0
	ldy	#$14
	bne	LD49D
LD4C7:	ldy	#$08
	sty	apu_pulse_2_offset
; --------------
LD4CC:
		iny
		lsr	a
		bcc	LD4CC
; --------------
LD4D0:
	lda	apu_music_offsets-1, y
	tay
	lda	apu_music_offsets, y
	sta	apu_note_len_offset

	lda	apu_music_offsets+1, y
	sta	apu_track
	lda	apu_music_offsets+2, y
	sta	apu_track+1

	lda	apu_music_offsets+3, y
	sta	apu_triangle_offset
	lda	apu_music_offsets+4, y
	sta	apu_pulse_1_offset

	lda	apu_music_offsets+6, y
	sta	byte_5F4

	lda	apu_music_offsets+5, y
	sta	apu_noise_offset
	sta	apu_noise_loop_offset

	lda	#1
	sta	apu_pulse_2_note_len
	sta	apu_pulse_1_note_len
	sta	apu_triangle_note_len
	sta	apu_noise_note_len

	lsr	a
	sta	apu_pulse_2_offset
; --------------
LD511:
	dec	apu_pulse_2_note_len
	bne	LD570
	ldy	apu_pulse_2_offset
	inc	apu_pulse_2_offset
	lda	(apu_track), y
	beq	LD524
	bpl	LD55C
	bne	LD54E
LD524:	lda	apu_music_base_current
	and	#$7F
	bne	LD548
	lda	apu_musicplay_2
	and	#$25
	beq	LD53C
	lda	#$00
	sta	apu_musicplay_2
	lda	byte_61A
	bne	LD54B
LD53C:	lda	#$00
	sta	apu_music_base_current
	sta	apu_musicplay_2
	sta	DMCSTATUS
	rts

LD548:	jmp	LD4A0

LD54B:	jmp	LD48E

LD54E:	jsr	apu_note_length_load
	sta	apu_pulse_2_note_len_b
	ldy	apu_pulse_2_offset
	inc	apu_pulse_2_offset
	lda	(apu_track), y
LD55C:	jsr	apu_write_note_pulse_2
	beq	LD564
	jsr	LD64F
LD564:	sta	apu_pulse_2_env_ctl
	jsr	apu_write_base_pulse_2
	lda	apu_pulse_2_note_len_b
	sta	apu_pulse_2_note_len
LD570:	ldy	apu_pulse_2_env_ctl
	beq	LD578
	dec	apu_pulse_2_env_ctl
LD578:	jsr	LD661
	sta	PULSE2_VOL
	ldx	#$7F
	stx	PULSE2_SWEEP
; --------------
	ldy	apu_pulse_1_offset
	beq	LD5D0
	dec	apu_pulse_1_note_len
	bne	LD5B8
LD58D:	ldy	apu_pulse_1_offset
	inc	apu_pulse_1_offset
	lda	(apu_track), y
	bne	LD59C
	ldy	#$80
	jmp	LD58D

LD59C:	jsr	apu_note_decomp
	sta	apu_pulse_1_note_len
	ldy	byte_605
	bne	LD5D0
	txa
	and	#SND_CMP_NOTE_MASK
	jsr	apu_write_note_pulse_1
	beq	LD5B2
	jsr	LD64F
LD5B2:	sta	apu_pulse_1_env_ctl
	jsr	apu_write_base_pulse_1
LD5B8:	lda	byte_605
	bne	LD5D0
	ldy	apu_pulse_1_env_ctl
	beq	LD5C5
	dec	apu_pulse_1_env_ctl
LD5C5:	jsr	LD661
	sta	PULSE1_VOL
	lda	#$7F
	sta	PULSE1_SWEEP
LD5D0:	lda	apu_triangle_offset
	beq	LD612
	dec	apu_triangle_note_len
	bne	LD612
	ldy	apu_triangle_offset
	inc	apu_triangle_offset
	lda	(apu_track), y
	bpl	LD5F4
	jsr	apu_note_length_load
	sta	apu_triangle_note_len_b
	ldy	apu_triangle_offset
	inc	apu_triangle_offset
	lda	(apu_track), y
	beq	LD60F
LD5F4:	jsr	apu_write_note_triangle
	ldx	apu_triangle_note_len_b
	stx	apu_triangle_note_len
	lda	apu_musicplay_2
	and	#$09
	bne	LD60D
	txa
	cmp	#$1C
	bcs	LD60D
	lda	#$18
	bne	LD60F
LD60D:	lda	#$7F
LD60F:	sta	TRIANGLE_VOL
LD612:	lda	apu_noise_offset
	beq	LD64E
	dec	apu_noise_note_len
	bne	LD64E
LD61C:	ldy	apu_noise_offset
	inc	apu_noise_offset
	lda	(apu_track), y
	bne	LD62E
	lda	apu_noise_loop_offset
	sta	apu_noise_offset
	bne	LD61C
LD62E:	jsr	apu_note_decomp
	sta	apu_noise_note_len
	txa
	and	#SND_CMP_NOTE_MASK
	lsr	a
	lsr	a
	lsr	a
	lsr	a
	tay
	lda	LDFD0, y
	sta	NOISE_VOL
	lda	LDFD4, y
	sta	NOISE_PERIOD
	lda	LDFD8, y
	sta	NOISE_COUNTER
LD64E:	rts

LD64F:	lda	apu_pulse_2_note_len_b
	cmp	#$17
	bcs	LD65A
	lda	#$0F
	bne	LD65C
LD65A:	lda	#$27
LD65C:	ldx	#$82
	ldy	#$7F
	rts

LD661:	lda	apu_pulse_2_note_len_b
	cmp	#$17
	bcs	LD66D
	lda	LD679, y
	bne	LD670
LD66D:	lda	LD689, y
LD670:	ldx	byte_5F4
	bmi	:+ ; if () {
		rts
	: ; } else {
		ora	#$D0
		rts
	; }
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LD679:
	bcc	LD60F
	sty	$94, x
	sta	$95, x
	sta	$95, x
	stx	$96, y
	stx	$96, y
	stx	$97, y
	.byte	$97
	tya

LD689:
	bcc	LD61C
	sta	($91), y
	.byte	$92
	.byte	$92
	.byte	$92
	.byte	$93
	.byte	$93
	.byte	$93
	.byte	$93
	.byte	$93
	.byte	$93
	sty	$94, x
	sty	$94, x
	sty	$94, x
	sty	$94, x
	sta	$95, x
	sta	$95, x
	sta	$95, x
	stx	$96, y
	stx	$96, y
	stx	$96, y
	stx	$96, y
	stx	$97, y
	.byte	$97
	.byte	$97
	.byte	$97
	pla
	dec	$F4AE, x
	ora	$30
	ora	($60, x)
	ora	#$D0
	rts
	ora	#$D0
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
apu_music_offsets:
	.byte	$8C
	.byte	$31
	.byte	$69
	sec
	.byte	$70
	.byte	$69
	sty	$2319
	rol	a
	.byte	$62
	ror	$8577, x
	.byte	$93
	ora	$231C, y
	rol	a
	.byte	$3F
	lsr	$4D
	.byte	$54
	eor	$095B
	stx	a:$D7
	eor	$52D7, y
	rol	$87, x
	.byte	$80
	brk
	inc	$66D7
	.byte	$42
	dey
	.byte	$80
	brk
	bit	$47D8
	rol	$4D
	.byte	$80
	brk
	ldx	$3BDC, y
	.byte	$27
	brk
	ora	($09, x)
	sty	$D8
	.byte	$14
	.byte	$0C
	brk
	ora	($09, x)
	.byte	$9F
	cld
	.byte	$23
	.byte	$13
	and	$01, x
	ora	#$E0
	cld
	stx	$54, y
	.byte	$B7
	ora	($09, x)
	ldy	$D9
	cmp	$6C
	.byte	$EF
	ora	($09, x)
	nop
	cmp	$5F9A, y
	lda	#$01
	ora	#$98
	.byte	$DA
	.byte	$2B
	.byte	$1A
	and	$1101, y
	dec	$DA, x
	.byte	$3A
	plp
	brk
	.byte	$80
	brk
	eor	($DB, x)
	.byte	$14
	ora	$8000
	brk
	.byte	$72
	.byte	$DB
	asl	$0E, x
	brk
	ora	($09, x)
	sta	($DB), y
	eor	($25, x)
	brk
	ora	($09, x)
	.byte	$F3
	.byte	$DB
	.byte	$82
	.byte	$42
	brk
	.byte	$80
	ora	#$9D
	.byte	$DC
	brk
	.byte	$12
	brk
	ora	($11, x)
	adc	($DB, x)
	brk
	asl	a
	brk
	ora	($09, x)
	asl	$83DD
	.byte	$4B
	brk
	ora	($84, x)
	rol	a
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$2A
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$2A
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	.byte	$82
	.byte	$32
	asl	$2E
	asl	$00
	and	($E1, x)
	.byte	$07
	sbc	($07, x)
	sbc	($07, x)
	sbc	($1F, x)
	.byte	$DF
	.byte	$07
	.byte	$DF
	.byte	$07
	.byte	$DF
	.byte	$07
	.byte	$DF
	ora	$07DD, x
	cmp	$DD07, x
	.byte	$07
	cmp	$86A8, x
	.byte	$9C
	stx	$84
	rol	a
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$2A
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$2A
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	rol	a
	.byte	$82
	jsr	$2806
	asl	$E0
	dec	$00
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	bit	$82
	.byte	$32
	.byte	$32
	.byte	$82
	.byte	$32
	asl	$32
	sty	$06
	.byte	$87
	.byte	$32
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	bit	$83
	jsr	$2A82
	asl	$2A
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	bit	$84
	asl	$87
	asl	$1A83, x
	.byte	$82
	.byte	$32
	asl	$32
	sty	$06
LD818:	.byte	$87
	.byte	$32
	sty	$06
	.byte	$87
	rol	a
	sty	$06
	.byte	$87
	bit	$82
	.byte	$32
	.byte	$32
	sta	($2A, x)
	sta	$06
	.byte	$83
	asl	$00
	.byte	$83
	asl	$06
	brk
	ldy	#$86
	ldy	#$07
	sbc	($07, x)
	.byte	$DB
	.byte	$07
	.byte	$DB
	.byte	$D2
	.byte	$9E
	stx	$9E
	.byte	$07
	.byte	$DF
	.byte	$07
	.byte	$D7
	.byte	$07
	.byte	$D7
	.byte	$D2
	.byte	$9C
	stx	$9C
	.byte	$07
	cmp	$D707, x
	.byte	$07
	.byte	$C3
	ldy	#$A0
	.byte	$5A
	.byte	$47
	dec	$C6
	dec	$82
	.byte	$12
	asl	$12
	asl	$12
	asl	$12
	asl	$16
	asl	$16
	asl	$16
	asl	$16
	asl	$20
	asl	$20
	asl	$20
	asl	$20
	asl	$12
	asl	$83
	asl	$06
	asl	$83
	asl	$06
	bcc	LD818
	brk
	sbc	($E1, x)
	sbc	($21, x)
	sbc	($E1, x)
	.byte	$C7
	sbc	($E1, x)
	.byte	$C7
	sbc	($81, x)
	asl	$87
	bmi	LD88F
	plp
	jsr	$1C06
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LD88D:
	.byte	$83
	.byte	$1A
LD88F:
	brk
	lsr	$E1
	.byte	$C7
	.byte	$D7
	.byte	$C3
	.byte	$C7
	.byte	$CF
	dex
	sta	($06, x)
	.byte	$82
	plp
	jsr	$1283
	.byte	$87
	asl	$06
	rol	$04, x
	asl	$34
	rol	$2806
LD8A9:	bit	$06
	.byte	$22
	.byte	$82
	jsr	$2036
	asl	$00
	.byte	$C7
	.byte	$C7
	sbc	#$E7
	.byte	$C7
	sbc	$DD
	.byte	$C7
	.byte	$D7
	.byte	$D3
	.byte	$C7
	.byte	$C3
	.byte	$82
	tay
	.byte	$9C
	stx	$87
	asl	$06
	rol	$062C
	rol	a
	plp
	asl	$20
	.byte	$1C
	asl	$1A
	.byte	$82
	asl	$2E, x
	asl	$20, x
	dec	$C6
	bcc	LD8A9
	cmp	($D1), y
	cmp	($C7), y
	cmp	($D1), y
	cmp	($D1), y
	.byte	$87
	rol	$06, x
	rol	a
	.byte	$32
	asl	$82
	rol	$87, x
	rol	a
	.byte	$32
	asl	$36
	plp
	bmi	LD926
	rol	$8306, x
	.byte	$3C
	.byte	$87
	asl	$36
	asl	$26
	rol	$8206
	rol	$87, x
	rol	$2E
	asl	$36
	bit	$3632
	rol	$8306, x
	.byte	$3C
	.byte	$87
	rol	$0646, x
	rol	$0646, x
	.byte	$82
	.byte	$3C
	.byte	$87
	lsr	$3E
	asl	$3C
	rol	$06, x
	.byte	$04
	rol	$06, x
	.byte	$82
	.byte	$32
	.byte	$87
	.byte	$2C
LD920:	.byte	$2E
	.byte	$06
LD922:	.byte	$32
	.byte	$34
	asl	$32
LD926:	.byte	$34
	asl	$82
	plp
	.byte	$87
	.byte	$32
	rol	$8306
	rol	a
	asl	$87
	asl	$00
	.byte	$EB
	.byte	$C7
	.byte	$DB
	sbc	($C7, x)
	tax
	.byte	$DB
	sbc	($C7, x)
	.byte	$EB
	cmp	$E9E1, y
	sbc	($C7), y
	inx
	.byte	$C7
	.byte	$E7
	.byte	$C7
	.byte	$D7
	sbc	($C7, x)
	ldx	$D7
	sbc	($C7, x)
	.byte	$E7
	.byte	$DB
	sbc	$ED
	.byte	$F3
	.byte	$C7
	cpx	$FDF3
	.byte	$C7
	.byte	$F7
	sbc	$B4C7, x
	sbc	$C7F7, x
	cmp	$F3
	.byte	$C7
	sbc	($F3), y
	.byte	$C7
	tax
	sbc	$E9
	.byte	$C7
	sbc	$C7EF
	.byte	$EB
	.byte	$EF
	.byte	$C7
LD96F:	ldy	#$EB
	sbc	#$C7
	cpx	#$C6
	.byte	$C7
	.byte	$82
	.byte	$12
	jsr	$2012
	.byte	$02
	jsr	PPU_SR
	bpl	LD9A1
	bpl	LD9A3
	asl	$0E20
	jsr	$1C08
	.byte	$44
	asl	$200A, x
	asl	$1624
	.byte	$1C
	asl	a
	asl	$12, x
	asl	a
	.byte	$12
	asl	$86
	bcc	LD920
	bcc	LD922
	bcc	LD96F
	cmp	($D1), y
	.byte	$D1
LD9A1:	.byte	$C7
	.byte	$D1
LD9A3:	brk
	.byte	$87
	asl	$06
	.byte	$82
	.byte	$32
	.byte	$87
	asl	$36
	asl	$06
	.byte	$3C
	asl	$83
	lsr	$87
	asl	$3C
	asl	$36
	.byte	$32
	asl	$2A
	rol	$3206
	rol	$3206
	rol	$2406
	asl	$06
	.byte	$82
	.byte	$2E
LD9C7:	.byte	$80
	bit	$2E
	bit	$2E
	bit	$2E
	bit	$2E
	bit	$2E
	bit	$2E
	.byte	$87
	asl	$82
	rol	$3287
	rol	$3206
	rol	$3206
	asl	$06
	.byte	$82
	.byte	$3C
	.byte	$87
	rol	$3C, x
	asl	$36
	brk
	.byte	$87
	.byte	$32
	asl	$2E
	rol	a
	asl	$82
	.byte	$32
	.byte	$80
	jsr	$2032
	.byte	$32
	jsr	$2032
	.byte	$32
	jsr	$2032
	.byte	$32
	.byte	$87
	asl	$06
	.byte	$87
	asl	$06
	rol	$06, x
	bmi	LDA0F
	plp
	asl	$24
	asl	$20
	.byte	$06
LDA0F:	brk
	.byte	$C7
	.byte	$C7
	ldy	#$C7
	.byte	$EB
	.byte	$C7
	.byte	$C7
	.byte	$F3
	.byte	$C7
	inc	$C7, x
	.byte	$F3
	.byte	$C7
	.byte	$EB
	sbc	$C7
	sbc	($DF, x)
	.byte	$C7
	sbc	($DF, x)
	.byte	$C7
	sbc	($DF, x)
	.byte	$C7
	.byte	$D7
	.byte	$C7
	.byte	$C7
	.byte	$9E
	asl	$1E, x
	asl	$16, x
	asl	$1E16, x
	asl	$1E, x
	asl	$1E, x
	asl	$C7, x
	.byte	$9C
	sbc	($DD, x)
	.byte	$C7
	sbc	($DD, x)
	.byte	$C7
	sbc	($C7, x)
	.byte	$C7
	tax
	sbc	#$EB
	.byte	$C7
	sbc	#$E1
	.byte	$C7
	cmp	$C7DB, x
	tax
	.byte	$1A
	rol	a
	.byte	$1A
	rol	a
	.byte	$1A
	rol	a
	.byte	$1A
	rol	a
	.byte	$1A
	rol	a
	.byte	$1A
	rol	a
	.byte	$C7
	.byte	$C7
	.byte	$C7
	.byte	$C7
	sbc	#$C7
	sbc	$C7
	sbc	($C7, x)
	.byte	$DD
LDA66:	.byte	$C7
	cmp	$82C7, y
	.byte	$12
	asl	$1A, x
	jsr	$2024
	.byte	$1A
	.byte	$12
	asl	$1A, x
	asl	$870E, x
	asl	$06, x
	.byte	$1A
	.byte	$82
	.byte	$12
	.byte	$02
	asl	$200A
	.byte	$1C
	asl	$02, x
	asl	a
	.byte	$82
	asl	$1202
	.byte	$14
	asl	$18, x
	.byte	$87
	.byte	$1A
	asl	$02
	.byte	$82
	asl	$020A
	bcs	LDA66
	.byte	$C7
	cmp	($00), y
	.byte	$87
	.byte	$3C
	asl	$36
	.byte	$32
	.byte	$06
LDA9E:	.byte	$83
	rol	a
	.byte	$87
	asl	$0620, x
LDAA4:	.byte	$22
	bit	$06
	rol	a
	asl	$06
	.byte	$82
	asl	$87
	rol	a
	.byte	$82
	asl	$06
	brk
	.byte	$EB
	.byte	$C7
	sbc	#$E1
	.byte	$C7
	.byte	$DA
	stx	$C7
	cmp	LDBC7, x
	.byte	$C7
	.byte	$C7
	stx	$DB
	stx	$86
	asl	$1202
	asl	a
	asl	$1202
	.byte	$87
	asl	$06
	.byte	$12
	.byte	$82
	asl	$06
	bcs	LDAA4
	.byte	$C7
	cmp	($00), y
	sta	($52, x)
	cli
	php
	.byte	$22
	asl	$85
	bit	$81
	.byte	$52
	cli
	php
	.byte	$22
	asl	$85
	jsr	$682
	sta	$2A
	rol	$82
	.byte	$1C
	sta	$22
	.byte	$82
	rol	$81
	clc
	.byte	$82
	.byte	$1C
	sta	$12
	sta	$16
	stx	$18
	.byte	$82
	asl	$00
	.byte	$47
	tya
	.byte	$5B
	.byte	$47
	tya
	.byte	$57
	stx	$5D
	eor	$5992, y
	.byte	$9C
	bvc	LDA9E
	eor	#$4B
	sta	$8186
	php
	.byte	$12
	.byte	$1C
	bit	$06
	rol	$1C
	.byte	$12
	php
	.byte	$12
	.byte	$1C
	bit	$06
	.byte	$22
	.byte	$1C
	.byte	$12
	php
	.byte	$12
	.byte	$1C
	.byte	$12
	asl	$22
	.byte	$1C
	.byte	$12
	php
	.byte	$12
	.byte	$1C
	.byte	$12
	asl	$22
	.byte	$1C
	.byte	$12
	php
	.byte	$12
	.byte	$1C
	.byte	$12
	asl	$22
	.byte	$1C
	.byte	$12
	php
	.byte	$12
	.byte	$1C
	.byte	$12
	asl	$22
	.byte	$1C
	.byte	$12
	sta	$22
	sta	($22, x)
	.byte	$82
	.byte	$22
	.byte	$82
	rol	$36, x
	.byte	$34
	stx	$32
	brk
	eor	$9858, y
	ldx	$A6
	ldx	#$A1
	sta	$22
	sta	($22, x)
	.byte	$82
	.byte	$22
	.byte	$82
	bmi	LDB8E
	bit	$2A86
	.byte	$87
	plp
	rol	a
	bit	$062E
	rol	$82, x
	.byte	$34
	brk
	sbc	($E5, x)
	.byte	$E7
	sbc	#$C7
	.byte	$F3
	ldx	$3681
	.byte	$3C
	lsr	$20
	bit	$2A
	asl	a
	asl	$1282
	asl	a
	.byte	$12
	brk
	stx	$52
	stx	$52
	stx	$92
	.byte	$9C
	txs
	.byte	$82
	asl	$85
	lsr	$2A
	.byte	$82
LDB8E:	.byte	$12
	jsr	$8212
	.byte	$04
	sta	($3C, x)
	asl	$82
	.byte	$04
	sta	($3C, x)
	asl	$36
	.byte	$3C
	.byte	$3A
	.byte	$3A
	asl	$3C
	.byte	$3A
	asl	$82
	.byte	$04
	sta	($3C, x)
	asl	$82
	pha
	sta	($3C, x)
	asl	$4E
	jmp	$3A4A

	asl	$3C
	rol	$06, x
	brk
	ldy	$6E
	lsr	$A4
	ror	$5846
	.byte	$5C
	rts

	rts

	lsr	$5C
	rts

	lsr	$A4
	.byte	$6E
	.byte	$46
LDBC7:	sty	$6E
	lsr	$7A
	.byte	$7C
	ror	$70, x
	lsr	$6E
	bvs	LDC18
	sta	($16, x)
	bit	$16
	bit	$16
	bit	$16
	bit	$18
	rol	$18
	rol	$18
	rol	$18
	rol	$16
	bit	$16
	bit	$16
	bit	$16
	bit	$18
	rol	$18
	rol	$18
	rol	$18
	rol	$81
	asl	$18, x
	clc
	asl	$18, x
	clc
	asl	$18, x
	asl	$18, x
	clc
	asl	$18, x
	clc
	asl	$18, x
	asl	$18, x
	clc
	asl	$18, x
	clc
	asl	$18, x
	asl	$18, x
	clc
	asl	$18, x
	clc
	asl	$18, x
	.byte	$1A
	.byte	$1C
	.byte	$1C
	.byte	$1A
LDC18:	.byte	$1C
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1C
	.byte	$1A
	.byte	$1C
	.byte	$1C
	.byte	$1A
	.byte	$1C
	brk
	lsr	$64
	.byte	$64
	lsr	$64
	.byte	$64
	lsr	$64
	lsr	$64
	.byte	$64
LDC40:	lsr	$64
	.byte	$64
	lsr	$64
	lsr	$62
	.byte	$62
	lsr	$62
	.byte	$62
	lsr	$62
	lsr	$62
	.byte	$62
	lsr	$62
	.byte	$62
	lsr	$62
	lsr	$68
	pla
	lsr	$68
	pla
	lsr	$68
	lsr	$68
	pla
	lsr	$68
	pla
	lsr	$68
	lsr	$66
	ror	$46
	ror	$66
	lsr	$66
	lsr	$66
LDC6F:	ror	$46
	ror	$66
	lsr	$66
	sta	$1E
	asl	$1E82, x
	sta	$1E
	asl	$1E82, x
	sta	$1C
	.byte	$1C
	.byte	$82
	.byte	$1C
	sta	$1C
	.byte	$1C
	.byte	$82
	.byte	$1C
	sta	$22
	.byte	$22
	.byte	$82
	.byte	$22
	sta	$22
	.byte	$22
	.byte	$82
	.byte	$22
	sta	$20
	jsr	$2082
	sta	$20
	jsr	$2082
	sta	($32, x)
	sec
	.byte	$3C
	rol	$3C06, x
	sec
	.byte	$34
	.byte	$32
	.byte	$34
	bmi	LDCDE
	asl	$32
	rol	a
	bmi	LDCAF
LDCAF:	asl	$72
	sei
	.byte	$7C
LDCB3:	ldx	$787C, y
	.byte	$74
	.byte	$72
	.byte	$74
	bvs	LDC6F
	.byte	$72
	ror	a
	bvs	LDC40
	bit	$2A
	rol	$2A24
	rol	$2420
	.byte	$32
	jsr	$3424
	.byte	$32
	rol	a
	bit	$06
	.byte	$1C
	bit	$2A
	.byte	$32
	rol	$2420
	asl	$12
	asl	$0A
	asl	$0A06
	.byte	$42
LDCDE:	asl	$82
	cli
	asl	a
	.byte	$12
	asl	$00
	stx	$92
	stx	$92
	stx	$96
	stx	$9A
	stx	$A4
	stx	$A8
	tax
	rts

	ldy	$60
	txs
	.byte	$92
	stx	$EA
	.byte	$82
	.byte	$1C
	.byte	$34
	asl	$2004, x
	rol	$24, x
	.byte	$3C
	rol	$201C
	.byte	$2E
	rol	a
	asl	$06
	asl	$2A
	jsr	$612
	sta	$36
	.byte	$04
	.byte	$82
	rol	$0485
	.byte	$34
	.byte	$82
	bit	$3485
	.byte	$32
	.byte	$82
	rol	a
	sta	($30, x)
	stx	$2E
	sta	($06, x)
	sta	$3C
	sec
	.byte	$82
	.byte	$32
	sta	$38
	rol	$82, x
	bmi	LDCB3
	rol	$04, x
	.byte	$82
	rol	$3481
	stx	$32
	sta	($06, x)
	bmi	LDD68
	bmi	LDD6A
	bmi	LDD6C
	bmi	LDD6E
	.byte	$32
	bmi	LDD75
	bmi	LDD77
	bmi	LDD79
	bmi	LDD7D
	.byte	$32
	.byte	$34
	.byte	$32
	.byte	$34
	.byte	$32
	.byte	$34
	.byte	$32
	.byte	$04
	.byte	$34
	.byte	$04
	.byte	$34
	.byte	$04
	.byte	$34
	.byte	$04
	.byte	$34
	brk
	.byte	$67
	adc	$9C
	adc	$63
	txs
	.byte	$63
	adc	($98, x)
	lsr	$469D, x
	.byte	$6B
	adc	#$A0
LDD68:	adc	#$67
LDD6A:	.byte	$9E
	.byte	$67
LDD6C:	adc	$9C
LDD6E:	.byte	$64
	.byte	$A3
	lsr	$64
	.byte	$62
	.byte	$64
	.byte	$62
LDD75:	.byte	$64
	.byte	$62
LDD77:	.byte	$64
	.byte	$62
LDD79:	ror	$64
	ror	$64
LDD7D:	ror	$64
	ror	$64
	pla
	ror	$68
	ror	$68
	ror	$68
	ror	$6A
	pla
	ror	a
	pla
	ror	a
	pla
	ror	a
	pla
	sta	($20, x)
	bit	$26
	jsr	$2624
	jsr	$2026
	bit	$26
	jsr	$2624
	jsr	$2026
	bit	$26
	jsr	$2624
	jsr	$2026
	bit	$26
	jsr	$2624
	jsr	$1E26
	.byte	$22
	bit	$1E
	.byte	$22
	bit	$1E
	bit	$1E
	.byte	$22
	bit	$1E
	.byte	$22
	bit	$1E
	bit	$1E
	.byte	$22
	bit	$1E
	.byte	$22
	bit	$1E
	bit	$1E
	.byte	$22
	bit	$1E
	.byte	$22
	bit	$1E
	bit	$81
	.byte	$1C
	.byte	$82
	.byte	$1C
	.byte	$1C
	.byte	$1C
	sta	($1C, x)
	asl	$1E82, x
	asl	$811E, x
	asl	$8220, x
	jsr	$2020
	sta	($20, x)
	.byte	$22
	.byte	$82
	.byte	$22
	.byte	$22
	.byte	$22
	sta	($22, x)
	brk
	.byte	$02
	.byte	$02
	.byte	$02
	.byte	$42
	.byte	$02
	.byte	$02
	brk
	.byte	$02
	.byte	$02
	.byte	$22
	brk
	rol	$0210
	brk
	.byte	$42
; ------------------------------------------------------------
; data/apu/notes.s
; ------------------------------------------------------------
apu_notes:
	brk
	dey
	ora	($C4, x)
	brk
	.byte	$97
	brk
	brk
	.byte	$02
	.byte	$80
	.byte	$02
	.byte	$3A
	.byte	$02
	.byte	$1A
	ora	($FC, x)
	ora	($DF, x)
	ora	($AB, x)
	ora	($93, x)
	ora	($7C, x)
	ora	($67, x)
	ora	($53, x)
	ora	($40, x)
	ora	($2E, x)
	ora	($1D, x)
	ora	($0D, x)
	brk
	inc	$EF00, x
	brk
	.byte	$E2
	brk
	cmp	$00, x
	cmp	#$00
	ldx	$B300, y
	brk
	lda	#$00
	ldy	#$00
	stx	$8600
	brk
	.byte	$77
	brk
	ror	$7100, x
	.byte	$03
	.byte	$27
	.byte	$02
	ldx	$02
	.byte	$5C
	brk
	ror	a
	brk
	.byte	$5F
	brk
	eor	$5000, y
	brk
	.byte	$47
	ora	$4D
	ora	$01
	.byte	$03
	sed
	.byte	$03
	.byte	$89
	.byte	$03
	.byte	$57
; ------------------------------------------------------------
; src/apu/store.s
; ------------------------------------------------------------
	.export apu_write_base_pulse_1
apu_write_base_pulse_1:
	sty	PULSE1_SWEEP
	stx	PULSE1_VOL
	rts
; ------------------------------------------------------------
apu_write_pulse_1:
	jsr	apu_write_base_pulse_1

	.export	apu_write_note_pulse_1
apu_write_note_pulse_1:
	ldx	#CHANNEL_PULSE1*CHANNEL_SIZE
; -----------------------------
apu_write_note:
	tay
	lda	apu_notes+1, y
	beq	:+ ; if () {
		sta	CHANNEL_TIMER, x
	
		lda	apu_notes, y
		ora	#channel_counter::length_254
		sta	CHANNEL_COUNTER, x
	: ; }

	rts
; -----------------------------
	.export apu_write_base_pulse_2
apu_write_base_pulse_2:
	stx	PULSE2_VOL
	sty	PULSE2_SWEEP
	rts
; -----------------------------
apu_write_pulse_2:
	jsr	apu_write_base_pulse_2

	.export apu_write_note_pulse_2
apu_write_note_pulse_2:
	ldx	#CHANNEL_PULSE2*CHANNEL_SIZE
	bne	apu_write_note
; -----------------------------
	.export apu_write_note_triangle
apu_write_note_triangle:
	ldx	#CHANNEL_TRIANGLE*CHANNEL_SIZE
	bne	apu_write_note
; ------------------------------------------------------------
; src/apu/fds_store.s
; ------------------------------------------------------------
	.export apu_fds_write_note_wave
apu_fds_write_note_wave:
	ldx	#FDS_SND_MOD_ENV_4X
	stx	FDS_SND_FREQ+1
	tay
	lda	apu_fds_wave_notes, y
	sta	FDS_SND_FREQ+1
	lda	apu_fds_wave_notes+1, y
	sta	FDS_SND_FREQ
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
apu_fds_wave_offsets:
	.byte	$46
	.byte	$2B
	.byte	$2B
	.byte	$2B
	.byte	$22
	.byte	$4F
	.byte	$46
	.byte	$34
	bpl	LDEC0
	bpl	LDEB9
	and	$2B46, x
	lsr	$55
	.byte	$DF
	clc
	.byte	$03
	.byte	$DF
	.byte	$07
	.byte	$DF
	brk
	bmi	LDF0C
	.byte	$DF
	.byte	$FF
LDEB9:	sbc	$DE, x
	sbc	$DE, y
	rti

	.byte	$55
LDEC0:	.byte	$DF
	bvc	LDED0
	.byte	$DF
	ora	($DF), y
	jsr	$7520
	.byte	$DF
	rts

	sbc	$DE, x
	.byte	$80
	cmp	($00), y
LDED0:	brk
	and	$DF, x
	rts

	sta	$DF, x
	ldy	#$DF
	jsr	$A640
	.byte	$DF
	bmi	LDEFD
	.byte	$DF
	and	$DF
	jsr	$A640
	.byte	$DF
	bmi	apu_write_note+7
	cmp	($8E), y
	cmp	($20), y
	jsr	LDF55
	bmi	LDF1F
	.byte	$DF
	dec	$DF
	jsr	$A040
	.byte	$03
	.byte	$3F
	.byte	$FF
	ldy	#$01
	ldy	#$40
LDEFD:	brk
	and	($3A, x)
	bmi	LDF02
LDF02:	.byte	$FF
	ldy	#$01
	.byte	$0B
	jsr	$1A0
	asl	a
	asl	a
	brk
LDF0C:	clc
	ldy	#$1D
	.byte	$03
	rti

	dey
	ora	($46, x)
	and	$0F, x
	.byte	$0C
	.byte	$1F
	rti

	ora	($05, x)
	clc
	.byte	$80
	brk
	clc
LDF1F:	ldy	#$01
	.byte	$5A
	asl	$1F
	bit	$1A0
	.byte	$03
	and	$0F, x
	asl	$301F
	.byte	$03
	bmi	LDEC0
	.byte	$03
	.byte	$43
	asl	$0B, x
	rti

	brk
	ora	($02, x)
	.byte	$03
	.byte	$04
	ora	$07
	ora	#$0B
	ora	$1310
	asl	$19, x
	.byte	$1C
	.byte	$1F
	.byte	$22
	and	$28
	.byte	$2B
	rol	$3331
	and	$37, x
	and	$3B3A, y
	.byte	$3C
	and	$3E3E, x
LDF55:	bpl	LDF76
	.byte	$3A
	.byte	$3A
	.byte	$2B
	rol	$3C3D
	.byte	$3C
	and	$2E3E, x
	and	$33
	.byte	$37
	plp
	bpl	LDF7A
	plp
	.byte	$22
	ora	$03
	rol	$0916, x
	brk
	brk
	.byte	$0B
	.byte	$0B
	brk
	brk
	.byte	$02
	.byte	$10
LDF76:	bit	$272E
	.byte	$29
LDF7A:	.byte	$2B
	rol	a
	plp
	and	$29
	.byte	$2F
	and	$2A2C
	.byte	$22
	bit	$34
	.byte	$3F
	and	($2D), y
	.byte	$3A
	.byte	$3B
	.byte	$27
	.byte	$12
	asl	a
	.byte	$1F
	bit	$2327
	plp
	.byte	$22
	asl	$06A0, x
	.byte	$1F
	asl	$084A
	asl	a
	php
	lsr	a
	.byte	$04
	asl	a
	.byte	$82
	.byte	$02
	.byte	$52
	ora	$00
	rts

	brk
	.byte	$0C
	ora	$1B, x
	bit	$2D
	.byte	$33
	sec
	.byte	$3C
	and	$393B, x
	.byte	$37
	.byte	$37
	sec
	.byte	$34
	.byte	$34
	.byte	$32
	and	($30), y
	.byte	$2F
	.byte	$34
	.byte	$2F
	and	$2B2D
	and	#$26
	bit	$20
	ora	$9619, y
	.byte	$04
	.byte	$44
	and	$0F, x
	php
	.byte	$0F
	.byte	$20
	.byte	$00
	.byte	$40
; ------------------------------------------------------------
LDFD0:
	bpl	LDFF0
	.byte	$1F
	.byte	$16
LDFD4:
	brk
	.byte	$03
	asl	a
	.byte	$02

LDFD8:
	brk
	clc
	clc
	cli
; ------------------------------------------------------------
;
; ------------------------------------------------------------
	.export apu_note_decomp
apu_note_decomp:
	tax
	ror	a
	txa
	rol	a
	rol	a
	rol	a
; --------------
	.export apu_note_length_load
apu_note_length_load:
	and	#SND_NOTE_LEN_MASK
	clc
	adc	apu_note_len_offset
	tay
	lda	apu_note_lengths, y
	rts
; --------------
	.export apu_note_length_load_b
apu_note_length_load_b:
	and	#SND_NOTE_LEN_B_MASK
	tay
LDFF0:	lda	apu_note_lengths, y
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
	.byte	$1C, $00
