.include	"system/apu.i"

.include	"mem.i"
.include	"sound.i"

	.export apu_alt_cycle
apu_alt_cycle:
	lda	#$FF
	sta	DMCFRAMECOUNT

	lda	#dmcstatus::noise|dmcstatus::tri|dmcstatus::pulse_2|dmcstatus::pulse_1
	sta	DMCSTATUS

	jsr	LCC1F
	jsr	LD2A5

	lda	#0
	sta	apu_music_base_req
	sta	byte_602
	sta	apu_dpcm_queue
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LCC1C:
	jmp	LCCAA
; --------------
LCC1F:
	lda	apu_music_base_req
	bmi	LCC2C
	bne	LCC2F
	lda	apu_music_base_current
	bne	LCC1C
	rts
; --------------
LCC2C:
	jmp	LCCC2
; --------------
LCC2F:
	ldy	#$00
	sty	apu_pulse_2_offset
	sty	byte_61D
; --------------
LCC37:
	sta	apu_music_base_current
	inc	byte_61D
	ldy	byte_61D
	cpy	#$08
	bne	LCC4B
	jmp	LCCC2
; --------------
LCC47:
	: ; for (y; !carry; y++) {
		iny
		lsr	a
		bcc	:-
	; }
; --------------
LCC4B:
	lda	apu_alt_music_offsets-1, y
	tay
	lda	apu_alt_music_offsets, y
	sta	apu_note_len_offset

	lda	apu_alt_music_offsets+1, y
	sta	apu_track
	lda	apu_alt_music_offsets+2, y
	sta	apu_track+1

	lda	apu_alt_music_offsets+3, y
	sta	apu_triangle_offset
	lda	apu_alt_music_offsets+4, y
	sta	apu_pulse_1_offset
	sta	byte_61A

	lda	apu_alt_music_offsets+5, y
	sta	apu_noise_offset
	sta	apu_noise_loop_offset

	lda	apu_alt_music_offsets+6, y
	sta	byte_5EC
	lda	apu_alt_music_offsets+7, y
	sta	zp_byte_C5

	lda	apu_alt_music_offsets+8, y
	sta	byte_5F4
	lda	apu_alt_music_offsets+9, y
	sta	byte_61C
	jsr	apu_alt_fds_wave_proc

	lda	#1
	sta	apu_pulse_2_note_len
	sta	apu_pulse_1_note_len
	sta	apu_triangle_note_len
	sta	apu_noise_note_len

	sta	byte_5F1

	lda	#$00
	sta	apu_pulse_2_offset
	sta	byte_61E
; --------------
LCCAA:
	dec	apu_pulse_2_note_len
	bne	LCCFB
	ldy	apu_pulse_2_offset
	inc	apu_pulse_2_offset
	lda	(apu_track), y
	beq	:+
	bpl	LCCE7
	bne	LCCD9
	: lda	apu_music_base_current
	bne	LCCD6
; --------------
LCCC2:
	lda	#$00
	sta	apu_music_base_current
	sta	DMCSTATUS
	sta	byte_60E
	sta	byte_607
	lda	#$80
	sta	FDS_SND_VOL
	rts
; --------------
LCCD6:
	jmp	LCC37
; --------------
LCCD9:
	jsr	apu_alt_note_length_load
	sta	apu_pulse_2_note_len_b
	ldy	apu_pulse_2_offset
	inc	apu_pulse_2_offset
	lda	(apu_track), y
; --------------
LCCE7:
	jsr	apu_alt_write_note_pulse_2
	beq	:+ ; if () {
		jsr	LCF6C
	: ; }
	sta	apu_pulse_2_env_ctl

	jsr	apu_alt_write_base_pulse_2
	lda	apu_pulse_2_note_len_b
	sta	apu_pulse_2_note_len
; --------------
LCCFB:
	ldy	apu_pulse_2_env_ctl
	beq	:+ ; if () {
		dec	apu_pulse_2_env_ctl
	: ; }

	jsr	LCF85
	sta	PULSE2_VOL
	ldx	#$7F
	stx	PULSE2_SWEEP
; --------------
	ldy	apu_pulse_1_offset
	beq	:+++++++ ; if () {
		dec	apu_pulse_1_note_len
		bne	:++++ ; if () {
			: ; for () {
				ldy	apu_pulse_1_offset
				inc	apu_pulse_1_offset
				lda	(apu_track), y
				bne	:+ ; if () {
					lda	byte_61A
					sta	apu_pulse_1_offset
					bne	:-
				; } else break;
			: ; }

			jsr	apu_alt_note_decomp
			sta	apu_pulse_1_note_len
			txa
			and	#SND_CMP_NOTE_MASK
			jsr	apu_alt_write_note_pulse_1
			beq	:+ ; if () {
				jsr	LCF6C
			: ; }
			sta	apu_pulse_1_env_ctl

			jsr	apu_alt_write_base_pulse_1
		: ; }

		ldy	apu_pulse_1_env_ctl
		beq	:+ ; if () {
			dec	apu_pulse_1_env_ctl
		: ; }

		jsr	LCF7E
		sta	PULSE1_VOL
		lda	byte_61E
		bne	:+ ; if () {
			lda	#$7F
		: ; }
		sta	PULSE1_SWEEP
	: ; }
; --------------
	lda	apu_triangle_offset
	beq	:++++++++
	dec	apu_triangle_note_len
	bne	:++++++++ ; if () {
		: ; for () {
			ldy	apu_triangle_offset
			inc	apu_triangle_offset
			lda	(apu_track), y
			beq	:++++++
			bpl	:++++
			cmp	#$F0
			beq	:+
			bcc	:+++ ; if () {
				sec
				sbc	#$F0
				sta	byte_5F0

				lda	apu_triangle_offset
				sta	byte_5F5

				jmp	:-
			: dec	byte_5F0
			beq	:+ ; } else if () {
				lda	byte_5F5
				sta	apu_triangle_offset
			: ; }

			jmp	:---
		: ; }

		; if () {
			jsr	apu_alt_note_length_load
			sta	apu_triangle_note_len_b
			lda	#$1F
			sta	TRIANGLE_VOL

			ldy	apu_triangle_offset
			inc	apu_triangle_offset
			lda	(apu_track), y
			beq	:+++
		: ; }

		; if () {
			jsr	apu_alt_write_note_triangle
			ldx	apu_triangle_note_len_b
			stx	apu_triangle_note_len
			txa
			cmp	#$1C
			bcs	:+ ; if () {
				lda	#$0F
				bne	:++
			: ; } else {
				lda	#$FF
			; }
		: ; }

		sta	TRIANGLE_VOL
	: ; }
; --------------
	lda	byte_5EC
	bne	:+
	jmp	LCE80
	: ; if () {
		lda	byte_5F1
		cmp	#$02
		bne	:+ ; if () {
			lda	#$00
			sta	FDS_SND_VOL
		: ; }
		dec	byte_5F1
		bne	:++++ ; if () {
			ldy	byte_5EC
			inc	byte_5EC
			lda	(apu_track), y
			bpl	:+ ; if () {
				jsr	apu_alt_note_length_load
				sta	byte_5F2

				ldy	byte_5EC
				inc	byte_5EC
				lda	(apu_track), y
			: ; }

			jsr	apu_alt_fds_write_note_wave
			tay
			bne	:+ ; if () {
				ldx	#$80
				stx	FDS_SND_VOL

				bne	:++
			: ; } else {
				jsr	apu_fds_mod_get
				ldy	byte_5F7
			: ; }
			sty	byte_5F3

			ldy	#$00
			sty	byte_5F9
			sty	byte_5FB
			lda	(zp_addr_BF), y
			sta	FDS_SND_VOL
			lda	(zp_addr_C1), y
			sta	FDS_SND_MOD_ENV
			lda	#$00
			sta	FDS_SND_MOD_COUNT
			iny
			lda	(zp_addr_BF), y
			sta	byte_5F8
			lda	(zp_addr_C1), y
			sta	byte_5FA
			sty	byte_5F9
			sty	byte_5FB
			lda	byte_5F2
			sta	byte_5F1
		: ; }

		lda	byte_5F3
		beq	:++++ ; if () {
			dec	byte_5F3
			dec	byte_5F8
			bne	:+++ ; if () {
				: ; for () {
					inc	byte_5F9
					ldy	byte_5F9
					lda	(zp_addr_BF), y
					bpl	:+ ; if () {
						sta	FDS_SND_VOL
						bne	:-
					; } else break;
				: ; }
				sta	FDS_SND_VOL

				iny
				lda	(zp_addr_BF), y
				sta	byte_5F8
				sty	byte_5F9
			: ; }

			dec	byte_5FA
			bne	:+ ; if () {
				inc	byte_5FB
				ldy	byte_5FB
				lda	(zp_addr_C1), y
				sta	FDS_SND_MOD_ENV
				iny
				lda	(zp_addr_C1), y
				sta	FDS_SND_MOD_FREQ_LO
				iny
				lda	(zp_addr_C1), y
				sta	FDS_SND_MOD_FREQ_HI
				iny
				lda	(zp_addr_C1), y
				sta	byte_5FA
				sty	byte_5FB
			; }
		; }
	: ; }
; --------------
LCE80:
	lda	apu_music_base_current
	and	#%00100011
	beq	:+++++++
	dec	apu_noise_note_len
	bne	:+++++++ ; if () {
		: ; for () {
			ldy	apu_noise_offset
			inc	apu_noise_offset

			lda	(apu_track), y
			bne	:+ ; if () {
				lda	apu_noise_loop_offset
				sta	apu_noise_offset
				bne	:-
			; } else break;
		: ; }

		jsr	apu_alt_note_decomp
		sta	apu_noise_note_len
		txa
		and	#SND_CMP_NOTE_MASK
		beq	:+++
		cmp	#$30
		beq	:++
		cmp	#$20
		beq	:+
		and	#$10
		beq	:+++ ; if () {
			lda	#$18
			ldx	#$03
			ldy	#$18
			bne	:++++
		: ; } else if () {
			lda	#$18
			ldx	#$0C
			ldy	#$18
			bne	:+++
		: ; } else if () {
			lda	#$18
			ldx	#$03
			ldy	#$58
			bne	:++
		: ; } else {
			lda	#$10
		: ; }
		sta	NOISE_VOL
		stx	NOISE_PERIOD
		sty	NOISE_COUNTER
	: ; }
; --------------
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
apu_alt_fds_wave_proc:
	lda	zp_byte_C5
	bne	LCEDE
	rts
LCEDE:
	ldy	#0
	: ; for (y = 0; !carry; y++) {
		iny
		lsr	a
		bcc	:-
	; }


	lda	apu_alt_fds_wave_offsets-1, y
	tay
	lda	apu_alt_fds_wave_offsets, y
	sta	zp_addr_BD
	lda	apu_alt_fds_wave_offsets+1, y
	sta	zp_addr_BD+1
	lda	apu_alt_fds_wave_offsets+2, y
	sta	byte_5F7
	lda	apu_alt_fds_wave_offsets+3, y
	sta	zp_addr_BF
	lda	apu_alt_fds_wave_offsets+4, y
	sta	zp_addr_BF+1
	lda	apu_alt_fds_wave_offsets+5, y
	sta	zp_addr_C1
	lda	apu_alt_fds_wave_offsets+6, y
	sta	zp_addr_C1+1
	lda	apu_alt_fds_wave_offsets+7, y
	sta	byte_5F6

	jsr	LD41D

	lda	#$02
	sta	FDS_SND_WAVE_VOL
	rts
; ------------------------------------------------------------
LCF1A:
	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$07
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$07
	.byte	$77
	.byte	$77
	.byte	$77
	.byte	$77
	ora	($11), y
	ora	($11), y
	ora	($11), y
	ora	($11), y
	.byte	$77
	.byte	$77
	.byte	$77
	.byte	$77
	.byte	$33
	.byte	$22
	and	($10, x)
	.byte	$07
	ror	$66, x
	eor	$55, x
	ror	$67
	bvs	LCF49
	.byte	$12
LCF49:	.byte	$22
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	.byte	$33
	eor	$55, x
	eor	$55, x
	eor	$55, x
	eor	$55, x
; ------------------------------------------------------------
;
; ------------------------------------------------------------
apu_alt_note_decomp:
	tax
	ror	a
	txa
	rol	a
	rol	a
	rol	a
; --------------
apu_alt_note_length_load:
	and	#SND_NOTE_LEN_MASK
	clc
	adc	apu_note_len_offset
	tay
	lda	apu_alt_note_lengths, y
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LCF6C:	lda	byte_5F4
	and	#$0F
	bmi	LCF77
	lda	#$17
	bne	LCF79
LCF77:	lda	#$1F
LCF79:	ldx	#$82
	ldy	#$7F
	rts
; ------------------------------------------------------------
LCF7E:
	lda	byte_61C
	cmp	#$01
	beq	LCF92
LCF85:
	lda	byte_5F4
LCF88:
	bmi	LCF8E
	lda	LCFD9, y
	rts
; ------------------------------------------------------------
LCF8E:	lda	LCFF1, y
	rts
; ------------------------------------------------------------
LCF92:	lda	LCFD9, y
	rts
; ------------------------------------------------------------
; src/apu/alt_store.s
; ------------------------------------------------------------
	.export	apu_alt_write_base_pulse_1
apu_alt_write_base_pulse_1:
	sty	PULSE1_SWEEP
	stx	PULSE1_VOL
	rts
; ------------------------------------------------------------
apu_alt_write_pulse_1:
	jsr	apu_alt_write_base_pulse_1

	.export	apu_alt_write_note_pulse_1
apu_alt_write_note_pulse_1:
	ldx	#CHANNEL_PULSE1*CHANNEL_SIZE
; -----------------------------
apu_alt_write_note:
	tay
	lda	apu_alt_notes+1, y
	beq	:+ ; if () {
		sta	CHANNEL_TIMER, x

		lda	apu_alt_notes, y
		ora	#channel_counter::length_254
		sta	CHANNEL_COUNTER, x
	: ; }

	rts
; -----------------------------
        .export apu_alt_write_base_pulse_2
apu_alt_write_base_pulse_2:
	stx	PULSE2_VOL
	sty	PULSE2_SWEEP
	rts
; -----------------------------
apu_alt_write_pulse_2:
	jsr	apu_alt_write_base_pulse_2

        .export apu_alt_write_note_pulse_2
apu_alt_write_note_pulse_2:
	ldx	#CHANNEL_PULSE2*CHANNEL_SIZE
	bne	apu_alt_write_note
; -----------------------------
        .export apu_alt_write_note_triangle
apu_alt_write_note_triangle:
	ldx	#CHANNEL_TRIANGLE*CHANNEL_SIZE
	bne	apu_alt_write_note
; ------------------------------------------------------------
; src/apu/alt_fds_store.s
; ------------------------------------------------------------
        .export apu_alt_fds_write_note_wave
apu_alt_fds_write_note_wave:
	ldx	#FDS_SND_MOD_ENV_4X
	stx	FDS_SND_FREQ+1
	tay
	lda	apu_alt_fds_wave_notes, y
	sta	FDS_SND_FREQ+1
	lda	apu_alt_fds_wave_notes+1, y
	sta	FDS_SND_FREQ
	rts
; ------------------------------------------------------------
;
; ------------------------------------------------------------
LCFD9:
	sta	$95, x
	sta	$95, x
	sta	$95, x
	sta	$95, x
	sta	$95, x
	sta	$95, x
	sta	$96, x
	stx	$96, y
	stx	$97, y
	.byte	$97
	tya
	tya
	sta	$9B9A, y
; ------------------------------------------------------------
LCFF1:
	bcc	LCF88
	sta	$95, x
	stx	$96, y
	stx	$97, y
	.byte	$97
	.byte	$97
	tya
	tya
	tya
	sta	$9999, y
	txs
	txs
	txs
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9B
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9C
	.byte	$9D
	.byte	$9D
	.byte	$9D
; ------------------------------------------------------------
;
; ------------------------------------------------------------
apu_alt_fds_wave_offsets:	.byte	$0D
	.byte	$0D
	.byte	$04
	.byte	$0D
	.byte	$27
	.byte	$D0
	.byte	$44
	adc	$D0, x
	sta	($D0, x)
	jsr	$4700
	bne	LD081
	.byte	$67
	bne	LD09F
	bne	LD026
LD026:	brk
	ora	(zp_addr_0C, x)
	asl	$20, x
	bit	$27
	.byte	$27
	rol	a
	.byte	$2F
	.byte	$2F
	.byte	$32
	bmi	LD067
	rol	$3A, x
	rol	$37, x
	and	($32), y
	.byte	$34
	and	$3F3F, x
	.byte	$3B
	sec
	and	$33, x
	bmi	LD073
	and	#$23
	.byte	$23
	bpl	LD075
	rol	$2927
	.byte	$2B
	rol	a
	plp
	and	$29
	.byte	$2F
	and	$2A2C
	.byte	$22
	bit	$34
	.byte	$3F
	and	($2D), y
	.byte	$3A
	.byte	$3B
	.byte	$27
	.byte	$12
	asl	a
	.byte	$1F
	bit	$2327
	plp
	.byte	$22
	.byte	$1E
LD067:	ldy	#$02
	clc
	rts
	ldy	#$02
	asl	$28
	.byte	$80
	.byte	$03
	.byte	$82
	.byte	$10
LD073:	brk
	.byte	$50
LD075:	ldy	#$02
	rol	$25, x
	.byte	$80
	.byte	$34
	.byte	$80
	.byte	$02
	.byte	$80
	brk
	brk
	rts
LD081:	.byte	$80
	.byte	$2B
	sta	($0A, x)
	brk
	.byte	$04
	.byte	$82
	bpl	LD08A
LD08A:	rts
	ora	($07, x)
	ora	($C8, x)
	.byte	$03
	cld
	.byte	$02
	inx
	ora	($01, x)
	ora	($07, x)
	.byte	$27
	.byte	$72
	.byte	$27
	.byte	$82
	.byte	$27
	ror	$0301, x
LD09F:	.byte	$27
	cmp	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	.byte	$07
	.byte	$3B
	.byte	$74
	.byte	$27
	.byte	$57
	.byte	$27
	.byte	$97
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	$27
	adc	$27
	sta	$05, x
	.byte	$42
	rol	a
	eor	($87, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($03, x)
	pla
	bit	$01
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($01, x)
	ora	($08, x)
	dey
	dey
	dey
	clv
	.byte	$80
	brk
	rti
	.byte	$9C
