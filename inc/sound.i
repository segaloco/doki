.ifndef	SOUND_I
SOUND_I	= 1

.include	"system/apu.i"

	SND_TRACK_NOTE_LEN	= %10000000
	SND_CMP_LEN_MASK	= %11000001
	SND_CMP_NOTE_MASK	= %00111110
	SND_NOTE_LEN_MASK	= %00000111
        SND_NOTE_LEN_B_MASK     = %00001111

	.macro	snd_cmp_byte	len, note
		.byte	<(note|(len<<6)|(len>>2))
	.endmacro

	SND_TRACK_END		= 0

	.enum	music_base_alt
		music_00	= %00000001
		none		= %10000000
	.endenum

	.enum	sfx_dpcm_alt
		sfx_00		= %00000001
	.endenum

.endif	; SOUND_I
