.ifndef	CHR_I
CHR_I = 1

	.enum	chr_obj
		chr_00	= $00
		chr_02	= $02
		chr_04	= $04
		chr_06	= $06
		chr_08	= $08
		chr_0A	= $0A
		chr_0C	= $0C
		chr_0E	= $0E
		chr_10	= $10
		chr_12	= $12
		chr_14	= $14
		chr_16	= $16
		chr_18	= $18
		chr_1A	= $1A
		chr_1C	= $1C
		chr_1E	= $1E
		chr_20	= $20
		chr_22	= $22
		chr_24	= $24
		chr_26	= $26
		chr_28	= $28
		chr_2A	= $2A
		chr_2C	= $2C
		chr_2E	= $2E
		chr_30	= $30
		chr_32	= $32
		chr_E8	= $E8
		chr_E9	= $E9
		chr_EA	= $EA
		chr_EB	= $EB
		chr_EC	= $EC
		chr_ED	= $ED
		chr_EE	= $EE
		chr_EF	= $EF
		chr_F0	= $F0
		chr_F1	= $F1
		chr_F2	= $F2
		chr_F3	= $F3
		chr_F4	= $F4
		chr_F5	= $F5
		chr_F7	= $F7
		chr_F8	= $F8
		chr_F9	= $F9
		chr_FA	= $FA
		chr_FB	= $FB
		chr_FC	= $FC
		chr_FE	= $FE
		chr_FF	= $FF
	.endenum

.endif	; CHR_I
