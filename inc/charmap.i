.ifndef CHARMAP_I
CHARMAP_I = 1

.include	"./tunables.i"

	.charmap	'/', $9E
	.charmap	'c', $9F

	.charmap	'0', ENG_FONT_TILE_BASE+0
	.charmap	'1', ENG_FONT_TILE_BASE+1
	.charmap	'2', ENG_FONT_TILE_BASE+2
	.charmap	'3', ENG_FONT_TILE_BASE+3
	.charmap	'4', ENG_FONT_TILE_BASE+4
	.charmap	'5', ENG_FONT_TILE_BASE+5
	.charmap	'6', ENG_FONT_TILE_BASE+6
	.charmap	'7', ENG_FONT_TILE_BASE+7
	.charmap	'8', ENG_FONT_TILE_BASE+8
	.charmap	'9', ENG_FONT_TILE_BASE+9
	.charmap	'A', ENG_FONT_TILE_BASE+10
	.charmap	'B', ENG_FONT_TILE_BASE+11
	.charmap	'C', ENG_FONT_TILE_BASE+12
	.charmap	'D', ENG_FONT_TILE_BASE+13
	.charmap	'E', ENG_FONT_TILE_BASE+14
	.charmap	'F', ENG_FONT_TILE_BASE+15
	.charmap	'G', ENG_FONT_TILE_BASE+16
	.charmap	'H', ENG_FONT_TILE_BASE+17
	.charmap	'I', ENG_FONT_TILE_BASE+18
	.charmap	'J', ENG_FONT_TILE_BASE+19
	.charmap	'K', ENG_FONT_TILE_BASE+20
	.charmap	'L', ENG_FONT_TILE_BASE+21
	.charmap	'M', ENG_FONT_TILE_BASE+22
	.charmap	'N', ENG_FONT_TILE_BASE+23
	.charmap	'O', ENG_FONT_TILE_BASE+24
	.charmap	'P', ENG_FONT_TILE_BASE+25
	.charmap	'x', ENG_FONT_TILE_BASE+26
	.charmap	'R', ENG_FONT_TILE_BASE+27
	.charmap	'S', ENG_FONT_TILE_BASE+28
	.charmap	'T', ENG_FONT_TILE_BASE+29
	.charmap	'U', ENG_FONT_TILE_BASE+30
	.charmap	'V', ENG_FONT_TILE_BASE+31
	.charmap	'W', ENG_FONT_TILE_BASE+32
	.charmap	'X', ENG_FONT_TILE_BASE+33
	.charmap	'Y', ENG_FONT_TILE_BASE+34
	.charmap	'-', ENG_FONT_TILE_BASE+35
	.charmap	'?', ENG_FONT_TILE_BASE+39
	.charmap	'_', ENG_FONT_TILE_BASE+42
			; note, _ is a space, just a different one...
	.charmap	' ', ENG_FONT_TILE_BASE+43

.endif	; CHARMAP_I
