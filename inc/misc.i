.ifndef	MISC_I
MISC_I = 1

	.enum	jar_type
		warp		= $00
		inner_space	= $01
	.endenum

	.enum	scroll_req
		bit0	= 1
		bit1	= 2
		bit2	= 4
		bit3	= 8
		bit4	= 16
		bit5	= 32
		bit6	= 64
		bit7	= 128
	.endenum

.endif	; MISC_I
