.ifndef ACTORS_I
ACTORS_I = 1

	.enum	actor_collisions
		right = $01
		left = $02
		floor = $04
		ceil = $08
		damage = $10
		player_top = $20
		player_col = $40
		bit7 = $80
	.endenum

	.enum	actor_col_type
		type_0	= 0
		type_1	= 1
		type_2	= 2
		type_3	= 3
		type_4	= 4
	.endenum

	.enum	actor_attrs
		bit0 = $01
		bit1 = $02
		bit2 = $04
		bit3 = $08
		h_flip = $10
		bit5 = $20
		bit6 = $40
		v_flip = $80
	.endenum

	.enum	actor_flags
		harm_top	= 1
		cant_lift	= 2
		no_actor_col	= 4
		death_noise	= 8
		tilemap2	= 16
		wide		= 32
		fast		= 64
		mirror_ani	= 128
	.endenum

	ACTOR_DIR_RIGHT	= 1
	ACTOR_DIR_LEFT	= 2

	.enum	actor
		heart		= $00

		shyguy_red	= $01

		tweeter		= $02

		shyguy_pink	= $03

		porcupo		= $04
	
		snifit_red	= $05
		snifit_grey	= $06
		snifit_pink	= $07
	
		ostro		= $08
	
		bobomb		= $09
	
		albatross_carry	= $0A
		albatross_right	= $0B
		albatross_left	= $0C
	
		ninji_running	= $0D
		ninji_jumping	= $0E
	
		beezo_dive	= $0F
		beezo_normal	= $10
	
		wart_bubble	= $11
	
		pidgit		= $12
	
		trouter		= $13
		hoopstar	= $14
	
		spawnjar_shyguy	= $15
		spawnjar_bobomb	= $16
	
		phanto		= $17

		cobrat_jar	= $18
		cobrat_sand	= $19

		pokey		= $1A

		bullet		= $1B
		birdo		= $1C

		mouser		= $1D

		egg		= $1E

		tryclyde	= $1F

		fireball	= $20
	
		panser_1	= $21
		panser_2	= $22
		panser_3	= $23
	
		autobomb	= $24
		autobomb_fire	= $25
	
		whale_spout	= $26
	
		flurry		= $27
		fryguy		= $28
		fryguy_split	= $29

		wart		= $2A

		doormask_boss	= $2B

		spark_1		= $2C
		spark_2		= $2D
		spark_3		= $2E
		spark_4		= $2F

		vegetable_small	= $30
		vegetable_large	= $31
		vegetable_wart	= $32
		bighead		= $33
		coin		= $34
		bomb		= $35

		rocket		= $36

		mask_block	= $37
		pow_block	= $38

		waterfall_log	= $39
	
		door_subspace	= $3A
		key		= $3B
	
		lamp		= $3C
		life_heart	= $3D
		one_up		= $3E
		flying_carpet	= $3F

		doormask_right	= $40
		doormask_left	= $41

		crystal_ball	= $42
		starman		= $43
		stopwatch	= $44

		swarm		= $45
		swarm_albatross	= $45
		swarm_beezo	= $46
		swarm_stop	= $47
		swarm_vegetables = $48
	.endenum

	.enum	actor_sprite
		unk_00		= $00
		unk_01		= $01
		unk_02		= $02
		unk_03		= $03
		unk_04		= $04
		unk_05		= $05
		unk_06		= $06
		unk_07		= $07
		unk_08		= $08
		unk_09		= $09
		unk_0A		= $0A
		unk_0B		= $0B
		unk_0C		= $0C
		unk_0D		= $0D
		unk_0E		= $0E
		unk_0F		= $0F
		unk_10		= $10
		unk_11		= $11
		unk_12		= $12
		unk_13		= $13
		unk_14		= $14
		unk_15		= $15
		unk_16		= $16
		unk_17		= $17
		unk_18		= $18
		unk_19		= $19
		unk_1A		= $1A
		unk_1B		= $1B
		unk_1C		= $1C
		unk_1D		= $1D
		unk_1E		= $1E
		unk_1F		= $1F
		unk_20		= $20
		unk_21		= $21
		unk_22		= $22
		unk_23		= $23
		unk_24		= $24
		unk_25		= $25
		unk_26		= $26
		unk_27		= $27
		unk_28		= $28
		unk_29		= $29
		unk_2A		= $2A
		unk_2B		= $2B
		unk_2C		= $2C
		unk_2D		= $2D
		unk_2E		= $2E
		unk_2F		= $2F
		unk_30		= $30
		unk_31		= $31
		unk_32		= $32
		unk_33		= $33
		unk_34		= $34
		unk_35		= $35
		unk_36		= $36
		unk_37		= $37
		unk_38		= $38
		unk_39		= $39
		unk_3A		= $3A
		unk_3B		= $3B
		unk_3C		= $3C
		unk_3D		= $3D
		unk_3E		= $3E
		unk_3F		= $3F
		unk_40		= $40
		unk_41		= $41
		unk_42		= $42
		unk_43		= $43
		unk_44		= $44
		unk_45		= $45
		unk_46		= $46
		unk_47		= $47
		unk_48		= $48
		unk_49		= $49
		unk_4A		= $4A
		unk_4B		= $4B
		unk_4C		= $4C
		unk_4D		= $4D
		unk_4E		= $4E
		unk_4F		= $4F
		unk_50		= $50

		one_up		= $51

		unk_52		= $52
		unk_53		= $53
		unk_54		= $54
		unk_55		= $55
		unk_56		= $56
		unk_57		= $57
		unk_58		= $58
		unk_59		= $59
		unk_5A		= $5A
		unk_5B		= $5B
		unk_5C		= $5C

		null		= $00
		none		= $FF
	.endenum

.endif	; ACTORS_I
