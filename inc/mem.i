.ifndef	MEM_I
MEM_I	= 1

.include	"system/cpu.i"

.include	"system/fds.i"

PHYSICS_X_Y_OFFSET	= $0A

zp_work			= $00

zp_byte_00		= $00
zp_word_00		= zp_byte_00
zp_addr_00		= zp_word_00
zp_dbyt_00		= zp_word_00
eng_tile_type   	= zp_byte_00

zp_byte_01		= $01
zp_addr_01		= zp_byte_01
map_bg_ptr		= zp_addr_01

zp_byte_02		= $02
zp_word_02		= zp_byte_02
zp_addr_02		= zp_word_02
zp_dbyt_02		= zp_word_02
eng_actor_render_dir	= $02

zp_byte_03		= $03
eng_actor_render_attr	= $03
eng_map_bg_bound_b	= $03

zp_byte_04		= $04
zp_addr_04		= zp_byte_04

zp_byte_05		= $05
zp_word_05		= $05
zp_map_pointer		= $05
eng_ppu_seam		= $05
eng_actor_render_attr_b = $05
state_actor_screen_lo	= $05

zp_byte_06		= $06
zp_addr_06		= zp_byte_06
state_actor_screen_hi	= zp_byte_06

zp_byte_07		= $07
zp_addr_07		= zp_byte_07

zp_byte_08		= $08

zp_byte_09		= $09

zp_byte_0A		= $0A
zp_addr_0A		= zp_byte_0A

zp_byte_0B		= $0B
eng_boom_block_y_lo_b	= $0B
eng_actor_chr_idx_page_id = $0B

zp_byte_0C		= $0C
zp_addr_0C		= $0C
eng_boom_block_x_lo	= $0C

zp_byte_0D		= $0D
eng_boom_block_x_hi	= $0D

zp_byte_0E		= $0E
eng_boom_block_y_lo	= $0E
eng_actor_doclip_x_lo	= $0E

zp_byte_0F		= $0F
eng_boom_block_y_hi	= $0F
eng_actor_doclip_x_hi	= $0F
eng_actor_sprite_id	= $0F
eng_scroll_skip_value	= $0F
lib_calc_page_offset	= $0F
eng_actor_physics_side_lo = $0F
eng_col_current_mask	= $0F
; -----------------------------
frame_count		= $10
nmi_buffer_ptr		= $11
actor_index		= $12
areainit_done		= $13
; -----------------------------
pos_x_hi_obj		= $14
pos_x_hi_player		= $14
pos_x_hi_actor		= $15
pos_x_hi_actor_end	= $1E
pos_y_hi_obj		= $1E
pos_y_hi_player		= $1E
pos_y_hi_actor		= $1F
pos_y_hi_actor_end	= $28
pos_x_lo_obj		= $28
pos_x_lo_player 	= $28
pos_x_lo_actor		= $29
pos_x_lo_actor_end	= $32
pos_y_lo_obj		= $32
pos_y_lo_player 	= $32
pos_y_lo_actor		= $33
pos_y_lo_actor_end	= $3C
veloc_x_obj		= $3C
veloc_x_player		= $3C
veloc_x_actor		= $3D
veloc_x_actor_end	= $46
veloc_y_obj		= $46
veloc_y_player		= $46
veloc_y_actor		= $47
veloc_y_actor_end	= $50
proc_id_obj		= $50
proc_id_player		= $50
proc_id_actor		= $51
proc_id_actor_end	= $5A
collision_obj		= $5A
collision_player	= $5A
collision_actor		= $5B
collision_actor_end	= $64
attr_obj		= $64
attr_player		= $64
attr_actor		= $65
attr_actor_end		= $6E
disp_dir_obj		= $6E
disp_dir_player		= $6E
disp_dir_actor		= $6F
disp_dir_actor_end	= $78
zp_obj_work_byte_1	= $78
zp_player_work_byte_1	= $78
zp_actor_work_byte_1	= $79
zp_actor_work_byte_1_end = $82
eng_timer_player_state	= $82
timer_player_traction	= $84
timer_obj		= $85
timer_player		= $85
timer_actor		= $86
timer_actor_end		= $8F
obj_id_obj		= $8F
obj_id_player		= $8F
obj_id_actor		= $90
obj_id_actor_end	= $99
; -----------------------------
player_airborne		= $99
player_crouch		= $9A
player_mov_phase	= $9B
player_item_held	= $9C
player_dir		= $9D
; -----------------------------
timer_ani_obj		= $9E
timer_ani_player	= $9E
timer_ani_actor		= $9F
timer_ani_actor_end	= $A8
timer_actor_held 	= $A8
timer_actor_held_end	= $B1
zp_actor_work_byte_2	= $B1
zp_actor_work_byte_2_end = $BA
eng_camera_move_x	= $BA
; -----------------------------
apu_track		= $BB
zp_addr_BD		= $BD
zp_addr_BF		= $BF
zp_addr_C1		= $C1
zp_byte_C5		= $C5
zp_byte_C6		= $C6
; -----------------------------
eng_player_ani_frame	= $C7
; -----------------------------
ppu_scc_v_b		= $C8
ppu_scc_h_b		= $C9
ppu_scc_v_h_b		= ppu_scc_v_b
pos_y_hi_screen		= $CA
pos_y_lo_screen		= $CB
course_actor_addr	= $CC
eng_map_bg_bound_tbl	= $CE
eng_map_bg_bound	= $CE
eng_map_bg_bound_parts_tbl = $CF
eng_map_bg_bound_lo	= $CF
eng_map_bg_bound_hi	= $D0
ppu_tilescroll_buffer_addr = $D1
ppu_tilescroll_buffer_addr_lo_tbl = $D3
ppu_tilescroll_buffer_addr_lo_left = $D3
ppu_tilescroll_buffer_addr_lo_right = $D4
zp_byte_D5		= $D5
eng_bgcopy_counter	= $D6
eng_scroll_data_offset	= $D7
ppu_scroll_req		= $D8
ppu_scroll_attr_buffer	= $D9
zp_byte_E1		= $E1
zp_byte_E2		= $E2
zp_byte_E3		= $E3
zp_byte_E4		= $E4
eng_map_bg_tile_off_h	= $E5
eng_map_bg_tile_off_v	= $E6
eng_map_bg_tile_off	= $E7
eng_map_fg_page_no	= $E8
course_data_addr	= $E9
ppu_nmi_hold		= $EB
eng_level_h		= $EC
zp_byte_ED		= $ED
zp_byte_EE		= $EE
zp_byte_EF		= $EF
title_game_enter_clear_end = $F0
; -----------------------------
ppu_displist_write_addr	= $F0
timer_title_wait	= $F2
title_no_disk		= $F3
zp_byte_F4		= $F4
; -----------------------------
joypad_p1		= FDS_JOYPAD_PRESS_1
joypad_p1h		= FDS_JOYPAD_HOLD_1
joypad_p2h		= FDS_JOYPAD_HOLD_2
ppu_scc_v		= FDS_PPU_SCC_V_B
ppu_scc_h		= FDS_PPU_SCC_H_B
ppu_ctlr1_b		= FDS_PPU_CTLR1_B
ppu_ctlr0_b		= FDS_PPU_CTLR0_B
; ------------------------------------------------------------
stack_var5E		= STACK_BASE+$5E
stack_varE9		= STACK_BASE+$E9
; ------------------------------------------------------------
oam_buffer		= $200
; ------------------------------------------------------------
ppu_displist		= $300
ppu_displist_offset	= $300
ppu_displist_data	= $301
ppu_displist_addr_dbyt	= $301
ppu_displist_special	= $303
ppu_displist_count	= $303
ppu_displist_payload	= $304
; ------------------------------------------------------------
ppu_tilescroll_buffer	= $380
ppu_bg_attr		= $3BC
ppu_hscroll_attr	= $3BE

byte_400		= $400
byte_401		= $401
byte_402		= $402
byte_403		= $403
obj_id_player_b		= $404
course_no_b		= $405
; -----------------------------
subpix_x_obj		= $407
subpix_x_player		= $407
subpix_x_actor		= $408
subpix_x_actor_end	= $411
subpix_y_obj		= $411
subpix_y_player		= $411
subpix_y_actor		= $412
subpix_y_actor_end	= $41B
; -----------------------------
player_lock		= $41B
game_scrolldir		= $425
eng_col_res		= $426
eng_col_res_h		= $426
eng_col_res_v		= $427
eng_player_screen_x	= $428
pos_x_rel_actor		= $429
eng_player_screen_y_hi	= $42A
eng_player_screen_y_lo	= $42B
pos_y_rel_actor		= $42C
player_item_held_id	= $42D
; -----------------------------
timer_actor_throw	= $42F
timer_actor_throw_end	= $438
timer_actor_stun	= $438
timer_actor_stun_end	= $441
eng_actor_data_offset	= $441
eng_actor_data_offset_end = $44A
timer_actor_shake	= $44A
timer_actor_shake_end	= $453
eng_timer_actor_2	= $453
eng_timer_actor_2_end	= $45C
timer_actor_flash	= $45C
timer_actor_flash_end	= $465
hp_actor		= $465
hp_actor_end		= $46E
actor_mode		= $46E
actor_mode_end		= $477
eng_actor_tbl_477	= $477
eng_actor_tbl_477_end	= $480
eng_actor_tbl_480	= $480
eng_actor_tbl_480_end	= $489
col_box_actor		= $489
col_box_actor_end	= $492
eng_actor_tbl_492	= $492
eng_actor_tbl_492_end	= $49B
eng_enemy_door_tbl	= $49B
eng_enemy_door_tbl_end	= $4A4
eng_actor_noholdtop	= $4A4
eng_actor_noholdtop_end	= $4AD
; -----------------------------
eng_actor_heart_counter	= $4AD
eng_area_started	= $4AE
eng_carried_actor	= $4AF
eng_key_used		= $4B0
eng_player_on_carpet	= $4B2
eng_timer_subspace	= $4B3
eng_doormask_closing	= $4B4
eng_doormask_open_size	= $4B5
eng_timer_doormask_open	= $4B6
eng_timer_subspace_2	= $4B7
eng_boss_beaten		= $4B8
eng_swarm_type		= $4B9
byte_4BA		= $4BA
byte_4BB		= $4BB
eng_sky_color		= $4BC
eng_door_timer_ani	= $4BD
pos_x_hi_screen_left	= $4BE
pos_x_hi_screen_right	= $4BF
pos_x_lo_screen_left	= $4C0
pos_x_lo_screen_right	= $4C1
hp_player		= $4C2
hp_player_max		= $4C3
eng_timer_pow_quake	= $4C4
eng_bg_y		= $4C5
eng_timer_sky_flash	= $4C6
eng_player_in_rocket	= $4C7
eng_timer_jump_float	= $4C9
eng_timer_crouch_jump	= $4CA
; -----------------------------
accel_x_obj		= $4CB
accel_x_player		= $4CB
accel_x_actor		= $4CC
accel_x_actor_end	= $4D5
accel_y_obj		= $4D5
accel_y_player		= $4D5
accel_y_actor		= $4D6
accel_y_actor_end	= $4DF
; -----------------------------
eng_player_quicksand	= $4DF
timer_player_star	= $4E0
; -----------------------------
pos_x_lo_player_old	= $4E1
pos_y_lo_player_old	= $4E2
pos_x_lo_screen_old	= $4E3
pos_y_lo_screen_old	= $4E4
veloc_y_player_old	= $4E5
proc_id_player_b	= $4E6
eng_levels_old		= $4E7
eng_level_area_old	= $4E8
eng_level_page_old	= $4E9
eng_transition_old	= $4EA
; -----------------------------
byte_4EB		= $4EB
proc_id_system		= $4EC
player_lives		= $4ED
eng_jar_type		= $4EE
eng_tbl_maskdoors	 = $04EF
eng_actor_fryguy_split_frame = $4F8
eng_scroll_lock_x	= $4FA
eng_actor_lifeheart_pull_tbl = $4FB
eng_pokey_scr_x_b	= $4FD
byte_4FE		= $4FE
eng_timer_stopwatch	= $4FF

byte_502		= $502
byte_504		= $504
byte_505		= $505
byte_506		= $506
word_506		= $506
byte_507		= $507
; -----------------------------
ppu_scc_v_backup	= $509
ppu_scc_h_backup	= $50A
ppu_scc_v_b_backup	= $50B
ppu_scc_h_b_backup	= $50C
byte_50D		= $50D
byte_50E		= $50E
pos_x_hi_player_b	= $50F
pos_y_hi_player_b	= $510
pos_x_lo_player_b	= $511
pos_y_lo_player_b	= $512
pos_y_hi_screen_backup	= $513
pos_x_hi_screen_left_backup = $514
pos_y_lo_screen_backup	= $515
zp_byte_E1_backup	= $517
eng_level_area_b	= $519
; -----------------------------
byte_51B		= $51B
ppu_has_tilescroll	= $51C
tbl_51D			= $51D
eng_level		= $531
eng_level_area		= $532
eng_level_page		= $533
eng_transition_type	= $534
eng_page_current	= $535
eng_map_page_x		= $536
byte_537		= $537
byte_538		= $538
byte_539		= $539
byte_53A		= $53A
byte_53D		= $53D
byte_53E		= $53E
byte_53F		= $53F
eng_map_bg_page_no	= $540
byte_541		= $541
byte_542		= $542
byte_543		= $543
byte_544		= $544
apu_song_index		= $545
; -----------------------------
tbl_546			= $546
eng_jump_height_standing = $54C
eng_jump_height_quicksand = $552
eng_jump_float_max	= $553
eng_gravity_normal	= $554
eng_gravity_jump	= $555
eng_gravity_quicksand	= $556
tbl_557			= $557
tbl_55A			= $55A
; -----------------------------
tbl_55D			= $55D
tbl_55E			= $55E
tbl_55F			= $55F
tbl_583			= $583
byte_588		= $588
byte_5A7		= $5A7
byte_5A8		= $5A8

pal_5B8			= $5B8
PAL_5B8_SIZE0		= $0C
PAL_5B8_SIZE1		= $10
; -----------------------------
byte_5EC		= $5EC
byte_5F0		= $5F0
byte_5F1		= $5F1
byte_5F2		= $5F2
byte_5F3		= $5F3
byte_5F4		= $5F4
byte_5F5		= $5F5
byte_5F6		= $5F6
byte_5F7		= $5F7
byte_5F8		= $5F8
byte_5F9		= $5F9
byte_5FA		= $5FA
byte_5FB		= $5FB
byte_5FC		= $5FC
byte_5FD		= $5FD
; --------------
apu_music_base_req	= $600
apu_dpcm_queue		= $601
byte_602		= $602
apu_music_queue_2	= $603
apu_sfx_queue2		= $604
byte_605		= $605
apu_musicplay_2		= $606
byte_607		= $607
apu_music_base_current	= $608
apu_note_len_offset	= $609
apu_pulse_2_offset	= $60A
apu_pulse_1_offset	= $60B
apu_triangle_offset	= $60C
apu_noise_offset	= $60D
byte_60E		= $60E
apu_pulse_2_note_len_b	= $610
apu_pulse_2_note_len	= $611
apu_pulse_2_env_ctl	= $612
apu_pulse_1_note_len	= $613
apu_pulse_1_env_ctl	= $614
apu_triangle_note_len_b	= $615
apu_triangle_note_len	= $616
apu_noise_note_len	= $617
byte_618		= $618
byte_61A		= $61A
apu_noise_loop_offset	= $61B
byte_61C		= $61C
byte_61D		= $61D
byte_61E		= $61E
apu_wave_offset		= $61F
; -----------------------------
byte_620		= $620
word_621		= $621
byte_623		= $623
eng_surface_traction	= $624
tbl_625			= $625
eng_transition_area	= $627
eng_inner_space		= $628
course_sub		= $629
byte_62A		= $62A
bonus_coins		= $62B
byte_62C		= $62C
game_save_dat		= $62D
game_save_state		= $631
course_no		= $635

byte_636		= $636
BYTE_636_VAL_A5		= $A5

palette_player		= $637
tbl_63B			= $63B
tbl_643			= $643
tbl_64B			= $64B
tbl_653			= $653
tbl_65B			= $65B
tbl_67B			= $67B
byte_682		= $682
tbl_693			= $693
tbl_6AB			= $6AB
tbl_6BC			= $6BC
byte_6C7		= $6C7
tbl_6CB			= $6CB
tbl_6D9			= $6D9
tbl_6DE			= $6DE
tbl_6E3			= $6E3
tbl_6E8			= $6E8

tbl_700			= $700
map_700			= $700

	.assert (veloc_y_player-veloc_x_player = (PHYSICS_X_Y_OFFSET)), error, "veloc_y offset mismatch"
	.assert (accel_y_player-accel_x_player = (PHYSICS_X_Y_OFFSET)), error, "accel_y offset mismatch"
	.assert (subpix_y_player-subpix_x_player = (PHYSICS_X_Y_OFFSET)), error, "subpix_y offset mismatch"
	.assert (pos_y_lo_player-pos_x_lo_player = (PHYSICS_X_Y_OFFSET)), error, "pos_y_lo offset mismatch"
	.assert (pos_y_hi_player-pos_x_hi_player = (PHYSICS_X_Y_OFFSET)), error, "pos_y_hi offset mismatch"

.endif	; MEM_I
