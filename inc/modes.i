.ifndef	MODES_I
MODES_I	= 1

	.enum	system_modes
		game		= $00
		title_card	= $01
		game_over	= $02
		bonus_round	= $03
		warp		= $04
	.endenum

	.enum	player_state
		normal		= 0
		climb		= 1
		lift		= 2
		climbtrans	= 3
		jarenter	= 4
		jarexit		= 5
		maskexit	= 6
		death		= 7
	.endenum

	.enum	transition_state
		reset		= 0
		door		= 1
		jar		= 2
		climb		= 3
		subspace	= 4
		rocket		= 5
	.endenum

	.enum actor_states
		null		= 0
		normal		= 1
		dead		= 2
		blockfizz	= 3
		bombboom	= 4
		smokepoof	= 5
		sand		= 6
		sinking		= 7
	.endenum

	.enum	nmi_buffer_modes
		mode_00
		mode_01
		mode_02
		mode_03
		mode_04
		mode_05
		mode_06
		mode_07
		mode_08
		mode_09
		mode_10
		mode_11
		mode_12
		mode_13
		mode_14
		mode_15
		mode_16
		mode_17
		mode_18
		mode_19
		mode_20
	.endenum

	.enum	nmi_title_buffer_modes
		ppu_displist
		title_bg
		attr_1
		attr_2
		attr_3
		attr_4
		attr_5
		attr_6
		attr_0
		window
		wait
	.endenum

.endif	; MODES_I
