.ifndef	TITLE_I
TITLE_I	= 1

.include	"mem.i"

timer_title		= zp_byte_02
timer_title_balloon	= zp_byte_06
timer_title_story_b	= frame_count
title_story_proc_id	= actor_index

TIMER_TITLE_WAIT_VAL    = 30

TITLE_BALLOON_A_OBJ_ID	= 0
TITLE_BALLOON_B_OBJ_ID	= 1
TITLE_BALLOON_C_OBJ_ID	= 2
TITLE_BLIMP_OBJ_ID	= 3

TITLE_BALLOON_COUNT	= 3

TITLE_BLANK_TILE	= $FE

.endif	; TITLE_I
