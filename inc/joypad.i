.ifndef	JOYPAD_I
JOYPAD_I	= 1

.include	"system/fds.i"

	joypad_read	= fdsbios_joypad_read

	.enum	joypad_button
		right	= fds_joypad_button::right
		left	= fds_joypad_button::left
		down	= fds_joypad_button::down
		up	= fds_joypad_button::up
		start	= fds_joypad_button::start
		select	= fds_joypad_button::select
		face_b	= fds_joypad_button::face_b
		face_a	= fds_joypad_button::face_a
	.endenum

.endif	; JOYPAD_I
